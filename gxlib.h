//--------------------------------------------------
//
// gxLib 2019 2019/1116
// written by ragi.
//
// 最新版はgitlabに登録されています。
// https://gitlab.com/garuru.software/gxlib2019/
//
//--------------------------------------------------

#ifndef _GX_LIB_H_
#define _GX_LIB_H_

//以下のプリプロセッサをコンパイラに登録してください　※windowsでは、VC環境にて設定済み
//#define GX_DEBUG		//デバッグコンパイル時に使用（デバッグ情報を出力）、最適化なし
#define GX_RELEASE	//マスターコンパイル環境と同等だが、デバッグ出力あり、最適化レベル最高
//#define GX_MASTER		//マスターコンパイル、デバッグ出力なし、最適化レベル最高


//---------------------------------------------------------------------------
//ユーザー定義
//---------------------------------------------------------------------------
#define APPLICATION_NAME "VISORISER"
#define FILENAME_CONFIG  "gxLib.sav"
#define DOCUMENT_MANUAL_URL 	 "http://www.garuru.co.jp/gxLib2020/manual"
#define DOCUMENT_LICENCE_URL     "http://www.garuru.co.jp/gxLib2020/License"

//アプリのバージョン番号(gxLibのバージョンではない)
#define VERSION_MAJOR   (2025)
#define VERSION_MINOR   (2)
#define VERSION_RELEASE (15)

//画面解像度（※１）
#define SCREEN_WIDTH  (1280)
#define SCREEN_HEIGHT (720)

//マルチ解像度対応用
//縦横で異なる解像度を取り扱いたいときに２つ目の解像度を記載する
//画面解像度（※１）が横の設定なら、こっちには縦の設定を書く
//#define ENABLE_MULTI_RESOLUTION
	#define SCREEN_WIDTH2  (900)
	#define SCREEN_HEIGHT2 (1600)

extern int WINDOW_W;
extern int WINDOW_H;
extern int FRAME_PER_SECOND;

#define MAX_RAM_MB			( 1024  )		//最大メモリ使用量制限 (MB)
#define MAX_ORDER_NUM		( 10000 )		//グラフィックのオーダー（同時）限界数
#define MAX_TEXT_NUM		( 256   )		//gxLib::Printfでリクエスト可能な限界数
#define MASTERTEXTURE_MAX	( 8 )			//マスターテクスチャの最大枚数
#define MAX_SOUND_NUM		( 128 )			//サウンドのバンク数（同時発音数はマシンのｃｈ数）
#define MAX_PRIORITY_NUM	( 256 )			//優先順位
#define RENDER_FPS			( 60 )			//VSYNC待ちに関するFPS
#define FILENAMEBUF_LENGTH  ( 512 )			//ファイルネームに使用するバッファサイズ
#define PLAYER_MAX 			( 2 )			//ユーザー数（とりあえず２人以上）
#define DEFAULT_FRAME_PER_SECOND	( 60 )	//ゲーム処理に関するFPS(1秒間のゲームのUpdate回数、描画はマシンパワーによる)

//---------------------------------------------------------------------------
//型宣言
//---------------------------------------------------------------------------
#define LF "\r\n"

#include "gxLib/hard/gxTypes.h"
#include "gxLib/hard/machine.h"

enum {
	//標準的なボタンのアサイン
	enBtnDecision = BTN_A,	//決定
	enBtnCancel   = BTN_B,	//キャンセル
};

/*
L2                       R2
L1                       R1
  +  SELECT START   X Y Z
	   A1    A2     A B C
*/


class gxLib {

public:

	static void Destroy();

	struct StSprite
	{
		//スプライト設定用構造体
		gxTransform trans;
		//gxVector3  pos    = { 0 , 0 , 0 };
		//gxVector3  rot    = { 0.0f, 0.0f , 0.0f };
		//gxVector3  scl    = { 1.0f, 1.0f , 1.0f };
		Uint32     atr    = ATR_DFLT;
		Uint32     argb   = 0xffffffff;
		Uint32     blend  = 0x00000000;
		gxSprite   albedo = {-1,0,0,1,1};
		gxSprite   pallet = {-1,0,0,1,1};
		gxSprite   normal = {-1,0,0,1,1};
		Uint32     shader = 0;

		//点光源用（法線マップを使わなければ不必要）
		Float32 plight_pos[3] = { 0.f , 0.f , 1.0f };
		Float32 plight_rgb[3] = { 1.0f , 1.0f , 1.0f };
		Float32 plight_intensity = 1.0f;

		Float32    option[4] = { 0 };
		gxBool bHidden = gxFalse;
		union free {
			Sint8   i8[16];
			Sint16  i16[8];
			Sint32  i32[4];
			Float32 f32[4];
		} free;


		void Set(
			gxSprite *pSpr,
			Uint32 _atr = ATR_DFLT,
			Uint32 _col = ARGB_DFLT,
			Float32 _r = 0,
			Float32 _sx = 1.0f,
			Float32 _sy = 1.0f,
			Uint32 _blendColor = 0)
			{
				albedo = *pSpr;
				atr = _atr;
				argb = _col;
				trans.rot.z = _r;
				trans.scl.x = _sx;
				trans.scl.y = _sy;
				blend = _blendColor;
			}

		void Draw()
		{
			Draw(0, 0, 0);
		}

		void Draw(int x, int y , int z )
		{
			if (bHidden) return;

			gxLib::PutSprite(&albedo,
				x+trans.pos.x, y+trans.pos.y, z+trans.pos.z,
				atr, argb,
				trans.rot.z,
				trans.scl.x, trans.scl.y
			);

		}

	};

	//-----------------------------------------------------------
	//グラフィック関連
	//-----------------------------------------------------------

	//画面消去時の背景色を設定します
	//※RGBがゼロなら背景を消去しない
	//※Aはオフスクリーン全体のアルファ制御
	static void SetBgColor( Uint32 argb );

	//点
	static Uint32 DrawPoint( Sint32 x1,	Sint32 y1, Sint32 prio, Uint32 atr, Uint32 argb , Float32 fSize = 1.0f );

	//円
	static Uint32 DrawCircle( Sint32 x1, Sint32 y1, Sint32 prio, Uint32 atr, Uint32 argb , Float32 fRadius = 1.0f , Float32 fSize = 1.0f );

	//線
	static Uint32 DrawLine( Sint32 x1,	Sint32 y1,	Sint32 x2 , Sint32 y2 , Sint32 prio, Uint32 atr, Uint32 col , Float32 fSize = 1.0f );

	//三角形を描画します（ノンテクスチャ）
	static Uint32 DrawTriangle(
			Sint32 x1 , Sint32 y1,
			Sint32 x2 , Sint32 y2 ,
			Sint32 x3 , Sint32 y3 ,
			Sint32 prio,
			gxBool bFill,
			Uint32 atr=ATR_DFLT,	Uint32 col=ARGB_DFLT );

	//三角形（ノンテクスチャ＆グラデーションカラー）
	static Uint32 DrawColorTriangle(
			Sint32 x1 ,	Sint32 y1 ,Uint32 argb1,
			Sint32 x2 , Sint32 y2 ,Uint32 argb2,
			Sint32 x3 , Sint32 y3 ,Uint32 argb3,
			Sint32 prio,
			Uint32 atr );

	//四角形（ノンテクスチャ）
	static Uint32 DrawBox(
			//始点～終点座標
			Sint32 x1,		Sint32 y1,	Sint32 x2 , Sint32 y2 , Sint32 prio,

			//塗りつぶすか？
			gxBool bFill,
			Uint32 atr=ATR_DFLT,
			Uint32 col=ARGB_DFLT , Float32 fSize = 0.0f );

	//変形四角形
	//グラデーションの箱を描くことができる
	//※左上から右回りに頂点を設定する
	static Uint32 DrawColorBox(
			Sint32 x1 ,	Sint32 y1 ,Uint32 argb1,
			Sint32 x2 , Sint32 y2 ,Uint32 argb2,
			Sint32 x3 , Sint32 y3 ,Uint32 argb3,
			Sint32 x4 , Sint32 y4 ,Uint32 argb4,
			Sint32 prio,
			Uint32 atr );

	//テクスチャ付き三角形
	static Uint32 PutTriangle(
			Sint32 x1 , Sint32 y1 ,Sint32 u1,Sint32 v1,
			Sint32 x2 , Sint32 y2 ,Sint32 u2,Sint32 v2,
			Sint32 x3 , Sint32 y3 ,Sint32 u3,Sint32 v3,
			Sint32 prio,
			Sint32 tpg,
			Uint32 atr=ATR_DFLT,	Uint32 col=ARGB_DFLT , Uint32 blendColor=0x00000000 );


	//テクスチャ付き四角形
	static Uint32 PutSprite(

		//表示位置、優先順位
		Sint32 x,		Sint32 y,	Sint32 prio,

		//テクスチャページ、始点（XY）、幅、高さ
		Sint32 page,	Sint32 u, 	Sint32 v,	Sint32 w,	Sint32 h,

		//中心位置オフセット
		Sint32 cx=0,
		Sint32 cy=0,

		//オプション＆カラー
		Uint32 atr = ATR_DFLT,
		Uint32 col = ARGB_DFLT,

		//回転（deg）
		Float32 r  = 0,

		//拡大率
		Float32 sx = 1.0f,
		Float32 sy = 1.0f,
		Uint32 blendColor = 0);

	//（UVデータ構造体を渡して）テクスチャ付き四角形を描画します
	static Uint32 PutSprite(
		gxSprite* pSpr, Sint32 x,		Sint32 y,	Sint32 prio,	Uint32 atr = ATR_DFLT,	Uint32 col = ARGB_DFLT,
		Float32 r  = 0,	Float32 sx = 1.0f,	Float32 sy = 1.0f , Uint32 blendColor = 0);

	//（UVデータ構造体を渡して）テクスチャ付き四角形を描画します
	static Uint32 PutSprite( StSprite *pSprDesc , Sint32 x=0, Sint32 y = 0, Sint32 z = 0);

	//テクスチャをファイルからマスターテクスチャへ読み込みます
	static gxBool LoadTexture ( Uint32 texPage , const gxChar* fileName , Uint32 colorKey=0xff00ff00 ,Uint32 ox = 0 , Uint32 oy = 0 , Sint32 *w=nullptr , Sint32 *h=nullptr  );

	//テクスチャをメモリからマスターテクスチャへ読み込みます
	static gxBool ReadTexture ( Uint32 texPage , Uint8* pBuffer , size_t Size , Uint32 colorKey=0xff00ff00 ,Uint32 ox = 0 , Uint32 oy = 0 , Sint32 *w=nullptr , Sint32 *h=nullptr );

	//画面をキャプチャーする
	//キャプチャー時にラスタースクロールさせる
	static void CaptureScreen(Sint32 prio);
	static void PostProcessGreyScale(Sint32 prio, Float32 fRatio = 1.0f);
	static void PostProcessRasterScroll( Sint32 prio , Sint32 timeOffset = -1 , Float32 fRasterPow=1.0f , Float32 fRasterRatio=0.1f );

	static gxBool ChangeRenderTarget( Sint32 prio , Sint32 changePage );

	//マスターテクスチャをVRAMに転送します
	//※処理が重たいので、更新時にすべてのテクスチャを読み込んでから１回だけ行います
	static gxBool UploadTexture ( gxBool bForce = gxFalse , Sint32 forceTpg = -1  );

	//シザリング（プライオリティで制御）
	static Uint32 Scissor( Sint32 x1, Sint32 y1, Sint32 w , Sint32 h , Sint32 prio );

	//指定したプライオリティ以前の画像にDOFのブラー効果を作りだします
	static void CreatePostEffectBlur( Sint32 prio , Float32 fRatio = 0.5f , Sint32 Level=1 );

	static void CreatePostEffectBloom( Sint32 prio , Float32 fRatio = 0.5f , Float32 fRange=1.0f );

	//シェーダーテスト用
	static void ShaderTest( Sint32 prio , Float32 fRatio );

	//-----------------------------------------------------------
	//サウンド関連
	//-----------------------------------------------------------


	//サウンドファイルをメモリから指定バンクに読み込みます
	static gxBool ReadAudio( Uint32 uIndex , Uint8* pMemory , size_t uSize );

	//サウンドファイルをファイルから指定バンクに読み込みます
	static gxBool LoadAudio( Uint32 uIndex , const gxChar* pFileName );

	//指定バンクのサウンドを再生します
	static gxBool PlayAudio( Uint32 index , gxBool bLoop = gxFalse , gxBool bOverWrap = gxFalse , Uint32 uFrm = 0 );

	//指定バンクのサウンドを停止します
	static gxBool StopAudio( Uint32 index , Uint32 uFrm=0 );

	//指定バンクのサウンドを停止します
	static gxBool StopAllAudio( Uint32 uFrm=0 );

	//特定バンクのサウンドボリュームを設定します
	static gxBool  SetAudioVolume ( Uint32 index ,Float32 fVolume );
	static Float32 GetAudioVolume ( Uint32 index );

	//すべてのバンクのサウンドのボリュームを一括して調整します
	static gxBool  SetAudioMasterVolume( Float32 fVolume );
	static Float32 GetAudioMasterVolume();

	//特定バンクのサウンドの再生状況を返します
	static gxBool IsPlayAudio( Uint32 uIndex );

	//指定バンクのサウンドの音程を変更します
	static gxBool SetAudioPitch( Uint32 index , Float32 fRatio );
	static gxBool SetAudioPann( Uint32 uIndex , Float32 fPan );
	static gxBool SetAudioReverb( Uint32 uIndex , gxBool bEnable );

	//現在の時刻をミリ秒単位で取得します

    static Float32 GetTime( gxClock *pInfo = nullptr );
	static Float32 GetDeltaTime();

	//-----------------------------------------------------------
	//コントローラーデバイス
	//-----------------------------------------------------------

	//コントローラーの入力情報を得る
	static StJoyStat* Joy( Uint32 playerID = 0 );

	//キーボードデバイスからの入力情報を得る
	static Uint8 KeyBoard( Uint32 n );

	//タッチデバイスからの入力情報を得る
	static StTouch* Touch( Sint32 n );

	//すべての振動機能を一括ON/OFFする
	static void EnableRumble( gxBool bRumbleOn = gxTrue);

	//左右のモーターを振動させる(止めるときは両方にゼロを設定する)
	static void SetRumble( Sint32 playerID , Uint32 frm = 8, Float32 pann = 0.0f , Float32 vol = 1.0f  );

	//ボタンコンフィグを有効にする
	static void EnablePadConfig( gxBool ConfigOn = gxTrue );

	//アプリ側の指定のボタン操作を入れ替える（アプリ側のコンフィグは入力ボタンの入れ替えでのみ実現する　※二重アサインを防ぐため）
	static void GamePadConfigSwapButton( Uint32 playerID ,  EJoyBit btn1 , EJoyBit btn2 );

	//カスタマイズ設定を元に戻す
	static void GamePadConfigResetDefault( Uint32 playerID );

	//現在の設定を（ボタンごとに）取得する
	static EJoyBit GetGamePadConfigButton(Uint32 playerID, EJoyBit btn1 );

	//---------------------------------------------------------------------------------------------------------
	//ファイルアクセス
	//---------------------------------------------------------------------------------------------------------

		//-----------------------------------------------------------
		//■ [ 読み込み専用 ] Asset領域
		//-----------------------------------------------------------

		static Uint8* LoadFile( const gxChar* pFileName , size_t* pLength );
		static gxBool SaveFile( const gxChar* pFileName, void* pData, size_t uSize );
		static gxBool RemoveFile( const gxChar* pFileName );

		static Uint8* LoadStorageFile( const gxChar* pFileName, size_t* pLength , ESTORAGE_LOCATION location = ESTORAGE_LOCATION::AUTO );
		static gxBool SaveStorageFile( const gxChar* pFileName, void* pData, size_t uSize , ESTORAGE_LOCATION location = ESTORAGE_LOCATION::AUTO);
		static gxBool RemoveStorageFile( const gxChar* pFileName, ESTORAGE_LOCATION location = ESTORAGE_LOCATION::AUTO );

		//-----------------------------------------------------------
		//■ [ 読み書き可能 ] メモリーカード領域
		//-----------------------------------------------------------
		// メモリーカード領域からファイルを取得する
		//static Uint8* LoadMemoryCard( gxChar* pFileName , Uint32* pLength );

		//メモリーカード領域にファイルを保存する（どちらも同じ挙動）

		//gxLibの専用設定ファイルをセーブ / ロードします
		static gxBool SaveConfig();
		static gxBool LoadConfig();

		//-----------------------------------------------------------
		//■ [ 書き込み専用 ] SDカード領域
		//-----------------------------------------------------------

		//SDカード領域にファイルを保存する（SDカード領域は読み込み不可）
		//static gxBool SaveSDCard( gxChar* pFileName , Uint8* pData ,Uint32 uSize );

	//---------------------------------------------------------------------------------------------------------
	//その他システム系
	//---------------------------------------------------------------------------------------------------------

	static void OpenWebView( gxChar *pFormat , ... );
    static void OpenWebBrowser( gxChar *pFormat , ... );
	static void CloseWebView();
    //WEB上のファイルを取得する
    static Uint8* LoadWebFile( gxChar* pURL , size_t* pLength , const std::vector<uint8_t> &postdata = std::vector<uint8_t>() );

    static void SetToast( gxChar *pString , ... );

    //アプリ解説書の表示
	static void OpenAppManual(gxChar *pFormat = nullptr );

	//ライセンスの表示
	static void OpenEULADocument(gxChar *pFormat = nullptr );

	//ネットワークの接続確認
	gxBool IsOnline();

	static void BlueToothOn( gxBool bBlueToothOn = gxTrue );
	static gxBool IsBlueToothOn();

	//BlueToothデータ送信
	static gxBool BlueToothDataSend( Uint8* pData , size_t uSize);

	//BlueToothデータ受信
	static Uint8* BlueToothDataRecv(size_t*uSize );

	//スレッドを作成する
	static void CreateThread( void* (*pFunc)(void*) , void * pArg );
	static void Sleep( Uint32 msec );

	//IPアドレスを取得する
	static Uint32 GetIPAddressV4( gxBool bLocalAddress = gxFalse );

	//Unique IDを取得する
	static Uint32 GetUID();

	//---------------------------------------------------------------------------------------------------------
	//その他
	//---------------------------------------------------------------------------------------------------------
	static gxBool IsPortrait();
	static gxBool IsLandscape();

	static void SetFPS(Sint32 fps);
	static Sint32 GetFPS();
	static void ChangeFullScreen( gxBool bFullscreen );

    static void SetClipBoard(std::string clipBoardString );
    static std::string GetClipBoard();

	static void DispStatusBar( gxBool bOn );

    static Float32 GetBenchmarkScore();

	//Deviceネイティブの画面サイズと実ゲーム画面サイズを得る
	static void GetDeviceResolution( Sint32 *gw , Sint32 *gh , Sint32 *sw=nullptr , Sint32 *sh= nullptr);

	//Deviceネイティブの座標に変換する
	static void GetNativePosition( Sint32 *pCX , Sint32 *pCY );

	//バーチャルパッドの表示ON/OFF切り替え
	static void SetVirtualPad( gxBool bDispOn , Sint32 TypeFlag = 0x00 );

	//アチーブメント関連情報の取得と設定
	static gxBool GetAchievement( Uint32 achieveindex );
	static gxBool SetAchievement( Uint32 achieveindex );

    static void SetRenderFPS( Sint32 frm);

	//リプレイ関連

	static void RecordReplay( gxBool bRecord );
	static gxBool LoadReplayFile( const gxChar *pFileName );
	static gxBool SaveReplayFile( const gxChar *pFileName );
	static gxBool IsReplaying();
	static gxBool IsRecoedPlay();


#if 0
	!!!!

	//NOW Loading表示
	static void NowLoading( gxBool bLoading , Sint32 ptn );

	//OKのみのPopup表示
	static void PopUp( gxChar *pMessage , gxBool bLoading );

	//YES/NoのみのPopup表示
	static Sint32 DialogYesNo( gxChar *pMessage );

	//自動的に消える通知表示
	static void PopUpText( gxChar *pMessage , Uint32 frm );
#endif

	//-----------------------------------------------------------
	//デバッグ用
	//-----------------------------------------------------------

	//デバッグ文字を表示します
	static void Printf( Sint32 x , Sint32 y , Sint32 prio , Uint32 atr , Uint32 argb , gxChar* pFormat , ... );
	static void Printf(Sint32 x, Sint32 y, Sint32 prio, gxChar* pFormat, ...);

	//デバッグコンソールにデバッグ文字を出力します
	static void DebugLog( const gxChar* pFormat , ... );

	//（xorShift法の）ランダム値を生成する
	static Sint32 Rand( Uint32 uSeed=0 );

	//毎フレームインクリメントされるカウンタを取得する
	static Uint32 GetGameCounter();

	//アプリの一時停止（コード側から制御）
	static void Pause( gxBool bPauseOn = gxTrue );

	//アプリの終了（コード側から制御）
	static void Exit();

	//デバッグ用のスイッチを設定する
	static void   SetDebugSwitch( Sint32 n , gxBool bOn , gxBool bToggle = gxFalse );

	//デバッグ用のフラグを確認する
	static gxBool IsDebugSwitchOn(Sint32 n);


	//-----------------------------------------------------------
	//gxLibの設定ファイルです
	//-----------------------------------------------------------

	struct StSaveData {

		//設定ファイル構造体

		enum {
			enSaveDataFreeArea = 1024,
		};
		enum {
			ENABLE_VPAD,
			ENABLE_SOUND,
			FULLSCREEN,
			DIPSWITCH0,
			DIPSWITCH1,
			DIPSWITCH2,
			DIPSWITCH3,
			DIPSWITCH4,
			DIPSWITCH5,
			DIPSWITCH6,
			DIPSWITCH7,
		};

		Uint8 HEAD[3]{'G','X','A'};
		Sint8 Initialized = false;
		Uint32  Version_no   = VERSION_NUMBER;
		Float32 MasterVolume = 1.0f;
		Uint32  Flags        = 0x00000000;

		Sint16  WindowSizeW  = 0;
		Sint16  WindowSizeH  = 0;
		Sint16  WindowSizeX = 0;
		Sint16  WindowSizeY = 0;
		Sint8   RenderMode = 0;
		Sint8   RenderFilter = 0;
		Sint8   dummy[6] = { 0 };

		//Achievement
		Uint32 Achievement[8]={0};	//256フラグ

		//コントローラー設定

		Uint8 PadAssign[ PLAYER_MAX ][ BTN_MAX ];

		union _free {
			Uint8  u8[ enSaveDataFreeArea ];
			Uint16 u16[ enSaveDataFreeArea>>1 ];
			Uint32 u32[ enSaveDataFreeArea>>2 ];
		} free;


		StSaveData()
		{
			makeDefault();
		}

		void SetFlag(Uint8 id , gxBool on)
		{
			Flags &= ~(1 << (id));
			if (on)
			{
				Flags |= (1 << (id));
			}
		}

		gxBool GetFlag(Uint8 id)
		{
			return Flags & (1 << (id));
		}


		void makeDefault()
		{
			//デフォルトデータを作る
			Version_no   = VERSION_NUMBER;
			MasterVolume = 0.75f;
			Initialized = false;
			RenderMode   = 0;
			RenderFilter = 0;
			SetFlag(ENABLE_SOUND,gxTrue);
			SetFlag(ENABLE_VPAD, gxFalse);

			for( Sint32 ii=0;ii<PLAYER_MAX; ii ++ )
			{
				for( Sint32 jj=0;jj<BTN_MAX; jj ++ )
				{
					gxLib::SaveData.PadAssign[ii][jj] = jj;
				}
			}
		}

	};

	static StSaveData SaveData;

private:



};

#include "gxLib/util/gxUtil.h"

#define INLINE inline

#endif
