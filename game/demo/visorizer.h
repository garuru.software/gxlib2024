﻿//----------------------------------------------
//シューティングゲーム
//shooting.h
//----------------------------------------------
#include <gxLib.h>

typedef struct StSpriteDef
{
	int tpg;	//テクスチャページ番号
	int u,v;	//座標位置	UV ( 8 x 8 キャラ単位 ）
	int w,h;	//サイズ	WH ( 8  x 8 キャラ単位 ）
	int cx,cy;	//真中位置	CX,CY ( 1 ドット単位 ）

} StSpriteDef;

extern StSpriteDef SprVisor[];

void Visorizer( int bReset );

class visorSys
{
public:
	enum {
		WinW = 160,
		WinH = 160,
		Scale = 2,
	};
	visorSys(){}
	~visorSys(){}

	static void Vibration(int frm = 8)
	{
		//gxLib::SetRumble( 0  );
	}

	static void SoundLoad(Sint32 n , char *name )
	{
		gxLib::LoadAudio( n , name );
	}

	static void SoundPlay(Sint32 n)
	{
		if( n == 0 )
		{
			gxLib::PlayAudio( n , gxTrue );
		}
		else
		{
			gxLib::PlayAudio( n );
		}
		if( n == 3 )
		{
			gxLib::SetAudioVolume( n , 1.f );
		}
		else
		{
			gxLib::SetAudioVolume( n , 0.95f );
		}
	}
    static void SoundStop(Sint32 n)
    {
        gxLib::StopAudio( n  );
    }

	static void PutSprite(int px, int py, int sprno,int prio,int flip)
	{
		if( flip )
		{
			PutSpriteVisor(px,py,sprno,prio,ATR_FLIP_X,0xffffffff,1);
		}
		else
		{
			PutSpriteVisor(px,py,sprno,prio,ATR_DFLT,0xffffffff,1);
		}
	}

	static void PutSpriteVisor(int x,int y,int spr,int prio,int atr=ATR_DFLT , unsigned long col=ARGB_DFLT,float ras=0)
	{
		Sint32 cx = WINDOW_W/2;
		Sint32 cy = WINDOW_H/2;
		Sint32 lx = cx-WinW/2*Scale;
		Sint32 ly = cy-WinH/2*Scale;

		if(ras < 1) ras=1;

		if( col&0xff000000 ) col |= 0xff000000;
		else				 col &= 0x00ffffff;

		y -= 12*Scale;

		gxLib::PutSprite(
			//転送先設定
			 lx+ x*Scale , ly + y*Scale , prio,
			//転送元ページ
			 SprVisor[spr].tpg,				
			 SprVisor[spr].u*8, 	SprVisor[spr].v*8,		SprVisor[spr].w*8, SprVisor[spr].h*8,		//UVWH
			//センター位置
			 SprVisor[spr].cx,	SprVisor[spr].cy
			//オプション(デフォルト設定あり)
			 ,atr
			 ,col
			 ,0
			 ,ras*Scale
			 ,ras*Scale
			);

			if( prio > 5 )
			{
				//影
				gxLib::PutSprite(
					//転送先設定
					 lx+x*Scale - 2*Scale ,ly+y*Scale + 2*Scale , 0,
					//転送元ページ
					 SprVisor[spr].tpg,				
					 SprVisor[spr].u*8, 	SprVisor[spr].v*8,		SprVisor[spr].w*8, SprVisor[spr].h*8,		//UVWH
					//センター位置
					 SprVisor[spr].cx,	SprVisor[spr].cy
					//オプション(デフォルト設定あり)
					 ,0
					 ,0xff010101
					 ,0
					 ,ras*Scale
					 ,ras*Scale
					);
			}

		}

		static void SetBgColor(Uint32 argb )
		{
			gxLib::SetBgColor( argb );
		}

private:

};
