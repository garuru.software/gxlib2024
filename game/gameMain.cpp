// ===========================================================================
//
// gxLib 2024
//
// 2024.12.24 written by ragi.
// ===========================================================================

#include <gxLib.h>

gxBool GamePause()	 { return gxTrue; }
gxBool GameSleep()	 { return gxTrue; }
gxBool GameResume()	 { return gxTrue; }
gxBool GameEnd()	 { return gxTrue; }
gxBool GameReset()	 { return gxTrue; }
gxBool DragAndDrop (gxChar* pFileName) { return gxTrue; }
gxBool GameInit	   ( std::vector<std::string> args ){ return gxTrue; }

gxBool GameMain()
{
	return gxTrue;
}


