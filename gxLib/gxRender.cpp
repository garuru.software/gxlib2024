//--------------------------------------------------
//
// gxRender.h
// レンダリングする為の情報を管理します
//
//--------------------------------------------------

#include <gxLib.h>
#include "gx.h"
#include "gxOrderManager.h"
#include "gxTexManager.h"
#include "gxRender.h"
#include "gxDebug.h"

SINGLETON_DECLARE_INSTANCE( gxRender )

#define INVALID_TEXTURE    (-99)
#define NONE_TEXTURE (-1)
#define INVALID_ATTR    (0xffffffff)
#define INVALID_ORDER   (0xffffffff)

//#define PRIOtoZ(prio) ( (prio>=MAX_PRIORITY_NUM)? 0.0f : 1.0f - (1.0f*(prio)/MAX_PRIORITY_NUM_) )
//#define PRIOtoZ(prio) ( (prio>=MAX_PRIORITY_NUM)? 0.0f : (1.0f - (1.0f*(prio)/MAX_PRIORITY_NUM_)-0.5f)*2.0f )
#define PRIOtoZ(prio) ( (prio>=MAX_PRIORITY_NUM)? 0.0f : ( ( (1.0f*prio)/(MAX_PRIORITY_NUM)-0.5f)*-2.0f ) )

Sint32 m_sWindowResolutionW = WINDOW_W;
Sint32 m_sWindowResolutionH = WINDOW_H;

gxRender::gxRender()
{

	m_pCommand     = new CCommandList[enCommandMax];
	m_pCutomVertex = new StCustomVertex[enVertexMax];
	m_pIndexBuffer = new Uint32[enIndexMax];

	for( Sint32 ii=0; ii<enMapMax; ii++ )
	{
		m_sOldBankNum[ii]  = INVALID_TEXTURE;
	}
	m_sStartVertex = 0;
	m_sLastVertex  = 0;
	m_sStartIndex  = 0;
	m_sLastIndex   = 0;

	m_sOldAttribute = INVALID_ATTR;
	m_sLastOdrType  = INVALID_ORDER;

	m_sIBufferNum = 0;
	m_sIndexCnt   = 0;

	m_fTexturePageWidth  = gxTexManager::enMasterWidth;
	m_fTexturePageHeight = gxTexManager::enMasterHeight;
	m_sPageX = gxTexManager::enMasterWidth / gxTexManager::enPageWidth;

	m_uBackGroundColor = 0xff010101;
	SetClearColor( m_uBackGroundColor );

}


gxRender::~gxRender()
{
	delete[] m_pCommand;
	delete[] m_pCutomVertex;
	delete[] m_pIndexBuffer;
}


void gxRender::Action()
{
	//レンダリング用のコマンドを生成する

	Sint32 sMax = gxOrderManager::GetInstance()->GetOrderNum();

	m_sPPrioOverIndex     = -1;
	m_GameStartOrderIndex = -1;

//	gxOrderManager *pOdrMgr = gxOrderManager::GetInstance();

	//コマンド初期化

	m_sCommandNum = 0;
	m_sVBufferNum = 0;
	m_sIBufferNum = 0;

	for( Sint32 ii=0; ii<enMapMax; ii++ )
	{
		m_sOldBankNum[ii]  = INVALID_TEXTURE;
	}
	m_sStartVertex  = 0;
	m_sLastVertex   = 0;
	m_sStartIndex   = 0;
	m_sLastIndex	= 0;
	m_sIndexCnt     = 0;

	m_sOldAttribute = INVALID_ATTR;
	m_sLastOdrType  = INVALID_ORDER;
	m_OldShaderProgramIndex = gxShaderReset;

	//zソート
	gxOrderManager::GetInstance()->ZSort();

	Uint32* pZsortBuf = gxOrderManager::GetInstance()->getZsortBuf();

	m_sWindowResolutionW = WINDOW_W;
	m_sWindowResolutionH = WINDOW_H;

	for(Uint16 ii=0; ii<sMax; ii++)
	{
		makeCommand( (Uint32)pZsortBuf[ii] );

		if (m_sVBufferNum >= enVertexMax)
		{
			m_sVBufferNum = enVertexMax - 1;
		}
	}

	if (m_sPPrioOverIndex == -1)
	{
		//PrioMax以上がなければ全部描くことにする
		m_sPPrioOverIndex = m_sCommandNum;
	}

	gxDebug::GetInstance()->UpdateVertexInfo( m_sCommandNum , m_sVBufferNum , m_sIBufferNum );

}


INLINE void gxRender::makeCommand( Uint32 index )
{
	//コマンドを生成する

	StOrder* pOdr     = gxOrderManager::GetInstance()->get( index );

	if( m_GameStartOrderIndex == -1 )
	{
		if( pOdr->prio >= 0 )
		{
			m_GameStartOrderIndex = m_sCommandNum;
			for( Sint32 ii=0; ii<enMapMax; ii++ )
			{
				m_sOldBankNum[ii]  = INVALID_TEXTURE;
			}
			m_sOldAttribute = INVALID_ATTR;
			m_sLastOdrType  = INVALID_ORDER;
			m_sWindowResolutionW = WINDOW_W;
			m_sWindowResolutionH = WINDOW_H;
		}
		else
		{
			CGameGirl::GetInstance()->GetWindowsResolution(&m_sWindowResolutionW, &m_sWindowResolutionH);
		}
	}

	if( m_sPPrioOverIndex == -1 )
	{
		if( pOdr->prio >= MAX_PRIORITY_NUM )
		{
			m_sPPrioOverIndex = m_sCommandNum;
			CGameGirl::GetInstance()->GetWindowsResolution(&m_sWindowResolutionW, &m_sWindowResolutionH);

			for( Sint32 ii=0; ii<enMapMax; ii++ )
			{
				m_sOldBankNum[ii]  = INVALID_TEXTURE;
			}

			m_sOldAttribute = INVALID_ATTR;
			m_sLastOdrType  = INVALID_ORDER;
		}
	}

	m_sIndexCnt = m_sVBufferNum;

	switch( pOdr->sType ){
	case enOrderTypePoint:
		attachTexture( NONE_TEXTURE );
		setAttribute( pOdr->sAtribute );
		changeShader();
		drawPoint( pOdr );

		while( pOdr->m_pNextOrder )
		{
			drawPoint( pOdr->m_pNextOrder );
			pOdr = pOdr->m_pNextOrder;
		}
		break;

	case enOrderTypeTriangle:
		attachTexture( NONE_TEXTURE );
		setAttribute( pOdr->sAtribute );
		changeShader();
		drawTriangle( pOdr );
		while( pOdr->m_pNextOrder )
		{
			drawTriangle( pOdr->m_pNextOrder );
			pOdr = pOdr->m_pNextOrder;
		}
		break;

	case enOrderTypeLine:
		attachTexture( NONE_TEXTURE );
		setAttribute( pOdr->sAtribute );
		changeShader();
		drawLine( pOdr );
		while( pOdr->m_pNextOrder )
		{
			drawLine( pOdr->m_pNextOrder );
			pOdr = pOdr->m_pNextOrder;
		}
		break;

	case enOrderTypWired:
		attachTexture( NONE_TEXTURE );
		setAttribute( pOdr->sAtribute );
		changeShader();
		drawLineStrip( pOdr );
		while( pOdr->m_pNextOrder )
		{
			drawLineStrip( pOdr->m_pNextOrder );
			pOdr = pOdr->m_pNextOrder;
		}
		break;

	case enOrderTypeNoneTexPolygon:
		attachTexture( NONE_TEXTURE );
		setAttribute( pOdr->sAtribute );
		changeShader();
		drawSprite( pOdr );
		while( pOdr->m_pNextOrder )
		{
			drawSprite( pOdr->m_pNextOrder );
			pOdr = pOdr->m_pNextOrder;
		}
		break;

	case enOrderTypeTexturePolygon:
		mappingTextures( pOdr );
		setAttribute( pOdr->sAtribute );
        changeShader((gxShaderType)pOdr->shader);
		//changeShader();
		drawSprite( pOdr );
		while( pOdr->m_pNextOrder )
		{
			drawSprite( pOdr->m_pNextOrder );
			pOdr = pOdr->m_pNextOrder;
		}
		break;

	case enOrderTypeTextureTriangle:
		//attachTexture( gxTexManager::GetInstance()->getBankIndex(pOdr->pg) );
		mappingTextures( pOdr );
		setAttribute( pOdr->sAtribute );
		changeShader( (gxShaderType)pOdr->shader );
		drawTriangleSprite( pOdr );
		while( pOdr->m_pNextOrder )
		{
			drawTriangleSprite( pOdr->m_pNextOrder );
			pOdr = pOdr->m_pNextOrder;
		}
		break;
/*
//	case enOrderTypeFont:
//		drawFont( pOdr );
//		changeShader();
//		attachTexture( NONE_TEXTURE );
//		break;
*/
	case enOrderTypeChangeRenderTarget:
		{
			stCmd.sType = eCmdChangeRenderTarget;
			stCmd.x     = 0;
			stCmd.y     = 0;
			stCmd.arg1  = 0;
			stCmd.arg2  = 0;
			stCmd.arg3  = 0;
			stCmd.arg4  = 0;
			//stCmd.pString = NULL;
			addCommand( &stCmd );
			m_sOldBankNum[enTexmapAlbedo]  = INVALID_TEXTURE;	//次のテクスチャは新規にアタッチする
		}
		break;

	case enOrderTypeChangeScissor:
		{
			stCmd.sType = eCmdScissor;
			stCmd.x     = 0;
			stCmd.y     = 0;
			stCmd.arg1 = pOdr->u;
			stCmd.arg2 = pOdr->v;
			stCmd.arg3 = pOdr->w;
			stCmd.arg4 = pOdr->h;
			//stCmd.pString = NULL;
			addCommand( &stCmd );
		}
		break;

	case enOrderTypeProcessingBlur:
		{
			Sint32 max = 1;
			stCmd.sType = eCmdProcessingBlur;
			stCmd.x     = 0;
			stCmd.y     = 0;
			stCmd.arg1  = pOdr->x1[0]*10000;	//ブラー強度
			stCmd.arg2  = pOdr->x1[1];	//ブラー回数
			stCmd.arg3  = 0;
			stCmd.arg4  = 0;
			//stCmd.pString = NULL;
			addCommand( &stCmd );
			changeShader();
			m_sOldBankNum[enTexmapAlbedo]  = INVALID_TEXTURE;
		}
		break;


	case enOrderTypeProcessingBloom:
		{
			//Bloom
			stCmd.sType = eCmdProcessingBloom;
			stCmd.arg1 = pOdr->x1[0] * 10000;	//ブラー強度
			stCmd.arg2 = pOdr->x1[1] * 100;		//閾値
			//stCmd.pString = NULL;
			addCommand( &stCmd );
			m_sOldBankNum[enTexmapAlbedo]  = INVALID_TEXTURE;
			changeShader();
		}
		break;

	case enOrderTypeDevelop:
		{
			stCmd.sType = eCmdDevelop;
			stCmd.arg1  = pOdr->opt;
			//stCmd.pString = NULL;
			addCommand( &stCmd );
			m_sOldBankNum[enTexmapAlbedo]  = INVALID_TEXTURE;
			changeShader();
		}
		break;

	case enOrderTypeCaptureScreen:
		stCmd.sType = eCmdCaptureScreen;
		stCmd.arg1  = 10000*pOdr->x1[0];
		stCmd.arg2  = 10000*pOdr->x1[1];
		stCmd.arg3  = 10000*pOdr->x1[2];
		stCmd.arg4  = 10000*pOdr->x1[3];
		//stCmd.pString = NULL;
		addCommand( &stCmd );
		m_sOldBankNum[enTexmapAlbedo]  = INVALID_TEXTURE;
		changeShader();
		break;
	case enOrderTypeProcessingGreyScale:
	{
		Sint32 max = 1;
		stCmd.sType = eCmdPostProcessGreyScale;
		stCmd.x = 0;
		stCmd.y = 0;
		stCmd.arg1 = pOdr->option[0]*1000;	//白黒ぐあい
		stCmd.arg2 = 0;
		stCmd.arg3 = 0;
		stCmd.arg4 = 0;
		//stCmd.pString = NULL;
		addCommand(&stCmd);
		changeShader();
		m_sOldBankNum[enTexmapAlbedo] = INVALID_TEXTURE;

	}
	break;

	default:
		break;
	}

	return;

}


INLINE void gxRender::drawSprite( StOrder* pOdr )
{
	//４頂点を確定する

	if (pOdr->sx == 0.f || pOdr->sy == 0.f || ( pOdr->color[0]==0 ) )
	{
		//そもそも描画しない（一番速い）
		return;
	}

	pOdr->x1[0] = (Float32)-pOdr->cx;
	pOdr->y1[0] = (Float32)-pOdr->cy;

	pOdr->x1[1] = (Float32)-pOdr->cx + pOdr->w;
	pOdr->y1[1] = (Float32)-pOdr->cy;

	pOdr->x1[2] = (Float32)-pOdr->cx + pOdr->w;
	pOdr->y1[2] = (Float32)-pOdr->cy + pOdr->h;

	pOdr->x1[3] = (Float32)-pOdr->cx;
	pOdr->y1[3] = (Float32)-pOdr->cy + pOdr->h;

	spriteNormal( pOdr );

}


#define ODR2VTX( V , N ) \
	V->x = 2.0f*( pOdr->x1[N]+0.f) / (m_sWindowResolutionW-1);	\
	V->y = 2.0f*((pOdr->y1[N]+0.f) / (m_sWindowResolutionH-1))*-1.0f;	\
	V->z = PRIOtoZ(pOdr->prio);	\
	V->rhw = 0.0f;	\
	V->r = ((pOdr->color[N]&0x00ff0000)>>16)/255.0f;	\
	V->g = ((pOdr->color[N]&0x0000ff00)>>8 )/255.0f;	\
	V->b = ((pOdr->color[N]&0x000000ff)>>0 )/255.0f;	\
	V->a = ((pOdr->color[N]&0xff000000)>>24)/255.0f;	\
	V->cx = (2.0f* pOdr->x / (m_sWindowResolutionW/1.0f))- 1.0f;	\
	V->cy = (2.0f*(pOdr->y / (m_sWindowResolutionH/1.0f))- 1.0f)*-1.0f;	\
	V->sx = pOdr->sx;	\
	V->sy = pOdr->sy;	\
	V->rot = DEG2RAD(pOdr->rz);	\
	V->r2 = ((pOdr->blend&0x00ff0000)>>16)/255.0f;	\
	V->g2 = ((pOdr->blend&0x0000ff00)>>8 )/255.0f;	\
	V->b2 = ((pOdr->blend&0x000000ff)>>0 )/255.0f;	\
	V->a2 = ((pOdr->blend&0xff000000)>>24)/255.0f;	\
	V->fx = (pOdr->sAtribute&ATR_FLIP_X)? -1.0f : 1.0f;	\
	V->fy = (pOdr->sAtribute&ATR_FLIP_Y)? -1.0f : 1.0f;	\
	V->px = 2.0f*(pOdr->plight_pos[0]/m_sWindowResolutionW)-1.0f;	\
	V->py = (2.0f*(pOdr->plight_pos[1]/m_sWindowResolutionH)-1.0f)*-1.0f;	\
	V->pz = pOdr->plight_pos[2];	\
	V->pr = pOdr->plight_rgb[0];	\
	V->pg = pOdr->plight_rgb[1];	\
	V->pb = pOdr->plight_rgb[2];	\
	V->plength = pOdr->plight_intensity;	\
    V->option[0] = pOdr->option[0];    \
    V->option[1] = pOdr->option[1];    \
    V->option[2] = pOdr->option[2];    \
    V->option[3] = pOdr->option[3];    \

INLINE void gxRender::setVertex( StCustomVertex *pVtx1 , StOrder* pOdr )
{
	//頂点情報を与える
	Sint32 sVtxCnt = m_sVBufferNum;
	Sint32 sIdxCnt = m_sIBufferNum;

	ODR2VTX(pVtx1,0);
	m_sVBufferNum ++;
	pVtx1 ++;

	ODR2VTX(pVtx1,1);
	m_sVBufferNum ++;
	pVtx1 ++;

	ODR2VTX(pVtx1,3);
	m_sVBufferNum ++;
	pVtx1 ++;

	ODR2VTX(pVtx1,2);
	m_sVBufferNum ++;
	pVtx1 ++;

	m_pIndexBuffer[m_sIBufferNum] = m_sIndexCnt+0;	m_sIBufferNum ++;
	m_pIndexBuffer[m_sIBufferNum] = m_sIndexCnt+1;	m_sIBufferNum ++;
	m_pIndexBuffer[m_sIBufferNum] = m_sIndexCnt+2;	m_sIBufferNum ++;

	m_pIndexBuffer[m_sIBufferNum] = m_sIndexCnt+2;	m_sIBufferNum ++;
	m_pIndexBuffer[m_sIBufferNum] = m_sIndexCnt+1;	m_sIBufferNum ++;
	m_pIndexBuffer[m_sIBufferNum] = m_sIndexCnt+3;	m_sIBufferNum ++;
	m_sIndexCnt += (m_sVBufferNum - sVtxCnt);
}



INLINE void gxRender::setTextureUV( Sint32 tpg , StCustomVertex *pVtx0 , StOrder* pOdr )
{
	//テクスチャにUV座標を与える
	if( tpg == enNoneTexturePage || tpg == enWiredPage )
	{
		return;
	}

	pVtx0->u = getTetureOffsetX( tpg , pOdr->u );
	pVtx0->v = 1.0f-getTetureOffsetY( tpg , pOdr->v );
	pVtx0->u2 = (     getTetureOffsetX( pOdr->pg[1] , pOdr->map_u[1] )) - pVtx0->u;		//法線データへのＵＶオフセット
	pVtx0->v2 = (1.0f-getTetureOffsetY( pOdr->pg[1] , pOdr->map_v[1] )) - pVtx0->v;

	Float32 x,y;
	x = pVtx0->u2;
	y = pVtx0->v2;

	pVtx0++;


	pVtx0->u   = getTetureOffsetX( tpg , pOdr->u+pOdr->w );
	pVtx0->v   = 1.0f-getTetureOffsetY( tpg , pOdr->v );
	pVtx0->u2 = x;
	pVtx0->v2 = y;
	pVtx0 ++;

	pVtx0->u   = getTetureOffsetX( tpg , pOdr->u );
	pVtx0->v   = 1.0f-getTetureOffsetY( tpg , pOdr->v+pOdr->h );
	pVtx0->u2 = x;
	pVtx0->v2 = y;
	pVtx0 ++;

	pVtx0->u   = getTetureOffsetX( tpg , pOdr->u+pOdr->w );
	pVtx0->v   = 1.0f-getTetureOffsetY( tpg , pOdr->v+pOdr->h );
	pVtx0->u2 = x;
	pVtx0->v2 = y;
	pVtx0 ++;

}


INLINE Float32 gxRender::getTetureOffsetX( Sint32 page , Sint32 x )
{
	//Sint32 ox = page%m_sPageX;
	if (page == enNoneTexturePage || page == enWiredPage)
	{
		return 0.0f;
	}

	//if (page == enBackBuffer || page == enCapturePage)
	if(page <= -100)
	{
		return 1.0f * x / WINDOW_W;
	}

	Sint32 page1024 = (page/16)%4;
	Sint32 page256 = page % 16;

	Sint32 ox;
	ox =  (page1024 % 2) * 1024;
	ox += (page256 % 4 ) * gxTexManager::enPageWidth;

	x += ox;

	return x/m_fTexturePageWidth;
}

INLINE Float32 gxRender::getTetureOffsetY( Sint32 page , Sint32 y )
{
	//Sint32 oy = page/m_sPageX;
	//oy = oy % 4;
	//oy *= gxTexManager::enPageHeight;
	//y += oy;

	if (page == enNoneTexturePage || page == enWiredPage)
	{
		return 0.0f;
	}

	//if (page == enBackBuffer || page == enCapturePage)
	if(page <= -100)
	{
		return 1.0f * y / WINDOW_H;
	}

	Sint32 page4 = (page / 16) % 4;
	Sint32 page16 = page % 16;

	Sint32 oy;
	oy =  (page4/2) * 1024;
	oy += (page16 / 4) * gxTexManager::enPageHeight;

	y += oy;

	return y/m_fTexturePageHeight;
}


INLINE void gxRender::setAttribute( Uint32 sAtr )
{
	//アトリビュートの設定

	if( sAtr == m_sOldAttribute )
	{
		return;
	}

	m_sOldAttribute = sAtr;

	if( sAtr&ATR_ALPHA_ADD )
	{
		stCmd.sType = eCmdChgAttributeAlphaAdd;
		addCommand( &stCmd );
	}
	else if( sAtr&ATR_ALPHA_SUB )
	{
		stCmd.sType = eCmdChgAttributeAlphaSub;
		addCommand( &stCmd );
	}
	else if( sAtr&ATR_ALPHA_CRS )
	{
		stCmd.sType = eCmdChgAttributeAlphaCrs;
		addCommand( &stCmd );
	}
	else if( sAtr&ATR_ALPHA_RVS )
	{
		stCmd.sType = eCmdChgAttributeAlphaRvs;
		addCommand( &stCmd );
	}
	else if( sAtr&ATR_ALPHA_XOR )
	{
		stCmd.sType = eCmdChgAttributeAlphaXor;
		addCommand( &stCmd );
	}
	else if( sAtr&ATR_ALPHA_SCR )
	{
		stCmd.sType = eCmdChgAttributeAlphaScr;
		addCommand( &stCmd );
	}
	else
	{
		stCmd.sType = eCmdChgAttributeAlphaNml;
		addCommand( &stCmd );
	}
}

// -----------------------------------------------------------------------------------
// 描画コマンド発行
// -----------------------------------------------------------------------------------
INLINE void gxRender::mappingTextures( StOrder* pOdr )
{
	//テクスチャ関連をまとめてアタッチする

	attachTexture( gxTexManager::GetInstance()->getBankIndex(pOdr->pg[0]) , enTexmapAlbedo );

    if( pOdr->shader == gxShaderNormal )
	{
		//法線マップがアタッチされていた場合
		attachTexture( gxTexManager::GetInstance()->getBankIndex(pOdr->pg[1]) , enTexmapNormal );
		attachTexture(gxTexManager::GetInstance()->getBankIndex(pOdr->pg[1]), enTexmapCapture);
		//changeShader(gxShaderNormal);
	}
/*
	else if( pOdr->sAtribute & ATR_MAP_PALLET )
	{
		//パレットマップがアタッチされていた場合
		attachTexture( gxTexManager::GetInstance()->getBankIndex(pOdr->pg[2]) , enTexmapPallet );
		//changeShader(gxShaderPallet);
	}
*/
	else
	{
		//changeShader();
	}
}



INLINE void gxRender::attachTexture( Sint32 sBankIndex  , Sint32 mapType )
{
	//テクスチャのアタッチ

	if( sBankIndex == m_sOldBankNum[mapType] )
	{
		//前回と同じテクスチャページならアタッチしない
		return;
	}

	m_sOldBankNum[mapType] = sBankIndex;

	if( mapType == enTexmapAlbedo )
	{
		if( sBankIndex == NONE_TEXTURE )
		{
			//ノンテクスチャ
			stCmd.sType = eCmdBindNoneTexture;
			addCommand( &stCmd );
		}
		else
		{
			//テクスチャ
			stCmd.sType = eCmdBindAlbedoTexture;
			stCmd.arg1  = sBankIndex;
			addCommand( &stCmd );
		}
	}
	else if( mapType == enTexmapNormal )
	{
		stCmd.sType = eCmdBindNormalTexture;
		stCmd.arg1  = sBankIndex;
		addCommand( &stCmd );
	}
	else if( mapType == enTexmapPallet )
	{
		stCmd.sType = eCmdBindPalletTexture;
		stCmd.arg1  = sBankIndex;
		addCommand( &stCmd );
	}
	else if (mapType == enTexmapCapture)
	{
		stCmd.sType = eCmdBindCaptureTexture;
		stCmd.arg1 = TEXPAGE_CAPTURE;
		addCommand(&stCmd);
	}

}


INLINE void gxRender::changeShader( gxShaderType shaderIndex )
{
	//シェーダーを変更する

	if (shaderIndex == gxShaderReset)
	{
		shaderIndex = gxShaderDefault;
	}

	if( shaderIndex != m_OldShaderProgramIndex )
	{
		m_ShaderProgramIndex = shaderIndex;
		m_OldShaderProgramIndex = m_ShaderProgramIndex;

		stCmd.sType = eCmdChangeShader;
		stCmd.arg1  = m_ShaderProgramIndex;
		addCommand( &stCmd );
	}
}


INLINE void gxRender::drawPoint( StOrder* pOdr )
{
	//ポイント描画

	StCustomVertex *pVtx0 = &m_pCutomVertex[ m_sVBufferNum ];
	StCustomVertex *pVtx1 = pVtx0;

	Sint32 sVtxCnt = m_sVBufferNum;
	Sint32 sIdxCnt = m_sIBufferNum;

	ODR2VTX(pVtx1,0);

	pVtx1->a = ((pOdr->color[0]&0xff000000)>>24)/255.0f;
	pVtx1->r = ((pOdr->color[0]&0x00ff0000)>>16)/255.0f;
	pVtx1->g = ((pOdr->color[0]&0x0000ff00)>>8 )/255.0f;
	pVtx1->b = ((pOdr->color[0]&0x000000ff)>>0 )/255.0f;

	m_sVBufferNum ++;
	pVtx1 ++;

	m_pIndexBuffer[m_sIBufferNum] = m_sIndexCnt;	m_sIBufferNum ++;
	m_sIndexCnt ++;

	stCmd.sType        = eCmdRenderPoint;
	stCmd.sStartVertex = sVtxCnt;
	stCmd.sLastVertex  = m_sVBufferNum;
	stCmd.sStartIndex  = sIdxCnt;
	stCmd.sLastIndex   = m_sIBufferNum;

	addCommand( &stCmd );

}


INLINE void gxRender::drawLine( StOrder* pOdr )
{
	//ライン描画

	StCustomVertex *pVtx0 = &m_pCutomVertex[ m_sVBufferNum ];
	StCustomVertex *pVtx1 = pVtx0;

	Sint32 sVtxCnt = m_sVBufferNum;
	Sint32 sIdxCnt  = m_sIBufferNum;

	if( pOdr->opt )
	{
		//ラインストリップ（ボックスを描画 ）

		ODR2VTX(pVtx1,0);
		pVtx1->a = ((pOdr->color[0]&0xff000000)>>24)/255.0f;
		pVtx1->r = ((pOdr->color[0]&0x00ff0000)>>16)/255.0f;
		pVtx1->g = ((pOdr->color[0]&0x0000ff00)>>8 )/255.0f;
		pVtx1->b = ((pOdr->color[0]&0x000000ff)>>0 )/255.0f;

		pVtx1 ++;
		m_sVBufferNum ++;

		ODR2VTX(pVtx1,1);
		pVtx1->a = ((pOdr->color[1]&0xff000000)>>24)/255.0f;
		pVtx1->r = ((pOdr->color[1]&0x00ff0000)>>16)/255.0f;
		pVtx1->g = ((pOdr->color[1]&0x0000ff00)>>8 )/255.0f;
		pVtx1->b = ((pOdr->color[1]&0x000000ff)>>0 )/255.0f;

		pVtx1 ++;
		m_sVBufferNum ++;

		ODR2VTX(pVtx1,2);
		pVtx1->a = ((pOdr->color[2]&0xff000000)>>24)/255.0f;
		pVtx1->r = ((pOdr->color[2]&0x00ff0000)>>16)/255.0f;
		pVtx1->g = ((pOdr->color[2]&0x0000ff00)>>8 )/255.0f;
		pVtx1->b = ((pOdr->color[2]&0x000000ff)>>0 )/255.0f;

		pVtx1 ++;
		m_sVBufferNum ++;

		ODR2VTX(pVtx1,3);
		pVtx1->a = ((pOdr->color[3]&0xff000000)>>24)/255.0f;
		pVtx1->r = ((pOdr->color[3]&0x00ff0000)>>16)/255.0f;
		pVtx1->g = ((pOdr->color[3]&0x0000ff00)>>8 )/255.0f;
		pVtx1->b = ((pOdr->color[3]&0x000000ff)>>0 )/255.0f;

		pVtx1 ++;
		m_sVBufferNum ++;

		ODR2VTX(pVtx1,0);
		pVtx1->a = ((pOdr->color[0]&0xff000000)>>24)/255.0f;
		pVtx1->r = ((pOdr->color[0]&0x00ff0000)>>16)/255.0f;
		pVtx1->g = ((pOdr->color[0]&0x0000ff00)>>8 )/255.0f;
		pVtx1->b = ((pOdr->color[0]&0x000000ff)>>0 )/255.0f;

		pVtx1 ++;
		m_sVBufferNum ++;

		m_pIndexBuffer[m_sIBufferNum] = m_sIndexCnt+0;	m_sIBufferNum ++;
		m_pIndexBuffer[m_sIBufferNum] = m_sIndexCnt+1;	m_sIBufferNum ++;
		m_pIndexBuffer[m_sIBufferNum] = m_sIndexCnt+2;	m_sIBufferNum ++;
		m_pIndexBuffer[m_sIBufferNum] = m_sIndexCnt+3;	m_sIBufferNum ++;
		m_pIndexBuffer[m_sIBufferNum] = m_sIndexCnt+4;	m_sIBufferNum ++;
		m_sIndexCnt += (m_sVBufferNum - sVtxCnt);

		stCmd.sType        = eCmdRenderLineStrip;
		stCmd.sStartVertex = sVtxCnt;
		stCmd.sLastVertex  = m_sVBufferNum;
		stCmd.sStartIndex  = sIdxCnt;
		stCmd.sLastIndex   = m_sIBufferNum;
		stCmd.arg1         = pOdr->sx*100;
		addCommand( &stCmd );

	}
	else
	{
		//ライン
		ODR2VTX(pVtx1,0);
		pVtx1->a = ((pOdr->color[0]&0xff000000)>>24)/255.0f;
		pVtx1->r = ((pOdr->color[0]&0x00ff0000)>>16)/255.0f;
		pVtx1->g = ((pOdr->color[0]&0x0000ff00)>>8 )/255.0f;
		pVtx1->b = ((pOdr->color[0]&0x000000ff)>>0 )/255.0f;

		m_sVBufferNum ++;
		pVtx1 ++;

		ODR2VTX(pVtx1,1);
		pVtx1->a = ((pOdr->color[1]&0xff000000)>>24)/255.0f;
		pVtx1->r = ((pOdr->color[1]&0x00ff0000)>>16)/255.0f;
		pVtx1->g = ((pOdr->color[1]&0x0000ff00)>>8 )/255.0f;
		pVtx1->b = ((pOdr->color[1]&0x000000ff)>>0 )/255.0f;

		m_sVBufferNum ++;
		pVtx1 ++;

		m_pIndexBuffer[m_sIBufferNum] = m_sIndexCnt+0;	m_sIBufferNum ++;
		m_pIndexBuffer[m_sIBufferNum] = m_sIndexCnt+1;	m_sIBufferNum ++;
		m_sIndexCnt += (m_sVBufferNum - sVtxCnt);

		stCmd.sType        = eCmdRenderLineNormal;
		stCmd.sStartVertex = sVtxCnt;
		stCmd.sLastVertex  = m_sVBufferNum;
		stCmd.sStartIndex  = sIdxCnt;
		stCmd.sLastIndex   = m_sIBufferNum;
		stCmd.arg1         = pOdr->sx*100;
		addCommand( &stCmd );
	}

}


INLINE void gxRender::drawTriangle( StOrder* pOdr )
{
	//トライアングル描画

	StCustomVertex *pVtx0 = &m_pCutomVertex[ m_sVBufferNum ];
	StCustomVertex *pVtx1 = pVtx0;

	Sint32 sVtxCnt = m_sVBufferNum;
	Sint32 sIdxCnt  = m_sIBufferNum;

	ODR2VTX(pVtx1,0);
	pVtx1->a = ((pOdr->color[0]&0xff000000)>>24)/255.0f;
	pVtx1->r = ((pOdr->color[0]&0x00ff0000)>>16)/255.0f;
	pVtx1->g = ((pOdr->color[0]&0x0000ff00)>>8 )/255.0f;
	pVtx1->b = ((pOdr->color[0]&0x000000ff)>>0 )/255.0f;

	m_sVBufferNum ++;
	pVtx1 ++;

	ODR2VTX(pVtx1,1);
	pVtx1->a = ((pOdr->color[1]&0xff000000)>>24)/255.0f;
	pVtx1->r = ((pOdr->color[1]&0x00ff0000)>>16)/255.0f;
	pVtx1->g = ((pOdr->color[1]&0x0000ff00)>>8 )/255.0f;
	pVtx1->b = ((pOdr->color[1]&0x000000ff)>>0 )/255.0f;

	m_sVBufferNum ++;
	pVtx1 ++;

	ODR2VTX(pVtx1,2);
	pVtx1->a = ((pOdr->color[2]&0xff000000)>>24)/255.0f;
	pVtx1->r = ((pOdr->color[2]&0x00ff0000)>>16)/255.0f;
	pVtx1->g = ((pOdr->color[2]&0x0000ff00)>>8 )/255.0f;
	pVtx1->b = ((pOdr->color[2]&0x000000ff)>>0 )/255.0f;

	m_sVBufferNum ++;
	pVtx1 ++;

	m_pIndexBuffer[m_sIBufferNum] = m_sIndexCnt+0;	m_sIBufferNum ++;
	m_pIndexBuffer[m_sIBufferNum] = m_sIndexCnt+1;	m_sIBufferNum ++;
	m_pIndexBuffer[m_sIBufferNum] = m_sIndexCnt+2;	m_sIBufferNum ++;
	m_sIndexCnt += (m_sVBufferNum - sVtxCnt);

	stCmd.sType        = eCmdRenderTriangle;
	stCmd.sStartVertex = sVtxCnt;
	stCmd.sLastVertex  = m_sVBufferNum;
	stCmd.sStartIndex  = sIdxCnt;
	stCmd.sLastIndex   = m_sIBufferNum;

	addCommand( &stCmd );

}


INLINE void gxRender::drawTriangleSprite( StOrder* pOdr )
{
	//トライアングルースプライト描画

	StCustomVertex *pVtx0 = &m_pCutomVertex[ m_sVBufferNum ];
	StCustomVertex *pVtx1 = pVtx0;

	Sint32 sVtxCnt = m_sVBufferNum;
	Sint32 sIdxCnt  = m_sIBufferNum;

	ODR2VTX(pVtx1,0);
	pVtx1->a = ((pOdr->color[0]&0xff000000)>>24)/255.0f;
	pVtx1->r = ((pOdr->color[0]&0x00ff0000)>>16)/255.0f;
	pVtx1->g = ((pOdr->color[0]&0x0000ff00)>>8 )/255.0f;
	pVtx1->b = ((pOdr->color[0]&0x000000ff)>>0 )/255.0f;

	pVtx1->u = getTetureOffsetX( pOdr->pg[0] , (Sint32)pOdr->u1[0] );
	pVtx1->v = 1.0f-getTetureOffsetY( pOdr->pg[0], (Sint32)pOdr->v1[0] );
	pVtx0 ++;

	m_sVBufferNum ++;
	pVtx1 ++;

	ODR2VTX(pVtx1,1);
	pVtx1->a = ((pOdr->color[1]&0xff000000)>>24)/255.0f;
	pVtx1->r = ((pOdr->color[1]&0x00ff0000)>>16)/255.0f;
	pVtx1->g = ((pOdr->color[1]&0x0000ff00)>>8 )/255.0f;
	pVtx1->b = ((pOdr->color[1]&0x000000ff)>>0 )/255.0f;
	pVtx1->u = getTetureOffsetX( pOdr->pg[0], (Sint32)pOdr->u1[1] );
	pVtx1->v = 1.0f-getTetureOffsetY( pOdr->pg[0], (Sint32)pOdr->v1[1] );

	m_sVBufferNum ++;
	pVtx1 ++;

	ODR2VTX(pVtx1,2);
	pVtx1->a = ((pOdr->color[2]&0xff000000)>>24)/255.0f;
	pVtx1->r = ((pOdr->color[2]&0x00ff0000)>>16)/255.0f;
	pVtx1->g = ((pOdr->color[2]&0x0000ff00)>>8 )/255.0f;
	pVtx1->b = ((pOdr->color[2]&0x000000ff)>>0 )/255.0f;
	pVtx1->u = getTetureOffsetX( pOdr->pg[0], (Sint32)pOdr->u1[2] );
	pVtx1->v = 1.0f-getTetureOffsetY( pOdr->pg[0], (Sint32)pOdr->v1[2] );

	m_sVBufferNum ++;
	pVtx1 ++;

	m_pIndexBuffer[m_sIBufferNum] = m_sIndexCnt+0;	m_sIBufferNum ++;
	m_pIndexBuffer[m_sIBufferNum] = m_sIndexCnt+1;	m_sIBufferNum ++;
	m_pIndexBuffer[m_sIBufferNum] = m_sIndexCnt+2;	m_sIBufferNum ++;
	m_sIndexCnt += (m_sVBufferNum - sVtxCnt);

	stCmd.sType        = eCmdRenderTriangle;
	stCmd.sStartVertex = sVtxCnt;
	stCmd.sLastVertex  = m_sVBufferNum;
	stCmd.sStartIndex  = sIdxCnt;
	stCmd.sLastIndex   = m_sIBufferNum;
	addCommand( &stCmd );

}


INLINE void gxRender::drawLineStrip( StOrder* pOdr )
{
	//ワイヤーライン描画

	Sint32 tpg = pOdr->pg[0];	//テクスチャページ
	StCustomVertex *pVtx0 = &m_pCutomVertex[ m_sVBufferNum ];
	StCustomVertex *pVtx1 = pVtx0;

	Sint32 sVtxCnt = m_sVBufferNum;
	Sint32 sIdxCnt = m_sIBufferNum;

	pOdr->x1[0] = (Float32)pOdr->x;
	pOdr->y1[0] = (Float32)pOdr->y;

	pOdr->x1[1] = (Float32)pOdr->x+pOdr->w;
	pOdr->y1[1] = (Float32)pOdr->y;

	pOdr->x1[2] = (Float32)pOdr->x+pOdr->w;
	pOdr->y1[2] = (Float32)pOdr->y+pOdr->h;

	pOdr->x1[3] = (Float32)pOdr->x;
	pOdr->y1[3] = (Float32)pOdr->y+pOdr->h;

	//lineStrip描画に作業を振る

	StOrder tmp = *pOdr;

	tmp.opt = 1;
	tmp.prio = PRIOtoZ(pOdr->prio);
	tmp.x1[0]    = pOdr->x1[0];
	tmp.y1[0]    = pOdr->y1[0];
	tmp.color[0] = pOdr->color[0];

	tmp.x1[1]    = pOdr->x1[1];
	tmp.y1[1]    = pOdr->y1[1];
	tmp.color[1] = pOdr->color[1];

	tmp.x1[2]    = pOdr->x1[2];
	tmp.y1[2]    = pOdr->y1[2];
	tmp.color[2] = pOdr->color[2];

	tmp.x1[3]    = pOdr->x1[3];
	tmp.y1[3]    = pOdr->y1[3];
	tmp.color[3] = pOdr->color[3];

	tmp.x = 0;
	tmp.y = 0;
	tmp.cx = 0;
	tmp.cy = 0;

	drawLine( &tmp );

}


INLINE void gxRender::spriteNormal( StOrder* pOdr )
{
	//スプライト

	Sint32 tpg = pOdr->pg[0];	//テクスチャページ

	StCustomVertex *pVtx0 = &m_pCutomVertex[ m_sVBufferNum ];
	StCustomVertex *pVtx1 = pVtx0;

	Sint32 sVtxCnt = m_sVBufferNum;
	Sint32 sIdxCnt = m_sIBufferNum;

	pVtx1 = pVtx0;

	//頂点設定
	setVertex( pVtx1, pOdr );

	pVtx1 = pVtx0;

	//テクスチャ設定
	setTextureUV(tpg , pVtx1, pOdr );

	stCmd.sType        = eCmdRenderSquare;
	stCmd.sStartVertex = sVtxCnt;
	stCmd.sLastVertex  = m_sVBufferNum;
	stCmd.sStartIndex  = sIdxCnt;
	stCmd.sLastIndex   = m_sIBufferNum;
	addCommand( &stCmd );

}


INLINE void gxRender::drawFont( StOrder* pOdr )
{
	//フォントの表示

	stCmd.sType = eCmdRenderFont;
	stCmd.x     = pOdr->x;
	stCmd.y     = pOdr->y;
	stCmd.arg1  = (Uint32)((pOdr->color[0]>>24)&0x000000ff);
	stCmd.arg2  = (Uint32)((pOdr->color[0]>>16)&0x000000ff);
	stCmd.arg3  = (Uint32)((pOdr->color[0]>>8)&0x000000ff);
	stCmd.arg4  = (Uint32)((pOdr->color[0]>>0)&0x000000ff);
	//stCmd.pString = (void*)pOdr->pString;

	addCommand( &stCmd );

}



INLINE void gxRender::addCommand( StCmdMake *pCmd )
{

	switch( pCmd->sType ){

	//テクスチャ

	case eCmdBindAlbedoTexture:
	case eCmdBindNormalTexture:
	case eCmdBindPalletTexture:
	case eCmdBindCaptureTexture:
		m_pCommand[m_sCommandNum].eCommand = pCmd->sType;
		m_pCommand[m_sCommandNum].arg[0]   = pCmd->arg1;
		m_sCommandNum ++;
		break;

	case eCmdBindNoneTexture:
		m_pCommand[m_sCommandNum].eCommand = pCmd->sType;
		m_pCommand[m_sCommandNum].arg[0]   = NONE_TEXTURE;
		m_sCommandNum ++;
		break;

	//アトリビュート

	case eCmdChgAttributeAlphaAdd:
	case eCmdChgAttributeAlphaSub:
	case eCmdChgAttributeAlphaCrs:
	case eCmdChgAttributeAlphaRvs:
	case eCmdChgAttributeAlphaNml:
	case eCmdChgAttributeAlphaXor:
	case eCmdChgAttributeAlphaScr:
		m_pCommand[m_sCommandNum].eCommand = pCmd->sType;
		m_sCommandNum ++;
		break;

	//図形

	case eCmdRenderPoint:
		if( m_sLastOdrType != pCmd->sType )
		{
			m_pCommand[m_sCommandNum].eCommand = pCmd->sType;
			m_pCommand[m_sCommandNum].arg[0]   = pCmd->sStartVertex;
			m_pCommand[m_sCommandNum].arg[1]   = (pCmd->sLastVertex-pCmd->sStartVertex);
			m_pCommand[m_sCommandNum].arg[2]   = pCmd->sStartIndex;
			m_pCommand[m_sCommandNum].arg[3]   = (pCmd->sLastIndex-pCmd->sStartIndex);
			m_sCommandNum ++;
		}
		else
		{
			m_pCommand[m_sCommandNum-1].arg[1]   += (pCmd->sLastVertex-pCmd->sStartVertex);
			m_pCommand[m_sCommandNum-1].arg[3]   += (pCmd->sLastIndex-pCmd->sStartIndex);
		}
		break;

	case eCmdRenderLineNormal:
		if( m_sLastOdrType != pCmd->sType )
		{
			m_pCommand[m_sCommandNum].eCommand = pCmd->sType;
			m_pCommand[m_sCommandNum].arg[0]   = pCmd->sStartVertex;
			m_pCommand[m_sCommandNum].arg[1]   = (pCmd->sLastVertex-pCmd->sStartVertex);
			m_pCommand[m_sCommandNum].arg[2]   = pCmd->sStartIndex;
			m_pCommand[m_sCommandNum].arg[3]   = (pCmd->sLastIndex-pCmd->sStartIndex);
			m_pCommand[m_sCommandNum].opt	   = pCmd->arg1/100.0f;
			m_sCommandNum ++;
		}
		else
		{
			m_pCommand[m_sCommandNum-1].arg[1]   += (pCmd->sLastVertex-pCmd->sStartVertex);
			m_pCommand[m_sCommandNum-1].arg[3]   += (pCmd->sLastIndex-pCmd->sStartIndex);
		}
		break;

	case eCmdRenderLineStrip:
		m_pCommand[m_sCommandNum].eCommand = pCmd->sType;
		m_pCommand[m_sCommandNum].arg[0]   = pCmd->sStartVertex;
		m_pCommand[m_sCommandNum].arg[1]   = (pCmd->sLastVertex-pCmd->sStartVertex);
		m_pCommand[m_sCommandNum].arg[2]   = pCmd->sStartIndex;
		m_pCommand[m_sCommandNum].arg[3]   = (pCmd->sLastIndex-pCmd->sStartIndex);
		m_pCommand[m_sCommandNum].opt	   = pCmd->arg1/100.0f;
		m_sCommandNum ++;
		break;

	case eCmdRenderTriangle:
		m_pCommand[m_sCommandNum].eCommand = pCmd->sType;
		m_pCommand[m_sCommandNum].arg[0]   = pCmd->sStartVertex;
		m_pCommand[m_sCommandNum].arg[1]   = (pCmd->sLastVertex-pCmd->sStartVertex);
		m_pCommand[m_sCommandNum].arg[2]   = pCmd->sStartIndex;
		m_pCommand[m_sCommandNum].arg[3]   = (pCmd->sLastIndex-pCmd->sStartIndex);
		m_sCommandNum ++;
		break;

	case eCmdRenderSquare:
		if( m_sLastOdrType != pCmd->sType )
		{
			m_pCommand[m_sCommandNum].eCommand = pCmd->sType;
			m_pCommand[m_sCommandNum].arg[0]   = pCmd->sStartVertex;
			m_pCommand[m_sCommandNum].arg[1]   = (pCmd->sLastVertex-pCmd->sStartVertex);
			m_pCommand[m_sCommandNum].arg[2]   = pCmd->sStartIndex;
			m_pCommand[m_sCommandNum].arg[3]   = (pCmd->sLastIndex-pCmd->sStartIndex);
			m_sCommandNum ++;
		}
		else
		{
			m_pCommand[m_sCommandNum-1].arg[1]   += (pCmd->sLastVertex-pCmd->sStartVertex);
			m_pCommand[m_sCommandNum-1].arg[3]   += (pCmd->sLastIndex-pCmd->sStartIndex);
		}
		break;

	case eCmdRenderFont:
		m_pCommand[m_sCommandNum].eCommand = pCmd->sType;
		m_pCommand[m_sCommandNum].arg[0]   = pCmd->arg1;
		m_pCommand[m_sCommandNum].arg[1]   = pCmd->arg2;
		m_pCommand[m_sCommandNum].arg[2]   = pCmd->arg3;
		m_pCommand[m_sCommandNum].arg[3]   = pCmd->arg4;
		m_pCommand[m_sCommandNum].x        = pCmd->x;
		m_pCommand[m_sCommandNum].y        = pCmd->y;
		//m_pCommand[m_sCommandNum].pString  = pCmd->pString;
		m_sCommandNum ++;
		break;

	case eCmdChangeRenderTarget:
		break;

	case eCmdProcessingBlur:
		m_pCommand[m_sCommandNum].eCommand = pCmd->sType;
		m_pCommand[m_sCommandNum].arg[0]   = pCmd->arg1;
		m_pCommand[m_sCommandNum].arg[1]   = pCmd->arg2;
		m_sCommandNum ++;
		break;

	case eCmdScissor:
		m_pCommand[m_sCommandNum].eCommand = pCmd->sType;
		m_pCommand[m_sCommandNum].arg[0]   = pCmd->arg1;
		m_pCommand[m_sCommandNum].arg[1]   = pCmd->arg2;
		m_pCommand[m_sCommandNum].arg[2]   = pCmd->arg3;
		m_pCommand[m_sCommandNum].arg[3]   = pCmd->arg4;
		m_sCommandNum ++;
		break;

	case eCmdProcessingBloom:
		m_pCommand[m_sCommandNum].eCommand = pCmd->sType;
		m_pCommand[m_sCommandNum].arg[0] = pCmd->arg1;
		m_pCommand[m_sCommandNum].arg[1] = pCmd->arg2;
		m_sCommandNum ++;
		break;

	case eCmdDevelop:
		m_pCommand[m_sCommandNum].eCommand = pCmd->sType;
		m_pCommand[m_sCommandNum].arg[0]   = pCmd->arg1;
		m_sCommandNum ++;
		break;

	case eCmdCaptureScreen:
		m_pCommand[m_sCommandNum].eCommand = pCmd->sType;
		m_pCommand[m_sCommandNum].arg[0]   = pCmd->arg1;
		m_pCommand[m_sCommandNum].arg[1]   = pCmd->arg2;
		m_pCommand[m_sCommandNum].arg[2]   = pCmd->arg3;
		m_pCommand[m_sCommandNum].arg[3]   = pCmd->arg4;
		m_sCommandNum ++;
		break;

	case eCmdChangeShader:
		m_pCommand[m_sCommandNum].eCommand = pCmd->sType;
		m_pCommand[m_sCommandNum].arg[0]   = pCmd->arg1;
		m_sCommandNum++;
		break;

	case eCmdPostProcessGreyScale:
		m_pCommand[m_sCommandNum].eCommand = pCmd->sType;
		m_pCommand[m_sCommandNum].arg[0] = pCmd->arg1;
		m_pCommand[m_sCommandNum].arg[1] = pCmd->arg2;
		m_pCommand[m_sCommandNum].arg[2] = pCmd->arg3;
		m_pCommand[m_sCommandNum].arg[3] = pCmd->arg4;
		m_sCommandNum++;
		break;
	}

	m_sLastOdrType = pCmd->sType;

}


