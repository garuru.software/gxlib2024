#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h> 
#include <gxLib.h>
#include <gxLib/gx.h>

#include "gxFileManager.h"
#include "util/gxUIManager.h"
#include <string>


SINGLETON_DECLARE_INSTANCE( gxFileManager );

gxFileManager::gxFileManager()
{
	m_pDropFiles   = NULL;
	m_DropFilesNum = 0;
	m_DropFilesCnt = 0;

	m_bDragEnable = gxFalse;
	m_sReqCnt = 0;
}

gxFileManager::~gxFileManager()
{
	SAFE_DELETES( m_pDropFiles );
}

void gxFileManager::Action()
{
    std::lock_guard<std::mutex> lock(m_BarrierFileAccess);

	for( auto itr = m_FileInfo.begin() ; itr != m_FileInfo.end() ; itr ++)
	{
		StFileInfo *p;

		p = &itr->second;

		if( p->m_pNameBuf[0] == 0x00 ) continue;

		if( p->m_AccessMode == ACCESSMODE_READ )
		{
			//読み込み
			if( p->m_Location == ESTORAGE_LOCATION::ROM )
			{
				//ROMに探しに行く
				p->m_pDataBuf = loadFile( (const gxChar*)p->m_pNameBuf , &p->m_uSizeBuf , ESTORAGE_LOCATION::ROM );
			}
			else if( p->m_Location == ESTORAGE_LOCATION::INTERNAL)
			{
				//DVD領域からデータを探し出す
				p->m_pDataBuf = loadFile( (const gxChar*)p->m_pNameBuf , &p->m_uSizeBuf , ESTORAGE_LOCATION::INTERNAL);
			}
			else if (p->m_Location == ESTORAGE_LOCATION::EXTERNAL)
			{
				//HDD領域からデータを探し出す
				p->m_pDataBuf = loadFile((const gxChar*)p->m_pNameBuf, &p->m_uSizeBuf, ESTORAGE_LOCATION::EXTERNAL);
			}
			else if( p->m_Location == ESTORAGE_LOCATION::AUTO )
			{
				//メモリーカード領域から順番にデータを探し出す
				p->m_pDataBuf = loadFile( (const gxChar*)p->m_pNameBuf , &p->m_uSizeBuf , ESTORAGE_LOCATION::INTERNAL);
				if( p->m_pDataBuf == NULL )
				{
					//DVDになかったらROMに探しに行く
					p->m_pDataBuf = loadFile( (const gxChar*)p->m_pNameBuf , &p->m_uSizeBuf , ESTORAGE_LOCATION::ROM );
					if( p->m_pDataBuf == NULL )
					{
						//DVDになかったらROMに探しに行く
						p->m_pDataBuf = loadFile( (const gxChar*)p->m_pNameBuf , &p->m_uSizeBuf , ESTORAGE_LOCATION::EXTERNAL );
					}
				}
			}
		}
		else if( p->m_AccessMode == ACCESSMODE_WRITE )
		{
			//書き込み
			p->m_SaveSuccess = -1;
			if( p->m_Location == ESTORAGE_LOCATION::INTERNAL)
			{
				if (saveFile((const gxChar*)p->m_pNameBuf, p->m_pDataBuf, p->m_uSizeBuf, ESTORAGE_LOCATION::INTERNAL))
				{
					p->m_SaveSuccess = 1;
				}
			}
			else if (p->m_Location == ESTORAGE_LOCATION::EXTERNAL)
			{
				if (saveFile((const gxChar*)p->m_pNameBuf, p->m_pDataBuf, p->m_uSizeBuf, ESTORAGE_LOCATION::EXTERNAL))
				{
					p->m_SaveSuccess = 1;
				}
			}
			else if( p->m_Location == ESTORAGE_LOCATION::AUTO )
			{
				if (saveFile((const gxChar*)p->m_pNameBuf, p->m_pDataBuf, p->m_uSizeBuf, ESTORAGE_LOCATION::INTERNAL))
				{
					p->m_SaveSuccess = 1;
				}
			}
			SAFE_DELETE( p->m_pDataBuf );	//Save時のデータはManagerが退避しているので、ここで削除する
		}

		//GX_DEBUGLOG("  --gxFileManager::Action-END(\"%s\");", p->m_pNameBuf);
		p->m_pNameBuf[0] = 0x00;

        //p->m_bLoadDone = gxTrue;

		if( p->m_CallBack )
		{
			p->m_CallBack( p->id );
		}
	}

}


Sint32 gxFileManager::SaveReq(const gxChar *pFileName, void* pData, size_t uSize, ESTORAGE_LOCATION _Location)
{
	int id = m_sReqCnt;

	m_FileInfo[id].id = id;
	m_FileInfo[id].m_Location = _Location;
	m_FileInfo[id].m_AccessMode = ACCESSMODE_WRITE;
	m_FileInfo[id].m_CallBack = nullptr;
	m_FileInfo[id].m_SaveSuccess = 0;

	if (pData)
	{
		m_FileInfo[id].m_pDataBuf = new Uint8[uSize];	//セーブの時はバッファにためておかないとスレッドで保存するときにメモリの中身はないかもしれない
		gxUtil::MemCpy(m_FileInfo[id].m_pDataBuf, pData, uSize);
	}
	else
	{
		m_FileInfo[id].m_pDataBuf = nullptr;
	}

	m_FileInfo[id].m_uSizeBuf = uSize;

	//ファイル名を最後に指定しないとスレッドが走り出す
#ifdef BUILD_COMPILER_VISUALSTUDIO
	sprintf_s(&m_FileInfo[id].m_pNameBuf[0], 128, "%s", pFileName);	//androidで無効
#else
	sprintf(&m_FileInfo[id].m_pNameBuf[0], "%s", pFileName);
#endif
	m_sReqCnt ++;

	return id;
}


Sint32 gxFileManager::LoadReq( const gxChar *pFileName , ESTORAGE_LOCATION _Location , std::function<void(Sint32 id)>func )
{
	int id = m_sReqCnt;

	m_FileInfo[id].id           = id;
	m_FileInfo[id].m_Location   = _Location;
	m_FileInfo[id].m_AccessMode = ACCESSMODE_READ;
	m_FileInfo[id].m_CallBack   = func;
	m_FileInfo[id].m_SaveSuccess = 0;

	//最後に名前を入れないとスレッドが走り出す

#ifdef BUILD_COMPILER_VISUALSTUDIO
	sprintf_s( &m_FileInfo[id].m_pNameBuf[0] ,128, "%s" , pFileName );	//androidで無効
#else
	sprintf( &m_FileInfo[id].m_pNameBuf[0] ,"%s" , pFileName );
#endif

	m_sReqCnt ++;

	return id;
}

gxBool gxFileManager::IsLoadEnd( Sint32 id )
{
//	id = id%enRequestMax;
	if (m_FileInfo.find(id) == m_FileInfo.end() )
	{
		//見つからなかった場合
		GX_DEBUGLOG("[gxLib::Error]gxLib::LoadFile　IDがみつからない %d;", id);
	}

	if( m_FileInfo[id].m_pNameBuf[0] == 0x00 )
	{
		return gxTrue;
	}

	return gxFalse;
}

gxBool gxFileManager::IsSaveEnd( Sint32 id )
{
//	id = id%enRequestMax;

	if (m_FileInfo.find(id) == m_FileInfo.end())
	{
		//見つからなかった場合
		ASSERT(1);
	}

	if( m_FileInfo[id].m_pNameBuf[0] == 0x00 )
	{
		return gxTrue;
	}

	return gxFalse;
}


Uint8* gxFileManager::GetFileAddr(Sint32 id )
{
//	id = id%enRequestMax;
	if (m_FileInfo.find(id) == m_FileInfo.end())
	{
		//見つからなかった場合
		ASSERT(1);
	}

	if( m_FileInfo[id].m_pNameBuf[0] == 0x00 )
	{
		return m_FileInfo[id].m_pDataBuf;
	}

	return NULL;
}


size_t gxFileManager::GetFileSize(Sint32 id )
{
	if (m_FileInfo.find(id) == m_FileInfo.end())
	{
		//見つからなかった場合
		ASSERT(1);
	}

	return m_FileInfo[id].m_uSizeBuf;
}

void gxFileManager::Clear( Sint32 id )
{
	if (m_FileInfo.find(id) == m_FileInfo.end())
	{
		//見つからなかった場合
		ASSERT(1);
	}

	{
		m_FileInfo.erase(id);
	}
}

Uint8* gxFileManager::loadFile( const gxChar* pFileName , size_t* pLength , ESTORAGE_LOCATION _location )
{
	//デバイスのファイル読み込みを開始

	Uint8 *pData = NULL;
	size_t uSize = 0;
    
	pData = CDeviceManager::GetInstance()->LoadFile(pFileName, &uSize, _location);
    if( pLength )
    {
        *pLength = uSize;
    }

	gxUIManager::GetInstance()->NowLoading();

	return pData;

}


gxBool gxFileManager::saveFile( const gxChar* pFileName  , Uint8 *pData , size_t uSize , ESTORAGE_LOCATION _location  )
{
	//デバイスのファイル書き込みを開始

	gxUIManager::GetInstance()->NowSaving();

    if (CDeviceManager::GetInstance()->SaveFile(pFileName, pData, uSize, _location))
	{
		return gxTrue;
	}

	return gxFalse;
}


void gxFileManager::SetDropFileNum( Sint32 num )
{
	//ドラッグ＆ドロップされたファイル数を記録する

	ClearDropFiles();

	m_DropFilesNum = num;

	m_pDropFiles = new gxChar*[ num ];
}


void gxFileManager::AddDropFile( gxChar* pFileName )
{
	//ドラッグ＆ドロップされたファイル名を追加する

	size_t length = strlen( pFileName )+1;

	m_pDropFiles[m_DropFilesCnt] = new gxChar[length];
#ifdef BUILD_COMPILER_VISUALSTUDIO
	sprintf_s( m_pDropFiles[m_DropFilesCnt], length, "%s", pFileName);
#else
	sprintf( m_pDropFiles[m_DropFilesCnt], "%s", pFileName);
#endif
	m_DropFilesCnt ++;
}


Sint32  gxFileManager::GetDropFileNum()
{
	return m_DropFilesNum;
}


gxChar* gxFileManager::GetDropFileName( Sint32 num )
{
	return m_pDropFiles[num];
}


void gxFileManager::ClearDropFiles()
{
	if( m_pDropFiles )
	{
		for( Sint32 ii=0; ii<m_DropFilesNum; ii++ )
		{
			SAFE_DELETES( m_pDropFiles[ii] );
		}

		SAFE_DELETES( m_pDropFiles );
	}

	m_DropFilesCnt = 0;
	m_DropFilesNum = 0;
}



