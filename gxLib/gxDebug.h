﻿//--------------------------------------------------
//
// gxDebug.h
//
//
//--------------------------------------------------


#ifndef _GXDEBUG_H_
#define _GXDEBUG_H_

class gxDebug
{
public:

	enum {
		enPageTop,
		enPageLog,
		enPagePad,
		enPageTex,
		enPageSound,
		enPageRAM,
		enPageFile,
		enPageHTTP,
		enPageRender,
		enPageMax,
	};

	enum EnErr
	{
		ErrVertexBufferOver,
	};

	typedef struct StInfo
	{
		Sint32 OrderNum;
		Sint32 SubOrderNum;

		Sint32 actionCnt;
		Sint32 drawCnt;
		Sint32  cmd_max;
		Sint32  vtx_max;
		Sint32  idx_max;
		Float32 RenderTime;

	} StInfo;

	gxDebug();
	~gxDebug();
	void Action();
	void Draw();

	void ResetDrawCallCnt()
	{
		m_sDrawCallCount = 0;
	}

	static Sint32 m_DebugSwitch[8];
	static gxBool m_bMasterDebugSwitch;
	static void SetDebugSwitch( gxBool bOn );

	void LogDisp( const gxChar *pString );

	void AddDrawCallCnt()
	{
		m_sDrawCallCount ++;
	}

	void AddAction()
	{
		m_sActionCount ++;
	}

	void AddDrawCount()
	{
		m_sDrawCount ++;
	}

	void UpdateRenderInfo( Sint32 orderNum , Sint32 subOrderNum )
	{
		m_stInfo.OrderNum    = orderNum;
		m_stInfo.SubOrderNum = subOrderNum;
	}

	void UpdateRenderTime( Float32 renderTime )
	{
		m_stInfo.RenderTime = renderTime;
	}

	void UpdateVertexInfo( Sint32 cmdNum , Sint32 vtxNum , Sint32 indexNum )
	{
		m_stInfo.cmd_max = cmdNum;
		m_stInfo.vtx_max = vtxNum;
		m_stInfo.idx_max = indexNum;
	}

	StInfo *GetInfo()
	{
		return &m_stInfo;
	}

	void SetError( EnErr errNo );
	void SetAlreadyUseRAM();

	void SetDebugPage( Sint32 page )
	{
		changePage( page );
	}

	static void DrawDebugPot(Float32 x, Float32 y , Sint32 prio= MAX_PRIORITY_NUM);

	Sint32 GetFPS()
	{
		return m_stInfo.actionCnt;
	}

	Sint32 GetRenderFPS()
	{
		return m_stInfo.drawCnt;
	}

	SINGLETON_DECLARE( gxDebug );

private:

	std::map<EnErr,int> m_Error;

	Sint32 m_sActionCount   = 0;;
	Sint32 m_sDrawCount     = 0;
	Sint32 m_sDrawCallCount = 0;

	Float32 m_fTimeOld;

	StInfo m_stInfo;

	enum {
		enStringMaxSize = 512,
		enLogMax        = 12,
	};

	Sint32 m_sDebugNum;
	Sint32 m_sDebugCnt;

	std::vector<std::string> m_LineString;

	///----------------------------------------------------------
	/// default Menu
	///----------------------------------------------------------

	void drawConsole();
	void drawCircleRader();

	///----------------------------------------------------------
	/// debug controller
	///----------------------------------------------------------

	size_t m_uSystemUseRAM = 0;

	//共通のカーソル
	Sint32 m_Cursor;
	Sint32 m_ConfigSeq;
	Sint32 m_sPage;

	Sint32 m_WinW = WINDOW_W;
	Sint32 m_WinH = WINDOW_H;

	//ベンチ
	Sint32 m_sPolygonNum;
	Sint32 m_sMaxPolygonNum;
	Float32 m_fOldTime;

	gxVector2 m_DragPos;
	gxVector2 m_ScrollOffset;
	gxVector2 m_DragOffset;
	gxBool m_bPageUpdate = gxFalse;

	///----------------------------------------------------------
	// キーコンフィグ
	///----------------------------------------------------------

	Sint32 m_TempAssignData[32]={0};
	gxBool m_bButtonDownWait = gxFalse;
	Uint32 m_uCurrentBit = 0x00;

	gxBool waitButtonDown();

	void drawgxKeyInfo( Sint32 id );
	void buttonConfig();
	void stickSelect();

	void drawDefault();

	void drawPageDetail(Sint32 page);
	void controllerConfig();
	void drawMenu();
	void debugLogDisp();
	void textureView();
	void soundView();
	void memView();
	void fileView();
	void httpView();
	void renderView();

	void changePage( Sint32 page )
	{
		m_Cursor = 0;
		m_ConfigSeq = 0;

		m_sPolygonNum    = 0;
		m_sMaxPolygonNum = 0;

		m_sPage = page;
		m_sPage = ( enPageMax + m_sPage)%enPageMax;

	}

	gxUtil::RoundButton* m_pVPadButton[2] = { nullptr ,nullptr };
	gxUtil::RoundButton* m_pManualButton[2] = { nullptr,nullptr };
	gxUtil::RoundButton* m_pFileButton = nullptr;


	Sint32 m_FontWidth = 32;
};

#endif
