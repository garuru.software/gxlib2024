#ifndef _CRENDER_H_
#define _CRENDER_H_


//頂点情報
typedef struct StCustomVertex
{
	Float32 x,y,z,rhw;
	Float32 r,g,b,a;
	Float32 u,v;
	Float32 u2,v2;			//normal用UVオフセット

	Float32 sx,sy;
	Float32 cx,cy;
	Float32 rot;
	Float32 fx,fy;			//flip
	Float32 r2,g2,b2,a2;	//blend

	//影響を受ける点光源の情報
	Float32 px,py,pz;
	Float32 pr,pg,pb;
	Float32 plength;
    Float32 option[4];

} StCustomVertex;


//レンダリングコマンド情報
class CCommandList
{
public:
	Uint32 eCommand;
	Uint32 arg[4];
	Sint32 x,y;
	void*  pString;
	Float32 opt;
private:

};


enum {
	enCommandMax = MAX_ORDER_NUM*4,

	enVertexMax  = 65535*40,
	enIndexMax   = 65535*10,
	enStringMaxSize   = 512,
};

typedef struct StCmdMake {
	Sint32 sType;
	Sint32 sStartVertex;
	Sint32 sLastVertex;
	Sint32 sStartIndex;
	Sint32 sLastIndex;
	Sint32 arg1,arg2,arg3,arg4;
	Sint32 x,y;
}StCmdMake;

enum {
	//特殊なテクスチャ指定の仕方
	enNoneTexturePage = -1,
	enWiredPage       = -2,
	enBackBuffer      = -3,
	enCapturePage     = -4,
};

enum {
	//gxでサポートされるテクスチャマッピングの種類
	enTexmapAlbedo,
	enTexmapNormal,
	enTexmapPallet,
	enTexmapCapture,
	enMapMax,
};

enum {
	//バーチャルレンダリングコマンド

	eCmdChgAttributeAlphaAdd,	//加算半透明設定にする
	eCmdChgAttributeAlphaSub,	//減算半透明設定にする
	eCmdChgAttributeAlphaCrs,	//加算半透明設定にする
	eCmdChgAttributeAlphaRvs,	//反転半透明設定にする
	eCmdChgAttributeAlphaNml,	//50:50の半透明設定にする
	eCmdChgAttributeAlphaXor,	//XOR演算半透明設定にする
	eCmdChgAttributeAlphaScr,	//スクリーン乗算半透明設定にする
	eCmdRenderSquare,			//４頂点ポリゴンを描画する
	eCmdRenderPoint,			//点
	eCmdRenderLineStrip,		//線
	eCmdRenderLineNormal,		//線
	eCmdRenderTriangle,			//三角
	eCmdRenderFont,				//フォント
	eCmdChangeRenderTarget,		//レンダーターゲットを変更する（未使用）
	eCmdProcessingBlur,			//指定プライオリティ以前の画像をボケ（ガウス）させる
	eCmdProcessingBloom,		//輝度値の高い部分にグレアフィルタを施す
	eCmdScissor,				//画面の一部にシザリング領域を設定する
	eCmdCaptureScreen,			//指定プライオリティ以前に描画した画面をキャプチャする（１枚のみ）
	eCmdPostProcessRasterScroll,
	eCmdPostProcessGreyScale,
	eCmdBindNoneTexture,		//ノンテクスチャ用テクスチャをバインドする
	eCmdBindAlbedoTexture,		//通常のテクスチャ（アルベドマップ）を指定する
	eCmdBindNormalTexture,		//法線マップ用のテクスチャを指定する
	eCmdBindPalletTexture,		//パレット用のテクスチャを指定する
	eCmdBindCaptureTexture,		//キャプチャバッファをアクティブテクスチャスロット４にバインドする
	eCmdChangeShader,			//シェーダーを変更する

	//debug用

	eCmdDevelop,
};


class gxRender
{
public:
	gxRender();
	~gxRender();

	void Action();
	void Draw();

	CCommandList* GetCommandList(Sint32 n)
	{
		return &m_pCommand[n];
	}

	Sint32 GetCommandNum()
	{
		return m_sCommandNum;
	}

	StCustomVertex* GetVertex(int n)
	{
		return &m_pCutomVertex[n];
	}

	Uint32* GetIndexBuffer(int n)
	{
		return &m_pIndexBuffer[0];
	}

//	Uint32* GetIndexBuffer32(int n)
//	{
//		return &m_pIndexBuffer[0];
//	}

	Uint32 GetVertexNum()
	{
		return m_sVBufferNum;
	}

	Uint32 GetIndexNum()
	{
		return m_sIBufferNum;
	}

	void SetClearColor( Uint32 argb )
	{
		m_uBackGroundColor = argb;
	}

	Uint32 GetBgColor()
	{
		return m_uBackGroundColor;
	}

	Sint32 GetBGOrderMax()
	{
		return m_GameStartOrderIndex;
	}
	Sint32 GetGameOrderMax()
	{
		return m_sPPrioOverIndex;
	}

    Sint32 GetRenderFPS()
    {
        return m_RenderFPS;
    }

    void SetRenderFPS( Sint32 num )
    {
        m_RenderFPS = num;
    }

	SINGLETON_DECLARE( gxRender );

private:

	void ZSort(int* zsort, int left, int right);
	void makeCommand(Uint32 index);

	INLINE void spriteNormal(StOrder* pOdr);
	INLINE void setVertex( StCustomVertex *pVtx0 , StOrder* pOdr );
	INLINE void setTextureUV( Sint32 tpg , StCustomVertex *pVtx0 , StOrder* pOdr );
	INLINE Float32 getTetureOffsetX( Sint32 page , Sint32 x );
	INLINE Float32 getTetureOffsetY( Sint32 page , Sint32 y );

	INLINE void addCommand( StCmdMake *pCmd );

	INLINE void mappingTextures( StOrder* pOdr );
	INLINE void attachTexture( Sint32 page , Sint32 mapType = enTexmapAlbedo );
	INLINE void changeShader( gxShaderType shaderIndex = gxShaderReset );

	INLINE void drawSprite( StOrder* pOdr );

	INLINE void drawLine( StOrder* pOdr );
	INLINE void drawFont( StOrder* pOdr );
	INLINE void drawPoint( StOrder* pOdr );
	INLINE void drawTriangle( StOrder* pOdr );
	INLINE void drawLineStrip( StOrder* pOdr );
	INLINE void drawTriangleSprite( StOrder* pOdr );

	INLINE void setAttribute( Uint32 sAtr);

	CCommandList*   m_pCommand;
	Sint32          m_sCommandNum;
	Uint32			m_sLastOdrType;

	StCustomVertex* m_pCutomVertex;
	Sint32          m_sVBufferNum;
	Sint32			m_sStartVertex;
	Sint32			m_sLastVertex;

	Uint32*			m_pIndexBuffer;
	Uint32			m_sIBufferNum;

	Sint32			m_sStartIndex;
	Sint32			m_sLastIndex;
	Sint32			m_sIndexCnt;
	Sint32          m_sOldBankNum[enMapMax];
	Sint32          m_sOldAttribute;

	gxShaderType	m_ShaderProgramIndex    = gxShaderDefault;
	gxShaderType	m_OldShaderProgramIndex = gxShaderDefault;

	StCmdMake stCmd;
	StCmdMake stCmdOld;

	Float32 m_fTexturePageWidth;
	Float32 m_fTexturePageHeight;
	Sint32  m_sPageX;
	Sint32  m_sPPrioOverIndex = -1;
	Sint32 m_GameStartOrderIndex = -1;
	Uint32 m_uBackGroundColor;

    Sint32 m_RenderFPS = RENDER_FPS;
};


#endif
