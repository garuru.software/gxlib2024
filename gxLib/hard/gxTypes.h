// --------------------------------------------------------------------
//
// つじつまあわせ用共通ヘッダ
//
// --------------------------------------------------------------------
#ifndef _GXTYPES_H_
#define _GXTYPES_H_
#include <math.h>

#ifdef PLATFORM_WINDOWS_DESKTOP
	#include <windows.h>
	#include <windowsx.h>

	//C Header
	#include <assert.h>
	#include <string.h>
	#include <math.h>

	// C++  Header
	#include <stdlib.h>
	#include <malloc.h>
	#include <memory.h>
	#include <tchar.h>
	#include <locale.h>
	#include <atomic>
	#include <string>

#elif defined PLATFORM_WINDOWS_STORE
#include <windows.h>
#include <windowsx.h>

//C Header
#include <assert.h>
#include <string.h>
#include <math.h>

// C++  Header
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include <locale.h>
#include <atomic>
#include <string>

#elif defined PLATFORM_ANDROID
	#include <stdio.h>
	#include <stdlib.h>
	#include <stddef.h>
	#include <stdint.h>	//size_t で必要
	#include <atomic>
	#include <string>

#elif defined PLATFORM_IOS
    #include <stdio.h>
    #include <stdlib.h>
    #include <stddef.h>
    #include <stdint.h>    //size_t で必要
	#include <atomic>
	#include <string>
#else
	#include <stdio.h>
	#include <stdlib.h>
	#include <stddef.h>
	#include <stdint.h>	//size_t で必要
	#include <atomic>
	#include <string>

#endif

#define INLINE inline


typedef signed char 		Sint8;
typedef short				Sint16;
typedef int 				Sint32;
typedef unsigned char		Uint8;
typedef unsigned short		Uint16;
typedef unsigned			Uint32;
typedef long long int		Sint64;
typedef unsigned long long 	Uint64;
typedef float				Float;
typedef float				Float32;
typedef double				Float64;
typedef char				gxBool;
typedef char				gxChar;

#define gxTrue  (1)
#define gxFalse (0)

#define TEXPAGE_NONE_TEX   (-1)
#define TEXPAGE_BACKBUFFER (-3)
#define TEXPAGE_CAPTURE    (-4)

#define MAX_MASTERTEX_NUM (MASTERTEXTURE_MAX+3)

//３枚余分を作る、うち１枚はフォント

enum class ESTORAGE_LOCATION {
//	STORAGE_LOCATION_CARD,			//sd        : memcard		 : documents	//外部ストレージ、読み書きOK（取り出し可）
//	STORAGE_LOCATION_DISC,			//obb       : disc			 : library		//外部ストレージ　読み込みのみ（取り出し不可）

	//ext
	EXTERNAL,		//any       : sd			 : documents	//外部ストレージ　読み込みのみ（取り出し不可）
	ROM,		//rom       : obb/assets	 : app	  		//インストールディレクトリ	読み書きOK
	INTERNAL,		//disk      : data/data		 : library 		//内部ストレージ　読み込みのみ（取り出し不可）

	AUTO,
};

enum RASTER_TYPE {
	EnRASTER_NONE,
	EnRASTER_V,
};

#define GX_TOUCH_MAX        ( 3 )			//タッチ検出数

enum gxShaderType {
    //gxでサポートされるシェーダーの種類
    gxShaderReset   = 9999,
    gxShaderDefault = 0,    //通常シェーダー
    gxShaderBloom,
    gxShaderBlur,
    gxShaderRaster,
    gxShaderNormal,            //法線マップシェーダー
    gxShaderPallet,            //パレットシェーダー（パレットと法線を組み合わせないのをどうするか。。。）
    gxShaderFont,            //パレットシェーダー（パレットと法線を組み合わせないのをどうするか。。。）
    gxShaderHighSpeed,
    gxShaderGreyScale,
    gxShaderMax,
};

#define VERSION_NUMBER  (VERSION_MAJOR*100*100 +  VERSION_MINOR*100 + VERSION_RELEASE)

//キャスト
#define s_cast static_cast
#define r_cast reinterpret_cast
#define c_cast const_cast
#define d_cast dynamic_cast

#define PI						(3.141592653589793238462643383279f)											///< π
#define RAD2DEG( r )	((r)*180.0f/PI)
#define DEG2RAD( r )	((r)/180.f*PI)
#define ABS( x )			( ( (x) < 0 ) ? -(x) : (x) )
#define	ARRAY_LENGTH( a )	( sizeof( a ) / sizeof( a[0] ) )
#define REV(v)					toF(1.0f/toF(v))															///< 逆数算出マクロ
#define SAFE_RELEASE(V)			if ( (V) != nullptr ) { (V)->Release(); (V) = nullptr; }							///< COM安全解放マクロ
#define SAFE_DELETE(V)			if ( (V) != nullptr ) { delete (V); (V) = nullptr; }								///< newメモリ安全解放
#define SAFE_DELETES(V)			if ( (V) != nullptr ) { delete [] (V); (V) = nullptr; }							///< new[]メモリ安全解放
#define SWAP(N1,N2)				{ N1 = N2 - N1; N2 -= N1; N1 += N2; }										///< 値交換マクロ
#define CLAMP(x, low, high) ((x) > (high))? (high) : ((x) < (low))? (low) : (x)
#define POW(n) ((n)*(n))

//角度を３６０度以内に正規化する
#define NORMALIZE( n ){	while( (n)<0)  { (n)+=360; }	while( (n)>360){ (n)-=360; }	}
#define LARGER(a,b)  ( (a) < (b) )? (b) : (a);
#define SMALLER(a,b) ( (a) > (b) )? (b) : (a);
#define INIT_ARRAY( a , b , c )	for( Sint32 __ii__=0; __ii__< (b); __ii__ ++ ) { a[__ii__] = (c); }
#define DESTROY_ARRAY( a , b ) \
{\
	for( int nnn=0; nnn<(b); nnn++ )\
	{\
		if( ((a))[nnn] ) delete ((a)[nnn]);\
		((a)[nnn])=nullptr;\
	}\
}\

#define ASSERT(n) assert(n)
#define	SINGLETON_DECLARE( T ) \
public: \
	static void CreateInstance() { s_pInstance = new T(); } \
	static void DeleteInstance() { if( s_pInstance ) delete( s_pInstance ); s_pInstance = nullptr;	} \
	static T* GetInstance()      { if( s_pInstance == nullptr ) CreateInstance(); return s_pInstance; } \
	static bool IsExistInstance()      { return ( s_pInstance )? true : false; } \
private: \
	static T* s_pInstance;

#define	SINGLETON_DECLARE_INSTANCE( T ) T* T::s_pInstance = nullptr;

//---------------------------------------
//特殊な設定
//---------------------------------------
struct gxClock
{
	Uint32 Year = 1970;
	Uint32 Month = 1;
	Uint32 Day = 1;
	Uint32 DOW = 0;    //DayOfWeek（曜日）
	Uint32 Hour = 0;
	Uint32 Min = 0;
	Uint32 Sec = 0;
	Uint32 MSec = 0;    // 1/1000Sec
	Uint32 USec = 0;    // 1/1000MSec

	Uint64 getSec();

	static gxClock Now();
};


typedef struct gxPos
{
	void Set( Sint32 _x , Sint32 _y , Sint32 _z = 0)
	{
		x = _x;
		y = _y;
		z = _z;
	}

	//よく使う座標管理用構造体
	Sint32 x,y,z;//,r;	//(x,y,z)＋回転

} gxPos;


struct gxVector3
{
public:
	float x=0,y=0,z=0;

	gxVector3() { x=y=z=0.0f; };

	gxVector3(Float32 _n)
	{
		x = Float32(_n);
		y = Float32(_n);
		z = Float32(_n);
	}

	gxVector3(float x, float y, float z) {
		(this->x) = x;
		(this->y) = y;
		(this->z) = z;
	};

	gxVector3 operator + (const gxVector3& dvec) const {
		gxVector3 v3;

		v3.x = ((this)->x) + dvec.x;
		v3.y = ((this)->y) + dvec.y;
		v3.z = ((this)->z) + dvec.z;
		return v3;
	};

	gxVector3 operator - (const gxVector3& dvec) const {
		gxVector3 v3;

		v3.x = ((this)->x) - dvec.x;
		v3.y = ((this)->y) - dvec.y;
		v3.z = ((this)->z) - dvec.z;
		return v3;
	};

	gxVector3 operator * (const gxVector3 &dvec) const {
		gxVector3 v3;

		v3.x = ((this)->x) * dvec.x;
		v3.y = ((this)->y) * dvec.y;
		v3.z = ((this)->z) * dvec.z;

		return v3;
	};
	gxVector3 operator * (float fDat) const {
		gxVector3 v3;

		v3.x = ((this)->x) * fDat;
		v3.y = ((this)->y) * fDat;
		v3.z = ((this)->z) * fDat;

		return v3;
	};

	gxVector3 operator / (const gxVector3& dvec) const {
		gxVector3 v3;

		v3.x = ((this)->x) / dvec.x;
		v3.y = ((this)->y) / dvec.y;
		v3.z = ((this)->z) / dvec.z;

		return v3;
	};

	gxVector3 operator / (float fDat) const {
		gxVector3 v3;

		if (fDat == 0.0f) {
			v3 = *this;
			return v3;
		}

		v3.x = ((this)->x) / fDat;
		v3.y = ((this)->y) / fDat;
		v3.z = ((this)->z) / fDat;
		return v3;
	};

	gxVector3 operator += (const gxVector3 dvec) {
		((this)->x) += dvec.x;
		((this)->y) += dvec.y;
		((this)->z) += dvec.z;
		return *this;
	};

	gxVector3 operator += (const Float32 fDat ) {
		((this)->x) += fDat;
		((this)->y) += fDat;
		((this)->z) += fDat;
		return *this;
	};

	gxVector3 operator -= (const gxVector3 dvec) {
		((this)->x) -= dvec.x;
		((this)->y) -= dvec.y;
		((this)->z) -= dvec.z;
		return *this;
	};

	gxVector3 operator *= (float fDat) {
		((this)->x) *= fDat;
		((this)->y) *= fDat;
		((this)->z) *= fDat;
		return *this;
	};

	gxVector3 operator *= (const gxVector3& dvec) {
		((this)->x) *= dvec.x;
		((this)->y) *= dvec.y;
		((this)->z) *= dvec.z;
		return *this;
	};

	gxVector3 operator /= (float fDat) {

		if (fDat == 0.0f) {
			return *this;
		}

		((this)->x) /= fDat;
		((this)->y) /= fDat;
		((this)->z) /= fDat;
		return *this;
	};

	gxVector3 operator /= (const gxVector3& dvec) {
		((this)->x) /= dvec.x;
		((this)->y) /= dvec.y;
		((this)->z) /= dvec.z;
		return *this;
	};

	gxBool operator == (gxVector3 &fDat) {

		if ((this->x) != fDat.x) return gxFalse;;
		if ((this->y) != fDat.y) return gxFalse;;
		if ((this->z) != fDat.z) return gxFalse;;
		return gxTrue;
	};

	gxBool operator != (gxVector3& fDat) {

		if ((this->x) != fDat.x) return gxTrue;;
		if ((this->y) != fDat.y) return gxTrue;;
		if ((this->z) != fDat.z) return gxTrue;;
		return gxFalse;
	};

	Float32 length()
	{
		return (Float32)sqrt ( (x*x)+ (y*y) + (z*z) );
//		return pow ( (x*x)+ (y*y) + (z*z) , 0.5 );
	}

	gxVector3 normalize() {
		Float32 len = length();
		if (len == 0.0f )
		{
			x = y = z = 0.0f;
		}
		else
		{
			x /= len;
			y /= len;
			z /= len;
		}

		return *this;
	}

	gxVector3 crossProduct( gxVector3 &v )
	{
		//外積
		//外積を行うとベクトルa,bに直行するベクトルが作られます。
		//ベクトルの差が 結果がゼロなら平行である、１なら直行している
		//・２つのベクトルに垂直なベクトルを求める。
		//・ポリゴンの向き(法線ベクトル)を求める。
		//・２つのベクトルが所属する平面において左右の位置関係を知る
		//・平面上の三角形と点の内外判定
		//・ポリゴンの面積を計る
		//・平面上の閉領域の面積を計る
		//・平面上の閉領域の向き(時計回りか、反時計回り)
		gxVector3 v2;

		v2.x = (y * v.z) - (z * v.y);
		v2.y = (z * v.x) - (x * v.z);
		v2.z = (x * v.y) - (y * v.x);
		return v2;
	
	}

	Float32 dotProduct(gxVector3 &v)
	{
		//内積から２つのベクトルのなす角が作り出す平行四辺形の面積が分かります
		//・２つのベクトルのなす角度を求める
		//・線上の最近点を求める
		//・点が平面の表、裏どちら側にあるか判定
		//・平面上に点があるか
		return (Float32)( x*v.x + y*v.y + z*v.z);
	}

	static Float32 dotProduct(gxVector3& v1 , gxVector3& v2)
	{
		//内積から２つのベクトルのなす角が作り出す平行四辺形の面積が分かります
		//・２つのベクトルのなす角度を求める
		//・線上の最近点を求める
		//・点が平面の表、裏どちら側にあるか判定
		//・平面上に点があるか
		return (Float32)(v1.x * v2.x + v1.y * v2.y + v1.z * v2.z);
	}


};// VECTOR;

struct gxVector2
{
public:
	float x=0,y=0;

	gxVector2() { x=y=0.0f; };

	gxVector2(Float32 _n)
	{
		x = Float32(_n);
		y = Float32(_n);
	}

	void Set(Float32 _x, Float32 _y)
	{
		x = Float32(_x);
		y = Float32(_y);
	}

	void Set(Float32 _n)
	{
		x = Float32(_n);
		y = Float32(_n);
	}

	const void Clamp(Float32 x1, Float32 y1, Float32 x2, Float32 y2)
	{
		if (x < x1)  x = x1;
		if (x > x2)  x = x2;

		if (y < y1)  y = y1;
		if (y > y2)  y = y2;
	}

	const void Loop(Float32 x1, Float32 y1, Float32 x2, Float32 y2)
	{
		Float32 w = ABS(x2 - x1);
		Float32 h = ABS(y2 - y1);

		while (gxTrue)
		{
			if (x < x1)
			{
				x += w;
				continue;
			}

			if (x > x2)
			{
				x -= w;
				continue;
			}

			if (y < y1)
			{
				y += h;
				continue;
			}

			if (y > y2)
			{
				y -= h;
				continue;
			}
			break;
		}
	}

	void SetPosFromRot( Float32 fRot , Float32 fDist );

	gxVector2(float x, float y) {
		(this->x) = x;
		(this->y) = y;
	};

	gxVector2 operator + (const gxVector2& dvec) const {
		gxVector2 v3;

		v3.x = ((this)->x) + dvec.x;
		v3.y = ((this)->y) + dvec.y;
		return v3;
	};

	gxVector2 operator - (const gxVector2& dvec) const {
		gxVector2 v3;

		v3.x = ((this)->x) - dvec.x;
		v3.y = ((this)->y) - dvec.y;
		return v3;
	};

	gxVector2 operator * (const gxVector2 &dvec) const {
		gxVector2 v3;

		v3.x = ((this)->x) * dvec.x;
		v3.y = ((this)->y) * dvec.y;
		return v3;
	};
	gxVector2 operator * (float fDat) const {
		gxVector2 v3;

		v3.x = ((this)->x) * fDat;
		v3.y = ((this)->y) * fDat;

		return v3;
	};

	gxVector2 operator / (const gxVector2& dvec) const {
		gxVector2 v3;

		v3.x = ((this)->x) / dvec.x;
		v3.y = ((this)->y) / dvec.y;
		return v3;
	};

	gxVector2 operator / (float fDat) const {
		gxVector2 v3;

		if (fDat == 0.0f) {
			v3 = *this;
			return v3;
		}

		v3.x = ((this)->x) / fDat;
		v3.y = ((this)->y) / fDat;
		return v3;
	};

	gxVector2 operator += (const gxVector2 dvec) {
		((this)->x) += dvec.x;
		((this)->y) += dvec.y;
		return *this;
	};

	gxVector2 operator += (const Float32 fDat ) {
		((this)->x) += fDat;
		((this)->y) += fDat;
		return *this;
	};

	gxVector2 operator -= (const gxVector2 dvec) {
		((this)->x) -= dvec.x;
		((this)->y) -= dvec.y;
		return *this;
	};

	gxVector2 operator *= (float fDat) {
		((this)->x) *= fDat;
		((this)->y) *= fDat;
		return *this;
	};

	gxVector2 operator *= (const gxVector2& dvec) {
		((this)->x) *= dvec.x;
		((this)->y) *= dvec.y;
		return *this;
	};

	gxVector2 operator /= (float fDat) {

		if (fDat == 0.0f) {
			return *this;
		}

		((this)->x) /= fDat;
		((this)->y) /= fDat;
		return *this;
	};

	gxVector2 operator /= (const gxVector2& dvec) {
		((this)->x) /= dvec.x;
		((this)->y) /= dvec.y;
		return *this;
	};

	gxBool operator == (gxVector2 &fDat) {

		if ((this->x) != fDat.x) return gxFalse;;
		if ((this->y) != fDat.y) return gxFalse;;
		return gxTrue;
	};

	Float32 length()
	{
		return (Float32)sqrt ( (x*x)+ (y*y) );
//		return pow ( (x*x)+ (y*y) + (z*z) , 0.5 );
	}

	gxVector2 normalize() {
		Float32 len = length();
		if (len == 0.0f )
		{
			x = y = 0.0f;
		}
		else
		{
			x /= len;
			y /= len;
		}

		return *this;
	}

	//gxVector2 crossProduct( gxVector2 &v )
	//{
	//	//外積
	//	//外積を行うとベクトルa,bに直行するベクトルが作られます。
	//	//ベクトルの差が 結果がゼロなら平行である、１なら直行している
	//	//・２つのベクトルに垂直なベクトルを求める。
	//	//・ポリゴンの向き(法線ベクトル)を求める。
	//	//・２つのベクトルが所属する平面において左右の位置関係を知る
	//	//・平面上の三角形と点の内外判定
	//	//・ポリゴンの面積を計る
	//	//・平面上の閉領域の面積を計る
	//	//・平面上の閉領域の向き(時計回りか、反時計回り)
	//	gxVector2 v2;
//
//		v2.x = (y * v.z) - (z * v.y);
//		v2.y = (z * v.x) - (x * v.z);
//		v2.z = (x * v.y) - (y * v.x);
//		return v2;
//	
//	}

	//Float32 dotProduct(gxVector2 &v)
	//{
	//	//内積から２つのベクトルのなす角が作り出す平行四辺形の面積が分かります
	//	//・２つのベクトルのなす角度を求める
	//	//・線上の最近点を求める
	//	//・点が平面の表、裏どちら側にあるか判定
	//	//・平面上に点があるか
	//	return (Float32)( x*v.x + y*v.y + z*v.z);
	//}

	//static Float32 dotProduct(gxVector2& v1 , gxVector2& v2)
	//{
	//	//内積から２つのベクトルのなす角が作り出す平行四辺形の面積が分かります
	//	//・２つのベクトルのなす角度を求める
	//	//・線上の最近点を求める
	//	//・点が平面の表、裏どちら側にあるか判定
	//	//・平面上に点があるか
	//	return (Float32)(v1.x * v2.x + v1.y * v2.y + v1.z * v2.z);
	//}

	const Float32 Angle();
	const Float32 Distance();

};// VECTOR;

struct gxRect
{
	//簡単な当たり判定付き矩形

	Float32 x1;
	Float32 y1;
	Float32 x2;
	Float32 y2;

	gxRect()
	{
		x1 = y1 = x2 = y2 = 0;
	}

	gxRect( Sint32 _x1 , Sint32 _y1 , Sint32 _x2 , Sint32 _y2 )
	{
		x1 = Float32(_x1);
		y1 = Float32(_y1);
		x2 = Float32(_x2);
		y2 = Float32(_y2);
	}

	gxRect(Float32 _x1, Float32 _y1, Float32 _x2, Float32 _y2)
	{
		x1 = _x1;
		y1 = _y1;
		x2 = _x2;
		y2 = _y2;
	}

	void SetXY(Sint32 _x1, Sint32 _y1, Sint32 _x2, Sint32 _y2)
	{
		x1 = _x1 * 1.0f;
		y1 = _y1 * 1.0f;
		x2 = _x2 * 1.0f;
		y2 = _y2 * 1.0f;
	}
		
	void SetWH( Sint32 px, Sint32 py, Sint32 w, Sint32 h )
	{
		x1 = Float32(px);
		y1 = Float32(py);
		x2 = Float32(x1+w);
		y2 = Float32(y1+h);
	}

	void SetWHCenter(Sint32 px, Sint32 py, Sint32 w, Sint32 h)
	{
		x1 = Float32(px)- (w/2);
		y1 = Float32(py)- (h/2);
		x2 = Float32(x1 + w);
		y2 = Float32(y1 + h);
	}

	Float32 Width()
	{
		return x2 - x1;
	}

	Float32 Height()
	{
		return y2 - y1;
	}

	gxBool IsHit( Sint32 x , Sint32 y )
	{
		if( x >= x1 && x < x2 && y >= y1 && y < y2 ) return gxTrue;

		return gxFalse;
	}

	gxBool IsHit(gxRect &rect)
	{
		if (rect.x2 < x1)  return gxFalse;
		if (rect.x1 > x2)  return gxFalse;
		if (rect.y2 < y1)  return gxFalse;
		if (rect.y1 > y2)  return gxFalse;

		return gxTrue;
	}

	const gxVector2 GetCenter()
	{
		gxVector2 ret;

		ret.x = (x1 + x2) / 2;
		ret.y = (y1 + y2) / 2;

		return ret;
	}

	void SetOffset(Sint32 x, Sint32 y)
	{
		x1 += x;
		y1 += y;
		x2 += x;
		y2 += y;
	}

	void AddPos(int x, int y)
	{
		x1 += x;
		x2 += x;
		y1 += y;
		y2 += y;
	}
};


struct gxSprite
{
	//スプライト定義用構造体
	gxSprite(Sint32 _tpg=0, Sint32 _u = 0, Sint32 _v = 0, Sint32 _w = 0, Sint32 _h = 0, Sint32 _cx = 0, Sint32 _cy = 0) :
		page(_tpg),
		u(_u),
		v(_v),
		w(_w),
		h(_h),
		cx(_cx),
		cy(_cy) {}

	Sint32 page=0,u=0,v=0,w=0,h=0,cx=0,cy=0;

};


struct gxTransform
{
	gxVector3 pos, rot, scl = {1.0f,1.0f,1.0f};
};

struct StARGB
{
	StARGB(Uint32 argb)
	{
		setARGB32(argb);
	}

	StARGB()
	{
		setARGB32(0x00000000);
	}

	StARGB(Float32 a , Float32 r , Float32 g , Float32 b )
	{
		setARGB(a,r,g,b);
	}

	static StARGB lerp(Float32 n , StARGB src, StARGB dst)
	{
		Float32 n1=n, n2=1.0f-n1;
		StARGB ret;

		ret.a = (src.a * n1) + (dst.a * n2);
		ret.r = (src.r * n1) + (dst.r * n2);
		ret.g = (src.g * n1) + (dst.g * n2);
		ret.b = (src.b * n1) + (dst.b * n2);

		ret = clamp(ret);
		return ret;
	}

	StARGB lerp(Float32 n, StARGB dst)
	{
		Float32 n1 = n, n2 = 1.0f - n1;
		StARGB ret;

		ret.a = (a * n1) + (dst.a * n2);
		ret.r = (r * n1) + (dst.r * n2);
		ret.g = (g * n1) + (dst.g * n2);
		ret.b = (b * n1) + (dst.b * n2);

		ret = clamp(ret);
		return ret;
	}

	void operator = (Uint32 argb )
	{
		setARGB32(argb);
	}

	StARGB operator * (Float32 n)
	{
		StARGB  tmp;
		tmp.a = a * n;
		tmp.r = r * n;
		tmp.g = g * n;
		tmp.b = b * n;

		clamp(tmp);
		return tmp;
	}

	StARGB operator / (Float32 n)
	{
		StARGB  tmp;
		tmp.a = a / n;
		tmp.r = r / n;
		tmp.g = g / n;
		tmp.b = b / n;

		clamp(tmp);
		return tmp;
	}

	StARGB operator + (StARGB& tmp)
	{
		StARGB ret;
		ret.a = a + tmp.a;
		ret.r = r + tmp.r;
		ret.g = g + tmp.g;
		ret.b = b + tmp.b;

		clamp(ret);
		return ret;
	}
		
	StARGB operator - (StARGB& tmp)
	{
		StARGB ret;
		ret.a = a - tmp.a;
		ret.r = r - tmp.r;
		ret.g = g - tmp.g;
		ret.b = b - tmp.b;

		clamp(ret);
		return ret;
	}

	void operator += (StARGB& tmp)
	{
		StARGB ret;
		a += tmp.a;
		r += tmp.r;
		g += tmp.g;
		b += tmp.b;

		clamp(*this);
	}

	void operator -= (StARGB& tmp)
	{
		StARGB ret;
		a -= tmp.a;
		r -= tmp.r;
		g -= tmp.g;
		b -= tmp.b;

		clamp(*this);
	}

	StARGB operator + (Float32 n)
	{
		StARGB  tmp;
		tmp.a = a + n;
		tmp.r = r + n;
		tmp.g = g + n;
		tmp.b = b + n;

		clamp(tmp);
		return tmp;
	}

	StARGB operator - (Float32 n)
	{
		StARGB  tmp;
		tmp.a = a - n;
		tmp.r = r - n;
		tmp.g = g - n;
		tmp.b = b - n;

		clamp(tmp);
		return tmp;
	}

	void operator /= (Float32 n)
	{
		a = a / n;
		r = r / n;
		g = g / n;
		b = b / n;

		clamp(*this);
	}

	void operator *= (Float32 n)
	{
		a = a * n;
		r = r * n;
		g = g * n;
		b = b * n;
		clamp(*this);
	}

	void operator += (Float32 n)
	{
		a = a + n;
		r = r + n;
		g = g + n;
		b = b + n;
		clamp(*this);
	}
	void operator -= (Float32 n)
	{
		a = a - n;
		r = r - n;
		g = g - n;
		b = b - n;
		clamp(*this);
	}

	Float32 a=0.0f,r=0.0f,g=0.0f,b=0.0f;

	void setAlpha(Uint8 a8)
	{
		a = Sint32(a8) / 255.0f;
		clamp(*this);
	}
		
	void setRGB(Uint8 r8, Uint8 g8, Uint8 b8)
	{
		r = Sint32(r8) / 255.0f;
		g = Sint32(g8) / 255.0f;
		b = Sint32(b8) / 255.0f;
		clamp(*this);
	}

	void setARGB(Float32 a32, Float32 r32, Float32 g32, Float32 b32)
	{
		a = a32;
		r = r32;
		g = g32;
		b = b32;
	}
		
	void setARGB8888(Uint8 a8, Uint8 r8, Uint8 g8, Uint8 b8)
	{
		a = Sint32(a8) / 255.0f;
		r = Sint32(r8) / 255.0f;
		g = Sint32(g8) / 255.0f;
		b = Sint32(b8) / 255.0f;
		clamp(*this);
	}

	void setRGB24(Uint32 rgb)
	{
		Uint8 r8 = (rgb & 0x00ff0000) >> 16;
		Uint8 g8 = (rgb & 0x0000ff00) >> 8;
		Uint8 b8 = (rgb & 0x000000ff) >> 0;

		setARGB(A8(), r8, g8, b8);
		clamp(*this);
	}

	void setARGB32(Uint32 argb)
	{
		Uint8 a8 = (argb & 0xff000000) >> 24;
		Uint8 r8 = (argb & 0x00ff0000) >> 16;
		Uint8 g8 = (argb & 0x0000ff00) >> 8;
		Uint8 b8 = (argb & 0x000000ff) >> 0;

		setARGB8888(a8, r8, g8, b8);
		clamp(*this);
	}
	void setARGB(Float32 fa, Uint32 rgb)
	{
		a = fa;
		setRGB24(rgb);
	}

	void setARGB(Uint8 a8, Uint32 rgb)
	{
		setAlpha(a8);
		setARGB32(rgb);
	}

	Uint32 get32()
	{
		int cola = int(255 * a);
		int colr = int(255 * r);
		int colg = int(255 * g);
		int colb = int(255 * b);

		clamp(*this);

		Uint32 argb = (cola << 24) | (colr << 16) | (colg << 8) | (colb << 0);

		return argb;
	}

	Uint8 A8()
	{
		return Uint8((Sint32)(255.0f * a));
	}
	Uint8 R8()
	{
		return Uint8((Sint32)(255.0f * r));
	}
	Uint8 G8()
	{
		return Uint8((Sint32)(255.0f * g));
	}
	Uint8 B8()
	{
		return Uint8((Sint32)(255.0f * b));
	}

	static StARGB& clamp(StARGB& src)
	{
		if (src.a < 0.0f) src.a = 0.0f;
		if (src.a > 1.0f) src.a = 1.0f;

		if (src.r < 0.0f) src.r = 0.0f;
		if (src.r > 1.0f) src.r = 1.0f;

		if (src.g < 0.0f) src.g = 0.0f;
		if (src.g > 1.0f) src.g = 1.0f;

		if (src.b < 0.0f) src.b = 0.0f;
		if (src.b > 1.0f) src.b = 1.0f;

		return src;
	}
};

//============================================================================
//描画関連
//============================================================================
#define ARGB_DFLT ((Uint32)0xffffffff)
#define ARGB(a,r,g,b)	((Uint32)(((a)<<24)|((r)<<16)|((g)<<8)|(b)))

//Floatで値をセットする
#define SET_ARGB(a,r,g,b) (((int)((a)*255)<<24) | ((int)((r)*255)<<16) | ((int)((g)*255)<<8) | ((int)((b)*255)<<0))
#define SET_ALPHA( alp , rgb ) ( ( (rgb)&0x00ffffff) | ( (int(alp*255)) <<24) )
#define SET_BRIGHTNESS(alp,rgb) ((rgb&0xff000000) | (int((int((rgb&0x00ff0000)>>16)*alp))<<16) | (int((int((rgb&0x0000ff00)>>8)*alp))<<8) | (int((int((rgb&0x000000ff)>>0)*alp))<<0) )
#define GET_ALPHA(argb) ((((argb>>24)&0xff)/255.0f))

//#define SET_ALPHA( alp , rgb ) ( ( (rgb)&0x00ffffff) | (alp<<24) )

enum {
	//ブレンド
	ATR_DFLT = (0x00000000),
	ATR_ALPHA_NML = (0x00000000),
	ATR_ALPHA_ADD = (0x00000001),	//加算
	ATR_ALPHA_SUB = (0x00000002),	//減算
	ATR_ALPHA_CRS = (0x00000004),	//乗算
	ATR_ALPHA_RVS = (0x00000008),	//反転
	ATR_ALPHA_XOR = (0x00000010),	//XOR
	ATR_ALPHA_SCR = (0x00000020),	//スクリーン乗算

	//反転
	ATR_FLIP_X = (0x00001000),
	ATR_FLIP_Y = (0x00002000),

	//文字列専用
	ATR_STR_LEFT   		= (0x00000000),	//左詰め
	ATR_STR_CENTER 		= (0x00010000),	//左右センタリング
	ATR_STR_RIGHT  		= (0x00020000),	//右詰め
	ATR_STR_TOP    		= (0x00040000),	//上詰め
	ATR_STR_MID    		= (0x00080000),	//上下センタリング
	ATR_STR_BOTTOM 		= (0x00100000),	//下詰め
	ATR_STR_NODISP 		= (0x00200000),	//表示しない
	ATR_STR_ADJUSTROT	= (0x00400000),	//回転角度に合わせる

	ATR_STR_OUTLINE		= (0x00800000),	//アウトライン
	ATR_STR_SHADOW		= (0x01000000),	//シャドウ

	ATR_STR_LT = ATR_STR_LEFT   | ATR_STR_TOP,
	ATR_STR_CT = ATR_STR_CENTER | ATR_STR_TOP,
	ATR_STR_RT = ATR_STR_RIGHT  | ATR_STR_TOP,
	ATR_STR_LM = ATR_STR_LEFT   | ATR_STR_MID,
	ATR_STR_CM = ATR_STR_CENTER | ATR_STR_MID,
	ATR_STR_RM = ATR_STR_RIGHT  | ATR_STR_MID,
	ATR_STR_LB = ATR_STR_LEFT   | ATR_STR_BOTTOM,
	ATR_STR_CB = ATR_STR_CENTER | ATR_STR_BOTTOM,
	ATR_STR_RB = ATR_STR_RIGHT  | ATR_STR_BOTTOM,

	//フィルター
	ATR_FILTER_NEAREST	 = (0x01000000),
	ATR_FILTER_LINEAR	 = (0x02000000),

	ATR_APPRESERVED1 = (0x10000000),	//ゲームアプリ拡張1
	ATR_APPRESERVED2 = (0x20000000),	//ゲームアプリ拡張2
	ATR_APPRESERVED3 = (0x40000000),	//ゲームアプリ拡張3
	ATR_APPRESERVED4 = (0x80000000),	//ゲームアプリ拡張4
};


//その他組み合わせ
#define ATR_DEFAULT     ( ATR_DFLT )
#define ARGB_DEFAULT    ( ARGB_DFLT )
#define ATR_ALPHA_PLUS	( ATR_ALPHA_ADD )	//加算半透明処理指定
#define ATR_ALPHA_MINUS	( ATR_ALPHA_SUB )	//減算半透明処理指定
#define ATR_FLIP_XY 	( ATR_FLIP_X|ATR_FLIP_Y)

#define ATR_ALPHA_NML	(0x00000000)
#define ATR_ALPHA_ADD	(0x00000001)		//加算
#define ATR_ALPHA_SUB	(0x00000002)		//減算
#define ATR_ALPHA_CRS   (0x00000004)		//乗算
#define ATR_ALPHA_RVS 	(0x00000008)		//反転
#define ATR_ALPHA_XOR 	(0x00000010)		//XOR
#define ATR_ALPHA_SCR 	(0x00000020)		//スクリーン乗算


enum {
	//キーボードの状態
	enPush    = 0x01,
	enTrig    = 0x02,
	enRepeat  = 0x04,
	enRelease = 0x08,
	enDouble  = 0x10,
	enLongTap = 0x20,


	enStatPush    = enPush,
	enStatTrig    = enTrig,
	enStatRepeat  = enRepeat,
	enStatRelease = enRelease,
	enStatDouble  = enDouble,
	enStatLongTap = enLongTap,
};

enum EJoyBit
{
	JOY_U =(0x00000001),
	JOY_R =(0x00000002),
	JOY_D =(0x00000004),
	JOY_L =(0x00000008),

	BTN_1 =(0x00000010),	//a
	BTN_2 =(0x00000020),	//b
	BTN_3 =(0x00000040),	//c
	BTN_4 =(0x00000080),	//x

	BTN_5 =(0x00000100),	//y
	BTN_6 =(0x00000200),	//z
	BTN_7 =(0x00000400),	//10
	BTN_8 =(0x00000800),

	BTN_9 =(0x00001000),
	BTN_10=(0x00002000),
	BTN_11=(0x00004000),
	BTN_12=(0x00008000),	//15:start


	BTN_13=(0x00010000),
	BTN_14=(0x00020000),
	BTN_15=(0x00040000),
	BTN_16=(0x00080000),	//a1

	BTN_17=(0x00100000),
	BTN_18=(0x00200000),
	BTN_19=(0x00400000),
	BTN_20=(0x00800000),	//a2

	BTN_21=(0x01000000),
	BTN_22=(0x02000000),
	BTN_23=(0x04000000),
	BTN_24=(0x08000000),	//27:マウスL
	BTN_25=(0x10000000),	//マウスR

	BTN_26=(0x20000000),	//マウスM
	BTN_27=(0x40000000),	//以下、未使用

	BTN_28=(0x80000000),	//

	BTN_MAX=32,
};

enum {
	//仮想キー

	//cmn

	BTN_SELECT =(BTN_9),
	BTN_BACK =(BTN_SELECT),	//XBOX360

	BTN_START  =(BTN_10),

	//SS

	BTN_A = (BTN_1),
	BTN_B = (BTN_2),
	BTN_X = (BTN_3),
	BTN_Y = (BTN_4),
	BTN_L = (BTN_5),
	BTN_R = (BTN_6),
	BTN_C = (BTN_13),
	BTN_Z = (BTN_14),

	// PS1

	BTN_CROSS    = BTN_A,
	BTN_CIRCLE   = BTN_B,
	BTN_SQUARE   = BTN_X,
	BTN_TRIANGLE = BTN_Y,
	BTN_L1=(BTN_5),
	BTN_R1=(BTN_6),
	BTN_L2=(BTN_7),
	BTN_R2=(BTN_8),

	//PS2

	BTN_L3=(BTN_11),
	BTN_R3=(BTN_12),


	//PS3

	BTN_PS=(BTN_15),
	BTN_ANDROID_BACKKEY=(BTN_15),	//Android向けバックキー


	//Analogスティック

	BTN_ANALOG1U =(BTN_16),
	BTN_ANALOG1R =(BTN_17),
	BTN_ANALOG1D =(BTN_18),
	BTN_ANALOG1L =(BTN_19),

	BTN_ANALOG2U =(BTN_20),
	BTN_ANALOG2R =(BTN_21),
	BTN_ANALOG2D =(BTN_22),
	BTN_ANALOG2L =(BTN_23),

	//mouse

	MOUSE_L = BTN_24,
	MOUSE_R = BTN_25,
	MOUSE_M = BTN_26,

	BTN_ANALOG3L =(BTN_27),
	BTN_ANALOG3R =(BTN_28),

};


struct StJoyStat
{
	Uint32  psh=0;	// 押しっぱなし
	Uint32  trg = 0;	// 押した瞬間
	Uint32  rep = 0;	// リピート
	Uint32  rls = 0;	// 離した瞬間
	Uint32  dcl = 0;	// ダブルクリック
	Uint32  tap = 0;	// ロングタップ
	Sint32  mx = 0;		// マウス座標Ｘ
	Sint32  my = 0;		// マウス座標Ｙ
	Float32 lx = 0;		// 左アナログＸ
	Float32 ly = 0;		// 左アナログＹ
	Float32 rx = 0;		// 右アナログＸ
	Float32 ry = 0;		// 右アナログＹ
	Float32 lt = 0;		// 左トリガー
	Float32 rt = 0;		// 右トリガー
	Sint32  whl = 0;	// ホイール回転

	//各種センサー値
	gxVector3 gyro;			//ジャイロ
	gxVector3 accel;		//加速度
	gxVector3 orientation;	//方向
	gxVector3 magneField;	//地磁気

};

struct StTouch
{
	Sint32 stat;
	Sint32 x,y;
	Sint32 sx,sy;
	Sint32 ex,ey;

};



namespace gxKey
{
	enum KeyType {
		KEYNONE,
		DEL           ,//= KEYBOARD_DELETE,
		INS           ,//= KEYBOARD_INSERT,
		NUMPAD0       ,//= KEYBOARD_N0,	//テンキー
		NUMPAD1       ,//= KEYBOARD_N1,
		NUMPAD2       ,//= KEYBOARD_N2,
		NUMPAD3       ,//= KEYBOARD_N3,
		NUMPAD4       ,//= KEYBOARD_N4,
		NUMPAD5       ,//= KEYBOARD_N5,
		NUMPAD6       ,//= KEYBOARD_N6,
		NUMPAD7       ,//= KEYBOARD_N7,
		NUMPAD8       ,//= KEYBOARD_N8,
		NUMPAD9       ,//= KEYBOARD_N9,

		ESC           ,//= KEYBOARD_ESCAPE,
		BS            ,//= KEYBOARD_BACKSPACE,
		TAB           ,//= KEYBOARD_TAB,
		RETURN        ,//= KEYBOARD_RETURN,
		SHIFT         ,//= KEYBOARD_SHIFT,
		RSHIFT        ,//= KEYBOARD_RSHIFT,
		CTRL          ,//= KEYBOARD_CTRL,
		RCTRL         ,//= KEYBOARD_RCTRL,
		ALT           ,//= KEYBOARD_ALT,
		RALT          ,//= KEYBOARD_RALT,
		PAGEUP        ,//= KEYBOARD_PAGEUP,
		PAGEDOWN      ,//= KEYBOARD_PAGEDOWN,
		UP            ,//= KEYBOARD_ARROW_UP,
		DOWN          ,//= KEYBOARD_ARROW_DOWN,
		LEFT          ,//= KEYBOARD_ARROW_LEFT,
		RIGHT         ,//= KEYBOARD_ARROW_RIGHT,
		SPACE         ,//= KEYBOARD_SPACE,
//		ENTER         ,//= KEYBOARD_ENTER,
		HOME          ,//= KEYBOARD_HOME,
		END	          ,//= KEYBOARD_END,
		F1            ,//= KEYBOARD_F1,
		F2            ,//= KEYBOARD_F2,
		F3            ,//= KEYBOARD_F3,
		F4            ,//= KEYBOARD_F4,
		F5            ,//= KEYBOARD_F5,
		F6            ,//= KEYBOARD_F6,
		F7            ,//= KEYBOARD_F7,
		F8            ,//= KEYBOARD_F8,
		F9            ,//= KEYBOARD_F9,
		F10           ,//= KEYBOARD_F10,
		F11           ,//= KEYBOARD_F11,
		F12           ,//= KEYBOARD_F12,
		NUM0          ,//= KEYBOARD_0,	//キーボードキー
		NUM1          ,//= KEYBOARD_1,
		NUM2          ,//= KEYBOARD_2,
		NUM3          ,//= KEYBOARD_3,
		NUM4          ,//= KEYBOARD_4,
		NUM5          ,//= KEYBOARD_5,
		NUM6          ,//= KEYBOARD_6,
		NUM7          ,//= KEYBOARD_7,
		NUM8          ,//= KEYBOARD_8,
		NUM9          ,//= KEYBOARD_9,

		NUM_ADD       ,//= KEYBOARD_9,
		NUM_SUB       ,//= KEYBOARD_9,
		NUM_MULTI     ,//= KEYBOARD_9,
		NUM_DIV       ,//= KEYBOARD_9,
		NUM_PERIOD    ,//= KEYBOARD_9,
		A             ,//= KEYBOARD_A,
		B             ,//= KEYBOARD_B,
		C             ,//= KEYBOARD_C,
		D             ,//= KEYBOARD_D,
		E             ,//= KEYBOARD_E,
		F             ,//= KEYBOARD_F,
		G             ,//= KEYBOARD_G,
		H             ,//= KEYBOARD_H,
		I             ,//= KEYBOARD_I,
		J             ,//= KEYBOARD_J,
		K             ,//= KEYBOARD_K,
		L             ,//= KEYBOARD_L,
		M             ,//= KEYBOARD_M,
		N             ,//= KEYBOARD_N,
		O             ,//= KEYBOARD_O,
		P             ,//= KEYBOARD_P,
		Q             ,//= KEYBOARD_Q,
		R             ,//= KEYBOARD_R,
		S             ,//= KEYBOARD_S,
		T             ,//= KEYBOARD_T,
		U             ,//= KEYBOARD_U,
		V             ,//= KEYBOARD_V,
		W             ,//= KEYBOARD_W,
		X             ,//= KEYBOARD_X,
		Y             ,//= KEYBOARD_Y,
		Z             ,//= KEYBOARD_Z,
		MINUS	,//-	220	
		HAT 	,//^	189
		YEN		,//\	222
		ATMARK	,//@	192
		LARGE_L	,//[	219
		SEMIC	,//;	187
		COLON	,//:	186
		LARGE_R	,//]	221
		COMMA	,//,	188
		PERIOD	,//.	190
		SLASH	,///	191
		WIN		,//WIN	91

		SNAPSHOT,
		NUMPAD_MULTIPLY,
		NUMPAD_PLUS,
		NUMPAD_MINUS,
		NUMPAD_PERIOD,
		NUMPAD_DIV,
		PAUSE,
		NUMLOCK,
		SCROLL,
		KEYMAX,

		//別名
		NUMPAD_UP    = NUMPAD8,
		NUMPAD_DOWN  = NUMPAD2,
		NUMPAD_LEFT  = NUMPAD4,
		NUMPAD_RIGHT = NUMPAD6,

		ENTER         = RETURN,
		LSHIFT		  = SHIFT,

	};
}
#endif
