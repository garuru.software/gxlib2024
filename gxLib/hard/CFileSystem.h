#ifndef _CFILESYSTEM_H_
#define _CFILESYSTEM_H_
//ファイルシステム

#include<filesystem>
#include<iostream>
#include <fstream>
using namespace std::chrono;

class CFileSystem
{
public:

	CFileSystem()
	{

	}

	~CFileSystem()
	{

	}

	static std::string GetCurrentDirectory()
	{
		return std::filesystem::current_path().string();
	}

	static std::vector<std::string> ScanDirectory( std::string path )
	{
		std::vector<std::string> list;

		if (!IsExist(path))
		{
			//そんなパスが存在しなければ何もしない
			return list;
		}

		for (auto& file : std::filesystem::recursive_directory_iterator(path) )
		{
			std::filesystem::path filepath = file.path();
			std::string str;
			str = (char*)filepath.u8string().c_str();
			if (std::filesystem::is_directory(filepath))
			{
				str += "/";
			}
			std::replace(str.begin(), str.end(), '\\', '/');
			const char* p2 = str.c_str();
			list.push_back( (char*)p2 );
		}
		return list;
	}

	static bool Copy(std::string src, std::string dst)
	{
		try {
			std::filesystem::copy(src, dst);
		}
		catch (std::filesystem::filesystem_error& err)
		{
			gxLib::DebugLog("[error]%s", err.what());
			return false;
		}
		return true;
	}

	static bool Move(std::string src, std::string dst)
	{
		try {
			std::filesystem::rename(src, dst);
		}
		catch (std::filesystem::filesystem_error& err)
		{
			gxLib::DebugLog("[error]%s", err.what());
			return false;
		}
		return true;
	}

	static bool Remove( std::string path )
	{
		try {
			std::filesystem::remove_all(path);
		}
		catch (std::filesystem::filesystem_error& err)
		{
			gxLib::DebugLog("[error]%s", err.what());
			return false;
		}
		return true;
	}

	static bool RemoveSJIS(std::string path)
	{
		size_t uSizePath = 0;
		std::string sjispath8 = CDeviceManager::UTF8toSJIS((gxChar*)path.c_str(), &uSizePath);
		//gxChar* pFileNameSJIS = new gxChar[uSizePath + 1];
		//gxUtil::MemCpy(pFileNameSJIS, pBuf, uSizePath);
		////pFileNameSJIS[uSizePath] = 0x00;
		////SAFE_DELETES(pBuf);
		////path = pFileNameSJIS + '\0';
		//SAFE_DELETES(pFileNameSJIS);

		try {
			std::filesystem::remove_all(sjispath8);
		}
		catch (std::filesystem::filesystem_error& err)
		{
			gxLib::DebugLog("[error]%s", err.what());
			return false;
		}
		return true;
	}

	static bool CreateDirectory(std::string path )
	{
		if (!std::filesystem::create_directories(path))
		{
			return false;
		}
		return true;
	}

	static bool IsExist(std::string path)
	{
		return std::filesystem::exists(path);
	}

	static bool GetFileSize( std::string path , size_t &size )
	{
		if (!IsExist(path))
		{
			//そんなパスが存在しなければ何もしない
			return false;
		}

		//std::ifstream fin(path, std::ios::in | std::ios::binary);

		//if (fin)
		{
			//fin.seekg(0, std::ifstream::end);
			//size_t eofPos = fin.tellg();
			//
			//fin.clear();
			//
			//fin.seekg(0, std::ifstream::beg);
			//size_t begPos = fin.tellg();
			//
			//size = eofPos - begPos;
			//
			size = std::filesystem::file_size(path);

			//fin.close();

			return true;
		}

		size = 0;
		return false;
	}

	static bool CreateDirectorySJIS(std::string path)
	{
		//UTF8で受けとり、SJISで書き出し(Windows用)

		size_t uSizePath = 0;
		std::string sjispath8 = CDeviceManager::UTF8toSJIS((gxChar*)path.c_str(), &uSizePath);

		if (uSizePath > 0)
		{
			if (!CFileSystem::CreateDirectory(sjispath8))
			{
				return false;
			}
		}


		return true;
	}

	static bool CreateDirectoryUTF8(std::string path)
	{
		//UTF8で受けとり、そのまま書き出し(Windows用)

		//size_t uSizePath = 0;

		//if (uSizePath > 0)
		{
			if (!CFileSystem::CreateDirectory(path))
			{
				return false;
			}
		}


		return true;
	}

	static bool WriteFile(std::string path, std::vector<uint8_t> &data)
	{
		WriteFile(path , &data[0] , data.size() );
		return true;
	}

	static bool WriteFileSJIS(std::string path , uint8_t *pData , size_t size )
	{
		//UTF8で受けとり、SJISで書き出し(Windows用)

		gxUtil::FileNames fpath = path;

		size_t uSizePath = 0;
		std::string sjispath8 = CDeviceManager::UTF8toSJIS((gxChar*)fpath.FilePath.c_str(), &uSizePath);
		//gxChar* pFileNameSJIS = new gxChar[uSizePath + 1];
		//memset(pFileNameSJIS, 0x00, uSizePath + 1);
		//gxUtil::MemCpy(pFileNameSJIS, pBuf, uSizePath);

		if(uSizePath > 0 )
		{
			if (!CFileSystem::CreateDirectory(sjispath8))
			{
				//return false;
			}
			//SAFE_DELETES(pBuf);
		}


		//SAFE_DELETES(pFileNameSJIS);

		sjispath8 = CDeviceManager::UTF8toSJIS((gxChar*)path.c_str(), &uSizePath);
		//pFileNameSJIS = new gxChar[uSizePath + 1];
		//memset(pFileNameSJIS, 0x00, uSizePath + 1);
		//gxUtil::MemCpy(pFileNameSJIS, pBuf, uSizePath);

		std::ofstream fout;

		fout.open(sjispath8, std::ios::out | std::ios::binary | std::ios::trunc);
		if (fout.is_open())
		{
			fout.write((char*)pData, size);
			fout.close();
			return true;
		}

		return false;
		//SAFE_DELETES(pFileNameSJIS);
		//SAFE_DELETES(pBuf);

	}

	static bool WriteFile(std::string path, uint8_t* pData, size_t size)
	{

		gxUtil::FileNames fpath = path;

		std::ofstream fout(path, std::ios::out | std::ios::binary | std::ios::trunc);

		if (!fout.is_open())
        {
            if (!CFileSystem::CreateDirectory(fpath.FilePath))
            {
                //sippai ga kaettekuruga dekiteru
                //return false;
            }
            std::ofstream fout(path, std::ios::out | std::ios::binary | std::ios::trunc);
            if (!fout.is_open())
            {
                return false;
            }
            fout.write((char*)pData, size);
            fout.close();

            return true;
        }


		//fout.open(path, std::ios::out | std::ios::binary | std::ios::trunc);
		fout.write((char*)pData, size);
		fout.close();

		return true;
	}

	static std::vector<uint8_t> ReadFile(std::string path)
	{
		std::vector<uint8_t> buf;
		ReadFile(path, buf);

		return buf;
	}

	static bool ReadFile(std::string path , std::vector<uint8_t>&buf )
	{
		std::ifstream fin(path, std::ios::in | std::ios::binary);

		if (fin)
		{
			//fin.seekg(0, std::ifstream::end);
			//size_t eofPos = fin.tellg();
			//
			//fin.clear();
			//
			//fin.seekg(0, std::ifstream::beg);
			//size_t begPos = fin.tellg();
			//
			//auto size = eofPos - begPos;
			size_t size = std::filesystem::file_size(path);

			//if (size > 0)
			{
				buf.resize(size);
				if (size > 0)
					fin.read((char*)&buf[0], size);
			}

			fin.close();

			return true;
		}

		return false;
	}

	static bool ReadFileSJIS(std::string path, std::vector<uint8_t>& buf)
	{
		//Windows用

		size_t uSizePath = 0;
		std::string sjispath8 = CDeviceManager::UTF8toSJIS((gxChar*)path.c_str(), &uSizePath);
		//gxChar* pFileNameSJIS = new gxChar[uSizePath + 1];
		//gxUtil::MemCpy(pFileNameSJIS, pBuf, uSizePath);
		//pFileNameSJIS[uSizePath] = 0x00;
		//SAFE_DELETES(pBuf);

		std::ifstream fin(sjispath8, std::ios::in | std::ios::binary);

		if (fin)
		{
			size_t size = std::filesystem::file_size(sjispath8);

			//if (size > 0)
			{
				//0bytenoファイルのこともある

				buf.resize(size);
				if (size > 0)
				{
					fin.read((char*)&buf[0], size);
				}
			}

			fin.close();

			//SAFE_DELETES(pFileNameSJIS);

			return true;
		}

		//SAFE_DELETES(pFileNameSJIS);

		return false;
	}

	static time_t CheckTimeStamp(std::string fileNameUTF8)
	{
		//更新日時を確認する

		//tm time_tm = { 0 };

		std::filesystem::file_time_type time_ftt;

		//タイムスタンプを取得する
		//auto fileName_sjis = gxUtil::UTF8toSJIS(fileNameUTF8);
		if (!IsExist(fileNameUTF8)) return 0;

		time_ftt = std::filesystem::last_write_time(fileNameUTF8);

		const system_clock::time_point sys_clk_time_point = time_point_cast<system_clock::duration>(
			time_ftt - std::filesystem::file_time_type::clock::now() + system_clock::now());

		time_t time_tt = system_clock::to_time_t(sys_clk_time_point);

		//localtime_s(&time_tm, &time_tt);

		/*
			, 1900 + time_tm.tm_year
			, time_tm.tm_mon + 1
			, time_tm.tm_mday
			, time_tm.tm_hour
			, time_tm.tm_min
			, time_tm.tm_sec
		*/

		return time_tt;
	}

private:

	/*
	void test()
	{
		auto path = CFileSystem::GetCurrentDirectory();
		auto list = CFileSystem::ScanDirectory(path);

		CFileSystem::CreateDirectory(path + "/test");

		std::vector<uint8_t> data(256);
		for (Sint32 ii = 0; ii < 256; ii++)
		{
			data[ii] = ii;
		}

		CFileSystem::WriteFile(path + "/test/dummy.bin", data);
		if (CFileSystem::IsExist(path + "/test/dummy.bin"))
		{
			size_t size;
			if (CFileSystem::GetFileSize(path + "/test/dummy.bin", size))
			{
				auto data = CFileSystem::ReadFile(path + "/test/dummy.bin");

				CFileSystem::Copy(path + "/test/dummy.bin", path + "/test/dummy.bin_copy1");
				//CFileSystem::Copy(path + "/test/dummy.bin", path + "/test/sub/dummy.bin_copy2");
				CFileSystem::Remove(path + "/test/dummy.bin");
			}
		}
	}
	*/
};
#endif
