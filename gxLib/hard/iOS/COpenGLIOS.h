#ifndef _COpenGLIOS_H_
#define _COpenGLIOS_H_

#include <gxLib/hard/COpenGLES3.h>
#include <gxLib/gxTexManager.h>


class COpenGLIOS : public COpenGLES3
{
public:

	COpenGLIOS();
	~COpenGLIOS();

	void Init();
	void Reset();
	void SwapBuffer();
	void GetFrameBufferImage(gxChar* pLength);

	SINGLETON_DECLARE( COpenGLIOS );

private:

//	EGLDisplay display;
//	EGLSurface surface;
//	EGLNativeWindowType win;
//	EGLContext eglContext;
};

#endif

