//------------------------------------------------------------
//
// machine.cpp
// マシン固有のデバイスへのアクセスを行うものはここに書く
// ハードウェアに依存する部分
//
//------------------------------------------------------------
#include <gxLib.h>
#include <gxLib/gx.h>
#include <gxLib/gxFileManager.h>
#include <gxLib/gxDebug.h>
#include "COpenGLIOS.h"
#include "../COpenAL.h"
#include "appMain.h"
#include "CHttpClient.h"
#include "../CFileSystem.h"
#include "CGamePad.h"

std::string _GetClipBoardString();
void _SetClipBoardString(std::string str);
//特殊
static pthread_t test_thread;

SINGLETON_DECLARE_INSTANCE( CDeviceManager );

Float32 GetFreeStorageSizeMB();
Uint8* loadFile2( const gxChar* pFileNameU8, size_t* pLength );

#define CGraphics COpenGLIOS
CDeviceManager::CDeviceManager()
{
	CHttpClient::CreateInstance();
}


CDeviceManager::~CDeviceManager()
{
	CHttpClient::DeleteInstance();
}


void CDeviceManager::AppInit()
{
	static gxBool bFirst = gxTrue;

	if( bFirst )
	{
		bFirst = gxFalse;
		CGameGirl::GetInstance()->Init();
		CAudio::GetInstance()->Init();
		CGraphics::GetInstance()->Init();
		CGamePad::GetInstance()->Init();
	}
	else
	{
        CGameGirl::GetInstance()->SetResume();
	}

	CGameGirl::GetInstance()->AdjustScreenResolution();

}


void CDeviceManager::GameInit()
{
	//バックで受け取った入力機器の情報をgxLib側へ更新する

	CGamePad::GetInstance()->Action();
}


gxBool CDeviceManager::GamePause()
{
	return ::GamePause();
}

gxBool CDeviceManager::GameUpdate()
{
    return gxTrue;
}


void CDeviceManager::Render()
{
	//描画処理
	
	if( CGameGirl::GetInstance()->IsResume() )
	{
		return;
	}

	CGraphics::GetInstance()->Update();
	CGraphics::GetInstance()->Render();
}


void CDeviceManager::vSync()
{
	//1/60秒の同期待ち
	static Float32 _TimeOld = CGameGirl::GetInstance()->GetTime();

	Float32 _TimeNow;
	do
	{
		_TimeNow = CGameGirl::GetInstance()->GetTime();

	}
	while( _TimeNow < ( _TimeOld + (1.0f/ FRAME_PER_SECOND) ) );

	_TimeOld = _TimeNow;

}


void CDeviceManager::Flip()
{
	//バックバッファに用意した画像を転送する

    CGraphics::GetInstance()->Present();
	CGraphics::GetInstance()->Present();
}

void CDeviceManager::Resume()
{

}


void CDeviceManager::Movie()
{
	
}


void CDeviceManager::Play()
{
	CAudio::GetInstance()->Action();
}


gxBool CDeviceManager::NetWork()
{
	if (!CHttpClient::IsExistInstance()) return gxTrue;

	CHttpClient::GetInstance()->Action();

	return gxTrue;
}


void CDeviceManager::UploadTexture(Sint32 sBank)
{
	CGraphics::GetInstance()->ReadTexture( sBank );
}


void CDeviceManager::LogDisp(char* pString)
{
	printf("%s\n",pString);
}


void CDeviceManager::ToastDisp( gxChar* pString )
{

}





void CDeviceManager::OpenWebClient( gxChar* pString , gxBool bOpenBrowser )
{
    //WebViewを表示するリクエストを発行する
	//iOSでは文字列の空判定を行って何かURLが入っていれば自動的に処理を行う
	
	CiOS::GetInstance()->m_WevViewURL = pString;
	CiOS::GetInstance()->m_bOpenWebBrowser = bOpenBrowser;
}


gxBool CDeviceManager::SetAchievement( Uint32 achieveindex )
{
	gxLib::SetToast("Achievement (%d)" , achieveindex );

	return gxTrue;
}

gxBool CDeviceManager::GetAchievement( Uint32 achieveindex )
{
	return gxTrue;
}

Uint8* CDeviceManager::LoadFile( const gxChar* pFileNameU8 , size_t* pLength , ESTORAGE_LOCATION uLocation )
{
	const char* fname = (const char*)pFileNameU8;
	char		 path[2048];

	Uint8* pData = nullptr;

#define STORAGE_PATH_ROM  "01_rom/"
#define STORAGE_PATH_DISK "/03_disk/"
#define STORAGE_PATH_VSD  "/03_VirtualSD/"

	if( uLocation == ESTORAGE_LOCATION::ROM )
	{
		//APP/ASSETS/
		GetResourcePath(path, sizeof path);
		strcat(path, "01_rom/");
		strcat(path, fname);
		pData = (Uint8*)loadFile2(path, pLength);
	}
	else if( uLocation == ESTORAGE_LOCATION::INTERNAL)
	{
		//loadStorage

		//lib-disc
		GetLibraryPath(path, 1024);
		strcat(path, "/03_disk/");
		strcat(path, fname);
		pData = (Uint8*)loadFile2(path, pLength);

		if( pData == nullptr )
		{
			//res-disc
			GetResourcePath(path, sizeof path);
			strcat(path, "/02_disc/");
			strcat(path, fname);
			pData = (Uint8*)loadFile2(path, pLength);
			if( pData == nullptr )
			{
				//res-rom
				GetResourcePath(path, sizeof path);
				strcat(path, "/01_rom/");
				strcat(path, fname);
				pData = (Uint8*)loadFile2(path, pLength);
			}
		}
	}
	else if( uLocation == ESTORAGE_LOCATION::EXTERNAL)
	{
		//loadFile

		//DOCUMENTS/
		GetDocumentPath(path, 1024);
		strcat(path, "/");
		strcat(path, fname);
		pData = (Uint8*)loadFile2(path, pLength);

	}

	return (Uint8*)pData;
}

gxBool CDeviceManager::SaveFile( const gxChar* pFileNameU8 , Uint8* pWritedBuf , size_t uSize , ESTORAGE_LOCATION uLocation )
{
    const char* fname = (const char*)pFileNameU8;
    char		 path[1024];
    
    if( uLocation == ESTORAGE_LOCATION::ROM )
    {
        return gxFalse;
    }
    else if( uLocation == ESTORAGE_LOCATION::INTERNAL)
    {
        //SaveStorageFile
        GetLibraryPath(path, 1024);
        strcat(path, "/03_disk/");
        strcat(path, fname);
        //_saveFile(path, pWritedBuf, uSize);
    }
    else if( uLocation == ESTORAGE_LOCATION::EXTERNAL)
    {
        //saveFile
        //document
        GetDocumentPath(path, 1024);
        strcat(path, "/");
        strcat(path, fname);
        //_saveFile(path, pWritedBuf, uSize);
    }
    
    //size_t _saveFile(const gxChar* pFileName , Uint8* pWritedBuf , Uint32 uSize)
    //assert(name != 0);
    //assert(buf != 0);
    //assert(size > 0);
    
    if (pWritedBuf == nullptr)
    {
        //ファイル削除
        
        if( !CFileSystem::Remove( path ) )
        {
            return false;
        }
        
        return true;
    }
    
    if (CFileSystem::WriteFile(path, pWritedBuf, uSize))
    {
        //SJISで書き出し成功
        return gxTrue;
    }
    
    return gxFalse;
}


void CDeviceManager::MakeThread( void* (*pFunc)(void*) , void * pArg )
{
	pthread_create( &test_thread, NULL, pFunc, (void *)pArg);

}


void CDeviceManager::Sleep( Uint32 msec )
{
	struct timespec	 req, res;
	req.tv_sec  = 0;
	req.tv_nsec = msec * 1000000;

	nanosleep(&req, &res);
}


gxBool CDeviceManager::PadConfig()
{
	//非対応
	return gxTrue;
}


gxBool CDeviceManager::PadConfig( Sint32 padNo , Uint32 button )
{
	return gxTrue;
}


std::vector<std::string> CDeviceManager::GetDirList( std::string rootDirUTF8 )
{
	//U8をSJISに変換
	//※Zipからの展開だとZipに内包されたファイル名がSJISでわたってくる可能性があるが
	//UTF8で取り回す仕様で固定する

	return CFileSystem::ScanDirectory(rootDirUTF8);
}


Float32 CDeviceManager::GetFreeStorageSize()
{
	return GetFreeStorageSizeMB();	//mmファイル
}


Uint8* loadFile2( const gxChar* pFileNameU8, size_t* pLength )
{
	gxChar fileName0[512];

	Uint32 len = sprintf(fileName0, "%s", pFileNameU8);

	//zip時は / でパスを区切る
	
	for (int ii = 0; ii<len; ii++)
	{
		if (fileName0[ii] == '\\')
		{
			fileName0[ii] = '/';
		}
	}

    std::string name = fileName0;
	
	std::vector<uint8_t> buf;

	if (CFileSystem::ReadFile(name, buf))
	{
		if (buf.size())
		{
			uint8_t* p = new Uint8[buf.size() + 1];
			gxUtil::MemCpy(p, &buf[0], buf.size());
			p[buf.size()] = 0x00;

			if (pLength)
			{
				*pLength = buf.size();
			}
			return p;
		}
	}

	return nullptr;
}

std::string CDeviceManager::GetClipBoardString()
{
	return _GetClipBoardString();
}


void CDeviceManager::SetClipBoardString(std::string str)
{
	_SetClipBoardString(str);
}

extern int s_bStatusBarDisp;
void CDeviceManager::SetStatusBar(gxBool bOn)
{
    s_bStatusBarDisp = bOn ? 1 : 0;
}


//-----------------------------------------
//専用
//-----------------------------------------

