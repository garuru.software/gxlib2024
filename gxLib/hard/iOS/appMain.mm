//ファイルローダー

#import <Foundation/Foundation.h>
#import <MobileCoreServices/UTCoreTypes.h>
#import <UIKit/UIKit.h>


#include <gxLib.h>
#include <gxLib/gx.h>
#import <unistd.h>
#import <sys/stat.h>
#include "appMain.h"
#include "CGamePad.h"

SINGLETON_DECLARE_INSTANCE( CiOS );

void AppInit()
{
    CDeviceManager::GetInstance()->AppInit();
    
}


void AppUpdate()
{
    CGameGirl::GetInstance()->Action();
    
    if (CGameGirl::GetInstance()->IsAppFinish())
    {
        
    }
    
}


void AppFlip()
{
    
    
}


void AppFinish()
{
    CGameGirl::DeleteInstance();
    CAudio::DeleteInstance();
    CDeviceManager::DeleteInstance();
    gxLib::Destroy();
}




bool CreateDirectories(char* pURL )
{
    gxBool bSuccess = gxTrue;
    
    std::vector<char*> separates;
    std::string url = pURL;;
    std::replace(url.begin(), url.end(), '\\', '/' );
    
    char *pBuf = new char[ url.size()+1];
    sprintf( pBuf, "%s", url.c_str());
    
    Sint32 cnt = 0;
    
    for ( Sint32 ii= url.size(); ii>=0; ii-- )
    {
        if ( pBuf[ii] == ':')
        {
            separates.push_back(&pBuf[0]);
            cnt++;
            break;
        }
        else if ( ii == 0 )
        {
            separates.push_back(&pBuf[0]);
            cnt++;
            break;
        }
        else if ( pBuf[ii] == '/')
        {
            pBuf[ii] = 0x00;
            if( cnt > 0 ) separates.push_back(&pBuf[ii + 1]);
            cnt++;
        }
    }
    
    size_t max = separates.size();
    std::string path = "";
    
    for (Sint32 ii = 0; ii < max; ii++)
    {
        path += separates[max - 1 - ii];
        
        if (!mkdir( path.c_str(), S_IRWXU ))
        {
            bSuccess = gxFalse;
        }
        
        path += "/";
    }
    
    delete[] pBuf;
    
    return bSuccess;
}


gxChar* GetDocumentPath(gxChar* dst, unsigned dstSize)
{
    NSArray*      dirs = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString*     dir  = [dirs objectAtIndex:0];
    
    const char* src  = [dir UTF8String];
    unsigned    len  = strlen(src);
    memcpy(dst, src, len);
    dst[len] = 0;
    
    return dst;
}

gxChar* GetLibraryPath(gxChar* dst, unsigned dstSize)
{
    NSArray*      dirs = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString*     dir  = [dirs objectAtIndex:0];
    
    const char* src  = [dir UTF8String];
    unsigned    len  = strlen(src);
    memcpy(dst, src, len);
    dst[len] = 0;
    
    return dst;
}



gxChar* GetResourcePath(gxChar* dst, unsigned dstSize)
{
    NSString *  res  = [[NSBundle mainBundle] pathForResource:@"PkgInfo" ofType:nil];
    const char* src  = [res UTF8String];
    unsigned    len  = strlen(src);

    if (len > 7)
        len -= 7;   //pkgInfoを削除

    if (len >  dstSize - 1) {
        assert(0);
        len  = dstSize - 1;
    }
    memcpy(dst, src, len);
    dst[len] = 0;
    
    return dst;
}


size_t getFileSize(const char* path)
{
    FILE *p = NULL;
    p = fopen( path, "rb" );
    if ( p ) {
        size_t size;
        fseek( p, 0, SEEK_END );
        size = ftell( p );
        fseek( p, 0, SEEK_SET );
        fclose( p );
        return size;
    }
    return 0;
}


Float32 GetFreeStorageSizeMB()
{
	NSArray *paths = NSSearchPathForDirectoriesInDomains( NSLibraryDirectory, NSUserDomainMask, YES );
	NSDictionary *dict = [[NSFileManager defaultManager] attributesOfFileSystemForPath:[paths lastObject] error : nil];

	if ( dict )
	{
		Float32 free = [[dict objectForKey : NSFileSystemFreeSize] floatValue];
		return free;
	}

	return 0.0f;
}


gxBool IsRootAccount()
{
/*
    FILE *fh = fopen("/bin/bash", "r");

    gxBool bBash = false;

    if (fh)
    {
        bBash = gxTrue;
    }

    fclose(f);
    return bBash;
*/
    return gxTrue;
    
}

const char* GetCountryCode()
{
    NSString *code = [[NSLocale currentLocale] objectForKey:NSLocaleCountryCode];
	return [code UTF8String];
}

const gxChar* GetDeviceName()
{
/*
 struct utsname info;
    uname(&info);

    NSString *name = [NSString stringWithCString:info.machine encoding:NSUTF8StringEncoding];
	return [name UTF8String];
*/
    return "DefaultiPhone";
}

std::string _GetClipBoardString()
{
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    NSString *string = pasteboard.string;
    //Nslog (@"clipboard text :%@",string);

    CDeviceManager::GetInstance()->m_ClipBoardString = [string UTF8String];
    return CDeviceManager::GetInstance()->m_ClipBoardString;
}

void _SetClipBoardString(std::string str)
{
    NSString *nsstrDst = [NSString stringWithUTF8String:str.c_str()];

    UIPasteboard *pb = [UIPasteboard generalPasteboard];
    [pb setValue:nsstrDst forPasteboardType:@"public.utf8-plain-text"];
}

