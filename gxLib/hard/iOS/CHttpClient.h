﻿//インポート
#ifndef _HTTPCLIENT_H_
#define _HTTPCLIENT_H_

struct ConnectUnit;
class CHttpClient
{
public:
	void Action();

	SINGLETON_DECLARE(CHttpClient);

private:
	void SeqMain();

	std::vector<ConnectUnit*> m_ConnectList;
};


#endif
