gxChar* GetDocumentPath(gxChar* dst, unsigned dstSize);
gxChar* GetLibraryPath(gxChar* dst, unsigned dstSize);
gxChar* GetResourcePath(gxChar* dst, unsigned dstSize);
void* _loadFile(const char* name, size_t* pLength);
size_t _saveFile(const char* name, const void* buf, size_t size);




