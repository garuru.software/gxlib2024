#import <Foundation/Foundation.h>
#include <gxLib.h>
#include <gxLib/gx.h>
#include <gxLib/gxNetworkManager.h>
#import "CHttpClient.h"
#import <UIKit/UIKit.h>

#define TIMEOUT_INTERVAL 10

@interface HttpRequestDelegate : NSObject<NSURLConnectionDelegate> {
	NSURLConnection *connection;
	NSMutableData *receivedData;
	NSURLResponse *responseData;
	BOOL synchronousOperationComplete;
}

@property (nonatomic, retain) NSURLConnection *connection;
@property (nonatomic, retain) NSMutableData *receivedData;
@property (nonatomic, retain) NSURLResponse *responseData;
@property BOOL synchronousOperationComplete;
@property (retain) id delegate;
@property BOOL error;
@property BOOL connectNetwork;

- (BOOL)post: (NSString *)urlString postString:(NSString *)postString;
- (BOOL)get: (NSString *)urlString;

@end


@interface NSURLRequest (IgnoreSSL)

+ (BOOL)allowsAnyHTTPSCertificateForHost:(NSString*)host;

@end







@implementation HttpRequestDelegate

@synthesize connection;
@synthesize receivedData;
@synthesize responseData;
@synthesize synchronousOperationComplete;

// ============
// 初期化処理
// ============
- init {
	if ((self = [super init])) {}
	self.connection = nil;
	return self;
}

// ============
// POSTメソッドでHTTPリクエストを送信します
// urString   : 送信するURL(http://もしくはhttps://から開始)
// postString : 送信するパラメータ(パラメータ名=値&パラメータ名=値&...)
// (備考)
// - GETメソッドでリクエストを送信する場合は[request setHTTPMethod:@"POST"]を[request setHTTPMethod:@"GET"]に変えるとできます
// - HTTPヘッダ情報は[request setValue:@"値" forHTTPHeaderField:@"HTTPヘッダフィールド名"]で指定できます
// ============
- (BOOL)post:(NSString *)urlString postString:(NSString *)postString {
	NSLog(@"POST: %@?%@", urlString, postString);
	self.receivedData = [[NSMutableData alloc] init];
	NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
	[request setURL:[NSURL URLWithString:urlString]];
	[request setHTTPMethod:@"POST"];
	[request setTimeoutInterval:TIMEOUT_INTERVAL];
	[request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
	[request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
	[request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
	if (self.connection == nil) {
		self.connection = [[NSURLConnection alloc]
						   initWithRequest:request
						   delegate:self
						   startImmediately:YES];
	} else {
		self.connection = [self.connection initWithRequest:request delegate:self startImmediately:YES];
	}
	self.synchronousOperationComplete = NO;
	if (!connection) {
		NSLog(@"connection failed :(");
		return false;
	} else {
		NSLog(@"connection succeeded :)");
	}
	return true;
}

- (BOOL)get:(NSString *)urlString {
	NSLog(@"GET: %@", urlString);
	self.receivedData = [[NSMutableData alloc] init];
	NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
	[request setURL:[NSURL URLWithString:urlString]];
	[request setHTTPMethod:@"GET"];
	[request setTimeoutInterval:TIMEOUT_INTERVAL];
	[request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
	[request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
	if (self.connection == nil) {
		self.connection = [[NSURLConnection alloc]
						   initWithRequest:request
						   delegate:self
						   startImmediately:YES];
	} else {
		self.connection = [self.connection initWithRequest:request delegate:self startImmediately:YES];
	}
	self.synchronousOperationComplete = NO;
	if (!connection) {
		NSLog(@"connection failed :(");
		return false;
	} else {
		NSLog(@"connection succeeded :)");
	}
	return true;
}

// ============
// Callbacks
// ============
#pragma mark NSURLConnection delegate method
- (NSURLRequest *)connection:(NSURLConnection *)connection
			 willSendRequest:(NSURLRequest *)request
			redirectResponse:(NSURLResponse *)redirectResponse {
	NSLog(@"Connection received data, retain count");
	return request;
}

// ============
// 【デリゲート】サーバからレスポンスが送られてきた時
// ============
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
//	NSLog(@"Received response: %@", response);
	
	responseData = [response copy];
	[receivedData setLength:0];
}

// ============
// 【デリゲート】サーバからデータが送られてきた時
// ============
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
//	NSLog(@"Received %lu bytes of data", (unsigned long)[data length]);
	[receivedData appendData:data];
//	NSLog(@"Received data is now %lu bytes", (unsigned long)[receivedData length]);
}

// ============
// 【デリゲート】何らかの原因でサーバにつながらなかった時
// ============
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
	NSLog(@"Error receiving response: %@", error);
//	[[NSAlert alertWithError:error] runModal];
	self.synchronousOperationComplete = YES;
	self.error = YES;
}

// ============
// 【デリゲート】データのロードが完了した時
// ============
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	NSLog(@"Succeeded! Received %lu bytes of data", (unsigned long)[receivedData length]);
	NSString *dataStr=[[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
	NSLog(@"Succeeded! Received data : %@", dataStr);
	self.synchronousOperationComplete = YES;
}

@end


@implementation NSURLRequest (IgnoreSSL)

+ (BOOL)allowsAnyHTTPSCertificateForHost:(NSString*)host
{
	return YES;
}
@end

struct ConnectUnit
{
	gxNetworkManager::StHTTP *httpInfo = nullptr;
	HttpRequestDelegate *httpRequest   = nullptr;
	NSRunLoop *runLoop = nullptr;
	gxBool bClose = false;
};

SINGLETON_DECLARE_INSTANCE(CHttpClient);

void CHttpClient::Action()
{
	// コネクションの確立
    if(m_ConnectList.size() > 0)
    {
        NSLog(@"Now Loading..." );
    }

//	for( Sint32 ii=0; ii<m_ConnectList.size(); ii++ )
	for( auto itr=m_ConnectList.begin(); itr !=m_ConnectList.end(); )
	{
		auto* unit = itr[0];
		unit->httpInfo->uReadFileSize = unit->httpRequest.receivedData.length;

        if( [unit->runLoop runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]])
		{
			if( unit->httpRequest.synchronousOperationComplete == NO )
			{
				[NSThread sleepForTimeInterval:1];
				++itr;
				continue;
			}
		}

		// HTTPレスポンスデータ受信
		//NSString *receivedDataStr = [[NSString alloc] initWithData:httpRequest.receivedData encoding:NSUTF8StringEncoding];
		//NSLog(@"receivedData=%@", receivedDataStr);
		if(unit->httpRequest.error)
		{
			NSLog(@"Error -------------------%s", unit->httpInfo->m_URL );
			unit->httpInfo->SetSeq( gxNetworkManager::ESeq::eHTTPSeqLoadEnd );
			unit->httpInfo->SetError(gxNetworkManager::EError::Failed);
			unit->httpInfo = NULL;
			//[httpRequest release];
			SAFE_DELETE( unit );
			itr = m_ConnectList.erase( itr );
			continue;
		}

		NSLog(@"Done! -------------------%s", unit->httpInfo->m_URL );
		unit->httpInfo->MoveData( (uint8_t*)unit->httpRequest.receivedData.bytes , (unsigned long)unit->httpRequest.receivedData.length );
		unit->httpInfo->uReadFileSize = unit->httpRequest.receivedData.length;
		unit->httpInfo->SetSeq( gxNetworkManager::ESeq::eHTTPSeqLoadEnd );
		unit->httpInfo->SetError(gxNetworkManager::EError::Success);
		unit->httpInfo = NULL;
		//[unit->httpRequest release];
		unit->bClose = gxTrue;
		SAFE_DELETE( unit );
		itr = m_ConnectList.erase( itr );
	}

	gxNetworkManager::StHTTP* httpInfo = gxNetworkManager::GetInstance()->GetNextReq();

	if( httpInfo )
	{
		ConnectUnit* unit = new ConnectUnit();

		unit->httpRequest = [[HttpRequestDelegate alloc] init];
//		NSString *url = @"http://www.yahoo.co.jp";
		NSString *url = [NSString stringWithCString: httpInfo->m_URL encoding:NSUTF8StringEncoding];

		if( httpInfo->PostData )
		{
			NSString *postString = [NSString stringWithCString: httpInfo->PostData encoding:NSUTF8StringEncoding];
			//NSString *postString = @"paramKey1=paramValue1&paramKey2=paramValue2";  // TODO ここにパラメータを記述
			if( [unit->httpRequest post:url postString:postString] == false )
			{
				unit->httpRequest.connectNetwork = false;
				httpInfo->SetSeq( gxNetworkManager::ESeq::eHTTPSeqClose );
				httpInfo->SetError(gxNetworkManager::EError::Success);
				httpInfo = NULL;
				return;
			}
		}
		else
		{
			if( [unit->httpRequest get:url] == false )
			{
				unit->httpRequest.connectNetwork =false;
				httpInfo->SetSeq( gxNetworkManager::ESeq::eHTTPSeqClose );
				httpInfo->SetError(gxNetworkManager::EError::Success);
				httpInfo = NULL;
				return;
			}
		}

		unit->httpInfo = httpInfo;
		unit->httpRequest.connectNetwork =true;
		unit->httpRequest.error = NO;

		unit->runLoop = [NSRunLoop currentRunLoop];

		m_ConnectList.push_back( unit );
	}

}


void CHttpClient::SeqMain()
{
//	static int cnt = 0;
//	if( cnt > 0 ) return;
//	cnt ++;

//	HttpRequestDelegate *httpRequest = [[HttpRequestDelegate alloc] init];

	// ※パラメータの中に記号(/や&など)がある場合URLパーセントエンコーディングを行う必要があります
	// NSString *encodedValue = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL, (CFStringRef)paramValue, NULL, (CFStringRef)@"!*'();:@&=+$,/?%#[]", kCFStringEncodingUTF8 ));
	
	// HTTPリクエストの送信
//	NSString *url = @"http://www.garuru.co.jp/files/sdf.zip";					// TODO ここにURLを記述
//	NSString *url = @"https://game.sh-hktm.jp/updata_CDN3_u5zbgabi/iOSArchives/updata0001_k762y4wn/D00012.seven";					// TODO ここにURLを記述

//	NSString *postString = @"paramKey1=paramValue1&paramKey2=paramValue2";  // TODO ここにパラメータを記述
//	[httpRequest post:url postString:postString];

//	[httpRequest get:url];

	// HTTPレスポンス待機
//	NSRunLoop *runLoop = [NSRunLoop currentRunLoop];
//	while (httpRequest.synchronousOperationComplete == NO && [runLoop runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]]) {
//	[NSThread sleepForTimeInterval:1];
//	}

//	// HTTPレスポンスデータ受信
//	NSString *receivedData = [[NSString alloc] initWithData:httpRequest.receivedData encoding:NSUTF8StringEncoding];
//	NSLog(@"receivedData=%@", receivedData);
/*
    Float32 ver = 0;
    if([UIDevice currentDevice]){
        ver = [UIDevice currentDevice].systemVersion.floatValue;
    }

        if(ver < 10.0f){
            UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
            [pasteboard setValue:[NSString stringWithFormat:@"URL=test"] forPasteboardType:@"public.text"];
        }else{
            UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
            pasteboard.string = [NSString stringWithFormat:@"URL=test"];
        }
*/
    //NSUUID *vendorUUID = [UIDevice currentDevice].identifierForVendor;
    //NSString* identifier = vendorUUID.UUIDString;
    //return [identifier UTF8String];

    //dispatch_async(dispatch_get_main_queue(), ^{
    //    NSURL * url = [NSURL URLWithString:@"http://www.yahoo.co.jp"];
    //    [[UIApplication sharedApplication] openURL:url];
    //});

	//return 0;
}

//iPhone実機でローカル環境に接続するとhttpsでないためエラーが出る
//https://hacknote.jp/archives/32606/
