#ifndef _CGAMEPAD_H_
#define _CGAMEPAD_H_

class CGamePad
{
public:
    struct StMouse
	{
		gxBool m_bPush;
		Sint32 mx,my;

		void Reset()
		{
			m_bPush = gxFalse;
			mx = my = 0;
		}

	};

	enum {
		enCursorMax = 8,
		enKeyMax    = 256,
		enAnalogMax = 32,
	};

	CGamePad()
	{
		for( Sint32 ii=0; ii<enCursorMax; ii++ )
		{
			m_Cursor[ii].Reset();
		}

		for( Sint32 ii=0; ii<enKeyMax; ii++ )
		{
			m_KeyBoardPush[ii] = 0x00;
			m_PadPush[ii] = 0x00;
		}
	}

	~CGamePad()
	{
	}

    void Init();

    void Action();

	void InputKeyCheck( Sint32 push , Sint32 keyCode );
	void InputCursorCheck( Sint32 id , gxBool bPush , Sint32 px , Sint32 py );

	void SetAnalog( Float32 *pAnalogInfo )
	{
		m_fAnalog[0] = pAnalogInfo[0];	//ax
		m_fAnalog[1] = pAnalogInfo[1];	//ay
		m_fAnalog[2] = pAnalogInfo[2];	//az


		m_fAnalog[3] = pAnalogInfo[3];	//rx
		m_fAnalog[4] = pAnalogInfo[4];	//ry
		m_fAnalog[5] = pAnalogInfo[5];	//rz

		m_fAnalog[6] = pAnalogInfo[6];
		m_fAnalog[7] = pAnalogInfo[7];
		m_fAnalog[8] = pAnalogInfo[8];

		m_fAnalog[9]  = pAnalogInfo[9];
		m_fAnalog[10] = pAnalogInfo[10];
		m_fAnalog[11] = pAnalogInfo[11];
	}

	void SetCensorData(Float32 *pData );

	SINGLETON_DECLARE( CGamePad );

private:

	enum {
		enID_NoneAccel ,			//
		enID_ChangeFullScreenMode ,//= 11001,	//フルスクリーン切り替え
		enID_AppExit              ,//= 11002,	//アプリ終了
		enID_GamePause			  ,//= 11003,	//ゲームのポーズ
		enID_GameStep			  ,//= 11004,	//ゲームのステップ
		enID_PadEnableSwitch	  ,//= 11005,	//コントローラー設定
		enID_DebugMode			  ,//= 11006,	//デバッグモードのON/OFF
		enID_Reset				  ,//= 11007,	//リセット
		enID_ScreenShot           ,//= 11008,	//スクリーンショット
		enID_FullSpeed			  ,//= 11009,	//フルスピード
		enID_SoundSwitch		  ,//= 11010,	//サウンドOn /Off
		enID_SamplingFilter		  ,//= 11011,	//サンプリングフィルター

		enID_MasterVolumeAdd	  ,//= 11012,	//Volume+
		enID_MasterVolumeSub	  ,//= 11013,	//Volume+
		enID_SwitchVirtualPad	  ,//= 11014,	//VirtualPad On/ Off
		enID_Switch3DView		  ,//= 11015,	//3DView
		enAccelMax			  	  ,//= 11,
	};

	Sint32 m_Accellarator[enAccelMax]={0};
	StMouse m_Cursor[enCursorMax];

	Sint32 ConvertKeyNumber( Sint32 id ,Sint32 *pKey , Sint32 *pPad );
	void adjustDefaultInput();

	void CheckAccellarator( Sint32 id , gxBool bPush );

	Uint8 m_KeyBoardPush[enKeyMax];
	Uint32 m_PadPush[enKeyMax];
	Float32 m_fAnalog[enAnalogMax]={0};
	Float32 m_fCensors[3*4]={0};

};

#endif

