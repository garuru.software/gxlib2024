#include <gxLib.h>
#include <gxLib/gx.h>
/*
#include <gxLib/gxOrderManager.h>
#include <gxLib/gxRender.h>
#include <gxLib/gxTexManager.h>
#include <gxLib/gxDebug.h>
*/
#include "COpenGLIOS.h"

SINGLETON_DECLARE_INSTANCE( COpenGLIOS );

const GLchar  *vtxShader1[] = {
//	"#version 330\n",
	"attribute mediump vec4  a_position;\n",
	"attribute mediump vec4  a_color;\n",
	"attribute mediump vec2  a_texCoord;\n",
	"attribute mediump vec2  a_nmlCoord;\n",
	"attribute mediump vec2  a_scale;\n",
	"attribute mediump vec2  a_offset;\n",
	"attribute mediump float a_rotation;\n",
	"attribute mediump vec2  a_flip;\n",
	"attribute mediump vec4  a_blend;\n",
	"attribute mediump vec3  a_pointlight_pos;\n",
	"attribute mediump vec3  a_pointlight_rgb;\n",
	"attribute mediump float a_pointlight_length;\n",
    "attribute mediump vec4 a_options;\n",
	"uniform   mediump vec2  u_screen;\n",
	"varying   mediump vec4  v_vtxColor;\n",
	"varying   mediump vec2  v_texCoord;\n",
	"varying   mediump vec2  v_nmlCoord;\n",
	"varying   mediump vec4  v_position;\n",
	"varying   mediump vec4  v_blend;\n",
	"varying   mediump vec3  v_pointlight_pos;\n",
	"varying   mediump vec3  v_pointlight_rgb;\n",
	"varying   mediump float v_pointlight_length;\n",
    "varying   mediump vec4  v_options;\n",
	"void main(void)\n",
	"{\n",
	"v_vtxColor    = a_color;\n",
	"v_texCoord    = a_texCoord;\n",
"v_nmlCoord    = a_nmlCoord;\n",
	"v_blend       = a_blend;\n",
"v_pointlight_pos  = a_pointlight_pos;\n",
"v_pointlight_rgb  = a_pointlight_rgb;\n",
"v_pointlight_length  = a_pointlight_length;\n",
    "v_options  = a_options;\n",
	"vec2 pos = vec2(0.0 , 0.0);\n",
	"pos.x  = a_position.x * a_scale.x * u_screen.x;\n",	//ここで解像度をかけておかないと回転の時に比率が合わなくなる
	"pos.y  = a_position.y * a_scale.y * u_screen.y;\n",
	"vec2 pos2;\n",
	"pos2.x  = (pos.x * cos( a_rotation )      - pos.y * sin( a_rotation )*-1.0);\n",
	"pos2.y  = (pos.x * sin( a_rotation )*-1.0 + pos.y * cos( a_rotation ) );\n",
	"pos2.x  = pos2.x * a_flip.x;\n",
	"pos2.y  = pos2.y * a_flip.y;\n",
	"vec2 pos3;\n",
	"pos3.x  = pos2.x / u_screen.x + a_offset.x;\n",		//かけておいた解像度を元に戻す
	"pos3.y  = pos2.y / u_screen.y + a_offset.y;\n",
"v_position    = vec4( pos3.x , pos3.y , 0.0 , 0.0 );\n",
	"gl_Position = vec4( pos3.x , pos3.y , 0.0 , 1.0 );\n",
	"}\n",
};

const GLchar  *vtxShaderHighSpeed[] = {
//	"#version 330\n",
	"attribute mediump vec4  a_position;\n",
	"attribute mediump vec4  a_color;\n",
	"attribute mediump vec2  a_texCoord;\n",
	"attribute mediump vec2  a_scale;\n",
	"attribute mediump vec2  a_offset;\n",
	"attribute mediump float a_rotation;\n",
	"attribute mediump vec2  a_flip;\n",
	"attribute mediump vec4  a_blend;\n",
	"uniform   mediump vec2  u_screen;\n",
	"varying   mediump vec4  v_vtxColor;\n",
	"varying   mediump vec2  v_texCoord;\n",
	"varying   mediump vec2  v_nmlCoord;\n",
	"varying   mediump vec4  v_position;\n",
	"varying   mediump vec4  v_blend;\n",
	"void main(void)\n",
	"{\n",
	"v_vtxColor    = a_color;\n",
	"v_texCoord    = a_texCoord;\n",
	"v_blend       = a_blend;\n",
	"vec2 pos = vec2(0.0 , 0.0);\n",
	"pos.x  = a_position.x * a_scale.x * u_screen.x;\n",	//ここで解像度をかけておかないと回転の時に比率が合わなくなる
	"pos.y  = a_position.y * a_scale.y * u_screen.y;\n",
	"vec2 pos2;\n",
	"float vcos = cos( a_rotation );\n",
	"float vsin = sin( a_rotation );\n",
	"pos2.x  = (pos.x * vcos      - pos.y * vsin*-1.0);\n",
	"pos2.y  = (pos.x * vsin*-1.0 + pos.y * vcos );\n",
	"vec2 pos3;\n",
	"pos3.x  = pos2.x / u_screen.x + a_offset.x;\n",		//かけておいた解像度を元に戻す
	"pos3.y  = pos2.y / u_screen.y + a_offset.y;\n",
	"v_position    = vec4( pos3.x , pos3.y , 0.0 , 0.0 );\n",
	"gl_Position = vec4( pos3.x , pos3.y , 0.0 , 1.0 );\n",
	"}\n",
};

//標準シェーダー
const GLchar  *pxlShader1[] = {
	"varying mediump vec2 v_texCoord;\n",
	"varying mediump vec4 v_vtxColor;\n",
	"varying mediump vec4 v_blend;\n",
	"uniform sampler2D u_textureID1;\n",
	"void main(void)\n",
	"{\n",
		"mediump vec4 color1 = texture2D( u_textureID1 , v_texCoord.xy)*v_vtxColor;\n",
		"mediump vec4 color2 = vec4( v_blend.r , v_blend.g , v_blend.b , 1.0 );\n",
		"mediump vec4 rgb1   = vec4( color1.r * (1.0 - v_blend.a) , color1.g * (1.0 - v_blend.a) , color1.b * (1.0 - v_blend.a ) , 1.0 );\n",
		"mediump vec4 rgb2   = vec4( color2.r * (      v_blend.a) , color2.g * (      v_blend.a) , color2.b * (      v_blend.a ) , 1.0 );\n",
		"gl_FragColor = vec4( rgb1.r + rgb2.r , rgb1.g + rgb2.g , rgb1.b + rgb2.b , color1.a );\n",
	"}\n",
};

//高速シェーダー
const GLchar  *pxlShaderHighSpeed[] = {
	"varying mediump vec2 v_texCoord;\n",
	"varying mediump vec4 v_vtxColor;\n",
	"varying mediump vec4 v_blend;\n",
	"uniform sampler2D u_textureID1;\n",
	"void main(void)\n",
	"{\n",
		"mediump vec4 color1 = texture2D( u_textureID1 , v_texCoord.xy)*v_vtxColor;\n",
		"mediump vec4 color2 = vec4( v_blend.r , v_blend.g , v_blend.b , 1.0 );\n",
		"mediump vec4 rgb1   = vec4( color1.r * (1.0 - v_blend.a) , color1.g * (1.0 - v_blend.a) , color1.b * (1.0 - v_blend.a ) , 1.0 );\n",
		"mediump vec4 rgb2   = vec4( color2.r * (      v_blend.a) , color2.g * (      v_blend.a) , color2.b * (      v_blend.a ) , 1.0 );\n",
		"gl_FragColor = vec4( rgb1.r + rgb2.r , rgb1.g + rgb2.g , rgb1.b + rgb2.b , color1.a );\n",
	"}\n",
};

//白黒シェーダー
const GLchar  *pxlShaderGreyScale[] = {
	"varying mediump vec2 v_texCoord;\n",
	"varying mediump vec4 v_vtxColor;\n",
	"varying mediump vec4 v_blend;\n",
	"uniform mediump float u_option[4];\n",
	"uniform sampler2D u_textureID1;\n",
//	"out vec4 FragColor;\n",
	"void main(void)\n",
	"{\n",
		"mediump vec4 color = texture2D( u_textureID1 , v_texCoord.xy);\n",
		"mediump float lumin = (color.r + color.g + color.b)/3.0;\n",
		"mediump float alpha = u_option[0];\n",
		"mediump float ralpha = 1.0-alpha;\n",
		"mediump vec3 rgb_wh = vec3( lumin , lumin , lumin)*alpha;\n",
		"mediump vec3 rgb_fl = vec3( color.r , color.g , color.b)*ralpha;\n",
		"mediump vec3 rgb = vec3( (rgb_wh.r+rgb_fl.r)*v_vtxColor.r , (rgb_wh.g+rgb_fl.g)*v_vtxColor.g , (rgb_wh.b+rgb_fl.b)*v_vtxColor.b);\n",
		"mediump vec4 blend = v_blend;\n",
		"rgb = rgb*(1.0 - blend.a) + (vec3(blend.r , blend.g , blend.b)*(blend.a) );\n",
		"gl_FragColor = vec4(rgb.r , rgb.g, rgb.b, color.a)*v_vtxColor.a;\n",

	"}\n",
};

//Bloomシェーダー
const GLchar  *pxlShader2[] = {
		//Bloomシェーダー
		"varying mediump vec2 v_texCoord;\n",
		"varying mediump vec4 v_vtxColor;\n",
		"varying mediump vec4 v_position;\n",
		"varying mediump vec4 v_blend;\n",
//		"varying mediump vec4 v_option;\n",
		"uniform mediump float u_option[4];\n",
		"uniform sampler2D u_textureID1;\n",
		"void main(void)\n",
		"{\n",
			"mediump vec4 col0 = texture2D( u_textureID1 , v_texCoord.xy );\n",
			"mediump float fBrightness = max( col0.r , max(col0.g , col0.b ) );\n",

			"if( fBrightness >= u_option[0] ){ \n",
			"	gl_FragColor = col0;\n",
			"}else{\n",
			"	gl_FragColor = vec4(0.0, 0.0, 0.0, 0.0);\n",
			"};\n",
		"}\n",
};

const GLchar  *pxlShader3[] = {
	//GaussianDOFシェーダー
	"varying mediump vec2 v_texCoord;\n",
	"varying mediump vec4 v_vtxColor;\n",
	"varying mediump vec4 v_position;\n",
	"varying mediump vec4 v_blend;\n",
//	"varying mediump  vec2 v_screen;\n",
//	"varying mediump  vec4 v_option;\n",
	"uniform mediump vec2 u_screen;\n",
	"uniform mediump float u_option[4];\n",
	"uniform mediump float u_weight[10];\n",
	"uniform sampler2D u_textureID;\n",
	"void main(void)\n",
	"{\n",

"   	mediump vec2  fc;\n",
"   	mediump vec3  destColor = vec3(0.0);\n",
"		if( u_option[0] == 0.0 )\n",
"		{\n",
"			fc = vec2(gl_FragCoord.x, gl_FragCoord.y);\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x-9.0)/u_screen.x , (fc.y+0.0)/u_screen.y )).rgb*0.0111111114;\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x-8.0)/u_screen.x , (fc.y+0.0)/u_screen.y )).rgb*0.0222222228;\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x-7.0)/u_screen.x , (fc.y+0.0)/u_screen.y )).rgb*0.0333333351;\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x-6.0)/u_screen.x , (fc.y+0.0)/u_screen.y )).rgb*0.0444444455;\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x-5.0)/u_screen.x , (fc.y+0.0)/u_screen.y )).rgb*0.0555555560;\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x-4.0)/u_screen.x , (fc.y+0.0)/u_screen.y )).rgb*0.0666666701;\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x-3.0)/u_screen.x , (fc.y+0.0)/u_screen.y )).rgb*0.0777777731;\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x-2.0)/u_screen.x , (fc.y+0.0)/u_screen.y )).rgb*0.0888888910;\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x-1.0)/u_screen.x , (fc.y+0.0)/u_screen.y )).rgb*0.0999999940;\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x-0.0)/u_screen.x , (fc.y+0.0)/u_screen.y )).rgb*0.111111112;;\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+1.0)/u_screen.x , (fc.y+0.0)/u_screen.y )).rgb*0.0999999940;\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+2.0)/u_screen.x , (fc.y+0.0)/u_screen.y )).rgb*0.0888888910;\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+3.0)/u_screen.x , (fc.y+0.0)/u_screen.y )).rgb*0.0777777731;\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+4.0)/u_screen.x , (fc.y+0.0)/u_screen.y )).rgb*0.0666666701;\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+5.0)/u_screen.x , (fc.y+0.0)/u_screen.y )).rgb*0.0555555560;\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+6.0)/u_screen.x , (fc.y+0.0)/u_screen.y )).rgb*0.0444444455;\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+7.0)/u_screen.x , (fc.y+0.0)/u_screen.y )).rgb*0.0333333351;\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+8.0)/u_screen.x , (fc.y+0.0)/u_screen.y )).rgb*0.0222222228;\n",
"			gl_FragColor = vec4(destColor, 1.0)*v_vtxColor.a;\n",                                             
"        }else{\n",                                                                                           
"			fc = vec2(gl_FragCoord.x, gl_FragCoord.y);\n",                                                    
"           destColor += texture2D( u_textureID, vec2( (fc.x+0.0)/u_screen.x , (fc.y-9.0)/u_screen.y )).rgb*0.0111111114;\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+0.0)/u_screen.x , (fc.y-8.0)/u_screen.y )).rgb*0.0222222228;\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+0.0)/u_screen.x , (fc.y-7.0)/u_screen.y )).rgb*0.0333333351;\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+0.0)/u_screen.x , (fc.y-6.0)/u_screen.y )).rgb*0.0444444455;\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+0.0)/u_screen.x , (fc.y-5.0)/u_screen.y )).rgb*0.0555555560;\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+0.0)/u_screen.x , (fc.y-4.0)/u_screen.y )).rgb*0.0666666701;\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+0.0)/u_screen.x , (fc.y-3.0)/u_screen.y )).rgb*0.0777777731;\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+0.0)/u_screen.x , (fc.y-2.0)/u_screen.y )).rgb*0.0888888910;\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+0.0)/u_screen.x , (fc.y-1.0)/u_screen.y )).rgb*0.0999999940;\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+0.0)/u_screen.x , (fc.y-0.0)/u_screen.y )).rgb*0.111111112 ;\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+0.0)/u_screen.x , (fc.y+1.0)/u_screen.y )).rgb*0.0999999940;\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+0.0)/u_screen.x , (fc.y+2.0)/u_screen.y )).rgb*0.0888888910;\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+0.0)/u_screen.x , (fc.y+3.0)/u_screen.y )).rgb*0.0777777731;\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+0.0)/u_screen.x , (fc.y+4.0)/u_screen.y )).rgb*0.0666666701;\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+0.0)/u_screen.x , (fc.y+5.0)/u_screen.y )).rgb*0.0555555560;\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+0.0)/u_screen.x , (fc.y+6.0)/u_screen.y )).rgb*0.0444444455;\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+0.0)/u_screen.x , (fc.y+7.0)/u_screen.y )).rgb*0.0333333351;\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+0.0)/u_screen.x , (fc.y+8.0)/u_screen.y )).rgb*0.0222222228;\n",
"			gl_FragColor = vec4(destColor, 1.0)*v_vtxColor.a;\n",
"        }\n",
	"}\n",

};

//Rasterシェーダー
const GLchar  *pxlShader4[] = {
		"varying mediump vec2 v_texCoord;\n",
		"varying mediump vec4 v_vtxColor;\n",
		"varying mediump vec4 v_position;\n",
		"varying mediump vec4 v_blend;\n",
		"uniform mediump float u_option[4];\n",
		"uniform sampler2D u_textureID1;\n",
		"void main(void)\n",
		"{\n",
			"mediump float num    = u_option[0];\n",
			"mediump float range  = u_option[1];\n",
			"mediump float time   = u_option[2];\n",
			"mediump vec2 texCoord = v_texCoord.xy;\n",
			"texCoord.x += cos( (texCoord.y+time)*num)*range;\n",
			"mediump vec4 rgba = texture2D( u_textureID1 , texCoord.xy);\n",
			"gl_FragColor = vec4(rgba.r , rgba.g, rgba.b, rgba.a)*v_vtxColor.a;\n",
		"}\n",
};
//			"mediump vec3 rgb = vec3( color.r , color.g , color.b)*v_vtxColor.rgb;\n",
//			"mediump vec4 blend = v_blend;\n",
//			"rgb = rgb*(1.0 - blend.a) + (vec3(blend.r , blend.g , blend.b)*(blend.a) );\n",
//			"gl_FragColor = vec4(rgb.r , rgb.g, rgb.b, color.a)*v_vtxColor.a;\n",


//法線マップシェーダー

const GLchar* pxlShader5[] = {
		"varying mediump vec2 v_texCoord;\n",
		"varying mediump vec2 v_nmlCoord;\n",
		"varying mediump vec4 v_vtxColor;\n",
		"varying mediump vec4 v_position;\n",
		"varying mediump vec4 v_blend;\n",
		"varying mediump vec3  v_pointlight_pos;\n",
		"varying mediump vec3  v_pointlight_rgb;\n",
		"varying mediump float v_pointlight_length;\n",
        "varying mediump vec4 v_options;\n",
		"uniform sampler2D u_textureID1;\n",
		"uniform sampler2D u_textureID2;\n",
		"uniform sampler2D u_textureID3;\n",
		"void main(void)\n",
		"{\n",
			"mediump vec4 color1  = texture2D( u_textureID1  , v_texCoord.xy);\n",								//アルベド
			"mediump vec4 color2  = texture2D( u_textureID2  , v_texCoord.xy+v_nmlCoord.xy );\n",				//法線
//			"mediump vec3 p1 = vec3( v_pointlight_pos[0] , v_pointlight_pos[1] , v_pointlight_pos[2]);\n",		//ライトのポジション
//			"mediump vec3 p2 = vec3( v_position[0]       , v_position[1]       , v_position[2]);\n",			//ピクセルのポジション
			"mediump vec3 p1 = vec3( v_pointlight_pos[0] , v_pointlight_pos[1] , 0.1);\n",			//ライトのポジション
			"mediump vec3 p2 = vec3( v_position[0]       , v_position[1]       , 0.0);\n",			//ピクセルのポジション
			"mediump vec3 p3 = vec3( p1.x-p2.x , p1.y- p2.y, p1.z - p2.z);\n",									//点光源へのベクトル
			"mediump vec3 p4 = vec3( (color2.r-0.5)*2.0 , (color2.g-0.5)*2.0, (color2.b-0.5)*2.0);\n",			//ピクセルの法線ベクトル
			"mediump vec3 p5 = vec3( v_position[0], v_position[1], v_pointlight_pos[2]);\n",										//ピクセルと点光源の2次元の距離
			//ライトからの距離を算出して光の届く範囲を得る
			"mediump float lightlength = distance(p1 , p5);\n",											//光の届く距離
			"lightlength = 1.0 - clamp(lightlength , 0.0 , v_pointlight_length ) / v_pointlight_length;\n",
			//光源と面の法線ベクトルの内積から傾きdを得る、dは0～1の範囲で暗さとなる
			"mediump vec3 vec3_light   = normalize( p3 );\n",
			"mediump vec3 vec3_surface = normalize( p4 );\n",
			"mediump float d = dot( vec3_light , vec3_surface );\n",
			"d = clamp(d , 0.0 , 1.0 );\n",
			//マテリアルの計算
//			"mediump float metalic   = color2.a;\n",														//メタリック＝映り込み（最大0.5）
            "mediump float metalic   = v_options[0];\n",                                                        //メタリック＝映り込み（最大0.5）
			"mediump float roughness = 1.0-metalic;\n",															//ラフネス  = 光沢

			//光沢（明るいところはより明るく、暗いところはより暗くする）
			"mediump vec3 specular = metalic*vec3( 1.0 , 1.0 , 1.0)*(d*pow(d,20.0*(1.0-roughness)));\n",				//スペキュラーの強さ（ラフネスから影響を受ける）

			//映り込みの場所を得る
			"mediump vec2 texel;\n",
			"texel.x = (v_position.x+1.0)/2.0 + (1.0+vec3_surface.x)/2.0;\n",
			"texel.y = (v_position.y+1.0)/2.0 + (1.0+vec3_surface.y)/2.0;\n",
			"texel.x = texel.x/2.0;\n",
			"texel.y = texel.y/2.0;\n",
            //"texel.x = v_position[0];\n",//(v_position[0]+1.0*(p4.x)/2.0);\n",
            //"texel.y = v_position[1];\n",//(v_position[1]+1.0*(p4.y)/2.0);\n",
        //    "texel.x = v_position[0] + (1.0-abs(v_position[0])*p4.x);\n",
        //    "texel.y = v_position[1] + (1.0-abs(v_position[1])*p4.y);\n",
            "mediump float ax = abs(v_position[0]);\n",
            "mediump float ay = abs(v_position[1]);\n",
    "texel.x = v_position[0]+(1.0-ax)*p4.x;\n",// + (1.0-abs(v_position[0])*p4.x);\n",
    "texel.y = v_position[1]+(1.0-ay)*p4.y;\n",// + (1.0-abs(v_position[1])*p4.y);\n",
            "texel.x = clamp(texel.x , -1.0 , 1.0);\n",
			"texel.y = clamp(texel.y , -1.0 , 1.0);\n",
			"texel.x = (texel.x+1.0)/2.0;\n",
			"texel.y = (texel.y+1.0)/2.0;\n",
			"mediump vec4 env = texture2D( u_textureID3  , texel.xy );\n",										//環境テクスチャ
			//"env = vec4(v_pointlight_length,v_pointlight_length,v_pointlight_length,1.0);\n",										//環境テクスチャ

			//環境テクスチャ
			"mediump float a1n = metalic*1.0;\n",
			"mediump float a1r = 1.0-a1n;\n",
			"mediump vec3 rgb = vec3( color1.r*a1r + env.r*a1n , color1.g*a1r+env.g*a1n , color1.b*a1r +env.b*a1n)*v_vtxColor.rgb;\n",		//色：（アルベド＋映り込み）＊頂点カラー（先に頂点カラーをかけないと色がついたスペキュラになる）
			"rgb = rgb*d*lightlength;\n",																							//影：法線方向による陰影と光の減衰率を掛け合わせる
			"rgb = rgb+specular;\n",																								//光：スペキュラ成分は最後に足す
			"gl_FragColor = vec4( rgb.x , rgb.y, rgb.z, color1.a)*v_vtxColor.a;\n",													//α：最後に半透明度合いを計算する
			//"gl_FragColor = vec4( (env.r+specular.r)*lightlength , (env.g+specular.g)*lightlength, (env.b+specular.b)*lightlength, color1.a)*v_vtxColor.a;\n",													//最後に半透明度合いを計算する
			//"vec3 rgb = vec4( env.r , env.g, env.b, color1.a)*v_vtxColor.a;\n",													//最後に半透明度合いを計算する
			//液体金属にしたい場合は法線マップを無効にする
			//"rgb = (rgb + specular)*d*lightlength;\n",																			//法線方向による影と光の減衰率を掛け合わせる

		"}\n",
};

//フォントシェーダー
const GLchar  *pxlShader6[] = {
    "varying mediump vec2 v_texCoord;\n",
    "varying mediump vec4 v_vtxColor;\n",
    "varying mediump vec4 v_blend;\n",
    "varying mediump vec4 v_options;\n",
    "uniform sampler2D u_textureID1;\n",
    "void main(void)\n",
    "{\n",
    "mediump vec4 color1 = texture2D( u_textureID1 , v_texCoord.xy);\n",
    "mediump vec4 color2 = vec4( v_blend.r , v_blend.g , v_blend.b , 1.0 );\n",
    "mediump float mode = v_options[0];\n",
    "mediump float alpha = color1.r;\n",
    "if(mode == 1.0) alpha = color1.g;\n",
    "if(mode == 2.0) alpha = color1.b;\n",
	"if(mode == 3.0) alpha = color1.a;\n",
    "mediump vec4 rgb1   = vec4( alpha * (1.0 - v_blend.a) , alpha * (1.0 - v_blend.a) , alpha * (1.0 - v_blend.a ) , alpha );\n",
    "mediump vec4 rgb2   = vec4( color2.r * (      v_blend.a) , color2.g * (      v_blend.a) , color2.b * (      v_blend.a ) , 1.0 );\n",
    "gl_FragColor = vec4( rgb1.r + rgb2.r , rgb1.g + rgb2.g , rgb1.b + rgb2.b , alpha )*v_vtxColor;\n",
    "}\n",
};

const GLchar* pxlShader0[] = {
		"varying mediump vec2 v_texCoord;\n",
		"varying mediump vec4 v_vtxColor;\n",
		"varying mediump vec4 v_position;\n",
		"varying mediump vec4 v_blend;\n",
		"uniform sampler2D u_textureID1;\n",
		"uniform sampler2D u_texNormal;\n",
		"uniform mediump float u_PLight[10];\n",
		"void main(void)\n",
		"{\n",
			"mediump vec4 color1  = texture2D( u_textureID1 , v_texCoord.xy);\n",
			"mediump vec4 color2  = texture2D( u_texNormal  , v_texCoord.xy);\n",
			"mediump vec3 p1 = vec3( u_PLight[0] , u_PLight[1] , u_PLight[2]);\n",
			"mediump vec3 p2 = vec3( v_position[0] , v_position[1] , v_position[2]);\n",
			"mediump vec3 p3 = vec3( p1.x-p2.x , p1.y- p2.y, p1.z - p2.z);\n",		//点光源へのベクトル
			"mediump vec3 p4 = vec3( (color2.r-0.5)*2.0 , (color2.g-0.5)*2.0, (color2.b-0.5)*2.0);\n",			//面の法線ベクトル
			"mediump vec3 vec3_light   = normalize( p3 );\n",
			"mediump vec3 vec3_surface = normalize( p4 );\n",
			"mediump float d = dot( vec3_light , vec3_surface );\n",
			"d = clamp(d , 0.0 , 1.0 );\n",
			"mediump float l = 0.0;\n",
			"d = clamp(d , 0.0 , 1.0 )*(1.0-l);\n",
			"mediump vec3 rgb = vec3( color1.r , color1.g , color1.b)*d;\n",
			"gl_FragColor = vec4( rgb.x , rgb.y, rgb.z, 1.0)*v_vtxColor.a;\n",
		"}\n",
};


COpenGLIOS::COpenGLIOS()
{
	m_FrameBufferIndex = -1;

	m_SizeOfVertexShader[0] = sizeof(vtxShaderHighSpeed)/ sizeof(vtxShaderHighSpeed[0]);
	m_SizeOfVertexShader[1] = sizeof(vtxShader1)/ sizeof(vtxShader1[0]);

	m_SizeOfPixelShader[0] = sizeof(pxlShader0)/ sizeof(pxlShader0[0]);
	m_SizeOfPixelShader[1] = sizeof(pxlShader1) / sizeof(pxlShader1[0]);
	m_SizeOfPixelShader[2] = sizeof(pxlShader2) / sizeof(pxlShader2[0]);
	m_SizeOfPixelShader[3] = sizeof(pxlShader3) / sizeof(pxlShader3[0]);
	m_SizeOfPixelShader[4] = sizeof(pxlShader4) / sizeof(pxlShader4[0]);
	m_SizeOfPixelShader[5] = sizeof(pxlShader5) / sizeof(pxlShader5[0]);
	m_SizeOfPixelShader[6] = sizeof(pxlShader6) / sizeof(pxlShader6[0]);
	m_SizeOfPixelShader[7] = sizeof(pxlShaderHighSpeed) / sizeof(pxlShaderHighSpeed[0]);
	m_SizeOfPixelShader[8] = sizeof(pxlShaderGreyScale) / sizeof(pxlShaderGreyScale[0]);
}


COpenGLIOS::~COpenGLIOS()
{

}


void COpenGLIOS::Init()
{
	//------------------------------------------------------
	// 頂点シェーダー ファイルを読み込んだ後、シェーダーと入力レイアウトを作成します。
	//------------------------------------------------------
	//Reset
	glViewport(0, 0, m_GameW, m_GameH);


	//display= eglGetDisplay( EGL_DEFAULT_DISPLAY );
    //surface = eglGetCurrentSurface(EGL_READ);

    //if( EGL_NO_SURFACE == surface )
    //{
    //    surface = eglGetCurrentSurface(EGL_DRAW);
    //}

    _CheckError();

	glActiveTexture(GL_TEXTURE0 );

	initShader();

	initTexture();

	initVBO();

	Reset();

		glEnable ( GL_TEXTURE_2D );
/*毒*///		glEnable ( GL_NORMALIZE );
/*毒*///		glEnable ( GL_ALPHA_TEST );
		glEnable ( GL_BLEND );
		glDisable( GL_DEPTH_TEST );

	m_bInitCompleted = gxTrue;

}


void COpenGLIOS::Reset()
{
	//フルスクリーン切り替え

	Sint32 gamew = m_GameW, gameh = m_GameH;
	Sint32 winw  = m_GameW, winh  = m_GameH;

	CGameGirl::GetInstance()->GetGameResolution(&gamew, &gameh);
	CGameGirl::GetInstance()->GetWindowsResolution(&winw, &winh);

	m_BackBufferSize.TopLeftX = 0;
	m_BackBufferSize.TopLeftY = 0;
	m_BackBufferSize.Width  = winw;
	m_BackBufferSize.Height = winh;

}

void COpenGLIOS::SwapBuffer()
{

}

void COpenGLIOS::GetFrameBufferImage(gxChar* pFileName)
{
	// フレームバッファの内容をPBOに転送

}
