// --------------------------------------------------------------------
//
// つじつまあわせ用共通ヘッダ
//
// --------------------------------------------------------------------
#ifndef _MACHINE_H_
#define _MACHINE_H_

#ifdef NDEBUG
	#define GX_RELEASE
#else
	#define GX_DEBUG
#endif

#ifdef PLATFORM_WINDOWS_DESKTOP
	#include "Windows/DirectX11/machine.h"

#elif defined PLATFORM_WINDOWS_STORE
	#include "Windows/WinStore/machine.h"

#elif defined PLATFORM_ANDROID
	#include "Android/machine.h"

#elif defined PLATFORM_IOS
    #include "iOS/machine.h"
#else
	#define PLATFORM_WINDOWS_COMMANDPROMPT
#include "Windows/dos/machine.h"
#endif

#define GLOBAL_IP_ADDRESS_CHECK_URL "http://garuru.co.jp/api/gxLib.php?ope=GetIP"

//テクスチャキャッシュを使うか？
//同一テクスチャページに同一ファイル名のテクスチャが読み込まれたときに読み込みをスキップする
#define USE_TEXTURE_CACHE

//------------------------------------------------
//各プラットフォーム共通のラッピング関数
//------------------------------------------------

class CDeviceManager
{
public:

	CDeviceManager();
	virtual ~CDeviceManager();

	virtual void   AppInit();

	virtual void   GameInit();
	virtual gxBool GameUpdate();
	virtual gxBool GamePause();
	virtual void   Render();
	virtual void   vSync();
	virtual void   Flip();
	virtual void   Resume();
	virtual void   Movie();
	virtual void   Play();
	virtual gxBool NetWork();

	virtual void   UploadTexture(Sint32 sBank);
	virtual void   LogDisp(char* pString);
	virtual void   SetStatusBar(gxBool bOn = gxTrue);


	//virtual void   Clock( gxClock *pClock );

	virtual Uint8* LoadFile( const gxChar* pFileName , size_t* pLength , ESTORAGE_LOCATION uLocation );
	virtual gxBool SaveFile( const gxChar* pFileName , Uint8* pReadBuf , size_t uSize , ESTORAGE_LOCATION uLocation );

	virtual void MakeThread( void* (*pFunc)(void*) , void * pArg=nullptr );
	virtual void Sleep( Uint32 msec );
	virtual gxBool PadConfig( Sint32 padNo , Uint32 button );

	//特殊

	virtual void ToastDisp( gxChar* pMessage );
	virtual void OpenWebClient( gxChar* pURL , gxBool bBrowser = gxFalse );

	virtual gxBool SetAchievement( Uint32 index );
	virtual gxBool GetAchievement( Uint32 index );

    virtual std::string GetClipBoardString();
    virtual void SetClipBoardString(std::string str);

    gxBool PadConfig();		//←同じ名前に注意

	void SetBatchMode()
	{
		m_bBatchMode = gxTrue;
	}

	gxBool IsBatchMode()
	{
		return m_bBatchMode;
	}

	void SetAutoSave( gxBool bAutoSave )
	{
		m_bAutoSave = bAutoSave;
	}

	gxBool GetAutoSave()
	{
		return m_bAutoSave;
	}

	static std::vector<Uint32>  UTF8toUTF32(gxChar* pString, size_t* pSize = NULL);
	static std::string UTF32toUTF8(std::vector<Uint32>u32, size_t* pSize = NULL);
	static std::string UTF8toSJIS ( gxChar*  pUTF8buf , size_t* pSize = NULL );
	static std::string SJIStoUTF8 ( gxChar*  pSJISbuf , size_t* pSize = NULL );

	//static gxChar*  SJIStoUTF32 ( gxChar*  pSJISbuf , size_t* pSize = NULL );
	//static gxChar*  UTF32toSJIS(gxChar* pString, size_t* pSize = NULL);


	//static wchar_t* UTF8toUTF16( gxChar*  pString  , size_t* pSize = NULL );
	//static gxChar*  UTF16toUTF8( wchar_t* pUTF16buf, size_t* pSize = NULL );

	//static gxChar*  UTF16toSJIS( wchar_t* pUTF16buf, size_t* pSize = NULL );
	//static wchar_t* SJIStoUTF16( gxChar*  pString  , size_t* pSize = NULL );

	static  void   SystemLogDisp(char* pFormat, ...)
	{
		if( s_pInstance->IsBatchMode() ) return;

		static gxChar _buf[FILENAMEBUF_LENGTH];
		va_list app;

		if( pFormat == NULL ) return;

		va_start( app, pFormat );

		if ( vsprintf(_buf, pFormat, app) >= FILENAMEBUF_LENGTH)
		{
			va_end( app );
			return;
		}

		va_end( app );
		_buf[FILENAMEBUF_LENGTH - 1] = 0x00;

		s_pInstance->LogDisp(_buf);
	}

	static std::vector<std::string> GetDirList( std::string rootDir );

	static Float32 GetFreeStorageSize();

    std::string m_ClipBoardString;
	
	std::atomic_int32_t m_ThreadNum;

	SINGLETON_DECLARE( CDeviceManager );


private:

	gxBool m_bBatchMode = gxFalse;
	gxBool m_bAutoSave = gxTrue;
	//size_t m_AvailableSize = 0;
};


#ifdef GX_DEBUG
	//最適化OFF / DEBUGLOG ON
	#define GX_DEBUGLOG CDeviceManager::SystemLogDisp

#elif defined GX_RELEASE
	//最適化ON / DEBUGLOG ON
	#define GX_DEBUGLOG CDeviceManager::SystemLogDisp

#else	//GX_MASETER
	//最適化ON / DEBUGLOG OFF
	#define GX_DEBUGLOG

#endif

//game Src側へのアクセス関数

gxBool GameInit( std::vector<std::string> args );
gxBool GamePause();
gxBool GameMain();
gxBool GameSleep();
gxBool GameResume();
gxBool GameEnd();
gxBool GameReset();

#endif
