//---------------------------------------------------------------
// アプリのライフサイクル管理
// JNIによるJavaとの交信を行う
//---------------------------------------------------------------
// OpenGL ES 2.0 code

#include <jni.h>
#include <android/log.h>

#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <gxLib.h>
#include <gxLib/gx.h>
#include <gxLib/gxPadManager.h>
#include <gxLib/gxNetworkManager.h>
#include "COpenGLAndroid.h"
#include "CGamePad.h"

#include <string.h>
#include <iostream>
#include<fstream>
#include <fcntl.h>
#include<sys/stat.h>
#include<sys/types.h>
#include <unistd.h>
#include <gxLib/gxSoundManager.h>
#include <gxLib/hard/COpenAL.h>
#include "play/asset_pack.h"
#include <android/native_activity.h>
#include "assetpack/assetControl.h"

gxBool GameEnd();
const char* _GetPackageName();
const int _GetAvailableStorageSize();
void  _SetClipBoardText( std::string str);
gxChar* _GetClipBoardText();


extern "C" {
	JNIEXPORT void JNICALL Java_jp_co_garuru_GameGirlJNI_appInit(JNIEnv * env, jobject obj);
	JNIEXPORT void JNICALL Java_jp_co_garuru_GameGirlJNI_appRestart(JNIEnv * env, jobject obj);
	JNIEXPORT void JNICALL Java_jp_co_garuru_GameGirlJNI_appResume(JNIEnv * env, jobject obj, jint width, jint height);
	JNIEXPORT void JNICALL Java_jp_co_garuru_GameGirlJNI_appUpdate(JNIEnv * env, jobject obj);
    JNIEXPORT void JNICALL Java_jp_co_garuru_GameGirlJNI_appPause(JNIEnv * env, jobject obj);
	JNIEXPORT void JNICALL Java_jp_co_garuru_GameGirlJNI_appStop(JNIEnv * env, jobject obj);
    JNIEXPORT void JNICALL Java_jp_co_garuru_GameGirlJNI_appEnd(JNIEnv * env, jobject obj);

	JNIEXPORT void JNICALL Java_jp_co_garuru_GameGirlJNI_cursorUpdate( JNIEnv * env, jobject obj , int id , int px , int py , int bPush );
	JNIEXPORT void JNICALL Java_jp_co_garuru_GameGirlJNI_inputUpdate( JNIEnv * env, jobject obj , int id , int push , int keyCode );
	JNIEXPORT void JNICALL Java_jp_co_garuru_GameGirlJNI_analogUpdate( JNIEnv * env, jobject obj , int id, jfloatArray analog );
    JNIEXPORT void JNICALL Java_jp_co_garuru_GameGirlJNI_UpdateCensors( JNIEnv * env, jobject obj , jfloatArray analog );
	JNIEXPORT void JNICALL Java_jp_co_garuru_GameGirlJNI_ReturnHttpResult( JNIEnv * env, jobject obj , int id , jbyteArray ary  , jboolean  bSuccess);

	JNIEXPORT void JNICALL Java_jp_co_garuru_GameGirlJNI_SetJava2Cpp(JNIEnv * env, jobject obj , jbyteArray recvData , int size );
	JNIEXPORT jbyteArray JNICALL Java_jp_co_garuru_GameGirlJNI_GetCppState(JNIEnv * env, jobject obj );
	JNIEXPORT void JNICALL Java_jp_co_garuru_GameGirlJNI_step(JNIEnv * env, jobject obj);
	JNIEXPORT int JNICALL Java_jp_co_garuru_GameGirlJNI_SetOBBPath(JNIEnv * env, jobject obj , jstring jstr1 );
    JNIEXPORT int JNICALL Java_jp_co_garuru_GameGirlJNI_SetSDPath(JNIEnv * env, jobject obj , jstring jstr1 );

    JNIEXPORT jbyteArray JNICALL Java_jp_co_garuru_GameGirlJNI_GetBTData( JNIEnv * env, jobject obj );
    JNIEXPORT void JNICALL Java_jp_co_garuru_GameGirlJNI_SetBTData( JNIEnv * env, jobject obj , jbyteArray recvData , int size );

	JNIEXPORT void JNICALL Java_jp_co_garuru_GameGirlJNI_SetAndroidContext( JNIEnv * env, jobject obj , jobject p_context );

};


typedef struct StGameGirlState
{
	gxPos  Mouse[5];
	Uint32 m_Button[4];
	Uint32 m_Analog[4];
	Uint32 m_Menu[32];

} StGameGirlState;

AAssetManager* GetAssetManager();

JNIEXPORT void JNICALL Java_jp_co_garuru_GameGirlJNI_SetAndroidContext( JNIEnv * env, jobject obj , jobject p_context )
{
	CAndroid::GetInstance()->SetAndroidContext( p_context );
}

gxBool assetPackManager_Init(JNIEnv * env)
{
	// ---------------------------------------------------------------------
	// aabの初期化準備
	// ---------------------------------------------------------------------
    CAndroid::GetInstance()->m_pJavaVM->AttachCurrentThread( &env, NULL );

	JavaVM* jvm = CAndroid::GetInstance()->m_pJavaVM;
	jobject jobj = CAndroid::GetInstance()->GetAndroidContext();

	jclass activityThread = env->FindClass("android/app/ActivityThread");
	jmethodID currentActivityThread = env->GetStaticMethodID(activityThread, "currentActivityThread", "()Landroid/app/ActivityThread;");
	jobject at = env->CallStaticObjectMethod(activityThread, currentActivityThread);

	jmethodID getApplication = env->GetMethodID(activityThread, "getApplication", "()Landroid/app/Application;");
	jobject context = env->CallObjectMethod(at, getApplication);
	jobj = context;
	CAndroid::GetInstance()->SetAndroidContext( context );

	// ---------------------------------------------------------------------
	// fast-follow 配信と on-demand 配信の初期化
	// ---------------------------------------------------------------------

	//AAssetManager *am = GetAssetManager();
	AssetPackErrorCode code = AssetPackManager_init( jvm, jobj);

#if 0
	AssetPackLocation* location;
	AssetPackErrorCode error_code = AssetPackManager_getAssetPackLocation("PK_InstallPack", &location);

	if (error_code == ASSET_PACK_NO_ERROR)
	{
		size_t size = 1;
		const char *packs[]={
				"PK_FastFollowPack",
		};
		code = AssetPackManager_requestInfo(packs,size);      // Call once
	}
#endif
	return gxFalse;
}


void assetPackManager_Update()
{

	AssetController::GetInstance()->Update();

/*
	AssetPackDownloadState_ *state;
	AssetPackErrorCode code = AssetPackManager_getDownloadState("PK_FastFollowPack",&state);
	uint64_t downloadedBytes = AssetPackDownloadState_getBytesDownloaded(state);
	uint64_t totalBytes = AssetPackDownloadState_getTotalBytesToDownload(state);
	AssetPackDownloadState_destroy(state);

	//-----------------------------
	AssetPackLocation* location;

	AssetPackStorageMethod storage_method = AssetPackLocation_getStorageMethod(location);
	const char* assets_path = AssetPackLocation_getAssetsPath(location);
	AssetPackLocation_destroy(location);
	//アセットが見つかったら、fopen や ifstream などの関数を使用してファイルにアクセスします。
*/

}


JNIEXPORT void JNICALL Java_jp_co_garuru_GameGirlJNI_appInit(JNIEnv * env, jobject obj)
{
	(*env).GetJavaVM( &CAndroid::GetInstance()->m_pJavaVM );
	CAndroid::GetInstance()->SetPackageName( _GetPackageName() );

	if( !assetPackManager_Init(env) )
	{
		//アセットパックの初期化に失敗した
	}

	CDeviceManager::GetInstance()->AppInit();

	//gxChar*pClip = CAndroid::GetClipBoardText();
	//CAndroid::SetClipBoardText( "garurruClipBoardText" );

}

static Float32 tempVolume = -1.0f;
static Float32 volumeOn = true;

JNIEXPORT void JNICALL Java_jp_co_garuru_GameGirlJNI_appRestart(JNIEnv * env, jobject obj)
{
    if(tempVolume >= 0.0f )
    {
        if(!volumeOn)
        {
            CGameGirl::GetInstance()->SetPause( gxFalse , 120 );
            gxLib::SetAudioMasterVolume(tempVolume);
			CAudio::GetInstance()->ResumeAllSound(1.0f);
        }
    }
    volumeOn = true;
}

gxBool assetPackManager_IsError( AssetPackErrorCode code )
{
	//// There was no error with the request.
	switch(code) {
		case ASSET_PACK_NO_ERROR:
		case ASSET_PACK_APP_UNAVAILABLE:
			/// 要求されたアプリが利用できない
			/// 複数の理由が考えられる
			/// アプリがGooglePlayStoreでパブリッシュされていない
			/// アプリのバージョン番号が古い可能性がある
			/// アプリにアクセスできるユーザーではない（ClosedBetaTestトラックなどで）
			return gxFalse;

		case ASSET_PACK_UNAVAILABLE:
			/// 要求されたアセットパックが利用できない
			/// aabにアセットパックが含まれてない可能性があるよ
			break;

		case ASSET_PACK_INVALID_REQUEST:
			/// 要求したリクエストが不正
			break;

		case ASSET_PACK_DOWNLOAD_NOT_FOUND:
			/// ダウンロードファイルが存在しなかった
			break;

		case ASSET_PACK_API_NOT_AVAILABLE:
			/// "Asset Pack API"が利用できない
			break;

		case ASSET_PACK_NETWORK_ERROR:
			/// Network error. Unable to obtain asset pack details.
			//ネットワークエラー。アセットパックの詳細を取得できません
			break;

		case ASSET_PACK_ACCESS_DENIED:
			///たとえば、現在のデバイスの状況ではダウンロードは許可されていません
			///アプリがバックグラウンドで実行されているか、デバイスがGoogleアカウントにサインインしていない
			break;

		case ASSET_PACK_INSUFFICIENT_STORAGE:
			/// ストレージが不十分なため、アセットパックのダウンロードに失敗しました。
			break;

		case ASSET_PACK_PLAY_STORE_NOT_FOUND:
			/// Playストアアプリがインストールされていないか、公式バージョンではありません。
			break;

		case ASSET_PACK_NETWORK_UNRESTRICTED:
			/// "showCellularDataConfirmation"が呼び出されたが、Wi-Fiを待機しているアセットパックがない場合に返されます。
			break;

		case ASSET_PACK_APP_NOT_OWNED:
			/// このアプリは、このデバイスのどのユーザーも所有していません。
			/// アプリがPlayストアから得た場合、そのアプリは「所有」されています。
			break;

		case ASSET_PACK_INTERNAL_ERROR:
			/// アセットパックのダウンロード中に不明なエラーが発生しました。
			break;

		case ASSET_PACK_INITIALIZATION_NEEDED:
			///要求された操作は失敗しました：AssetPackManager_init（）を呼び出す必要があります
			break;

		case ASSET_PACK_INITIALIZATION_FAILED:
			///	Asset PackAPIの初期化中にエラーが発生しました。
			break;
	}

	return gxTrue;
}

JNIEXPORT void JNICALL Java_jp_co_garuru_GameGirlJNI_appResume(JNIEnv * env, jobject obj, jint width, jint height)
{
	CGameGirl::GetInstance()->SetWindowSize(width , height);
	CGameGirl::GetInstance()->AdjustScreenResolution();
	AssetPackErrorCode code = AssetPackManager_onResume();

	assetPackManager_IsError( code );

}


JNIEXPORT void JNICALL Java_jp_co_garuru_GameGirlJNI_appPause(JNIEnv * env, jobject obj)
{
//	int appPause = 0;
//	appPause ++;
//
//    if( volumeOn )
//    {
//        tempVolume = gxLib::GetAudioMasterVolume();
//        gxLib::SetAudioMasterVolume(0.0f);
//        CAudio::GetInstance()->AllCutOff();
//        CGameGirl::GetInstance()->SetPause( gxTrue );
//        volumeOn = false;
//    }
	AssetPackErrorCode code = AssetPackManager_onPause();
	assetPackManager_IsError( code );
}

JNIEXPORT void JNICALL Java_jp_co_garuru_GameGirlJNI_appStop(JNIEnv * env, jobject obj)
{
	//int appPause = 0;
	//appPause ++;

	if( volumeOn )
	{
		tempVolume = gxLib::GetAudioMasterVolume();
		gxLib::SetAudioMasterVolume(0.0f);
		CAudio::GetInstance()->AllCutOff();
		CGameGirl::GetInstance()->SetPause( gxTrue );
		volumeOn = false;
	}
}

JNIEXPORT void JNICALL Java_jp_co_garuru_GameGirlJNI_appEnd(JNIEnv * env, jobject obj)
{
    ::GameEnd();
}

JNIEXPORT void JNICALL Java_jp_co_garuru_GameGirlJNI_appUpdate(JNIEnv * env, jobject obj)
{
	//メインスレッドから呼ばれる毎フレームの処理
	//別スレッドで処理できないJNIの処理などはここにまとめる

	CGameGirl::GetInstance()->Action();

    //JNIEnv* env;

    if( CAndroid::GetInstance()->m_pJavaVM == NULL )
    {
        return;
    }

	assetPackManager_Update();

    //CAndroid::GetInstance()->m_pJavaVM->AttachCurrentThread( &env, NULL );

	//WebView

    jclass jc = env->FindClass( JAVA_PROGRAM_PATH );

    if( CAndroid::GetInstance()->m_WebViewURL != "" )
    {

        {
            const char* pStr = CAndroid::GetInstance()->m_WebViewURL.c_str();

			if( CAndroid::GetInstance()->m_bOpenWebBrowser )
			{
				jmethodID SetWebBrowserURL = env->GetStaticMethodID(jc, "SetWebBrowserURL", "(Ljava/lang/String;)V");
				jstring str = env->NewStringUTF( pStr );
				env->CallStaticVoidMethod(jc, SetWebBrowserURL ,str );
			} else{
				jmethodID SetWebViewURL = env->GetStaticMethodID(jc, "SetWebViewURL", "(Ljava/lang/String;)V");
				jstring str = env->NewStringUTF( pStr );
				env->CallStaticVoidMethod(jc, SetWebViewURL ,str );
			}

			CAndroid::GetInstance()->m_WebViewURL = "";
        }
    }

	//Vibration
	Float32 fRatio = 0.0f;
	if( gxPadManager::GetInstance()->GetMotorStat(0, 0, &fRatio) )
	{
		jmethodID id = env->GetStaticMethodID(jc, "StartVibration", "(I)V");
		
		Sint32 msec = 100 * fRatio;
		env->CallStaticVoidMethod(jc, id , msec );
	}


	//http

    if( CAndroid::GetInstance()->m_HTTPURLString[0] )
    {
		jmethodID id = env->GetStaticMethodID( jc, "HTTPAccess", "(ILjava/lang/String;)I" );

        jstring str = env->NewStringUTF( CAndroid::GetInstance()->m_HTTPURLString );
		env->CallStaticIntMethod( jc, id , CAndroid::GetInstance()->m_HTTPURLIndex , str );
		CAndroid::GetInstance()->m_HTTPURLString[0] = 0x00;
	}

	//freeSize

	if( CAndroid::GetInstance()->m_bGetAvailableSize.load() )
	{
		int sz = _GetAvailableStorageSize();
		CAndroid::GetInstance()->m_AvailableSize.store( sz );
		CAndroid::GetInstance()->m_bGetAvailableSize.store(gxFalse);
	}

	//ClipBoard
	if( CAndroid::GetInstance()->m_SetClipBoardText != "" )
	{
		//クリップボードに値をセット
		_SetClipBoardText( CAndroid::GetInstance()->m_SetClipBoardText );
		CAndroid::GetInstance()->m_SetClipBoardText = "";
	}

	if( CAndroid::GetInstance()->m_bGetClipBoardText.load() )
	{
		gxChar* pStr = _GetClipBoardText();
		CAndroid::GetInstance()->m_GetClipBoardText = pStr;
		CAndroid::GetInstance()->m_bGetClipBoardText.store( gxFalse );
	}

	if (CGameGirl::GetInstance()->IsAppFinish())
	{
		JNIEnv* env;

		if( CAndroid::GetInstance()->m_pJavaVM == NULL )
		{
			return;
		}

	    if( CAndroid::GetInstance()->m_pJavaVM->GetEnv( (void**)&env, JNI_VERSION_1_6 ) == JNI_OK )
	    {
	        CAndroid::GetInstance()->m_pJavaVM->AttachCurrentThread( &env, NULL );

			//javaからパッケージ名を引っ張ってくる

			jc = env->FindClass( JAVA_PROGRAM_PATH );

			if (jc != 0)
			{
				jmethodID id = env->GetStaticMethodID( jc, "callCloseApp", "()V" );

				if( id != NULL )
				{
					env->CallStaticVoidMethod( jc ,id );
				}
			}
	    }
	}

}



JNIEXPORT void JNICALL Java_jp_co_garuru_GameGirlJNI_cursorUpdate( JNIEnv * env, jobject obj , int id , int px , int py , int push )
{
    if( id == 999 )
    {
        //何も押されていないのにPush判定が立っている場合はここで切り落とす
        if( gxLib::Joy(0)->psh&MOUSE_L ) CGamePad::GetInstance()->InputCursorCheck( 0 , gxFalse , 0 , 0 );
        if( gxLib::Joy(0)->psh&MOUSE_R ) CGamePad::GetInstance()->InputCursorCheck( 1 , gxFalse , 0 , 0 );
        if( gxLib::Joy(0)->psh&MOUSE_M ) CGamePad::GetInstance()->InputCursorCheck( 2 , gxFalse , 0 , 0 );
    }


	if( id < 0 || id >= 8 ) return;

	gxBool bPush = gxFalse;

	if( push == 1 )
	{
		bPush = gxTrue;
	}
	else if( push == 2 )
	{
		bPush = gxFalse;
	}
	else
	{
		return;
	}

	CGamePad::GetInstance()->InputCursorCheck( id , bPush , px , py );
}


JNIEXPORT void JNICALL Java_jp_co_garuru_GameGirlJNI_SetJava2Cpp(JNIEnv * env, jobject obj , jbyteArray recvData , int size )
{
	//java -> Cpp

	jbyte * elems = env->GetByteArrayElements( recvData, NULL );

	//StGameGirlState *pState = (StGameGirlState*)elems;

	if( elems[16])
	{
#ifndef GX_MASTER
        CGameGirl::GetInstance()->ToggleDeviceMode();
#endif
	}
    if( elems[17])
    {
        int vol = elems[17]-1;
        gxLib::SetAudioMasterVolume( vol / 100.0f );
    }

}

JNIEXPORT jbyteArray JNICALL Java_jp_co_garuru_GameGirlJNI_GetCppState(JNIEnv * env, jobject obj )
{
	//Cpp -> Java

	jbyteArray arrj = env->NewByteArray(5);

	//// 要素列取得 arrjをコピーして、そのポインタを返す。①
	jbyte * elems = env->GetByteArrayElements(arrj, NULL );

	elems[0] = 0x00;
	elems[1] = 0x00;
	elems[2] = 0x00;
	elems[3] = 0x00;
	elems[4] = 0x00;

	env->SetByteArrayRegion(arrj, 0, 5, elems);
	env->ReleaseByteArrayElements(arrj, elems, 0);// ①の取得を開放

	return arrj;
}


JNIEXPORT void JNICALL Java_jp_co_garuru_GameGirlJNI_inputUpdate( JNIEnv * env, jobject obj , int id , int push , int keyCode )
{
	if( keyCode == 0 ) return;

	CGamePad::GetInstance()->InputKeyCheck( id , push , keyCode );
}


JNIEXPORT void JNICALL Java_jp_co_garuru_GameGirlJNI_analogUpdate( JNIEnv * env, jobject obj , int id , jfloatArray analog )
{
	float * pFloatElems = env->GetFloatArrayElements( analog, NULL );

	CGamePad::GetInstance()->SetAnalog( id , pFloatElems );

}


JNIEXPORT void JNICALL Java_jp_co_garuru_GameGirlJNI_UpdateCensors( JNIEnv * env, jobject obj , jfloatArray data )
{
    float * pFloatElems = env->GetFloatArrayElements( data, NULL );

    CGamePad::GetInstance()->SetCensorData( pFloatElems );

}


JNIEXPORT void JNICALL Java_jp_co_garuru_GameGirlJNI_ReturnHttpResult( JNIEnv * env, jobject obj , int id ,jbyteArray jstr1 , jboolean  bSuccess )
{
//	const char* pData = env->GetStringUTFChars(jstr1, 0); //GetStringChars
//	size_t uSize = env->GetStringLength(jstr1);

	jbyte * pByteData = env->GetByteArrayElements( jstr1, NULL );
	int nSize = env->GetArrayLength( jstr1 );

	gxNetworkManager::StHTTP *phttp;
	phttp = gxNetworkManager::GetInstance()->GetHTTPInfo( id );

	phttp->uFileSize	  = nSize;
	phttp->uReadFileSize  = nSize;
	if(bSuccess)
	{
		phttp->pData = new Uint8[nSize+1];
		gxUtil::MemCpy( phttp->pData , pByteData , nSize);
		phttp->pData[ nSize ] = 0x00;
		phttp->SetError(gxNetworkManager::EError::Success);
	} else{
		phttp->pData = nullptr;
		phttp->SetError(gxNetworkManager::EError::Failed);
	}

    phttp->SetSeq( gxNetworkManager::ESeq::eHTTPSeqLoadEnd );   //先にこれを通すとスレッドの順番の都合でファイルコピー中につき失敗扱いになる


/*
	gxNetworkManager::StHTTP *phttp;
	phttp = gxNetworkManager::GetInstance()->GetHTTPInfo( id );

	//SAFE_DELETES(phttp->pData);


	phttp->pData = new Uint8[ uSize+1 ];

	gxUtil::MemCpy(phttp->pData , (void*)pData , uSize );
	phttp->pData[ uSize ] = 0x00;
	phttp->uSize		  = uSize;
	phttp->SetSeq( 999 );

	env->ReleaseStringUTFChars(jstr1, pData);
*/
}

JNIEXPORT int JNICALL Java_jp_co_garuru_GameGirlJNI_SetSDPath(JNIEnv * env, jobject obj , jstring jstr1 )
{
    const char* pData = env->GetStringUTFChars(jstr1, 0); //GetStringChars
    size_t uSize = env->GetStringLength(jstr1);

    char *pSDPath = new char[ uSize+1 ];

    gxUtil::MemCpy( pSDPath , (void*)pData , uSize );

    pSDPath[uSize] = 0x00;

    if( strcmp( pSDPath , "SD NONE" ) == 0 )
    {
        CAndroid::GetInstance()->SetSD_Path( 0 , NULL );
    } else{
        CAndroid::GetInstance()->SetSD_Path( 0 , pSDPath );
    }
    //
    delete[] pSDPath;

    return 0;
}

JNIEXPORT jbyteArray JNICALL Java_jp_co_garuru_GameGirlJNI_GetBTData( JNIEnv * env, jobject obj )
{
    //Dummy
    jbyteArray ret;
    return ret;
}


JNIEXPORT void JNICALL Java_jp_co_garuru_GameGirlJNI_SetBTData( JNIEnv * env, jobject obj , jbyteArray recvData , int size )
{
    //dummy
}

const char* _GetPackageName()
{
	//パッケージ名の取得

	static char m_pPackageName[FILENAMEBUF_LENGTH];

	JNIEnv* env;

	if( CAndroid::GetInstance()->m_pJavaVM == NULL )
	{
		return NULL;
	}

    if( CAndroid::GetInstance()->m_pJavaVM->GetEnv( (void**)&env, JNI_VERSION_1_6 ) == JNI_OK )
    {
        CAndroid::GetInstance()->m_pJavaVM->AttachCurrentThread( &env, NULL );
    } else{
        return NULL;
    }

	//javaからパッケージ名を引っ張ってくる

	jclass jc = env->FindClass( JAVA_PROGRAM_PATH );

	if (jc != 0)
	{
		jmethodID id = env->GetStaticMethodID( jc, "GetPackageName", "()Ljava/lang/String;" );

		if( id != NULL )
		{
			jstring jstr = (jstring)( env->CallStaticObjectMethod( jc, id ) );

			// パッケージ名をコピー
			const char* pcg = env->GetStringUTFChars(jstr, NULL);
			strcpy( m_pPackageName, pcg );
			env->ReleaseStringUTFChars( jstr, pcg );
			return m_pPackageName;
		}
	}

	return NULL;

}


const int _GetAvailableStorageSize()
{
	static char m_pPackageName[FILENAMEBUF_LENGTH];

	JNIEnv* env;

	if( CAndroid::GetInstance()->m_pJavaVM == NULL )
	{
		return 0;
	}

    if( CAndroid::GetInstance()->m_pJavaVM->GetEnv( (void**)&env, JNI_VERSION_1_6 ) == JNI_OK )
    {
        CAndroid::GetInstance()->m_pJavaVM->AttachCurrentThread( &env, NULL );
    }
    else
    {
        return 0;
    }

	//javaからパッケージ名を引っ張ってくる

	jclass jc = env->FindClass( JAVA_PROGRAM_PATH );

	if (jc != 0)
	{
		jmethodID id = env->GetStaticMethodID( jc, "callGetStorageAvailableSize", "()I" );

		if( id != NULL )
		{
			jint availableSize = env->CallStaticIntMethod( jc, id );

			return availableSize;
		}
	}

	return 0;

}

void CAndroid::SetClipBoardText( std::string str )
{
	m_SetClipBoardText = str;
}

gxBool IsRootAccount()
{
/*
 * JNIEnv* env = nullptr;
    jint ret = m_vm->GetEnv((void**)&env, JNI_VERSION_1_6);
    if (ret != JNI_OK) {
        return true;
    }
    //jboolean r =  env->CallStaticBooleanMethod(m_class_Module, m_method_callIsRootAccount);

    if(r == JNI_TRUE){
        return true;
    } else{
        return false;
    }
*/

}

gxChar* _GetClipBoardText()
{
    static char m_pClipBoardText[FILENAMEBUF_LENGTH];

	JNIEnv* env;

	if( CAndroid::GetInstance()->m_pJavaVM == NULL )
	{
		return NULL;
	}

    if( CAndroid::GetInstance()->m_pJavaVM->GetEnv( (void**)&env, JNI_VERSION_1_6 ) == JNI_OK )
    {
        //CAndroid::GetInstance()->m_pJavaVM->AttachCurrentThread( &env, NULL );
    } else{
        return NULL;
    }

	//javaからパッケージ名を引っ張ってくる

	jclass jc = env->FindClass( JAVA_PROGRAM_PATH );

	if (jc != 0)
	{
		jmethodID GetClipBoardText = env->GetStaticMethodID( jc, "GetClipBoardText", "()Ljava/lang/String;" );

		if( GetClipBoardText != NULL )
		{
			jstring jstr = (jstring)( env->CallStaticObjectMethod( jc, GetClipBoardText ) );

			// クリップボードからテキストをコピー
			const char* pcg = env->GetStringUTFChars(jstr, NULL);
			strcpy( m_pClipBoardText, pcg );
			env->ReleaseStringUTFChars( jstr, pcg );
			return m_pClipBoardText;
		}
	}

	return NULL;
}

void  _SetClipBoardText( std::string Str )
{
    JNIEnv* env;

	if( CAndroid::GetInstance()->m_pJavaVM == NULL )
	{
		return;
	}

    if( CAndroid::GetInstance()->m_pJavaVM->GetEnv( (void**)&env, JNI_VERSION_1_6 ) == JNI_OK )
    {
        CAndroid::GetInstance()->m_pJavaVM->AttachCurrentThread( &env, NULL );
    } else{
        return;
    }

	jclass jc = env->FindClass( JAVA_PROGRAM_PATH );

	if (jc != 0)
	{
		jmethodID SetClipBoardText = env->GetStaticMethodID(jc, "SetClipBoardText", "(Ljava/lang/String;)V");

		const char* pStr = Str.c_str();

		jstring str = env->NewStringUTF( pStr );
		env->CallStaticVoidMethod(jc, SetClipBoardText ,str );
	}
}

void CAndroid::Vibration( Sint32 sec )
{
//	//振動
//
//	//javaからパッケージ名を引っ張ってくる
//	JNIEnv *env;
//	CAndroid::GetInstance()->m_pJavaVM->AttachCurrentThread( &env, NULL );
//
//	//if( CPadManager::GetInstance()->GetMotorStat(0, 0, &fRatio) )
//	{
//		jclass jc = env->FindClass( JAVA_PROGRAM_PATH );
//
//		if (jc != 0)
//		{
//			jmethodID id = env->GetStaticMethodID(jc, "StartVibration", "(I)V");
//			env->CallStaticVoidMethod(jc, id , sec );
//		}
//	}

}

//ifdef _USE_OBB
//JNIEXPORT int JNICALL Java_jp_co_garuru_GameGirlJNI_SetOBBPath(JNIEnv * env, jobject obj , jstring jstr1 )
//{
//	const char* pData = env->GetStringUTFChars(jstr1, 0); //GetStringChars
//	size_t uSize = env->GetStringLength(jstr1);
//
//	char *pOBBPath = new char[ uSize+1 ];
//
//	gxUtil::MemCpy( pOBBPath , (void*)pData , uSize );
//
//	pOBBPath[uSize] = 0x00;
//
//	if( strcmp( pOBBPath , "OBB NONE" ) == 0 )
//	{
//		CAndroid::GetInstance()->SetOBB_Path( 0 , NULL );
//	} else{
//		CAndroid::GetInstance()->SetOBB_Path( 0 , pOBBPath );
//	}
//	//
//	delete[] pOBBPath;
//
//#if 0
//	int fh;
//	char* pBuffer=NULL;
//
//	char filepath[512];
//
////	sprintf( filepath, "%s/%s" , pData2 , "com_popup.png" );//test.txt");
//	sprintf( filepath, "%s/%s" , pOBBPath , "test.txt");
//
//	delete[] pOBBPath;
//
//	fh = open((char*)filepath,O_RDONLY);
//
//	if(fh<0)
//	{
//		//読み込みミス
//		//close(fh);
//		return 0 ;
//	}
//	else
//	{
//		pBuffer = (char*)malloc( 1024*1024*8 );
//		size_t sz = read(fh,pBuffer,1024*1024*8 );
//		close( fh );
//
//		free( pBuffer );
//		return 1;
//	}
//
//	close( fh );
//#endif
//	return 0;
//}
//#endif
