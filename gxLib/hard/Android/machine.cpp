//------------------------------------------------------------
//
// machine.cpp
// マシン固有のデバイスへのアクセスを行うものはここに書く
// ハードウェアに依存する部分
//
//------------------------------------------------------------
#include <gxLib.h>
#include <gxLib/gx.h>
#include <gxLib/gxFileManager.h>
#include <gxLib/gxDebug.h>
#include "COpenGLAndroid.h"
#include "../COpenAL.h"
#include <gxLib/gxPadManager.h>
#include <gxLib/gxNetworkManager.h>
#include <gxLib/gxSoundManager.h>
#include "../CFileSystem.h"
#include "CGamePad.h"

#include <time.h>


//save/load
#include <iostream>
#include<fstream>
#include <fcntl.h>
#include<sys/stat.h>
#include<sys/types.h>
#include <unistd.h>
#include "assetpack/assetControl.h"
#include <iostream>
#include<fstream>
/*
    #define  LOG_TAG    "gxLib_jni"
    #define  LOGD(...)  __android_log_print(ANDROID_LOG_DEBUG,LOG_TAG,__VA_ARGS__)
    #define  LOGW(...)  __android_log_print(ANDROID_LOG_WARN,LOG_TAG,__VA_ARGS__)
    #define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
    #define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)
*/
SINGLETON_DECLARE_INSTANCE( CDeviceManager );
SINGLETON_DECLARE_INSTANCE( CAndroid )

#define STORAGE_PATH_ROM  "01_rom/"
#define STORAGE_PATH_DISK "/03_disk/"
#define STORAGE_PATH_VSD  "/03_VirtualSD/"



static pthread_t test_thread;

enum {
    eLoadASSET,
    eLoadDATA,
    eLoadSD,

    eSaveDATA,
    eSaveSD,
	eSaveVSD,
};


gxBool saveFile2( const gxChar* pFileName , Uint8* pWritedBuf , Uint32 uSize , Uint32 location );
Uint8* loadFile2( const gxChar* pFileName , size_t* pLength , Uint32 location );

AAssetManager* GetAssetManager()
{
	JNIEnv* env;
	static AAssetManager *s_pAM = nullptr;

	//if( CAndroid::GetInstance()->m_pJavaVM->GetEnv( (void**)&env, JNI_VERSION_1_6 ) != JNI_OK )
	//{
	//	return NULL;
	//}

	if( s_pAM == nullptr )
	{
	    CAndroid::GetInstance()->m_pJavaVM->AttachCurrentThread( &env, NULL );

		//クラス取得
		jclass jc = env->FindClass( JAVA_PROGRAM_PATH );

		if (jc != 0)
		{
			jmethodID id = env->GetStaticMethodID( jc, "GetAssetManager", "()Ljava/lang/Object;");

			if( id != NULL )
			{
				jobject jobj = env->CallStaticObjectMethod(jc, id);

				//AAssetManager* pAM;
				s_pAM = AAssetManager_fromJava( env, jobj );

				env->DeleteLocalRef(jobj);
			}
		}
	}

	return s_pAM;
}


#define CGraphics COpenGLAndroid
CDeviceManager::CDeviceManager()
{
	//@CHttpClient::CreateInstance();
}


CDeviceManager::~CDeviceManager()
{
	//@CHttpClient::DeleteInstance();
}


void CDeviceManager::AppInit()
{
	static gxBool bFirst = gxTrue;

	if( bFirst )
	{
		bFirst = gxFalse;
		CGameGirl::GetInstance()->Init();
		CAudio::GetInstance()->Init();
        CGraphics::GetInstance()->Init();
		//@CGamePad::GetInstance()->Init();
	}
	else
	{
        CGameGirl::GetInstance()->SetResume();
	}

	//@ CGameGirl::GetInstance()->AdjustScreenResolution();

}


void CDeviceManager::GameInit()
{
	//バックで受け取った入力機器の情報をgxLib側へ更新する

	CGamePad::GetInstance()->Action();
}


gxBool CDeviceManager::GamePause()
{
	return ::GamePause();
}

gxBool CDeviceManager::GameUpdate()
{
	return gxTrue;
}


void CDeviceManager::Render()
{
	//描画処理
	
	if( CGameGirl::GetInstance()->IsResume() )
	{
		return;
	}

	CGraphics::GetInstance()->Update();
	CGraphics::GetInstance()->Render();
}


void CDeviceManager::vSync()
{
	//1/60秒の同期待ち
	static Float32 _TimeOld = CGameGirl::GetInstance()->GetTime();

	Float32 _TimeNow;
	do
	{
		_TimeNow = CGameGirl::GetInstance()->GetTime();

	}
	while( _TimeNow < ( _TimeOld + (1.0f/ FRAME_PER_SECOND) ) );

	_TimeOld = _TimeNow;

}


void CDeviceManager::Flip()
{
	//バックバッファに用意した画像を転送する

	CGraphics::GetInstance()->Present();
}

void CDeviceManager::Resume()
{
	COpenGLAndroid::DeleteInstance();
	COpenGLAndroid::GetInstance()->Init();
	gxTexManager::GetInstance()->UploadTexture(gxTrue);


/*
	CAudio::DeleteInstance();
	CAudio::GetInstance()->Init();
	gxSoundManager::GetInstance()->ResumeAllSounds();
*/
}


void CDeviceManager::Movie()
{
	
}


void CDeviceManager::Play()
{
	CAudio::GetInstance()->Action();
}


gxBool CDeviceManager::NetWork()
{
	//@CHttpClient::GetInstance()->Action();
	static gxNetworkManager::StHTTP *http = NULL;

	if( http == NULL )
	{
		http = gxNetworkManager::GetInstance()->GetNextReq();
        if( http )
        {
            CAndroid::GetInstance()->HttpRequest( http->index , http->m_URL );
        }
	}
	else
	{
		if( http->GetSeq() == gxNetworkManager::ESeq::eHTTPSeqClose )
		{
			http = NULL;
		}
		return gxFalse;
	}

	return gxTrue;
}


void CDeviceManager::UploadTexture(Sint32 sBank)
{
	CGraphics::GetInstance()->ReadTexture( sBank );
}


void CDeviceManager::LogDisp(char* pString)
{
	//__android_log_print(ANDROID_LOG_WARN , APPLICATION_NAME , "%s" , pString );
    __android_log_print(ANDROID_LOG_ERROR , APPLICATION_NAME , "%s" , pString );

}


void CDeviceManager::ToastDisp( gxChar* pMessage )
{
	//Toastテキストを表示する

    JNIEnv* env;

    if( CAndroid::GetInstance()->m_pJavaVM == NULL )
    {
       return;
    }

    CAndroid::GetInstance()->m_pJavaVM->AttachCurrentThread( &env, NULL );
    jclass jc = env->FindClass( JAVA_PROGRAM_PATH );

    if (jc != 0)
    {
        jmethodID id = env->GetStaticMethodID(jc, "SetToastText", "(Ljava/lang/String;)V");

        const char* pStr = pMessage;

        jstring str = env->NewStringUTF( pStr );
        env->CallStaticVoidMethod(jc, id ,str );
    }
}


void CDeviceManager::OpenWebClient( gxChar* pString , gxBool bOpenBrowser )
{
    //WebViewを表示するリクエストを発行する
	//iOSでは文字列の空判定を行って何かURLが入っていれば自動的に処理を行う

    CAndroid::GetInstance()->m_WebViewURL = pString;
	CAndroid::GetInstance()->m_bOpenWebBrowser = bOpenBrowser;
}


gxBool CDeviceManager::SetAchievement( Uint32 achieveindex )
{
	gxLib::SetToast("Achievement (%d)" , achieveindex );

	return gxTrue;
}

gxBool CDeviceManager::GetAchievement( Uint32 achieveindex )
{
	return gxTrue;
}

Uint8* CDeviceManager::LoadFile( const gxChar* pFileName , size_t* pLength , ESTORAGE_LOCATION location )
{
	//assetからデータを読み出す(read only)

	Uint32 len = strlen(pFileName);

	gxChar *fileName0 = new gxChar[ 32+len ];

	len = sprintf( fileName0 , "%s" , pFileName );

	for( int ii=0; ii<len; ii++ )
	{
		if( fileName0[ii] == '\\' )
		{
			fileName0[ii] = '/';
		}
	}

	Uint8 *pData = nullptr;

	if( location == ESTORAGE_LOCATION::ROM )
	{
		//loadFile

		//asset "01_rom"
		pData = loadFile2(fileName0 , pLength , eLoadASSET );
	}
	else if( location == ESTORAGE_LOCATION::INTERNAL )
	{
		//loadStorageFile

		//"03_disk"
		pData = loadFile2(fileName0 , pLength , eLoadDATA );

	}
	else if( location == ESTORAGE_LOCATION::EXTERNAL )
	{
		//04_sd
		pData = loadFile2(fileName0 , pLength , eLoadSD );
	}

	SAFE_DELETES( fileName0 );

	return pData;
}



gxBool CDeviceManager::SaveFile( const gxChar* pFileNameU8 , Uint8* pWritedBuf , size_t uSize , ESTORAGE_LOCATION uLocation )
{
    switch( uLocation ){
        case ESTORAGE_LOCATION::ROM:
        	//保存不能
            return gxFalse;

        case ESTORAGE_LOCATION::INTERNAL:
			//ユーザーに見えない領域に
 			return saveFile2( pFileNameU8 , pWritedBuf , uSize , eSaveDATA );


        case ESTORAGE_LOCATION::EXTERNAL:
			//外部SD領域に
        	if( saveFile2( pFileNameU8 , pWritedBuf , uSize , eSaveSD ))
			{
				return gxTrue;
			}
			//保存失敗したらテンポラリに
			saveFile2( pFileNameU8 , pWritedBuf , uSize , eSaveVSD );
        	return gxFalse;

        default:
            break;
    }

	return gxTrue;
}


void CDeviceManager::MakeThread( void* (*pFunc)(void*) , void * pArg )
{
	pthread_create( &test_thread, NULL, pFunc, (void *)pArg);

}


void CDeviceManager::Sleep( Uint32 msec )
{
	struct timespec	 req, res;
	req.tv_sec  = 0;
	req.tv_nsec = msec * 1000000;

	nanosleep(&req, &res);
}


gxBool CDeviceManager::PadConfig()
{
	//非対応
	return gxTrue;
}


gxBool CDeviceManager::PadConfig( Sint32 padNo , Uint32 button )
{
	return gxTrue;
}


std::vector<std::string> CDeviceManager::GetDirList( std::string rootDirUTF8 )
{
	//U8をSJISに変換
	//※Zipからの展開だとZipに内包されたファイル名がSJISでわたってくる可能性があるが
	//UTF8で取り回す仕様で固定する

	return CFileSystem::ScanDirectory(rootDirUTF8);
}


Float32 CDeviceManager::GetFreeStorageSize()
{
	CAndroid::GetInstance()->m_bGetAvailableSize.store( gxTrue );

	while( CAndroid::GetInstance()->m_bGetAvailableSize.load() )
	{
		gxLib::Sleep(1);
	}

	size_t size = CAndroid::GetInstance()->m_AvailableSize.load();

	size = size / 1024;
	return Float32( size / 1024.0f );
}


Uint8* loadFile2( const gxChar* pFileName , size_t* pLength , Uint32 location )
{
	gxChar fileName[1024];

	switch( location ){

	case eLoadASSET:
        {
            // data/data/jp.co.garuru/01_rom/fileName
            sprintf( fileName, STORAGE_PATH_ROM );
            strcat ( fileName, pFileName );

			//AAB対応
			uint8_t *pData = nullptr;
			size_t uSize = 0;
			pData = AssetController::GetInstance()->LoadAABFile( fileName , &uSize );
	        *pLength = uSize;
	        return pData;
        }
        break;

	case eLoadDATA:
        {
            // data/data/jp.co.garuru/03_disk/fileName
            //sprintf( fileName, "03_disk/" );
            //strcat ( fileName, pFileName );
			sprintf( fileName, "/data/data/" );
			strcat ( fileName, CAndroid::GetInstance()->GetPackageName() );
			strcat ( fileName, STORAGE_PATH_DISK );
			strcat ( fileName, pFileName );
        }
        break;

	case eLoadSD:		//				//読み書き、取り出しOK
		{
			// external SD / internal SD
			char filepath[512];
		    sprintf( filepath, "%s/",CAndroid::GetInstance()->GetSD_Path(0) );
			sprintf( fileName, "%s%s"  , filepath,pFileName );
		}
		break;

	default:
		break;
	}

	std::vector<uint8_t> buf;

	if (CFileSystem::ReadFile(fileName, buf))
	{
		if (buf.size())
		{
			uint8_t* p = new Uint8[buf.size() + 1];
			gxUtil::MemCpy(p, &buf[0], buf.size());
			p[buf.size()] = 0x00;

			if (pLength)
			{
				*pLength = buf.size();
			}
			return p;
		}
	}

	return nullptr;
}


std::string CDeviceManager::GetClipBoardString()
{
	CAndroid::GetInstance()->m_bGetClipBoardText.store( gxTrue );

	while( CAndroid::GetInstance()->m_bGetClipBoardText.load() )
	{
		gxLib::Sleep(1);
	}

	return CAndroid::GetInstance()->m_GetClipBoardText;
}

void CDeviceManager::SetClipBoardString(std::string str)
{
	CAndroid::GetInstance()->SetClipBoardText( str );
}


//-----------------------------------------
//専用
//-----------------------------------------


gxBool saveFile2( const gxChar* pFileName , Uint8* pWritedBuf , Uint32 uSize , Uint32 location )
{
    char filepath[512];

    switch( location ){

        case eSaveDATA:
            //SaveStorageFile
            sprintf( filepath, "/data/data/" );
            strcat( filepath, CAndroid::GetInstance()->GetPackageName() );
            strcat( filepath, STORAGE_PATH_DISK );
            strcat( filepath, pFileName );
            break;


        case eSaveSD:
            //SaveFile://SD
            sprintf( filepath, "%s/",CAndroid::GetInstance()->GetSD_Path(0) );
            strcat( filepath, pFileName );
            break;

		case eSaveVSD:
			//SaveVirtualSD
			sprintf( filepath, "/data/data/" );
			strcat( filepath, CAndroid::GetInstance()->GetPackageName() );
			strcat( filepath, STORAGE_PATH_VSD );
			strcat( filepath, pFileName );
			break;

        default:
            break;
    }

	if (pWritedBuf == nullptr)
	{
		//ファイル削除

		if( !CFileSystem::Remove( filepath ) )
		{
			return false;
		}

		return true;
	}

	if (CFileSystem::WriteFile(filepath, pWritedBuf, uSize))
	{
		//SJISで書き出し成功
		return gxTrue;
	}

    return gxFalse;
}

