﻿#ifndef _COpenGLAndroid_H_
#define _COpenGLAndroid_H_

#include <gxLib/hard/COpenGLES3.h>
#include <gxLib/gxTexManager.h>


class COpenGLAndroid : public COpenGLES3
{
public:

	COpenGLAndroid();
	~COpenGLAndroid();

	void Init();
	void Reset();
	void SwapBuffer();
	void GetFrameBufferImage(gxChar* pLength);

	SINGLETON_DECLARE( COpenGLAndroid );

private:

	EGLDisplay display;
	EGLSurface surface;
	EGLNativeWindowType win;
	EGLContext eglContext;
};

#endif

