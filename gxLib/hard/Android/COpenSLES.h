﻿#pragma once

#include <gxLib.h>
#include <SLES/OpenSLES.h>
#include <SLES/OpenSLES_Android.h>

class COpenSLES {
public:
	 COpenSLES();
	 ~COpenSLES();

	void Init();
	void Action();

	gxBool LoadSound( Sint32 index , gxChar *pFileName );
	gxBool PlaySound( Uint32 uIndex , gxBool bLoop = gxFalse , gxBool bOverWrap = gxFalse , Float32 fVolume = 1.0f );
	gxBool StopSound( Uint32 uIndex );

	gxBool __ReadSound( Sint32 index );
	gxBool ReadSound( Sint32 index );
	gxBool PauseSound( Sint32 index );
	gxBool SetVolume( Uint32 uIndex );
	gxBool SetFrequency(Sint32 index);

	 struct Channel {
		SLboolean loaded;
		SLObjectItf playerObject;
		SLPlayItf playerPlay;
		SLSeekItf playerSeek;
		SLVolumeItf playerVolume;
		SLPitchItf  playerPitch;
		SLAndroidSimpleBufferQueueItf PlayerBufferQueue;
//		SLAndroidBufferQueueItf PlayerBufferQueue;
	 };

	 Channel* m_channels = nullptr;

	SINGLETON_DECLARE( COpenSLES );

private:

	gxBool isRunning();
	void finish();

	gxBool init( Sint32 channelCount );
	gxBool loadFromAsset( char* fname, Sint32 index );
	gxBool setChannelState( Sint32 index, SLuint32 state );
	SLuint32 getChannelState( Sint32 index );
	void closeChannel( Sint32 index );
	int getChannelCount();

	SLmillibel getChannelVolume( Sint32 index );
	SLmillibel setChannelVolume( Sint32 index, SLmillibel volumeLevel );
	SLmillibel getChannelMaxVolume( Sint32 index );

	gxBool seekChannel( Sint32 index, Sint32 pos, SLuint32 seekMode );
	gxBool playChannel( Sint32 index );
	gxBool resumeChannel( Sint32 index );

	gxBool isLoaded( Sint32 channelIndex );

	gxBool getChannelLooping( Sint32 index );
	gxBool setChannelLooping( Sint32 index, gxBool enable );

	SLresult checkError( char* message, SLresult result );

	gxBool m_running;

	Sint32 m_channelCount;


	 SLObjectItf m_engineObject;
	 SLEngineItf m_engineEngine;
	 SLObjectItf m_outputMixObject;
};