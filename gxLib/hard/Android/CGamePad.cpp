﻿#include <jni.h>
#include <android/log.h>

#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <gxLib.h>
#include <gxLib/gx.h>
#include <gxLib/gxPadManager.h>

#include "CGamePad.h"


SINGLETON_DECLARE_INSTANCE( CGamePad );

void CGamePad::Action()
{
	//Sint32 ii = 0;

	Sint32 gamew,gameh;
	Sint32 scrw,scrh;
	Float32 ofx,ofy;

	CGameGirl::GetInstance()->GetGameResolution( &gamew , &gameh );
	CGameGirl::GetInstance()->GetWindowsResolution( &scrw , &scrh );

	ofx = (scrw - gamew) / 2.0f;
	ofy = (scrh - gameh) / 2.0f;

	for( Sint32 ii=0; ii<gxPadManager::enTouchMax; ii++ )
	{
		Sint32 mx,my;

		mx = m_Cursor[ii].mx;
		my = m_Cursor[ii].my;

		mx -= ofx;
		my -= ofy;

		mx = WINDOW_W * mx / gamew;
		my = WINDOW_H * my / gameh;

		if( m_Cursor[ii].m_bPush )
		{
			gxPadManager::GetInstance()->SetMouseButtonDown( ii );

			//if( ii == 0 )
			{
				//タッチ１のみマウス座標の移動に割り当てる
				gxPadManager::GetInstance()->SetMousePosition( ii , mx , my );
			}

			gxPadManager::GetInstance()->SetTouchInfo( ii , gxTrue , mx , my );
		}
		else
		{
			if( ii == 0 ||  ii == 1 || ii == 2 )
			{
				//タッチ１、タッチ２のみマウスLRのクリックに割り当てる（２本指のときマウスR相当になる）
				gxPadManager::GetInstance()->SetMouseButtonUp( ii );
			}

			gxPadManager::GetInstance()->SetTouchInfo( ii , gxFalse , mx , my );
		}

	}

	for( Sint32 ii=0; ii<256; ii++ )
	{
		if( m_KeyBoardPush[ii] == 0x01 )
		{
			gxPadManager::GetInstance()->SetKeyDown(ii);
		}
		if( m_KeyBoardPush[ii] == 0x02 )
		{
			gxPadManager::GetInstance()->SetKeyUp(ii);
		}

		//m_KeyBoardPush[ii] = 0x00;
	}

	adjustDefaultInput();

	//センサー類
	gxPadManager::StSensor sensor;

	sensor.gyro.x   = m_fCensors[0];
	sensor.gyro.y   = m_fCensors[1];
	sensor.gyro.z   = m_fCensors[2];
	sensor.accel.x  = m_fCensors[3];
	sensor.accel.y  = m_fCensors[4];
	sensor.accel.z  = m_fCensors[5];
	sensor.magne.x  = m_fCensors[6];
	sensor.magne.y  = m_fCensors[7];
	sensor.magne.z  = m_fCensors[8];
	sensor.orient.x = m_fCensors[9];
	sensor.orient.y = m_fCensors[10];
	sensor.orient.z = m_fCensors[11];
	gxPadManager::GetInstance()->SetSensorInfo( &sensor );

/*
    gxPadManager::GetInstance()->SetSensorInfo( gxPadManager::enSensorTypeGyro   ,&m_fCensors[0] );
    gxPadManager::GetInstance()->SetSensorInfo( gxPadManager::enSensorTypeAccel  ,&m_fCensors[3] );
    gxPadManager::GetInstance()->SetSensorInfo( gxPadManager::enSensorTypeMagne  ,&m_fCensors[6] );
	gxPadManager::GetInstance()->SetSensorInfo( gxPadManager::enSensorTypeOrient ,&m_fCensors[9] );
*/

}

Sint32 CGamePad::ConvertKeyNumber( Sint32 id ,Sint32 *pKey , Sint32 *pPad )
{
	Sint32 key = 0;
	Sint32 pad = 0;

	switch(id){
	case 111:	key=gxKey::ESC			;break;//KEYCODE_ESCAPE
//	case 4  :	key=gxKey::BS			;break;//KEYCODE_BACK   //バックキー
    case 4  :	key=gxKey::BS			;break;//KEYCODE_BACK   //バックキーはデバッグキーにしてみる
	case 61 :	key=gxKey::TAB			;break;//KEYCODE_TAB
	case 66 :	key=gxKey::RETURN		;break;//KEYCODE_ENTER
	case 59 :	key=gxKey::SHIFT		;break;//KEYCODE_SHIFT_LEFT
	case 60 :	key=gxKey::RSHIFT		;break;//KEYCODE_SHIFT_RIGHT
	case 113:	key=gxKey::CTRL			;break;//KEYCODE_CTRL_LEFT
	case 114:	key=gxKey::RCTRL	    ;break;//KEYCODE_CTRL_RIGHT
	case 57 :	key=gxKey::ALT		 	;break;//KEYCODE_ALT_LEFT
	case 58 :	key=gxKey::RALT			;break;//KEYCODE_ALT_RIGHT
	case 19 :	key=gxKey::UP			;break;//KEYCODE_DPAD_UP
	case 20 :	key=gxKey::DOWN  		;break;//KEYCODE_DPAD_DOWN
	case 21 :	key=gxKey::LEFT  		;break;//KEYCODE_DPAD_LEFT
	case 22 :	key=gxKey::RIGHT 		;break;//KEYCODE_DPAD_RIGHT
	case 62 :	key=gxKey::SPACE	   	;break;//KEYCODE_SPACE
	//case 66 :	key=gxKey::ENTER	   	;break;//KEYCODE_ENTER
	case 131:	key=gxKey::F1		  	;break;//KEYCODE_F1
	case 132:	key=gxKey::F2		  	;break;//
	case 133:	key=gxKey::F3		  	;break;//
	case 134:	key=gxKey::F4		  	;break;//
	case 135:	key=gxKey::F5		  	;break;//
	case 136:	key=gxKey::F6		  	;break;//
	case 137:	key=gxKey::F7		  	;break;//
	case 138:	key=gxKey::F8		  	;break;//
	case 139:	key=gxKey::F9		  	;break;//
	case 140:	key=gxKey::F10		 	;break;//
	case 141:	key=gxKey::F11		 	;break;//
	case 142:	key=gxKey::F12		 	;break;// KEYCODE_F12
	case 144:	key=gxKey::NUM0		  	;break;// KEYCODE_NUMPAD_0
	case 145:	key=gxKey::NUM1		  	;break;//
	case 146:	key=gxKey::NUM2		  	;break;//
	case 147:	key=gxKey::NUM3		  	;break;//
	case 148:	key=gxKey::NUM4		  	;break;//
	case 149:	key=gxKey::NUM5		  	;break;//
	case 150:	key=gxKey::NUM6		  	;break;//
	case 151:	key=gxKey::NUM7		  	;break;//
	case 152:	key=gxKey::NUM8		  	;break;//
	case 153:	key=gxKey::NUM9		  	;break;//
	case 92 :	key=gxKey::PAGEUP	  	;break;// KEYCODE_PAGE_UP
	case 93 :	key=gxKey::PAGEDOWN		;break;// KEYCODE_PAGE_DOWN
	case 67 :	key=gxKey::BS	  	;break;// KEYCODE_BS
	case 112 :	key=gxKey::DEL	  	;break;// KEYCODE_FORWARD DEL
	case 124:	key=gxKey::INS	  	;break;// KEYCODE_INSERT
	case 3  :	key=gxKey::HOME		  	;break;// KEYCODE_HOME
	case 123:	key=gxKey::END		  	;break;// KEYCODE_MOVE_END
	case 7  :	key=gxKey::NUMPAD0		   	;break;// KEYCODE_0
	case 8  :	key=gxKey::NUMPAD1		   	;break;//
	case 9  :	key=gxKey::NUMPAD2		   	;break;//
	case 10 :	key=gxKey::NUMPAD3		   	;break;//
	case 11 :	key=gxKey::NUMPAD4		   	;break;//
	case 12 :	key=gxKey::NUMPAD5		   	;break;//
	case 13 :	key=gxKey::NUMPAD6		   	;break;//
	case 14 :	key=gxKey::NUMPAD7		   	;break;//
	case 15 :	key=gxKey::NUMPAD8		   	;break;//
	case 16 :	key=gxKey::NUMPAD9		   	;break;//  KEYCODE_9
	case 29 :	key=gxKey::A		   	;break;//  KEYCODE_A
	case 30 :	key=gxKey::B		   	;break;//
	case 31 :	key=gxKey::C		   	;break;//
	case 32 :	key=gxKey::D		   	;break;//
	case 33 :	key=gxKey::E		   	;break;//
	case 34 :	key=gxKey::F		   	;break;//
	case 35 :	key=gxKey::G		   	;break;//
	case 36 :	key=gxKey::H		   	;break;//
	case 37 :	key=gxKey::I		   	;break;//
	case 38 :	key=gxKey::J		   	;break;//
	case 39 :	key=gxKey::K		   	;break;//
	case 40 :	key=gxKey::L		   	;break;//
	case 41 :	key=gxKey::M		   	;break;//
	case 42 :	key=gxKey::N		   	;break;//
	case 43 :	key=gxKey::O		   	;break;//
	case 44 :	key=gxKey::P		   	;break;//
	case 45 :	key=gxKey::Q		   	;break;//
	case 46 :	key=gxKey::R		   	;break;//
	case 47 :	key=gxKey::S		   	;break;//
	case 48 :	key=gxKey::T		   	;break;//
	case 49 :	key=gxKey::U		   	;break;//
	case 50 :	key=gxKey::V		   	;break;//
	case 51 :	key=gxKey::W		   	;break;//
	case 52 :	key=gxKey::X		   	;break;//
	case 53 :	key=gxKey::Y		   	;break;//
	case 54 :	key=gxKey::Z		   	;break;//  KEYCODE_Z
		break;
	case 24:	//KEYCODE_VOLUME_UP:
		break;
	case 25:	//KEYCODE_VOLUME_DOWN:
		break;
	case 164:   //KEYCODE_VOLUME_MUTE
			gxLib::SetAudioMasterVolume(0.25f);
			break;
	case 96 :	pad=BTN_A;		break;	//KEYCODE_BUTTON_A;
	case 97 :	pad=BTN_B;		break;	//KEYCODE_BUTTON_B;
	case 98 :	pad=BTN_C;		break;	//KEYCODE_BUTTON_C;
	case 99 :	pad=BTN_X;		break;	//KEYCODE_BUTTON_X;
	case 100:	pad=BTN_Y;		break;	//KEYCODE_BUTTON_Y;
	case 101:	pad=BTN_Z;		break;	//KEYCODE_BUTTON_Z;
	case 102:	pad=BTN_L1;		break;	//KEYCODE_BUTTON_L1	   = ;
	case 103:	pad=BTN_R1;		break;	//KEYCODE_BUTTON_R1	   = ;
	case 104:	pad=BTN_L2;		break;	//KEYCODE_BUTTON_L2	   = ;
	case 105:	pad=BTN_R2;		break;	//KEYCODE_BUTTON_R2	   = ;
	case 108:	pad=BTN_START;	break;	//KEYCODE_BUTTON_START	= ;
	case 109:	pad=BTN_SELECT;	break;	//KEYCODE_BUTTON_SELECT   = ;
	case 106:	pad=BTN_L3;		break;	//KEYCODE_BUTTON_START	= ;
	case 107:	pad=BTN_R3;		break;	//KEYCODE_BUTTON_SELECT   = ;
    case 110:	pad=BTN_PS;		break;	//KEYCODE_BUTTON_SELECT   = ;
	default:
		break;
	}

	*pKey = key;
	*pPad = pad;

	if( key ) return 1;
	if( pad ) return 2;

	return 0;
}


void CGamePad::adjustDefaultInput()
{
	//Float32 padAnalog[6] ={0};

	for( Sint32 ii=0; ii<PLAYER_MAX; ii++)
	{
		int n = 0;
		Uint32 padButtonPush = 0x00;
		gxPadManager::StAnalog padAnalog = {0};

		if(ii < m_PlayerID.size())
		{
			n = m_PlayerID[ii];
			padButtonPush = m_PadPush[n];
			if( padButtonPush)
			{
				padButtonPush = padButtonPush;
			}
		}

		gxPadManager::GetInstance()->SetPadInfo(ii, padButtonPush);

		//analog情報を更新

		if (m_fAnalog.find(n) != m_fAnalog.end())
		{
			auto analog = &m_fAnalog[n];

			padAnalog.left.x = analog->left.x;    //0-ax
			padAnalog.left.y = analog->left.y;    //1-ay

			padAnalog.right.x = analog->right.x; //2-az
			padAnalog.right.y = analog->right.y; //5-rz

			padAnalog.leftTrigger = analog->leftTrigger;//6
			padAnalog.rightTrigger = analog->rightTrigger;//7
		}

		gxPadManager::GetInstance()->SetAnalogInfo(ii, &padAnalog);
	}

//	gxPadManager::GetInstance()->SetAnalogInfo( 1 , &padAnalog[1] );
//	gxPadManager::GetInstance()->SetAnalogInfo( 0 , padAnalog );

}



void CGamePad::InputCursorCheck( Sint32 id , gxBool bPush , Sint32 px , Sint32 py )
{
	CGamePad::GetInstance()->m_Cursor[id].m_bPush = bPush;

	CGamePad::GetInstance()->m_Cursor[id].mx = px;
	CGamePad::GetInstance()->m_Cursor[id].my = py;

}


void CGamePad::CheckAccellarator( Sint32 id , gxBool bPush )
{
	Sint32 key = enID_NoneAccel;
	switch( id ){
	case gxKey::F1:
		key = enID_GamePause;
		break;
	case gxKey::F2:
		key = enID_SoundSwitch;
		break;
	case gxKey::F3:
		key = enID_GameStep;
		break;
	case gxKey::F4:
		key = enID_PadEnableSwitch;
		break;
	case gxKey::F5:
		key = enID_ChangeFullScreenMode;
		break;
	case gxKey::F6:
		key = enID_SamplingFilter;
		break;
	case gxKey::F7:
		key = enID_Reset;
		break;
	case gxKey::F8:
		key = enID_SwitchVirtualPad;
		break;
	case gxKey::F9:
		key = enID_DebugMode;
		break;
	case gxKey::F11:
		key = enID_FullSpeed;
		break;
	case gxKey::F12:
		key = enID_ScreenShot;
		break;
	case gxKey::PAGEUP:
		key = enID_MasterVolumeAdd;
		break;
	case gxKey::PAGEDOWN:
		key = enID_MasterVolumeSub;
		break;

	case gxKey::NUM3:
		key = enID_Switch3DView;
		break;

	case gxKey::ESC:
//		if (CWindows::GetInstance()->IsFullScreen())
//		{
//			key = enID_ChangeFullScreenMode;
//		}
//		else
//		{
//			key = enID_AppExit;
//		}
		break;
	default:
		break;

	}

	if( key == enID_NoneAccel ) return;

	if( bPush )
	{
		m_Accellarator[ key ] ++;
		if( m_Accellarator[ key ] != 1 ) return;
	}
	else
	{
		m_Accellarator[ key ] = 0;
		return;
	}

	switch( key ){
	case enID_ChangeFullScreenMode:	//フルスクリーン切り替え
//		if (CWindows::GetInstance()->IsFullScreen())
//		{
//			CWindows::GetInstance()->SetFullScreen(gxFalse);
//			auto av = Windows::UI::ViewManagement::ApplicationView::GetForCurrentView();
//			av->ExitFullScreenMode();
//			av->FullScreenSystemOverlayMode = Windows::UI::ViewManagement::FullScreenSystemOverlayMode::Standard;
//		}
//		else
//		{
//			CWindows::GetInstance()->SetFullScreen(gxTrue);
//			auto av = Windows::UI::ViewManagement::ApplicationView::GetForCurrentView();
//			if (av->TryEnterFullScreenMode())
//			{
//				av->FullScreenSystemOverlayMode = Windows::UI::ViewManagement::FullScreenSystemOverlayMode::Minimal;
//			}
//		}
		break;
/*
	case IDM_WINDOW_ORIGINAL:
		CWindows::GetInstance()->SetScreenMode( CWindows::enScreenModeOriginal );
		CDeviceManager::GetInstance()->AdjustScreenResolution();
		break;

	case IDM_WINDOW_ASPECT:
		CWindows::GetInstance()->SetScreenMode( CWindows:: enScreenModeAspect );
		CDeviceManager::GetInstance()->AdjustScreenResolution();
		break;

	case IDM_WINDOW_STRETCH:
		CWindows::GetInstance()->SetScreenMode( CWindows::enScreenModeFull );
		CDeviceManager::GetInstance()->AdjustScreenResolution();
		break;
*/
	case enID_GamePause			 : 	//ゲームのポーズ
		CGameGirl::GetInstance()->SetPause( !CGameGirl::GetInstance()->IsPause() );
		break;

	case enID_GameStep			 : 	//ゲームのステップ
		if( CGameGirl::GetInstance()->IsPause() )
		{
			CGameGirl::GetInstance()->PauseStep();
		}
		else
		{
			CGameGirl::GetInstance()->SetPause(gxTrue);
		}
		break;

	case enID_PadEnableSwitch	 : 	//コントローラー切り替え
		gxPadManager::GetInstance()->SetEnableController( !gxPadManager::GetInstance()->IsEnableController() );
		break;

	case enID_DebugMode			 : 	//デバッグモードの切り替え
		CGameGirl::GetInstance()->ToggleDeviceMode();
		break;

	case enID_SamplingFilter:
		//サンプリングフィルター切り替え
//		if( CWindows::GetInstance()->GetRenderingFilter() == CWindows::enSamplingNearest )
//		{
//			CWindows::GetInstance()->SetRenderingFilter( CWindows::enSamplingBiLenear );
//		}
//		else
//		{
//			CWindows::GetInstance()->SetRenderingFilter( CWindows::enSamplingNearest );
//		}
		break;

	case enID_FullSpeed			 : 	//フルスピード
		CGameGirl::GetInstance()->WaitVSync( !CGameGirl::GetInstance()->IsWaitVSync() );
		break;

	case enID_SoundSwitch		 : 	//サウンドON/OFF
//		if( CWindows::GetInstance()->IsSoundEnable() )
//		{
//			CWindows::GetInstance()->SetSoundEnable(gxFalse);
//		}
//		else
//		{
//			CWindows::GetInstance()->SetSoundEnable(gxTrue);
//		}
		break;

	case enID_MasterVolumeAdd:
		//Volume ++
		{
			Float32 fVol = gxLib::GetAudioMasterVolume();
			gxLib::SetAudioMasterVolume(fVol += 0.1f);
		}
		break;

	case enID_MasterVolumeSub:
		//Volume --
		{
			Float32 fVol = gxLib::GetAudioMasterVolume();
			gxLib::SetAudioMasterVolume( fVol -= 0.1f );
		}
		break;

	case enID_SwitchVirtualPad:
//		{
//			CWindows::GetInstance()->m_bVirtualPad = !CWindows::GetInstance()->m_bVirtualPad;
//			gxLib::SetVirtualPad(CWindows::GetInstance()->m_bVirtualPad );
//		}
		break;
	case enID_Switch3DView:
		CGameGirl::GetInstance()->Set3DView( !CGameGirl::GetInstance()->Is3DView() );
		break;

	case enID_AppExit             :	//アプリ終了
		CGameGirl::GetInstance()->Exit();
        break;
	}
}

void CGamePad::SetCensorData(Float32 *pData )
{
	for( Sint32 ii=0; ii<12; ii++ )
	{
		m_fCensors[ii] = pData[ii];
	}
}


void CGamePad::InputKeyCheck( Sint32 id, Sint32 push , Sint32 keyCode )
{
	Sint32 key,pad;

	key = -1;
	pad = -1;

	if( !ConvertKeyNumber( keyCode , &key , &pad ) )
	{
		return;
	}

	if( key > 0)
	{
		if( push == 0x01 )
		{
			m_KeyBoardPush[ key ] = 0x01;
			CheckAccellarator( key , gxTrue );
		}
		else if( push == 0x02 )
		{
			m_KeyBoardPush[ key ] = 0x02;
			CheckAccellarator( key , gxFalse );
		}
	}

	if( pad > 0)
	{
		if( push == 0x01 )
		{
			m_PadPush[id] |= pad;
		}
		else if( push == 0x02 )
		{
			m_PadPush[id] &= ~pad;
		}
	}

}

void CGamePad::SetAnalog(Sint32 id, Float32 *pAnalogInfo)
{
	addController(id);

	m_fAnalog[id].left.x = pAnalogInfo[0];    //ax
	m_fAnalog[id].left.y = pAnalogInfo[1];    //ay
	m_fAnalog[id].right.x = pAnalogInfo[2];    //ax
	m_fAnalog[id].right.y = pAnalogInfo[5];    //ax
	m_fAnalog[id].leftTrigger = pAnalogInfo[6];    //ax
	m_fAnalog[id].rightTrigger = pAnalogInfo[7];    //ax
}
