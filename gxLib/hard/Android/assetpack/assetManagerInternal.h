#ifndef assetManagerInternal_h
#define assetManagerInternal_h

#define MAX_ASSET_PATH_LENGTH 512

struct AssetPackInfo {

public:

	AssetPackInfo()
	{
		mAssetPackBasePath = NULL;
		mAssetPackStatus = GAMEASSET_NOT_FOUND;
		mAssetPackDownloadSize = 0;
		mAssetPackCompletion = 0.0f;
	}

	~AssetPackInfo()
	{
	}

	GameAssetStatus mAssetPackStatus;
	const char *mAssetPackBasePath;

	GameAssetPackType mPackType;
	const char *mPackName;

	float mAssetPackCompletion;
	size_t mAssetPackDownloadSize;

};

struct RequestInfo {
	int ope = 0;
	std::string assetPackName = "";
};

class assetManagerInternal {
public:
	assetManagerInternal(AAssetManager *assetManager, JavaVM *jvm, jobject nativeActivity);

	~assetManagerInternal();

	uint8_t* loadExternal(const char *assetName, size_t *bufferSize);
	uint8_t* loadInternal(const char *assetName, size_t *bufferSize);

	AssetPackInfo *GetAssetPack(const int index)
	{
		return index < mAssetPacks.size() ? mAssetPacks[index] : NULL;
	}

	AssetPackInfo *GetAssetPackByName(const char *assetPackName);

	int GetAssetPackCount() const
	{
		return mAssetPacks.size();
	}

	const char *GetAssetPackErrorMessage() const
	{
		return mAssetPackErrorMessage;
	}

	//----------------------------------------------------------------------------------------
	//アセットパックマネージャで追加された関数群
	// Asset Pack Manager support functions below
	//----------------------------------------------------------------------------------------
	bool GetAssetPackManagerInitialized() const { return mAssetPackManagerInitialized; }

	// Requests
	bool RequestAssetPackDownload(const char *assetPackName);

	void RequestAssetPackCancelDownload(const char *assetPackName);

	bool RequestAssetPackRemoval(const char *assetPackName);

	void RequestMobileDataDownloads();

	void UpdateAssetPackFromDownloadState(AssetPackInfo *assetPackInfo, AssetPackDownloadState *downloadState);

	void UpdateMobileDataRequestStatus();

	//自分で検出したエラーを保存しておく関数
	void setAssetPackErrorStatus(const AssetPackErrorCode assetPackErrorCode, AssetPackInfo *assetPackInfo, const char *message);

	bool generateFullAssetPath(const char *assetName, const AssetPackInfo *packInfo, char *pathBuffer, const size_t bufferSize);

private:

	// Update processing
	void updateAssetPackBecameAvailable(AssetPackInfo *assetPackInfo);

	void changeAssetPackStatus(AssetPackInfo *packInfo, const GameAssetStatus newStatus)
	{
		//アセットパックの状態を変更する
		if (packInfo->mAssetPackStatus != newStatus)
		{
			packInfo->mAssetPackStatus = newStatus;
		}
	}

	AAssetManager *mAssetManager;
	jobject mNativeActivity;

	std::vector<AssetPackInfo *> mAssetPacks;
	const char *mAssetPackErrorMessage;
	bool mRequestingMobileDownload;
	bool mAssetPackManagerInitialized;	//Fast-Follow、On-demandでは初期化が必要になる
};


#endif
