/*
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef nativegamepad_gameassetmanager_hpp
#define nativegamepad_gameassetmanager_hpp

//#include <jni.h>
//#include <stddef.h>
//#include "util.hpp"

// Internal implementation utility class
class GameAssetManagerInternals;
struct AAssetManager;

static const char *INSTALL_ASSETPACK_NAME    = "Pack01_Install";
static const char *FASTFOLLOW_ASSETPACK_NAME = "Pack10_FastFollow";
static const char *ONDEMAND_ASSETPACK_NAME   = "Pack20_OnDemand";

class GameAssetManager {
public:

    GameAssetManager(AAssetManager *assetManager, JavaVM *jvm, jobject android_context);

    ~GameAssetManager();

    // Event handlers for pause and resume events
    void OnPause();

    void OnResume();

    // Call once every game frame to update internal asset states
    void UpdateGameAssetManager();
    void ProcUpdateRequest();

    // If an asset was set to the GAMEASSET_ERROR status, this will return
    // an error message describing the reason for the error
    const char *GetGameAssetErrorMessage();

    // Return the name of the asset pack that contains the specified asset file,
    // returns NULL if no parent pack could be found.
    const char *GetGameAssetParentPackName(const char *assetName);

    // If the status of the asset is GAMEASSET_READY, return the size in bytes
    // If the status anything else, 0 will be returned
    uint64_t GetGameAssetSize(const char *assetName);

    // If the status of the asset is GAMEASSET_READY, the file data into the specified buffer,
    // returns true if successful
    //bool LoadGameAsset(const char *assetName, const size_t bufferSize, void *loadBuffer);
	uint8_t* LoadFile(const char *assetName, size_t* bufferSize );

    // Returns an array of filenames of files present in the specified asset pack,
    // returns NULL if the asset pack name was not found
    //const char **GetGameAssetPackFileList(const char *assetPackName, int *fileListSize);

    // Returns the status of an asset pack
    GameAssetStatus GetGameAssetPackStatus(const char *assetPackName);

    // Returns the type of an asset pack
    GameAssetPackType GetGameAssetPackType(const char *assetPackName);

    // Request permission to download large asset packs over a mobile data connection
    void RequestMobileDataDownloads();

    // Starts the download process for the specified asset pack, only
    // fast-follow and on-demand packs can be downloaded,
    // returns true if the download begins successfully
    bool RequestDownload(const char *assetPackName);

    // Requests a cancellation of the download process for the specified asset pack,
    // is only a request, not a guarantee, will have no impact on an asset pack with
    // a status other than GAMEASSET_DOWNLOADING
    void RequestDownloadCancellation(const char *assetPackName);

    // Requests removal  specified asset pack, only fast-follow and on-demand packs
    // can be removed, removal is a request and not guaranteed. Returns false if an
    // error was reported
    bool RequestRemoval(const char *assetPackName);

    // Returns the status of an asset pack file, including progress
    // information if the asset pack file is in the process of being downloaded
    GameAssetStatus GetDownloadStatus(const char *assetPackName,
                                      float *completionProgress, uint64_t *totalPackSize);

private:
    assetManagerInternal *mInternals;

	std::vector<RequestInfo> m_Req;
};

#endif