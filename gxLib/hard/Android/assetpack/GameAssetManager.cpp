//--------------------------------------------------------------------------------------------
//
//ここからアセットマネージャの窓口クラス
//
//--------------------------------------------------------------------------------------------
/*
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *	 https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <gxLib.h>
#include <gxLib/gx.h>
#include <string.h>
#include <iostream>
#include<fstream>
#include <fcntl.h>
#include<sys/stat.h>
#include<sys/types.h>
#include <unistd.h>
#include <string>
#include <vector>
#include <sys/stat.h>
#include <vector>
#include <stdio.h>
#include <string.h>

#include <android/log.h>
#include <android/asset_manager.h>
#include <play/asset_pack.h>
#include "assetControl.h"
#include "assetManagerInternal.h"
#include "GameAssetManager.h"




GameAssetManager::GameAssetManager(AAssetManager *assetManager, JavaVM *jvm, jobject android_context)
{
	//実作業者を生成
	mInternals = new assetManagerInternal(assetManager, jvm, android_context);
}

GameAssetManager::~GameAssetManager()
{
	//実作業者を開放

	delete mInternals;
	mInternals = NULL;
}

void GameAssetManager::OnPause()
{
	GX_DEBUGLOG("GameAssetManager onPause");
	//----------------------------------------------------------------------------------------------------------------------------------------
	AssetPackManager_onPause();	// --------------------- ★★★
	//----------------------------------------------------------------------------------------------------------------------------------------
}

void GameAssetManager::OnResume() {
	GX_DEBUGLOG("GameAssetManager onResume");

	//----------------------------------------------------------------------------------------------------------------------------------------
	AssetPackManager_onResume();	// --------------------- ★★★
	//----------------------------------------------------------------------------------------------------------------------------------------
}

const char *GameAssetManager::GetGameAssetErrorMessage()
{
	return mInternals->GetAssetPackErrorMessage();
}

void GameAssetManager::ProcUpdateRequest()
{
	for( int ii=0; ii<m_Req.size(); ii++ )
	{
		switch( m_Req[ii].ope ){
			case 0:
				//load
				mInternals->RequestAssetPackDownload( m_Req[ii].assetPackName.c_str() );
				break;
			case 1:
				//remove
				mInternals->RequestAssetPackRemoval( m_Req[ii].assetPackName.c_str() );
				break;
			case 2:
				//cancel
				mInternals->RequestAssetPackCancelDownload( m_Req[ii].assetPackName.c_str() );
				break;
			case 3:
				//mobile
				mInternals->RequestMobileDataDownloads();
				break;

		}
	}

	m_Req.clear();
}

void GameAssetManager::UpdateGameAssetManager()
{
	// モバイルデータリクエストのステータス結果を更新します
	ProcUpdateRequest();

	mInternals->UpdateMobileDataRequestStatus();

	// 必要に応じてアセットパックのステータスを更新します
	for (int i = 0; i < mInternals->GetAssetPackCount(); ++i)
	{
		//各種アセットパックの状態を監視する

		AssetPackInfo *assetPackInfo = mInternals->GetAssetPack(i);

		if (assetPackInfo != NULL)
		{
			// アセットパックのダウンロード状態を照会し、それに応じてステータスを更新する
			// 内部ステータスにある場合は、そうします

			if (assetPackInfo->mAssetPackStatus == GAMEASSET_WAITING_FOR_STATUS ||
				assetPackInfo->mAssetPackStatus == GAMEASSET_DOWNLOADING ||
				assetPackInfo->mAssetPackStatus == GAMEASSET_PENDING_ACTION ||
				assetPackInfo->mAssetPackStatus == GAMEASSET_NEEDS_MOBILE_AUTH) {

				const char *assetPackName = assetPackInfo->mPackName;
				AssetPackDownloadState *downloadState = NULL;

				//----------------------------------------------------------------------------------------------------------------------------------------
				AssetPackErrorCode assetPackErrorCode = AssetPackManager_getDownloadState( assetPackName, &downloadState );// --------------------- ★★★
				//----------------------------------------------------------------------------------------------------------------------------------------

				if (assetPackErrorCode == ASSET_PACK_NO_ERROR)
				{
					// 返されたダウンロード状態を使用して、アセットパック情報を更新します
					mInternals->UpdateAssetPackFromDownloadState(assetPackInfo, downloadState);
				}
				else
				{
					// If an error is reported, mark the asset pack as being in error and bail on the update process
					// エラーが報告された場合は、アセットパックをエラーとしてマークし、更新プロセスを中止します

					mInternals->setAssetPackErrorStatus(assetPackErrorCode, assetPackInfo,"GameAssetManager: getDownloadState");

					//エラーだった場合はここで終了
					return;
				}

				//----------------------------------------------------------------------------------------------------------------------------------------
				AssetPackDownloadState_destroy(downloadState);	// --------------------- ★★★
				//----------------------------------------------------------------------------------------------------------------------------------------

			}
		}
	}
}


uint8_t* GameAssetManager::LoadFile( const char *assetName, size_t* bufferSize )
{
	//ファイルの読み込み（テクスチャ読み込み関数からのみ呼ばれる）
	uint8_t *pData = nullptr;

	if (assetName != NULL) {
		for (int ii = 0; ii < mInternals->GetAssetPackCount(); ii++)
		{
			if (mInternals->GetAssetPack(ii)->mAssetPackStatus == GAMEASSET_READY)
			{
				//アセットパックの用意ができていればパックタイプに応じてファイルを読み込む
				size_t size = 0;

				switch ( mInternals->GetAssetPack(ii)->mPackType) {
				case GAMEASSET_PACKTYPE_INTERNAL:
					pData = mInternals->loadInternal(assetName, &size);
					*bufferSize = size;
					break;

				case GAMEASSET_PACKTYPE_FASTFOLLOW:
				case GAMEASSET_PACKTYPE_ONDEMAND:
					//loadSuccess = mInternals->LoadExternalGameAsset(assetName, bufferSize,	loadBuffer, packInfo);
					{
						AssetPackInfo *packInfo = mInternals->GetAssetPack(ii);
						char fullAssetFilePath[MAX_ASSET_PATH_LENGTH];

						mInternals->generateFullAssetPath( assetName, packInfo, fullAssetFilePath, MAX_ASSET_PATH_LENGTH);
						pData = mInternals->loadExternal(fullAssetFilePath, &size);
						*bufferSize = size;
					}
					break;
				}

				if (pData) break;
			}
		}
	}

	return pData;
}


GameAssetStatus GameAssetManager::GetGameAssetPackStatus(const char *assetPackName)
{
	//demo sceneから呼ばれる（アセットパックの状態を監視している模様）

	GameAssetStatus assetStatus = GAMEASSET_NOT_FOUND;

	if (assetPackName != nullptr)
	{
		AssetPackInfo *packInfo = mInternals->GetAssetPackByName(assetPackName);
		if (packInfo != nullptr)
		{
			assetStatus = packInfo->mAssetPackStatus;
		}
	}

	return assetStatus;
}

GameAssetPackType GameAssetManager::GetGameAssetPackType( const char *assetPackName)
{
	//demo sceneから呼ばれる（アセットパックのタイプを取得している模様）
	//※internalでなければ削除できるようにしている

	GameAssetPackType assetPackType = GAMEASSET_PACKTYPE_INTERNAL;

	if (assetPackName != NULL) {
		AssetPackInfo *packInfo = mInternals->GetAssetPackByName(assetPackName);
		if (packInfo != NULL) {
			assetPackType = packInfo->mPackType;
		}
	}

	return assetPackType;
}

void GameAssetManager::RequestMobileDataDownloads()
{
	//モバイルデータのダウンロードリクエストを受け付ける
	//WIFI以外でのダウンロードのリクエストを発生させる

	if (mInternals->GetAssetPackManagerInitialized())
	{
		RequestInfo info;
		info.ope = 3;
		info.assetPackName = "";
		m_Req.push_back( info );
	}
}


bool GameAssetManager::RequestDownload(const char *assetPackName)
{
	//初期化が済んでいればアセットパックのダウンロードを要求する
	//demo Sceneから呼ばれる

	bool downloadStarted = false;

	GX_DEBUGLOG("GameAssetManager :: UI called RequestDownload %s", assetPackName);

	if (mInternals->GetAssetPackManagerInitialized())
	{
		//downloadStarted = mInternals->RequestAssetPackDownload(assetPackName);
		RequestInfo info;
		info.ope = 0;
		info.assetPackName = assetPackName;
		m_Req.push_back( info );
	}

	return downloadStarted;
}

void GameAssetManager::RequestDownloadCancellation(const char *assetPackName)
{
	//アセットパックのダウンロードをキャンセルする

	//mInternals->RequestAssetPackCancelDownload(assetPackName);
	RequestInfo info;
	info.ope = 2;
	info.assetPackName = assetPackName;
	m_Req.push_back( info );
}

bool GameAssetManager::RequestRemoval(const char *assetPackName)
{
	//アセットパックを削除する

//	return mInternals->RequestAssetPackRemoval(assetPackName);

	RequestInfo info;
	info.ope = 1;
	info.assetPackName = assetPackName;
	m_Req.push_back( info );

	return true;
}


GameAssetStatus GameAssetManager::GetDownloadStatus( const char *assetPackName, float *completionProgress, uint64_t *totalPackDownloadSize )
{
	//現在のダウンロード状況を取得する

	//状態の更新処理は↓こちら
	//void GameAssetManagerInternals::UpdateAssetPackFromDownloadState

	GameAssetStatus assetStatus = GAMEASSET_NOT_FOUND;

	if (assetPackName != NULL) {
		AssetPackInfo *packInfo = mInternals->GetAssetPackByName(assetPackName);

		if (packInfo != NULL)
		{
			assetStatus = packInfo->mAssetPackStatus;

			if (completionProgress != NULL)
			{
				*completionProgress = packInfo->mAssetPackCompletion;
			}

			if (totalPackDownloadSize != NULL)
			{
				*totalPackDownloadSize = packInfo->mAssetPackDownloadSize;
			}
		}
	}

	return assetStatus;
}





