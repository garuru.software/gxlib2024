#include <gxLib.h>
#include <gxLib/gx.h>
#include <string.h>
#include <iostream>
#include<fstream>
#include <fcntl.h>
#include<sys/stat.h>
#include<sys/types.h>
#include <unistd.h>
#include <string>
#include <vector>
#include <sys/stat.h>
#include <vector>
#include <stdio.h>
#include <string.h>

#include <android/log.h>
#include <android/asset_manager.h>
#include <play/asset_pack.h>
#include "assetControl.h"
#include "assetManagerInternal.h"
#include "GameAssetManager.h"


AAssetManager* GetAssetManager();
AssetController *AssetController::s_pInstance = nullptr;


AssetController::AssetController()
{
	JavaVM* jvm = CAndroid::GetInstance()->m_pJavaVM;
	jobject jobj = CAndroid::GetInstance()->GetAndroidContext();
	AAssetManager* am = GetAssetManager();

	p_gameAssetManager = new GameAssetManager(am,jvm,jobj);
}


std::string AssetController::GetAssetPackStatus( const char  *pAssetName )
{
	AssetPackDownloadState *downloadState;
	AssetPackErrorCode assetPackErrorCode = AssetPackManager_getDownloadState( pAssetName, &downloadState );// --------------------- ★★★

	char *pStr = "";

	switch (assetPackErrorCode) {
		case ASSET_PACK_UNKNOWN:
			//謎のアセットパック。AssetPackManager_requestInfo（）を呼び出してサイズを確認するか、AssetPackManager_requestDownload（）でダウンロードを開始してください。
			pStr = "ASSET_PACK_UNKNOWN";
			break;

		case ASSET_PACK_DOWNLOAD_PENDING:
			//AssetPackManager_requestDownload（）非同期リクエストは保留中です。
			pStr = "ASSET_PACK_DOWNLOAD_PENDING";
			break;

		case ASSET_PACK_DOWNLOADING:
			//アセットパックのダウンロードが進行中です。
			pStr = "ASSET_PACK_DOWNLOADING";
			break;

		case ASSET_PACK_TRANSFERRING:
			//アセットパックがアプリに転送されています。
			pStr = "ASSET_PACK_TRANSFERRING";
			break;

		case ASSET_PACK_DOWNLOAD_COMPLETED:
			//ダウンロードと転送が完了しました。アセットはアプリで利用できます。
			pStr = "ASSET_PACK_DOWNLOAD_COMPLETED";
			break;

		case ASSET_PACK_DOWNLOAD_FAILED:
			//AssetPackManager_requestDownload（）が失敗しました。
			//AssetPackErrorCodeは対応するエラーであり、ASSET_PACK_NO_ERRORではありません。
			//このステータスでは、bytes_downloadedフィールドとtotal_bytes_to_downloadフィールドは信頼できません。
			pStr = "ASSET_PACK_DOWNLOAD_FAILED";
			break;

		case ASSET_PACK_DOWNLOAD_CANCELED:
			//アセットパックのダウンロードはキャンセルされました。
			pStr = "ASSET_PACK_DOWNLOAD_CANCELED";
			break;

		case ASSET_PACK_WAITING_FOR_WIFI:
			//アセットパックのダウンロードは、Wi-Fiが進行するのを待っています。
			//必要に応じて、AssetPackManager_showCellularDataConfirmation（）を呼び出して、セルラーデータを介したダウンロードの確認をユーザーに依頼します
			pStr = "ASSET_PACK_WAITING_FOR_WIFI";
			break;

		case ASSET_PACK_NOT_INSTALLED:
		{
			//アセットパックがインストールされていません。
		}
			pStr = "ASSET_PACK_NOT_INSTALLED";
			break;

		case ASSET_PACK_INFO_PENDING:
			//AssetPackManager_requestInfo（）非同期リクエストが開始されましたが、結果はまだ不明です。
			pStr = "ASSET_PACK_INFO_PENDING";
			break;

		case ASSET_PACK_INFO_FAILED:
			//AssetPackManager_requestInfo（）非同期要求が失敗しました。
			//AssetPackErrorCodeは、対応するエラーになります。
			//ASSET_PACK_NO_ERRORは決してありません。
			//このステータスでは、bytes_downloadedフィールドとtotal_bytes_to_downloadフィールドは信頼できません。
			pStr = "ASSET_PACK_INFO_FAILED";
			break;

		case ASSET_PACK_REMOVAL_PENDING:
			//AssetPackManager_requestRemoval（）非同期リクエストが開始されました。
			pStr = "ASSET_PACK_REMOVAL_PENDING";
			break;

		case ASSET_PACK_REMOVAL_FAILED:
			//AssetPackManager_requestRemoval（）非同期要求が失敗しました。
			pStr = "ASSET_PACK_REMOVAL_FAILED";
			break;

		default:
			break;
	}

	std::string ret = pStr;
	return ret;
}


std::string AssetController::GetErrorMessage(GameAssetStatus assetPackStatus)
{
    char *pStr = "";

    switch(assetPackStatus){
        case GAMEASSET_NOT_FOUND:
            pStr = "GAMEASSET_NOT_FOUND";
            break;
        case GAMEASSET_WAITING_FOR_STATUS:
            pStr = "GAMEASSET_WAITING_FOR_STATUS";
            break;
        case GAMEASSET_NEEDS_DOWNLOAD:
            pStr = "GAMEASSET_NEEDS_DOWNLOAD";
            break;
        case GAMEASSET_NEEDS_MOBILE_AUTH:
            pStr = "GAMEASSET_NEEDS_MOBILE_AUTH";
            break;
        case GAMEASSET_DOWNLOADING:
            pStr = "GAMEASSET_DOWNLOADING";
            break;
        case GAMEASSET_READY:
            pStr = "GAMEASSET_READY";
            break;
        case GAMEASSET_PENDING_ACTION:
            pStr = "GAMEASSET_PENDING_ACTION";
            break;
        case GAMEASSET_ERROR:
            pStr = "GAMEASSET_ERROR";
            break;
    }

    std::string ret = pStr;

    return ret;
}

uint8_t* AssetController::LoadTestFile( char* pFileName )
{
	size_t size = 0;
	uint8_t* pData = p_gameAssetManager->LoadFile( pFileName ,&size );

	return pData;
}

uint8_t* AssetController::LoadAABFile( char* pFileName , size_t *uSize )
{
	return p_gameAssetManager->LoadFile( pFileName ,uSize );
}


void AssetController::RequestCancelDownLoad(int n)
{
	switch( n ){
		case 0:
			//Install-Timeデータは削除できない
			//p_gameAssetManager->RequestDownloadCancellation(INSTALL_ASSETPACK_NAME);
			break;
		case 1:
			p_gameAssetManager->RequestDownloadCancellation(FASTFOLLOW_ASSETPACK_NAME);
			break;
		case 2:
			p_gameAssetManager->RequestDownloadCancellation(ONDEMAND_ASSETPACK_NAME);
			break;
	}
}

void AssetController::RemovePak( int n )
{
	switch( n ){
		case 0:
			//Install-Timeデータは削除できない
			//p_gameAssetManager->RequestDownload(INSTALL_ASSETPACK_NAME);
			break;
		case 1:
			p_gameAssetManager->RequestRemoval(FASTFOLLOW_ASSETPACK_NAME);
			break;
		case 2:
			p_gameAssetManager->RequestRemoval(ONDEMAND_ASSETPACK_NAME);
			break;
	}
}

void AssetController::RequestDownLoad( int n )
{
	switch( n ){
	case 0:
		p_gameAssetManager->RequestDownload(INSTALL_ASSETPACK_NAME);
		break;
	case 1:
		p_gameAssetManager->RequestDownload(FASTFOLLOW_ASSETPACK_NAME);
		break;
	case 2:
		p_gameAssetManager->RequestDownload(ONDEMAND_ASSETPACK_NAME);
		break;
	}
}

GameAssetStatus AssetController::GetCurrentStatus( int n )
{
	return m_AssetPackStatus[n];
}

std::string AssetController::GetStatus( int n )
{

	return m_CurrentStatus[n];

}


std::string AssetController::GetStatus2( int n )
{
	return m_CurrentStatus2[n];
}

void AssetController::Update()
{
	p_gameAssetManager->UpdateGameAssetManager();

	m_AssetPackStatus[0] = p_gameAssetManager->GetGameAssetPackStatus(INSTALL_ASSETPACK_NAME);
	m_AssetPackStatus[1] = p_gameAssetManager->GetGameAssetPackStatus(FASTFOLLOW_ASSETPACK_NAME);
	m_AssetPackStatus[2] = p_gameAssetManager->GetGameAssetPackStatus(ONDEMAND_ASSETPACK_NAME);

	m_bDownLoading = gxFalse;


	switch( m_Sequence ){
	case 0:
		//自動ダウンロード処理
		{
			gxBool bChange = gxFalse;
			for( Sint32 ii=0; ii<3; ii++ )
			{
				if( GetCurrentStatus(ii) == GAMEASSET_READY)
				{
					//準備完了(GAMEASSET_READY)
				}
				else if( GetCurrentStatus(ii) == GAMEASSET_DOWNLOADING )
				{
					m_bDownLoading = gxTrue;
				}
				else if( GetCurrentStatus(ii) == GAMEASSET_NEEDS_DOWNLOAD )
				{
					RequestDownLoad(ii);
				}
				else if( GetCurrentStatus(ii) == GAMEASSET_ERROR )
				{
					//とりあえずトライしてみる
					RequestDownLoad(ii);
				}
			}
			m_Sequence = 100;
		}

		break;
	case 100:
		m_ReloadCnt ++;
		if( m_ReloadCnt >= 160 )
		{
			m_ReloadCnt = 0;
			m_Sequence = 0;
		}
		break;
	default:
		break;
	}


#if 0
	{
        //Install Time

	    GameAssetManager *gameAssetManager = p_gameAssetManager;
	    const GameAssetStatus assetPackStatus = gameAssetManager->GetGameAssetPackStatus(INSTALL_ASSETPACK_NAME);
        auto str = GetErrorMessage(assetPackStatus);
		auto str2 = GetAssetPackStatus(INSTALL_ASSETPACK_NAME);

		m_CurrentStatus[0] = str;
		m_CurrentStatus2[0] = str2;
		m_AssetPackStatus[0] = assetPackStatus;

        uint64_t assetPackSize = 0;
        gameAssetManager->GetDownloadStatus(INSTALL_ASSETPACK_NAME, nullptr, &assetPackSize);
        const float assetPackSizeMB = ((float) assetPackSize) / (1024.0f * 1024.0f);

        //if( assetPackStatus == GAMEASSET_NEEDS_DOWNLOAD )
        {
            //uint64_t assetPackSize = 0;
            //Float32 fProgressRatio = 0.0f;
            //gameAssetManager->GetDownloadStatus(FASTFOLLOW_ASSETPACK_NAME, &fProgressRatio, &assetPackSize);
            //const float assetPackSizeMB = ((float) assetPackSize) / (1024.0f * 1024.0f);
            //char downloadLabel[128];
            //snprintf(downloadLabel, 128, "Download %s %.1f MB", INSTALL_ASSETPACK_NAME, assetPackSizeMB);
			gameAssetManager->GetDownloadStatus(INSTALL_ASSETPACK_NAME, &m_fDownLoadStatus[0], (uint64_t*)&m_PackSize[0]);

        }
	}


    {
        //FAST FOLLOW

        GameAssetManager *gameAssetManager = p_gameAssetManager;
        const GameAssetStatus assetPackStatus = gameAssetManager->GetGameAssetPackStatus(FASTFOLLOW_ASSETPACK_NAME);
        auto str = GetErrorMessage(assetPackStatus);
		auto str2 = GetAssetPackStatus(FASTFOLLOW_ASSETPACK_NAME);

		m_CurrentStatus[1] = str;
		m_CurrentStatus2[1] = str2;
		m_AssetPackStatus[1] = assetPackStatus;
		gameAssetManager->GetDownloadStatus(FASTFOLLOW_ASSETPACK_NAME, &m_fDownLoadStatus[1], (uint64_t*)&m_PackSize[1]);
    }


	{
		//OnDemand

		GameAssetManager *gameAssetManager = p_gameAssetManager;
		const GameAssetStatus assetPackStatus = gameAssetManager->GetGameAssetPackStatus(ONDEMAND_ASSETPACK_NAME);
		auto str = GetErrorMessage(assetPackStatus);
		auto str2 = GetAssetPackStatus(ONDEMAND_ASSETPACK_NAME);

		m_CurrentStatus[2] = str;
		m_CurrentStatus2[2] = str2;
		m_AssetPackStatus[2] = assetPackStatus;
		gameAssetManager->GetDownloadStatus(ONDEMAND_ASSETPACK_NAME, &m_fDownLoadStatus[2], (uint64_t*)&m_PackSize[2]);

	}

//	gameAssetManager->RequestMobileDataDownloads();

#endif

}



//---------------------------------------------------------------------------


//---------------------------------------------------------------------------

//void AssetControl()
//{
//	//メインスレッドから呼ばれるアップデート処理
//
//	AssetController::GetInstance()->Update();
//
//}


void AssetControlFromGameThread()
{
	//Game Thread から呼ばれるアセットのコントロール

	static Sint32 seq = 0;
	static std::vector<gxUtil::RoundButton*> btns;

	if( seq == 0 )
	{
		btns.push_back( new gxUtil::RoundButton( 32 , 32+100*0 , 100 ,0xffff0000 , 48 ) );
		btns.push_back( new gxUtil::RoundButton( 32 , 32+100*1 , 100 ,0xff00ff00 , 48 ) );
		btns.push_back( new gxUtil::RoundButton( 32 , 32+100*2 , 100 ,0xff0000ff , 48 ) );

		AssetController::GetInstance()->loadbtns.push_back( new gxUtil::RoundButton( 320 , 32+100*0 , 100 ,0xff0000ff , 48 ) );
		AssetController::GetInstance()->loadbtns.push_back( new gxUtil::RoundButton( 320 , 32+100*1 , 100 ,0xff0000ff , 48 ) );
		AssetController::GetInstance()->loadbtns.push_back( new gxUtil::RoundButton( 320 , 32+100*2 , 100 ,0xff0000ff , 48 ) );
		AssetController::GetInstance()->loadbtns.push_back( new gxUtil::RoundButton( 320 , 32+100*3 , 100 ,0xff0000ff , 48 ) );
		seq ++;
	}

	for( Sint32 ii=0; ii<btns.size(); ii++)
	{
		btns[ii]->Update();
		auto pos = btns[ii]->GetPos();
		gxLib::Printf( pos.x+64 , pos.y-32 , pos.z , ATR_DFLT , ARGB_DFLT , "%s" ,AssetController::GetInstance()->GetStatus(ii).c_str() );

		gxLib::Printf( pos.x+64 , pos.y+0 , pos.z , ATR_DFLT , ARGB_DFLT , "%s(%d)" ,
				 							AssetController::GetInstance()->GetStatus2(ii).c_str(),
					   						AssetController::GetInstance()->GetCurrentStatus(ii) );

		gxLib::Printf( pos.x+64 , pos.y+32 , pos.z , ATR_DFLT , ARGB_DFLT , "%.2f , %d" ,
					   AssetController::GetInstance()->m_fDownLoadStatus[ii] ,
					   AssetController::GetInstance()->m_PackSize[ii] );

		if( AssetController::GetInstance()->GetCurrentStatus(ii) == GAMEASSET_READY)
		{
			//準備完了(GAMEASSET_READY)
			btns[ii]->SetARGB(0xff00ff00);
			if( btns[ii]->IsTrigger() )
			{
				AssetController::GetInstance()->RemovePak(ii);
			}
		}
		else if( AssetController::GetInstance()->GetCurrentStatus(ii) == GAMEASSET_DOWNLOADING)
		{
			//ダウンロード中(GAMEASSET_DOWNLOADING)
			btns[ii]->SetARGB(0xffff8000);
			if( btns[ii]->IsTrigger() )
			{
				AssetController::GetInstance()->RequestCancelDownLoad(ii);
			}
		}
		else
		{
			//ダウンロードしていない
			//GAMEASSET_NOT_FOUND
			//GAMEASSET_WAITING_FOR_STATUS,
			//GAMEASSET_NEEDS_DOWNLOAD
			//GAMEASSET_NEEDS_MOBILE_AUTH
			//GAMEASSET_PENDING_ACTION
			//GAMEASSET_ERROR

			btns[ii]->SetARGB(0xffff0000);
			if( btns[ii]->IsTrigger() )
			{
				//アセットパック読み込み
				AssetController::GetInstance()->RequestDownLoad(ii);
			}
		}
	}

	for( Sint32 ii=0; ii<btns.size(); ii++)
	{
		btns[ii]->Draw();
	}


	static char* fileName[]={
			"test0.txt",
			"test1.txt",
			"test2.txt",
			"testa.txt",
	};


	for( Sint32 ii=0; ii<AssetController::GetInstance()->loadbtns.size(); ii++)
	{
		AssetController::GetInstance()->loadbtns[ii]->Update();
		if( AssetController::GetInstance()->loadbtns[ii]->IsTrigger() ) {
			size_t uSize = 0;
			char *p = (char*)gxLib::LoadFile(fileName[ii] , &uSize );
			if( p )
			{
				AssetController::GetInstance()->text[ii] = p;
			}
			else
			{
				AssetController::GetInstance()->text[ii] = "---";
			}
		}
		auto* ac = AssetController::GetInstance();
		auto pos = ac->loadbtns[ii]->GetPos();
		gxLib::Printf( pos.x+32 , pos.y , pos.z , ATR_DFLT , ARGB_DFLT , "%s" , AssetController::GetInstance()->text[ii].c_str() );
		AssetController::GetInstance()->loadbtns[ii]->Draw();

	}


	//ファイル読み込み
	/*
	for( Sint32 ii=0; ii<AssetController::GetInstance()->loadbtns.size(); ii++)
	{
		AssetController::GetInstance()->loadbtns[ii]->Update();
		if( AssetController::GetInstance()->loadbtns[ii]->IsTrigger() )
		{
			char *p = (char*)AssetController::GetInstance()->LoadTestFile(fileName[ii]);
			if( p )
			{
				AssetController::GetInstance()->text[ii] = p;
			}
			else
			{
				AssetController::GetInstance()->text[ii] = "---";
			}
		}
		auto* ac = AssetController::GetInstance();
		auto pos = ac->loadbtns[ii]->GetPos();
		gxLib::Printf( pos.x+32 , pos.y , pos.z , ATR_DFLT , ARGB_DFLT , "%s" , AssetController::GetInstance()->text[ii].c_str() );
		AssetController::GetInstance()->loadbtns[ii]->Draw();
	}
	 */

}


////del Z:\dev\Sample\final\app\build\outputs\bundle\debug\app-debug.aab
////del Z:\dev\Sample\final\nativegamepad.apks
////java -jar bundletool-all-1.6.1.jar build-apks --bundle=app\build\outputs\bundle\debug\app-debug.aab --output=nativegamepad.apks --local-testing
////java -jar bundletool-all-1.6.1.jar install-apks --apks=nativegamepad.apks
