//------------------------------------------------------------------

//------------------------------------------------------------------
#include <gxLib.h>
#include <gxLib/gxSoundManager.h>
#include <gxLib/util/CFileWave.h>

#include "COpenAL.h"
#ifdef PLATFORM_WINDESKTOP
	#pragma comment(lib,"OpenAL32.lib")
#endif

#ifdef PLATFORM_ANDROID
	#include <openal/OpenAL32/Include/alMain.h>
#endif

#ifdef _USE_OPENAL
SINGLETON_DECLARE_INSTANCE( COpenAL );

COpenAL::COpenAL()
{
	m_MaxCount = MAX_SOUND_NUM;
}


COpenAL::~COpenAL()
{
	finish();
}

void COpenAL::Init()
{
	init( m_MaxCount );
}

gxBool COpenAL::init( Sint32 channelCount )
{
    m_Device = alcOpenDevice(NULL); // open default m_Device

    if (m_Device != NULL)
    {
#ifdef PLATFORM_ANDROID
        GX_DEBUGLOG("[UpdateSize]%d",m_Device->UpdateSize);
        GX_DEBUGLOG("[MaxNumOfSource]%d",m_Device->MaxNoOfSources);
#endif
        m_Context=alcCreateContext(m_Device,NULL); // create m_Context

        if (m_Context != NULL)
        {
            alcMakeContextCurrent(m_Context); // set active m_Context
        }
    }

    alGenSources( channelCount,m_Source);
    ALenum error = alGetError();
    alGenBuffers( channelCount,m_Buffer);

	m_MaxCount = channelCount;

/*
    size_t pcm_freq = 44100;
    float key_freq = 440.0;
    unsigned short *data = new unsigned short[pcm_freq];
    size_t sz = pcm_freq*2;

    for( size_t ii=0; ii<pcm_freq; ii ++ )
    {
        data[ii] = sin(key_freq*3.14f*2.0*ii/pcm_freq )*65535;
    }

    alBufferferData(m_Buffer[0] , AL_FORMAT_MONO16 ,data,sz,pcm_freq);

    alSourcei(m_Source[0] , AL_m_BufferFER , m_Buffer[0] );

    alSourcePlay(m_Source[0]);
    alSourcei( m_Source[0], AL_LOOPING , AL_TRUE);   // 繰り返し
    alSourcei( m_Source[0], AL_PITCH   , 1.0f);      //
    alSourcei( m_Source[0], AL_GAIN    , 0.45f);     // 音量
*/

	return true;
}


void COpenAL::finish()
{
	//終了処理

    alcDestroyContext( m_Context );
    alcCloseDevice( m_Device );
	GX_DEBUGLOG("andio_engine::close");

	alDeleteSources( m_MaxCount , m_Source );

	alDeleteBuffers( m_MaxCount , m_Buffer );

}


void COpenAL::Action()
{
	for(Sint32 ii=0; ii<MAX_SOUND_NUM; ii++)
	{
		StPlayInfo *pInfo;

		pInfo = gxSoundManager::GetInstance()->GetPlayInfo( ii );

		if( pInfo->bReq )
		{
			if( pInfo->uStatus & enSoundReqStop )
			{
				StopSound( ii );
			}
			if( pInfo->uStatus & enSoundReqLoad )
			{
				//read( ii );
                LoadSound( ii , gxSoundManager::GetInstance()->GetPlayInfo(ii)->fileName );
			}
			if( pInfo->uStatus & enSoundReqPlay )
			{
				PlaySound( ii , pInfo->bLoop , pInfo->bOverWrap , pInfo->fVolume );
			}
			if( pInfo->uStatus & enSoundReqVolume )
			{
				SetVolume( ii , gxSoundManager::GetInstance()->GetPlayInfo( ii )->fVolume );
			}
			if (pInfo->uStatus & enSoundReqChangeFreq)
			{
				SetFrequency( ii , gxSoundManager::GetInstance()->GetPlayInfo( ii )->fFreqRatio );
			}
            if (pInfo->uStatus & enSoundReqChangePan)
            {

            }
            if (pInfo->uStatus & enSoundReqSetReverb)
            {
				SetReverb( ii );
            }

			pInfo->bReq = gxFalse;
			pInfo->uStatus = enSoundReqNone;
		}

		gxSoundManager::GetInstance()->GetPlayInfo(ii)->bPlayNow = IsPlay(ii);

	}

}



gxBool COpenAL::LoadSound( Sint32 index , gxChar *pFileName )
{
	//loadFromAsset( pFileName , index );
	ReadSound(index);
	return gxTrue;
}

gxBool COpenAL::ReadSound( Sint32 index )
{
    //メモリからPCMデータを読み込む

    Uint8 *pData  = (Uint8*)gxSoundManager::GetInstance()->GetPlayInfo( index )->m_WaveData.GetPCMImage();	//gxSoundManager::GetInstance()->GetPlayInfo( index )->m_pTempm_Bufferfer;
    Uint32 uSize  = gxSoundManager::GetInstance()->GetPlayInfo( index )->m_WaveData.GetFileSize();			//gxSoundManager::GetInstance()->GetPlayInfo( index )->m_uTempSize;
    gxChar *pName = gxSoundManager::GetInstance()->GetPlayInfo( index )->fileName;
    StWaveInfo *pFormat = gxSoundManager::GetInstance()->GetPlayInfo( index )->m_WaveData.GetFormat();

    int BitPerSample = pFormat->Format.wBitsPerSample;
    int Channels = pFormat->Format.nChannels;
    size_t pcm_freq = pFormat->Format.nSamplesPerSec;//44100;

/*
    float key_freq = 440.0;
    unsigned short *data = new unsigned short[pcm_freq];
    size_t sz = pcm_freq*2;

    for( size_t ii=0; ii<pcm_freq; ii ++ )
    {
        data[ii] = sin(key_freq*3.14f*2.0*ii/pcm_freq )*65535;
    }
    alm_BufferferData(m_Buffer[index] , AL_FORMAT_MONO16 ,data,sz,pcm_freq);
*/
    alSourceStop( m_Source[index] );
    ALenum error = alGetError();

    alDeleteBuffers( 1 , &m_Buffer[index] );
    error = alGetError();

    alGenBuffers( 1 , &m_Buffer[index] );
    error = alGetError();

	if( BitPerSample == 16 )
	{
		if( Channels == 1 )
		{
		   alBufferData( m_Buffer[index] , AL_FORMAT_MONO16 ,pData,(ALsizei)uSize, (ALsizei)pcm_freq);
		}
		else
		{
		   alBufferData( m_Buffer[index] , AL_FORMAT_STEREO16 ,pData, (ALsizei)uSize, (ALsizei)pcm_freq);
		}
	}
	else
	{
		if( Channels == 1 )
		{
		   alBufferData( m_Buffer[index] , AL_FORMAT_MONO8 ,pData, (ALsizei)uSize, (ALsizei)pcm_freq);
		}
		else
		{
		   alBufferData( m_Buffer[index] , AL_FORMAT_STEREO8 ,pData, (ALsizei)uSize, (ALsizei)pcm_freq);
		}
	}
    error = alGetError();
//    alm_BufferferData(m_Buffer[index] , AL_FORMAT_MONO16 ,pData,uSize,22050);

    //alSourcei( m_Source[index] , AL_BUFFER , NULL );
    //alDeleteSources( 1 , &m_Source[index] );
    //alGenSources( 1 , &m_Source[index] );
    //ALenum error = alGetError();

    alSourcei( m_Source[index] , AL_BUFFER , m_Buffer[index] );
	m_bUsed[index] = gxTrue;

    error = alGetError();

    return true;
}

//gxBool COpenAL::loadFromAsset(char* fname, Sint32 index)
//{
//	return true;
//}


gxBool COpenAL::PlaySound( Uint32 uIndex , gxBool bLoop , gxBool bOverWrap , Float32 fVolume )
{

    //overwrapできない

    if( !bOverWrap )
    {
//        StopSound( uIndex );
    }

//    alSourcef( m_Source[0], AL_SEC_OFFSET, 0 );

    alSourcePlay( m_Source[uIndex] );

	if( bLoop )
	{
	    alSourcei( m_Source[uIndex], AL_LOOPING , AL_TRUE);   // 繰り返しあり
	}
	else
	{
	    alSourcei( m_Source[uIndex], AL_LOOPING , AL_FALSE );   // 繰り返しなし
	}

    alSourcef( m_Source[ uIndex ], AL_GAIN    , fVolume );     // 音量

    gxSoundManager::GetInstance()->PlayNow( uIndex , gxTrue );

    return gxTrue;
}

gxBool COpenAL::PauseSound(Sint32 index)
{
	alSourcePause( m_Source[index] );

	return gxTrue;
}


gxBool COpenAL::StopSound( Uint32 index)
{
	alSourceStop( m_Source[index] );

	return gxTrue;
}


gxBool COpenAL::IsPlay( Uint32 index)
{
	ALenum state;

    alGetSourcei( m_Source[index] , AL_SOURCE_STATE, &state );
    
    return (state == AL_PLAYING);
}

gxBool COpenAL::SetVolume( Uint32 uIndex , Float32 fVolume )
{
    Sint32 i = uIndex;
    Float32 fMasterVolume = gxSoundManager::GetInstance()->GetMasterVolume();
    Float32 fForceVolume  = 1.0f;//gxSoundManager::GetInstance()->Get->GetForceVolume();

    Float32 fVol = fVolume*fMasterVolume;

/*
    if ( fVol*100.f > 100.f )
    {
        fVol = 0.f;
    }
    else if ( fVol*100.f < 0.f )
    {
        fVol = -10000.f;
    }
    else
    {
        fVol *= fMasterVolume;
        fVol = -(Float32)pow( 100.0f, (Float32)(log10(100-fVol)) );
    }

	fVol = 1.0f - fVol;

	//
*/

	alSourcef( m_Source[ uIndex ], AL_GAIN , fVol );     // 音量

    return gxTrue;
}


gxBool COpenAL::SetFrequency(Sint32 index , Float32 fRatio )
{
    alSourcef( m_Source[index], AL_PITCH   , fRatio );      //

	return gxFalse;
}


gxBool COpenAL::SetReverb(Sint32 index)
{
	return gxFalse;
}

//#include "AL/efx-presets.h"
//#if 0
//static LPALGENEFFECTS alGenEffects;
//static LPALDELETEEFFECTS alDeleteEffects;
//static LPALISEFFECT alIsEffect;
//static LPALEFFECTI alEffecti;
//static LPALEFFECTIV alEffectiv;
//static LPALEFFECTF alEffectf;
//static LPALEFFECTFV alEffectfv;
//static LPALGETEFFECTI alGetEffecti;
//static LPALGETEFFECTIV alGetEffectiv;
//static LPALGETEFFECTF alGetEffectf;
//static LPALGETEFFECTFV alGetEffectfv;
//
///* Auxiliary Effect Slot object functions */
//static LPALGENAUXILIARYEFFECTSLOTS alGenAuxiliaryEffectSlots;
//static LPALDELETEAUXILIARYEFFECTSLOTS alDeleteAuxiliaryEffectSlots;
//static LPALISAUXILIARYEFFECTSLOT alIsAuxiliaryEffectSlot;
//static LPALAUXILIARYEFFECTSLOTI alAuxiliaryEffectSloti;
//static LPALAUXILIARYEFFECTSLOTIV alAuxiliaryEffectSlotiv;
//static LPALAUXILIARYEFFECTSLOTF alAuxiliaryEffectSlotf;
//static LPALAUXILIARYEFFECTSLOTFV alAuxiliaryEffectSlotfv;
//static LPALGETAUXILIARYEFFECTSLOTI alGetAuxiliaryEffectSloti;
//static LPALGETAUXILIARYEFFECTSLOTIV alGetAuxiliaryEffectSlotiv;
//static LPALGETAUXILIARYEFFECTSLOTF alGetAuxiliaryEffectSlotf;
//static LPALGETAUXILIARYEFFECTSLOTFV alGetAuxiliaryEffectSlotfv;
//#endif
//
//gxBool COpenAL::SetReverb(Sint32 index)
//{
//
///*
//    const ALCchar *name;
//
//    if(alcIsExtensionPresent(m_Device, "ALC_ENUMERATE_ALL_EXT"))
//    {
//        name = alcGetString(m_Device, ALC_ALL_DEVICES_SPECIFIER);
//    }
//
//    if(!name || alcGetError(m_Device) != AL_NO_ERROR)
//    {
//        name = alcGetString(m_Device, ALC_DEVICE_SPECIFIER);
//    }
//
//    GX_DEBUGLOG("Opened \"%s\"\n", name );
//#define LOAD_PROC(x)  ((x) = alGetProcAddress(#x))
//    LOAD_PROC(alGenEffects);
//    LOAD_PROC(alDeleteEffects);
//    LOAD_PROC(alIsEffect);
//    LOAD_PROC(alEffecti);
//    LOAD_PROC(alEffectiv);
//    LOAD_PROC(alEffectf);
//    LOAD_PROC(alEffectfv);
//    LOAD_PROC(alGetEffecti);
//    LOAD_PROC(alGetEffectiv);
//    LOAD_PROC(alGetEffectf);
//    LOAD_PROC(alGetEffectfv);
//
//    LOAD_PROC(alGenAuxiliaryEffectSlots);
//    LOAD_PROC(alDeleteAuxiliaryEffectSlots);
//    LOAD_PROC(alIsAuxiliaryEffectSlot);
//    LOAD_PROC(alAuxiliaryEffectSloti);
//    LOAD_PROC(alAuxiliaryEffectSlotiv);
//    LOAD_PROC(alAuxiliaryEffectSlotf);
//    LOAD_PROC(alAuxiliaryEffectSlotfv);
//    LOAD_PROC(alGetAuxiliaryEffectSloti);
//    LOAD_PROC(alGetAuxiliaryEffectSlotiv);
//    LOAD_PROC(alGetAuxiliaryEffectSlotf);
//    LOAD_PROC(alGetAuxiliaryEffectSlotfv);
//#undef LOAD_PROC
//*/
//	EFXEAXREVERBPROPERTIES reverb = EFX_REVERB_PRESET_GENERIC;
//    ALuint effect = 0, slot = 0;
//    ALenum err;
//
//    slot = 0;
//	alGenAuxiliaryEffectSlots(1, &slot);
//
//	alGenEffects(1, &effect);
//    if( 1 )
//   {
//         /* EAX Reverb is available. Set the EAX effect type then load the
//         * reverb properties. */
//        alEffecti(effect, AL_EFFECT_TYPE, AL_EFFECT_EAXREVERB);
//
//        alEffectf(effect, AL_EAXREVERB_DENSITY, reverb.flDensity);
//        alEffectf(effect, AL_EAXREVERB_DIFFUSION, reverb.flDiffusion);
//        alEffectf(effect, AL_EAXREVERB_GAIN, reverb.flGain);
//        alEffectf(effect, AL_EAXREVERB_GAINHF, reverb.flGainHF);
//        alEffectf(effect, AL_EAXREVERB_GAINLF, reverb.flGainLF);
//        alEffectf(effect, AL_EAXREVERB_DECAY_TIME, reverb.flDecayTime);
//        alEffectf(effect, AL_EAXREVERB_DECAY_HFRATIO, reverb.flDecayHFRatio);
//        alEffectf(effect, AL_EAXREVERB_DECAY_LFRATIO, reverb.flDecayLFRatio);
//        alEffectf(effect, AL_EAXREVERB_REFLECTIONS_GAIN, reverb.flReflectionsGain);
//        alEffectf(effect, AL_EAXREVERB_REFLECTIONS_DELAY, reverb.flReflectionsDelay);
//        alEffectfv(effect, AL_EAXREVERB_REFLECTIONS_PAN, reverb.flReflectionsPan);
//        alEffectf(effect, AL_EAXREVERB_LATE_REVERB_GAIN, reverb.flLateReverbGain);
//        alEffectf(effect, AL_EAXREVERB_LATE_REVERB_DELAY, reverb.flLateReverbDelay);
//        alEffectfv(effect, AL_EAXREVERB_LATE_REVERB_PAN, reverb.flLateReverbPan);
//        alEffectf(effect, AL_EAXREVERB_ECHO_TIME, reverb.flEchoTime);
//        alEffectf(effect, AL_EAXREVERB_ECHO_DEPTH, reverb.flEchoDepth);
//        alEffectf(effect, AL_EAXREVERB_MODULATION_TIME, reverb.flModulationTime);
//        alEffectf(effect, AL_EAXREVERB_MODULATION_DEPTH, reverb.flModulationDepth);
//        alEffectf(effect, AL_EAXREVERB_AIR_ABSORPTION_GAINHF, reverb.flAirAbsorptionGainHF);
//        alEffectf(effect, AL_EAXREVERB_HFREFERENCE, reverb.flHFReference);
//        alEffectf(effect, AL_EAXREVERB_LFREFERENCE, reverb.flLFReference);
//        alEffectf(effect, AL_EAXREVERB_ROOM_ROLLOFF_FACTOR, reverb.flRoomRolloffFactor);
//        alEffecti(effect, AL_EAXREVERB_DECAY_HFLIMIT, reverb.iDecayHFLimit);
//    }
//	else
//	{ 
//		alEffecti(effect, AL_EFFECT_TYPE, AL_EFFECT_REVERB);
//
//	    alEffectf(effect, AL_REVERB_DENSITY					, reverb.flDensity );
//	    alEffectf(effect, AL_REVERB_DIFFUSION				, reverb.flDiffusion );
//	    alEffectf(effect, AL_REVERB_GAIN					, reverb.flGain );
//	    alEffectf(effect, AL_REVERB_GAINHF					, reverb.flGainHF );
//	    alEffectf(effect, AL_REVERB_DECAY_TIME				, reverb.flDecayTime );
//	    alEffectf(effect, AL_REVERB_DECAY_HFRATIO			, reverb.flDecayHFRatio );
//	    alEffectf(effect, AL_REVERB_REFLECTIONS_GAIN		, reverb.flReflectionsGain );
//	    alEffectf(effect, AL_REVERB_REFLECTIONS_DELAY		, reverb.flReflectionsDelay );
//	    alEffectf(effect, AL_REVERB_LATE_REVERB_GAIN		, reverb.flLateReverbGain );
//	    alEffectf(effect, AL_REVERB_LATE_REVERB_DELAY		, reverb.flLateReverbDelay );
//	    alEffectf(effect, AL_REVERB_AIR_ABSORPTION_GAINHF	, reverb.flAirAbsorptionGainHF );
//	    alEffectf(effect, AL_REVERB_ROOM_ROLLOFF_FACTOR		, reverb.flRoomRolloffFactor );
//	    alEffecti(effect, AL_REVERB_DECAY_HFLIMIT			, reverb.iDecayHFLimit );
//    }
//
///*
//    err = alGetError();
//    if(err != AL_NO_ERROR)
//    {
//		if(alIsEffect(effect))
//		{
//			alDeleteEffects(1, &effect);
//		}
//	}
//*/
//}


void COpenAL::AllCutOff()
{
	for( Sint32 ii=0; ii<enBufMax; ii++ )
	{
		if(m_bUsed[ii])
		{
			SetVolume( ii , 1.0f );
		}
	}
}

void COpenAL::ResumeAllSound( Float32 fVolume )
{
	for( Sint32 ii=0; ii<enBufMax; ii++ )
	{
		if(m_bUsed[ii])
		{
			if( IsPlay(ii) )
			{
				SetVolume( ii , fVolume );
			}
		}
	}
}

gxBool COpenAL::checkError( const char* message )
{
	GX_DEBUGLOG( (char*)message );

	return gxFalse;
}


void soundALTest()
{
	static int seq = 0;
	
	if( seq == 0 )
	{
	}

	if( gxLib::Joy(0)->trg&MOUSE_L )
	{
		if( seq%2 == 0 )
		{
			gxLib::LoadAudio( 0 , "visor/wav/bgm.wav" );
			gxLib::PlayAudio( 0 , gxTrue );
		}
		else if( seq%2 == 1 )
		{
			gxLib::LoadAudio( 0 , "visor/wav/warning.wav" );
			gxLib::PlayAudio( 0 , gxTrue );
		}
		//gxLib::SetAudioPitch( 0 , 1.0f + (gxLib::Joy(0)->my-512.0f) / 1024 );
		//gxLib::SetAudioVolume( 0 , 1.0f + (gxLib::Joy(0)->my-512.0f) / 512  );
		seq ++;
	}

	if( gxLib::Joy(0)->trg&BTN_R1 )
	{
        Sint32 ax,ay;

        ax = 64;
        ay = 64;

        gxLib::DrawBox( ax-32 , ay-32 , ax+32 , ay+32 , 222 , gxTrue , ATR_DFLT , 0xff00ff00 );
/*
		gxLib::SetAudioReverb( 0 , gxTrue  );

		if( gxLib::Joy(0)->my < 256 )
		{
			gxLib::StopAudio( 0 );
		}
		else
		{
			gxLib::PlayAudio( 0 );
		}
*/
	}

	Sint32 x = gxLib::Joy(0)->mx;
	Sint32 y = gxLib::Joy(0)->my;
	gxLib::DrawBox( x-32 , y-32 , x+32 , y+32 , 222 , gxTrue , ATR_DFLT , 0xff00ff00 );

}


#endif