#include <gxLib.h>
#include <gxLib/gx.h>
#include <gxLib/gxDebug.h>
#include <gxLib/gxPadManager.h>
#include <gxLib/gxNetworkManager.h>
#include <gxLib/gxFileManager.h>

#include "SJISUTF8CnvTbl.h"

std::map<Uint32, Uint32> s_U32EncodeConvTbl;
std::map<Uint32, Uint32> s_SJISEncodeConvTbl;


Uint64 gxClock::getSec()
{
    struct tm src = { 0 };
    time_t tmz;

    src.tm_year = Year - 1900;
    src.tm_sec  = Sec;     // seconds after the minute - [0, 60] including leap second
    src.tm_min  = Min;     // minutes after the hour - [0, 59]
    src.tm_hour = Hour;    // hours since midnight - [0, 23]
    src.tm_mday = Day;     // day of the month - [1, 31]
    src.tm_mon  = Month;   // months since January - [0, 11]

    tmz = mktime(&src);

    return tmz;
}


gxClock gxClock::Now()
{
    gxClock ret;

    std::chrono::system_clock::time_point now;
    now = std::chrono::system_clock::now();
    time_t tt = std::chrono::system_clock::to_time_t(now);
    tm local_tm = *localtime(&tt);

    std::chrono::system_clock::duration tp = now.time_since_epoch();
    std::chrono::microseconds           usec = std::chrono::duration_cast<std::chrono::microseconds>(tp);

    ret.Year	= local_tm.tm_year + 1900;	// years since 1900
    ret.Month	= local_tm.tm_mon + 1; 		// months since January - [0, 11]
    ret.Day		= local_tm.tm_mday;         // day of the month - [1, 31]
    ret.DOW		= local_tm.tm_wday;         // days since Sunday - [0, 6]
    ret.Hour	= local_tm.tm_hour;         // hours since midnight - [0, 23]
    ret.Min		= local_tm.tm_min;          // minutes after the hour - [0, 59]
    ret.Sec		= local_tm.tm_sec;          // seconds after the minute - [0, 60] including leap second
    ret.MSec	= (usec.count() / 1000) % (1000);
    ret.USec	= usec.count() % (1000);

    return ret;
}

//-----------------------------------------------------------------------------------

unsigned long utf8ToUtf32(unsigned char* input, int* bytesInSequence)
{

	unsigned char c1, c2, c3, c4, c5, c6;

	*bytesInSequence = 1;
	if (!input)
	{

		return 0;
	}

	//0xxxxxxx (ASCII)      7bit
	c1 = input[0];
	if ((c1 & 0x80) == 0x00)
	{
		return c1;
	}

	//10xxxxxx              high-order byte
	if ((c1 & 0xc0) == 0x80)
	{
		return 0;
	}

	//0xFE or 0xFF          BOM (not utf-8)
	if (c1 == 0xfe || c1 == 0xFF)
	{
		return 0;
	}

	//110AAAAA 10BBBBBB     5+6bit=11bit
	c2 = input[1];
	if (((c1 & 0xe0) == 0xc0) &&
		((c2 & 0xc0) == 0x80))
	{
		*bytesInSequence = 2;


		return ((c1 & 0x1f) << 6) | (c2 & 0x3f);
	}

	//1110AAAA 10BBBBBB 10CCCCCC        4+6*2bit=16bit
	c3 = input[2];
	if (((c1 & 0xf0) == 0xe0) &&
		((c2 & 0xc0) == 0x80) &&
		((c3 & 0xc0) == 0x80))
	{
		*bytesInSequence = 3;
		return ((c1 & 0x0f) << 12) | ((c2 & 0x3f) << 6) | (c3 & 0x3f);
	}

	//1111 0AAA 10BBBBBB 10CCCCCC 10DDDDDD      3+6*3bit=21bit
	c4 = input[3];
	if (((c1 & 0xf8) == 0xf0) &&
		((c2 & 0xc0) == 0x80) &&
		((c3 & 0xc0) == 0x80) &&
		((c4 & 0xc0) == 0x80))
	{
		*bytesInSequence = 4;
		return ((c1 & 0x07) << 18) | ((c2 & 0x3f) << 12) | ((c3 & 0x3f) << 6) | (c4 & 0x3f);
	}

	//1111 00AA 10BBBBBB 10CCCCCC 10DDDDDD 10EEEEEE     2+6*4bit=26bit
	c5 = input[4];
	if (((c1 & 0xfc) == 0xf0) &&
		((c2 & 0xc0) == 0x80) &&
		((c3 & 0xc0) == 0x80) &&
		((c4 & 0xc0) == 0x80) &&
		((c5 & 0xc0) == 0x80))
	{
		*bytesInSequence = 4;
		return ((c1 & 0x03) << 24) | ((c2 & 0x3f) << 18) | ((c3 & 0x3f) << 12) | ((c4 & 0x3f) << 6) | (c5 & 0x3f);
	}

	//1111 000A 10BBBBBB 10CCCCCC 10DDDDDD 10EEEEEE 10FFFFFF        1+6*5bit=31bit
	c6 = input[5];
	if (((c1 & 0xfe) == 0xf0) &&
		((c2 & 0xc0) == 0x80) &&
		((c3 & 0xc0) == 0x80) &&
		((c4 & 0xc0) == 0x80) &&
		((c5 & 0xc0) == 0x80) &&
		((c6 & 0xc0) == 0x80))
	{
		*bytesInSequence = 4;
		return ((c1 & 0x01) << 30) | ((c2 & 0x3f) << 24) | ((c3 & 0x3f) << 18) | ((c4 & 0x3f) << 12) | ((c5 & 0x3f) << 6) | (c6 & 0x3f);
	}

	return 0;
}

std::vector<Uint32> CDeviceManager::UTF8toUTF32(gxChar* pString, size_t* pSize)
{
	//utf8->utf32に変換する

	size_t u8Len = strlen(pString);
	size_t maxSize = (u8Len+1) * 4;
	std::vector<Uint32> buf32;
	int n = 0;
	int num = 0;
	Uint32 utf32Code = 0;

	Sint32 ii = 0;
	for ( ii = 0; ii < maxSize; ii++)
	{
		if (pString[n] == 0x00) break;
		utf32Code = utf8ToUtf32((Uint8*)&pString[n], &num);
		buf32.push_back(utf32Code);
		n += num;

		if (utf32Code == 0x00000000) {
			break;
		}

	}

	if (pSize)
	{
		*pSize = buf32.size()*4;
	}

	return buf32;
}

char* ConvChU32ToU8(const char32_t u32Ch , Uint32 *size )
{
	static char u8Ch[4];
	u8Ch[0] = 0;
	u8Ch[1] = 0;
	u8Ch[2] = 0;
	u8Ch[3] = 0;

	if (u32Ch == 0)
	{
		*size = 0;
		return u8Ch;
	}

	if (u32Ch < 0 || u32Ch > 0x10FFFF) {
		*size = 0;
		return u8Ch;
	}

	if (u32Ch < 128) {
		u8Ch[0] = char(u32Ch);
		u8Ch[1] = 0;
		u8Ch[2] = 0;
		u8Ch[3] = 0;
		*size = 1;
	}
	else if (u32Ch < 2048) {
		u8Ch[0] = 0xC0 | char(u32Ch >> 6);
		u8Ch[1] = 0x80 | (char(u32Ch) & 0x3F);
		u8Ch[2] = 0;
		u8Ch[3] = 0;
		*size = 2;
	}
	else if (u32Ch < 65536) {
		u8Ch[0] = 0xE0 | char(u32Ch >> 12);
		u8Ch[1] = 0x80 | (char(u32Ch >> 6) & 0x3F);
		u8Ch[2] = 0x80 | (char(u32Ch) & 0x3F);
		u8Ch[3] = 0;
		*size = 3;
	}
	else {
		u8Ch[0] = 0xF0 | char(u32Ch >> 18);
		u8Ch[1] = 0x80 | (char(u32Ch >> 12) & 0x3F);
		u8Ch[2] = 0x80 | (char(u32Ch >> 6) & 0x3F);
		u8Ch[3] = 0x80 | (char(u32Ch) & 0x3F);
		*size = 4;
	}

	return u8Ch;
}


std::string CDeviceManager::UTF32toUTF8(std::vector<Uint32> strU32, size_t* pSize)
{
	std::vector<gxChar> buf;
	Uint32 sz = 0;
	gxChar* pData;

	for (int ii = 0; ii < strU32.size(); ii++)
	{
		pData = ConvChU32ToU8(strU32 [ii], &sz);
		if (sz == 0)
		{
			buf.push_back(0x00);
			break;
		}
		for (Sint32 jj = 0; jj < Sint32(sz);jj++)
		{
			buf.push_back(pData[jj]);
		}
	}

	buf.push_back(0x00);

	if (pSize)
	{
		*pSize = buf.size();
	}

	std::string moji = &buf[0];

	return moji;
}


void makeSJISEncodeTbl()
{
	if (s_SJISEncodeConvTbl.size() > 0) return;

	s_SJISEncodeConvTbl.clear();

	Uint32 n = 0;
	while (true)
	{
		if (EncodeConvertTbl[n][0] == 0x00000000) break;
		auto itr = s_SJISEncodeConvTbl.find(EncodeConvertTbl[n][2]);
		if (itr == s_SJISEncodeConvTbl.end())
		{
			s_SJISEncodeConvTbl[EncodeConvertTbl[n][2]] = n;
		}
		n++;
	}
}

void makeU32EncodeTbl()
{
	if( s_U32EncodeConvTbl.size() > 0 ) return;

	s_U32EncodeConvTbl.clear();

	Uint32 n = 0;
	while(true)
	{
		if( EncodeConvertTbl[n][0] == 0x00000000) break;

		s_U32EncodeConvTbl[ EncodeConvertTbl[n][1] ] = n;
		n ++;
	}

}


std::string _UTF8toSJIS( std::string str )
{
	if( s_U32EncodeConvTbl.size() == 0 )
	{
		makeU32EncodeTbl();
	}
	size_t sz=0;
	std::vector<Uint32> u32 = CDeviceManager::UTF8toUTF32((char*)str.c_str(),&sz);

	std::string ret;

	for (size_t ii = 0; ii < sz/4; ii++)
	{
		auto itr = s_U32EncodeConvTbl.find(u32[ii]);
		if (itr == s_U32EncodeConvTbl.end())
		{
			char buf[256];
			sprintf(buf, "/*{0x08%x}*/", u32[ii]);
			ret += buf;
			continue;
		}

		Uint32 index = itr->second;

		char data[5] = { 0 };
		
		data[0] = ((char*)&EncodeConvertTbl[index][2])[0];
		data[1] = ((char*)&EncodeConvertTbl[index][2])[1];
		data[2] = ((char*)&EncodeConvertTbl[index][2])[2];
		data[3] = ((char*)&EncodeConvertTbl[index][2])[3];
		data[4] = 0x00;
		ret += (char*)data;
	}

	return ret;
}

std::string _SJIStoUTF8( std::string str )
{
	if (s_SJISEncodeConvTbl.size() == 0)
	{
		makeSJISEncodeTbl();
	}

	std::string ret;

	Uint8* pStr = (Uint8*)str.c_str();

	Uint8 buf[4];

	while (true)
	{
		if (pStr[0] == 0x00) break;

		if (((*pStr >= 0x81) && (*pStr <= 0x9f)) || ((*pStr >= 0xe0) && (*pStr <= 0xfc)))
		{
			buf[0] = pStr[0];
			buf[1] = pStr[1];
			buf[2] = 0x00;
			pStr += 2;
		}
		else
		{
			buf[0] = pStr[0];
			buf[1] = 0x00;
			buf[2] = 0x00;
			pStr += 1;
		}
		buf[3] = 0x00;

		auto itr = s_SJISEncodeConvTbl.find(*(Uint32*)buf);
		if (itr == s_SJISEncodeConvTbl.end())
		{
			continue;
		}

		Uint32 index = itr->second;
		char data[5] = { 0 };

		data[0] = ((char*)&EncodeConvertTbl[index][0])[0];
		data[1] = ((char*)&EncodeConvertTbl[index][0])[1];
		data[2] = ((char*)&EncodeConvertTbl[index][0])[2];
		data[3] = ((char*)&EncodeConvertTbl[index][0])[3];
		data[4] = 0x00;
		ret += (char*)data;
	}

	return ret;
}


std::string CDeviceManager::UTF8toSJIS( gxChar* pUTF8buf, size_t* pSize)
{
	std::string src = pUTF8buf;

	auto dst = _UTF8toSJIS(src);

	if(pSize) *pSize = dst.size();

	return dst;
}

std::string CDeviceManager::SJIStoUTF8( gxChar* pSJISbuf, size_t* pSize)
{
	std::string src = pSJISbuf;

	auto dst = _SJIStoUTF8(src);

	if(pSize) *pSize = dst.size();

	return dst;

}

