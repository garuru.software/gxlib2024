﻿//-------------------------------------------------------------------------------------
//
//
// http client for Windows
//
//
//-------------------------------------------------------------------------------------

#ifndef _CHTTP_CLIENT_H_
#define _CHTTP_CLIENT_H_

#include <WININET.H>

#define NAME_USERAGENT	"VHttpClient"

class CHttpClient
{
public:

	enum {
		enRequestMax = 8,			//リクエスト可能な最大数
		enReadSize = 1024*1024,		//１度に落としてくるファイルサイズ
		enMinimumBuffer = 1024,		//最低限確保しておくファイルバッファ
	};

	enum {
		//ダウンロード状況
		enStatWait = 0,
		enStatDownloading = 1,
		enStatCompleteSuccessful = 2,
		enStatCompleteFailed = -1,
	};


	enum {
		//サーバーからのエラーコード一式
		NoError,
		Error403_Forbidden,
		Error404_NotFound,
		Error405_MethodNotAllowed,
		Error406_NotAcceptable,
		Error407_ProxyAuthenticationRequired,
		Error408_RequestTimeout,
		Error409_Conflict,
		Error410_Gone,
		Error411_LengthRequired,
		Error412_PreconditionFailed,
		Error413_PayloadTooLarge,
		Error414_URITooLong,
		Error415_UnsupportedMediaType,
		Error416_RangeNotSatisfiable,
		Error417_ExpectationFailed,
		Error418_Imateapot,
		Error421_MisdirectedRequest,
		Error424_FailedDependency,
		Error426_UpgradeRequired,
		Error451_UnavailableForLegalReasons,
		Error500_InternalServerError,
		Error501_NotImplemented,
		Error502_BadGateway,
		Error503_ServiceUnavailable,
		Error504_GatewayTimeout,
		Error505_HTTPVersionNotSupported,
		Error506_VariantAlsoNegotiates,
		Error507_InsufficientStorage,
		Error508_LoopDetected,
		Error509_BandwidthLimitExceeded,
		Error510_NotExtended,
		Error999_UnknownError,
	};


	struct StDLTaskStat {
		//ダウンロード状況の管理用

		StDLTaskStat()
		{
			pData        = NULL;
			uMaxSize     = 0;
			pURL         = NULL;
			pPostData    = NULL;
			stat         = enStatWait;
			PostDataSize = 0;
		}

		~StDLTaskStat();

		Uint8   *pData;				//ファイルへのポインタ(Deleteはしなくてよい)
		size_t  uMaxSize;			//ダウンロードすることになるファイルサイズ
		size_t  uReadSize;			//すでに読み込んだファイルサイズ
		Float32 downloadRatio;		//ダウンロード率
		char    *pURL;				//ダウンロードに使用したURI
		char    *pPostData;			//Post用のテキスト
		Uint32  PostDataSize;		//Post時のデータサイズ
		Uint32  stat;				//状況

	};


	CHttpClient();
	~CHttpClient();

	gxBool IsActive();

	//ダウンロードが完了しているか？
	gxBool IsDownloaded( Sint32 id , Float32 *fRatio = NULL );

	//ダウンロードに成功したか？
	gxBool IsSucceeded( Sint32 id  );

	//GET通信でデータを取得する、戻り値は作業ID
	Sint32 Get( char *pURL );

	//POST通信でデータを取得する
	Sint32 Post( gxChar *pURL , gxChar *pPostData , size_t postDataSize);

	//毎フレーム呼び出してね
	void Action();

	//デバッグ用状況表示
	void Draw();

	//ダウンロード済みのファイルを取得する
	Uint8* GetDownLoadFile( Sint32 id , size_t *uSize );

	//デバッグ用強制切断
	void DisConnect();

	SINGLETON_DECLARE( CHttpClient );

private:

	StDLTaskStat* getCurrent( Sint32 id = -1 );


	gxBool seqRead();
	gxBool seqReadInit();
	gxBool seqReadMain();

	//インターネット接続環境があるか？
	gxBool m_bInternetConnectionExist;

	HINTERNET m_hInternet;
	HINTERNET m_hHttpSession;
	HINTERNET m_hHttpRequest;

	Sint32 m_Sequence     = 0;
	Sint32 m_CurrentIndex = 0;
	Sint32 m_uRequestNum  = 0;

	Uint32 m_uErrorNo     = 0;
	Uint8* m_pReadBuffer  = NULL;

	StDLTaskStat m_DownLoadTask[ enRequestMax ];

	Sint32 errorCheck();

	Sint32 m_RetryCnt = 0;
};


#endif

//エラーコード一覧
//https://support.microsoft.com/ja-jp/help/193625/info-wininet-error-codes-12001-through-12156

//WiniNEtのポスト通信
//https://www.hiramine.com/programming/windows/httprequest.html
