//---------------------------------------------------------------
//
// Windowsストア向け::サウンドメイン
//
//
//
//---------------------------------------------------------------

#include <gxLib.h>
#include <gxLib/gx.h>
#include <gxLib/gxSoundManager.h>
#include <gxLib/util/CFileWave.h>
#include <gxLib/gxSoundManager.h>

#ifdef _USE_OPENAL
	//
#else

#include "CXAudio2.h"
#include <XAPOFX.h>

#pragma comment(lib, "xaudio2.lib")
//#pragma comment(lib, "XAPOFX.lib")

SINGLETON_DECLARE_INSTANCE( CXAudio );

void CAudioEngineCallbacks::Initialize( CXAudio* audio )
{
	m_pXAudio2 = audio;
}

// Called when a critical system error causes XAudio2
// to be closed and restarted. The error code is given in Error.
void _stdcall CAudioEngineCallbacks::OnCriticalError( HRESULT Error )
{
	m_pXAudio2->SetEngineExperiencedCriticalError();
}

gxBool CXAudio::Init()
{
	//1つのマスターボイスで複数のAudioをミキシングできる
	HRESULT hr;

	hr = XAudio2Create( &m_pXAudioEngine );

	if FAILED( hr )
	{
		return gxFalse;
	}

	m_pXAudioEngineCallback.Initialize( this );

	m_pXAudioEngine->RegisterForCallbacks( &m_pXAudioEngineCallback );

	//ゲームカテゴリでマスターボイスを作成する

	hr = m_pXAudioEngine->CreateMasteringVoice( &m_XA2MasterVoice, 2, 48000, 0, NULL, NULL, AudioCategory_GameEffects );

	if FAILED( hr )
	{
		return gxFalse;
	}

	return gxTrue;
}


gxBool CXAudio::clear()
{
	if( m_XA2MasterVoice )
	{
		m_XA2MasterVoice->DestroyVoice();
	}

	if( m_pXAudioEngine )
	{
		m_pXAudioEngine->Release();
		m_pXAudioEngine = NULL;
	}

	return gxTrue;
}


void CXAudio::release()
{
	for (int ii = 0; ii < enSoundMax; ii++)
	{
		if ( m_Sound[ii].m_SourceVoice != NULL )
		{
			m_Sound[ii].m_SourceVoice->DestroyVoice();
			//SAFE_DELETES( m_Sound[ii].m_pFileWave);
		}

		m_Sound[ii].m_SourceVoice = NULL;
	}
}

void CXAudio::Action()
{
	Float32 fMasterVolume = gxSoundManager::GetInstance()->GetMasterVolume();

	setMasterVolume( fMasterVolume );

	for (Sint32 ii = 0; ii < MAX_SOUND_NUM; ii++)
	{
		StPlayInfo *pAudioInfo = gxSoundManager::GetInstance()->GetPlayInfo(ii);

		if ( pAudioInfo->bReq )
		{
			//load
			if (pAudioInfo->uStatus & enSoundReqLoad)
			{
				loadWavFile( ii , pAudioInfo->fileName );
			}

			//stop
			if (pAudioInfo->uStatus & enSoundReqStop)
			{
				stopSoundEffect(ii);
				gxSoundManager::GetInstance()->GetPlayInfo(ii)->bPlayNow = gxFalse;
			}

			//play
			if (pAudioInfo->uStatus & enSoundReqPlay)
			{
				playSoundEffect(ii);
				gxSoundManager::GetInstance()->GetPlayInfo(ii)->bPlayNow = gxTrue;
			}

			//volume
			if (pAudioInfo->uStatus & enSoundReqVolume )
			{
				setVolume( ii , gxSoundManager::GetInstance()->GetPlayInfo(ii)->fVolume );
			}

			//freq
			if (pAudioInfo->uStatus & enSoundReqChangeFreq)
			{
				setSoundFrequency(ii, pAudioInfo->fFreqRatio );
			}

			//reverb
			if ( pAudioInfo->uStatus & enSoundReqSetReverb )
			{
				setReverb( ii, pAudioInfo->bReverb );
			}

			//pan
			if ( pAudioInfo->uStatus & enSoundReqChangePan )
			{
				setPan( ii , pAudioInfo->fPanning );
			}

			pAudioInfo->bReq = gxFalse;
			pAudioInfo->uStatus = enSoundReqNone;

		}

		gxSoundManager::GetInstance()->GetPlayInfo(ii)->bPlayNow = isPlay(ii);
	}
}


gxBool CXAudio::loadWavFile( Sint32 n , char *pFileName )
{
	//----------------------------------------------
	//wavファイルの読み込み
	//----------------------------------------------

	stopSoundEffect( n );	//とりあえず止める

	if (m_Sound[n].m_SourceVoice == NULL )
	{

	}
	else
	{
		m_Sound[n].m_SourceVoice->Stop();
		m_Sound[n].m_SourceVoice->FlushSourceBuffers();
		m_Sound[n].m_SourceVoice->SubmitSourceBuffer(&m_Sound[n].m_AudioBuf);
	}

	Uint8 *pData               = gxSoundManager::GetInstance()->GetPlayInfo(n)->m_WaveData.GetPCMImage();
	size_t uSize               = gxSoundManager::GetInstance()->GetPlayInfo(n)->m_WaveData.GetFileSize();
	StWAVEFORMATEX waveFotrmat = *(StWAVEFORMATEX*)gxSoundManager::GetInstance()->GetPlayInfo(n)->m_WaveData.GetFormat();

	if( !readPCMFile( n , pData , uSize , &waveFotrmat ) )
	{
		GX_DEBUGLOG("[gxLib::XAudio2::Error] AudioFile[%d] decode error" , n );
		return gxFalse;
	}

	return gxTrue;
}

gxBool CXAudio::readPCMFile( Sint32 n , Uint8 *pData, Uint32 uSize , StWAVEFORMATEX* pFormat )
{
	//PCMデータを読む(WAVではない）

	HRESULT hr;

	XAUDIO2_SEND_DESCRIPTOR descriptors[1];
	descriptors[0].pOutputVoice = m_XA2MasterVoice;
	descriptors[0].Flags = 0;
/*
	descriptors[1].pOutputVoice = m_soundEffectReverbVoiceSmallRoom;
	descriptors[1].Flags = 0;
	descriptors[2].pOutputVoice = m_soundEffectReverbVoiceLargeRoom;
	descriptors[2].Flags = 0;
*/
	XAUDIO2_VOICE_SENDS sends = {0};
	sends.SendCount = 1;
	sends.pSends = descriptors;

	//プロパティをセットされたリソースをエンジンに登録する

	hr = m_pXAudioEngine->CreateSourceVoice(
			&m_Sound[n].m_SourceVoice,
			(WAVEFORMATEX*)pFormat,
			0,
			2.0f,
			&m_voiceContext,
			&sends);

	if FAILED( hr )
	{
		return gxFalse;
	}

	m_Sound[n].m_uSampleRate = pFormat->nSamplesPerSec;

	//オーディオバッファをクリア

	ZeroMemory(&m_Sound[n].m_AudioBuf, sizeof(m_Sound[n].m_AudioBuf) );


	//オーディオバッファの設定

	m_Sound[n].m_AudioBuf.AudioBytes= uSize;
	m_Sound[n].m_AudioBuf.pAudioData= pData;
	m_Sound[n].m_AudioBuf.pContext	= &m_Sound[n];
	m_Sound[n].m_AudioBuf.Flags	 = XAUDIO2_END_OF_STREAM;

	StPlayInfo *pAudioInfo = gxSoundManager::GetInstance()->GetPlayInfo( n );

	m_Sound[n].m_AudioBuf.LoopCount = 0;
	//m_Sound[n].m_pSoundData = pData;

	setVolume( n , pAudioInfo->fVolume );
	setSoundFrequency( n , pAudioInfo->fFreqRatio);
	hr = m_Sound[n].m_SourceVoice->SubmitSourceBuffer( &m_Sound[n].m_AudioBuf );

	if FAILED( hr )
	{
		return gxFalse;
	}

	stopSoundEffect( n );

	return gxTrue;
}


void CXAudio::playSoundEffect(int sound)
{
	//if( m_Sound[sound].m_pSoundData == NULL )
	//{
	//	//オーディオデータがなかった
	//	return;
	//}

	stopSoundEffect( sound );

	XAUDIO2_BUFFER buf = {0};
	XAUDIO2_VOICE_STATE state = {0};

	if (m_bEngineCriticalError)
	{
		// If there's an error, then we'll recreate the engine on the next
		// render pass.
		return;
	}

	StSoundEffectData* pSoundData = &m_Sound[ sound ];

	if (pSoundData->m_SourceVoice == NULL) return;

	//reverb

	if( 0 )
	{
		static ::IUnknown* reverb;
		StSoundEffectData* pSoundData = &m_Sound[ sound ];

		//-------------------------------------------------------------------------
		//reverbテスト
		//-------------------------------------------------------------------------
		static ::XAUDIO2_EFFECT_CHAIN chain;

		::CreateFX(__uuidof(::FXReverb),&reverb);

		::XAUDIO2_EFFECT_DESCRIPTOR desc;
		desc.InitialState= TRUE;
		desc.OutputChannels = 2;	// 出力チャンネル数
		desc.pEffect	 = reverb; // エフェクトへのポインタ

		chain.pEffectDescriptors = &desc; // Descriptorへのポインタ、複数個接続する場合は配列の先頭
		chain.EffectCount		= 1; 	

		pSoundData->m_SourceVoice->SetEffectChain( &chain );

		::FXREVERB_PARAMETERS param;
		param.Diffusion = FXREVERB_DEFAULT_DIFFUSION; // 散布量（拡散量？）
		param.RoomSize= FXREVERB_DEFAULT_ROOMSIZE;// 音が鳴っている施設の大きさを示す

		pSoundData->m_SourceVoice->SetEffectParameters(0,&param,sizeof(FXREVERB_PARAMETERS));

		reverb->Release();
	}

	//pan
	setPan( sound , 0 );

	Float32 fVolume = gxSoundManager::GetInstance()->GetPlayInfo( sound )->fVolume;

	fVolume *= m_fMasterVolume;

	if( !CWindows::GetInstance()->IsSoundEnable() )
	{
		fVolume = 0.0f;
		return;
	}

	setVolume( sound , fVolume );

	//-------------------------------------------------------------------------

	HRESULT hr = pSoundData->m_SourceVoice->Start();

	if FAILED( hr )
	{
		SetEngineExperiencedCriticalError();
		return;
	}

	if ( gxSoundManager::GetInstance()->GetPlayInfo(sound)->bLoop )
	{
		pSoundData->m_AudioBuf.LoopBegin  = 0;
		pSoundData->m_AudioBuf.LoopLength = 0;
		pSoundData->m_AudioBuf.LoopCount  = XAUDIO2_LOOP_INFINITE;
	}
	else
	{
		pSoundData->m_AudioBuf.LoopBegin  = 0;
		pSoundData->m_AudioBuf.LoopLength = 0;
		pSoundData->m_AudioBuf.LoopCount  = 0;
	}

	pSoundData->m_SourceVoice->GetState( &state, XAUDIO2_VOICE_NOSAMPLESPLAYED );
	pSoundData->m_SourceVoice->SubmitSourceBuffer( &pSoundData->m_AudioBuf );


	m_Sound[sound].m_bPlay = gxTrue;

}


void CXAudio::stopSoundEffect( int sound )
{
	m_Sound[sound].m_bPlay = gxFalse;

	if ( m_bEngineCriticalError )
	{
		return;
	}

	//if( m_Sound[sound].m_pSoundData == NULL )
	//{
	//	//オーディオデータがなかった
	//	return;
	//}

	if (m_Sound[sound].m_SourceVoice == NULL)
	{
		return;
	}

	m_Sound[sound].m_SourceVoice->FlushSourceBuffers();

	HRESULT hr = m_Sound[sound].m_SourceVoice->Stop();

	if FAILED( hr )
	{
		// If there's an error, then we'll recreate the engine on the next render pass
		SetEngineExperiencedCriticalError();
		return;
	}

	m_Sound[sound].m_SourceVoice->FlushSourceBuffers();

}

void CXAudio::setSoundFrequency( int sound , Float32 ratio )
{
	//if( m_Sound[sound].m_pSoundData == NULL )
	//{
	//	return;
	//}

	Float32 fRatio = gxSoundManager::GetInstance()->GetPlayInfo(sound)->fFreqRatio;

	if( m_Sound[sound].m_SourceVoice )
	{
		m_Sound[sound].m_SourceVoice->SetFrequencyRatio( fRatio );
	}

}

void CXAudio::setReverb( int sound , gxBool bEnable )
{
	StSoundEffectData* pSoundData = &m_Sound[ sound ];

	if (m_Sound[sound].m_SourceVoice == NULL)
	{
		return;
	}

	if (bEnable)
	{
		::IUnknown* reverb;
		StSoundEffectData* pSoundData = &m_Sound[sound];

		//-------------------------------------------------------------------------
		//reverbテスト
		//-------------------------------------------------------------------------
		static ::XAUDIO2_EFFECT_CHAIN chain;

		::CreateFX(__uuidof(::FXReverb), &reverb);

		::XAUDIO2_EFFECT_DESCRIPTOR desc;
		desc.InitialState = TRUE;
		desc.OutputChannels = 2;	// 出力チャンネル数
		desc.pEffect = reverb; // エフェクトへのポインタ

		chain.pEffectDescriptors = &desc; // Descriptorへのポインタ、複数個接続する場合は配列の先頭
		chain.EffectCount = 1;

		pSoundData->m_SourceVoice->SetEffectChain(&chain);

		::FXREVERB_PARAMETERS param;
		param.Diffusion = FXREVERB_DEFAULT_DIFFUSION; // 散布量（拡散量？）
		param.RoomSize = FXREVERB_DEFAULT_ROOMSIZE;// 音が鳴っている施設の大きさを示す

		pSoundData->m_SourceVoice->SetEffectParameters(0, &param, sizeof(FXREVERB_PARAMETERS));

		reverb->Release();
	}
	else
	{
		pSoundData->m_SourceVoice->DisableEffect(0);
	}
}


void CXAudio::setPan( int sound , Float32 pan )
{

	StSoundEffectData* pSoundData = &m_Sound[ sound ];

	float outputMatrix[8] = {1.0f , 0.0f, };

	DWORD dwChannelMask;
	m_XA2MasterVoice->GetChannelMask( &dwChannelMask );

	float left = 0.5f - pan / 2;
	float right = 0.5f + pan / 2; 

    outputMatrix[0] = left;
    outputMatrix[1] = right;

	switch (dwChannelMask)
	{
	//case SPEAKER_MONO:
	case 1:
	    outputMatrix[0] = 1.0;
	    break;
	//case SPEAKER_STEREO:
	//case SPEAKER_2POINT1:
	//case SPEAKER_SURROUND:
	case 2:
	case 3:
	    outputMatrix[0] = left;
	    outputMatrix[1] = right;
	    break;
	//case SPEAKER_QUAD:
	case 4:
	    outputMatrix[0] = outputMatrix[2] = left;
	    outputMatrix[1] = outputMatrix[3] = right;
	    break;
	//case SPEAKER_4POINT1:
	case 5:
	    outputMatrix[ 0 ] = outputMatrix[ 3 ] = left;
	    outputMatrix[ 1 ] = outputMatrix[ 4 ] = right;
	    break;
	//case SPEAKER_5POINT1:
	//case SPEAKER_7POINT1:
	//case SPEAKER_5POINT1_SURROUND:
	case 6:
	case 7:
	    outputMatrix[ 0 ] = outputMatrix[ 4 ] = left;
	    outputMatrix[ 1 ] = outputMatrix[ 5 ] = right;
	    break;
	//case SPEAKER_7POINT1_SURROUND:
	case 8:
	    outputMatrix[ 0 ] = outputMatrix[ 4 ] = outputMatrix[ 6 ] = left;
	    outputMatrix[ 1 ] = outputMatrix[ 5 ] = outputMatrix[ 7 ] = right;
	    break;
	default:
	    outputMatrix[0] = left;
	    outputMatrix[1] = right;
	    break;
	}

	XAUDIO2_VOICE_DETAILS VoiceDetails;
	pSoundData->m_SourceVoice->GetVoiceDetails(&VoiceDetails);

	XAUDIO2_VOICE_DETAILS MasterVoiceDetails;
	m_XA2MasterVoice->GetVoiceDetails(&MasterVoiceDetails);

	pSoundData->m_SourceVoice->SetOutputMatrix( NULL, VoiceDetails.InputChannels, MasterVoiceDetails.InputChannels, outputMatrix );
}


gxBool CXAudio::isPlay( int sound )
{
	if ( m_bEngineCriticalError )
	{
		return gxFalse;
	}

	//if( m_Sound[sound].m_pSoundData == NULL )
	//{
	//	//オーディオデータがなかった
	//	return gxFalse;
	//}

	if (m_Sound[sound].m_SourceVoice == NULL)
	{
		return gxFalse;
	}

	XAUDIO2_VOICE_STATE xa2state;
	m_Sound[sound].m_SourceVoice->GetState( &xa2state );

	if (xa2state.BuffersQueued == 0)
	{
		return gxFalse;
	}

	return gxTrue;

}

void CXAudio::setVolume( Sint32 no , Float32 fVolume )
{
	//ボリューム設定

	if( fVolume <= 0.0f ) fVolume = 0.0f;
	if( fVolume >= 1.0f ) fVolume = 1.0f;

	if( m_Sound[no].m_SourceVoice )
	{
		//Float32 fVolume2 = fVolume * m_fMasterVolume;
		//Float32 fVolume3 = (Float32)(log10( 100.0f - fVolume2*100.0f ) );
		//Float32 fVolume4 = (Float32)pow( 100.0f, fVolume3 ) ;

		m_Sound[no].m_SourceVoice->SetVolume( fVolume * m_fMasterVolume	);
	}
}


void CXAudio::setMasterVolume( Float32 fMasterVolume )
{
	//マスターボリューム設定

	if( fMasterVolume <= 0.0f ) fMasterVolume = 0.0f;
	if( fMasterVolume >= 1.0f ) fMasterVolume = 1.0f;

//	if( m_fMasterVolume == fMasterVolume ) return;

	m_fMasterVolume = fMasterVolume;

	for (Sint32 ii = 0; ii < MAX_SOUND_NUM; ii++)
	{
		if( !CWindows::GetInstance()->IsSoundEnable() )
		{
			if( m_Sound[ii].m_SourceVoice )
			{
				m_Sound[ii].m_SourceVoice->SetVolume( 0.0f );
				continue;
			}
		}
		else
		{
			StPlayInfo *pAudioInfo = gxSoundManager::GetInstance()->GetPlayInfo( ii );
			

			if( m_Sound[ii].m_SourceVoice )
			{
				m_Sound[ii].m_SourceVoice->SetVolume( pAudioInfo->fVolume * m_fMasterVolume );
			}
		}
	}
}
#endif
