﻿//------------------------------------------------------------
//
// machine.cpp
// マシン固有のデバイスへのアクセスを行うものはここに書く
// ハードウェアに依存する部分
//
//------------------------------------------------------------
#include <gxLib.h>
#include <gxLib/gx.h>
#include <gxLib/gxFileManager.h>
//#include <gxLib/gxDebug.h>
//#include "gxLibResource.h"
#include "../CHttpClient.h"
#include "../../CFileSystem.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <io.h>
#include <fcntl.h>
#include <shellapi.h>	//<- VC2017ではこっち

SINGLETON_DECLARE_INSTANCE( CDeviceManager );

#define NAME_APRICATION APPLICATION_NAME
#define STORAGE_PATH_ROM  "Storage\\01_rom"
#define STORAGE_PATH_DISK "Storage\\03_disk"

static wchar_t WCharBuf[1024];
static wchar_t* pWChar = WCharBuf;// NULL;

enum class EGXDir {
	eGXDirectoryNeedSearch,
	eGXDirectoryExist,
	eGXDirectoryDisable,
};

static EGXDir s_bEnableGxDirectory = EGXDir::eGXDirectoryNeedSearch;
Uint8* loadFile2(const gxChar* pFileNameU8, size_t* pLength);

//------------------------------------------------------------

CDeviceManager::CDeviceManager()
{
	CHttpClient::CreateInstance();
}


CDeviceManager::~CDeviceManager()
{
	//CHttpClient::DeleteInstance();
}


void CDeviceManager::AppInit()
{
	static gxBool bFirst = gxTrue;

	if( bFirst )
	{
		bFirst = gxFalse;
		CGameGirl::GetInstance()->Init();
		//CGraphics::GetInstance()->Init();
		//CAudio::GetInstance()->Init();
	    //CGamePad::GetInstance()->Init();
		//gxLib::SetVirtualPad( CWindows::GetInstance()->m_bVirtualPad );
	}
	else
	{
        CGameGirl::GetInstance()->SetResume();
	}

	CGameGirl::GetInstance()->AdjustScreenResolution();

}


void CDeviceManager::GameInit()
{
	//バックで受け取った入力機器の情報をgxLib側へ更新する

	//CGamePad::GetInstance()->Action();
}


gxBool CDeviceManager::GamePause()
{
	return ::GamePause();
}

gxBool CDeviceManager::GameUpdate()
{
	return gxTrue;
}


void CDeviceManager::Render()
{
	//描画処理
	
	if( CGameGirl::GetInstance()->IsResume() )
	{
		return;
	}

//	COpenGLES2::GetInstance()->Update();
//	COpenGLES2::GetInstance()->Render();
}


void CDeviceManager::vSync()
{
	//1/60秒の同期待ち
	static Float32 _TimeOld = CGameGirl::GetInstance()->GetTime();

	Float32 _TimeNow;
	do
	{
		_TimeNow = CGameGirl::GetInstance()->GetTime();

	}
	while( _TimeNow < ( _TimeOld + (1.0f/ FRAME_PER_SECOND) ) );

	_TimeOld = _TimeNow;

}


void CDeviceManager::Flip()
{
	//バックバッファに用意した画像を転送する

	//CGraphics::GetInstance()->Present();
}

void CDeviceManager::Resume()
{

}


void CDeviceManager::Movie()
{
	
}


void CDeviceManager::Play()
{
	//CAudio::GetInstance()->Action();
}


gxBool CDeviceManager::NetWork()
{
	if (!CHttpClient::IsExistInstance()) return gxTrue;

	CHttpClient::GetInstance()->Action();

	return gxTrue;
}


void CDeviceManager::UploadTexture(Sint32 sBank)
{
	//CGraphics::GetInstance()->ReadTexture( sBank );
}


void CDeviceManager::LogDisp(char* pString)
{
	auto SJIS = CDeviceManager::UTF8toSJIS(pString);

//	printf("%s" LF , pSJIS);
	printf("%s\n" , SJIS.c_str());
}


void CDeviceManager::ToastDisp( gxChar* pMessage )
{
	LogDisp("-------------------------------------------");
	LogDisp(pMessage);
	LogDisp("-------------------------------------------");

}


void CDeviceManager::OpenWebClient( gxChar* pString , gxBool bOpenWebBrowser )
{
    //WebViewを表示するリクエストを発行する
	Uint32 len = (Uint32)strlen(pString);
#if 1
	wchar_t wStr[0xff];
	len = MultiByteToWideChar(CP_ACP, 0, (char*)pString, (int)strlen((char*)pString), wStr, 0xff);
	wStr[len] = 0x0000;
	//ShellExecuteW( CWindows::GetInstance()->m_hWindow, L"open",  wStr, NULL, NULL, SW_SHOWNORMAL);
	ShellExecuteW(nullptr, L"open", wStr, NULL, NULL, SW_SHOWNORMAL);
#else
	ShellExecute(CWindows::GetInstance()->m_hWindow, _T("open"), Appname, NULL, NULL, SW_SHOWNORMAL);
#endif
}


gxBool CDeviceManager::SetAchievement( Uint32 achieveindex )
{
	gxLib::SetToast("Achievement (%d)" , achieveindex );

	return gxTrue;
}

gxBool CDeviceManager::GetAchievement( Uint32 achieveindex )
{
	return gxTrue;
}

Uint8* CDeviceManager::LoadFile( const gxChar* pFileNameU8 , size_t* pLength , ESTORAGE_LOCATION location )
{
	std::string url = pFileNameU8;
	std::replace(url.begin(), url.end(), '/', '\\');
	std::string fileName = "";

	if (std::string::npos != url.find(':'))
	{
		location = ESTORAGE_LOCATION::EXTERNAL;
	}

	if (s_bEnableGxDirectory == EGXDir::eGXDirectoryNeedSearch )
	{
		//Storageディレクトリが存在しない場合はexeの実行環境にファイルを保存する

		if (CFileSystem::IsExist(STORAGE_PATH_ROM))
		{
			//存在する
			s_bEnableGxDirectory = EGXDir::eGXDirectoryExist;
		}
		else
		{
			//存在しない
			s_bEnableGxDirectory = EGXDir::eGXDirectoryDisable;
		}
	}

	if (s_bEnableGxDirectory == EGXDir::eGXDirectoryDisable)
	{
		//Storage/01_romフォルダがない場合は実行ファイル直下にファイルを読みに行く
		location = ESTORAGE_LOCATION::EXTERNAL;
	}

	Uint8 *pData = nullptr;

	switch( location ){
	case ESTORAGE_LOCATION::ROM:

		//rom
		fileName = STORAGE_PATH_ROM;
		fileName += '\\';
		fileName += url;
		pData = loadFile2(fileName.c_str(), pLength);
		break;

	case ESTORAGE_LOCATION::INTERNAL:
		//disk
		fileName = STORAGE_PATH_DISK;
		fileName += '\\';
		fileName += url;
		pData = loadFile2(fileName.c_str(), pLength);
		break;

	case ESTORAGE_LOCATION::EXTERNAL:
	default:
		fileName = url;
		pData = loadFile2(fileName.c_str(), pLength);
		break;
	}

	return pData;
}


gxBool CDeviceManager::SaveFile( const gxChar* pFileNameU8 , Uint8* pReadBuf , size_t uSize, ESTORAGE_LOCATION location )
{
	//ファイルの書き込み

	Uint32 len = gxUtil::StrLen(pFileNameU8);

	std::string url = pFileNameU8;
	std::replace(url.begin(), url.end(), '/', '\\');
	std::string fileName = "";

	if (s_bEnableGxDirectory == EGXDir::eGXDirectoryNeedSearch)
	{
		//Storageディレクトリが存在しない場合はexeの実行環境にファイルを保存する

		if (CFileSystem::IsExist(STORAGE_PATH_DISK))
		{
			//存在する
			s_bEnableGxDirectory = EGXDir::eGXDirectoryExist;
		}
		else
		{
			//存在しない
			s_bEnableGxDirectory = EGXDir::eGXDirectoryDisable;
		}
	}

	if (std::string::npos != url.find(':'))
	{
		location = ESTORAGE_LOCATION::EXTERNAL;
	}
	else
	{
		// : を発見できなかったらカレントディレクトリ直下に作成
		bool bCurDirWrite = false;
		if (s_bEnableGxDirectory == EGXDir::eGXDirectoryDisable) bCurDirWrite = true;

		if (location == ESTORAGE_LOCATION::EXTERNAL) bCurDirWrite = true;

		if(bCurDirWrite)
		{
			char buf[1024];
			GetCurrentDirectory(1024, buf);	//sjisで来るので注意！！
			std::string tmp;
			tmp = buf;

			size_t uSizePath = 0;
			tmp = SJIStoUTF8((char*)tmp.c_str(), &uSizePath);
			
			tmp += '\\';
			tmp += url;
			url = tmp;
		}
	}

	switch (location) {
	case ESTORAGE_LOCATION::ROM:
		return gxFalse;

	case ESTORAGE_LOCATION::INTERNAL:
		fileName = STORAGE_PATH_DISK;
		fileName += '\\';
		fileName += url;
		if (s_bEnableGxDirectory == EGXDir::eGXDirectoryDisable)
		{
			fileName = url;
		}
		break;

	case ESTORAGE_LOCATION::EXTERNAL:
	default:
		fileName = url;
		break;
	}

	if (pReadBuf == nullptr)
	{
		//ファイル削除

		CFileSystem::RemoveSJIS(fileName);

		std::string temp = fileName;

		if (temp.back() == '/' || temp.back() == '\\')
		{
			temp.pop_back();
			CFileSystem::CreateDirectorySJIS(temp);
		}

		return true;
	}

	if (!CFileSystem::WriteFileSJIS(fileName, pReadBuf, uSize))
	{
		//SJISで書き出し成功できなかった
		if (!CFileSystem::WriteFile(fileName, pReadBuf, uSize))
		{
			return gxFalse;
		}
	}

	return gxTrue;
}


void CDeviceManager::MakeThread( void* (*pFunc)(void*) , void * pArg )
{
	// スレッドの作成
	//void func( void *arg )型の関数ポインタを渡すこと！ 

	DWORD threadId;
	HANDLE hThread;

	hThread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)pFunc, (LPVOID)pArg, CREATE_SUSPENDED, &threadId); 
	if (hThread)
	{
		SetThreadPriority(hThread, THREAD_PRIORITY_HIGHEST);

		// スレッドの起動 
		ResumeThread(hThread);
	}
}


void CDeviceManager::Sleep( Uint32 msec )
{
	::Sleep( msec );
}


gxBool CDeviceManager::PadConfig()
{
	//非対応
	return gxTrue;
}


gxBool CDeviceManager::PadConfig( Sint32 padNo , Uint32 button )
{
	return gxTrue;
}


std::vector<std::string> CDeviceManager::GetDirList( std::string rootDirUTF8 )
{
	//U8をSJISに変換
	//※Zipからの展開だとZipに内包されたファイル名がSJISでわたってくる可能性があるが
	//UTF8で取り回す仕様で固定する

	size_t uSizePath = 0;
	auto sjis = UTF8toSJIS((gxChar*)rootDirUTF8.c_str(), &uSizePath);

	return CFileSystem::ScanDirectory(sjis);
}

#include <shlobj.h>
Float32 CDeviceManager::GetFreeStorageSize()
{
	LPITEMIDLIST pidlist;

	char szPathName[MAX_PATH];
	HRESULT ret = SHGetSpecialFolderLocation( NULL, CSIDL_PERSONAL, &pidlist );
	if (ret == S_OK)
	{
		SHGetPathFromIDList(pidlist, szPathName);
		std::string cPath = szPathName;

		ULARGE_INTEGER FreeAvailableSize, TotalSize, FreeSize;
		if (GetDiskFreeSpaceEx(cPath.c_str(), &FreeAvailableSize, &TotalSize, &FreeSize))
		{
			size_t sz = FreeAvailableSize.QuadPart / 1024;

			return Float32(sz / 1024.0f);
		}
	}

	return 0.0f;
}


Uint8* loadFile2( const gxChar* pFileNameU8, size_t* pLength )
{
	std::vector<uint8_t> buf;

	if (CFileSystem::ReadFileSJIS(pFileNameU8, buf))
	{
		uint8_t* p = nullptr;
		if (buf.size())	//0byteのファイルの場合もありうる
		{
			p = new Uint8[buf.size() + 1];
			gxUtil::MemCpy(p, &buf[0], buf.size());
			p[buf.size()] = 0x00;
		}

		if (pLength)
		{
			*pLength = buf.size();
		}

		return p;
	}

	if (CFileSystem::ReadFile(pFileNameU8, buf))
	{
		//if (buf.size())	//0byteのファイルの場合もありうる
		{
			uint8_t* p = new Uint8[buf.size() + 1];
			gxUtil::MemCpy(p, &buf[0], buf.size());
			p[buf.size()] = 0x00;

			if (pLength)
			{
				*pLength = buf.size();
			}
			return p;
		}
	}

	return nullptr;
}


std::string CDeviceManager::GetClipBoardString()
{
	//未対応

	//クリップボードのテキストを参照する
	std::string str;

	HGLOBAL hMem;       // 取得用のメモリ変数
	LPTSTR  lpBuff;     // 参照用のポインタ
	//gxChar* p = NULL;

	if (OpenClipboard(nullptr))
	{
		if ((hMem = GetClipboardData(CF_TEXT)) != NULL)
		{
			if ((lpBuff = (LPTSTR)GlobalLock(hMem)) != NULL)
			{
				Uint32 len = (Uint32)strlen(lpBuff);
				//p= new gxChar[len];
				//sprintf( p , "%s" , lpBuff );

				str = CDeviceManager::GetInstance()->SJIStoUTF8(lpBuff);
				//str = lpBuff;

				GlobalUnlock(hMem);
			}
		}
		CloseClipboard();
	}

	return str;
}

void CDeviceManager::SetClipBoardString(std::string str)
{
	HGLOBAL     hMem;       // 設定用のメモリ変数
	LPTSTR      lpBuff;     // 複写用のポインタ
	DWORD       dwSize;     // 複写元の長さ

	dwSize = (DWORD)((str.size() + 1) * sizeof(TCHAR));   // '\0' 文字分を加算

	//make
	if ((hMem = GlobalAlloc((GHND | GMEM_SHARE), dwSize)) != NULL)
	{
		if ((lpBuff = (LPTSTR)GlobalLock(hMem)) != NULL)
		{
			lstrcpy(lpBuff, str.c_str());                  // lpBuff にコピー
			GlobalUnlock(hMem);

			//if (OpenClipboard(CWindows::GetInstance()->m_hWindow))
			if (OpenClipboard(nullptr))
			{
				EmptyClipboard();
				SetClipboardData(CF_TEXT, hMem);      // データを設定
				CloseClipboard();
				GlobalFree(hMem);                     // クリップボードが開けないとき解放
				return;
			}
		}
		GlobalFree(hMem);                             // クリップボードが開けないとき解放
	}
}


//-----------------------------------------
//専用
//-----------------------------------------
