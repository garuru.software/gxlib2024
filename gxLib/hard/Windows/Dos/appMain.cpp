﻿//------------------------------------------------------------
//
// machine.cpp
// マシン固有のデバイスへのアクセスを行うものはここに書く
// ハードウェアに依存する部分
//
//------------------------------------------------------------
#include <gxLib.h>
#include <gxLib/gx.h>
#include "../CHttpClient.h"

int main(int argc, char** argv)
{
	{
		CDeviceManager::GetInstance()->SetBatchMode();
		CDeviceManager::GetInstance()->SetAutoSave(gxFalse);
	}

	for (Sint32 ii = 0; ii < argc; ii++)
	{
		auto args = CDeviceManager::SJIStoUTF8((gxChar*)argv[ii]);
		CGameGirl::GetInstance()->DragAndDropArgs.push_back(args);
	}

	CDeviceManager::GetInstance()->AppInit();

	CGameGirl::GetInstance()->Init();

	CGameGirl::GetInstance()->AdjustScreenResolution();

	while (gxTrue)
	{
		CGameGirl::GetInstance()->Action();

		if (CGameGirl::GetInstance()->IsAppFinish())
		{
			break;
		}

		gxLib::Sleep(1);
	}

	CGameGirl::GetInstance()->End();

	while (CGameGirl::GetInstance()->IsGameMainThreadExist())
	{
		gxLib::Sleep(10);
	}

	CGameGirl::DeleteInstance();

	CDeviceManager::DeleteInstance();

	gxLib::Destroy();

	return 0;
}
