﻿// --------------------------------------------------------------------
//
// つじつまあわせ用共通ヘッダ
//
// --------------------------------------------------------------------

//#define FILE_FROM_ZIP

//---------------------------------------
//以下自動設定
//---------------------------------------

#define GX_BUILD_OPTIONx64

// ヘッダー ファイル:

#define WIN32_LEAN_AND_MEAN             // Windows ヘッダーから使用されていない部分を除外します。
#include <windows.h>
#include <windowsx.h>
#include <assert.h>
#include <math.h>

// C ランタイム ヘッダー ファイル
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include <locale.h>


// C++ ランタイム ヘッダー ファイル
#include <string>
#include <iostream>
#include <map>
#include <vector>
#include <functional>
#include <cstdlib>
#include <chrono>
#include <algorithm>

#pragma warning(disable : 4996)
#pragma warning(disable : 4819)
#pragma warning(disable : 4244)

//----------------------------------------------------
//プラットフォーム専用関数
//----------------------------------------------------
