﻿#ifndef _COpenGLWin_H_
#define _COpenGLWin_H_

#ifdef _USE_OPENGL
#include <gxLib/hard/COpenGLES3.h>
#include <gxLib/gxTexManager.h>


class COpenGLWin : public COpenGLES3
{
public:

	COpenGLWin();
	~COpenGLWin();

	void Init();
	void Reset();
	void SwapBuffer();
	void GetFrameBufferImage(gxChar* pLength);

	SINGLETON_DECLARE( COpenGLWin );

private:

	HGLRC m_hRC;

};
#endif

#endif

