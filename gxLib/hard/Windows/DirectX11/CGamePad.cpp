﻿//------------------------------------------------------------------
//
// 入力デバイス状態を管理
//
//------------------------------------------------------------------

#include <gxLib.h>
#include <gxLib/gx.h>
#include <gxLib/gxPadManager.h>
#include <gxLib/gxFileManager.h>
#include "CGamePad.h"
#include "CDirectInput.h"
#include "CXInput.h"

#define NOT_USE_ID (PLAYER_MAX)

#define _PAD_ASSIGN_CONFIG_FILE_ "gxLib/padDeviceConfig.dic"

typedef struct StPadAssign {
	Uint32 version = VERSION_NUMBER;
	Uint8 playerID = 0;
	Uint8 bDisable = 0;
	Uint8 enableConfig = 0;
	gxChar name[256] = {};
	Uint8  button[BTN_MAX] = {};
	Uint8  analog[9] = {
		gxPadManager::ANALOG_LY,
		gxPadManager::ANALOG_LX,
		gxPadManager::ANALOG_RY,
		gxPadManager::ANALOG_RX,
		gxPadManager::ANALOG_LT,
		gxPadManager::ANALOG_RT,
	};
	Uint8  keyboard[32] = {};

} StPadAssign;


SINGLETON_DECLARE_INSTANCE( CGamePad );

#define AXIS_RANGE (1.0f)
#define PRIORITY_DEBUG_MAX ( MAX_PRIORITY_NUM )

extern gxKey::KeyType KeyConfigTbl[32];

const GAMINGDEVICE_BTNID g_ButtonAssignTbl[]={
	GDBTN_BUTTON00,
	GDBTN_BUTTON01,
	GDBTN_BUTTON02,
	GDBTN_BUTTON03,
	GDBTN_BUTTON04,
	GDBTN_BUTTON05,
	GDBTN_BUTTON06,
	GDBTN_BUTTON07,
	GDBTN_BUTTON08,
	GDBTN_BUTTON09,
	GDBTN_BUTTON10,
	GDBTN_BUTTON11,
	GDBTN_BUTTON12,
	GDBTN_BUTTON13,
	GDBTN_BUTTON14,
	GDBTN_BUTTON15,
	GDBTN_BUTTON16,
	GDBTN_BUTTON17,
	GDBTN_BUTTON18,
	GDBTN_BUTTON19,
	GDBTN_BUTTON20,
	GDBTN_BUTTON21,
	GDBTN_BUTTON22,
	GDBTN_BUTTON23,
	GDBTN_BUTTON24,
	GDBTN_BUTTON25,
	GDBTN_BUTTON26,
	GDBTN_BUTTON27,
	GDBTN_BUTTON28,
	GDBTN_BUTTON29,
	GDBTN_BUTTON30,
	GDBTN_BUTTON31,

	GDBTN_AXIS_XA,
	GDBTN_AXIS_XS,

	GDBTN_AXIS_YA,
	GDBTN_AXIS_YS,

	GDBTN_AXIS_ZA,
	GDBTN_AXIS_ZS,

	GDBTN_ROT_XA,
	GDBTN_ROT_XS,

	GDBTN_ROT_YA,
	GDBTN_ROT_YS,

	GDBTN_ROT_ZA,
	GDBTN_ROT_ZS,

	GDBTN_POV0_U,
	GDBTN_POV0_R,
	GDBTN_POV0_D,
	GDBTN_POV0_L,

	GDBTN_POV1_U,
	GDBTN_POV1_R,
	GDBTN_POV1_D,
	GDBTN_POV1_L,

	GDBTN_POV2_U,
	GDBTN_POV2_R,
	GDBTN_POV2_D,
	GDBTN_POV2_L,

	GDBTN_POV3_U,
	GDBTN_POV3_R,
	GDBTN_POV3_D,
	GDBTN_POV3_L,

	GDBTN_SLIDER0_U,
	GDBTN_SLIDER0_D,

	GDBTN_SLIDER1_U,
	GDBTN_SLIDER1_D,
};

struct gxPadName
{
	Uint32 button1;
	gxChar Name[64];
};

//ボタンコンフィグ用
//GAMINGDEVICE_BTNIDの並びと合わせる必要があるので注意
const gxPadName GamingDeviceButtonNameTbl[]={
	{JOY_U 		, "UP"},
	{JOY_R 		, "RIGHT"},
	{JOY_D 		, "DOWN"},
	{JOY_L 		, "LEFT"},
	{BTN_A 		, "ボタン A（決定）"},
	{BTN_B 		, "ボタン B（キャンセル）"},
	{BTN_X 		, "ボタン X"},
	{BTN_Y 		, "ボタン Y"},
	{BTN_L1		, "ボタン L1"},
	{BTN_R1		, "ボタン R1"},
	{BTN_L2		, "トリガー L2"},
	{BTN_R2		, "トリガー R2"},
	{BTN_SELECT , "SELECT"},
	{BTN_START  , "START"},
	{BTN_L3	 , "ボタン L3"},
	{BTN_R3	 , "ボタン R3"},
	{BTN_C 		, "ボタン C"},
	{BTN_Z 		, "ボタン Z"},
};


const gxChar* DeviceName[]={
	"不明",//GDBTN_BUTTON00,
	"ボタン01",//GDBTN_BUTTON00,
	"ボタン02",//GDBTN_BUTTON01,
	"ボタン03",//GDBTN_BUTTON02,
	"ボタン04",//GDBTN_BUTTON03,
	"ボタン05",//GDBTN_BUTTON04,
	"ボタン06",//GDBTN_BUTTON05,
	"ボタン07",//GDBTN_BUTTON06,
	"ボタン08",//GDBTN_BUTTON07,
	"ボタン09",//GDBTN_BUTTON08,
	"ボタン10",//GDBTN_BUTTON09,
	"ボタン11",//GDBTN_BUTTON10,
	"ボタン12",//GDBTN_BUTTON11,
	"ボタン13",//GDBTN_BUTTON12,
	"ボタン14",//GDBTN_BUTTON13,
	"ボタン15",//GDBTN_BUTTON14,
	"ボタン16",//GDBTN_BUTTON15,
	"ボタン17",//GDBTN_BUTTON16,
	"ボタン18",//GDBTN_BUTTON17,
	"ボタン19",//GDBTN_BUTTON18,
	"ボタン20",//GDBTN_BUTTON19,
	"ボタン21",//GDBTN_BUTTON20,
	"ボタン22",//GDBTN_BUTTON21,
	"ボタン23",//GDBTN_BUTTON22,
	"ボタン24",//GDBTN_BUTTON23,
	"ボタン25",//GDBTN_BUTTON24,
	"ボタン26",//GDBTN_BUTTON25,
	"ボタン27",//GDBTN_BUTTON26,
	"ボタン28",//GDBTN_BUTTON27,
	"ボタン29",//GDBTN_BUTTON28,
	"ボタン30",//GDBTN_BUTTON29,
	"ボタン31",//GDBTN_BUTTON30,
	"ボタン32",//GDBTN_BUTTON31,

	"アナログ X-AXIS+",//GDBTN_AXIS_XA,
	"アナログ X-AXIS-",//GDBTN_AXIS_XS,

	"アナログ Y-AXIS+",//GDBTN_AXIS_YA,
	"アナログ Y-AXIS-",//GDBTN_AXIS_YS,

	"アナログ Z-AXIS+",//GDBTN_AXIS_ZA,
	"アナログ Z-AXIS-",//GDBTN_AXIS_ZS,

	"アナログ 回転 X+",//GDBTN_ROT_XA,
	"アナログ 回転 X-",//GDBTN_ROT_XS,

	"アナログ 回転 Y+",//GDBTN_ROT_YA,
	"アナログ 回転 Y-",//GDBTN_ROT_YS,

	"アナログ 回転 Z+",//GDBTN_ROT_ZA,
	"アナログ 回転 Z-",//GDBTN_ROT_ZS,

	"ハットスイッチ1-U",//GDBTN_POV0_U,
	"ハットスイッチ1-R",//GDBTN_POV0_R,
	"ハットスイッチ1-D",//GDBTN_POV0_D,
	"ハットスイッチ1-L",//GDBTN_POV0_L,

	"ハットスイッチ2-U",//GDBTN_POV1_U,
	"ハットスイッチ2-R",//GDBTN_POV1_R,
	"ハットスイッチ2-D",//GDBTN_POV1_D,
	"ハットスイッチ2-L",//GDBTN_POV1_L,

	"ハットスイッチ3-U",//GDBTN_POV2_U,
	"ハットスイッチ3-R",//GDBTN_POV2_R,
	"ハットスイッチ3-D",//GDBTN_POV2_D,
	"ハットスイッチ3-L",//GDBTN_POV2_L,

	"ハットスイッチ4-U",//GDBTN_POV3_U,
	"ハットスイッチ4-R",//GDBTN_POV3_R,
	"ハットスイッチ4-D",//GDBTN_POV3_D,
	"ハットスイッチ4-L",//GDBTN_POV3_L,

	"スライダー1 UP",//GDBTN_SLIDER0_U,
	"スライダー1 DOWN",//GDBTN_SLIDER0_D,

	"スライダー2 UP",//GDBTN_SLIDER1_U,
	"スライダー2 DOWN",//GDBTN_SLIDER1_D,
};

const gxChar *pGxPadTbl[]={
	//padコンフィグ用リスト

	"up",
	"down",
	"left",
	"right",

	"a",
	"b",
	"x",
	"y",

	"L1",
	"R1",
	"L2",
	"R2",

	"Select",
	"Start",

	"L3",
	"R3",

	"c",
	"z",

	"Back",

	"Analog-1U",
	"Analog-1R",
	"Analog-1D",
	"Analog-1L",

	"Analog-2U",
	"Analog-2R",
	"Analog-2D",
	"Analog-2L",
};

extern Sint32 GxPadBitIndexTbl[];

const gxChar *AnalogName[]={
	"Analog Axis-X",
	"Analog Axis-Y",
	"Analog Axis-Z",

	"Analog Rotation-X",
	"Analog Rotation-Y",
	"Analog Rotation-Z",

	"-------------------", //MAX
	"-------------------", //NoUse
};

const gxChar *StickName[]={
	"Analog Left Stick-X",
	"Analog Left Stick-Y",
	"Analog Right Stick-X",
	"Analog Right Stick-Y",
	"Left Trigger(Brake)",
	"Right Trigger(Accel)",
};

CGameingDevice::CGameingDevice()
{
	_num_pov = 0;
	_num_slider = 0;

	memset( &_Axis , 0x00 , sizeof(PadStat_t) );
	memset( &_Rotation , 0x00 , sizeof(PadStat_t) );
	memset( &_caribrate , 0x00 , sizeof(PadStat_t) );
	sprintf(_hardwarename, "Unconnected");

	_DeviceType = 0;

	_Pov0 = 0;
	_Pov1 = 0;
	_Pov2 = 0;
	_Pov3 = 0;
	_Slider0 = 0;
	_Slider1 = 0;

	//サポート状況を初期化
	_bPov[0]	= gxFalse;			//ハットスイッチ0
	_bPov[1]	= gxFalse;			//ハットスイッチ1
	_bPov[2]	= gxFalse;			//ハットスイッチ2
	_bPov[3]	= gxFalse;			//ハットスイッチ3
	_num_slider = 0;				//スライダーコントロール数
	_bAxisX =
	_bAxisY =
	_bAxisZ = gxFalse;				//サポートされているAXIS
	_bAxisRX =
	_bAxisRY =
	_bAxisRZ = gxFalse;				//サポートされているAXIS
	_bRumble = gxFalse;				//振動可能か？


	//使用するかどうか(基本的には全部使用することにする)
	_bUseAxisX
	= _bUseAxisY
	= _bUseAxisZ
	= _bUseRotationX
	= _bUseRotationY
	= _bUseRotationZ
	= _bUsePOV0
	= _bUsePOV1
	= _bUsePOV2
	= _bUsePOV3
	= _bUseSlider0
	= _bUseSlider1
	= _bUseForceFeedBack = true;

	for(int i=0;i<128;i++)
	{
		_button[i] = gxFalse;
	}

	_bSlider[0] = _bSlider[1] = gxFalse;
	_ForceFeefBackAxis = 0;

	_bCarribrateCompleted = gxFalse;
	_bDeviceDisable = gxFalse;
	_bEnableConfig  = gxFalse;
	_PlayerID = 0;

	_bXInput = gxFalse;
}


CGameingDevice::~CGameingDevice()
{

}

gxBool CGameingDevice::IsPress(GAMINGDEVICE_BTNID id)
{
	//------------------------------------------------------
	//ボタンが押されているかどうかデジタル入力に変換して返す
	//------------------------------------------------------

	switch(id) {
	//BUTTON
	case GDBTN_BUTTON00:
	case GDBTN_BUTTON01:
	case GDBTN_BUTTON02:
	case GDBTN_BUTTON03:
	case GDBTN_BUTTON04:
	case GDBTN_BUTTON05:
	case GDBTN_BUTTON06:
	case GDBTN_BUTTON07:
	case GDBTN_BUTTON08:
	case GDBTN_BUTTON09:
	case GDBTN_BUTTON10:
	case GDBTN_BUTTON11:
	case GDBTN_BUTTON12:
	case GDBTN_BUTTON13:
	case GDBTN_BUTTON14:
	case GDBTN_BUTTON15:
	case GDBTN_BUTTON16:
	case GDBTN_BUTTON17:
	case GDBTN_BUTTON18:
	case GDBTN_BUTTON19:
	case GDBTN_BUTTON20:
	case GDBTN_BUTTON21:
	case GDBTN_BUTTON22:
	case GDBTN_BUTTON23:
	case GDBTN_BUTTON24:
	case GDBTN_BUTTON25:
	case GDBTN_BUTTON26:
	case GDBTN_BUTTON27:
	case GDBTN_BUTTON28:
	case GDBTN_BUTTON29:
	case GDBTN_BUTTON30:
	case GDBTN_BUTTON31:
		return IsPressButton(_button[id]);// -GDBTN_BUTTON00]);

	//AXIS
	case GDBTN_AXIS_XA:
	case GDBTN_AXIS_XS:
		if(!_bUseAxisX) return gxFalse;
		return IsPressAxis(_Axis.x,id-GDBTN_AXIS_XA);
	case GDBTN_AXIS_YA:
	case GDBTN_AXIS_YS:
		if(!_bUseAxisY) return gxFalse;
		return IsPressAxis(_Axis.y,id-GDBTN_AXIS_YA);
	case GDBTN_AXIS_ZA:
	case GDBTN_AXIS_ZS:
		if(!_bUseAxisZ) return gxFalse;
		return IsPressAxis(_Axis.z,id-GDBTN_AXIS_ZA);

	//ROTATION
	case GDBTN_ROT_XA:
	case GDBTN_ROT_XS:
		if(!_bUseRotationX) return gxFalse;
		return IsPressAxis(_Rotation.x,id-GDBTN_ROT_XA);
	case GDBTN_ROT_YA:
	case GDBTN_ROT_YS:
		if(!_bUseRotationY) return gxFalse;
		return IsPressAxis(_Rotation.y,id-GDBTN_ROT_YA);
	case GDBTN_ROT_ZA:
	case GDBTN_ROT_ZS:
		if(!_bUseRotationZ) return gxFalse;
		return IsPressAxis(_Rotation.z,id-GDBTN_ROT_ZA);

	//POV
	case GDBTN_POV0_U:
	case GDBTN_POV0_R:
	case GDBTN_POV0_D:
	case GDBTN_POV0_L:
		if(!_bUsePOV0) return gxFalse;
		return IsPressPov(_Pov0,id-GDBTN_POV0_U);

	case GDBTN_POV1_U:
	case GDBTN_POV1_R:
	case GDBTN_POV1_D:
	case GDBTN_POV1_L:
		if(!_bUsePOV1) return gxFalse;
		return IsPressPov(_Pov1,id-GDBTN_POV1_U);

	case GDBTN_POV2_U:
	case GDBTN_POV2_R:
	case GDBTN_POV2_D:
	case GDBTN_POV2_L:
		if(!_bUsePOV2) return gxFalse;
		return IsPressPov(_Pov2,id-GDBTN_POV2_U);

	case GDBTN_POV3_U:
	case GDBTN_POV3_R:
	case GDBTN_POV3_D:
	case GDBTN_POV3_L:
		if(!_bUsePOV3) return gxFalse;
		return IsPressPov(_Pov3,id-GDBTN_POV3_U);

	//SLIDER
	case GDBTN_SLIDER0_U:
	case GDBTN_SLIDER0_D:
		if(!_bUseSlider0) return gxFalse;
		return IsPressSlider(_Slider0);// , id - GDBTN_SLIDER0_U);

	case GDBTN_SLIDER1_U:
	case GDBTN_SLIDER1_D:
		if(!_bUseSlider1) return gxFalse;
		return IsPressSlider(_Slider1);//,id-GDBTN_SLIDER1_U);
	}

	return gxFalse;
}

gxBool CGameingDevice::IsPressButton( gxBool& btn)
{
	//ボタンが押されているかどうかチェック

	return (btn)? gxTrue : gxFalse;
}

gxBool CGameingDevice::IsPressAxis( Float32& axis , Sint32 dir)
{
	//アナログスティックが倒されているかどうかチェック
	int d = 0;

	if(axis < -AXIS_RANGE/2) d = -1;
	if(axis >  AXIS_RANGE/2) d =  1;

	if(dir == 0 && 	d == 1)  return gxTrue;
	if(dir == 1 && 	d == -1) return gxTrue;

	return gxFalse;
}

gxBool CGameingDevice::IsPressPov( Sint32 &pov , Sint32 urdl)
{
	//---------------------------------------------------------------------
	//POVが入力されているかどうか
	//---------------------------------------------------------------------

	//---------------------------------------------------------------------
	//押されているボタンとリクエスト方向が一致すれば押されていることにする
	//---------------------------------------------------------------------
	switch(urdl){
	case 0:	//上
		if(pov & JOY_U) return gxTrue;
		break;
	case 1:	//右
		if(pov & JOY_R) return gxTrue;
		break;
	case 2:	//下
		if(pov & JOY_D) return gxTrue;
		break;
	case 3:	//左
		if(pov & JOY_L) return gxTrue;
		break;
	}

	return gxFalse;
}

gxBool CGameingDevice::IsPressSlider( Float32 &sldr )
{
	//０～２５５の値が返ってくる

	if( sldr > 200 ) return gxTrue;

	return gxFalse;
}



CGamePad::CGamePad()
{
	//キーボード
	for( Sint32 ii=0; ii<256; ii++ )
	{
		m_KeyBoardPush[ii] = 0x00;
	}

	//マウス
	for( Sint32 ii=0; ii<8; ii++ )
	{
		m_MouseClick[ii] = 0x00;
	}
	m_MouseX = m_MouseY = 0;
	m_FloatMouseX = m_FloatMouseY = 0;
	m_Wheel = 0;

	m_DeviceCnt = 0;

}


CGamePad::~CGamePad()
{
	StPadAssign assignTbl[enDeviceMax];
	gxChar buf[256] = {};

	for(Sint32 ii=0;ii<enDeviceMax; ii++ )
	{
		if( ii>=m_DeviceCnt ) continue;

		CGameingDevice *pDevice = &m_Device[ii];

		//デバイス情報の記録

		assignTbl[ii].playerID = (Uint8)pDevice->_PlayerID;
		assignTbl[ii].bDisable = pDevice->_bDeviceDisable? 1 : 0;

		for(Sint32 jj=0; jj<BTN_MAX ;jj++)
		{
			if(pDevice->_Assign[jj] ) assignTbl[ii].enableConfig = 1;
		}

		//Device名を記録することにした
		memcpy(buf, pDevice->_hardwarename, 128);
		buf[128] = 0x00;
		sprintf(assignTbl[ii].name, "%s", buf);
		assignTbl[ii].name[255] = 0x00;

		//デバイスごとのコンフィグテーブルを記録する

		for( Sint32 jj=0; jj<BTN_MAX; jj++ )
		{
			assignTbl[ii].button[jj] = pDevice->_Assign[jj];
		}

		for( Sint32 jj=0; jj<gxPadManager::ANALOG_MAX; jj++ )
		{
			assignTbl[ii].analog[ jj ] = pDevice->_cnvAnalog[ jj ];
		}

		for( Sint32 jj=0; jj<ARRAY_LENGTH( KeyConfigTbl ); jj++ )
		{
			assignTbl[ii].keyboard[jj] = KeyConfigTbl[jj];
		}
	}

	CDeviceManager::GetInstance()->SaveFile( _PAD_ASSIGN_CONFIG_FILE_ , (Uint8*)assignTbl, sizeof(assignTbl) , ESTORAGE_LOCATION::INTERNAL);

	CDirectInput::DeleteInstance();
	CXInput::DeleteInstance();

}

void CGamePad::Init()
{
	CDirectInput::GetInstance()->Init();
	CXInput::GetInstance()->Init();

	size_t uSize = 0;

	//コントローラーのコンフィグ辞書からコンフィグデータを復元する

	StPadAssign *pAssignTbl;

	pAssignTbl = (StPadAssign*)CDeviceManager::GetInstance()->LoadFile( _PAD_ASSIGN_CONFIG_FILE_, &uSize , ESTORAGE_LOCATION::INTERNAL);

	if(pAssignTbl == NULL || (pAssignTbl && pAssignTbl->version != VERSION_NUMBER) )
	{
		//異なるバージョンなので無視する
		return;
	}

	size_t num = uSize/sizeof(StPadAssign);

	//各コントローラーをプレイヤーに割り振る

	gxBool bAssigned[32] = {};

	if( pAssignTbl )
	{
		for (Sint32 ii = 0; ii < m_DeviceCnt; ii++)
		{
			CGameingDevice* pDevice = &m_Device[ii];
			Sint32 kouho = -1;

			for (Sint32 jj = 0; jj < num; jj++)
			{

				if (strcmp(pAssignTbl[jj].name, pDevice->_hardwarename) == 0)
				{
					//記録された名前があればそれを適用する
					kouho = jj;
					if (bAssigned[jj])
					{
						//すでにアサインされていたので次を探す
						kouho = jj;
						continue;
					}
					else
					{
						//あったので、それを使う
						kouho = jj;
						bAssigned[jj] = true;
						break;
					}
				}
			}

			if (kouho != -1)
			{
				StPadAssign *q = &pAssignTbl[kouho];

				pDevice->_PlayerID	   = q->playerID;
				pDevice->_bDeviceDisable = q->bDisable ? gxTrue : gxFalse;
				pDevice->_bEnableConfig  = q->enableConfig ? gxTrue : gxFalse;;

				for( Sint32 jj=0; jj<BTN_MAX; jj++ )
				{
					pDevice->_Assign[jj] = q->button[jj];
				}

				for( Sint32 jj=0; jj<gxPadManager::ANALOG_MAX; jj++ )
				{
					pDevice->_cnvAnalog[ jj ] = q->analog[ jj ];
				}
			}
		}

		//キーボードコンフィグ

		for( Sint32 jj=0; jj<ARRAY_LENGTH( KeyConfigTbl ); jj++ )
		{
			if((gxKey::KeyType)pAssignTbl->keyboard[jj] != gxKey::KEYNONE )
			{
				KeyConfigTbl[jj] = (gxKey::KeyType)pAssignTbl->keyboard[jj];
			}
		}


	}

	updateGamepadAssignTolPlayer();

	SAFE_DELETES( pAssignTbl );

}

void CGamePad::Action()
{

	//for( int ii=0; ii<128;ii++)
	//{
	//	if( !CXInput::GetInstance()->Action(ii) )
	//	{
	//		if( !CDirectInput::GetInstance()->Action(ii) )
	//		{
	//			break;
	//		}
	//	}
	//
	//}

	for( int ii=0; ii<16;ii++)
	{
		CXInput::GetInstance()->Action(ii);
		CDirectInput::GetInstance()->Action(ii);
	}


	//設定したコントローラー情報をgxLibに通知する

	//キーボード

	if( (m_KeyBoardPush[ gxKey::RSHIFT ] == 0x01) && (GetKeyState( VK_RSHIFT )&0x8000) == 0 )   m_KeyBoardPush[ gxKey::RSHIFT ] = 0x02;

	if( (m_KeyBoardPush[ gxKey::RCTRL ] == 0x01) && (GetKeyState( VK_RCONTROL )&0x8000) == 0 ) m_KeyBoardPush[ gxKey::RCTRL ]  = 0x02;

	if( (m_KeyBoardPush[ gxKey::RALT ] == 0x01) && (GetKeyState( VK_RMENU )&0x8000) == 0 )	m_KeyBoardPush[ gxKey::RALT ]   = 0x02;


	for( Sint32 ii=0; ii<256; ii++ )
	{
		if( m_KeyBoardPush[ii] == 0x01 )
		{
			gxPadManager::GetInstance()->SetKeyDown(ii);
		}
		if( m_KeyBoardPush[ii] == 0x02 )
		{
			gxPadManager::GetInstance()->SetKeyUp(ii);
		}

		//m_KeyBoardPush[ii] = 0x00;
	}

	//マウス

	Float32 mx = m_MouseX;
	Float32 my = m_MouseY;
	Sint32 gamew,gameh;
	Sint32 scrw,scrh;
	Float32 ofx,ofy;

	CGameGirl::GetInstance()->GetGameResolution( &gamew , &gameh );
	CGameGirl::GetInstance()->GetWindowsResolution( &scrw , &scrh );
	ofx = (scrw - gamew)/2.0f;
	ofy = (scrh - gameh)/2.0f;

	mx -= ofx;
	my -= ofy;

	mx = WINDOW_W * mx / gamew;
	my = WINDOW_H * my / gameh;

	//	gxPadManager::GetInstance()->SetMousePosition( mx , my );
	gxPadManager::GetInstance()->SetMouseWheel( -m_Wheel );

	//	タッチポジションをシミュレートする
	//	gxPadManager::GetInstance()->SetTouchInfo( ii , (m_MouseClick[ii]==0x01)? gxTrue : gxFalse , mx , my );

	gxPadManager::GetInstance()->SetMousePosition(0, mx, my);

	for( Sint32 ii=0; ii<3; ii++ )
	{
		if( m_MouseClick[ii] == 0x01 )
		{
			//押された
			gxPadManager::GetInstance()->SetMouseButtonDown(ii);
			gxPadManager::GetInstance()->SetTouchInfo(ii, gxTrue, mx, my);
		}
		if( m_MouseClick[ii] == 0x02 )
		{
			//離された
			gxPadManager::GetInstance()->SetMouseButtonUp(ii);
			gxPadManager::GetInstance()->SetTouchInfo(ii, gxFalse, mx, my);
			m_MouseClick[ii] = 0x00;
		}
	}

	//コントローラー入力を行わない



	configInput();
	m_Wheel = 0.0f;

}


Sint32 CGamePad::ConvertKeyNumber( WPARAM id )
{
	//キーと配列をマッチングさせる

	switch( id ){
	case VK_ESCAPE:		return gxKey::ESC	  ;
	case VK_BACK:	   return gxKey::BS   ;
	case VK_TAB:		return gxKey::TAB		 ;
	case VK_RETURN:	 return gxKey::RETURN	  ;
//	case VK_RETURN:	 return gxKey::KEYBOARD_ENTER	   ;
	case VK_SHIFT:
	case VK_LSHIFT:	// return gxKey::SHIFT;
	case VK_RSHIFT:	// return gxKey::RSHIFT;
	{
		short input = GetKeyState(VK_RSHIFT);
		if (input & 0x8000)
		{
			return gxKey::RSHIFT;
		}
		return gxKey::SHIFT;
	}

	case VK_CONTROL:	// return gxKey::CTRL;
	case VK_LCONTROL:	// return gxKey::CTRL		;
	case VK_RCONTROL:
		if (GetKeyState(VK_RCONTROL) & 0x8000)
		{
			return gxKey::RCTRL;
		}
		return gxKey::CTRL;

	case VK_LMENU:
	case VK_RMENU://	  return gxKey::RALT;
		if (GetKeyState(VK_RMENU) & 0x8000)
		{
			return gxKey::RALT;
		}
		return gxKey::ALT;
	case VK_UP:		 return gxKey::UP	;
	case VK_DOWN:	   return gxKey::DOWN  ;
	case VK_LEFT:	   return gxKey::LEFT  ;
	case VK_RIGHT:	  return gxKey::RIGHT ;
	case VK_SPACE:	  return gxKey::SPACE	   ;
	case VK_F1:		 return gxKey::F1			  ;
	case VK_F2:		 return gxKey::F2		  ;
	case VK_F3:		 return gxKey::F3		  ;
	case VK_F4:		 return gxKey::F4		  ;
	case VK_F5:		 return gxKey::F5		  ;
	case VK_F6:		 return gxKey::F6		  ;
	case VK_F7:		 return gxKey::F7		  ;
	case VK_F8:		 return gxKey::F8		  ;
	case VK_F9:		 return gxKey::F9		  ;
	case VK_F10:		return gxKey::F10		 ;
	case VK_F11:		return gxKey::F11		 ;
	case VK_F12:		return gxKey::F12		 ;
	case VK_NUMPAD0:	return gxKey::NUMPAD0	;
	case VK_NUMPAD1:	return gxKey::NUMPAD1	;
	case VK_NUMPAD2:	return gxKey::NUMPAD2	;
	case VK_NUMPAD3:	return gxKey::NUMPAD3	;
	case VK_NUMPAD4:	return gxKey::NUMPAD4	;
	case VK_NUMPAD5:	return gxKey::NUMPAD5	;
	case VK_NUMPAD6:	return gxKey::NUMPAD6	;
	case VK_NUMPAD7:	return gxKey::NUMPAD7	;
	case VK_NUMPAD8:	return gxKey::NUMPAD8	;
	case VK_NUMPAD9:	return gxKey::NUMPAD9	;
	case VK_PRIOR: 		return gxKey::PAGEUP;
	case VK_NEXT:		return gxKey::PAGEDOWN	;
	case VK_DELETE:		return gxKey::DEL;
	case VK_INSERT:		return gxKey::INS;
	case VK_END:		return gxKey::END;
	case VK_HOME:		return gxKey::HOME;
	case VK_SNAPSHOT:	return gxKey::SNAPSHOT;
	case VK_MULTIPLY:	return gxKey::NUMPAD_MULTIPLY;

	case VK_ADD:		return gxKey::NUMPAD_PLUS;
	case VK_SEPARATOR:	return gxKey::RETURN;
	case VK_SUBTRACT:	return gxKey::NUMPAD_MINUS;
	case VK_DECIMAL:	return gxKey::NUMPAD_PERIOD;
	case VK_DIVIDE:		return gxKey::NUMPAD_DIV;

	case VK_PAUSE:		return gxKey::PAUSE;
	case VK_NUMLOCK:	return gxKey::NUMLOCK;
	case VK_SCROLL:		return gxKey::SCROLL;

	default:
		switch (id) {
		case 189:   return gxKey::MINUS;	//-	189
		case 222:   return gxKey::HAT;		//^	222
		case 220:	return gxKey::YEN;		//	220	
		case 192:   return gxKey::ATMARK;	//@	192
		case 219:   return gxKey::LARGE_L;	//[	219
		case 187:   return gxKey::SEMIC;	//;	187
		case 186:   return gxKey::COLON;	//:	186
		case 221:   return gxKey::LARGE_R;	//]	221
		case 188:   return gxKey::COMMA;	//,	188
		case 190:   return gxKey::PERIOD;	//.	190
		case 191:   return gxKey::SLASH;	///	191
		case 226:   return gxKey::YEN;		//\	226
		case 91:	return gxKey::WIN;		//WIN	91
		default:
			if (id >= '0' && id <= '9')
			{
				return gxKey::NUM0 + id - '0';
			}
			if (id >= 'A' && id <= 'Z')
			{
				return gxKey::A + id - 'A';
			}
			return id;
		}
	}

	return 0x00;
}


void CGamePad::InputKeyCheck( UINT iMsg , WPARAM wParam ,LPARAM lParam )
{
	//キー入力されたら記録する

	switch(iMsg) {
	case WM_KEYDOWN:
		//キーボード押した
		//gxPadManager::GetInstance()->SetKeyDown(wParam);
		m_KeyBoardPush[ ConvertKeyNumber( wParam ) ] = 0x01;
		return;

	case WM_KEYUP:
		//キーボード離した
		m_KeyBoardPush[ ConvertKeyNumber( wParam ) ] = 0x02;
		return;

	case WM_LBUTTONDOWN:
		//マウスボタン押した
		m_MouseClick[0] = 0x01;
		break;
	case WM_LBUTTONUP:
		//マウスボタン離した
		m_MouseClick[0] = 0x02;
		break;
	case WM_RBUTTONDOWN:
		//マウスボタン押した
		m_MouseClick[1] = 0x01;
		break;
	case WM_RBUTTONUP:
		//マウスボタン離した
		m_MouseClick[1] = 0x02;
		break;
	case WM_MBUTTONDOWN:
		//マウスボタン押した
		m_MouseClick[2] = 0x01;
		break;
	case WM_MBUTTONUP:
		//マウスボタン離した
		m_MouseClick[2] = 0x02;
		break;

	case WM_MOUSEMOVE:
		{
			//マウスが移動した
			POINT point={0,0};
			POINT point2={0,0};

			point.x = GET_X_LPARAM( lParam );
			point.y = GET_Y_LPARAM( lParam );
			point2 = point;

			ScreenToClient(CWindows::GetInstance()->m_hWindow, &point2);

			CGamePad::GetInstance()->m_FloatMouseX = point2.x;
			CGamePad::GetInstance()->m_FloatMouseY = point2.y;

			//if(  )
			{
				Sint32 w, h;
				CGameGirl::GetInstance()->GetWindowsResolution(&w, &h);

				if( w == 0 || h == 0 ) break;

				if( GetActiveWindow() == CWindows::GetInstance()->m_hWindow )
				{
					m_MouseX = point.x;// WINDOW_W*point.x / w;
					m_MouseY = point.y;// WINDOW_H*point.y / h;
				}
			}
		}
		break;

	case WM_MOUSEWHEEL:
		{
			Sint32 rot = (Sint16)HIWORD(wParam);
			m_Wheel = 1.0f * rot / WHEEL_DELTA;
			m_Wheel = CLAMP(m_Wheel, -1.0f, 1.0f);
		}
		break;

	case WM_TOUCH:
		//タッチ対応
		//InputTouchCheck( iMsg , wParam , lParam );
		break;
	default:
		break;
	}


}


void CGamePad::configInput()
{
	Sint32  max = PLAYER_MAX;
	Uint32  padButtonPush[PLAYER_MAX] = {};
	//Float32 padAnalog[PLAYER_MAX][gxPadManager::ANALOG_MAX]={};
	gxPadManager::StAnalog padAnalog[PLAYER_MAX] = {0};

	size_t num = sizeof(GamingDeviceButtonNameTbl) / sizeof(GamingDeviceButtonNameTbl[0]);

	for( Sint32 ii=0; ii<m_DeviceCnt; ii++ )
	{
		CGameingDevice *pDevice = &m_Device[ii];

		Sint32 pl = pDevice->_PlayerID;

		//未使用設定のデバイスは無視する
		if( pl >= PLAYER_MAX || pDevice->_bDeviceDisable ) continue;

		if( pDevice->_bEnableConfig )
		{
			//変換テーブルを参照して押されているボタンを確認

			for( Sint32 jj=0; jj<num; jj++ )
			{
				Uint8 cnvbtn = pDevice->_Assign[jj];

				if( pDevice->IsPress( (GAMINGDEVICE_BTNID)cnvbtn ) )
				{
					padButtonPush[pl] |= GamingDeviceButtonNameTbl[jj].button1;
				}

			}
		}
		else
		{
			//デフォルト設定を使う
			Sint32 dir4 = 4;

			for( Sint32 jj=0; jj< dir4; jj++ )
			{
				static const Uint32 tbl[]={
					JOY_U,JOY_R,JOY_D,JOY_L
				};

				if( pDevice->_Pov0 & tbl[jj] )
				{
					padButtonPush[pl] |= (0x01 << jj);
				}
			}

			//各ボタン情報を割当て

			for( Sint32 jj=GDBTN_BUTTON00; jj<GDBTN_BUTTON31; jj++ )
			{
				if( pDevice->IsPress((GAMINGDEVICE_BTNID)jj) )
				{
					//padButtonPush[pl] |= GamingDeviceButtonNameTbl[jj].button1;
					//padButtonPush[pl] |= (BTN_A << (jj- GDBTN_BUTTON00) );	//Cボタンのつじつまが合わない
					padButtonPush[pl] |= GamingDeviceButtonNameTbl[4+jj- GDBTN_BUTTON00].button1;
				}
			}
		}

		//--------------------------------------------------
		//analog情報(かち合ったら加算する)
		//--------------------------------------------------

		Float32 analog[ gxPadManager::ANALOG_MAX ] ={};

/*
		Uint32 cnvAnalog[] ={
			gxPadManager::ANALOG_LY,
			gxPadManager::ANALOG_LX,
			gxPadManager::ANALOG_RY,
			gxPadManager::ANALOG_RX,
			gxPadManager::ANALOG_LT,
			gxPadManager::ANALOG_RT,
		};
*/
		analog[ gxPadManager::ANALOG_LX ] = pDevice->_Axis.x;
		analog[ gxPadManager::ANALOG_LY ] = pDevice->_Axis.y;
		analog[ gxPadManager::ANALOG_RX ] = pDevice->_Rotation.x; 
		analog[ gxPadManager::ANALOG_RY ] = pDevice->_Rotation.y;
		analog[ gxPadManager::ANALOG_LT ] = pDevice->_Slider0;////;pDevice->_Axis.z; 
		analog[ gxPadManager::ANALOG_RT ] = pDevice->_Slider1;////;pDevice->_Rotation.z;

		Uint32 cnv[6] ={
			pDevice->_cnvAnalog[ gxPadManager::ANALOG_LX ],
			pDevice->_cnvAnalog[ gxPadManager::ANALOG_LY ],
			pDevice->_cnvAnalog[ gxPadManager::ANALOG_RX ],
			pDevice->_cnvAnalog[ gxPadManager::ANALOG_RY ],
			pDevice->_cnvAnalog[ gxPadManager::ANALOG_LT] ,
			pDevice->_cnvAnalog[ gxPadManager::ANALOG_RT] ,
		};

		if( cnv[ gxPadManager::ANALOG_LX ] != gxPadManager::NOUSE_ANALOG_AXIS ) padAnalog[pl].left.x += analog[ cnv[ gxPadManager::ANALOG_LX ] ];
		if( cnv[ gxPadManager::ANALOG_LY ] != gxPadManager::NOUSE_ANALOG_AXIS ) padAnalog[pl].left.y += analog[ cnv[ gxPadManager::ANALOG_LY ] ];
		if( cnv[ gxPadManager::ANALOG_RX ] != gxPadManager::NOUSE_ANALOG_AXIS ) padAnalog[pl].right.x += analog[ cnv[ gxPadManager::ANALOG_RX ] ];
		if( cnv[ gxPadManager::ANALOG_RY ] != gxPadManager::NOUSE_ANALOG_AXIS ) padAnalog[pl].right.y += analog[ cnv[ gxPadManager::ANALOG_RY ] ];
		if( cnv[ gxPadManager::ANALOG_LT ] != gxPadManager::NOUSE_ANALOG_AXIS ) padAnalog[pl].leftTrigger += analog[ cnv[ gxPadManager::ANALOG_LT ]  ];
		if( cnv[ gxPadManager::ANALOG_RT ] != gxPadManager::NOUSE_ANALOG_AXIS ) padAnalog[pl].rightTrigger += analog[ cnv[ gxPadManager::ANALOG_RT ]  ];
	}

	for(Sint32 pl=0; pl< PLAYER_MAX;pl++)
	{
		padAnalog[pl].leftTrigger  = ABS(padAnalog[pl].leftTrigger);
		padAnalog[pl].rightTrigger = ABS(padAnalog[pl].rightTrigger);

		padAnalog[pl].left.x	   = CLAMP( padAnalog[pl].left.x, -1.0f , 1.0f );
		padAnalog[pl].left.y	   = CLAMP( padAnalog[pl].left.y , -1.0f , 1.0f );
		padAnalog[pl].right.x	  = CLAMP( padAnalog[pl].right.x , -1.0f , 1.0f );
		padAnalog[pl].right.y	  = CLAMP( padAnalog[pl].right.y , -1.0f , 1.0f );
		padAnalog[pl].leftTrigger  = CLAMP( padAnalog[pl].leftTrigger , -1.0f , 1.0f );
		padAnalog[pl].rightTrigger = CLAMP( padAnalog[pl].rightTrigger , -1.0f , 1.0f );

		//ボタン情報をggaに送信する

		//--------------------------------------------
		//変換テーブルによって入力を返還する
		//--------------------------------------------

		gxPadManager::GetInstance()->SetPadInfo   ( pl , padButtonPush[pl] );
		gxPadManager::GetInstance()->SetAnalogInfo( pl , &padAnalog[pl]);
	}
}


//---------------------------------------------------------------------

void CGamePad::DeviceAssignmentConfig( gxBool bControlEnable )
{
	switch( m_Selecter ){
	case 0:
		gxPadInformation();
		break;

	case 100:
		deviceUseConfig();
		break;

	case 200:
		deviceInformation();
		break;

	case 300:
	case 350:
		buttonConfig();
		break;

	case 400:
		keyBoardConfig();
		break;
	}

}

void CGamePad::gxPadInformation()
{
	//デフォルトメニュー
	gxChar *tbl[]={
		"Controller Device Config",
		"Adjust Player Device",
		"KeyBoard Settings",
		"Rumble Test",
	};

	Sint32 menuNum = sizeof(tbl) / sizeof(tbl[0]);

	if( gxUtil::KeyBoard()->IsTrigger( gxKey::NUMPAD_UP ))
	{
		m_Cursor --;
	}
	else if( gxUtil::KeyBoard()->IsTrigger( gxKey::NUMPAD_DOWN ))
	{
		m_Cursor ++;
	}
	else if( gxUtil::KeyBoard()->IsTrigger( gxKey::RETURN ))
	{
		switch( m_Cursor ){
		case 0:	//パッドコンフィグ
			m_Selecter = 200;
			m_Cursor = 0;
			break;

		case 1:	//使用デバイスセレクター
			m_Selecter = 100;
			m_Cursor = 0;
			break;

		case 2:	//キーボードコンフィグ
			m_Selecter = 400;
			m_Cursor = 0;
			m_OldButtonID = -1;
			m_bInitConfig = gxFalse;
			break;

		case 3:	//振動テスト
			gxLib::SetRumble(0,120);
			gxLib::SetRumble(1, 120);
			break;
		default:
			break;
		}
	}
	else if( gxUtil::KeyBoard()->IsTrigger( gxKey::BS ))
	{
		m_Selecter = 0;
		m_Cursor = 0;
	}

	m_Cursor = (menuNum + m_Cursor)%menuNum;


	Sint32 ax, ay,az,ah=40;

	ax = 32;
	ay = 64;
	az = PRIORITY_DEBUG_MAX;

	for(Sint32 ii=0;ii<menuNum;ii++)
	{
		Uint32 argb = 0xffffffff;
		if( m_Cursor == ii )
		{
			argb = 0xff00ff00;
			gxLib::Printf( ax-32 , ay+ah*m_Cursor , az , ATR_DFLT , argb , ">" );
		}
		gxLib::Printf( ax , ay+ah*ii , az , ATR_DFLT , argb , "%s" , tbl[ii] );
	}

	//gxLibのコントローラー入力情報を表示する

/*
	for (Sint32 ii = 0; ii < PLAYER_MAX; ii++)
	{
		drawgxKeyInfo(ii);
	}
*/


}

void CGamePad::drawgxKeyInfo(Sint32 id)
{
	//gxLibで受け付けた入力内容を表示する

/*
	gxVector2 analogL;
	gxVector2 analogR;
	gxVector2 analogN;
	Uint32  key = gxLib::Joy(id)->psh;
	gxVec gyro,accel,orientation,magneField;

	analogL.x = gxLib::Joy(id)->lx;		// 左アナログＸ
	analogL.y = gxLib::Joy(id)->ly;		// 左アナログＹ
	analogR.x = gxLib::Joy(id)->rx;		// 右アナログ
	analogR.y = gxLib::Joy(id)->ry;		// 右アナログ
	analogN.x = gxLib::Joy(id)->lt;		// Lトリガー
	analogN.y = gxLib::Joy(id)->rt;		// Rトリガー

	//各種センサー値
	gyro  = gxLib::Joy(id)->gyro;		//ジャイロ
	accel = gxLib::Joy(id)->accel;		//加速度
	orientation = gxLib::Joy(id)->orientation;	//方向
	magneField  = gxLib::Joy(id)->magneField;	//地磁気

	Sint32 ax=32+id*(WINDOW_W/2),ay=128,az=PRIORITY_DEBUG_MAX;

	Sint32 sz = 27;// sizeof(GxPadBitIndexTbl) / sizeof(GxPadBitIndexTbl[0]);
	Sint32 yy = 0;

	gxLib::Printf( ax+64 , ay, az ,ATR_STR_CENTER , ARGB_DFLT , "Player %d" , id+1 );
	ay += 32;

	Sint32 wx = ax + 64;
	Sint32 wy = ay + 64;

	gxLib::DrawBox(wx - 64, wy - 64, wx + 64, wy + 64, az, gxTrue, ATR_DFLT, 0xff808080);
	gxLib::DrawPoint(wx + analogL.x * 64, wy + analogL.y * 64, az, ATR_DFLT, 0xffff0000, 32.0f);
	gxLib::DrawPoint(wx + analogR.x * 64, wy + analogR.y * 64, az, ATR_DFLT, 0xfffff00f, 24.0f);

	gxLib::DrawPoint(wx -64, wy-64 + analogN.x * 128, az, ATR_DFLT, 0xff00ffff, 16.0f);
	gxLib::DrawPoint(wx +64, wy-64 + analogN.y * 128, az, ATR_DFLT, 0xff00ffff, 16.0f);

	//ボタン

	ay += 144;

	for( Sint32 ii=0; ii<sz; ii++ )
	{
		if( key&(0x01<<GxPadBitIndexTbl[ii]) )
		{
			gxLib::Printf( ax , ay+yy*16, az ,ATR_DFLT , ARGB_DFLT , "%s" , pGxPadTbl[ii] );
			yy ++;
		}
	}
*/
}

void CGamePad::deviceInformation()
{
	//----------------------------------------------------
	//インプットデバイス情報を表示する
	//----------------------------------------------------

	if (gxUtil::KeyBoard()->IsTrigger(gxKey::NUMPAD_UP))
	{
		m_fTgtScrollY -= 1;
	}
	else if (gxUtil::KeyBoard()->IsTrigger(gxKey::NUMPAD_DOWN))
	{
		m_fTgtScrollY += 1;
	}
	else if (gxUtil::KeyBoard()->IsTrigger(gxKey::RETURN))
	{
		m_Selecter = 300;
		m_Cursor = 0;
		m_OldButtonID = -1;
	}
	else if (gxUtil::KeyBoard()->IsTrigger(gxKey::BS))
	{
		m_Selecter = 0;
		m_Cursor = 0;
	}

	m_fTgtScrollY = CLAMP( m_fTgtScrollY , 0 , m_DeviceCnt-1 );
	m_fCurrentScrollY += ( -m_fTgtScrollY*160 - m_fCurrentScrollY )/10.0f;

	//現在選択中のデバイス
	m_CurentDevice = m_fTgtScrollY;

	Sint32 ax=32,ay=340,az=PRIORITY_DEBUG_MAX;
	Sint32 max = 2;

	gxLib::Scissor( 0, 320 , WINDOW_W, WINDOW_H , MAX_PRIORITY_NUM + 1 );

	for( Sint32 ii=0; ii<m_DeviceCnt; ii++ )
	{
		CGameingDevice *pDevice = &m_Device[ii];

		ax = 64;// +ii * 160;
		ay = 360 + ii * 160 + m_fCurrentScrollY;

		drawDevice( ii , ax , ay , gxTrue );
	}

	gxLib::Scissor( 0, 0,0,0 ,MAX_PRIORITY_NUM + 1 );

}


void CGamePad::drawDevice( Sint32 id , Sint32 ax , Sint32 ay , gxBool bControlEnable )
{
	//----------------------------------------------------
	//コントローラーの状態を表示する
	//----------------------------------------------------

	Sint32 az = MAX_PRIORITY_NUM + 1;

	static int n_Cnt = 0;
	n_Cnt ++;
	if( n_Cnt < 30 ) return;

	CGameingDevice *pDevice = &m_Device[id];

	if( m_CurentDevice == id )
	{
		gxLib::DrawBox( 32 , ay-24 , WINDOW_W-32 , ay+128+16 , az , gxFalse , ATR_DFLT , 0xFF00ff00 , 3.0f );
	}

	if( pDevice->_DeviceType & 0x01 )
	{
		//360コントローラー
		gxLib::DrawBox( ax , ay , ax+128 , ay+128 , az , gxTrue , ATR_DFLT , 0xFF808080 );
	}
	else
	{
		gxLib::DrawBox( ax , ay , ax+128 , ay+128 , az , gxTrue , ATR_DFLT , 0xFF808080 );
	}

	Sint32 m = pDevice->_PlayerID;
	gxChar* tbl[] = {
		"1P" , "2P", "3P" , "4P","Not USE"
	};

	gxChar* pPlayeIndex = tbl[m];

	if( m >= PLAYER_MAX ) pPlayeIndex = "NoUse";

	gxLib::DrawLine( ax , ay+64 , ax+128 , ay+64 , az , ATR_DFLT , 0x4000ff00 );
	gxLib::DrawLine( ax+64 , ay , ax+64 , ay+128 , az , ATR_DFLT , 0x4000ff00 );
	gxLib::Printf( ax,ay-16,az,ATR_DFLT , ARGB_DFLT , "%02d:%s [ %s ] %s" , id , pDevice->_hardwarename , pPlayeIndex  , pDevice->_bEnableConfig? "Configured" : "" );

	//axy
	Sint32 wh = 12;
	if( pDevice->_bAxisX && pDevice->_bAxisY )
	{
		Sint32 lx = ax+64 + pDevice->_Axis.x*64;
		Sint32 ly = ay+64 + pDevice->_Axis.y*64;

		gxLib::DrawBox( lx -wh , ly-wh , lx+wh , ly+wh , az , gxTrue , ATR_DFLT , 0xFFff0000 );
	}

	//rxy
	wh = 8;

	if( pDevice->_bAxisRX && pDevice->_bAxisRY )
	{
		Sint32 rx = ax+64 + pDevice->_Rotation.x*64;
		Sint32 ry = ay+64 + pDevice->_Rotation.y*64;
		gxLib::DrawBox( rx -wh , ry-wh , rx+wh , ry+wh , az , gxTrue , ATR_DFLT , 0xFF0000ff );
	}

	//z
	wh = 4;
	if( pDevice->_bAxisZ || pDevice->_bAxisRZ )
	{
		Sint32 zx = ax+64 + pDevice->_Axis.z*64;
		Sint32 zy = ay+64 + pDevice->_Rotation.z*64;
		gxLib::DrawBox( zx -wh , zy-wh , zx+wh , zy+wh , az , gxTrue , ATR_DFLT , 0xFFffff00 );
	}

	//pov

	if( pDevice->_bPov[0] )
	{
		for( Sint32 ii=0; ii<4; ii++ )
		{
			Sint32 px = ax + ii*16 + 140;
			Sint32 py = ay+8;
			int tbl[]={
				JOY_U,JOY_R,JOY_D,JOY_L
			};
			
			Uint32 argb = 0xFF00F000;
			gxLib::DrawBox( px , py , px+14 , py+14 , az , gxTrue , ATR_DFLT , (pDevice->_Pov0 & tbl[ii])? argb : 0xFF808080 );
		}
	}

	//btn

	gxBool bBtn = gxFalse;

	for( Uint32 jj=GDBTN_BUTTON00; jj< GDBTN_BUTTON31; jj++ )
	{
		GAMINGDEVICE_BTNID id = (GAMINGDEVICE_BTNID)jj;

		Sint32 tx,ty;
		Sint32 ii = jj - GDBTN_BUTTON00;

		tx = ax+(ii%4)*16 + 140;
		ty = ay+32+ii/4*16;

		if (ii >= 24) continue;	//24ボタン以上は関知しない

		if (pDevice->IsPress( id ) )
		{
			id = id;
		}

		Uint32 argb = 0xFF00F000;
		gxLib::DrawBox( tx , ty , tx+14 , ty+14 , az , gxTrue , ATR_DFLT , pDevice->IsPress( id )? argb : 0xFF808080 );
	}
}


void CGamePad::deviceUseConfig()
{
	//使用するコントローラの可否を決定する

	Sint32 max = PLAYER_MAX;
	Sint32 ax = 64;
	Sint32 ay = 320;
	Sint32 az = MAX_PRIORITY_NUM + 1;

	Sint32 dx,dy;

		//pDevice->_PlayerID;

	if( gxUtil::KeyBoard()->IsTrigger( gxKey::NUMPAD_UP ))
	{
		m_Cursor --;
	}
	else if( gxUtil::KeyBoard()->IsTrigger( gxKey::NUMPAD_DOWN ))
	{
		m_Cursor ++;
	}
	else if( gxUtil::KeyBoard()->IsTrigger( gxKey::RETURN ))
	{
		m_Device[m_Cursor]._PlayerID ++;
		m_Device[m_Cursor]._PlayerID = ((max + 1) + m_Device[m_Cursor]._PlayerID) % (max + 1);
	}
	else if( gxUtil::KeyBoard()->IsTrigger( gxKey::BS ) )
	{
		m_Selecter = 0;
		m_Cursor = 0;
	}

	m_Cursor = ( m_DeviceCnt + m_Cursor )%m_DeviceCnt;


	//------------------------------------------------
	//btn

	for( Sint32 ii=0; ii<m_DeviceCnt; ii++ )
	{
		CGameingDevice *pDevice = &m_Device[ii];
		gxBool bBtn = gxFalse;

		for( Sint32 jj= GDBTN_BUTTON00; jj< GDBTN_BUTTON31; jj++ )
		{
			if( pDevice->IsPress( (GAMINGDEVICE_BTNID)jj ) )//_button[jj] )
			{
				bBtn = gxTrue;
				break;
			}
		}

		if( bBtn )
		{
			m_ButtonPush[ii] ++;
			if( m_ButtonPush[ii] >= 32 )
			{
				m_ButtonPush[ii] = 32;
			}
		}
		else
		{
			m_ButtonPush[ii] = 0;
		}
	}


	static gxChar* tbl[] = {
		"1P" , "2P" , "3P" , "4P" , "NA"
	};

	for( Sint32 ii=0; ii<m_DeviceCnt; ii++ )
	{
		CGameingDevice *pDevice = &m_Device[ii];

		if( m_ButtonPush[ii] == 0x01 )
		{
			m_Device[ii]._PlayerID ++;
			m_Device[ii]._PlayerID = ((max+1)+m_Device[ii]._PlayerID )%(max+1);
		}

		dx = ax+32;
		dy = ay + 16*ii;

		Uint32 argb = 0xffffffff;
		for( Sint32 jj=0; jj<=max; jj++ )
		{
			argb = 0xffffffff;
			if( jj == pDevice->_PlayerID )
			{
				argb = 0xffff0000;
			}
			if (jj >= max) tbl[jj] = "NoUSE";

			gxLib::Printf( dx+jj*32 , dy , az , ATR_DFLT , argb , "%s" , tbl[jj]  );
		}

		dx = ax+256;
		dy = ay + 16*ii;

		argb = 0xffffffff;
		if(m_Cursor == ii )
		{
			argb = 0xff00ff00;
		}
		gxLib::Printf( dx,dy,az,ATR_DFLT , argb , "%02d:%s" , ii , pDevice->_hardwarename );
	}


	//コントローラーの使用可否状況をアップデートする
	updateGamepadAssignTolPlayer();

}


void CGamePad::buttonConfig()
{
	//ボタンをアサインする

	Sint32 id = m_CurentDevice;
	CGameingDevice* pDevice = &m_Device[id];

	size_t num = sizeof(GamingDeviceButtonNameTbl) / sizeof(GamingDeviceButtonNameTbl[0]);

	Sint32 ax = 64;
	Sint32 ay = 320;
	Sint32 az = MAX_PRIORITY_NUM + 1;

	gxLib::Printf(ax, ay, az, ATR_DFLT, ARGB_DFLT, "%s", pDevice->_hardwarename);

	Sint32 max = sizeof(pGxPadTbl) / sizeof(pGxPadTbl[0]);

	//gxLib::Printf(ax, ay+64+0*16, az, ATR_DFLT, ARGB_DFLT, "%s", pGxPadTbl[0] );


	//-----------------------------------

	ay += 64;
	drawDevice(id, ax, ay, gxTrue);
	ay = 480;

	//--------------------------------------------
	//全ボタンをスキャンする
	//--------------------------------------------

	if( m_Selecter == 350 )
	{
		analogConfig();
		return;
	}

	if (gxUtil::KeyBoard()->IsTrigger(gxKey::RETURN))
	{
		m_Cursor = 0;
		m_OldButtonID = -1;
		m_Selecter = 350;
		return;
	}
	else if (gxUtil::KeyBoard()->IsRepeat(gxKey::NUMPAD_DOWN))
	{
		m_Cursor++;
	}
	else if (gxUtil::KeyBoard()->IsRepeat(gxKey::NUMPAD_UP))
	{
		m_Cursor--;
	}
	else if (gxUtil::KeyBoard()->IsTrigger(gxKey::DEL))
	{
		pDevice->_Assign[m_Cursor] = GDBTN_NONE;

		/*
			m_Cursor = 0;
			m_OldButtonID = -1;
			for (Sint32 ii = 0; ii < BTN_MAX; ii++)
			{
				pDevice->_Assign[ii] = 0;
			}
		*/

		return;
	}
	else if (gxUtil::KeyBoard()->IsTrigger(gxKey::BS))
	{
		m_Selecter = 200;
		m_Cursor = 0;
		return;
	}

	Uint32 padButtonPush[GDBTN_MAX] = {};
	Float32 fRange = 0.85f;

	Sint32 btnID = -1;

	Sint32 maxNum = sizeof(g_ButtonAssignTbl) / sizeof(g_ButtonAssignTbl[0]);

	for(Sint32 ii=0; ii<maxNum; ii++ )
	{
		if (ii == 32)
		{
			ii += 0;
		}

		if(pDevice->IsPress( (GAMINGDEVICE_BTNID)g_ButtonAssignTbl[ ii ] ) )
		{
			btnID = g_ButtonAssignTbl[ ii ];
			break;
		}
	}

	ay = WINDOW_H/2;

	if( btnID != m_OldButtonID && btnID != -1 )
	{
		pDevice->_Assign[m_Cursor] = btnID;
		m_Cursor ++;
	}
	m_OldButtonID = btnID;

	m_Cursor = m_Cursor%num;

	Uint32 argb = 0xffffffff;
	for( Sint32 ii=0; ii<num; ii++ )
	{
		argb = 0xffffffff;
		if( m_Cursor == ii ) argb = 0xff00ff00;

		const gxPadName *p = &GamingDeviceButtonNameTbl[ii];

		gxLib::Printf( ax, ay + ii*16, az, ATR_DFLT, argb, "%s", p->Name );

		if( pDevice->_Assign[ii] == 0 )
		{
			gxLib::Printf( ax + 256 , ay + ii*16, az, ATR_DFLT, argb, "-----" );
		}
		else
		{
			//gxLib::Printf( ax + 256 , ay + ii*16, az, ATR_DFLT, argb, "%d", pDevice->_Assign[ii] , DeviceName[ pDevice->_Assign[ii] ] );
			gxLib::Printf( ax + 256 , ay + ii*16, az, ATR_DFLT, argb, "%s", DeviceName[ pDevice->_Assign[ii] ] );
		}
	}

	//コンフィグ済みかどうか？

	pDevice->_bEnableConfig = 0;

	for (Sint32 jj = 0; jj < BTN_MAX; jj++)
	{
		if (pDevice->_Assign[jj]) pDevice->_bEnableConfig = 1;
	}
}


void CGamePad::analogConfig()
{
	//アナログスティックのバインドを変更する

	Sint32 ax,ay,az;
	Sint32 w = WINDOW_W/2;
	Sint32 h = 160;

	ax = WINDOW_W/2;
	ay = WINDOW_H/2;
	az = MAX_PRIORITY_NUM + 1;

	if (gxUtil::KeyBoard()->IsTrigger(gxKey::BS))
	{
		m_OldButtonID = -1;
		m_Selecter = 300;
		return;
	}

	gxLib::DrawBox( ax-w/2, ay-h/2, ax+w/2, ay+h/2, az , gxTrue , ATR_DFLT , 0xC00101ff );
	gxLib::DrawBox( ax-w/2, ay-h/2, ax+w/2, ay+h/2, az, gxFalse, ATR_DFLT, 0xf001ff01 , 3.0f);

	Sint32 id = m_CurentDevice;
	CGameingDevice* pDevice = &m_Device[m_CurentDevice];
	Sint32 index = -1;

	if( (pDevice->IsPress( GDBTN_AXIS_XA ) ) || (pDevice->IsPress( GDBTN_AXIS_XS ) ) )
	{
		index = gxPadManager::ANALOG_LX;
	}
	else if( (pDevice->IsPress( GDBTN_AXIS_YA ) ) || (pDevice->IsPress( GDBTN_AXIS_YS ) ) )
	{
		index = gxPadManager::ANALOG_LY;
	}
	if( (pDevice->IsPress( GDBTN_ROT_XA ) ) || (pDevice->IsPress( GDBTN_ROT_XS ) ))
	{
		index = gxPadManager::ANALOG_RX;
	}
	else if( (pDevice->IsPress( GDBTN_ROT_YA ) ) || (pDevice->IsPress( GDBTN_ROT_YS ) ))
	{
		index = gxPadManager::ANALOG_RY;
	}
	else if( (pDevice->IsPress( GDBTN_AXIS_ZA ) ) || (pDevice->IsPress( GDBTN_AXIS_ZS ) ))
	{
		index = gxPadManager::ANALOG_LT;
	}
	else if( (pDevice->IsPress( GDBTN_ROT_ZA ) ) || (pDevice->IsPress( GDBTN_ROT_ZS ) ))
	{
		index = gxPadManager::ANALOG_RT;
	}

	if( index != -1 && m_OldButtonID != index )
	{
		gxLib::Printf( ax, ay+h, az, ATR_DFLT, ARGB_DFLT, "%s", AnalogName[index] );
		pDevice->_cnvAnalog[ m_Cursor ] = index;
		m_Cursor++;
		m_Cursor = m_Cursor % 6;
	}
	m_OldButtonID = index;

	if (gxUtil::KeyBoard()->IsRepeat(gxKey::NUMPAD_DOWN))
	{
		m_Cursor++;
		m_Cursor = (6+m_Cursor)%6;
	}
	else if (gxUtil::KeyBoard()->IsRepeat(gxKey::NUMPAD_UP))
	{
		m_Cursor--;
m_Cursor = (6 + m_Cursor) % 6;
	}
	else if (gxUtil::KeyBoard()->IsTrigger(gxKey::DEL))
	{
	pDevice->_cnvAnalog[m_Cursor] = gxPadManager::NOUSE_ANALOG_AXIS;
	return;
	}
	else if (gxUtil::KeyBoard()->IsTrigger(gxKey::RETURN))
	{
	for (Sint32 ii = 0; ii < gxPadManager::ANALOG_MAX; ii++)
		pDevice->_cnvAnalog[ii] = ii;
	return;
	}


	for (Sint32 ii = 0; ii < 6; ii++)
	{
		Uint32 argb = 0xffffffff;
		if (m_Cursor == ii) argb = 0xff00ff00;
		gxLib::Printf(ax - w / 2 + 32, ay - h / 2 + ii * 24 + 16, az, ATR_STR_LEFT, argb, "%s", StickName[ii]);
		gxLib::Printf(ax + w / 2 - 32, ay - h / 2 + ii * 24 + 16, az, ATR_STR_RIGHT, argb, "%s", AnalogName[pDevice->_cnvAnalog[ii]]);
	}


}

void CGamePad::keyBoardConfig()
{
	//キーボードのバインドを変更する
	Sint32 maxNum = sizeof(GamingDeviceButtonNameTbl) / sizeof(GamingDeviceButtonNameTbl[0]);

	if (gxUtil::KeyBoard()->IsTrigger(gxKey::RETURN))
	{
		//m_Cursor = 0;
		//m_OldButtonID = -1;
		//m_Selecter = 350;
		return;
	}
	else if (gxUtil::KeyBoard()->IsTrigger(gxKey::BS))
	{
		m_Selecter = 0;
		m_Cursor = 0;
		return;
	}
	else if (gxUtil::KeyBoard()->IsRepeat(gxKey::NUMPAD_DOWN))
	{
		m_Cursor++;
		m_Cursor = (maxNum + m_Cursor) % maxNum;
		return;
	}
	else if (gxUtil::KeyBoard()->IsRepeat(gxKey::NUMPAD_UP))
	{
		m_Cursor--;
		m_Cursor = (maxNum + m_Cursor) % maxNum;
		return;
	}
	else if (gxUtil::KeyBoard()->IsTrigger(gxKey::DEL))
	{
		//pDevice->_Assign[m_Cursor] = GDBTN_NONE;
		KeyConfigTbl[m_Cursor] = gxKey::KEYNONE;
		m_OldButtonID = gxKey::DEL;
		m_Cursor++;
		m_Cursor = (maxNum + m_Cursor) % maxNum;
		return;
	}

	Sint32 ax = 64;
	Sint32 ay = 320;
	Sint32 az = MAX_PRIORITY_NUM + 1;
	Uint32 argb = 0xffffffff;

	for (Sint32 ii = 0; ii < maxNum; ii++)
	{
		argb = 0xffffffff;
		if (m_Cursor == ii) argb = 0xff00ff00;

		const gxPadName* p = &GamingDeviceButtonNameTbl[ii];

		gxLib::Printf(ax, ay + ii * 16, az, ATR_DFLT, argb, "%s", p->Name);

		if (KeyConfigTbl[ii] == gxKey::KEYNONE)
		{
			gxLib::Printf(ax + 256, ay + ii * 16, az, ATR_DFLT, argb, "-- NONE -- ");
		}
		else
		{
			//gxLib::Printf( ax + 256 , ay + ii*16, az, ATR_DFLT, argb, "%d", KeyConfigTbl[ ii ] );
			gxLib::Printf(ax + 256, ay + ii * 16, az, ATR_DFLT, argb, "%s", gxUtil::KeyBoard()->GetKeyString(KeyConfigTbl[ii]));
		}
	}

	//--------------------------------------------------

	Sint32 key = -1;

	for (Sint32 ii = 0; ii < 256; ii++)
	{
		if (m_KeyBoardPush[ii] == 1)
		{
			key = ii;
			break;
		}
	}

	if (!m_bInitConfig )
	{
		if (key == -1)
		{
			m_bInitConfig = gxTrue;
		}

	}

	if ((m_OldButtonID == key && key!=-1) || !m_bInitConfig)
	{
		//キーを離すまで待つ
		gxLib::Printf(ax + 32, ay + 18 * 16, az, ATR_DFLT, 0xffff0000, "Please Release [%s] Key!", gxUtil::KeyBoard()->GetKeyString((gxKey::KeyType)key));
		return;
	}

	if( key != -1 && (m_OldButtonID != key) )
	{
		KeyConfigTbl[ m_Cursor ] = (gxKey::KeyType )key;
		m_Cursor ++;
	}
	m_Cursor = (maxNum + m_Cursor) % maxNum;
	m_OldButtonID = key;


}


void CGamePad::updateGamepadAssignTolPlayer()
{
	//使用可否からプレイヤーIDを割り振る

	for( Sint32 ii=0; ii<m_DeviceCnt; ii++ )
	{
		CGameingDevice *pDevice = &m_Device[ii];

		if( pDevice->_bDeviceDisable )
		{
			pDevice->_PlayerID = NOT_USE_ID;
			continue;
		}

	}
}


void ControllerDeviceConfig()
{
	//デバイスごとのコントローラーのコンフィグ画面（Windows用）

	CGamePad::GetInstance()->DeviceAssignmentConfig(gxTrue);

}

