﻿//------------------------------------------------------------
//
// machine.cpp
// マシン固有のデバイスへのアクセスを行うものはここに書く
// ハードウェアに依存する部分
//
//------------------------------------------------------------
#include <gxLib.h>
#include <gxLib/gx.h>
#include <gxLib/gxFileManager.h>
#include <gxLib/gxDebug.h>
#include "gxLibResource.h"
#include "../CHttpClient.h"
#include "../../CFileSystem.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <io.h>
#include <fcntl.h>
#include <shellapi.h>
#include <shlobj.h>
#include <shlobj_core.h>
#include <cassert>
#include <filesystem>
#include <fstream>
//-----------------------------

#ifdef _USE_OPENGL
	#include "COpenGLWin.h"
#else
	#include "CDx11Desktop.h"
#endif

#ifdef _USE_OPENAL
	#include "../../COpenAL.h"
#else
	#include "../CXAudio2.h"
#endif

#include "CGamePad.h"


SINGLETON_DECLARE_INSTANCE( CDeviceManager );

#define NAME_APRICATION APPLICATION_NAME
#define STORAGE_PATH_ROM  "Storage\\01_rom"
#define STORAGE_PATH_DISK "Storage\\03_disk"

static wchar_t WCharBuf[1024];
static wchar_t* pWChar = WCharBuf;// NULL;

enum class EGXDir {
	eGXDirectoryNeedSearch,
	eGXDirectoryExist,
	eGXDirectoryDisable,
};

static Float32 s_win_time = 0.0f;

static EGXDir s_bEnableGxDirectory = EGXDir::eGXDirectoryNeedSearch;
Uint8* loadFile2(const gxChar* pFileNameU8, size_t* pLength);
wchar_t* CDeviceManager_UTF8toUTF16(gxChar* pString, size_t* pSize = nullptr);
std::vector<Uint16> CDeviceManager_UTF8toUTF16(std::string);

//------------------------------------------------------------

CDeviceManager::CDeviceManager()
{
	CHttpClient::CreateInstance();
}


CDeviceManager::~CDeviceManager()
{
	//CHttpClient::DeleteInstance();
}


void CDeviceManager::AppInit()
{
	static gxBool bFirst = gxTrue;

	if( bFirst )
	{
		bFirst = gxFalse;
		CGameGirl::GetInstance()->Init();
		CGraphics::GetInstance()->Init();
		CAudio::GetInstance()->Init();
	    CGamePad::GetInstance()->Init();
		gxLib::SetVirtualPad( CWindows::GetInstance()->m_bVirtualPad );
	}
	else
	{
        CGameGirl::GetInstance()->SetResume();
	}

	CGameGirl::GetInstance()->AdjustScreenResolution();

}


void CDeviceManager::GameInit()
{
	//バックで受け取った入力機器の情報をgxLib側へ更新する

	CGamePad::GetInstance()->Action();
}


gxBool CDeviceManager::GamePause()
{
	return ::GamePause();
}

gxBool CDeviceManager::GameUpdate()
{
	return gxTrue;
}


void CDeviceManager::Render()
{
	//描画処理
	
	if( CGameGirl::GetInstance()->IsResume() )
	{
		return;
	}

	CGraphics::GetInstance()->Update();
	CGraphics::GetInstance()->Render();
}


void CDeviceManager::vSync()
{
	//1/60秒の同期待ち
	const Float32 Thr = (1.0f/ FRAME_PER_SECOND);

	Float32 _TimeNow  = CGameGirl::GetInstance()->GetTime();

	Float32 DiffTime  = _TimeNow - s_win_time;
	int		SleepMS   = ((Thr - DiffTime) * 1000) - 1;
	if (SleepMS > 0)
	{
		CDeviceManager::GetInstance()->Sleep(SleepMS);
	}

	Float32 TimeEnd   = s_win_time + Thr;
	TimeEnd -= Thr / 32;	// vSync後 Flip が終わるまでの時間を若干考慮.
	do
	{
		_TimeNow = CGameGirl::GetInstance()->GetTime();

#ifdef _USE_OPENGL
		CDeviceManager::GetInstance()->Sleep(1);
#else
		CDeviceManager::GetInstance()->Sleep(0);	//DirectXの時はここのSLEEP(1)が安定しない
#endif

	}
	while( _TimeNow < TimeEnd );

	s_win_time = CGameGirl::GetInstance()->GetTime();
}


void CDeviceManager::Flip()
{
	//バックバッファに用意した画像を転送する

	CGraphics::GetInstance()->Present();

	// Flip と vSync が対で使われる前提で時間計測.
	s_win_time = CGameGirl::GetInstance()->GetTime();
}

void CDeviceManager::Resume()
{

}


void CDeviceManager::Movie()
{
	
}


void CDeviceManager::Play()
{
	CAudio::GetInstance()->Action();
}


gxBool CDeviceManager::NetWork()
{
	if (!CHttpClient::IsExistInstance()) return gxTrue;

	CHttpClient::GetInstance()->Action();

	return gxTrue;
}


void CDeviceManager::UploadTexture(Sint32 sBank)
{
	CGraphics::GetInstance()->ReadTexture( sBank );
}


void CDeviceManager::SetStatusBar(gxBool bOn)
{

}

void CDeviceManager::LogDisp(char* pString)
{
	//デバッグログの表示

	if( CDeviceManager::GetInstance()->IsBatchMode() )
	{
		printf(pString);
		printf("\n");
		return;
		/*
		wchar_t* p = CDeviceManager_UTF8toUTF16(pString);
		std::wcout.imbue(std::locale("", std::locale::ctype));
		std::wstring str = p;

		// 標準出力へ出力する
		std::wcout << str << std::endl;
		SAFE_DELETES(p);
		return;
		*/
	}

	wchar_t* p = CDeviceManager_UTF8toUTF16(pString);
	OutputDebugStringW(p);
	OutputDebugStringW( L"\n" );
	SAFE_DELETES(p);
}


void CDeviceManager::ToastDisp( gxChar* pMessage )
{
	NOTIFYICONDATA	tn = { NOTIFYICONDATA_V2_SIZE };
	tn.hWnd = CWindows::GetInstance()->m_hWindow;
	tn.uID = 100;
	tn.uFlags = NIF_MESSAGE | NIF_ICON | NIF_TIP;
	//tn.uCallbackMessage = WM_FASTCOPY_NOTIFY;
	tn.hIcon = LoadIcon(CWindows::GetInstance()->m_hInstance, (LPCSTR)(LONG_PTR)IDI_GXLIB_ICON);	//hIcon;
	sprintf(tn.szTip, "testtest");
	tn.uFlags |= NIF_INFO;

	//strncpy(tn.szInfoTitle, APPLICATION_NAME, sizeof(tn.szInfoTitle));
	//strncpy(tn.szInfo, pMessage, sizeof(tn.szInfo));

	gxChar buf[1024];
	size_t sz;
	sprintf(buf, "%s", APPLICATION_NAME);
	std::string strSJIS = CDeviceManager::UTF8toSJIS(buf, &sz);
	strncpy(tn.szInfoTitle, strSJIS.c_str(), sizeof(tn.szInfoTitle));

	sprintf(buf, "%s", pMessage);
	strSJIS = CDeviceManager::UTF8toSJIS(buf, &sz);
	strncpy(tn.szInfo, strSJIS.c_str(), sizeof(tn.szInfo));


	tn.uTimeout		= 3 * 1000;
	tn.dwInfoFlags	= NIIF_INFO | NIIF_NOSOUND;
	tn.dwState = NIS_SHAREDICON;
	
	::Shell_NotifyIcon(NIM_DELETE, &tn);
	::Shell_NotifyIcon(NIM_ADD, &tn);

}


void CDeviceManager::OpenWebClient( gxChar* pString , gxBool bOpenWebBrowser )
{
    //WebViewを表示するリクエストを発行する
    //sprintf( CAndroid::GetInstance()->m_WebViewURLString,"%s" , pString );
	//CWindows::GetInstance()->ExecuteApp( pString );
	CWindows::GetInstance()->m_bWebBrowser = bOpenWebBrowser;
	CWindows::GetInstance()->m_WebViewURL = pString;
}


gxBool CDeviceManager::SetAchievement( Uint32 achieveindex )
{
	gxLib::SetToast("Achievement (%d)" , achieveindex );

	return gxTrue;
}

gxBool CDeviceManager::GetAchievement( Uint32 achieveindex )
{
	return gxTrue;
}

Uint8* CDeviceManager::LoadFile( const gxChar* pFileNameU8 , size_t* pLength , ESTORAGE_LOCATION location )
{
	std::string url = pFileNameU8;
	std::replace(url.begin(), url.end(), '/', '\\');
	std::string fileName = "";

	if (std::string::npos != url.find(':'))
	{
		location = ESTORAGE_LOCATION::EXTERNAL;
	}

	if (s_bEnableGxDirectory == EGXDir::eGXDirectoryNeedSearch )
	{
		//Storageディレクトリが存在しない場合はexeの実行環境にファイルを保存する

		if (CFileSystem::IsExist(STORAGE_PATH_ROM))
		{
			//存在する
			s_bEnableGxDirectory = EGXDir::eGXDirectoryExist;
		}
		else
		{
			//存在しない
			s_bEnableGxDirectory = EGXDir::eGXDirectoryDisable;
		}
	}

	if (s_bEnableGxDirectory == EGXDir::eGXDirectoryDisable)
	{
		//Storage/01_romフォルダがない場合は実行ファイル直下にファイルを読みに行く
		location = ESTORAGE_LOCATION::EXTERNAL;
	}

	Uint8 *pData = nullptr;

	switch( location ){
	case ESTORAGE_LOCATION::ROM:

		//rom
		fileName = STORAGE_PATH_ROM;
		fileName += '\\';
		fileName += url;
		pData = loadFile2(fileName.c_str(), pLength);
		break;

	case ESTORAGE_LOCATION::INTERNAL:
		//disk
		fileName = STORAGE_PATH_DISK;
		fileName += '\\';
		fileName += url;
		pData = loadFile2(fileName.c_str(), pLength);
		break;

	case ESTORAGE_LOCATION::EXTERNAL:
	default:
		fileName = url;
		pData = loadFile2(fileName.c_str(), pLength);
		break;
	}

	return pData;
}


gxBool CDeviceManager::SaveFile( const gxChar* pFileNameU8 , Uint8* pWriteBuf , size_t uSize, ESTORAGE_LOCATION location )
{
	//ファイルの書き込み

	Uint32 len = gxUtil::StrLen(pFileNameU8);

	std::string url = pFileNameU8;
	std::replace(url.begin(), url.end(), '/', '\\');
	std::string fileName = "";

	if (s_bEnableGxDirectory == EGXDir::eGXDirectoryNeedSearch)
	{
		//Storageディレクトリが存在しない場合はexeの実行環境にファイルを保存する

		if (CFileSystem::IsExist(STORAGE_PATH_DISK))
		{
			//存在する
			s_bEnableGxDirectory = EGXDir::eGXDirectoryExist;
		}
		else
		{
			//存在しない
			s_bEnableGxDirectory = EGXDir::eGXDirectoryDisable;
		}
	}

	if (std::string::npos != url.find(':'))
	{
		location = ESTORAGE_LOCATION::EXTERNAL;
	}
	else
	{
		// : を発見できなかったらカレントディレクトリ直下に作成
		bool bCurDirWrite = false;
		if (s_bEnableGxDirectory == EGXDir::eGXDirectoryDisable) bCurDirWrite = true;

		if (location == ESTORAGE_LOCATION::EXTERNAL) bCurDirWrite = true;

		if(bCurDirWrite)
		{
			char buf[1024];
			GetCurrentDirectory(1024, buf);	//sjisで来るので注意！！
			std::string tmp;
			tmp = buf;

			size_t uSizePath = 0;
			tmp = SJIStoUTF8((char*)tmp.c_str(), &uSizePath);
			
			tmp += '\\';
			tmp += url;
			url = tmp;
		}
	}

	switch (location) {
	case ESTORAGE_LOCATION::ROM:
		return gxFalse;

	case ESTORAGE_LOCATION::INTERNAL:
		fileName = STORAGE_PATH_DISK;
		fileName += '\\';
		fileName += url;
		if (s_bEnableGxDirectory == EGXDir::eGXDirectoryDisable)
		{
			fileName = url;
		}
		break;

	case ESTORAGE_LOCATION::EXTERNAL:
	default:
		fileName = url;
		break;
	}

	if (pWriteBuf == nullptr)
	{
		//ファイル削除

		CFileSystem::RemoveSJIS(fileName);

		std::string temp = fileName;

		if (temp.back() == '/' || temp.back() == '\\')
		{
			temp.pop_back();
			CFileSystem::CreateDirectorySJIS(temp);
		}

		return true;
	}

	if (!CFileSystem::WriteFileSJIS(fileName, pWriteBuf, uSize))
	{
		//SJISで書き出し成功できなかった
		if (!CFileSystem::WriteFile(fileName, pWriteBuf, uSize))
		{
			return gxFalse;
		}
	}

	return gxTrue;
}


void CDeviceManager::MakeThread( void* (*pFunc)(void*) , void * pArg )
{
	// スレッドの作成
	//void func( void *arg )型の関数ポインタを渡すこと！ 

	DWORD threadId;
	HANDLE hThread;

	hThread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)pFunc, (LPVOID)pArg, CREATE_SUSPENDED, &threadId); 
	if (hThread)
	{
		SetThreadPriority(hThread, THREAD_PRIORITY_HIGHEST);

		// スレッドの起動 
		ResumeThread(hThread);
	}
}


void CDeviceManager::Sleep( Uint32 msec )
{
	::Sleep( msec );
}


gxBool CDeviceManager::PadConfig()
{
	//非対応
	return gxTrue;
}


gxBool CDeviceManager::PadConfig( Sint32 padNo , Uint32 button )
{
	return gxTrue;
}


std::vector<std::string> CDeviceManager::GetDirList( std::string rootDirUTF8 )
{
	//U8をSJISに変換
	//※Zipからの展開だとZipに内包されたファイル名がSJISでわたってくる可能性があるが
	//UTF8で取り回す仕様で固定する

	size_t uSizePath = 0;
	auto sjis = UTF8toSJIS((gxChar*)rootDirUTF8.c_str(), &uSizePath);

	return CFileSystem::ScanDirectory(sjis);
}

Float32 CDeviceManager::GetFreeStorageSize()
{
	LPITEMIDLIST pidlist;

	char szPathName[MAX_PATH];
	HRESULT ret = SHGetSpecialFolderLocation( NULL, CSIDL_PERSONAL, &pidlist );
	if (ret == S_OK)
	{
		SHGetPathFromIDList(pidlist, szPathName);
		std::string cPath = szPathName;

		ULARGE_INTEGER FreeAvailableSize, TotalSize, FreeSize;
		if (GetDiskFreeSpaceEx(cPath.c_str(), &FreeAvailableSize, &TotalSize, &FreeSize))
		{
			size_t sz = FreeAvailableSize.QuadPart / 1024;

			return Float32(sz / 1024.0f);
		}
	}

	return 0.0f;
}


Uint8* loadFile2( const gxChar* pFileNameU8, size_t* pLength )
{
	std::vector<uint8_t> buf;

	if (CFileSystem::ReadFileSJIS(pFileNameU8, buf))
	{
		uint8_t* p = nullptr;
		if (buf.size())	//0byteのファイルの場合もありうる
		{
			p = new Uint8[buf.size() + 1];
			gxUtil::MemCpy(p, &buf[0], buf.size());
			p[buf.size()] = 0x00;
		}

		if (pLength)
		{
			*pLength = buf.size();
		}

		return p;
	}

	if (CFileSystem::ReadFile(pFileNameU8, buf))
	{
		//if (buf.size())	//0byteのファイルの場合もありうる
		{
			uint8_t* p = new Uint8[buf.size() + 1];
			gxUtil::MemCpy(p, &buf[0], buf.size());
			p[buf.size()] = 0x00;

			if (pLength)
			{
				*pLength = buf.size();
			}
			return p;
		}
	}

	return nullptr;
}

std::string CDeviceManager::GetClipBoardString()
{
	//クリップボードのテキストを参照する
	std::string str;

	HGLOBAL hMem;       // 取得用のメモリ変数
	LPTSTR  lpBuff;     // 参照用のポインタ
//	gxChar* p = NULL;

	if ( OpenClipboard( CWindows::GetInstance()->m_hWindow ) )
	{
	    if ( (hMem = GetClipboardData(CF_TEXT)) != NULL )
	    {
	        if ( (lpBuff = (LPTSTR)GlobalLock(hMem)) != NULL )
	        {
				Uint32 len = (Uint32)strlen( lpBuff );
				//p= new gxChar[len];
				//sprintf( p , "%s" , lpBuff );

				str = CDeviceManager::GetInstance()->SJIStoUTF8(lpBuff);

	            GlobalUnlock( hMem );
	        }
	    }
	    CloseClipboard();
	}


	return str;
}


void CDeviceManager::SetClipBoardString(std::string str)
{
	//テキストをクリップボードに保存する
	HGLOBAL     hMem;       // 設定用のメモリ変数
	LPTSTR      lpBuff;     // 複写用のポインタ
	DWORD       dwSize;     // 複写元の長さ

	dwSize = ((lstrlen(str.c_str()) + 1) * sizeof(TCHAR));   // '\0' 文字分を加算

	//make
	if ((hMem = GlobalAlloc((GHND | GMEM_SHARE), dwSize)) != NULL)
	{
		if ((lpBuff = (LPTSTR)GlobalLock(hMem)) != NULL)
		{
			lstrcpy(lpBuff, str.c_str());                  // lpBuff にコピー
			GlobalUnlock(hMem);

			//if (OpenClipboard(CWindows::GetInstance()->m_hWindow))
			if (OpenClipboard(nullptr))
			{
				EmptyClipboard();
				SetClipboardData(CF_TEXT, hMem);      // データを設定
				CloseClipboard();
				GlobalFree(hMem);                             // クリップボードが開けないとき解放
				return;
			}
		}
		GlobalFree(hMem);                             // クリップボードが開けないとき解放
	}


	return;
}



//-----------------------------------------
//専用
//-----------------------------------------



std::string CDeviceManager_UTF16toUTF8(wchar_t *pUTF16buf, size_t* pSize)
{
	Uint8* u8 = (Uint8*)pUTF16buf;

	if (u8[0] == 0xff && u8[1] == 0xfe)
	{
		//BOM付きだったのでオフセットする
		pUTF16buf++;
	}

	size_t u16len = wcslen(pUTF16buf);

	if (u16len == 0)
	{
		gxChar* pBuf = new gxChar[1];
		if (pSize) *pSize = 0;
		pBuf[0] = 0x00;
		return pBuf;
	}

	gxChar* pBuf = new gxChar[u16len*4];

	size_t length = WideCharToMultiByte( CP_UTF8, 0, pUTF16buf, -1, &pBuf[0], int(u16len * 4), NULL, NULL);

	if (length > u16len * 4)
	{
		if(pSize) *pSize = 0;
		pBuf[0] = 0x00;
		return pBuf;
	}

	if( pSize ) *pSize = length;

	std::string ret;

	ret = pBuf;

	return ret;

	/*
	gxChar* pBuf2 = new gxChar[length + 1];
	gxUtil::MemCpy(pBuf2, pBuf, length);
	pBuf2[length] = 0x00;
	SAFE_DELETES( pBuf );

	if( pSize ) *pSize = length;

	return (gxChar*)pBuf2;
	*/
}


wchar_t *CDeviceManager_SJIStoUTF16( gxChar* pString , size_t *pSize )
{
	//char から wcharに変換する

	size_t u8Len = strlen( pString );
	size_t maxSize = u8Len*4;

	wchar_t *pW1 = new wchar_t[maxSize];

	//UTF8 -> WIDECHAR(UTF16)
	//size_t sz = MultiByteToWideChar( CP_UTF8, 0, (char*)pString, u8Len, pW1, maxSize);

	//SJIS -> WIDECHAR(UTF16)
	size_t sz = MultiByteToWideChar( CP_ACP, 0, (char*)pString, (Uint32)u8Len, pW1, (Uint32)maxSize);

	//ここのSZはWIDECHARの文字数なのでバイト数に変換する
	sz *= 2;

	Uint8 *pW2 = new Uint8[sz + 2];

	gxUtil::MemCpy( pW2 , pW1 , sz );
	pW2[sz+0] = 0x00;
	pW2[sz+1] = 0x00;

	SAFE_DELETES(pW1);

	if( pSize ) *pSize = sz;

	return (wchar_t*)pW2;
}

wchar_t* CDeviceManager_UTF8toUTF16(gxChar* pString, size_t* pSize)
{
	//char から wcharに変換する

	size_t u8Len = strlen(pString);
	size_t maxSize = u8Len * 4;

	wchar_t* pW1 = new wchar_t[maxSize];


	//UTF8 -> WIDECHAR(UTF16)
	size_t sz = MultiByteToWideChar(CP_UTF8, 0, (char*)pString, (Uint32)u8Len, pW1, (Uint32)maxSize);

	//SJIS -> WIDECHAR(UTF16)
	//size_t sz = MultiByteToWideChar(CP_ACP, 0, (char*)pString, u8Len, pW1, maxSize);

	//ここのSZはWIDECHARの文字数なのでバイト数に変換する
	sz *= 2;

	Uint8* pW2 = new Uint8[sz + 2];

	gxUtil::MemCpy(pW2, pW1, sz);
	pW2[sz + 0] = 0x00;
	pW2[sz + 1] = 0x00;

	SAFE_DELETES(pW1);

	if (pSize)*pSize = sz;

	return (wchar_t*)pW2;
}

std::vector<Uint16> CDeviceManager_UTF8toUTF16( std::string str )
{
	//UTF-8->UTF16 へ変換
	size_t size = 0;
	wchar_t* pData = CDeviceManager_UTF8toUTF16 ((char*)str.c_str() , &size );

	std::vector<Uint16> ret;
	if( pData )
	{
		for( Sint32 ii=0;ii<size/2;ii++)
		{
			ret.push_back( (Uint16)pData[ii] );
		}
	}

	SAFE_DELETES(pData);

	return ret;
}
