﻿//---------------------------------------------------------------------
//
// DirectX11
//
//---------------------------------------------------------------------
#include <gxLib.h>

#ifndef _USE_OPENGL

#include <gxLib/gx.h>
#include <gxLib/gxOrderManager.h>
#include <gxLib/gxRender.h>
#include <gxLib/gxTexManager.h>
#include <gxLib/gxDebug.h>
#include <d3dcompiler.h>
#include "CDx11Desktop.h"

#define FORMAT_TEXTURE_2D DXGI_FORMAT_B8G8R8A8_UNORM
//#define FORMAT_TEXTURE_2D DXGI_FORMAT_R8G8B8A8_UNORM
//#define FORMAT_TEXTURE_2D DXGI_FORMAT_B8G8R8X8_UNORM_SRGB

#pragma comment( lib,"d3d11.lib" )
#pragma comment( lib,"d3dcompiler.lib" )

CDirectX11 *CDirectX11::s_pInsatnce = NULL;

//ゲームスクリーン
float GameW = WINDOW_W;
float GameH = WINDOW_H;

Float32 HiGameW = 1920;
Float32 HiGameH = 1024;

const char vtxShaderScript[] = {
	"struct VertexShaderInput\n"\
	"{\n"\
	"	float4 pos      : POSITION;\n"\
	"	float4 color    : COLOR;\n"\
	"	float2 texcoord : TEXCOORD;\n"\
	"	float2 scale    : SCALE;\n"\
	"	float2 offset   : OFFSET;\n"\
	"	float  rotation : ROTATION;\n"\
	"	float2 flip     : FLIP;\n"\
	"	float4 blend    : BLEND;\n"\
	"	float4 option   : OPTION;\n"\
	"};\n"\
	"\n"\
	"struct PixelShaderInput\n"\
	"{\n"\
	"	float4 pos      : SV_POSITION;\n"\
	"	float4 color    : COLOR;\n"\
	"	float2 texcoord : TEXCOORD;\n"\
	"	float4 blend    : BLEND;\n"\
	"	float4 option   : OPTION;\n"\
	"};\n"\
	"struct OptionBuffer\n"\
	"{\n"\
	"	float d3offset;\n"\
	"	float width;\n"\
	"	float height;\n"\
	"	float option4;\n"\
	"};\n"\
	"cbuffer CB2 : register( b0 )\n"\
	"{\n"\
	"	OptionBuffer Option;\n"\
	"};\n"\
	"\n"\
	"PixelShaderInput main(VertexShaderInput input)\n"\
	"{\n"\
	"	PixelShaderInput output;\n"\
	"	float4 pos;\n"\
	"	float2 screen = float2( Option.width , Option.height );\n"\
	"	pos.x = input.pos.x * screen[0] * input.scale.x;\n"\
	"	pos.y = input.pos.y * screen[1] * input.scale.y;\n"\
	"	float2 pos2;\n"\
	"	pos2.x  = (pos.x *  cos( input.rotation ))       - pos.y * (sin( input.rotation )*-1.0);\n"\
	"	pos2.y  = (pos.x * (sin( input.rotation )*-1.0)) + pos.y *  cos( input.rotation );\n"\
	"	pos2.x  = pos2.x * input.flip.x;\n"\
	"	pos2.y  = pos2.y * input.flip.y;\n"\
	"	float4 pos3;\n"\
	"	pos3.x  = pos2.x / screen[0] + input.offset.x;\n"\
	"	pos3.y  = pos2.y / screen[1] + input.offset.y;\n"\
	"	output.pos      = float4( pos3.x , pos3.y , 0.0 , 1.0 );\n"\
	"	output.color    = input.color;\n"\
	"	output.texcoord = input.texcoord;\n"\
	"	output.blend    = input.blend;\n"\
	"	output.option   = input.option;\n"\
	"	\n"\
	"	return output;\n"\
	"}\n"\
};


const char pxlShaderScript[] = {
	"struct PixelShaderInput\n"\
	"{\n"\
	"	float4 pos      : SV_POSITION;\n"\
	"	float4 color    : COLOR;\n"\
	"	float2 texcoord : TEXCOORD;\n"\
	"	float4 blend    : BLEND;\n"\
	"	float4 option   : OPTION;\n"\
	"};\n"\
	"Texture2D txDiffuse : register( t0 );\n"\
	"SamplerState samLinear : register( s0 );\n"\
	"float4 main(PixelShaderInput input) : SV_TARGET\n"\
	"{\n"\
	"	float4 col = txDiffuse.Sample(samLinear, input.texcoord) * input.color;\n"\
	"	float3 rgb = float3( col.r , col.g , col.b );\n"\
	"	rgb = rgb*( 1.0 - input.blend.a ) + float3( input.blend.r , input.blend.g , input.blend.b )*input.blend.a;\n"\
	"	float4 rgba = float4( rgb.r , rgb.g , rgb.b , col.a );\n"\
	"	return rgba;\n"\
	"}\n"\
};

const char pxlShaderFont[] = {
	"struct PixelShaderInput\n"\
	"{\n"\
	"	float4 pos      : SV_POSITION;\n"\
	"	float4 color    : COLOR;\n"\
	"	float2 texcoord : TEXCOORD;\n"\
	"	float4 blend    : BLEND;\n"\
	"	float4 option   : OPTION;\n"\
	"};\n"\
	"Texture2D txDiffuse : register( t0 );\n"\
	"SamplerState samLinear : register( s0 );\n"\
	"float4 main(PixelShaderInput input) : SV_TARGET\n"\
	"{\n"\
	"	float4 col = txDiffuse.Sample(samLinear, input.texcoord);\n"\
    "	float alpha = col.r;\n"\
    "	float mode = input.option[0];\n"\
    "	if(mode == 1.0) alpha = col.g;\n"\
    "	if(mode == 2.0) alpha = col.b;\n"\
	"	if(mode == 3.0) alpha = col.a;\n"\
	"	float4 rgba = float4( 1.0 , 1.0 , 1.0 , alpha ) * input.color;\n"\
	"	return rgba;\n"\
	"}\n"\
};
//	"	rgb = rgb*( 1.0 - input.blend.a ) + float3( input.blend.r , input.blend.g , input.blend.b )*input.blend.a;\n"\


// DeviceResources に対するコンストラクター。
CDirectX11::CDirectX11() :
	m_screenViewport(),
	m_d3dFeatureLevel(D3D_FEATURE_LEVEL_9_1),
	m_outputSize(),
	m_d3dDevice(NULL),
	m_d3dContext(NULL),
	m_swapChain(NULL)
{
	// このフラグは、カラー チャネルの順序が API の既定値とは異なるサーフェスのサポートを追加します。
	// これは、Direct2D との互換性を保持するために必要です。

	m_bInitCompleted = gxFalse;

//	m_bUpConvert = gxFalse;
	m_bUpConvert = gxTrue;

	m_b3DView = gxFalse;

	GameW = WINDOW_W;
	GameH = WINDOW_H;

	m_inputLayout  = NULL;
	m_vertexBuffer = NULL;
	m_indexBuffer  = NULL;
	m_vertexShader = NULL;
	m_pixelShader  = NULL;
	m_pixelShaderFont  = NULL;

	UINT creationFlags = D3D11_CREATE_DEVICE_BGRA_SUPPORT;

/*
#if defined(GX_DEBUG)
	if (SdkLayersAvailable())
	{
		// プロジェクトがデバッグ ビルドに含まれる場合、このフラグを使用して SDK レイヤーによるデバッグを有効にします。
		creationFlags |= D3D11_CREATE_DEVICE_DEBUG;
	}
#endif
*/
	// この配列では、このアプリケーションでサポートされる DirectX ハードウェア機能レベルのセットを定義します。
	// 順序が保存されることに注意してください。
	// アプリケーションの最低限必要な機能レベルをその説明で宣言することを忘れないでください。
	// 特に記載がない限り、すべてのアプリケーションは 9.1 をサポートすることが想定されます。
	D3D_FEATURE_LEVEL featureLevels[] = 
	{
		D3D_FEATURE_LEVEL_11_1,
		D3D_FEATURE_LEVEL_11_0,
		D3D_FEATURE_LEVEL_10_1,
		D3D_FEATURE_LEVEL_10_0,
		D3D_FEATURE_LEVEL_9_3,
		D3D_FEATURE_LEVEL_9_2,
		D3D_FEATURE_LEVEL_9_1
	};

	// Direct3D 11 API デバイス オブジェクトと、対応するコンテキストを作成します。
//	ID3D11Device* device;
//	ID3D11DeviceContext* context;

	HRESULT hr = 0;
	hr = D3D11CreateDevice(
		nullptr,					// 既定のアダプターを使用する nullptr を指定します。
		D3D_DRIVER_TYPE_HARDWARE,	// ハードウェア グラフィックス ドライバーを使用してデバイスを作成します。
		0,							// ドライバーが D3D_DRIVER_TYPE_SOFTWARE でない限り、0 を使用してください。
		creationFlags,				// デバッグ フラグと Direct2D 互換性フラグを設定します。
		featureLevels,				// このアプリがサポートできる機能レベルの一覧を表示します。
		ARRAYSIZE(featureLevels),	// 上記リストのサイズ。
		D3D11_SDK_VERSION,			// Windows ストア アプリでは、これには常に D3D11_SDK_VERSION を設定します。
		&m_d3dDevice,					// 作成された Direct3D デバイスを返します。
		&m_d3dFeatureLevel,			// 作成されたデバイスの機能レベルを返します。
		&m_d3dContext					// デバイスのイミディエイト コンテキストを返します。
		);

	if (FAILED(hr))
	{
		// 初期化が失敗した場合は、WARP デバイスにフォール バックします。
		// WARP の詳細については、次を参照してください:
		// http://go.microsoft.com/fwlink/?LinkId=286690
			D3D11CreateDevice(
				nullptr,
				D3D_DRIVER_TYPE_WARP, // ハードウェア デバイスの代わりに WARP デバイスを作成します。
				0,
				creationFlags,
				featureLevels,
				ARRAYSIZE(featureLevels),
				D3D11_SDK_VERSION,
				&m_d3dDevice,
				&m_d3dFeatureLevel,
				&m_d3dContext
				);
	}

	// Direct3D 11.1 API デバイスへのポインターとイミディエイト コンテキストを保存します。
//毒	device.As(&m_d3dDevice);
//毒	context.As(&m_d3dContext);

}


CDirectX11::~CDirectX11()
{
	for( Sint32 ii=0; ii<enTexturePageMax; ii++ )
	{
		if( m_TextureData[ ii ].texture2D )
		{
			m_TextureData[ ii ].texture2D->Release();
		}

		if( m_TextureData[ ii ].shaderResourceView )
		{
			m_TextureData[ ii ].shaderResourceView->Release();
		}

		if( m_TextureData[ ii ].renderTargetView )
		{
			m_TextureData[ ii ].renderTargetView->Release();
		}
	}

	if( m_d3dContext ) m_d3dContext->ClearState();
	if( m_d3dContext ) m_d3dContext->Flush();

	if( m_inputLayout )  m_inputLayout->Release();
	if( m_vertexBuffer ) m_vertexBuffer->Release();
	if( m_indexBuffer )  m_indexBuffer->Release();
	if( m_vertexShader ) m_vertexShader->Release();
	if( m_pixelShader )  m_pixelShader->Release();
	if( m_pixelShaderFont )  m_pixelShaderFont->Release();
	if( m_constantBuffer )  m_constantBuffer->Release();
}


// このメソッドは、CoreWindow オブジェクトが作成 (または再作成) されるときに呼び出されます。
void CDirectX11::Reset()
{
//	DisplayInformation^ currentDisplayInformation = DisplayInformation::GetForCurrentView();
//
//	m_logicalSize = Windows::Foundation::Size(
//		CWindows::GetInstance()->GetWindow()->Bounds.Width,
//		CWindows::GetInstance()->GetWindow()->Bounds.Height);
//
//	m_nativeOrientation = currentDisplayInformation->NativeOrientation;
//
//	m_currentOrientation = currentDisplayInformation->CurrentOrientation;
//
//	m_dpi = currentDisplayInformation->LogicalDpi;
//
//	// ディスプレイ DPI の変更時に、ウィンドウの論理サイズ (Dip 単位) も変更されるため、更新する必要があります。
//	m_logicalSize = Windows::Foundation::Size(
//		CWindows::GetInstance()->GetWindow()->Bounds.Width,
//		CWindows::GetInstance()->GetWindow()->Bounds.Height );
//
	MakeSwapChane();

//	StXMVECTORF32 rgba = {
//		0.5f,
//		0.5f,
//		1.0f,
//		1.0f,
//	};


/*
	for( Sint32 ii=0; ii<enTexturePageMax; ii++ )
	{
		ID3D11RenderTargetView *const targets[1] = { m_TextureData[ii].renderTargetView };
		m_d3dContext->ClearRenderTargetView( targets[0], &rgba.a );
	}
*/

}


void CDirectX11::MakeSwapChane()
{
	// 前のウィンドウ サイズに固有のコンテキストをクリアします。
	ID3D11RenderTargetView* nullViews[] = {nullptr};
	m_d3dContext->OMSetRenderTargets( ARRAYSIZE(nullViews) , nullViews, nullptr);
	m_TextureData[enBackBuff].renderTargetView  = nullptr;

	m_d3dContext->Flush();

	// 必要なレンダリング ターゲットのサイズをピクセル単位で計算します。
//毒	m_outputSize.Width  = ConvertDipsToPixels(m_logicalSize.Width, m_dpi);
//毒	m_outputSize.Height = ConvertDipsToPixels(m_logicalSize.Height, m_dpi);

	// サイズ 0 の DirectX コンテンツが作成されることを防止します。
//毒	m_outputSize.Width  = max(m_outputSize.Width, 1);
//毒	m_outputSize.Height = max(m_outputSize.Height, 1);

	//m_outputSize.Width  = HiGameW;//WINDOW_W;
	//m_outputSize.Height = HiGameH;//WINDOW_H;

	//--------------------------------------------------
	// スワップ チェーン
	//--------------------------------------------------

	// スワップ チェーンの幅と高さは、ウィンドウの横長方向の幅と高さを
	// ネイティブ方向の幅と高さ。ウィンドウがネイティブではない場合は、
	// サイズを反転させる必要があります

	m_BackBufferSize.Width  = WINDOW_W;
	m_BackBufferSize.Height = WINDOW_H;

	if( m_bUpConvert )
	{
		m_BackBufferSize.Width  = HiGameW;
		m_BackBufferSize.Height = HiGameH;
	}

	// ディスプレイデバイスを取得
	DEVMODE devMode;
	DISPLAY_DEVICE displayDevice;
	displayDevice.cb = sizeof(DISPLAY_DEVICE);
	if (EnumDisplayDevices(nullptr, 0, &displayDevice, 0)) {
		devMode.dmSize = sizeof(DEVMODE);

		if (EnumDisplaySettings(displayDevice.DeviceName, ENUM_CURRENT_SETTINGS, &devMode))
		{
			m_BackBufferSize.Width = devMode.dmPelsWidth;
			m_BackBufferSize.Height = devMode.dmPelsHeight;
		}
	}


	//---------------------------------------------------------


	if (m_swapChain != nullptr)
	{
		m_swapChain->SetFullscreenState( CWindows::GetInstance()->IsFullScreen() , NULL );
		//
		// スワップ チェーンが既に存在する場合は、そのサイズを変更します。
		HRESULT hr = m_swapChain->ResizeBuffers(
			2, // ダブル バッファーされたスワップ チェーンです。
			lround(m_BackBufferSize.Width),
			lround(m_BackBufferSize.Height),
			FORMAT_TEXTURE_2D,
			0
			);

		//IDXGIDevice3* dxgiDevice = NULL;
		//IDXGIAdapter* dxgiAdapter = NULL;
		//IDXGIFactory4* dxgiFactory = NULL;
		//
		//m_d3dDevice->QueryInterface(__uuidof(IDXGIDevice1), (void**)&dxgiDevice);
		//
		//dxgiDevice->GetAdapter(&dxgiAdapter);
		//
		//IDXGIOutput* output = nullptr;
		//m_swapChain->GetContainingOutput(&output);
		//for (int ii = 0; ii < 10; ii++)
		//{
		//	IDXGIOutput* output = nullptr;
		//	hr = dxgiAdapter->EnumOutputs(ii, &output);
		//	if (FAILED(hr)) {
		//		break;
		//	}
		//
		//	m_swapChain->GetContainingOutput(&output);
		//	DXGI_OUTPUT_DESC outputDesc;
		//	output->GetDesc(&outputDesc);
		//
		//	if (outputDesc.DesktopCoordinates.left == 0 && outputDesc.DesktopCoordinates.top == 0)
		//	{
		//		Sint32 w = outputDesc.DesktopCoordinates.right - outputDesc.DesktopCoordinates.left;
		//		Sint32 h = outputDesc.DesktopCoordinates.bottom - outputDesc.DesktopCoordinates.top;
		//		CGameGirl::GetInstance()->SetWindowSize(w, h);
		//		CGameGirl::GetInstance()->AdjustScreenResolution();
		//		break;
		//	}
		//
		//	// リソースの解放
		//	if (output) output->Release();
		//
		//}


		if (hr == DXGI_ERROR_DEVICE_REMOVED || hr == DXGI_ERROR_DEVICE_RESET)
		{
			// 何らかの理由でデバイスを削除した場合、新しいデバイスとスワップ チェーンを作成する必要があります。
			//HandleDeviceLost();

			//すべての設定が完了しました。このメソッドの実行を続行しないでください。HandleDeviceLost はこのメソッドに再入し、
			// 新しいデバイスを正しく設定します。
			return;
		}
	}
	else
	{
		// それ以外の場合は、既存の Direct3D デバイスと同じアダプターを使用して、新規作成します。
		DXGI_SWAP_CHAIN_DESC swapChainDesc;// = { 0 };


		swapChainDesc.BufferDesc.Width  = lround(m_BackBufferSize.Width);		// ウィンドウのサイズと一致させます。
		swapChainDesc.BufferDesc.Height = lround(m_BackBufferSize.Height);
		swapChainDesc.BufferDesc.Format	 = FORMAT_TEXTURE_2D; 						// これは、最も一般的なスワップ チェーンのフォーマットです。
		//swapChainDesc.Stereo			 = false;
		swapChainDesc.SampleDesc.Count	 = 1; 										// マルチサンプリングはサポートされていない
		swapChainDesc.SampleDesc.Quality = 0;
		swapChainDesc.BufferUsage		 = DXGI_USAGE_RENDER_TARGET_OUTPUT | DXGI_USAGE_SHADER_INPUT;
		swapChainDesc.BufferCount		 = 2; // 遅延を最小限に抑えるにはダブル バッファーを使用します。
		swapChainDesc.SwapEffect		 = DXGI_SWAP_EFFECT_DISCARD;

		//default
		swapChainDesc.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;	//これを指定していると勝手にフルスクリーン/Windowを切り替えてしまう
		swapChainDesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
		swapChainDesc.BufferDesc.Scaling   = DXGI_MODE_SCALING_UNSPECIFIED;	//DXGI_SCALING_STRETCH
		//swapChainDesc.AlphaMode = DXGI_ALPHA_MODE_IGNORE;

		swapChainDesc.OutputWindow = CWindows::GetInstance()->GetWindowHandle();// hWnd;
		swapChainDesc.Windowed = TRUE;
		swapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
		swapChainDesc.BufferDesc.RefreshRate.Denominator = 1;

		// このシーケンスは、上の Direct3D デバイスを作成する際に使用された DXGI ファクトリを取得します。
		IDXGIDevice3  *dxgiDevice = NULL;
		IDXGIAdapter  *dxgiAdapter = NULL;
		IDXGIFactory4 *dxgiFactory = NULL;

		m_d3dDevice->QueryInterface(__uuidof(IDXGIDevice1), (void**)&dxgiDevice );

		dxgiDevice->GetAdapter( &dxgiAdapter );

		HRESULT hr2 = dxgiAdapter->GetParent( IID_PPV_ARGS( &dxgiFactory ) );

		if (FAILED(hr2))
		{
			//Windows7ではIID_PPV_ARGSで取得できない
			HRESULT hr3;
			hr3 = dxgiAdapter->GetParent(__uuidof(IDXGIFactory), (void**)&dxgiFactory);
		}

#if 0
		IDXGISwapChain* swapChain;
		dxgiFactory->CreateSwapChainForCoreWindow(
				m_d3dDevice.Get(),
				reinterpret_cast<IUnknown*>( CWindows::GetInstance()->GetWindow() ),
				&swapChainDesc,
				nullptr,
				&swapChain
				);
//		swapChain.As(&m_swapChain);
#else
		HRESULT hr = dxgiFactory->CreateSwapChain(
			dxgiDevice,
			&swapChainDesc,
			&m_swapChain
			);
#endif


	//フルスクリーンの自動切り替えを抑制しておく
	dxgiFactory->MakeWindowAssociation(  CWindows::GetInstance()->GetWindowHandle() , DXGI_MWA_NO_ALT_ENTER);

		// DXGI が 1 度に複数のフレームをキュー処理していないことを確認します。これにより、遅延が減少し、
		// アプリケーションが各 VSync の後でのみレンダリングすることが保証され、消費電力が最小限に抑えられます。
		dxgiDevice->SetMaximumFrameLatency(1);
	}

	// スワップ チェーンの適切な方向を設定し、回転されたスワップ チェーンにレンダリングするための 2D および
	// 3D マトリックス変換を生成します。
	// 2D および 3D 変換の回転角度は異なります。
	// これは座標空間での違いによります。さらに、
	// 丸めエラーを回避するために 3D マトリックスが明示的に指定されます。
	// 0 度 Z 回転


//	static const StXMFLOAT4X4 Rotation0( 
//		1.0f, 0.0f, 0.0f, 0.0f,
//		0.0f, 1.0f, 0.0f, 0.0f,
//		0.0f, 0.0f, 1.0f, 0.0f,
//		0.0f, 0.0f, 0.0f, 1.0f
//		);
//
//
//	m_orientationTransform3D = Rotation0;
//
//	m_swapChain->SetRotation( DXGI_MODE_ROTATION_IDENTITY );

	//------------------------------------------------------
	// スワップ チェーンのバック バッファーのレンダリング ターゲット ビューを作成します。
	//------------------------------------------------------

	ID3D11Texture2D* backBuffer;	//これは書き換わる「m_TextureData[enBackBuff].renderTargetView」でないもので取得すること！！！

	m_swapChain->GetBuffer( 0, IID_PPV_ARGS( &backBuffer) );

	m_d3dDevice->CreateRenderTargetView(
		backBuffer,
		nullptr,
		&m_TextureData[enBackBuff].renderTargetView
		);

	//------------------------------------------------------
	// 3D レンダリング ビューポートをウィンドウ全体をターゲットにするように設定します。
	//------------------------------------------------------

	m_screenViewport = CD3D11_VIEWPORT(
		0.0f,
		0.0f,
		m_BackBufferSize.Width,
		m_BackBufferSize.Height
		);

	m_d3dContext->RSSetViewports(1, &m_screenViewport);

	//壁紙用レンダーテクスチャを作る

	makeTexture( &m_TextureData[enWallPaper]  , 256   , 256   ,32 , NULL );

	//ゲームスクリーン用レンダーテクスチャを作る

	if( m_bUpConvert )
	{
		makeTexture( &m_TextureData[enGameScreen1]  , HiGameW , HiGameH ,32 , NULL );
		makeTexture( &m_TextureData[enGameScreen2]  , HiGameW , HiGameH ,32 , NULL );
		makeTexture( &m_TextureData[enCaptureBuff]  , HiGameW , HiGameH ,32 , NULL );
	}
	else
	{
		makeTexture( &m_TextureData[enGameScreen1] , GameW , GameH ,32 , NULL );
		makeTexture( &m_TextureData[enGameScreen2] , GameW , GameH, 32, NULL);
		makeTexture( &m_TextureData[enCaptureBuff] , GameW , GameH ,32 , NULL );
	}

	//基本テクスチャの生成
	//ノンテクスチャポリゴン用テクスチャという矛盾したテクスチャ
	Uint8 *pBasicTex = new Uint8[32*32*4];

	memset( pBasicTex , 0xFF , 32*32*4 );

	makeTexture( &m_TextureData[enBasicBuff] , 32 , 32 ,32 , pBasicTex );

	SAFE_DELETES( pBasicTex );
}


void CDirectX11::Init()
{
	//------------------------------------------------------
	// 頂点シェーダー ファイルを読み込んだ後、シェーダーと入力レイアウトを作成します。
	//------------------------------------------------------

	HRESULT hr;
	//Uint8 *pData;
	Uint32 uSize = 0;

	{
		ID3DBlob *pVertexShaderBlob;

		//ID3DBlob *pPixelShaderBlob;
		ID3DBlob* errorBlob;
		DWORD dwShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;

#if 1
		hr = D3DCompile(
			vtxShaderScript,
			sizeof(vtxShaderScript),
			NULL,
			NULL,
			NULL,
			"main",
			"vs_4_0",
			dwShaderFlags,
			0,
			&pVertexShaderBlob,
			&errorBlob
			);
		if (hr != S_OK)
		{
			gxLib::DebugLog("ShaderCompileError %s" ,errorBlob->GetBufferPointer() );
		}

		hr = GetD3DDevice()->CreateVertexShader(
			pVertexShaderBlob->GetBufferPointer(),
			pVertexShaderBlob->GetBufferSize(),
			nullptr,
			&m_vertexShader
		);
#else
		wchar_t buf[1024];
		GetCurrentDirectory(1024, buf);
		//fileからシェーダーを取得する場合
		hr = D3DCompileFromFile(
			L"gxLib/hard/Windows/DirectX11/Shader/VertexShader.hlsl",
			NULL,
			NULL,
			"main",
			"vs_4_0",
			dwShaderFlags,
			0,
			&pVertexShaderBlob,
			&errorBlob
			);
#endif

		//------------------------------------------------------
		//頂点情報の組み合わせレイアウトを作成
		//------------------------------------------------------

		static const D3D11_INPUT_ELEMENT_DESC layoutDesc [] =
		{
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32A32_FLOAT , 0, 0,						D3D11_INPUT_PER_VERTEX_DATA, 0 },	//16
			{ "COLOR"	, 0, DXGI_FORMAT_R32G32B32A32_FLOAT	, 0, 16, 					D3D11_INPUT_PER_VERTEX_DATA, 0 },	//16
			{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT	   	, 0, 16+16, 				D3D11_INPUT_PER_VERTEX_DATA, 0 },	//8
			{ "SCALE"   , 0, DXGI_FORMAT_R32G32_FLOAT	   	, 0, 16+16+8,				D3D11_INPUT_PER_VERTEX_DATA, 0 },	//8
			{ "OFFSET"  , 0, DXGI_FORMAT_R32G32_FLOAT	   	, 0, 16+16+8+8,				D3D11_INPUT_PER_VERTEX_DATA, 0 },	//8
			{ "ROTATION", 0, DXGI_FORMAT_R32_FLOAT		   	, 0, 16+16+8+8+8,			D3D11_INPUT_PER_VERTEX_DATA, 0 },	//4
			{ "FLIP"    , 0, DXGI_FORMAT_R32G32_FLOAT	   	, 0, 16+16+8+8+8+4,			D3D11_INPUT_PER_VERTEX_DATA, 0 },	//8
			{ "BLEND"   , 0, DXGI_FORMAT_R32G32B32A32_FLOAT	, 0, 16+16+8+8+8+4+8,		D3D11_INPUT_PER_VERTEX_DATA, 0 },	//16
			{ "OPTION"  , 0, DXGI_FORMAT_R32G32B32A32_FLOAT	, 0, 16+16+8+8+8+4+8+16,	D3D11_INPUT_PER_VERTEX_DATA, 0 },	//16
		};

		hr = GetD3DDevice()->CreateInputLayout(
			layoutDesc,
			ARRAYSIZE(layoutDesc),
			pVertexShaderBlob->GetBufferPointer(),//pData,
			pVertexShaderBlob->GetBufferSize(),//uSize,
			&m_inputLayout
		);
	}

	//------------------------------------------------------
	// ピクセル シェーダー ファイルを読み込んだ後、シェーダーと定数バッファーを作成します。
	//------------------------------------------------------

	{
#if 1
		ID3DBlob *pPixelShaderBlob;
		ID3DBlob* errorBlob;
		DWORD dwShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;

		hr = D3DCompile(
			pxlShaderScript,
			sizeof(pxlShaderScript),
			NULL,
			NULL,
			NULL,
			"main",
			"ps_4_0",
			dwShaderFlags,
			0,
			&pPixelShaderBlob,
			&errorBlob
		);

		if ( hr != S_OK )
		{
			gxLib::DebugLog("ShaderCompileError %s" , errorBlob->GetBufferPointer());
		}
		hr = GetD3DDevice()->CreatePixelShader(
			pPixelShaderBlob->GetBufferPointer(),
			pPixelShaderBlob->GetBufferSize(),
			nullptr,
			&m_pixelShader
		);

		//フォントシェーダー

		hr = D3DCompile(
			pxlShaderFont,
			sizeof(pxlShaderFont),
			NULL,
			NULL,
			NULL,
			"main",
			"ps_4_0",
			dwShaderFlags,
			0,
			&pPixelShaderBlob,
			&errorBlob
		);

		if ( hr != S_OK )
		{
			gxLib::DebugLog("ShaderCompileError %s" , errorBlob->GetBufferPointer());
		}

		hr = GetD3DDevice()->CreatePixelShader(
			pPixelShaderBlob->GetBufferPointer(),
			pPixelShaderBlob->GetBufferSize(),
			nullptr,
			&m_pixelShaderFont
		);


#else
		//fileからシェーダーを取得する場合
		hr = D3DCompileFromFile(
			L"gxLib/hard/Windows/DirectX11/Shader/PixelShader.hlsl",
			NULL,
			NULL,
			"main",
			"ps_4_0",
			dwShaderFlags,
			0,
			&pPixelShaderBlob ,
			&errorBlob
		);
		hr = GetD3DDevice()->CreatePixelShader(
			pPixelShaderBlob->GetBufferPointer(),
			pPixelShaderBlob->GetBufferSize(),
			nullptr,
			&m_pixelShader
		);
#endif
	}


	//------------------------------------------------------
	//頂点バッファーを作る
	//------------------------------------------------------

	//size_t size = enVertexBufferSize;
	void *m_pVertexBuffer = new unsigned char[enVertexBufferSize];
	void *m_pIndexBuffer = new unsigned char[enIndexBufferSize];;

	D3D11_SUBRESOURCE_DATA vertexBufferData = {0};
	vertexBufferData.pSysMem = m_pVertexBuffer;
	vertexBufferData.SysMemPitch = 0;
	vertexBufferData.SysMemSlicePitch = 0;

	CD3D11_BUFFER_DESC vertexBufferDesc( enVertexBufferSize, D3D11_BIND_VERTEX_BUFFER);
	vertexBufferDesc.Usage          = D3D11_USAGE_DYNAMIC;
	vertexBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	GetD3DDevice()->CreateBuffer(
			&vertexBufferDesc,
			&vertexBufferData,
			&m_vertexBuffer		);

	//------------------------------------------------------
	//インデックスバッファーを作る
	//------------------------------------------------------

	D3D11_SUBRESOURCE_DATA indexBufferData = {0};
	indexBufferData.pSysMem = m_pIndexBuffer;
	indexBufferData.SysMemPitch = 0;
	indexBufferData.SysMemSlicePitch = 0;

	CD3D11_BUFFER_DESC indexBufferDesc( enIndexBufferSize, D3D11_BIND_INDEX_BUFFER);

	indexBufferDesc.Usage          = D3D11_USAGE_DYNAMIC;
	indexBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	GetD3DDevice()->CreateBuffer(
		&indexBufferDesc,
		&indexBufferData,
		&m_indexBuffer	);

	//------------------------------------------------------
	//コンスタントバッファーの作成
	//------------------------------------------------------
	CD3D11_BUFFER_DESC constantBufferDesc( sizeof( ConstantBufferForView3D ) , D3D11_BIND_CONSTANT_BUFFER );

	hr = GetD3DDevice()->CreateBuffer(
			&constantBufferDesc,
			nullptr,
			&m_constantBuffer
		);

	m_ConstBuffer3dView.pos[0] = 1.0f;

	m_d3dContext->UpdateSubresource( m_constantBuffer , 0, NULL, &m_ConstBuffer3dView, 0, 0 );

	//------------------------------------------------------
	//定数データの設定
	//------------------------------------------------------

	makeConstantData();

	//------------------------------------------------------
	//サンプリング方法（テクスチャ引用元の設定）
	//------------------------------------------------------
	configTextureSampling();

	//------------------------------------------------------
	// ブレンドステート（ピクセル重ね合わせ処理）
	//------------------------------------------------------

	configBlendState();

	//------------------------------------------------------
	// ラスタライザー（ピクセル処理）
	//------------------------------------------------------

	D3D11_RASTERIZER_DESC rd = {};
	// 背面カリングをしない場合
	rd.CullMode = D3D11_CULL_NONE;	//常にすべての三角形を描画します。
	//D3D11_CULL_NONE;//	常にすべての三角形を描画します。
	//D3D11_CULL_FRONT;//	前向きの三角形を描画しません。
	//D3D11_CULL_BACK;//	後ろ向きの三角形を描画しません。(default)

	//rd.FrontCounterClockwise = FALSE;	// 時計回り(clockwise)の頂点を持つ面を表と見なす (デフォルト)
	rd.FrontCounterClockwise = TRUE;	// 反時計回り(counter clockwise)の頂点を持つ面を表と見なす

	//以下デフォルト設定

	rd.DepthBias = 0;
	rd.SlopeScaledDepthBias = 0.f;
	rd.DepthBiasClamp = 0.f;
	rd.DepthClipEnable = FALSE;// TRUE;
	rd.FillMode = D3D11_FILL_SOLID;	//D3D11_FILL_WIREFRAME 頂点によって形成された三角形を塗りつぶします。隣接する頂点は描画されません。(default)
	rd.MultisampleEnable = FALSE;
	rd.AntialiasedLineEnable = FALSE;
	rd.ScissorEnable = false;

	// ラスタライザーステートを生成して設定
	GetD3DDevice()->CreateRasterizerState( &rd, &m_pRasterizerState);

	m_d3dContext->RSSetState(m_pRasterizerState);

	Reset();

	m_bInitCompleted = gxTrue;

}


void CDirectX11::Update()
{
	makeWallPaper();

	m_b3DView = CGameGirl::GetInstance()->Is3DView();

	//IDXGIOutput* output = nullptr;
	//m_swapChain->GetContainingOutput(&output);
	//DXGI_OUTPUT_DESC outputDesc;
	//output->GetDesc(&outputDesc);
	//Sint32 w = outputDesc.DesktopCoordinates.right - outputDesc.DesktopCoordinates.left;
	//Sint32 h = outputDesc.DesktopCoordinates.bottom - outputDesc.DesktopCoordinates.top;
	//CGameGirl::GetInstance()->SetWindowSize(w, h);
	//// リソースの解放
	//if (output) output->Release();

}


void CDirectX11::Render()
{
	if( !m_bInitCompleted ) return;

	// ビューポートをリセットしてラスタライザに全画面をターゲットとします。
	auto viewport = GetScreenViewport();
	auto context = GetD3DDeviceContext();

	//GLの最終座標
	float glX1, glY1;
	float glX2, glY2;

	Sint32 gamew = WINDOW_W,gameh = WINDOW_H;
	Sint32 winw  = WINDOW_W,winh  = WINDOW_H;

	CGameGirl::GetInstance()->GetGameResolution   ( &gamew , &gameh );
	CGameGirl::GetInstance()->GetWindowsResolution( &winw  , &winh   );

	//RECT desktop;
	//GetWindowRect(GetDesktopWindow(), (LPRECT)&desktop);
	//
	//HiGameW = desktop.right - desktop.left;
	//HiGameH = desktop.bottom - desktop.top;

	glX1 = 0.0f;
	glY1 = 0.0f;

	glX2 = 2.0f * gamew / winw;
	glY2 = 2.0f * gameh / winh;

	glX1 = -glX2 /2.0f;
	glY1 =  glY2 /2.0f;
	glX2 =  glX2 /2.0f;
	glY2 = -glY2 /2.0f;


	{
		Sint32 sCommandMax      = gxRender::GetInstance()->GetCommandNum();
		StCustomVertex *pVertex = gxRender::GetInstance()->GetVertex(0);

		Sint32 indexUnitSize = 0;

		Uint32         *pGxIndex  = gxRender::GetInstance()->GetIndexBuffer( 0 );
		indexUnitSize = 4;

		Uint32 vtx_max = gxRender::GetInstance()->GetVertexNum();
		Uint32 idx_max = gxRender::GetInstance()->GetIndexNum();


		Uint32 vram_draw_max = enVertexBufferSize / sizeof(VertexPositionColorTexCoord);
		if (vtx_max >= vram_draw_max-32)
		{
			vtx_max = vram_draw_max-32;
			gxDebug::GetInstance()->SetError(gxDebug::ErrVertexBufferOver);
		}

		D3D11_MAPPED_SUBRESOURCE msr;

		context->Map(
			m_vertexBuffer,
			0,
			D3D11_MAP_WRITE_DISCARD,//D3D11_MAP_WRITE_NO_OVERWRITE,
			0,
			&msr);

		VertexPositionColorTexCoord* pVtxTex = (VertexPositionColorTexCoord*)msr.pData;

		VertexPositionColorTexCoord *pVtx;

		for( Uint32 ii=0; ii<vtx_max; ii++ )
		{
			//頂点情報を変換する

			pVtx = &pVtxTex[ii];

			pVtx->pos.x = pVertex[ii].x;
			pVtx->pos.y = pVertex[ii].y*1.0f;
			pVtx->pos.z = pVertex[ii].z*0.01f;
			pVtx->pos.w = 0.f;//pVertex[ii].z;

			pVtx->color.x = pVertex[ii].r;
			pVtx->color.y = pVertex[ii].g;
			pVtx->color.z = pVertex[ii].b;
			pVtx->color.w = pVertex[ii].a;

			pVtx->texcoord.x = pVertex[ii].u;
			pVtx->texcoord.y = 1.0f - pVertex[ii].v;

			pVtx->scale.x = pVertex[ii].sx;
			pVtx->scale.y = pVertex[ii].sy;

			pVtx->offset.x = pVertex[ii].cx;
			pVtx->offset.y = pVertex[ii].cy;

			pVtx->rotation = pVertex[ii].rot;

			pVtx->flip.x = pVertex[ii].fx;
			pVtx->flip.y = pVertex[ii].fy;

			pVtx->blend.x = pVertex[ii].r2;
			pVtx->blend.y = pVertex[ii].g2;
			pVtx->blend.z = pVertex[ii].b2;
			pVtx->blend.w = pVertex[ii].a2;

			pVtx->option[0] = pVertex[ii].option[0];
			pVtx->option[1] = pVertex[ii].option[1];
			pVtx->option[2] = pVertex[ii].option[2];
			pVtx->option[3] = pVertex[ii].option[3];

		}

		//頂点バッファ、インデックスバッファは小分けにしてMap,UnMapしてはいけない
		//やるなら位置をずらすこと

		Float32 w = 1.0f;
		Float32 offsetx = 0.0f;
		if( m_b3DView )
		{
			offsetx = 0.5f;
		}

		const VertexPositionColorTexCoord cubeVertices[] = 
		{
			//背景を描く
			{ StXMFLOAT4( -1.f*w,  1.f*w,  0.f,1), StXMFLOAT4( 1.0f, 1.0f, 1.0f , 1.0f ) , StXMFLOAT2( 0.f , 0.f ) ,StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f,StXMFLOAT2(1.0f,1.0f), StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f)},
			{ StXMFLOAT4(  1.f*w, -1.f*w,  0.f,1), StXMFLOAT4( 1.0f, 1.0f, 1.0f , 1.0f ) , StXMFLOAT2( 1.f , 1.f ) ,StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f,StXMFLOAT2(1.0f,1.0f), StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f)},
			{ StXMFLOAT4( -1.f*w, -1.f*w,  0.f,1), StXMFLOAT4( 1.0f, 1.0f, 1.0f , 1.0f ) , StXMFLOAT2( 0.f , 1.f ) ,StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f,StXMFLOAT2(1.0f,1.0f), StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f)},
			{ StXMFLOAT4(  1.f*w,  1.f*w,  0.f,1), StXMFLOAT4( 1.0f, 1.0f, 1.0f , 1.0f ) , StXMFLOAT2( 1.f , 0.f ) ,StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f,StXMFLOAT2(1.0f,1.0f), StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f)},

			//レンダーテクスチャを描く
			//FLIP1用
			{ StXMFLOAT4( glX1 - offsetx,  glY1 ,  0.f,1), StXMFLOAT4(1.0f, 1.0f, 1.0f , 1.0f) , StXMFLOAT2(0.f , 0.f) ,StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f,StXMFLOAT2(1.0f,1.0f), StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f)},
			{ StXMFLOAT4( glX2 - offsetx,  glY2 ,  0.f,1), StXMFLOAT4(1.0f, 1.0f, 1.0f , 1.0f) , StXMFLOAT2(1.f , 1.f) ,StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f,StXMFLOAT2(1.0f,1.0f), StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f)},
			{ StXMFLOAT4( glX1 - offsetx,  glY2 ,  0.f,1), StXMFLOAT4(1.0f, 1.0f, 1.0f , 1.0f) , StXMFLOAT2(0.f , 1.f) ,StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f,StXMFLOAT2(1.0f,1.0f), StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f)},
			{ StXMFLOAT4( glX2 - offsetx,  glY1 ,  0.f,1), StXMFLOAT4(1.0f, 1.0f, 1.0f , 1.0f) , StXMFLOAT2(1.f , 0.f) ,StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f,StXMFLOAT2(1.0f,1.0f), StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f)},

			//レンダーテクスチャを描く
			//FLIP2用
			{ StXMFLOAT4( glX1 + offsetx,  glY1 ,  0.f,1), StXMFLOAT4(1.0f, 1.0f, 1.0f , 1.0f) , StXMFLOAT2(0.f , 0.f) ,StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f,StXMFLOAT2(1.0f,1.0f), StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f)},
			{ StXMFLOAT4( glX2 + offsetx , glY2 ,  0.f,1), StXMFLOAT4(1.0f, 1.0f, 1.0f , 1.0f) , StXMFLOAT2(1.f , 1.f) ,StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f,StXMFLOAT2(1.0f,1.0f), StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f)},
			{ StXMFLOAT4( glX1 + offsetx,  glY2 ,  0.f,1), StXMFLOAT4(1.0f, 1.0f, 1.0f , 1.0f) , StXMFLOAT2(0.f , 1.f) ,StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f,StXMFLOAT2(1.0f,1.0f), StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f)},
			{ StXMFLOAT4( glX2 + offsetx,  glY1 ,  0.f,1), StXMFLOAT4(1.0f, 1.0f, 1.0f , 1.0f) , StXMFLOAT2(1.f , 0.f) ,StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f,StXMFLOAT2(1.0f,1.0f), StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f)},
		};

		gxUtil::MemCpy( &pVtxTex[vtx_max] , (void*)cubeVertices , sizeof(cubeVertices) );

		context->Unmap( m_vertexBuffer, 0 );

		//---------------------------

		context->Map(
			m_indexBuffer,
			0,
			D3D11_MAP_WRITE_DISCARD,//D3D11_MAP_WRITE_NO_OVERWRITE,
			0,
			&msr);

		Uint32* pIndex = (unsigned int*)msr.pData;
		Uint32 cubeIndices[] =
		{
			0+vtx_max, 1+vtx_max, 2+vtx_max,
			0+vtx_max, 3+vtx_max, 1+vtx_max,

			0+vtx_max+4, 1+vtx_max+4, 2+vtx_max+4,
			0+vtx_max+4, 3+vtx_max+4, 1+vtx_max+4,

			0+vtx_max+8, 1+vtx_max+8, 2+vtx_max+8,
			0+vtx_max+8, 3+vtx_max+8, 1+vtx_max+8,
		};

		gxUtil::MemCpy( &pIndex[0], pGxIndex, indexUnitSize*idx_max );
		gxUtil::MemCpy( &pIndex[idx_max], cubeIndices, sizeof(cubeIndices) );

		context->Unmap( m_indexBuffer, 0 );
	}

	//背景色をクリア

	Uint32 _argb = gxRender::GetInstance()->GetBgColor();

	Float32 rgba[] = {
		((_argb >> 16) & 0xff) / 255.0f,
		((_argb >> 8) & 0xff) / 255.0f,
		((_argb >> 0) & 0xff) / 255.0f,
		((_argb >> 24) & 0xff) / 255.0f,
	};

#if 0
	//なんでもいいからシェーダーリソースを与えておく必要がある
	//context->PSSetShaderResources(0, 1, m_TextureData[enWallPaper].shaderResourceView.GetAddressOf() );
#else
	//リソースを外す
	const ID3D10ShaderResourceView *srv[1] = { 0 };
	context->PSSetShaderResources(0, 1, (ID3D11ShaderResourceView *const *)srv);
//	context->VSSetShader( NULL, NULL, 0 );
//	context->PSSetShader( NULL, NULL, 0 );
#endif

	Float32 fLR = 3.0f;

	viewport.TopLeftX = 0.0f;
	viewport.TopLeftY = 0.0f;
	viewport.Width    = GameW;
	viewport.Height   = GameH;

	if( m_bUpConvert )
	{
		viewport.Width    = HiGameW;
		viewport.Height   = HiGameH;
	}

	context->RSSetViewports(1, &viewport);

	//glDisable ( GL_SCISSOR_TEST );

	{
		D3D11_RASTERIZER_DESC rd = {};
		// 背面カリングをしない場合
		rd.CullMode = D3D11_CULL_NONE;	//常にすべての三角形を描画します。
		rd.FrontCounterClockwise = TRUE;	// 反時計回り(counter clockwise)の頂点を持つ面を表と見なす
		rd.DepthBias = 0;
		rd.SlopeScaledDepthBias = 0.f;
		rd.DepthBiasClamp = 0.f;
		rd.DepthClipEnable = FALSE;// TRUE;
		rd.FillMode = D3D11_FILL_SOLID;	//D3D11_FILL_WIREFRAME 頂点によって形成された三角形を塗りつぶします。隣接する頂点は描画されません。(default)
		rd.MultisampleEnable = FALSE;
		rd.AntialiasedLineEnable = FALSE;
		rd.ScissorEnable = false;

		// ラスタライザーステートを生成して設定
		GetD3DDevice()->CreateRasterizerState( &rd, &m_pRasterizerState);

		m_d3dContext->RSSetState(m_pRasterizerState);
	}



	if( !m_b3DView )
	{
		if( rgba[0] + rgba[1] + rgba[2] )
		{
			//レンダーターゲットをゲーム画面用テクスチャにする
			context->OMSetRenderTargets( 1, &m_TextureData[enGameScreen1].renderTargetView, nullptr );

			// バック バッファーと深度ステンシル ビューをクリアします。
			context->ClearRenderTargetView( m_TextureData[enGameScreen1].renderTargetView, rgba );
		}

		m_ConstBuffer3dView.pos[0] = 0.0f;
		m_ConstBuffer3dView.pos[1] = WINDOW_W;
		m_ConstBuffer3dView.pos[2] = WINDOW_H;
		m_ConstBuffer3dView.pos[3] = 0.0f;
		m_d3dContext->UpdateSubresource( m_constantBuffer , 0, NULL, &m_ConstBuffer3dView, 0, 0 );

		Sint32 start = gxRender::GetInstance()->GetBGOrderMax();
		Sint32 max   = gxRender::GetInstance()->GetGameOrderMax();
		renderGameObject( start , max );
	}
	else
	{

//		if( rgba[0] + rgba[1] + rgba[2] )
//		{
//			//レンダーターゲットをゲーム画面用テクスチャにする
//			context->OMSetRenderTargets( 1, &m_TextureData[enGameScreen1].renderTargetView, nullptr );
//
//			// バック バッファーと深度ステンシル ビューをクリアします。
//			context->ClearRenderTargetView( m_TextureData[enGameScreen1].renderTargetView, rgba );
//		}
//
//		m_ConstBuffer3dView.pos[0] = -1.0f;
//		m_ConstBuffer3dView.pos[1] = WINDOW_W;
//		m_ConstBuffer3dView.pos[2] = WINDOW_H;
//		m_ConstBuffer3dView.pos[3] = 0.0f;
//		m_d3dContext->UpdateSubresource( m_constantBuffer , 0, NULL, &m_ConstBuffer3dView, 0, 0 );
//		renderGameObject();
//
//		if( rgba[0] + rgba[1] + rgba[2] )
//		{
//			//レンダーターゲットをゲーム画面用テクスチャにする
//			context->OMSetRenderTargets( 1, &m_TextureData[enGameScreen2].renderTargetView, nullptr );
//
//			// バック バッファーと深度ステンシル ビューをクリアします。
//			context->ClearRenderTargetView( m_TextureData[enGameScreen2].renderTargetView, rgba );
//		}
//
//		m_ConstBuffer3dView.pos[0] = +1.0f;
//		m_ConstBuffer3dView.pos[1] = WINDOW_W;
//		m_ConstBuffer3dView.pos[2] = WINDOW_H;
//		m_ConstBuffer3dView.pos[3] = 0.0f;
//		m_d3dContext->UpdateSubresource( m_constantBuffer , 0, NULL, &m_ConstBuffer3dView, 0, 0 );
//		renderGameObject();
	}


	//-----------------------------------
	//system
	//-----------------------------------
	//glDisable ( GL_SCISSOR_TEST );

	{
		D3D11_RASTERIZER_DESC rd = {};
		// 背面カリングをしない場合
		rd.CullMode = D3D11_CULL_NONE;	//常にすべての三角形を描画します。
		rd.FrontCounterClockwise = TRUE;	// 反時計回り(counter clockwise)の頂点を持つ面を表と見なす
		rd.DepthBias = 0;
		rd.SlopeScaledDepthBias = 0.f;
		rd.DepthBiasClamp = 0.f;
		rd.DepthClipEnable = FALSE;// TRUE;
		rd.FillMode = D3D11_FILL_SOLID;	//D3D11_FILL_WIREFRAME 頂点によって形成された三角形を塗りつぶします。隣接する頂点は描画されません。(default)
		rd.MultisampleEnable = FALSE;
		rd.AntialiasedLineEnable = FALSE;
		rd.ScissorEnable = false;

		// ラスタライザーステートを生成して設定
		GetD3DDevice()->CreateRasterizerState( &rd, &m_pRasterizerState);

		m_d3dContext->RSSetState(m_pRasterizerState);
	}

	viewport.TopLeftX = 0.0f;
	viewport.TopLeftY = 0.0f;
	viewport.Width    = m_BackBufferSize.Width;
	viewport.Height   = m_BackBufferSize.Height;

	context->RSSetViewports(1, &viewport);

	renderSystem();

}


void CDirectX11::renderGameObject( Sint32 start , Sint32 max )
{
	//テクスチャにゲームオブジェクトを描く
	auto context = GetD3DDeviceContext();

	UINT stride = sizeof(VertexPositionColorTexCoord);
	UINT offset = 0;

	context->IASetVertexBuffers(	0,	1,	&m_vertexBuffer,	&stride,	&offset	);

 	context->IASetIndexBuffer( m_indexBuffer,		DXGI_FORMAT_R32_UINT,	0	);

	context->IASetInputLayout( m_inputLayout );

	// テクスチャが使用するサンプラー設定
	context->PSSetSamplers( 0, 1, &m_SamplerState[0] );

	// ピクセル シェーダーをアタッチします。
	context->PSSetShader( m_pixelShader,	nullptr,	0	);

	//定数バッファをアタッチします
	ID3D11Buffer* cb[1]={ m_constantBuffer };
	context->VSSetConstantBuffers( 0, 1, cb );

	// 頂点シェーダーをアタッチします。
	context->VSSetShader( m_vertexShader,	nullptr,	0	);

	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	//ゲーム画面の描画

	static const float blendFactor[] = { 0,0,0,0 };
	context->OMSetBlendState( m_pBlendState[ enBlendTypeDefault ], blendFactor, 0xffffffff);

	CCommandList* pCommand = NULL;
	Sint32 cmdMax = max;//gxRender::GetInstance()->GetCommandNum();

	Uint32 idx_max = gxRender::GetInstance()->GetIndexNum();

	for(Sint32 n=start; n<cmdMax; n++)
	{
		pCommand = gxRender::GetInstance()->GetCommandList(n);

		Sint32 v_start = pCommand->arg[0];
		Sint32 v_num   = pCommand->arg[1];
		Sint32 i_start = pCommand->arg[2];
		Sint32 i_num   = pCommand->arg[3];

		switch( pCommand->eCommand ){
		case eCmdBindNoneTexture:
			//テクスチャをはずす
		    //m_pDirect3DDevice->SetTexture( 0, NULL );
			context->PSSetShaderResources(0, 1, &m_TextureData[ enBasicBuff ].shaderResourceView );
			
			break;

		case eCmdBindAlbedoTexture:
			//テクスチャをつける
			//m_pDirect3DDevice->SetTexture( 0, m_D3dTexture[ pCommand->arg[0] ] );
			//m_pDirect3DDevice->SetTextureStageState(0,D3DTSS_COLOROP   , D3DTOP_MODULATE);
			//m_pDirect3DDevice->SetTextureStageState(0,D3DTSS_ALPHAOP   , D3DTOP_MODULATE);
			//m_pDirect3DDevice->SetTextureStageState(0,D3DTSS_ALPHAARG1 , D3DTA_TEXTURE);
			//m_pDirect3DDevice->SetTextureStageState(0,D3DTSS_ALPHAARG2 , D3DTA_DIFFUSE);
			//m_pDirect3DDevice->SetTextureStageState(0,D3DTSS_COLORARG1 , D3DTA_TEXTURE);
			//m_pDirect3DDevice->SetTextureStageState(0,D3DTSS_COLORARG2 , D3DTA_DIFFUSE);
			context->PSSetShaderResources(0, 1, &m_TextureData[ pCommand->arg[0] ].shaderResourceView );
			break;

		case eCmdChgAttributeAlphaNml:
			//ブレンディング(標準)
			//m_pDirect3DDevice->SetRenderState( D3DRS_SRCBLEND,  D3DBLEND_SRCALPHA );
			//m_pDirect3DDevice->SetRenderState( D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA );
			//m_pDirect3DDevice->SetRenderState( D3DRS_COLORVERTEX, TRUE );
			//m_pDirect3DDevice->SetRenderState( D3DRS_ALPHABLENDENABLE,TRUE);
			context->OMSetBlendState( m_pBlendState[ enBlendTypeDefault ], blendFactor, 0xffffffff);
			break;

		case eCmdChgAttributeAlphaAdd:
			//ブレンディング(加算)
			//m_pDirect3DDevice->SetRenderState( D3DRS_SRCBLEND,  D3DBLEND_SRCALPHA );
			//m_pDirect3DDevice->SetRenderState( D3DRS_DESTBLEND, D3DBLEND_ONE );
			//m_pDirect3DDevice->SetRenderState( D3DRS_COLORVERTEX, TRUE );
			//m_pDirect3DDevice->SetRenderState( D3DRS_ALPHABLENDENABLE,TRUE);
			context->OMSetBlendState( m_pBlendState[ enBlendTypeAdd ], blendFactor, 0xffffffff);
			break;

		case eCmdChgAttributeAlphaSub:
			//ブレンディング(減算)
			//m_pDirect3DDevice->SetRenderState( D3DRS_SRCBLEND,  D3DBLEND_ZERO );
			//m_pDirect3DDevice->SetRenderState( D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
			////m_pDirect3DDevice->SetRenderState( D3DRS_DESTBLEND, D3DBLEND_INVSRCCOLOR);
			//m_pDirect3DDevice->SetRenderState( D3DRS_COLORVERTEX, TRUE );
			//m_pDirect3DDevice->SetRenderState( D3DRS_ALPHABLENDENABLE,TRUE);
			context->OMSetBlendState( m_pBlendState[ enBlendTypeSub ], blendFactor, 0xffffffff);
			break;

		case eCmdChgAttributeAlphaCrs:
			//ブレンディング(乗算)
			//m_pDirect3DDevice->SetRenderState( D3DRS_SRCBLEND,  D3DBLEND_ZERO );
			//m_pDirect3DDevice->SetRenderState( D3DRS_DESTBLEND, D3DBLEND_SRCCOLOR);
			//m_pDirect3DDevice->SetRenderState( D3DRS_COLORVERTEX, TRUE );
			//m_pDirect3DDevice->SetRenderState( D3DRS_ALPHABLENDENABLE,TRUE);
			context->OMSetBlendState( m_pBlendState[ enBlendTypeCross ], blendFactor, 0xffffffff);
			break;

		case eCmdChgAttributeAlphaRvs:
			//ブレンディング(反転)
			//m_pDirect3DDevice->SetRenderState( D3DRS_SRCBLEND,  D3DBLEND_INVDESTCOLOR );
			//m_pDirect3DDevice->SetRenderState( D3DRS_DESTBLEND, D3DBLEND_ZERO);
			//m_pDirect3DDevice->SetRenderState( D3DRS_COLORVERTEX, TRUE );
			//m_pDirect3DDevice->SetRenderState( D3DRS_ALPHABLENDENABLE,TRUE);
			context->OMSetBlendState( m_pBlendState[ enBlendTypeReverse ], blendFactor, 0xffffffff);
			break;

		case eCmdChgAttributeAlphaXor:
			//ブレンディング(XOR)
			//m_pDirect3DDevice->SetRenderState( D3DRS_SRCBLEND,  D3DBLEND_INVDESTCOLOR );
			//m_pDirect3DDevice->SetRenderState( D3DRS_DESTBLEND, D3DBLEND_INVSRCCOLOR );
			//m_pDirect3DDevice->SetRenderState( D3DRS_COLORVERTEX, TRUE );
			//m_pDirect3DDevice->SetRenderState( D3DRS_ALPHABLENDENABLE,TRUE);
			context->OMSetBlendState( m_pBlendState[ enBlendTypeXor ], blendFactor, 0xffffffff);
			break;

		case eCmdChgAttributeAlphaScr:
			//ブレンディング(スクリーン乗算)
			//m_pDirect3DDevice->SetRenderState( D3DRS_SRCBLEND,  D3DBLEND_INVDESTCOLOR );
			//m_pDirect3DDevice->SetRenderState( D3DRS_DESTBLEND, D3DBLEND_ONE );
			//m_pDirect3DDevice->SetRenderState( D3DRS_COLORVERTEX, TRUE );
			//m_pDirect3DDevice->SetRenderState( D3DRS_ALPHABLENDENABLE,TRUE);
			context->OMSetBlendState( m_pBlendState[ enBlendTypeScreen ], blendFactor, 0xffffffff);
			break;

		case eCmdRenderPoint:
			//点の描画
			context->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_POINTLIST);
			//m_pDirect3DDevice->DrawPrimitive( D3DPT_POINTLIST , v_start , v_num );
			context->DrawIndexed( i_num, i_start, 0	);
			break;

		case eCmdRenderLineStrip:
			//連続線の描画
			context->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_LINESTRIP);
			//m_pDirect3DDevice->DrawIndexedPrimitive( D3DPT_LINESTRIP, 0,v_start,v_num,i_start , 4 );
			context->DrawIndexed( i_num, i_start, 0	);
			break;

		case eCmdRenderLineNormal:
			//線の描画
			context->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_LINELIST);
			//m_pDirect3DDevice->DrawIndexedPrimitive( D3DPT_LINELIST,0,v_start,v_num,i_start,v_num/2 );
			context->DrawIndexed( i_num, i_start, 0	);
			break;

		case eCmdRenderTriangle:		case eCmdRenderSquare:
			//三角形の描画
			//m_pDirect3DDevice->DrawIndexedPrimitive( D3DPT_TRIANGLELIST, 0,v_start,v_num,i_start,i_num/3 );
			context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
			context->DrawIndexed( i_num, i_start, 0	);
			break;

		case eCmdRenderFont:
			//フォントの描画
			break;

		case eCmdScissor:
			if( pCommand->arg[2] == 0 || pCommand->arg[3] == 0 )
			{
				//glDisable ( GL_SCISSOR_TEST );
				D3D11_RASTERIZER_DESC rd = {};
				// 背面カリングをしない場合
				rd.CullMode = D3D11_CULL_NONE;	//常にすべての三角形を描画します。
				rd.FrontCounterClockwise = TRUE;	// 反時計回り(counter clockwise)の頂点を持つ面を表と見なす
				rd.DepthBias = 0;
				rd.SlopeScaledDepthBias = 0.f;
				rd.DepthBiasClamp = 0.f;
				rd.DepthClipEnable = FALSE;// TRUE;
				rd.FillMode = D3D11_FILL_SOLID;	//D3D11_FILL_WIREFRAME 頂点によって形成された三角形を塗りつぶします。隣接する頂点は描画されません。(default)
				rd.MultisampleEnable = FALSE;
				rd.AntialiasedLineEnable = FALSE;
				rd.ScissorEnable = false;

				// ラスタライザーステートを生成して設定
				GetD3DDevice()->CreateRasterizerState( &rd, &m_pRasterizerState);

				m_d3dContext->RSSetState(m_pRasterizerState);
			}
			else
			{
				Sint32 x,y,w,h;
				x = pCommand->arg[0];
				y = pCommand->arg[1];
				w = pCommand->arg[2];
				h = pCommand->arg[3];
				//glEnable ( GL_SCISSOR_TEST );
				//glScissor( x, y, w ,h );
				//g_pD3D11User->m_D3DDeviceContext->RSSetScissorRects( _countof( rect ), rect );
				D3D11_RECT rect;//[1]={x,y,w,h};
				rect.top = y;
				rect.left = x;
				rect.right = x + w;
				rect.bottom = y + h;
				context->RSSetScissorRects( 1, &rect );


				D3D11_RASTERIZER_DESC rd = {};
				// 背面カリングをしない場合
				rd.CullMode = D3D11_CULL_NONE;	//常にすべての三角形を描画します。
				rd.FrontCounterClockwise = TRUE;	// 反時計回り(counter clockwise)の頂点を持つ面を表と見なす
				rd.DepthBias = 0;
				rd.SlopeScaledDepthBias = 0.f;
				rd.DepthBiasClamp = 0.f;
				rd.DepthClipEnable = FALSE;// TRUE;
				rd.FillMode = D3D11_FILL_SOLID;	//D3D11_FILL_WIREFRAME 頂点によって形成された三角形を塗りつぶします。隣接する頂点は描画されません。(default)
				rd.MultisampleEnable = FALSE;
				rd.AntialiasedLineEnable = FALSE;
				rd.ScissorEnable = true;

				// ラスタライザーステートを生成して設定
				GetD3DDevice()->CreateRasterizerState( &rd, &m_pRasterizerState);

				m_d3dContext->RSSetState(m_pRasterizerState);
   			}
			break;
		case eCmdChangeShader:
				switch( pCommand->arg[0] ){
				case gxShaderReset:
				case gxShaderDefault:
					changeShader(enShaderDefault );
					break;
				case gxShaderBloom:
					changeShader(enShaderBloom );
					break;
				case gxShaderBlur:
					changeShader(enShaderBlur );
					break;
				case gxShaderRaster:
					changeShader(enShaderRaster );
					break;
				case gxShaderNormal:
					changeShader(enShaderNormal );
					break;
				case gxShaderPallet:
					changeShader(enShaderNormal );
					break;
                case gxShaderFont:
				// テクスチャが使用するサンプラー設定(Bilinear)
				GetD3DDeviceContext()->PSSetSamplers( 0, 1, &m_SamplerState[1] );
				//context->PSSetSamplers( 0, 1, &m_SamplerState[0] );
                    changeShader(enShaderFont );
                    break;
				default:
					//changeShader(enShaderDev );
					changeShader(enShaderDefault );
					break;
				}
			break;

		case eCmdChangeRenderTarget:
			break;
	
		case eCmdProcessingBloom:
		case eCmdProcessingBlur:
		case eCmdCaptureScreen:
		case eCmdDevelop:
			break;

		default:
			break;
		}
	}
}


void CDirectX11::renderSystem()
{
	auto context = GetD3DDeviceContext();

	Uint32 idx_max = gxRender::GetInstance()->GetIndexNum();

	//----------------------------------------------------------------------

	// 頂点シェーダーをアタッチします。
	context->VSSetShader( m_vertexShader,	nullptr,	0	);

	// ピクセル シェーダーをアタッチします。
	context->PSSetShader( m_pixelShader,	nullptr,	0	);

	//----------------------------------------------------------------------

	StXMVECTORF32 rgba = { 0,0,0.5f,1 };	//rgba
	context->OMSetRenderTargets(1, &m_TextureData[enBackBuff].renderTargetView, nullptr );

	// バック バッファーと深度ステンシル ビューをクリアします。
	context->ClearRenderTargetView( m_TextureData[enBackBuff].renderTargetView, &rgba.a );

	// テクスチャが使用するサンプラー設定(Bilinear)
	switch( CWindows::GetInstance()->GetRenderFilter() ){
	case CWindows::enSamplingNearest:
		context->PSSetSamplers( 0, 1, &m_SamplerState[0] );
		break;
	case CWindows::enSamplingBiLenear:
		context->PSSetSamplers( 0, 1, &m_SamplerState[1] );
		break;
	default:
		break;
	}

	// ピクセル シェーダーをアタッチします。
	context->PSSetShader( m_pixelShader,	nullptr,	0	);

	// 頂点シェーダーをアタッチします。
	context->VSSetShader( m_vertexShader,	nullptr,	0	);

	context->IASetInputLayout( m_inputLayout );

	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	//１つめの矩形

	static const float blendFactor[] = { 0,0,0,0 };
	context->OMSetBlendState(m_pBlendState[ enBlendTypeDefault ], 0, 0xffffffff);

	//--------------------------------------------------
	//プライオリティが０以前の描画は全画面で行う
	//--------------------------------------------------

	Sint32 start = 0;
	Sint32 max   = gxRender::GetInstance()->GetBGOrderMax();

	if( max )
	{
		renderGameObject( start , max );
	}
	else
	{
		//壁紙画像をアタッチ
		context->PSSetShaderResources(0, 1, &m_TextureData[enWallPaper].shaderResourceView );
		context->DrawIndexed( 6, idx_max,	0	);
	}

	//--------------------------------------------------
	//ゲーム画面をフリップ
	//--------------------------------------------------

	context->OMSetBlendState(m_pBlendState[ enBlendTypeDefault ], blendFactor, 0xffffffff);

	if( !m_b3DView )
	{
		//ピクセルシェーダーにリソース（テクスチャ）をアタッチする
		context->PSSetShaderResources(0, 1, &m_TextureData[enGameScreen1].shaderResourceView );

		//ゲーム画面を描画
		context->DrawIndexed( 6, idx_max+6,	0	);
	}
	else
	{
		//ピクセルシェーダーにリソース（テクスチャ）をアタッチする
		context->PSSetShaderResources(0, 1, &m_TextureData[enGameScreen1].shaderResourceView );

		//ゲーム画面を描画
		context->DrawIndexed( 6, idx_max+6,	0	);

		//ピクセルシェーダーにリソース（テクスチャ）をアタッチする
		context->PSSetShaderResources(0, 1, &m_TextureData[enGameScreen2].shaderResourceView );

		//ゲーム画面を描画
		context->DrawIndexed( 6, idx_max+12, 0	);
	}

	//--------------------------------------------------
	//プライオリティ以上の描画は全画面で行う
	//--------------------------------------------------

	start = gxRender::GetInstance()->GetGameOrderMax();
	max   = gxRender::GetInstance()->GetCommandNum();
	renderGameObject( start , max );

}


void CDirectX11::Present() 
{
	//画面のフリップ

	if ( !m_bInitCompleted ) return;

	DXGI_PRESENT_PARAMETERS parameters = { 0 };

	HRESULT hr = m_swapChain->Present(0, 0);//, &parameters);
}

void CDirectX11::ReadTexture( int texPage  )
{
	//外部からファイルを読み込む

	Uint32 w, h;
	
	w = gxTexManager::enMasterWidth;
	h = gxTexManager::enMasterHeight;

	Uint32 *pData = (Uint32*)malloc(w*h*4);
	Uint32 *pData2 = (Uint32*)gxTexManager::GetInstance()->GetAtlasTexture(texPage)->GetTexelImage();

	for( Uint32 y=0; y<h; y++ )
	{
		for( Uint32 x=0; x<w; x++ )
		{
			pData[y*w+x] = pData2[(h-1-y)*w+x];
		}
	}

	makeTexture( &m_TextureData[ texPage ] , w , h ,32 , (Uint8*)pData );

	free( pData );

}


gxBool CDirectX11::makeTexture( TextureData *pTexture , int w , int h , int bitDepth , Uint8 *pData , Uint32 uSize )
{
	//----------------------------------------------------------------
	//書き込み可能なテクスチャの設定
	//----------------------------------------------------------------
	if (pTexture)
	{
		if (pTexture->texture2D)
		{
			pTexture->texture2D->Release();
			pTexture->texture2D = NULL;
		}
		if (pTexture->shaderResourceView)
		{
			pTexture->shaderResourceView->Release();
		}

		if (pTexture->renderTargetView)
		{
			pTexture->renderTargetView->Release();
		}
	}


	D3D11_SUBRESOURCE_DATA subresourceData;

	subresourceData.pSysMem     = pData;
	subresourceData.SysMemPitch = w*(bitDepth/8);
	subresourceData.SysMemSlicePitch = 0;

	// 2次元テクスチャの設定（これとステンシルのサイズは同じにしないとダメ！！）
	D3D11_TEXTURE2D_DESC texDesc;

	memset( &texDesc, 0, sizeof( texDesc ) );

	texDesc.Usage              = D3D11_USAGE_DEFAULT;
	texDesc.Format             = FORMAT_TEXTURE_2D;
	texDesc.BindFlags          = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
	texDesc.Width              = w;
	texDesc.Height             = h;
	texDesc.CPUAccessFlags     = 0;
	texDesc.MipLevels          = 1;
	texDesc.ArraySize          = 1;
	texDesc.SampleDesc.Count   = 1;
	texDesc.SampleDesc.Quality = 0;

	// 2次元テクスチャの生成

	HRESULT hr;

	if( pData )
	{
		hr = GetD3DDevice()->CreateTexture2D( &texDesc, &subresourceData, &pTexture->texture2D );
	}
	else
	{
		hr = GetD3DDevice()->CreateTexture2D( &texDesc, NULL, &pTexture->texture2D );
	}

	if ( FAILED( hr ) )
	{
		return gxFalse;
	}

	//----------------------------------------------
	// 新しいシェーダリソースビューの作成
	//----------------------------------------------

	// 2D テクスチャにアクセスするシェーダリソースビューの設定
	// これを作成しておかないとテクスチャリソースとして使用できない
	D3D11_SHADER_RESOURCE_VIEW_DESC shaderResourceViewDesc;

	shaderResourceViewDesc.Format                    = FORMAT_TEXTURE_2D;
	shaderResourceViewDesc.ViewDimension             = D3D10_SRV_DIMENSION_TEXTURE2D;
	shaderResourceViewDesc.Texture2D.MipLevels       = 1;
	shaderResourceViewDesc.Texture2D.MostDetailedMip = 0;

	// シェーダリソースビューの作成

	GetD3DDevice()->CreateShaderResourceView	(
		pTexture->texture2D,		// アクセスするテクスチャ リソース
		&shaderResourceViewDesc,		// シェーダ リソース ビューの設定
		&pTexture->shaderResourceView	// 受け取る変数
	);

	//-----------------------------------------------------------
	// 新しいレンダーターゲットビューの設定
	//-----------------------------------------------------------
	D3D11_RENDER_TARGET_VIEW_DESC rtvDesc;
	memset( &rtvDesc, 0, sizeof( rtvDesc ) );
	rtvDesc.Format             = FORMAT_TEXTURE_2D;
	rtvDesc.ViewDimension      = D3D11_RTV_DIMENSION_TEXTURE2D;


	// レンダーターゲットビューの生成
	hr = m_d3dDevice->CreateRenderTargetView(
		pTexture->texture2D,
		&rtvDesc,
		&pTexture->renderTargetView );
	
	if ( FAILED( hr ) )
	{
		return gxFalse;
	}

	return gxTrue;

}


void CDirectX11::makeWallPaper()
{
	//ノイズテクスチャの作成

	/*
	Sint32 wh = 256;
	static Uint32 *p1 = NULL;
	Uint32 cnt = 0;

	if( p1 == NULL )
	{
		p1 = (Uint32*)malloc( wh*wh*4 );
	}

	for (int ii = 0; ii<wh; ii++)
	{
		for (int jj = 0; jj < wh; jj++)
		{
			p1[cnt] = 0xFF000000|((gxLib::Rand()%256)<<16)|((gxLib::Rand()%256)<<8)|((gxLib::Rand()%256)<<0);
			cnt ++;
		}
	}
	makeTexture( &m_TextureData[enWallPaper] , wh , wh , 32 , (Uint8*)p1 , wh*wh*4 );
	*/

	//free( p1 );
}

void CDirectX11::makeConstantData()
{
	//定数データの作成

	return;

//	Windows::Foundation::Size outputSize = GetOutputSize();
//	float aspectRatio = outputSize.Width / outputSize.Height;
//	float fovAngleY = 90.0f * XM_PI / 180.0f;
//
//	// これは、アプリケーションが縦向きビューまたはスナップ ビュー内にあるときに行うことのできる
//	// 変更の簡単な例です。
//	//if (aspectRatio < 1.0f)
//	//{
//	//	fovAngleY *= 2.0f;
//	//}
//
//	// OrientationTransform3D マトリックスは、シーンの方向を表示方向と
//	// 正しく一致させるため、ここで事後乗算されます。
//	// この事後乗算ステップは、スワップ チェーンのターゲット ビットマップに対して行われるすべての
//	// 描画呼び出しで実行する必要があります。他のターゲットに対する呼び出しでは、
//	// 適用する必要はありません。
//
//	// このサンプルでは、行優先のマトリックスを使用した右辺座標系を使用しています。
//	//視野に基づいて、右手座標系のパースペクティブ射影行列を作成します。
//
//	XMMATRIX perspectiveMatrix = XMMatrixPerspectiveFovRH(
//		fovAngleY,
//		1.f,//aspectRatio,
//		0.01f,
//		100.0f
//		);
//
//	StXMFLOAT4X4 orientation = GetOrientationTransform3D();
//
//	//4x4の集まりを行列にする
//	XMMATRIX orientationMatrix = XMLoadFloat4x4(&orientation);
//
//#if 0
//	//MATRIXから4x4の配列にする（最終的にこのプロジェクションに入った行列こそが視錘台のマトリックス）
//	XMStoreFloat4x4( &m_constantBufferData.projection,	XMMatrixTranspose(perspectiveMatrix * orientationMatrix)	);
//	XMStoreFloat4x4( &m_constantBuffer2Data.projection,	XMMatrixTranspose(perspectiveMatrix * orientationMatrix)	);
//
//	// 視点は (0,0.7,1.5) の位置にあり、y 軸に沿って上方向のポイント (0,-0.1,0) を見ています。
//	static const XMVECTORF32 eye = { 0.0f, 0.0f, 1.f, 0.0f };
//	static const XMVECTORF32 at = { 0.0f, 0.0f, 0.0f, 0.0f };
//	static const XMVECTORF32 up = { 0.0f, 1.0f, 0.0f, 0.0f };
//
//	XMStoreFloat4x4(&m_constantBufferData.view, XMMatrixTranspose(XMMatrixLookAtRH(eye, at, up)));
//	XMStoreFloat4x4(&m_constantBuffer2Data.view, XMMatrixTranspose(XMMatrixLookAtRH(eye, at, up)));
//
//	XMStoreFloat4x4( &m_constantBufferData.model  ,  XMMatrixTranspose( XMMatrixRotationY( 0 ) ) );
//	XMStoreFloat4x4( &m_constantBuffer2Data.model ,  XMMatrixTranspose( XMMatrixRotationY( 0 ) ) );
//#endif
}


void CDirectX11::configTextureSampling()
{
	//------------------------------------------------------
	//テクスチャのサンプリング方法の設定
	//------------------------------------------------------

	D3D11_SAMPLER_DESC samplerDesc;

	//ポイントサンプリング

	ZeroMemory(&samplerDesc, sizeof(samplerDesc));
//		samplerDesc.Filter   = D3D11_FILTER_MIN_MAG_MIP_LINEAR;	//縮小、拡大、およびミップレベルのサンプリングに線形補間を使用します。
	samplerDesc.Filter         = D3D11_FILTER_MIN_MAG_MIP_POINT;	//縮小、拡大、およびミップレベルのサンプリングでポイント サンプリングを使用します。
	samplerDesc.AddressU       = D3D11_TEXTURE_ADDRESS_CLAMP;
	samplerDesc.AddressV       = D3D11_TEXTURE_ADDRESS_CLAMP;
	samplerDesc.AddressW       = D3D11_TEXTURE_ADDRESS_CLAMP;
	samplerDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;	//D3D11_COMPARISON_ALWAYS
	samplerDesc.MinLOD         = 0;
	samplerDesc.MaxLOD         = D3D11_FLOAT32_MAX;
	samplerDesc.MaxAnisotropy  = 0;
	samplerDesc.MipLODBias     = 0.0f;

	samplerDesc.BorderColor[0] = 0.0f;
	samplerDesc.BorderColor[1] = 0.0f;
	samplerDesc.BorderColor[2] = 0.0f;
	samplerDesc.BorderColor[3] = 0.0f;

	GetD3DDevice()->CreateSamplerState( &samplerDesc, &m_SamplerState[0]  );

	//Bilinear

	ZeroMemory(&samplerDesc, sizeof(samplerDesc));
	//samplerDesc.Filter   = D3D11_FILTER_MIN_MAG_MIP_LINEAR;	//縮小、拡大、およびミップレベルのサンプリングに線形補間を使用します。
	samplerDesc.Filter   = D3D11_FILTER_ANISOTROPIC;	//最も高価、表示品質は一番高い


	samplerDesc.AddressU       = D3D11_TEXTURE_ADDRESS_CLAMP;
	samplerDesc.AddressV       = D3D11_TEXTURE_ADDRESS_CLAMP;
	samplerDesc.AddressW       = D3D11_TEXTURE_ADDRESS_CLAMP;
	samplerDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;	//D3D11_COMPARISON_ALWAYS
	samplerDesc.MinLOD         = 0;
	samplerDesc.MaxLOD         = D3D11_FLOAT32_MAX;
	samplerDesc.MaxAnisotropy  = 0;
	samplerDesc.MipLODBias     = 0.0f;

	samplerDesc.BorderColor[0] = 0.0f;
	samplerDesc.BorderColor[1] = 0.0f;
	samplerDesc.BorderColor[2] = 0.0f;
	samplerDesc.BorderColor[3] = 0.0f;

	GetD3DDevice()->CreateSamplerState( &samplerDesc, &m_SamplerState[1]  );

}


void CDirectX11::configBlendState()
{
	//------------------------------------------------------
	// ブレンドステート（透過テクスチャ用）作成
	//------------------------------------------------------

	CD3D11_DEFAULT default_state;
	CD3D11_BLEND_DESC blendDesc(default_state);

	blendDesc.AlphaToCoverageEnable  = FALSE;		//交差したポリゴンでのアルファブレンディング処理

	blendDesc.IndependentBlendEnable = FALSE;

	for(int i = 0; i < 1; ++i)
	{
		//blendDesc.RenderTarget[i].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
		blendDesc.RenderTarget[i].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_RED | D3D11_COLOR_WRITE_ENABLE_BLUE | D3D11_COLOR_WRITE_ENABLE_GREEN;
		blendDesc.RenderTarget[i].BlendEnable = TRUE;

		{
			//通常半透明

			//カラーのブレンディング方法

			blendDesc.RenderTarget[i].SrcBlend       = D3D11_BLEND_SRC_ALPHA;		//アルファ次第
			blendDesc.RenderTarget[i].DestBlend      = D3D11_BLEND_INV_SRC_ALPHA;	//D3D11_BLEND_INV_SRC_ALPHA;	//リバースアルファ次第
			blendDesc.RenderTarget[i].BlendOp        = D3D11_BLEND_OP_ADD;

			//アルファのブレンディング方法

			blendDesc.RenderTarget[i].SrcBlendAlpha  = D3D11_BLEND_ONE;		//アルファの扱い(SRC)
			blendDesc.RenderTarget[i].DestBlendAlpha = D3D11_BLEND_ZERO;		//アルファの扱い(DST)
			blendDesc.RenderTarget[i].BlendOpAlpha   = D3D11_BLEND_OP_ADD;

			GetD3DDevice()->CreateBlendState(&blendDesc, &m_pBlendState[ enBlendTypeDefault ] );
		}

		{
			//加算半透明

			blendDesc.RenderTarget[i].SrcBlend       = D3D11_BLEND_SRC_ALPHA;
			blendDesc.RenderTarget[i].DestBlend      = D3D11_BLEND_ONE;
			blendDesc.RenderTarget[i].BlendOp        = D3D11_BLEND_OP_ADD;

			blendDesc.RenderTarget[i].SrcBlendAlpha  = D3D11_BLEND_ONE;		//アルファの扱い(SRC)
			blendDesc.RenderTarget[i].DestBlendAlpha = D3D11_BLEND_ONE;		//アルファの扱い(DST)
			blendDesc.RenderTarget[i].BlendOpAlpha   = D3D11_BLEND_OP_ADD;

			GetD3DDevice()->CreateBlendState(&blendDesc, &m_pBlendState[ enBlendTypeAdd ] );
		}

		{
			//減算半透明

			blendDesc.RenderTarget[i].SrcBlend = D3D11_BLEND_SRC_ALPHA;
			blendDesc.RenderTarget[i].DestBlend = D3D11_BLEND_ONE;
			blendDesc.RenderTarget[i].BlendOp = D3D11_BLEND_OP_REV_SUBTRACT;

			blendDesc.RenderTarget[i].SrcBlendAlpha = D3D11_BLEND_ONE;		//アルファの扱い(SRC)
			blendDesc.RenderTarget[i].DestBlendAlpha = D3D11_BLEND_ZERO;		//アルファの扱い(DST)
			blendDesc.RenderTarget[i].BlendOpAlpha = D3D11_BLEND_OP_ADD;

			GetD3DDevice()->CreateBlendState(&blendDesc, &m_pBlendState[enBlendTypeSub]);
		}
		{
			//乗算半透明

			blendDesc.RenderTarget[i].SrcBlend = D3D11_BLEND_ZERO;
			blendDesc.RenderTarget[i].DestBlend = D3D11_BLEND_SRC_COLOR;
			blendDesc.RenderTarget[i].BlendOp = D3D11_BLEND_OP_ADD;

			blendDesc.RenderTarget[i].SrcBlendAlpha = D3D11_BLEND_ONE;		//アルファの扱い(SRC)
			blendDesc.RenderTarget[i].DestBlendAlpha = D3D11_BLEND_ZERO;		//アルファの扱い(DST)
			blendDesc.RenderTarget[i].BlendOpAlpha = D3D11_BLEND_OP_ADD;

			GetD3DDevice()->CreateBlendState(&blendDesc, &m_pBlendState[enBlendTypeCross]);
		}
		{
			//ブレンディングなし

			blendDesc.RenderTarget[i].SrcBlend = D3D11_BLEND_ONE;
			blendDesc.RenderTarget[i].DestBlend = D3D11_BLEND_ZERO;
			blendDesc.RenderTarget[i].BlendOp = D3D11_BLEND_OP_ADD;

			blendDesc.RenderTarget[i].SrcBlendAlpha = D3D11_BLEND_ONE;		//アルファの扱い(SRC)
			blendDesc.RenderTarget[i].DestBlendAlpha = D3D11_BLEND_ZERO;		//アルファの扱い(DST)
			blendDesc.RenderTarget[i].BlendOpAlpha = D3D11_BLEND_OP_ADD;

			GetD3DDevice()->CreateBlendState(&blendDesc, &m_pBlendState[enBlendTypeReverse]);
		}
	}


}

void CDirectX11::changeShader(ETypeShader sheaderIndex , Float32 *pFloatValue )
{
	auto context = GetD3DDeviceContext();

	switch(sheaderIndex ){
	case enShaderDefault:
	case enShaderBloom:
	case enShaderBlur:
	case enShaderRaster:
	case enShaderNormal:
		context->PSSetShader(m_pixelShader, nullptr, 0);
		break;
	case enShaderFont:
		context->PSSetShader(m_pixelShaderFont, nullptr, 0);
		break;
	case enShaderDev:
		context->PSSetShader( m_pixelShader,	nullptr,	0	);
        break;
	default:
		context->PSSetShader( m_pixelShader,	nullptr,	0	);
		break;
	}
}

#endif
