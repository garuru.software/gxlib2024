﻿#ifndef _XIINPUT_H_
#define _XIINPUT_H_
//------------------------------------------------------
//
// コントローラー制御( XInput使用)
//
//------------------------------------------------------
#include <XInput.h>


class CXInput
{
public:
	enum {
		enControllerMax = 16,
	};

	CXInput();
	~CXInput();

	void Init();
	bool Action(int id);

	Uint32 GetDeviceNum()
	{
		return m_uGamingDeviceNum;
	}

	SINGLETON_DECLARE( CXInput );

private:

	struct ControlerState
	{
	    XINPUT_STATE state;
	    bool bConnected;
	};

	bool InitXInput();
	void UpdateGamingDevices(int n);			//ハードウェア情報を更新

	void rumble();

	ControlerState GamePad360[enControllerMax];

	//ハードウェアパッド情報
	CGameingDevice 	*m_pGamingDevice[enControllerMax];

	//検出されたパッドの数
	Uint32 m_uGamingDeviceNum;

	gxBool m_bUseController;
	//gxBool m_bEnable[enControllerMax][GDBTN_MAX];
};


#endif
