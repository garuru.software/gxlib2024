﻿//-----------------------------------------------
//
// BlueToothクライアント
//
//-----------------------------------------------

#include <stdio.h>
#include <iostream>
#include <WinSock2.h>
#include <ws2bth.h>
#include <gxLib.h>

#pragma comment(lib, "Ws2_32")

void BT_Server_Thread();
bool recvBTData();
bool sendBTData();

SOCKET			bt_socket;		
char			recByte[FILENAMEBUF_LENGTH];	// 受信データ

void BlueToothThreadMake()
{
	// BT_サーバースレッド設定

	HANDLE  hThread1 = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)BT_Server_Thread, NULL, 0, NULL);
}

void BT_Server_Thread()
{
	CBlueToothManager::GetInstance()->SetEnable(gxFalse);

	WSAData			wsaData;		// Windowsソケット情報
	SOCKET			bt_lis_sock;	// ソケット
	SOCKADDR_BTH	lis_sa_bt;		// BlueToothアドレス格納
	CSADDR_INFO		info;			// ソケット情報

	SOCKADDR_BTH	sa_bt;
	int				size;

	// BlueTooth関連情報の初期化
	memset( &wsaData,	  NULL, sizeof(wsaData) );
	memset( &bt_lis_sock, NULL, sizeof(bt_lis_sock) );
	memset( &lis_sa_bt,	  NULL, sizeof(lis_sa_bt) );
	memset( &info,		  NULL, sizeof(info) );

	memset( &bt_socket,	  NULL, sizeof(bt_socket) );
	memset( &sa_bt,		  NULL, sizeof(sa_bt) );

	WSAStartup(MAKEWORD(2, 2), &wsaData);
	
	// ソケットの生成
	bt_lis_sock = socket(AF_BTH, SOCK_STREAM, BTHPROTO_RFCOMM);

    if (bt_lis_sock == INVALID_SOCKET)
	{
		GX_DEBUGLOG("[BT]ソケット生成失敗");
        return;
    }
	else
	{
		GX_DEBUGLOG("[BT]ソケット生成");
	}

	// バインドの作成
    lis_sa_bt.addressFamily  = AF_BTH;
    lis_sa_bt.port           = BT_PORT_ANY;

    if ( bind(bt_lis_sock, (SOCKADDR *)&lis_sa_bt, sizeof(lis_sa_bt)) == SOCKET_ERROR )
	{
		GX_DEBUGLOG("[BT]バインド失敗");
        return;
    }
	else
	{
		GX_DEBUGLOG("[BT]バインド成功");

		size = sizeof(lis_sa_bt);
		if( getsockname(bt_lis_sock, (SOCKADDR*)&lis_sa_bt, &size) == SOCKET_ERROR )
		{
			GX_DEBUGLOG("[BT]getsockname失敗");
			return;
		}

		gxChar* lis_Add = new gxChar[FILENAMEBUF_LENGTH];
		sprintf(lis_Add, TEXT("binding %04x%08x"), GET_NAP(lis_sa_bt.btAddr), GET_SAP(lis_sa_bt.btAddr));
		GX_DEBUGLOG(lis_Add);
		delete[] (lis_Add);
	}

    info.LocalAddr.lpSockaddr      = (LPSOCKADDR)&lis_sa_bt;
    info.LocalAddr.iSockaddrLength = sizeof(lis_sa_bt);
    info.iSocketType               = SOCK_STREAM;
    info.iProtocol                 = BTHPROTO_RFCOMM;

    WSAQUERYSET set = { 0 };
    set.dwSize = sizeof(WSAQUERYSET);                              // Must be set to sizeof(WSAQUERYSET)
    set.dwOutputFlags = 0;                                         // Not used
    set.lpszServiceInstanceName = "BT_NINJYA";                    // Recommended.
    set.lpServiceClassId    = (LPGUID)&SerialPortServiceClass_UUID;// Requred.
    set.lpVersion           = NULL;                                // Not used.
    set.lpszComment         = NULL;                                // Optional.
    set.dwNameSpace         = NS_BTH;                              // Must be NS_BTH.
    set.lpNSProviderId      = NULL;                                // Not required.
    set.lpszContext         = NULL;                                // Not used.
    set.dwNumberOfProtocols = 0;                                   // Not used.
    set.lpafpProtocols      = NULL;                                // Not used.
    set.lpszQueryString     = NULL;                                // not used.
    set.dwNumberOfCsAddrs   = 1;                                   // Must be 1.
    set.lpcsaBuffer         = &info;                               // Pointer to a CSADDR_INFO.
    set.lpBlob = NULL;                                             // Optional.

	GX_DEBUGLOG("[BT] 1/5 WSASetService");

    if (WSASetService(&set, RNRSERVICE_REGISTER, 0) != 0)
    {
		printf("起動失敗");
        return;
    }
	else
	{
		GX_DEBUGLOG("[BT]WSASetService開始");
	}

	GX_DEBUGLOG("[BT] 2/5 listen");

    int lis = listen(bt_lis_sock, 0);

    if (lis == SOCKET_ERROR)
    {
		GX_DEBUGLOG("[BT]listen失敗");
        return;
    }
	else
	{
		GX_DEBUGLOG("[BT]listen成功");
	}

WAIT:
	// Accept待ち
	GX_DEBUGLOG("[BT] 3/5 accept");


	int ilen;
	ilen = sizeof(sa_bt);
	bt_socket = accept(bt_lis_sock, (SOCKADDR *)&sa_bt, &ilen);

	if (bt_socket == INVALID_SOCKET )
	{
		GX_DEBUGLOG("[BT]accept失敗");
        return;
	}
	else
	{
		GX_DEBUGLOG("[BT]accept成功");

		// 接続アドレス表示(デバッグ用)
		/*
		LPWSTR connAdd = new WCHAR[FILENAMEBUF_LENGTH];
		wsprintf(connAdd, TEXT("Connecting %04x%08x"), GET_NAP(lis_sa_bt.btAddr), GET_SAP(lis_sa_bt.btAddr));
		GX_DEBUGLOG(connAdd);
		delete(connAdd);
		*/
	}

	GX_DEBUGLOG("[BT] 4/5 ioctlsocket");

	//非同期設定
	u_long val = 1;
	ioctlsocket( bt_socket, FIONBIO, &val );

	GX_DEBUGLOG("[BT] 5/5 all successful");

	CBlueToothManager::GetInstance()->SetEnable( gxTrue );

	while(CBlueToothManager::GetInstance()->IsEnable()  )
	{
		if( !sendBTData() ) break;
		if( !recvBTData() ) break;

		gxLib::Sleep( 1000/120 );
	}

	CBlueToothManager::GetInstance()->SetEnable( gxFalse );

	if (CBlueToothManager::GetInstance()->IsBlueToothExist())
	{
		goto WAIT;
	}

	shutdown(bt_socket, SD_BOTH);

    closesocket(bt_lis_sock);
    WSACleanup();
}

bool recvBTData()
{

	while( true )
	{
		int uSize = 0;

		uSize = recv( bt_socket, recByte, 256, 0 );

		if( uSize == 0 )
		{
			return false;
		}

		if( uSize == SOCKET_ERROR )
		{
			return true;
		}

		unsigned char *pRecData = (unsigned char*)&recByte[0];

		if (pRecData[0] != 'G' || pRecData[1] != 'X')
		{
			//header check
			return true;
		}

		int sz = pRecData[2];

		if ( pRecData[4+sz+0] != 'B' || pRecData[4 + sz + 1] != 'T')
		{
			//footer check
			return true;
		}

		//header / footer チェック成功

		int n = CBlueToothManager::GetInstance()->StoreRecvCnt;

		n = n%CBlueToothManager::enBlueToothStoreMax;

		Uint8 *pData = CBlueToothManager::GetInstance()->pStoreRecvData[n];

		SAFE_DELETES( pData );

		pData = CBlueToothManager::GetInstance()->pStoreRecvData[n] = new Uint8[ sz+1 ];
		gxUtil::MemCpy( pData, &pRecData[4], sz);
		pData[ sz ] = 0;

		CBlueToothManager::GetInstance()->StoreRecvDataSize[n] = sz;
		CBlueToothManager::GetInstance()->StoreRecvCnt++;
	}

	return true;
}

bool sendBTData()
{
	Sint32 sendMax = CBlueToothManager::GetInstance()->StoreSendCnt;
	Sint32 sendMin = CBlueToothManager::GetInstance()->StoreWriteCnt;

	for( int ii= sendMin; ii<sendMax; ii++ )
	{
		Sint32 n = ii%CBlueToothManager::enBlueToothStoreMax;
		Uint8 *pData = CBlueToothManager::GetInstance()->pStoreSendData[n];
		Uint32 uSize = CBlueToothManager::GetInstance()->StoreSendDataSize[n];

		recByte[0] = 'G';
		recByte[1] = 'X';
		recByte[2] = uSize;
		recByte[3 + uSize + 0 ] = 'B';
		recByte[3 + uSize + 1 ] = 'T';

		gxUtil::MemCpy( &recByte[3] , pData , uSize );

		//int result = 
		send(bt_socket,(const char*) recByte , uSize + 5, 0);

		CBlueToothManager::GetInstance()->StoreWriteCnt++;
	}

	return true;
}

