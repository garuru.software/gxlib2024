﻿// --------------------------------------------------------------------
//
// つじつまあわせ用共通ヘッダ
//
// --------------------------------------------------------------------
#define PLATFORM_WINDESKTOP
#define TARGET_OS_WINDOWS

//#define STEAM_BUILD	//Steam用ビルドかどうかフラグ
//#define STEAM_TRIAL	//Steam体験版用定義
#if defined(STEAM_BUILD)
// SteamAPI用のライブラリのリンク
#pragma comment(lib,"steam_api64.lib")
#pragma comment(lib,"sdkencryptedappticket64.lib")
#endif // defined(STEAM_BUILD)

//#define _USE_OPENGL
#define _USE_OPENAL
#define USE_WEBVIEW

//---------------------------------------
//以下自動設定
//---------------------------------------

#ifdef WIN32
	#define GX_BUILD_OPTIONx86
#else
	#define GX_BUILD_OPTIONx64
#endif


#ifdef _USE_OPENAL
	#define CAudio COpenAL
#else
	#define CAudio CXAudio
#endif


//#define GX_STRING_ENCODE_SJIS

#define WIN32_LEAN_AND_MEAN             // Windows ヘッダーから使用されていない部分を除外します。

// Windows ヘッダー ファイル:
#include <windows.h>
#include <windowsx.h>

//C Header
#include <assert.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include <locale.h>

// C++ ランタイム ヘッダー ファイル
#include <atomic>
#include <string>
#include <fstream>
#include <iostream>
#include <filesystem>

#include <map>
#include <vector>
#include <functional>

#include <cstdlib>
#include <chrono>
#include <algorithm>

#ifdef _USE_OPENGL
	#include <lib/glew-2.1.0/include/GL/glew.h>
	#define CGraphics COpenGLWin
#else
	#define CGraphics CDirectX11
	#include <dxgi1_4.h>
	#include <d3d11_3.h>
	#include <DirectXMath.h>
#endif

#pragma warning(disable : 4996)
#pragma warning(disable : 4819)
#pragma warning(disable : 4244)

//----------------------------------------------------
//プラットフォーム専用
//----------------------------------------------------

class CWindows
{
	//Windowsデバイス間での制御用クラス

public:

	enum eSamplingFilter {
		enSamplingBiLenear,
		enSamplingNearest,
	};

	CWindows()
	{
		m_bFullScreen  = gxFalse;
		m_bSoundEnable = true;

		m_SamplingFilter = enSamplingBiLenear;

		m_bVirtualPad = gxFalse;
	}

	~CWindows()
	{

	}

	void SetFullScreen(gxBool bFullScreen);

	gxBool IsFullScreen()
	{
		return m_bFullScreen;
	}

	void SetSoundEnable( gxBool bSoundEnable );
	void   SetRenderMode( Sint32 mode);
	Sint32 GetRenderMode()
	{
		return m_ScreenMode;
	}

	void SetControllMode(gxBool bVPadOn);

	gxBool IsSoundEnable()
	{
		return m_bSoundEnable;
	}


	eSamplingFilter GetRenderFilter()
	{
		return m_SamplingFilter;
	}

	void  SetRenderFilter( eSamplingFilter Filter );

	HWND GetWindowHandle()
	{
		return m_hWindow;
	}


	void ExecuteApp(char *Appname);

	void MenuUpdate();

	static std::vector<std::string> GetDirList( std::string rootDir );


	//クリップボード関連
	std::string GetClipBoard( Uint32 type );
	gxBool  SetClipBoard( gxChar *pText );

	HINSTANCE m_hInstance;
	HWND      m_hWindow;
	WPARAM    m_wParam;
	HACCEL    m_hAccel;
	HDC		  m_WinDC;
	Uint32	  m_AppStyle;
	RECT	  m_WinRect;

    //HACCEL hAccelTable;
	//LONGLONG Vsyncrate;

	gxBool m_bFullScreen;
	gxBool m_bSoundEnable;
	gxBool m_bVirtualPad;

	std::string m_WebViewURL;
	gxBool m_bWebBrowser = gxFalse;
	Sint32 m_ChangeWindowSize = 0;

private:

	SINGLETON_DECLARE( CWindows );

	//サンプリングモード
	eSamplingFilter m_SamplingFilter;

	std::vector<std::string> m_DirectoryList;
	std::map<std::string,gxBool> m_TempDirectoryList;
	Sint32 m_ScreenMode = 0;
};


