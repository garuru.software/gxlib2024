﻿//------------------------------------------------------
//
// コントローラー制御( Direct Input version8使用)
//
//------------------------------------------------------

#define DIRECTINPUT_VERSION 0x0800
#include <dinput.h>


class CDirectInput
{
public:

	enum {
		enControllerMax = 32,
	};

	CDirectInput();
	~CDirectInput();

	void Init();
	bool Action(int id);

/*
	void AddDeviceNum()
	{
		m_uGamingDeviceNum ++;
	}
*/

	void AddDevice( gxChar *pDeviceName );

	Uint32 GetDeviceNum()
	{
		return m_uGamingDeviceNum;
	}

	CGameingDevice* GetGamingDevice( int n)
	{
		return m_pGamingDevice[n];
	}

	SINGLETON_DECLARE( CDirectInput );

private:

	bool InitDirectInput();
	void CheckForceFeedBackEffect(int device_id);
	void UpdateGamingDevices(int n);			//ハードウェア情報を更新

	void rumble();

	//検出されたパッドの数
	Uint32 m_uGamingDeviceNum;

	//ハードウェアパッド情報
	CGameingDevice 	*m_pGamingDevice[enControllerMax];

	//フォースフィードバック対応エフェクト
	LPDIRECTINPUTEFFECT   g_pEffect[enControllerMax];

	gxBool m_bUseController;

};


