﻿//----------------------------------------------------
//ハードウェア的なパッド情報
//----------------------------------------------------
typedef struct PadStat_t {
	Float32 x,y,z;
}PadStat_t;

enum GAMINGDEVICE_BTNID{
	//ゲーミングデバイスの割り当て定義
	GDBTN_NONE,
	GDBTN_BUTTON00,
	GDBTN_BUTTON01,
	GDBTN_BUTTON02,
	GDBTN_BUTTON03,
	GDBTN_BUTTON04,
	GDBTN_BUTTON05,
	GDBTN_BUTTON06,
	GDBTN_BUTTON07,
	GDBTN_BUTTON08,
	GDBTN_BUTTON09,
	GDBTN_BUTTON10,
	GDBTN_BUTTON11,
	GDBTN_BUTTON12,
	GDBTN_BUTTON13,
	GDBTN_BUTTON14,
	GDBTN_BUTTON15,
	GDBTN_BUTTON16,
	GDBTN_BUTTON17,
	GDBTN_BUTTON18,
	GDBTN_BUTTON19,
	GDBTN_BUTTON20,
	GDBTN_BUTTON21,
	GDBTN_BUTTON22,
	GDBTN_BUTTON23,
	GDBTN_BUTTON24,
	GDBTN_BUTTON25,
	GDBTN_BUTTON26,
	GDBTN_BUTTON27,
	GDBTN_BUTTON28,
	GDBTN_BUTTON29,
	GDBTN_BUTTON30,
	GDBTN_BUTTON31,

	GDBTN_AXIS_XA,
	GDBTN_AXIS_XS,

	GDBTN_AXIS_YA,
	GDBTN_AXIS_YS,

	GDBTN_AXIS_ZA,
	GDBTN_AXIS_ZS,

	GDBTN_ROT_XA,
	GDBTN_ROT_XS,

	GDBTN_ROT_YA,
	GDBTN_ROT_YS,

	GDBTN_ROT_ZA,
	GDBTN_ROT_ZS,

	GDBTN_POV0_U,
	GDBTN_POV0_R,
	GDBTN_POV0_D,
	GDBTN_POV0_L,

	GDBTN_POV1_U,
	GDBTN_POV1_R,
	GDBTN_POV1_D,
	GDBTN_POV1_L,

	GDBTN_POV2_U,
	GDBTN_POV2_R,
	GDBTN_POV2_D,
	GDBTN_POV2_L,

	GDBTN_POV3_U,
	GDBTN_POV3_R,
	GDBTN_POV3_D,
	GDBTN_POV3_L,

	GDBTN_SLIDER0_U,
	GDBTN_SLIDER0_D,

	GDBTN_SLIDER1_U,
	GDBTN_SLIDER1_D,

	DUMMY1=127,
	GDBTN_MAX,

	GDBTN_BUTTON_A  = GDBTN_BUTTON00,
	GDBTN_BUTTON_B  = GDBTN_BUTTON01,
	GDBTN_BUTTON_X  = GDBTN_BUTTON02,
	GDBTN_BUTTON_Y  = GDBTN_BUTTON03,
	GDBTN_BUTTON_L1 = GDBTN_BUTTON04,
	GDBTN_BUTTON_R1 = GDBTN_BUTTON05,
	GDBTN_BUTTON_L2 = GDBTN_BUTTON06,
	GDBTN_BUTTON_R2 = GDBTN_BUTTON07,
	GDBTN_BUTTON_SL = GDBTN_BUTTON08,
	GDBTN_BUTTON_ST = GDBTN_BUTTON09,
	GDBTN_BUTTON_L3 = GDBTN_BUTTON10,
	GDBTN_BUTTON_R3 = GDBTN_BUTTON11,


};

class CGameingDevice
{
public:
	enum {
		JOYMAX = (16),
	};

	CGameingDevice();
	~CGameingDevice();

	void setButton( GAMINGDEVICE_BTNID id , gxBool bOn = gxTrue )
	{
		_button[id] = bOn;
	}

//	Sint32 		_id;
	gxChar 		_hardwarename[FILENAMEBUF_LENGTH];	//機器の名前
	Uint8 		_DeviceType;		//機器のタイプ
	PadStat_t 	_caribrate;
	PadStat_t 	_caribrateR;
	PadStat_t	_Axis;				//アナログスティック値
	PadStat_t	_Rotation;			//アナログスティック値
	Sint32 		_Pov0;				//アナログスティック値
	Sint32 		_Pov1;				//アナログスティック値
	Sint32 		_Pov2;				//アナログスティック値
	Sint32 		_Pov3;				//アナログスティック値
	Float32		_Slider0;			//アナログスティック値
	Float32		_Slider1;			//アナログスティック値

	//------------------------------------------------------
	//サポート状況
	//------------------------------------------------------
	Sint32 _num_pov;					//ハットスイッチ数
	Sint32 _num_slider;					//スライダーコントロール数
	gxBool _bPov[4];					//ハットスイッチ
	gxBool _bAxisX,_bAxisY,_bAxisZ;		//サポートされているAXIS
	gxBool _bAxisRX,_bAxisRY,_bAxisRZ;	//サポートされているAXIS
	gxBool _bSlider[2];					//サポートされているSLIDER
	gxBool _bRumble;					//振動可能か？
	Sint32 _ForceFeefBackAxis;			//フォースフィードバックの軸数

	//-------------------------------------------------------
	//各軸の使用の可否
	//-------------------------------------------------------
	gxBool _bUseAxisX;
	gxBool _bUseAxisY;
	gxBool _bUseAxisZ;
	gxBool _bUseRotationX;
	gxBool _bUseRotationY;
	gxBool _bUseRotationZ;
	gxBool _bUsePOV0;
	gxBool _bUsePOV1;
	gxBool _bUsePOV2;
	gxBool _bUsePOV3;
	gxBool _bUseSlider0;
	gxBool _bUseSlider1;
	gxBool _bUseForceFeedBack;

	gxBool _bCarribrateCompleted;
	gxBool _bDeviceDisable;			//使用しないデバイスとする
	gxBool _bEnableConfig = gxFalse;
	Sint32 _PlayerID;				//割り当てられたプレイヤーID
	gxBool _bXInput = gxFalse;

	gxBool m_bUsedDevice = gxFalse;
	gxBool      _bRumbling = gxFalse;

	//-------------------------------------------------------
	//コントローラーのアサイン状況
	//-------------------------------------------------------
	Uint8  _Assign[BTN_MAX]={};

	//アナログの軸をコンフィグできるようにする変換テーブル
	Uint32 _cnvAnalog[6]={
		0,1,2,3,4,5
		/*
		gxPadManager::ANALOG_LY,
		gxPadManager::ANALOG_LX,
		gxPadManager::ANALOG_RY,
		gxPadManager::ANALOG_RX,
		gxPadManager::ANALOG_LT,
		gxPadManager::ANALOG_RT,
		*/
	};

	gxBool IsPress( GAMINGDEVICE_BTNID id );
	gxBool IsPressButton ( gxBool& btn);
	gxBool IsPressAxis   ( Float32& axis,Sint32 dir);
	gxBool IsPressPov    ( Sint32 &pov ,Sint32 urdl);
	gxBool IsPressSlider ( Float32 &sldr );

private:

	gxBool 		_button[128];		//ボタン入力（USBパッドのボタン１２８個分）
};


class CGamePad
{
public:
	enum {
		enDeviceMax = 32,
	};

	CGamePad();
	~CGamePad();

	void Init();
	void Action();

	CGameingDevice* GetDevice()
	{
		if( m_DeviceCnt >= enDeviceMax )
		{
			return NULL;
		}

		m_DeviceCnt++;
		return &m_Device[m_DeviceCnt-1];
	}

	Sint32 ConvertKeyNumber( WPARAM id );
	void   InputKeyCheck( UINT iMsg , WPARAM wParam ,LPARAM lParam );

	Sint32 m_FloatMouseX,m_FloatMouseY;

	void DeviceAssignmentConfig( gxBool bControl );

	SINGLETON_DECLARE( CGamePad );

private:

	void configInput();			//複数プレイヤーへのコンフィグ対応用
	void analogConfig();		//アナログのコンフィグ対応用
	void keyBoardConfig();		//キーボード操作のコンフィグ対応用

	//使用するコントローラ情報をアップデートする
	void updateGamepadAssignTolPlayer();

	Sint32 m_DeviceCnt;
	CGameingDevice m_Device[enDeviceMax];
	Sint32 m_ButtonPush[enDeviceMax]={0};

	//キーボード
	Uint8 m_KeyBoardPush[256];

	//マウス
	Uint8   m_MouseClick[8];
	Float32 m_Wheel;

	Float32 m_MouseX,m_MouseY;

	//コントローラーコンフィグ用

	Sint32  m_Selecter = 0;
	Float32 m_fCurrentScrollY = 0.0f;
	Float32 m_fTgtScrollY     = 0.0f;
	Sint32  m_Cursor = 0;
	Sint32  m_CurentDevice = 0;
	gxBool m_bInitConfig = gxFalse;
	Sint32  m_OldButtonID = -1;

	void gxPadInformation();
	void drawgxKeyInfo( Sint32 ii);
	void deviceInformation();
	void drawDevice( Sint32 id , Sint32 ax , Sint32 ay , gxBool bControlEnable );
	void deviceUseConfig();
	void buttonConfig();


};
