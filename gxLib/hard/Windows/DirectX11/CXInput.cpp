﻿//-------------------------------------------------
//
// input
// 
//-------------------------------------------------
#include <gxLib.h>
#include <gxLib/gx.h>
#include <gxLib/gxPadManager.h>
#include "CGamePad.h"
#include "CXInput.h"

#pragma comment(lib,"XInput9_1_0.lib ")

SINGLETON_DECLARE_INSTANCE( CXInput )
//-----------------------------------------------------------------
//ゲームパッド関連情報を管理
//-----------------------------------------------------------------
CXInput::CXInput()
{
	m_bUseController = gxTrue;

/*
	for(Uint32 ii=0;ii<enControllerMax;ii++)
	{
		for(Uint32 jj=0;jj<GDBTN_MAX;jj++)
		{
			m_bEnable[ii][jj] = gxTrue;
		}
	}
*/
	m_uGamingDeviceNum = 0;		//デバイスの数はゼロ

}


CXInput::~CXInput()
{
	//--------------------------------------------------
	//ジョイパッドデバイスを解放する
	//--------------------------------------------------

}


//DirectInputの初期化
void CXInput::Init()
{
	InitXInput();
	GX_DEBUGLOG("【Control】XIパッドデバイスの初期化完了");
}


bool CXInput::InitXInput()
{
	//---------------------------------------------------------
	//XInputオブジェクトを作成する
	//---------------------------------------------------------

	Sint32   numJoyStickDeviceNum=0;						//ジョイスティック検出数

    DWORD dwResult;

    for( DWORD i = 0; i < enControllerMax; i++ )
    {
        dwResult = XInputGetState( i, &GamePad360[i].state );

        if( dwResult == ERROR_SUCCESS )
		{
            //GamePad360[i].bConnected = gxTrue;
			numJoyStickDeviceNum ++;
		}
        else
		{
            GamePad360[i].bConnected = gxFalse;
		}
    }

	if( numJoyStickDeviceNum == 0)
	{
		//JOYSTICK検出できず
        return gxFalse;
    }

	//検出されたジョイパッド数を記録
	m_uGamingDeviceNum = 0;//

	for( Sint32 ii=0; ii< numJoyStickDeviceNum; ii++ )
	{
		m_pGamingDevice[ii] = CGamePad::GetInstance()->GetDevice();
		if (m_pGamingDevice[ii] == NULL)
		{
			continue;
		}
		m_uGamingDeviceNum++;
		m_pGamingDevice[ii]->_DeviceType = 0x01;			//360ゲームパッド
//		sprintf(m_pGamingDevice[ii]->_hardwarename, "XINPUT(%d)" , ii );
		sprintf(m_pGamingDevice[ii]->_hardwarename, "XINPUT");
		m_pGamingDevice[ii]->_bPov[0]    = gxTrue;			//ハットスイッチ0
		m_pGamingDevice[ii]->_bPov[1]    = gxTrue;			//ハットスイッチ1
		m_pGamingDevice[ii]->_bPov[2]    = gxTrue;			//ハットスイッチ2
		m_pGamingDevice[ii]->_bPov[3]    = gxTrue;			//ハットスイッチ3
		m_pGamingDevice[ii]->_num_slider = 0;				//スライダーコントロール数
		m_pGamingDevice[ii]->_bAxisX	 =
		m_pGamingDevice[ii]->_bAxisY	 =
		m_pGamingDevice[ii]->_bAxisZ	 = gxTrue;				//サポートされているAXIS
		m_pGamingDevice[ii]->_bAxisRX	 =
		m_pGamingDevice[ii]->_bAxisRY	 =
		m_pGamingDevice[ii]->_bAxisRZ	 = gxTrue;				//サポートされているAXIS
		m_pGamingDevice[ii]->_bRumble	 = gxTrue;				//振動可能か？
		m_pGamingDevice[ii]->_bXInput	 = gxTrue;

	}

	GX_DEBUGLOG("【Control】使用可能なパッド数は%d",m_uGamingDeviceNum);

	return gxTrue;
}

bool CXInput::Action(int i)
{
	//毎フレーム通る処理
//	Uint32 i;

//	for(i=0;i<m_uGamingDeviceNum;i++)
	if(i >= (int)m_uGamingDeviceNum) return false;
	{
		//使用しない機能について制限する

		bool bFlag;
		bFlag = true;

		if( !m_bUseController )
		{
			//そもそもコントローラーを使用しない場合には全てのボタンを認識させない
			bFlag = gxFalse;
		}

	    DWORD dwResult;

        dwResult = XInputGetState( i, &GamePad360[i].state );

        if( dwResult != ERROR_SUCCESS )
		{
            GamePad360[i].bConnected = gxFalse;
		}

		if (!GamePad360[i].bConnected) return false;;

		UpdateGamingDevices(i);
	}

//	rumble();

	return true;
}


void CXInput::UpdateGamingDevices(int n)
{
	//-----------------------------------------
	//ハードウェア情報を更新する
	//-----------------------------------------
	CGameingDevice *pGameDevice = m_pGamingDevice[n];

	if( !GamePad360[n].bConnected ) return;

	WORD Buttons = GamePad360[n].state.Gamepad.wButtons;

	//状態の取得に成功した

	//---------------------------------
	//AXISについて
	//---------------------------------

	pGameDevice->_Axis.x 		=  GamePad360[n].state.Gamepad.sThumbLX / (65535/2.0f);
	pGameDevice->_Axis.y 		=  GamePad360[n].state.Gamepad.sThumbLY / (-65535/2.0f);
	pGameDevice->_Axis.z		=  GamePad360[n].state.Gamepad.bLeftTrigger / 255.0f;// (-65535 / 2.0f);//(Buttons & XINPUT_GAMEPAD_LEFT_THUMB)? 1 : 0;

	pGameDevice->_Rotation.x 	= GamePad360[n].state.Gamepad.sThumbRX / (65535/2.0f);
	pGameDevice->_Rotation.y 	= GamePad360[n].state.Gamepad.sThumbRY / (-65535/2.0f);
	pGameDevice->_Rotation.z	= GamePad360[n].state.Gamepad.bRightTrigger / 255.0f;// (Buttons & XINPUT_GAMEPAD_RIGHT_THUMB) ? 1 : 0;

	//---------------------------------
	//スライダーコントロールについて
	//---------------------------------
	pGameDevice->_Slider0		 = GamePad360[n].state.Gamepad.bLeftTrigger / 255.0f;
	pGameDevice->_Slider1		 = GamePad360[n].state.Gamepad.bRightTrigger / 255.0f;

	//---------------------------------
	//ハットスイッチについて
	//---------------------------------
	pGameDevice->_Pov0 = 0;
	if( Buttons & XINPUT_GAMEPAD_DPAD_UP    ) pGameDevice->_Pov0 |= JOY_U;
	if( Buttons & XINPUT_GAMEPAD_DPAD_RIGHT ) pGameDevice->_Pov0 |= JOY_R;
	if( Buttons & XINPUT_GAMEPAD_DPAD_DOWN  ) pGameDevice->_Pov0 |= JOY_D;
	if( Buttons & XINPUT_GAMEPAD_DPAD_LEFT  ) pGameDevice->_Pov0 |= JOY_L;

	pGameDevice->_Pov1 = 0;
	pGameDevice->_Pov2 = 0;
	pGameDevice->_Pov3 = 0;

#if 1
	if (Buttons || ABS(pGameDevice->_Axis.x) > 0.5f ||  ABS(pGameDevice->_Axis.y) > 0.5f )
	{
		//一度でもコントローラーに入力したことがあるフラグを立てる
		pGameDevice->m_bUsedDevice = gxTrue;
	}
	//XInput準拠
	pGameDevice->setButton( GDBTN_BUTTON_A, ( Buttons & XINPUT_GAMEPAD_A )			 	? 1:0);
	pGameDevice->setButton( GDBTN_BUTTON_B, ( Buttons & XINPUT_GAMEPAD_B )			 	? 1:0);
	pGameDevice->setButton( GDBTN_BUTTON_X, ( Buttons & XINPUT_GAMEPAD_X )			 	? 1:0);
	pGameDevice->setButton( GDBTN_BUTTON_Y, ( Buttons & XINPUT_GAMEPAD_Y )			 	? 1:0);
	pGameDevice->setButton( GDBTN_BUTTON_L1,( Buttons & XINPUT_GAMEPAD_LEFT_SHOULDER )	? 1:0);
	pGameDevice->setButton( GDBTN_BUTTON_R1,( Buttons & XINPUT_GAMEPAD_RIGHT_SHOULDER )	? 1:0);
	pGameDevice->setButton( GDBTN_BUTTON_L2,( GamePad360[n].state.Gamepad.bLeftTrigger  > 128.0f )? 1:0);
	pGameDevice->setButton( GDBTN_BUTTON_R2,( GamePad360[n].state.Gamepad.bRightTrigger > 128.0f )? 1:0);
	pGameDevice->setButton( GDBTN_BUTTON_SL,( Buttons & XINPUT_GAMEPAD_BACK )			? 1:0);
	pGameDevice->setButton( GDBTN_BUTTON_ST,( Buttons & XINPUT_GAMEPAD_START )		 	? 1:0);
	pGameDevice->setButton( GDBTN_BUTTON_L3,( Buttons & XINPUT_GAMEPAD_LEFT_THUMB )	 	? 1:0);
	pGameDevice->setButton( GDBTN_BUTTON_R3,( Buttons & XINPUT_GAMEPAD_RIGHT_THUMB )	 ? 1:0);
#else
	//DInput準拠
	pGameDevice->_button[0]  = ( Buttons & XINPUT_GAMEPAD_A )			 	? 1:0;
	pGameDevice->_button[1]  = ( Buttons & XINPUT_GAMEPAD_B )			 	? 1:0;
	pGameDevice->_button[2]  = ( Buttons & XINPUT_GAMEPAD_X )			 	? 1:0;
	pGameDevice->_button[3]  = ( Buttons & XINPUT_GAMEPAD_Y )			 	? 1:0;
	pGameDevice->_button[4]  = ( Buttons & XINPUT_GAMEPAD_LEFT_SHOULDER )	? 1:0;
	pGameDevice->_button[5]  = ( Buttons & XINPUT_GAMEPAD_RIGHT_SHOULDER )	? 1:0;

	pGameDevice->_button[6]  = ( Buttons & XINPUT_GAMEPAD_BACK )			? 1:0;
	pGameDevice->_button[7]  = ( Buttons & XINPUT_GAMEPAD_START )		 	? 1:0;

	pGameDevice->_button[10]  = ( GamePad360[n].state.Gamepad.bLeftTrigger > 128.0f )? 1:0;
	pGameDevice->_button[11]  = ( GamePad360[n].state.Gamepad.bRightTrigger > 128.0f )? 1:0;
	pGameDevice->_button[8]  = ( Buttons & XINPUT_GAMEPAD_LEFT_THUMB )	 	? 1:0;
	pGameDevice->_button[9]  = ( Buttons & XINPUT_GAMEPAD_RIGHT_THUMB )	 ? 1:0;
#endif



}


void CXInput::rumble()
{
	//振動

	for( Sint32 ii=0; ii<PLAYER_MAX; ii++ )
	{
		Float32 fRatio[2];

		//該当のプレイヤー使用中のデバイスでのみ振動させる

		gxPadManager::GetInstance()->GetMotorStat( ii, 0, &fRatio[0] );
		gxPadManager::GetInstance()->GetMotorStat( ii, 1, &fRatio[1] );

		for(Uint32 n=0;n<m_uGamingDeviceNum;n++)
		{
			if( fRatio[0] == 0 && fRatio[1] == 0 )
			{
				//停止させる

				XINPUT_VIBRATION vibration;
				ZeroMemory( &vibration, sizeof(XINPUT_VIBRATION) );

				vibration.wLeftMotorSpeed  = 0;
				vibration.wRightMotorSpeed = 0;

				XInputSetState( n, &vibration );

				continue;
			}

			//if( プレイヤー番号 != 割り振られたコントローラ番号 ) continue;
			if (m_pGamingDevice[n]->_PlayerID != ii) continue;

			//一度も入力を受け付けたことがなければ放置パッドなので振動させない
			if (!m_pGamingDevice[n]->m_bUsedDevice) continue;

			XINPUT_VIBRATION vibration;
			ZeroMemory( &vibration, sizeof(XINPUT_VIBRATION) );
			vibration.wLeftMotorSpeed  = (Sint32)(fRatio[0]*65535);
			vibration.wRightMotorSpeed = (Sint32)(fRatio[1]*65535);
			DWORD err = XInputSetState( n, &vibration );
			if (err != ERROR_SUCCESS)
			{
				err = err;
			}

	    }

	}

}

