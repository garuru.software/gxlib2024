﻿//---------------------------------------------------------------
//
// Windows Desktop向け::メイン
//
//
//
//---------------------------------------------------------------
//ren
#include <gxLib.h>
#include <gxLib/gx.h>
#include <gxLib/gxPadManager.h>
#include <gxLib/gxFileManager.h>
#include <gxLib/util/gxUIManager.h>
#include <gxLib/gxDebug.h>
#include "../CHttpClient.h"

#ifdef _USE_OPENGL
	#include "COpenGLWin.h"
#else
	#include "CDx11Desktop.h"
#endif

#ifdef _USE_OPENAL
	#include "../../COpenAL.h"
#else
	#include "../CXAudio2.h"
#endif

#include "gxLibResource.h"
#include "CGamePad.h"

#include <shellapi.h>	//<- VC2017ではこっち

#if defined(STEAM_BUILD)
#include "SteamAPI/steam_api.h"
#endif // defined(STEAM_BUILD)

SINGLETON_DECLARE_INSTANCE( CWindows );

#define NAME_APRICATION APPLICATION_NAME
#define NAME_APRICLASS  "gxLib2018"

void makeAccelKey();

enum {
	enID_ChangeFullScreenMode = 11001,	//フルスクリーン切り替え
	enID_AppExit              = 11002,	//アプリ終了
	enID_GamePause			  = 11003,	//ゲームのポーズ
	enID_GameStep			  = 11004,	//ゲームのステップ
	enID_PadEnableSwitch	  = 11005,	//コントローラー設定
	enID_DebugMode			  = 11006,	//デバッグモードのON/OFF
	enID_Reset				  = 11007,	//リセット
	enID_ScreenShot           = 11008,	//スクリーンショット
	enID_FullSpeed			  = 11009,	//フルスピード
	enID_SoundSwitch		  = 11010,	//サウンドOn /Off
	enID_SamplingFilter		  = 11011,	//サンプリングフィルター

	enID_MasterVolumeAdd	  = 11012,	//Volume+
	enID_MasterVolumeSub	  = 11013,	//Volume+
	enID_SwitchVirtualPad	  = 11014,	//VirtualPad On/ Off
	enID_DispManual			  = 11015,	//マニュアル表示
	enID_DispEULA			  = 11016,	//EULA表示

	enID_SoftPause       	  = 11300,	//Game Pause
	enAccelMax			  	  = 32,

};


LRESULT CALLBACK    gxLibWindowProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

void makeWindow( );
void AddNotificationIcon( HWND hWnd );

#define ID_TRAYICON (1)
#define WM_TASKTRAY     (WM_APP + 1) 
NOTIFYICONDATA nid = { sizeof(NOTIFYICONDATA) };

//------------------------------------------------------------------------
#ifdef USE_WEBVIEW
#include <windows.h>
#include <stdlib.h>
#include <string>
#include <tchar.h>
#include <wrl.h>
#include <wil/com.h>
// include WebView2 header
#include "WebView2.h"

using namespace Microsoft::WRL;
// Pointer to WebViewController
static wil::com_ptr<ICoreWebView2Controller> webviewController;

// Pointer to WebView window
static wil::com_ptr<ICoreWebView2> webviewWindow;
void makeWebView(std::string url);
void closeWebView();
#endif
//------------------------------------------------------------------------
void changeWindowSize(Sint32 width=0, Sint32 height=0);
void appUpdate();

wchar_t* CDeviceManager_SJIStoUTF16(gxChar* pString, size_t* pSize=nullptr);
std::string CDeviceManager_UTF16toUTF8(wchar_t* pUTF16buf, size_t* pSize = nullptr);
std::vector<Uint16> CDeviceManager_UTF8toUTF16(std::string);

int APIENTRY WinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

#if defined(STEAM_BUILD)
	// アプリのID
	int APP_ID = 3423840;

 #if defined(STEAM_TRIAL)
	// 体験版用のアプリIDに差し替える。
	APP_ID = 3476650;
 #endif // defined(STEAM_TRIAL)

	//SteamAPI用処理
	if (SteamAPI_RestartAppIfNecessary(APP_ID))
	{
		// Steamを経由してゲームを実行していない。
		// 一度アプリを閉じて、Steamを起動して、Steamからアプリを起動する流れになる。
		return 0;
	}
	else
	{
		// Steamを経由してゲームを実行している。(正常)
		if (!SteamAPI_Init())
		{
			// Steam APIの初期化失敗
			//printf("SteamAPIの初期化に失敗しました。\n");
			return 0;
		}
		// Steam APIの初期化成功
	}
#endif // defined(STEAM_BUILD)

#if 1
	//引数分解
	{
		gxChar *pBuf = new gxChar[1024];
		GetModuleFileName(hInstance, pBuf, 256);
		std::string UTF8 = CDeviceManager::SJIStoUTF8(pBuf);
		CGameGirl::GetInstance()->DragAndDropArgs.push_back(UTF8);
		SAFE_DELETES(pBuf);

		int NumArgs = 0;
		wchar_t* pWCharStr = CDeviceManager_SJIStoUTF16(lpCmdLine);
		LPWSTR *argv = CommandLineToArgvW(pWCharStr, &NumArgs);
		for (Sint32 ii = 0; ii < NumArgs; ii++)
		{
			size_t sz = 0;
			UTF8 = CDeviceManager_UTF16toUTF8(argv[ii], &sz);

			CGameGirl::GetInstance()->DragAndDropArgs.push_back(UTF8);
		}
	}

	auto &agv = CGameGirl::GetInstance()->DragAndDropArgs;

	for (Sint32 ii = 0; ii < agv.size(); ii++)
	{
		if (agv[ii] == "-batchmode")
		{
			SetConsoleOutputCP(CP_UTF8);
			CDeviceManager::GetInstance()->SetBatchMode();
			if (!::AttachConsole(ATTACH_PARENT_PROCESS))
			{
				::AllocConsole();
			}
			freopen("CON", "r", stdin);     // 標準入力の割り当て
			freopen("CON", "w", stdout);    // 標準出力の割り当て
			printf("%s:batchmodeStart\n", NAME_APRICATION);

			CGameGirl::GetInstance()->DragAndDropArgs.erase(agv.begin() + ii);
		}
	}
#endif

	CWindows::GetInstance()->m_hInstance = hInstance;

	makeWindow();
	makeAccelKey();

	CDeviceManager::GetInstance()->AppInit();

	changeWindowSize();

	if( CWindows::GetInstance()->IsFullScreen() )
	{
		PostMessage(CWindows::GetInstance()->m_hWindow, WM_COMMAND, IDM_SCREENMODE_FULL, 0);
	}

	while( gxTrue )
	{
		CGameGirl::GetInstance()->Action();

		if (CGameGirl::GetInstance()->IsAppFinish())
		{
			break;
		}

		// --------------------------------------------------------------------
		// メイン メッセージ ループ:
		// ※これがないとWindowsからのメッセージを受けられなくなるので注意
		// --------------------------------------------------------------------

		MSG msg;

		while (PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
		{
			if (!GetMessage(&msg, NULL, 0, 0))
			{
				CWindows::GetInstance()->m_wParam = msg.wParam;
			}

			if (!TranslateAccelerator(msg.hwnd, CWindows::GetInstance()->m_hAccel, &msg))
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		appUpdate();

		//スクリーンセーバーカウンターをリセット
		::SetThreadExecutionState(ES_DISPLAY_REQUIRED);
		//マウスを動かして、スクリーンセーバー抑止
		INPUT input[1];
		::ZeroMemory(input, sizeof(INPUT));
		input[0].type = INPUT_MOUSE;
		input[0].mi.dwFlags = MOUSEEVENTF_MOVE;//相対座標指定でマウスを動かす。
		input[0].mi.dx = 0; //でも０で初期化しているので相対座標０で動かさない。
		input[0].mi.dy = 0;
		::SendInput(1, input, sizeof(INPUT)); //移動

	}

	CHttpClient::DeleteInstance();

	//CDeviceManager::GetInstance()->SaveConfig();

	GX_DEBUGLOG("system close");

	CGameGirl::GetInstance()->End();

	//メインスレッドの終了まちを追加
	while (CGameGirl::GetInstance()->IsGameMainThreadExist())
	{
		gxLib::Sleep(10);
	}

	CGameGirl::DeleteInstance();

	CGraphics::DeleteInstance();
	CAudio::DeleteInstance();
    CGamePad::DeleteInstance();

	DestroyAcceleratorTable( CWindows::GetInstance()->m_hAccel);
	DestroyWindow( CWindows::GetInstance()->m_hWindow );

	CWindows::DeleteInstance();

	CDeviceManager::DeleteInstance();

	gxLib::Destroy();

	printf("done.\n");

#if defined(STEAM_BUILD)
	// SteamAPI用処理
	// SteamAPIを終了させる。
	SteamAPI_Shutdown();
#endif // defined(STEAM_BUILD)

	return 0;
}

void appUpdate()
{
#ifdef USE_WEBVIEW
	std::string url = CWindows::GetInstance()->m_WebViewURL;

	//とりあえずメインスレッドの処理はここでしかできないから。。。

	if (url != "")
	{
		if (CWindows::GetInstance()->m_bWebBrowser)
		{
			CWindows::GetInstance()->ExecuteApp((char*)url.c_str());
		}
		else
		{
			if (url == "CloseWebView")
			{
				closeWebView();
			}
			else
			{
				makeWebView(CWindows::GetInstance()->m_WebViewURL);
			}
		}
		CWindows::GetInstance()->m_WebViewURL = "";
	}
#endif

	if (CWindows::GetInstance()->m_ChangeWindowSize > 0)
	{
		CWindows::GetInstance()->m_ChangeWindowSize--;
		if (CWindows::GetInstance()->m_ChangeWindowSize == 0)
		{
			RECT rect;
			GetWindowRect(
				CWindows::GetInstance()->m_hWindow, &rect);
			gxLib::SaveData.WindowSizeW = rect.right - rect.left;
			gxLib::SaveData.WindowSizeH = rect.bottom - rect.top;
			gxLib::SaveData.WindowSizeX = rect.left;
			gxLib::SaveData.WindowSizeY = rect.top;
			gxLib::SaveConfig();
		}
	}

}



void makeWindow()
{
	gxChar buf[1024];
	size_t sz;
	sprintf(buf, "%s", NAME_APRICATION);
	std::string  str = CDeviceManager::UTF8toSJIS(buf, &sz);
	sprintf(buf, "%s", str.c_str());

	Sint32 ax, ay;
	Sint32 desktop_w, desktop_h, client_w, client_h;
	RECT desktop;
	client_w = 120;
	client_h = 120;
	GetWindowRect(GetDesktopWindow(), (LPRECT)&desktop);

	desktop_w = (desktop.right - desktop.left);
	desktop_h = (desktop.bottom - desktop.top);

	ax = desktop_w / 2 - client_w / 2;
	ay = desktop_h / 2 - client_h / 2;

	if (ax < 0) ax = 0;
	if (ay < 0) ay = 0;

	WNDCLASSEX windowConfig;

	windowConfig.cbSize = sizeof(WNDCLASSEX);

	windowConfig.style = CS_HREDRAW | CS_VREDRAW;
	windowConfig.lpfnWndProc = gxLibWindowProc;
	windowConfig.cbClsExtra = 0;
	windowConfig.cbWndExtra = 0;
	windowConfig.hInstance = CWindows::GetInstance()->m_hInstance;
	windowConfig.hIcon = LoadIcon(CWindows::GetInstance()->m_hInstance, MAKEINTRESOURCE(IDI_GXLIB_ICON));
	windowConfig.hCursor = LoadCursor(nullptr, IDC_ARROW);
	windowConfig.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
#ifdef GX_DEBUG
	windowConfig.lpszMenuName = MAKEINTRESOURCE(IDC_HELPWINDOW);
	windowConfig.lpszMenuName = nullptr;
#else
	windowConfig.lpszMenuName = nullptr;
#endif
	windowConfig.lpszClassName = TEXT(NAME_APRICLASS);
	windowConfig.hIconSm = LoadIcon(windowConfig.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	RegisterClassEx(&windowConfig);

	CWindows::GetInstance()->m_hWindow = CreateWindow(
		TEXT(NAME_APRICLASS),
		TEXT(buf),
		WS_OVERLAPPEDWINDOW | WS_POPUP | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX,
		ax,
		ay,
		client_w,
		client_h,
		NULL,
		NULL,
		CWindows::GetInstance()->m_hInstance,
		NULL);

	CWindows::GetInstance()->m_WinDC = GetDC(CWindows::GetInstance()->m_hWindow);
	CWindows::GetInstance()->m_AppStyle = GetWindowLong(CWindows::GetInstance()->m_hWindow, GWL_STYLE);

	/*-- 念のためウインドウサイズ補正 --*/
	// ハードがマルチタッチをサポートしているかどうか
	int  value = ~GetSystemMetrics(SM_DIGITIZER);

	if (!(value & 0xc0))
	{
		RegisterTouchWindow(CWindows::GetInstance()->m_hWindow, 0);
	}

	//QueryPerformanceFrequency((LARGE_INTEGER*)&CWindows::GetInstance()->Vsyncrate);		//秒間のカウント


}

void changeWindowSize( Sint32 width , Sint32 height )
{

	//画面の中央にセット
	RECT desktop;
	GetWindowRect( GetDesktopWindow(), (LPRECT)&desktop);

	Sint32 x,y;
	x = desktop.right/2;
	y = desktop.bottom/2;
	// フレームなどのクライアント領域以外のサイズを考慮

	Sint32 client_w,client_h;
	Sint32 frame_w, frame_h;
	Sint32 desktop_w, desktop_h;

	RECT clientRect = {0,0, WINDOW_W , WINDOW_H};

	AdjustWindowRect( &clientRect, WS_OVERLAPPEDWINDOW | WS_POPUP | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX , TRUE );

	client_w = clientRect.right - clientRect.left;
	client_h = clientRect.bottom - clientRect.top;
	frame_w = client_w - WINDOW_W;
	frame_h = client_h - WINDOW_H;
	desktop_w = (desktop.right - desktop.left);
	desktop_h = (desktop.bottom - desktop.top);
	client_w = WINDOW_W;
	client_h = WINDOW_H;

	if( gxLib::SaveData.WindowSizeW > 0 && gxLib::SaveData.WindowSizeH > 0 )
	{
		client_w = gxLib::SaveData.WindowSizeW;
		client_h = gxLib::SaveData.WindowSizeH;
		x = gxLib::SaveData.WindowSizeX;
		y = gxLib::SaveData.WindowSizeY;
	}
	else if ( width > 0 && height > 0)
	{
		client_w = width;
		client_h = height;
		client_w += frame_w;
		client_h += frame_h;

		x += -client_w/2;
		y += -client_h/2;
	}
	else
	{
		if ( client_w >= desktop_w*0.9f)
		{
			//大きすぎる時は画面に収まるサイズに調整
			Float32 fRatio = 1.0f * desktop_w * 0.9f / WINDOW_W;
			client_w = desktop_w*0.9f;
			client_h = WINDOW_H * fRatio;
		}

		if(client_h > desktop_h * 0.9f)
		{
			Float32 fRatio = 1.0f* desktop_h * 0.9f / WINDOW_H;
			client_h = desktop_h * 0.9f;
			client_w = WINDOW_W *fRatio;
			client_h -= 2;
		}

		client_w += frame_w;
		client_h += frame_h;

		x += -client_w/2;
		y += -client_h/2;
	}

	MoveWindow(CWindows::GetInstance()->m_hWindow, x, y, client_w, client_h, true);

	ShowWindow( CWindows::GetInstance()->m_hWindow, !CDeviceManager::GetInstance()->IsBatchMode() );

	UpdateWindow( CWindows::GetInstance()->m_hWindow );

	GetWindowRect( CWindows::GetInstance()->m_hWindow , &CWindows::GetInstance()->m_WinRect );
}


LRESULT CALLBACK gxLibWindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	// --------------------------------------------------------------------
	//基本的なウインドウコールバック受付
	// --------------------------------------------------------------------
	static HWND hStatus;

	switch (message){
    case WM_CREATE:
	    DragAcceptFiles(hWnd, gxTrue);
	    AddNotificationIcon( hWnd );
		break;

	case WM_DROPFILES:
		{
			HDROP hDrop = (HDROP) wParam;
			Sint32 sNum = DragQueryFile(hDrop , 0xffffffff , NULL , NULL );

			gxFileManager::GetInstance()->SetDragEnable( gxFalse );
			gxFileManager::GetInstance()->SetDropFileNum( sNum );

			gxChar str[512];

			for(Sint32 ii=0;ii<sNum;ii++)
			{
#if 0
				wchar_t wStr[512];
				DragQueryFile(hDrop , ii , (LPWSTR)wStr , sizeof(wStr) );
				Uint32 uLen = wcslen(wStr);
				WideCharToMultiByte( CP_ACP , 0 ,(LPCWSTR)wStr , uLen+1 , str, uLen*2 ,NULL,NULL);
#else
				DragQueryFile(hDrop , ii , str , sizeof(str) );
#endif
				gxFileManager::GetInstance()->AddDropFile( str );
			}
			gxFileManager::GetInstance()->SetDragEnable( gxTrue );
		}
		break;


	case WM_SYSCOMMAND:
		//スクリーンセーバー抑制
		if ( wParam == SC_SCREENSAVE )
	    {
	        return 1;
	    }
		if ( wParam == SC_MONITORPOWER )
	    {
	        return 1;
	    }
	    return (DefWindowProc(hWnd, message, wParam, lParam));

	case WM_TASKTRAY:
		if ( wParam == ID_TRAYICON )
		{
			switch ( lParam ){
			case WM_RBUTTONDOWN:
				{
					POINT po;
					HMENU hMenu , tmp;
					SetForegroundWindow( hWnd );
					GetCursorPos(&po);
					//.x = GET_X_LPARAM(lParam);
					//po.y = GET_Y_LPARAM(lParam);
					//ClientToScreen(hWnd , &po );
					tmp = GetMenu(hWnd);//LoadMenu( CWindows::GetInstance()->m_hInstance , TEXT(NAME_APRICLASS));
					hMenu = GetSubMenu(tmp , 0);
					TrackPopupMenu( hMenu , TPM_LEFTALIGN | TPM_BOTTOMALIGN ,	po.x , po.y , 0 , hWnd , NULL );
					GX_DEBUGLOG("WM_TASKTRAY");
				}
				break;
			}
		}
		break;

    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
            // 選択されたメニューの解析:
            switch (wmId)
            {
			case IDM_WINDOW_ORIGINAL:
				changeWindowSize();
				CGameGirl::GetInstance()->SetScreenMode(CGameGirl::enScreenModeOriginal );
				CWindows::GetInstance()->SetRenderMode(CGameGirl::enScreenModeOriginal);
				CGameGirl::GetInstance()->AdjustScreenResolution();
				break;

			case IDM_WINDOW_ASPECT:
				CGameGirl::GetInstance()->SetScreenMode(CGameGirl:: enScreenModeAspect );
				CWindows::GetInstance()->SetRenderMode(CGameGirl::enScreenModeAspect);
				CGameGirl::GetInstance()->AdjustScreenResolution();
				break;

			case IDM_WINDOW_STRETCH:
				CGameGirl::GetInstance()->SetScreenMode(CGameGirl::enScreenModeFull );
				CWindows::GetInstance()->SetRenderMode(CGameGirl::enScreenModeFull);
				CGameGirl::GetInstance()->AdjustScreenResolution();
				break;

			case IDM_DRAW_3D:
				CGameGirl::GetInstance()->Set3DView( !CGameGirl::GetInstance()->Is3DView() );
				break;

			case enID_SoftPause:
				CGameGirl::GetInstance()->SetSoftPause( !CGameGirl::GetInstance()->IsSoftPause() );
				break;

			case enID_GamePause			 : 	//ゲームのポーズ
				CGameGirl::GetInstance()->SetPause( !CGameGirl::GetInstance()->IsPause() );
				break;

			case enID_GameStep			 : 	//ゲームのステップ
				if( CGameGirl::GetInstance()->IsPause() )
				{
					CGameGirl::GetInstance()->PauseStep();
				}
				else
				{
					CGameGirl::GetInstance()->SetPause(gxTrue);
				}
				break;

			case enID_DebugMode			 : 	//デバイスモードの切り替え
				CGameGirl::GetInstance()->ToggleDeviceMode();
				break;

			case enID_Reset:
			case IDM_RESET:
			 	//リセット
			 	CGameGirl::GetInstance()->SetReset();
				break;

			case IDM_MANUAL:
			case enID_DispManual:
				//マニュアル表示
				gxLib::OpenAppManual();
				break;

			case enID_DispEULA:
			case IDM_LICENSE:
				//EULA表示
				//DialogBox(CWindows::GetInstance()->m_hInstance, MAKEINTRESOURCE(IDD_DIALOG_LICENSE), hWnd, About);
				gxLib::OpenEULADocument();
				break;

			case enID_ScreenShot         : 	//スクリーンショット
			case IDM_SCREENSHOT:
				CGameGirl::GetInstance()->SetScreenShot(gxTrue);
				gxLib::SetToast("スクリーンショットを保存しました");
				break;

			case IDM_ABOUT:
				DialogBox(CWindows::GetInstance()->m_hInstance, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
				break;

			case enID_SamplingFilter:
				//サンプリングフィルター切り替え
				if( CWindows::GetInstance()->GetRenderFilter() == CWindows::enSamplingNearest )
				{
					CWindows::GetInstance()->SetRenderFilter( CWindows::enSamplingBiLenear );
				}
				else
				{
					CWindows::GetInstance()->SetRenderFilter( CWindows::enSamplingNearest );
				}
				break;
			case IDM_DRAW_BILINEAR:
				CWindows::GetInstance()->SetRenderFilter(CWindows::enSamplingBiLenear);
				break;
			case IDM_DRAW_NEAREST:
				CWindows::GetInstance()->SetRenderFilter(CWindows::enSamplingNearest);
				break;

			case enID_FullSpeed			 : 	//フルスピード
				CGameGirl::GetInstance()->WaitVSync( !CGameGirl::GetInstance()->IsWaitVSync() );
				break;

			case enID_SoundSwitch		 : 	//サウンドON/OFF
				if( CWindows::GetInstance()->IsSoundEnable() )
				{
					CWindows::GetInstance()->SetSoundEnable(gxFalse);
				}
				else
				{
					CWindows::GetInstance()->SetSoundEnable(gxTrue);
				}
				break;

			case IDM_WINDOW_DEBUG_OVERVIEW:
				CGameGirl::GetInstance()->SetDebugModePage(gxDebug::enPageTop);
				break;
			case IDM_WINDOW_DEBUG_LOG:
				CGameGirl::GetInstance()->SetDebugModePage(gxDebug::enPageLog);
				break;
			case IDM_WINDOW_DEBUG_TEXTURE:
				CGameGirl::GetInstance()->SetDebugModePage(gxDebug::enPageTex);
				break;
			case IDM_WINDOW_DEBUG_SOUND:
				CGameGirl::GetInstance()->SetDebugModePage(gxDebug::enPageSound);
				break;
			case IDM_WINDOW_DEBUG_INPUT:
				CGameGirl::GetInstance()->SetDebugModePage(gxDebug::enPagePad);
				break;

			case enID_ChangeFullScreenMode:	//フルスクリーン切り替え
			case IDM_FULLSCREEN_SWITCH:
				{
					gxBool bCurrent = CWindows::GetInstance()->IsFullScreen();
					CWindows::GetInstance()->SetFullScreen(!bCurrent);
					gxLib::SaveData.SetFlag(gxLib::StSaveData::FULLSCREEN, !bCurrent);
					CGraphics::GetInstance()->Reset();
					CWindows::GetInstance()->MenuUpdate();
				}
				break;
			case IDM_SCREENMODE_FULL:
				CWindows::GetInstance()->SetFullScreen(gxTrue);
				CGraphics::GetInstance()->Reset();
				CWindows::GetInstance()->MenuUpdate();
				break;
			case IDM_SCREENMODE_WINDOW:
				CWindows::GetInstance()->SetFullScreen(gxFalse);
				CGraphics::GetInstance()->Reset();
				CWindows::GetInstance()->MenuUpdate();
				break;

			case IDM_WINDOW_SIZE50:
			{
				gxLib::SaveData.WindowSizeW = 0;
				gxLib::SaveData.WindowSizeH = 0;
				changeWindowSize(WINDOW_W / 2, WINDOW_H / 2);
				RECT rect;
				GetWindowRect(
					CWindows::GetInstance()->m_hWindow, &rect);
				gxLib::SaveData.WindowSizeW = rect.right - rect.left;
				gxLib::SaveData.WindowSizeH = rect.bottom - rect.top;
			}
			break;
			case IDM_WINDOW_SIZE100:
			{
				gxLib::SaveData.WindowSizeW = 0;
				gxLib::SaveData.WindowSizeH = 0;
				changeWindowSize(WINDOW_W, WINDOW_H);
				RECT rect;
				GetWindowRect(
					CWindows::GetInstance()->m_hWindow, &rect);
				gxLib::SaveData.WindowSizeW = rect.right - rect.left;
				gxLib::SaveData.WindowSizeH = rect.bottom - rect.top;
			}
			break;
			case IDM_WINDOW_SIZE200:
			{
				gxLib::SaveData.WindowSizeW = 0;
				gxLib::SaveData.WindowSizeH = 0;
				changeWindowSize(WINDOW_W*2,WINDOW_H*2);
				RECT rect;
				GetWindowRect(
					CWindows::GetInstance()->m_hWindow, &rect);
				gxLib::SaveData.WindowSizeW = rect.right - rect.left;
				gxLib::SaveData.WindowSizeH = rect.bottom - rect.top;
			}
			break;


			case IDM_SOUND_ON:
				CWindows::GetInstance()->SetSoundEnable( gxTrue );
				break;
			case IDM_SOUND_OFF:
				CWindows::GetInstance()->SetSoundEnable( gxFalse );
				break;

			case enID_MasterVolumeAdd:
				//Volume ++
				{
					Float32 fVol = gxLib::GetAudioMasterVolume();
					gxLib::SetAudioMasterVolume(fVol += 0.1f);
				}
				break;

			case enID_MasterVolumeSub:
				//Volume --
				{
					Float32 fVol = gxLib::GetAudioMasterVolume();
					gxLib::SetAudioMasterVolume( fVol -= 0.1f );
				}
				break;

			case enID_PadEnableSwitch: 	//コントローラー切り替え
			{
				gxBool bEnableController = gxPadManager::GetInstance()->IsEnableController();
				gxPadManager::GetInstance()->SetEnableController(!bEnableController);
			}
			break;
			case IDM_CONTROLLER:
			case enID_SwitchVirtualPad:
				{
					gxBool bVirtualPad = !CWindows::GetInstance()->m_bVirtualPad;
					CWindows::GetInstance()->SetControllMode(bVirtualPad);
			}
				break;

			case enID_AppExit             :	//アプリ終了
            case IDM_EXIT:
				if (CWindows::GetInstance()->IsFullScreen())
				{
					CWindows::GetInstance()->SetFullScreen(gxFalse);
					CGraphics::GetInstance()->Reset();
				}
				else
				{
					CGameGirl::GetInstance()->Exit();
				}
                break;
            default:
                return DefWindowProc(hWnd, message, wParam, lParam);
            }
        }
        break;

	case WM_MOVE:
	{
		if( CGameGirl::GetInstance()->IsStartGameGirl() )
		{
			CWindows::GetInstance()->m_ChangeWindowSize = 8;
		}
	}
	break;
	
	case WM_SIZE:
		{
			Sint32 w, h;
			//if (!CWindows::GetInstance()->IsFullScreen())
			{
				w = LOWORD(lParam);
				h = HIWORD(lParam);
				CGameGirl::GetInstance()->SetWindowSize(w, h);
				CGameGirl::GetInstance()->AdjustScreenResolution();
				CDeviceManager::GetInstance()->Resume();
				CWindows::GetInstance()->m_ChangeWindowSize = 8;
			}


#ifdef USE_WEBVIEW
			if (webviewController)
			{
				RECT bounds;
				GetClientRect(hWnd, &bounds);
				w = bounds.right - bounds.left;
				h = bounds.bottom - bounds.top;

				Sint32 blank = 0;
				blank = w * 0.1f;

				bounds.left   += blank;
				bounds.right  += -blank;
				bounds.top    += blank;
				bounds.bottom += -blank;

				webviewController->put_Bounds(bounds);
			}
#endif
		}
		break;

    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
            // TODO: HDC を使用する描画コードをここに追加してください...
            EndPaint(hWnd, &ps);
        }
        break;

	case WM_QUERYENDSESSION:
		ShutdownBlockReasonCreate(hWnd,L"gxLib running" );
		return false;

 	case WM_ENDSESSION:
		CGameGirl::GetInstance()->Exit();
		ShutdownBlockReasonDestroy(hWnd);
		break;

	case WM_CLOSE:
		DragAcceptFiles(hWnd, gxFalse);
		CGameGirl::GetInstance()->Exit();
        break;

    case WM_DESTROY:
        PostQuitMessage(0);
        Shell_NotifyIcon( NIM_DELETE, &nid );
        break;

    default:
		//デバイスの入力状態をチェック
		if (!CGameGirl::IsEndApp())
		{
			CGamePad::GetInstance()->InputKeyCheck(message, wParam, lParam);
		}

        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}


INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	// --------------------------------------------------------------------
	// アバウトダイアログ
	// --------------------------------------------------------------------

    UNREFERENCED_PARAMETER(lParam);

    switch (message){
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}


void makeAccelKey()
{
	// --------------------------------------------------------------------
	// アクセラレーターキーを登録する
	// --------------------------------------------------------------------

	ACCEL wAccel[ enAccelMax ];

	struct AccelInfo {
		Uint32 id;
		Uint32 vKey1;
		Uint32 vKey2;
	};

	AccelInfo AccelTbl[]={

#ifdef GX_MASTER
		{ enID_AppExit 				,VK_ESCAPE	,0 },
		{ enID_ChangeFullScreenMode	,VK_RETURN	,FALT },
//		{ enID_SoftPause			,VK_F1		,FVIRTKEY },
//		{ enID_SoundSwitch			,VK_F2		,FVIRTKEY },
//		{ enID_GameStep				,VK_F3		,FVIRTKEY },
//		{ enID_SwitchVirtualPad		,VK_F4		,FVIRTKEY },

//		{ enID_Reset				,VK_F5		,FVIRTKEY },
//		{ enID_SamplingFilter		,VK_F6		,FVIRTKEY },
//		{ enID_FullSpeed			,VK_F7		,FVIRTKEY },
//		{ enID_PadEnableSwitch		,VK_F8		,FVIRTKEY },

	//	{ enID_DebugMode			,VK_F9		,FVIRTKEY },
//		{ enID_DispManual			,VK_F10		,FVIRTKEY },
//		{ enID_DispEULA				,VK_F11		,FVIRTKEY },
//		{ enID_ScreenShot			,VK_F12		,FVIRTKEY },

		{ enID_MasterVolumeAdd		,VK_ADD			/*VK_PRIOR*/,FVIRTKEY },
		{ enID_MasterVolumeSub		,VK_SUBTRACT	/*VK_NEXT*/	,FVIRTKEY },

#else
		{ enID_AppExit 				,VK_ESCAPE	,0 },
		{ enID_SoftPause	     	,'P'		,FALT|FVIRTKEY },
		{ enID_ChangeFullScreenMode	,VK_RETURN	,FALT },
		{ enID_GamePause			,VK_F1		,FVIRTKEY },
		{ enID_SoundSwitch			,VK_F2		,FVIRTKEY },
		{ enID_GameStep				,VK_F3		,FVIRTKEY },
		{ enID_SwitchVirtualPad		,VK_F4		,FVIRTKEY },

		{ enID_Reset				,VK_F5		,FVIRTKEY },
		{ enID_SamplingFilter		,VK_F6		,FVIRTKEY },
		{ enID_FullSpeed			,VK_F7		,FVIRTKEY },
		{ enID_PadEnableSwitch		,VK_F8		,FVIRTKEY },

		{ enID_DebugMode			,VK_F9		,FVIRTKEY },
		{ enID_DispManual			,VK_F10		,FVIRTKEY },
		{ enID_DispEULA				,VK_F11		,FVIRTKEY },
		{ enID_ScreenShot			,VK_F12		,FVIRTKEY },

		{ enID_MasterVolumeAdd		,VK_ADD			/*VK_PRIOR*/,FVIRTKEY },
		{ enID_MasterVolumeSub		,VK_SUBTRACT	/*VK_NEXT*/	,FVIRTKEY },

//		{ enID_ChangeFullScreenMode	,VK_F5		,FVIRTKEY },

#endif
	};

	Sint32 max = ARRAY_LENGTH(AccelTbl);

	for( Sint32 ii=0; ii<max; ii++ )
	{
		wAccel[ii].cmd   = AccelTbl[ii].id;
		wAccel[ii].key   = AccelTbl[ii].vKey1;
		wAccel[ii].fVirt = AccelTbl[ii].vKey2;
	}

	CWindows::GetInstance()->m_hAccel = CreateAcceleratorTable( wAccel, max );

}


void AddNotificationIcon( HWND hWnd )
{

	//nid.cbSize = ;
	nid.hWnd = hWnd;
	nid.uFlags = NIF_ICON | NIF_TIP | NIF_MESSAGE | NIF_SHOWTIP | NIF_GUID;
	nid.hIcon = LoadIcon(CWindows::GetInstance()->m_hInstance, MAKEINTRESOURCE(IDI_SMALL));;
	nid.uID   = ID_TRAYICON;
	//nid.uCallbackMessage = WMAPP_NOTIFYCALLBACK;
	nid.uCallbackMessage = WM_TASKTRAY;      // 通知メッセージの定数
	lstrcpy(nid.szTip, TEXT(NAME_APRICATION));

	if( !Shell_NotifyIcon(NIM_ADD, &nid) )
	{
		DWORD lasterror;
		lasterror = GetLastError();
		// GetLastError == ERROR_SUCCESS or ERROR_FILE_NOT_FOUND // taskbar was not found.
		while( lasterror == ERROR_TIMEOUT )
		{
			Sleep(1000);
			if( Shell_NotifyIcon(NIM_MODIFY, &nid) )
			{
				break;// NotifyIcon alredy exists.
			}
			if( Shell_NotifyIcon(NIM_ADD, &nid) )
			{
				break;// NotifyIcon adding is successful.
			}
			lasterror = GetLastError();
		}
	}
	// NOTIFYICON_VERSION_4 is prefered
	nid.uVersion = NOTIFYICON_VERSION_4;

	//return Shell_NotifyIcon(NIM_SETVERSION, &nid);
}


void CWindows::ExecuteApp( char *Appname )
{
	//アプリケーションを実行する

	Uint32 len = (Uint32)strlen( Appname );

#if 1
	wchar_t wStr[0xff];
	len = MultiByteToWideChar( CP_ACP , 0 ,(char*)Appname , (int)strlen((char*)Appname) , wStr , 0xff );
	wStr[len] = 0x0000;
	//ShellExecuteW( CWindows::GetInstance()->m_hWindow, L"open",  wStr, NULL, NULL, SW_SHOWNORMAL);
	ShellExecuteW( nullptr, L"open", wStr, NULL, NULL, SW_SHOWNORMAL);
#else
	ShellExecute(CWindows::GetInstance()->m_hWindow, _T("open"),  Appname, NULL, NULL, SW_SHOWNORMAL);
#endif

}

void CWindows::SetRenderFilter( eSamplingFilter Filter )
{
	m_SamplingFilter = Filter;

	HMENU hMenu = GetMenu(CWindows::GetInstance()->m_hWindow);
	CheckMenuRadioItem(hMenu, IDM_DRAW_BILINEAR, IDM_DRAW_NEAREST, IDM_DRAW_BILINEAR + ((Filter ==0) ? 0 : 1), MF_BYCOMMAND);

	gxLib::SaveData.RenderFilter = (Sint8)Filter;
}

void CWindows::SetSoundEnable( gxBool bSoundEnable )
{
	static Float32 s_fVol = gxLib::GetAudioMasterVolume();

	if( bSoundEnable )
	{
		gxLib::SetAudioMasterVolume( s_fVol );
	}
	else
	{
		s_fVol = gxLib::GetAudioMasterVolume();
		gxLib::SetAudioMasterVolume( 0.0f );
	}
	m_bSoundEnable = bSoundEnable;

	HMENU hMenu = GetMenu(CWindows::GetInstance()->m_hWindow);
	CheckMenuRadioItem( hMenu, IDM_SOUND_ON, IDM_SOUND_OFF, IDM_SOUND_ON + (bSoundEnable? 0 : 1), MF_BYCOMMAND );

}

void CWindows::SetRenderMode( Sint32  mode )
{
	HMENU hMenu = GetMenu(CWindows::GetInstance()->m_hWindow);
	m_ScreenMode = mode;

	switch (mode) {
	case CGameGirl::enScreenModeOriginal:
		CheckMenuRadioItem(hMenu, IDM_WINDOW_ORIGINAL, IDM_WINDOW_STRETCH, IDM_WINDOW_ORIGINAL, MF_BYCOMMAND);
		break;
	case CGameGirl::enScreenModeAspect:
		CheckMenuRadioItem(hMenu, IDM_WINDOW_ORIGINAL, IDM_WINDOW_STRETCH, IDM_WINDOW_ASPECT, MF_BYCOMMAND);
		break;
	case CGameGirl::enScreenModeFull:
		CheckMenuRadioItem(hMenu, IDM_WINDOW_ORIGINAL, IDM_WINDOW_STRETCH, IDM_WINDOW_STRETCH, MF_BYCOMMAND);
		break;
	}

	gxLib::SaveData.RenderMode  = (Sint8)mode;
}

void CWindows::SetControllMode(gxBool bVPadOn)
{
	HMENU hMenu = GetMenu(CWindows::GetInstance()->m_hWindow);

	if (bVPadOn)
	{
		CheckMenuItem(hMenu, IDM_CONTROLLER, MF_CHECKED);
	}
	else
	{
		CheckMenuItem(hMenu, IDM_CONTROLLER, MF_UNCHECKED);
	}
	m_bVirtualPad = bVPadOn;
	gxLib::SetVirtualPad(bVPadOn);
}

void CWindows::SetFullScreen(gxBool bFullScreen)
{
	HMENU hMenu = GetMenu(CWindows::GetInstance()->m_hWindow);

	if (m_bFullScreen != bFullScreen)
	{
		CGameGirl::GetInstance()->SetNeedReDraw();
	}
	m_bFullScreen = bFullScreen;

	CheckMenuRadioItem(hMenu, IDM_SCREENMODE_WINDOW, IDM_SCREENMODE_FULL, IDM_SCREENMODE_WINDOW+ (bFullScreen? 1:0), MF_BYCOMMAND);

	gxLib::SaveData.SetFlag(gxLib::StSaveData::FULLSCREEN, bFullScreen ? gxTrue : gxFalse );

}

void CWindows::MenuUpdate()
{
	SetFullScreen( gxLib::SaveData.GetFlag(gxLib::StSaveData::FULLSCREEN) );
	SetSoundEnable(gxLib::SaveData.GetFlag(gxLib::StSaveData::ENABLE_SOUND));
	SetRenderMode(gxLib::SaveData.RenderMode );
	SetControllMode(gxLib::SaveData.GetFlag(gxLib::StSaveData::ENABLE_VPAD));
	SetRenderFilter( (CWindows::eSamplingFilter)gxLib::SaveData.RenderFilter);
}

void closeWebView()
{
#ifdef USE_WEBVIEW
	if (webviewController)
	{
		webviewController->Close();
	}
#endif
}

void makeWebView(std::string url )
{
#ifdef USE_WEBVIEW
	static std::string _url;
	_url = url;

	HWND hWnd = CWindows::GetInstance()->m_hWindow;

	// 3 - Create a single WebView2 control in the parent window
// Locate the browser and set up the environment for WebView
	CreateCoreWebView2EnvironmentWithOptions(nullptr, nullptr, nullptr,
		Callback<ICoreWebView2CreateCoreWebView2EnvironmentCompletedHandler>(
			[hWnd](HRESULT result, ICoreWebView2Environment* env) -> HRESULT {

				// Create a CoreWebView2Controller and get the associated CoreWebView2 whose parent is the main window hWnd
				env->CreateCoreWebView2Controller(hWnd, Callback<ICoreWebView2CreateCoreWebView2ControllerCompletedHandler>(
					[hWnd](HRESULT result, ICoreWebView2Controller* controller) -> HRESULT {
						if (controller != nullptr) {
							webviewController = controller;
							webviewController->get_CoreWebView2(&webviewWindow);
						}

						// Add a few settings for the webview
						// The demo step is redundant since the values are the default settings
						ICoreWebView2Settings* Settings;
						webviewWindow->get_Settings(&Settings);
						Settings->put_IsScriptEnabled(TRUE);
						Settings->put_AreDefaultScriptDialogsEnabled(TRUE);
						Settings->put_IsWebMessageEnabled(TRUE);

						// Resize the WebView2 control to fit the bounds of the parent window
						{
							Sint32 w, h;
							RECT bounds;
							GetClientRect(hWnd, &bounds);
							w = bounds.right - bounds.left;
							h = bounds.bottom - bounds.top;

							Sint32 blank = 0;
							blank = w * 0.1f;

							bounds.left += blank;
							bounds.right += -blank;
							bounds.top += blank;
							bounds.bottom += -blank;

							webviewController->put_Bounds(bounds);
						}


						// Schedule an async task to navigate to Bing
						//webviewWindow->Navigate(L"https://www.bing.com/");

						std::string url = _url;
						std::vector<Uint16> ret = CDeviceManager_UTF8toUTF16(url);
						ret.push_back(0);
						webviewWindow->Navigate((LPCWSTR)&ret[0]);
						// 4 - Navigation events

						// 5 - Scripting

						// 6 - Communication between host and web content

						return S_OK;
					}).Get());
				return S_OK;
			}).Get());
#endif
}

void GameConfigDelegate(int n)
{
	switch (n) {
	case 0:
		break;
	case 100:
		CGameGirl::GetInstance()->SetNeedReDraw();
		PostMessage(CWindows::GetInstance()->m_hWindow, WM_COMMAND, IDM_WINDOW_ORIGINAL, 0);
		break;
	case 200:
		CGameGirl::GetInstance()->SetNeedReDraw();
		PostMessage(CWindows::GetInstance()->m_hWindow, WM_COMMAND, IDM_WINDOW_ASPECT, 0);
		break;
	case 300:
		CGameGirl::GetInstance()->SetNeedReDraw();
		PostMessage(CWindows::GetInstance()->m_hWindow, WM_COMMAND, IDM_WINDOW_STRETCH, 0);
		break;
	case 1000:
		CGameGirl::GetInstance()->SetNeedReDraw();
		PostMessage(CWindows::GetInstance()->m_hWindow, WM_COMMAND, IDM_SCREENMODE_FULL, 0);
		break;
	case 1010:
		CGameGirl::GetInstance()->SetNeedReDraw();
		PostMessage(CWindows::GetInstance()->m_hWindow, WM_COMMAND, IDM_SCREENMODE_WINDOW, 0);
		break;
	}
}

