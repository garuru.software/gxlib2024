﻿//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ で生成されたインクルード ファイル。
// gxLib.rc で使用
//
#define IDC_MYICON                      2
#define IDD_WINDOWSPROJECT1_DIALOG      102
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_GXLIB_ICON                  107
#define IDI_SMALL                       108
#define IDC_HELPWINDOW                  109
#define IDM_LICENSE                     110
#define IDM_MANUAL                      111
#define IDM_RESET                       112
#define IDM_WINDOW_DEBUG_OVERVIEW       113
#define IDM_WINDOW_DEBUG_LOG            114
#define IDM_WINDOW_DEBUG_TEXTURE        115
#define IDM_WINDOW_DEBUG_SOUND          116
#define IDM_WINDOW_DEBUG_INPUT          117
#define IDM_SCREENSHOT                  118

#define IDR_MAINFRAME                   128
#define IDD_DIALOG1                     129
#define IDD_DIALOG_LICENSE              130
#define IDC_EDIT1                       1000
#define IDM_WINDOW_ORIGINAL             1001
#define IDM_WINDOW_ASPECT               1002
#define IDM_WINDOW_STRETCH              1003
#define IDM_FULLSCREEN_SWITCH           1200
#define IDM_SCREENMODE_WINDOW           1201
#define IDM_SCREENMODE_FULL             1202
#define IDM_DRAW_BILINEAR               1300
#define IDM_DRAW_NEAREST                1301
#define IDM_DRAW_3D                     1320
#define IDM_WINDOW_SIZE50				1350
#define IDM_WINDOW_SIZE100				1351
#define IDM_WINDOW_SIZE200				1352
#define IDM_SOUND_ON                    1400
#define IDM_SOUND_OFF                   1401
#define IDM_CONTROLLER                  1420
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32772
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
