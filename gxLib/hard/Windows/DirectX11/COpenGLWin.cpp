﻿#include <gxLib.h>
#include <gxLib/gx.h>

#ifdef _USE_OPENGL

#include <gxLib/gxOrderManager.h>
#include <gxLib/gxRender.h>
#include <gxLib/gxTexManager.h>
#include <gxLib/gxDebug.h>
#include "gxLibResource.h"
#include "COpenGLWin.h"

#pragma comment (lib, "opengl32.lib")
#pragma comment (lib, "lib/glew-2.1.0/lib/Release/x64/glew32.lib")

SINGLETON_DECLARE_INSTANCE( COpenGLWin );

const GLchar  *vtxShader1[] = {
	//-1.0f ～ +1.0fの座標で頂点データを受け取る
	"#version 330\n",
	"in   vec4  a_position;\n",
	"in   vec4  a_color;\n",
	"in   vec2  a_texCoord;\n",
	"in   vec2  a_nmlCoord;\n",
	"in   vec2  a_scale;\n",
	"in   vec2  a_offset;\n",
	"in   float a_rotation;\n",
	"in   vec2  a_flip;\n",
	"in   vec4  a_blend;\n",
	"in   vec3  a_pointlight_pos;\n",
	"in   vec3  a_pointlight_rgb;\n",
	"in   float a_pointlight_length;\n",
    "in   vec4 a_options;\n",
	"uniform vec2 u_screen;\n",
	"out  vec4  v_vtxColor;\n",
	"out  vec2  v_texCoord;\n",
	"out  vec2  v_nmlCoord;\n",
	"out  vec4  v_position;\n",
	"out  vec4  v_blend;\n",
	"out   vec3  v_pointlight_pos;\n",
	"out   vec3  v_pointlight_rgb;\n",
	"out   float v_pointlight_length;\n",
    "out   vec4  v_options;\n",
	"void main(void)\n",
	"{\n",
	"v_vtxColor    = a_color;\n",
	"v_texCoord    = a_texCoord;\n",
	"v_nmlCoord    = a_nmlCoord;\n",
	"v_blend       = a_blend;\n",
	"v_pointlight_pos  = a_pointlight_pos;\n",
	"v_pointlight_rgb  = a_pointlight_rgb;\n",
	"v_pointlight_length  = a_pointlight_length;\n",
    "v_options  = a_options;\n",
	"vec2 pos = vec2(0.0 , 0.0);\n",
	"pos.x  = a_position.x * a_scale.x * u_screen.x;\n",	//ここで解像度をかけておかないと回転の時に比率が合わなくなる
	"pos.y  = a_position.y * a_scale.y * u_screen.y;\n",
	"vec2 pos2;\n",
	"pos2.x  = (pos.x * cos( a_rotation )      - pos.y * sin( a_rotation )*-1.0);\n",
	"pos2.y  = (pos.x * sin( a_rotation )*-1.0 + pos.y * cos( a_rotation ) );\n",
	"pos2.x  = pos2.x * a_flip.x;\n",
	"pos2.y  = pos2.y * a_flip.y;\n",
	"vec2 pos3;\n",
	"pos3.x  = pos2.x / u_screen.x + a_offset.x;\n",		//かけておいた解像度を元に戻す
	"pos3.y  = pos2.y / u_screen.y + a_offset.y;\n",
"v_position    = vec4( pos3.x , pos3.y , 0.0 , 0.0 );\n",
	"gl_Position = vec4( pos3.x , pos3.y , 0.0 , 1.0 );\n",
	"}\n",
};

const GLchar  *vtxShaderHighSpeed[] = {
	//-1.0f ～ +1.0fの座標で頂点データを受け取る
	"#version 330\n",
	"in   vec4  a_position;\n",
	"in   vec4  a_color;\n",
	"in   vec2  a_texCoord;\n",
	"in   vec2  a_scale;\n",
	"in   vec2  a_offset;\n",
	"in   float a_rotation;\n",
	"in   vec2  a_flip;\n",
	"in   vec4  a_blend;\n",
	"uniform vec2 u_screen;\n",
	"out  vec4  v_vtxColor;\n",
	"out  vec2  v_texCoord;\n",
	"out  vec4  v_position;\n",
	"out  vec4  v_blend;\n",
	"void main(void)\n",
	"{\n",
	"v_vtxColor    = a_color;\n",
	"v_texCoord    = a_texCoord;\n",
	"v_blend       = a_blend;\n",
	"vec2 pos = vec2(0.0 , 0.0);\n",
	"pos.x  = a_position.x * a_scale.x * u_screen.x;\n",	//ここで解像度をかけておかないと回転の時に比率が合わなくなる
	"pos.y  = a_position.y * a_scale.y * u_screen.y;\n",
	"vec2 pos2;\n",
	"float vcos = cos( a_rotation );\n",
	"float vsin = sin( a_rotation );\n",
	"pos2.x  = (pos.x * vcos      - pos.y * vsin*-1.0);\n",
	"pos2.y  = (pos.x * vsin*-1.0 + pos.y * vcos );\n",
	"pos2.x  = pos2.x * a_flip.x;\n",
	"pos2.y  = pos2.y * a_flip.y;\n",
	"vec2 pos3;\n",
	"pos3.x  = pos2.x / u_screen.x + a_offset.x;\n",		//かけておいた解像度を元に戻す
	"pos3.y  = pos2.y / u_screen.y + a_offset.y;\n",
	"v_position  = vec4( pos3.x , pos3.y , 0.0 , 0.0 );\n",
	"gl_Position = vec4( pos3.x , pos3.y , 0.0 , 1.0 );\n",
	"}\n",
};


//標準シェーダー

const GLchar  *pxlShader1[] = {
		"in vec2 v_texCoord;\n",
		"in vec4 v_vtxColor;\n",
		"in vec4 v_position;\n",
		"in vec4 v_blend;\n",
		"uniform sampler2D u_textureID1;\n",
		"void main(void)\n",
		"{\n",
			"vec4 color = texture2D( u_textureID1 , v_texCoord.xy);\n",
			"vec3 rgb = vec3( color.r , color.g , color.b)*v_vtxColor;\n",
			"vec4 blend = v_blend;\n",
			"rgb = rgb*(1.0 - blend.a) + (vec3(blend.r , blend.g , blend.b)*(blend.a) );\n",
			"gl_FragColor = vec4(rgb.r , rgb.g, rgb.b, color.a)*v_vtxColor.a;\n",
		"}\n",
};


//高速シェーダー
const GLchar  *pxlShaderHighSpeed[] = {
		"in vec2 v_texCoord;\n",
		"in vec4 v_vtxColor;\n",
		"in vec4 v_position;\n",
		"in vec4 v_blend;\n",
		"uniform sampler2D u_textureID1;\n",
		"void main(void)\n",
		"{\n",
			"vec4 color = texture2D( u_textureID1 , v_texCoord.xy);\n",
			"vec3 rgb = vec3( color.r , color.g , color.b)*v_vtxColor;\n",
			"vec4 blend = v_blend;\n",
			"rgb = rgb*(1.0 - blend.a) + (vec3(blend.r , blend.g , blend.b)*(blend.a) );\n",
			"gl_FragColor = vec4(rgb.r , rgb.g, rgb.b, color.a)*v_vtxColor.a;\n",
		"}\n",
};


//白黒シェーダー
const GLchar  *pxlShaderGreyScale[] = {
		"in vec2 v_texCoord;\n",
		"in vec4 v_vtxColor;\n",
		"in vec4 v_position;\n",
		"in vec4 v_blend;\n",
		"uniform float u_option[4];\n",
		"uniform sampler2D u_textureID1;\n",
		"void main(void)\n",
		"{\n",
			"vec4 color = texture2D( u_textureID1 , v_texCoord.xy);\n",
			"float lumin = (color.r + color.g + color.b)/3;\n",
			"float alpha = u_option[0];\n",
			"float ralpha = 1.0-alpha;\n",
			"vec3 rgb_wh = vec3( lumin , lumin , lumin)*alpha;\n",
			"vec3 rgb_fl = vec3( color.r , color.g , color.b)*ralpha;\n",
			"vec3 rgb = vec3( rgb_wh.r+rgb_fl.r , rgb_wh.g+rgb_fl.g , rgb_wh.b+rgb_fl.b)*v_vtxColor;\n",
			"vec4 blend = v_blend;\n",
			"rgb = rgb*(1.0 - blend.a) + (vec3(blend.r , blend.g , blend.b)*(blend.a) );\n",
			"gl_FragColor = vec4(rgb.r , rgb.g, rgb.b, color.a)*v_vtxColor.a;\n",
		"}\n",
};

//Bloomシェーダー
const GLchar  *pxlShader2[] = {
		//Bloomシェーダー
		"in vec2 v_texCoord;\n",
		"in vec4 v_vtxColor;\n",
		"in vec4 v_position;\n",
		"in vec4 v_blend;\n",
//		"in vec4 v_option;\n",
		"uniform float u_option[4];\n",
		"uniform sampler2D u_textureID1;\n",
		"void main(void)\n",
		"{\n",
			"vec4 col0 = texture2D( u_textureID1 , v_texCoord.xy );\n",
			"float fBrightness = max( col0.r , max(col0.g , col0.b ) );\n",

			"if( fBrightness >= u_option[0] ){ \n",
			"	gl_FragColor = col0;\n",
			"}else{\n",
			"	gl_FragColor = vec4(0.0, 0.0, 0.0, 0.0);\n",
			"};\n",
		"}\n",
};


const GLchar  *pxlShader3[] = {
	//GaussianDOFシェーダー
	"in vec2 v_texCoord;\n",
	"in vec4 v_vtxColor;\n",
	"in vec4 v_position;\n",
	"in vec4 v_blend;\n",
//	"in  vec2 v_screen;\n",
//	"in  vec4 v_option;\n",
	"uniform vec2 u_screen;\n",
	"uniform float u_option[4];\n",
	"uniform float u_weight[10];\n",
	"uniform sampler2D u_textureID;\n",
	"void main(void)\n",
	"{\n",
"   	vec2  fc;\n",
"   	vec3  destColor = vec3(0.0);\n",
"		if( u_option[0] < 0.5 )\n",
"		{\n",
"			fc = vec2(gl_FragCoord.x, gl_FragCoord.y);\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x-9.0)/u_screen.x , (fc.y+0.0)/u_screen.y )).rgb*u_weight[9];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x-8.0)/u_screen.x , (fc.y+0.0)/u_screen.y )).rgb*u_weight[8];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x-7.0)/u_screen.x , (fc.y+0.0)/u_screen.y )).rgb*u_weight[7];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x-6.0)/u_screen.x , (fc.y+0.0)/u_screen.y )).rgb*u_weight[6];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x-5.0)/u_screen.x , (fc.y+0.0)/u_screen.y )).rgb*u_weight[5];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x-4.0)/u_screen.x , (fc.y+0.0)/u_screen.y )).rgb*u_weight[4];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x-3.0)/u_screen.x , (fc.y+0.0)/u_screen.y )).rgb*u_weight[3];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x-2.0)/u_screen.x , (fc.y+0.0)/u_screen.y )).rgb*u_weight[2];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x-1.0)/u_screen.x , (fc.y+0.0)/u_screen.y )).rgb*u_weight[1];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x-0.0)/u_screen.x , (fc.y+0.0)/u_screen.y )).rgb*u_weight[0];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+1.0)/u_screen.x , (fc.y+0.0)/u_screen.y )).rgb*u_weight[1];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+2.0)/u_screen.x , (fc.y+0.0)/u_screen.y )).rgb*u_weight[2];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+3.0)/u_screen.x , (fc.y+0.0)/u_screen.y )).rgb*u_weight[3];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+4.0)/u_screen.x , (fc.y+0.0)/u_screen.y )).rgb*u_weight[4];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+5.0)/u_screen.x , (fc.y+0.0)/u_screen.y )).rgb*u_weight[5];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+6.0)/u_screen.x , (fc.y+0.0)/u_screen.y )).rgb*u_weight[6];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+7.0)/u_screen.x , (fc.y+0.0)/u_screen.y )).rgb*u_weight[7];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+8.0)/u_screen.x , (fc.y+0.0)/u_screen.y )).rgb*u_weight[8];\n",
"			gl_FragColor = vec4(destColor, 1.0)*v_vtxColor.a;\n",
"        }else{\n",
"			fc = vec2(gl_FragCoord.x, gl_FragCoord.y);\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+0.0)/u_screen.x , (fc.y-9.0)/u_screen.y )).rgb*u_weight[9];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+0.0)/u_screen.x , (fc.y-8.0)/u_screen.y )).rgb*u_weight[8];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+0.0)/u_screen.x , (fc.y-7.0)/u_screen.y )).rgb*u_weight[7];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+0.0)/u_screen.x , (fc.y-6.0)/u_screen.y )).rgb*u_weight[6];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+0.0)/u_screen.x , (fc.y-5.0)/u_screen.y )).rgb*u_weight[5];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+0.0)/u_screen.x , (fc.y-4.0)/u_screen.y )).rgb*u_weight[4];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+0.0)/u_screen.x , (fc.y-3.0)/u_screen.y )).rgb*u_weight[3];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+0.0)/u_screen.x , (fc.y-2.0)/u_screen.y )).rgb*u_weight[2];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+0.0)/u_screen.x , (fc.y-1.0)/u_screen.y )).rgb*u_weight[1];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+0.0)/u_screen.x , (fc.y-0.0)/u_screen.y )).rgb*u_weight[0];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+0.0)/u_screen.x , (fc.y+1.0)/u_screen.y )).rgb*u_weight[1];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+0.0)/u_screen.x , (fc.y+2.0)/u_screen.y )).rgb*u_weight[2];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+0.0)/u_screen.x , (fc.y+3.0)/u_screen.y )).rgb*u_weight[3];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+0.0)/u_screen.x , (fc.y+4.0)/u_screen.y )).rgb*u_weight[4];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+0.0)/u_screen.x , (fc.y+5.0)/u_screen.y )).rgb*u_weight[5];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+0.0)/u_screen.x , (fc.y+6.0)/u_screen.y )).rgb*u_weight[6];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+0.0)/u_screen.x , (fc.y+7.0)/u_screen.y )).rgb*u_weight[7];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+0.0)/u_screen.x , (fc.y+8.0)/u_screen.y )).rgb*u_weight[8];\n",
"			gl_FragColor = vec4(destColor, 1.0)*v_vtxColor.a;\n",
"        }\n",
	"}\n",

};


//Rasterシェーダー
const GLchar  *pxlShader4[] = {
		"in vec2 v_texCoord;\n",
		"in vec4 v_vtxColor;\n",
		"in vec4 v_position;\n",
		"in vec4 v_blend;\n",
		"uniform float u_option[4];\n",
		"uniform sampler2D u_textureID1;\n",
		"void main(void)\n",
		"{\n",
			"float num    = u_option[0];\n",
			"float range  = u_option[1];\n",
			"float time   = u_option[2];\n",
			"vec2 texCoord = v_texCoord.xy;\n",
			"texCoord.x += cos( (texCoord.y+time)*num)*range;\n",
			"vec4 color = texture2D( u_textureID1 , texCoord.xy);\n",
			"vec3 rgb = vec3( color.r , color.g , color.b)*v_vtxColor;\n",
			"vec4 blend = v_blend;\n",
			"rgb = rgb*(1.0 - blend.a) + (vec3(blend.r , blend.g , blend.b)*(blend.a) );\n",
			"gl_FragColor = vec4(rgb.r , rgb.g, rgb.b, color.a)*v_vtxColor.a;\n",
		"}\n",
};

//法線マップシェーダー

const GLchar* pxlShader5[] = {
		"in vec2 v_texCoord;\n",
		"in vec2 v_nmlCoord;\n",
		"in vec4 v_vtxColor;\n",
		"in vec4 v_position;\n",
		"in vec4 v_blend;\n",
		"in vec3  v_pointlight_pos;\n",
		"in vec3  v_pointlight_rgb;\n",
		"in float v_pointlight_length;\n",
		"uniform sampler2D u_textureID1;\n",
		"uniform sampler2D u_textureID2;\n",
		"uniform sampler2D u_textureID3;\n",
		"void main(void)\n",
		"{\n",
			"vec4 color1  = texture2D( u_textureID1  , v_texCoord.xy);\n",								//アルベド
			"vec4 color2  = texture2D( u_textureID2  , v_texCoord.xy+v_nmlCoord.xy );\n",				//法線
//			"vec3 p1 = vec3( v_pointlight_pos[0] , v_pointlight_pos[1] , v_pointlight_pos[2]);\n",		//ライトのポジション
			"vec3 p1 = vec3( v_pointlight_pos[0] , v_pointlight_pos[1] , 0.1);\n",		//ライトのポジション
//			"vec3 p2 = vec3( v_position[0]       , v_position[1]       , v_position[2]);\n",			//ピクセルのポジション
			"vec3 p2 = vec3( v_position[0]       , v_position[1]       , 0.0);\n",			//ピクセルのポジション
			"vec3 p3 = vec3( p1.x-p2.x , p1.y- p2.y, p1.z - p2.z);\n",									//点光源へのベクトル
			"vec3 p4 = vec3( (color2.r-0.5)*2.0 , (color2.g-0.5)*2.0, (color2.b-0.5)*2.0);\n",			//ピクセルの法線ベクトル
			"vec3 p5 = vec3( v_position[0], v_position[1], v_pointlight_pos[2]);\n",										//ピクセルと点光源の2次元の距離
			//ライトからの距離を算出して光の届く範囲を得る
			"float lightlength = distance(p1 , p5);\n",											//光の届く距離
			"lightlength = 1.0 - clamp(lightlength , 0.0 , v_pointlight_length ) / v_pointlight_length;\n",
			//光源と面の法線ベクトルの内積から傾きdを得る、dは0～1の範囲で暗さとなる
			"vec3 vec3_light   = normalize( p3 );\n",
			"vec3 vec3_surface = normalize( p4 );\n",
			"float d = dot( vec3_light , vec3_surface );\n",
			"d = clamp(d , 0.0 , 1.0 );\n",
			//マテリアルの計算
			"float metalic   = color2.a;\n",														//メタリック＝映り込み（最大0.5）
			"float roughness = 1.0-metalic;\n",															//ラフネス  = 光沢

			//光沢（明るいところはより明るく、暗いところはより暗くする）
			"vec3 specular = metalic*vec3( 1.0 , 1.0 , 1.0)*(d*pow(d,20.0*(1.0-roughness)));\n",				//スペキュラーの強さ（ラフネスから影響を受ける）

			//映り込みの場所を得る
			"vec2 texel;\n",
			"texel.x = (v_position.x+1.0)/2 + (1.0+vec3_surface.x)/2.0;\n",
			"texel.y = (v_position.y+1.0)/2 + (1.0+vec3_surface.y)/2.0;\n",
			"texel.x = texel.x/2.0;\n",
			"texel.y = texel.y/2.0;\n",
			"texel.x = (v_position[0]+1.0*(p4.x)/2.0);\n",	
			"texel.y = (v_position[1]+1.0*(p4.y)/2.0);\n",
			"texel.x = clamp(texel.x , -1.0 , 1.0);\n",
			"texel.y = clamp(texel.y , -1.0 , 1.0);\n",
			"texel.x = (texel.x+1.0)/2.0;\n",
			"texel.y = (texel.y+1.0)/2.0;\n",
			"vec4 env = texture2D( u_textureID3  , texel.xy );\n",										//環境テクスチャ
			//"env = vec4(v_pointlight_length,v_pointlight_length,v_pointlight_length,1.0);\n",										//環境テクスチャ

			//環境テクスチャ
			"float a1n = metalic*1.0;\n",
			"float a1r = 1.0-a1n;\n",
			"vec3 rgb = vec3( color1.r*a1r + env.r*a1n , color1.g*a1r+env.g*a1n , color1.b*a1r +env.b*a1n)*v_vtxColor.rgb;\n",		//色：（アルベド＋映り込み）＊頂点カラー（先に頂点カラーをかけないと色がついたスペキュラになる）
			"rgb = rgb*d*lightlength;\n",																							//影：法線方向による陰影と光の減衰率を掛け合わせる
			"rgb = rgb+specular;\n",																								//光：スペキュラ成分は最後に足す
			"gl_FragColor = vec4( rgb.x , rgb.y, rgb.z, color1.a)*v_vtxColor.a;\n",													//α：最後に半透明度合いを計算する
			//"gl_FragColor = vec4( (env.r+specular.r)*lightlength , (env.g+specular.g)*lightlength, (env.b+specular.b)*lightlength, color1.a)*v_vtxColor.a;\n",													//最後に半透明度合いを計算する
			//"vec3 rgb = vec4( env.r , env.g, env.b, color1.a)*v_vtxColor.a;\n",													//最後に半透明度合いを計算する
			//液体金属にしたい場合は法線マップを無効にする
			//"rgb = (rgb + specular)*d*lightlength;\n",																			//法線方向による影と光の減衰率を掛け合わせる
		"}\n",
};

//フォントシェーダー
const GLchar  *pxlShader6[] = {
    "in vec2 v_texCoord;\n",
    "in vec4 v_vtxColor;\n",
    "in vec4 v_blend;\n",
    "in vec4 v_options;\n",
    "uniform sampler2D u_textureID1;\n",
    "void main(void)\n",
    "{\n",
    "vec4 color1 = texture2D( u_textureID1 , v_texCoord.xy);\n",
    "vec4 color2 = vec4( v_blend.r , v_blend.g , v_blend.b , 1.0 );\n",
    "float mode = v_options[0];\n",
    "float alpha = color1.r;\n",
    "if(mode == 1.0) alpha = color1.g;\n",
    "if(mode == 2.0) alpha = color1.b;\n",
	"if(mode == 3.0) alpha = color1.a;\n",
    "gl_FragColor = vec4( 1.0 , 1.0 , 1.0 , alpha )*v_vtxColor;\n",
	"}\n",
};

const GLchar* pxlShader0[] = {
		"in vec2 v_texCoord;\n",
		"in vec4 v_vtxColor;\n",
		"in vec4 v_position;\n",
		"in vec4 v_blend;\n",
		"uniform sampler2D u_textureID1;\n",
		"uniform sampler2D u_texNormal;\n",
		"uniform float u_PLight[10];\n",
		"void main(void)\n",
		"{\n",
			"vec4 color1  = texture2D( u_textureID1 , v_texCoord.xy);\n",
			"vec4 color2  = texture2D( u_texNormal  , v_texCoord.xy);\n",
			"vec3 p1 = vec3( u_PLight[0] , u_PLight[1] , u_PLight[2]);\n",
			"vec3 p2 = vec3( v_position[0] , v_position[1] , v_position[2]);\n",
			"vec3 p3 = vec3( p1.x-p2.x , p1.y- p2.y, p1.z - p2.z);\n",		//点光源へのベクトル
			"vec3 p4 = vec3( (color2.r-0.5)*2.0 , (color2.g-0.5)*2.0, (color2.b-0.5)*2.0);\n",			//面の法線ベクトル
			"vec3 vec3_light   = normalize( p3 );\n",
			"vec3 vec3_surface = normalize( p4 );\n",
			"float d = dot( vec3_light , vec3_surface );\n",
			"d = clamp(d , 0.0 , 1.0 );\n",
			"float l = 0.0;\n",////length( vec3(p2.x-p1.x , p2.y-p1.y , 0.0) );\n",
			"d = clamp(d , 0.0 , 1.0 )*(1.0-l);\n",
			"vec3 rgb = vec3( color1.r , color1.g , color1.b)*d;\n",
			"gl_FragColor = vec4( rgb.x , rgb.y, rgb.z, 1.0)*v_vtxColor.a;\n",
		"}\n",
};

COpenGLWin::COpenGLWin()
{
	m_FrameBufferIndex = 0;
	m_SizeOfVertexShader[0] = sizeof(vtxShaderHighSpeed)/ sizeof(vtxShaderHighSpeed[0]);
	m_SizeOfVertexShader[1] = sizeof(vtxShader1)/ sizeof(vtxShader1[0]);

	m_SizeOfPixelShader[0] = sizeof(pxlShader0)/ sizeof(pxlShader0[0]);
	m_SizeOfPixelShader[1] = sizeof(pxlShader1) / sizeof(pxlShader1[0]);
	m_SizeOfPixelShader[2] = sizeof(pxlShader2) / sizeof(pxlShader2[0]);
	m_SizeOfPixelShader[3] = sizeof(pxlShader3) / sizeof(pxlShader3[0]);
	m_SizeOfPixelShader[4] = sizeof(pxlShader4) / sizeof(pxlShader4[0]);
	m_SizeOfPixelShader[5] = sizeof(pxlShader5) / sizeof(pxlShader5[0]);
	m_SizeOfPixelShader[6] = sizeof(pxlShader6) / sizeof(pxlShader6[0]);
	m_SizeOfPixelShader[7] = sizeof(pxlShaderHighSpeed) / sizeof(pxlShaderHighSpeed[0]);
	m_SizeOfPixelShader[8] = sizeof(pxlShaderGreyScale) / sizeof(pxlShaderGreyScale[0]);
}


COpenGLWin::~COpenGLWin()
{

}


void COpenGLWin::Init()
{
	//------------------------------------------------------
	// 頂点シェーダー ファイルを読み込んだ後、シェーダーと入力レイアウトを作成します。
	//------------------------------------------------------

	int pixelFormat;

	static PIXELFORMATDESCRIPTOR pfd = {
		sizeof(PIXELFORMATDESCRIPTOR),   // size of this pfd 
		1,                     // version number 
		PFD_DRAW_TO_WINDOW |   // support window 
		PFD_SUPPORT_OPENGL |   // support OpenGL 
		PFD_DOUBLEBUFFER,      // double buffered 
		PFD_TYPE_RGBA,         // RGBA type 
		24,                    // 24-bit color depth 
		0, 0, 0, 0, 0, 0,      // color bits ignored 
		0,                     // no alpha buffer 
		0,                     // shift bit ignored 
		0,                     // no accumulation buffer 
		0, 0, 0, 0,            // accum bits ignored 
		32,                    // 32-bit z-buffer 
		1,                     // no stencil buffer 
		1,                     // no auxiliary buffer 
		PFD_MAIN_PLANE,        // main layer 
		0,                     // reserved 
		0, 0, 0                // layer masks ignored 
	};

	pixelFormat = ChoosePixelFormat( CWindows::GetInstance()->m_WinDC, &pfd);
	SetPixelFormat(CWindows::GetInstance()->m_WinDC, pixelFormat, &pfd);

	m_hRC = wglCreateContext(CWindows::GetInstance()->m_WinDC );
	wglMakeCurrent( CWindows::GetInstance()->m_WinDC, m_hRC);

	GLenum err = glewInit();

	//	行列
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	//Reset
	glViewport(0, 0, m_GameW, m_GameH);

	BOOL(WINAPI *wglSwapIntervalEXT)(int) = NULL;

	wglSwapIntervalEXT = (BOOL(WINAPI*)(int))wglGetProcAddress("wglSwapIntervalEXT");

	if (wglSwapIntervalEXT)
	{
		wglSwapIntervalEXT(0);
	}

	int MaxUnitNum;
/*
	glGetIntegerv(GL_MAX_TEXTURE_UNITS,&MaxUnitNum);
	GX_DEBUGLOG( "GL_MAX_TEXTURE_UNITS = %d", MaxUnitNum );			//OpenGL 3で非推奨

	glGetIntegerv(GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS,&MaxUnitNum);
	GX_DEBUGLOG( "GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS = %d", MaxUnitNum );

	glGetIntegerv(GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS,&MaxUnitNum);
	GX_DEBUGLOG( "GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS = %d", MaxUnitNum );

	glGetIntegerv(GL_MAX_TEXTURE_IMAGE_UNITS,&MaxUnitNum);
	GX_DEBUGLOG( "GL_MAX_TEXTURE_IMAGE_UNITS = %d", MaxUnitNum );
*/

	glGetIntegerv(GL_MAX_TEXTURE_IMAGE_UNITS_ARB,&MaxUnitNum);
	GX_DEBUGLOG( "GL_MAX_TEXTURE_IMAGE_UNITS_ARB = %d", MaxUnitNum );

	struct stGPU
	{
		Uint32 maxSpeed;
		Uint32 maxRam;
		Uint32 maxTextureUnit;
	};
	//gxDebug::GetInstance()->SetGPUPerformance();
/*
	err = glGetError();
	GLuint texture2D;

	Uint8 *pData = (Uint8*)malloc(1024*1024*4);

	glActiveTexture(GL_TEXTURE0 + 0);
	for(Sint32 ii=0; ii<1024*1024; ii++ )
	{
		glGenTextures( 1, &texture2D );
		glBindTexture( GL_TEXTURE_2D, texture2D );
		glPixelStorei( GL_UNPACK_ALIGNMENT, 1);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 1024, 1024, 0, GL_RGBA, GL_UNSIGNED_BYTE, pData );
		err = glGetError();
		if( err )
		{
			err = err;
		}
	}

	free(pData);
*/

	glActiveTexture(GL_TEXTURE0 );

	initShader();

	initTexture();

	initVBO();

	Reset();

		glEnable ( GL_TEXTURE_2D );
/*毒*///		glEnable ( GL_NORMALIZE );
/*毒*///		glEnable ( GL_ALPHA_TEST );
		glEnable ( GL_BLEND );
		glDisable( GL_DEPTH_TEST );

	m_bInitCompleted = gxTrue;

}


// このメソッドは、CoreWindow オブジェクトが作成 (または再作成) されるときに呼び出されます。
void COpenGLWin::Reset()
{
	//フルスクリーン切り替え

	DEVMODE devmode;

	EnumDisplaySettings(NULL, 0, &devmode);

	if( !CDeviceManager::GetInstance()->IsBatchMode() )
	{
		if( CWindows::GetInstance()->IsFullScreen() )
		{
			RECT rect;
			HWND hDeskWnd = GetDesktopWindow(); //この関数でデスクトップのハンドルを取得
			GetWindowRect(hDeskWnd, &rect); //デスクトップのハンドルからその(画面の)大きさを取得

			GetWindowRect(CWindows::GetInstance()->m_hWindow, &CWindows::GetInstance()->m_WinRect);

			Sint32 w,h;
			w = rect.right - rect.left;
			h = rect.bottom - rect.top;

			SetMenu           ( CWindows::GetInstance()->m_hWindow , NULL );
			SetWindowLong     ( CWindows::GetInstance()->m_hWindow , GWL_STYLE, WS_POPUP|WS_VISIBLE|WS_EX_TOPMOST );
			MoveWindow        (	CWindows::GetInstance()->m_hWindow , 0,0, w , h , true	);

			{
				RECT rc;
				SetRect(&rc, 0, 0, w, h );
				CWindows::GetInstance()->m_WinRect;
				ClipCursor( &rc );
				UpdateWindow( CWindows::GetInstance()->m_hWindow );

				while( ShowCursor(FALSE)>0 );

			}

			return;
		}
		else
		{
			SetMenu( CWindows::GetInstance()->m_hWindow, LoadMenu(CWindows::GetInstance()->m_hInstance , MAKEINTRESOURCE( IDC_HELPWINDOW)) );

			//Windowの大きさを元に戻す
			SetWindowLong( CWindows::GetInstance()->m_hWindow , GWL_STYLE  , CWindows::GetInstance()->m_AppStyle );
			SetWindowPos ( CWindows::GetInstance()->m_hWindow,
			               HWND_NOTOPMOST,
			               CWindows::GetInstance()->m_WinRect.left   , CWindows::GetInstance()->m_WinRect.top,
						   CWindows::GetInstance()->m_WinRect.right  - CWindows::GetInstance()->m_WinRect.left,
						   CWindows::GetInstance()->m_WinRect.bottom - CWindows::GetInstance()->m_WinRect.top,  SWP_SHOWWINDOW );

			ChangeDisplaySettings( NULL , 0 );
			ClipCursor( NULL );
			UpdateWindow( CWindows::GetInstance()->m_hWindow );
			ShowCursor(TRUE);
		}
	}


	Sint32 gamew = m_GameW, gameh = m_GameH;
	Sint32 winw  = m_GameW, winh  = m_GameH;

	CGameGirl::GetInstance()->GetGameResolution(&gamew, &gameh);
	CGameGirl::GetInstance()->GetWindowsResolution(&winw, &winh);

	m_BackBufferSize.TopLeftX = 0;
	m_BackBufferSize.TopLeftY = 0;
	m_BackBufferSize.Width  = winw;
	m_BackBufferSize.Height = winh;

}

void COpenGLWin::SwapBuffer()
{
	SwapBuffers( CWindows::GetInstance()->m_WinDC );
}

void COpenGLWin::GetFrameBufferImage(gxChar* pFileName)
{
	// フレームバッファの内容をPBOに転送

	// PBO作成とバインド
	Uint32 length = m_GameW * m_GameH * 3;

#if 0
	GLuint pbo;


	glGenBuffers(1, &pbo);
	glBindBuffer(GL_PIXEL_UNPACK_BUFFER_ARB, pbo);

	glBufferData(GL_PIXEL_UNPACK_BUFFER_ARB, *pLength, 0, GL_DYNAMIC_DRAW);    // バッファの作成と初期化

	glBindBuffer(GL_PIXEL_UNPACK_BUFFER_ARB, 0);

	glBindBuffer(GL_PIXEL_PACK_BUFFER_ARB, pbo);    // PBOをアクティブにする

	glReadPixels(0, 0, m_GameW, m_GameH, GL_BGRA, GL_UNSIGNED_BYTE, 0);     // 描画をPBOにロード

	// PBOの内容を編集
	GLubyte* ptr = (GLubyte*)glMapBuffer(GL_PIXEL_PACK_BUFFER, GL_READ_WRITE);
	glUnmapBufferARB(GL_PIXEL_PACK_BUFFER_ARB);
	glBindBuffer(GL_PIXEL_PACK_BUFFER_ARB, 0);
#endif

	GLubyte* ptr = (GLubyte*)new GLubyte[length];
	glBindFramebuffer(GL_FRAMEBUFFER, m_OffScreenTexture[ enGamePostProcess0 + m_CurrentPage ].frameBufferObject);

	glReadBuffer(GL_FRONT);

	// OpenGLで画面に描画されている内容をバッファに格納
	glReadPixels(
		0,                 //読み取る領域の左下隅のx座標
		0,                 //読み取る領域の左下隅のy座標 //0 or getCurrentWidth() - 1
		m_GameW,          //読み取る領域の幅
		m_GameH,          //読み取る領域の高さ
		GL_BGR, //it means GL_BGR,           //取得したい色情報の形式
		GL_UNSIGNED_BYTE,  //読み取ったデータを保存する配列の型
		ptr      //ビットマップのピクセルデータ（実際にはバイト配列）へのポインタ
	);

	if (ptr)
	{
		Uint32 argb;
		CFileTarga tga;
		tga.Create(m_GameW, m_GameH, 32);
		Uint32 n = 0;
		for (Uint32 yy = 0; yy < m_GameH; yy++)
		{
			for (Uint32 xx = 0; xx < m_GameW; xx++)
			{
				argb = ARGB( 0xff , ptr[3*n+2], ptr[3*n+1], ptr[3*n+0] );
				tga.SetARGB(xx, m_GameH - yy -1 , argb);
				n++;
			}
		}
		tga.SaveFile(pFileName);
	}

	SAFE_DELETES(ptr);
}

#endif
