
#include <gxLib.h>
#include <gxLib/gx.h>
#include <gxLib/gxOrderManager.h>
#include <gxLib/gxRender.h>
#include <gxLib/gxTexManager.h>
#include <gxLib/gxDebug.h>

#ifdef _USE_OPENGL
#include "COpenGLES3.h"
extern const GLchar *vtxShader1[];
extern const GLchar *vtxShaderHighSpeed[];

extern const GLchar  *pxlShader1[];
extern const GLchar  *pxlShader2[];
extern const GLchar  *pxlShader3[];
extern const GLchar  *pxlShader4[];
extern const GLchar* pxlShader5[];
extern const GLchar  *pxlShader6[];
extern const GLchar* pxlShader0[];
extern const GLchar* pxlShaderHighSpeed[];
extern const GLchar* pxlShaderGreyScale[];

#define USE_FLIP
#define FINAL_BUFFER_WIDTH (1024*2)

COpenGLES3::COpenGLES3()
{
	m_bInitCompleted = gxFalse;

//	m_bUpConvert = gxFalse;
	m_bUpConvert = gxTrue;

	m_b3DView = gxFalse;

	m_GameW = WINDOW_W;
	m_GameH = WINDOW_H;

	m_pVertexBuffer = NULL;
	m_pIndexBuffer  = NULL;

	m_ShadingWindowW = m_GameW;
	m_ShadingWindowH = m_GameH;

	m_FrameBufferIndex = -1;
}


COpenGLES3::~COpenGLES3()
{
	for( Sint32 ii=0; ii<enTexturePageMax; ii++ )
	{
		if( m_TextureData[ ii ].texture2D )
		{
		//	m_TextureData[ ii ].texture2D->Release();
		}

		//if( m_TextureData[ ii ].shaderResourceView )
		//{
		//	m_TextureData[ ii ].shaderResourceView->Release();
		//}

		//if( m_TextureData[ ii ].renderTargetView )
		//{
		//	m_TextureData[ ii ].renderTargetView->Release();
		//}
	}
	SAFE_DELETES(m_pVertexBuffer);
	SAFE_DELETES(m_pIndexBuffer);

/*
	if( m_inputLayout )  m_inputLayout->Release();
	if( m_vertexBuffer ) m_vertexBuffer->Release();
	if( m_indexBuffer )  m_indexBuffer->Release();
	if( m_vertexShader ) m_vertexShader->Release();
	if( m_pixelShader )  m_pixelShader->Release();
	if( m_constantBuffer )  m_constantBuffer->Release();
*/
}

void COpenGLES3::Init()
{
	//------------------------------------------------------
	// 頂点シェーダー ファイルを読み込んだ後、シェーダーと入力レイアウトを作成します。
	//------------------------------------------------------
	//Reset
	glViewport(0, 0, m_GameW, m_GameH);

//	display= eglGetDisplay( EGL_DEFAULT_DISPLAY );
//    surface = eglGetCurrentSurface(EGL_READ);
//
//    //if( EGL_NO_SURFACE == surface )
//    {
//        surface = eglGetCurrentSurface(EGL_DRAW);
//    }

    _CheckError();

	glActiveTexture(GL_TEXTURE0 );

	initShader();

	initTexture();

	initVBO();

	Reset();

		glEnable ( GL_TEXTURE_2D );
/*毒*///		glEnable ( GL_NORMALIZE );
/*毒*///		glEnable ( GL_ALPHA_TEST );
		glEnable ( GL_BLEND );
		glDisable( GL_DEPTH_TEST );

	m_bInitCompleted = gxTrue;

}


// このメソッドは、CoreWindow オブジェクトが作成 (または再作成) されるときに呼び出されます。
void COpenGLES3::Reset()
{
	//フルスクリーン切り替え

	Sint32 gamew = m_GameW, gameh = m_GameH;
	Sint32 winw  = m_GameW, winh  = m_GameH;

	CGameGirl::GetInstance()->GetGameResolution(&gamew, &gameh);
	CGameGirl::GetInstance()->GetWindowsResolution(&winw, &winh);

	m_BackBufferSize.TopLeftX = 0;
	m_BackBufferSize.TopLeftY = 0;
	m_BackBufferSize.Width  = winw;
	m_BackBufferSize.Height = winh;

}


GLuint COpenGLES3::compileShader( GLchar const* const* vtxShader , size_t sz_vtx , GLchar const* const* pxlShader ,size_t sz_pxl )
{
	GLint status;
	GLsizei bufSize;
	GLsizei length;
	GLchar *infoLog;

	GLuint  vshader = glCreateShader(GL_VERTEX_SHADER);;
	GLuint  pshader = glCreateShader( GL_FRAGMENT_SHADER );

	glShaderSource ( vshader, (GLsizei)sz_vtx, vtxShader, NULL);
	glCompileShader( vshader );
	glGetShaderiv  ( vshader, GL_COMPILE_STATUS, &status );

	if (status == GL_FALSE)
	{
		glGetShaderiv( vshader, GL_INFO_LOG_LENGTH, &bufSize);
		infoLog = (GLchar*)malloc( bufSize );
		glGetShaderInfoLog( vshader, bufSize, &length, infoLog );
		gxLib::DebugLog("[vtx ShaderError] %s", infoLog );
		free(infoLog);
		return -1;
	}

	glShaderSource ( pshader, (GLsizei)sz_pxl, pxlShader, NULL );
	glCompileShader( pshader );
	glGetShaderiv  ( pshader, GL_COMPILE_STATUS, &status );

	if (status == GL_FALSE)
	{
		glGetShaderiv( pshader, GL_INFO_LOG_LENGTH, &bufSize);
		infoLog = (GLchar*)malloc( bufSize );
		glGetShaderInfoLog( pshader, bufSize, &length, infoLog );
		gxLib::DebugLog("[pxl ShaderError] %s", infoLog );
		free(infoLog);
		return -1;
	}

	//attach prog program

	GLuint prog = glCreateProgram();
	glAttachShader( prog , vshader );
	glAttachShader( prog , pshader );

	//link shader program

	glLinkProgram ( prog );
	glGetProgramiv( prog, GL_LINK_STATUS, &status);

	if (status == GL_FALSE)
	{
		glGetProgramiv(prog, GL_INFO_LOG_LENGTH, &bufSize );
		infoLog = (GLchar*)malloc( bufSize );
		glGetProgramInfoLog( prog, bufSize, &length, infoLog );
		gxLib::DebugLog("[Shader LinkError] %s", infoLog );
		free(infoLog);
		return -1;
	}

	glDeleteShader( vshader );
	glDeleteShader( pshader );

	return prog;
}


void COpenGLES3::initShader()
{
	//----------------------------------------------------------------------
	// init shader
	//----------------------------------------------------------------------

	m_ShaderProg[ enShaderDefault ] = compileShader(
		(GLchar const* const*)vtxShader1, m_SizeOfVertexShader[1],
		(GLchar const* const*)pxlShader1, m_SizeOfPixelShader[1]);

	m_ShaderProg[ enShaderBloom  ] = compileShader(
		(GLchar const* const*)vtxShader1, m_SizeOfVertexShader[1],
		(GLchar const* const*)pxlShader2, m_SizeOfPixelShader[2] );

	m_ShaderProg[ enShaderBlur   ] = compileShader(
		(GLchar const* const*)vtxShader1, m_SizeOfVertexShader[1],
		(GLchar const* const*)pxlShader3, m_SizeOfPixelShader[3]);

	m_ShaderProg[ enShaderRaster ] = compileShader(
		(GLchar const* const*)vtxShader1, m_SizeOfVertexShader[1],
		(GLchar const* const*)pxlShader4, m_SizeOfPixelShader[4]);

	m_ShaderProg[enShaderNormal] = compileShader(
		(GLchar const* const*)vtxShader1, m_SizeOfVertexShader[1],
		(GLchar const* const*)pxlShader5, m_SizeOfPixelShader[5]);

    m_ShaderProg[enShaderFont] = compileShader(
         (GLchar const* const*)vtxShader1, m_SizeOfVertexShader[1],
         (GLchar const* const*)pxlShader6, m_SizeOfPixelShader[6]);
	
	m_ShaderProg[enShaderDev] = compileShader(
		(GLchar const* const*)vtxShader1, m_SizeOfVertexShader[1],
		(GLchar const* const*)pxlShader0, m_SizeOfPixelShader[0]);
	
	m_ShaderProg[enShaderHighSpeed] = compileShader(
		(GLchar const* const*)vtxShaderHighSpeed, m_SizeOfVertexShader[0],
		(GLchar const* const*)pxlShaderHighSpeed, m_SizeOfPixelShader[7]);

    m_ShaderProg[enShaderGreyScale] = compileShader(
		(GLchar const* const*)vtxShader1, m_SizeOfVertexShader[1],
		(GLchar const* const*)pxlShaderGreyScale, m_SizeOfPixelShader[8]);


}


void COpenGLES3::initTexture()
{
	//----------------------------------------------------------------------
	//テクスチャ生成
	//----------------------------------------------------------------------

	Sint32 w, h;

	//通常テクスチャを生成

	w = gxTexManager::enMasterWidth;
	h = gxTexManager::enMasterHeight;

	//for (Sint32 ii = 0; ii < enTexturePageMax; ii++)
	//{
	//	//必要なときにだけ作ることにする
	//	makeTexture(&m_TextureData[ii] , w , h ,32 );
	//}

	//ノンテクスチャポリゴン用テクスチャという矛盾したテクスチャ
	Uint8 *pBasicTex = new Uint8[32*32*4];

	gxUtil::MemSet( pBasicTex , 0xFF , 32*32*4 );

	makeTexture( &m_OffScreenTexture[ enBasicBuff ] , 32 , 32 ,32 , pBasicTex );

	SAFE_DELETES( pBasicTex );

	//壁紙用テクスチャを生成

	makeTexture(&m_OffScreenTexture[ enWallPaper ], 256, 256);

	//フレームバッファ用テクスチャを生成

	w = m_GameW;
	h = m_GameH;

	if (m_bUpConvert)
	{
		w = 2048;
		h = 2048;
	}

	makeTexture(&m_OffScreenTexture[enGameScreen0], w, h,24);
	makeTexture(&m_OffScreenTexture[enGameScreen1], w, h,24);
	makeTexture(&m_OffScreenTexture[enGameScreen2], w, h,24);
	makeTexture(&m_OffScreenTexture[enCaptureScreen], w, h,24);
	makeTexture(&m_OffScreenTexture[enGamePostProcess0], w, h,24);
	makeTexture(&m_OffScreenTexture[enGamePostProcess1], w, h,24);
	makeTexture(&m_OffScreenTexture[enGamePostProcess2], w, h,24);
	makeTexture(&m_OffScreenTexture[enFinalBuffer], FINAL_BUFFER_WIDTH, FINAL_BUFFER_WIDTH , 24);

	createFrameBuffer(&m_OffScreenTexture[enGameScreen0]);
	createFrameBuffer(&m_OffScreenTexture[enGameScreen1]);
	createFrameBuffer(&m_OffScreenTexture[enGameScreen2]);
	createFrameBuffer(&m_OffScreenTexture[enCaptureScreen]);
	createFrameBuffer(&m_OffScreenTexture[enGamePostProcess0]);
	createFrameBuffer(&m_OffScreenTexture[enGamePostProcess1]);
	createFrameBuffer(&m_OffScreenTexture[enGamePostProcess2]);

	createFrameBuffer(&m_OffScreenTexture[enFinalBuffer]);

	//restore frame buffer state
	glBindFramebuffer( GL_FRAMEBUFFER, m_FrameBufferIndex );
	glBindTexture( GL_TEXTURE_2D, 0 );

}

void COpenGLES3::initVBO()
{
	//頂点バッファを作成

	SAFE_DELETES( m_pVertexBuffer );
	SAFE_DELETES( m_pIndexBuffer );

	m_pVertexBuffer = new Uint8[ enVertexBufferSize ];
	m_pIndexBuffer  = new Uint8[ enIndexBufferSize ];

	glGenBuffers(1, &m_vertexBufferObject );
	glBindBuffer(GL_ARRAY_BUFFER, m_vertexBufferObject);
	glBufferData(GL_ARRAY_BUFFER, enVertexBufferSize, m_pVertexBuffer, GL_STREAM_DRAW );

	glGenBuffers(1, &m_indexBufferObject );
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_indexBufferObject);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, enIndexBufferSize, m_pIndexBuffer, GL_STREAM_DRAW );

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

}



void COpenGLES3::createFrameBuffer( TextureData *pTextureData )
{
	// フレームバッファオブジェクトを作成する

	if( pTextureData->frameBufferObject != -1 )
	{
		//既に作成されていたら削除する
		glDeleteFramebuffers( 1 , &pTextureData->frameBufferObject );
	}

	glGenFramebuffers(1, &pTextureData->frameBufferObject );
	glBindFramebuffer( GL_FRAMEBUFFER, pTextureData->frameBufferObject );

	// フレームバッファオブジェクトにカラーバッファとしてテクスチャを結合する
	glFramebufferTexture2D( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, pTextureData->texture2D, 0 );

	// フレームバッファオブジェクトを解除する
	glBindFramebuffer( GL_FRAMEBUFFER, m_FrameBufferIndex );
	glBindTexture( GL_TEXTURE_2D, 0 );
}

void COpenGLES3::Update()
{
	static int seq = 0;

	if( seq == 0 )
	{
		//一度だけ作る
		makeWallPaper();
		seq ++;
	}

	m_b3DView = gxFalse;//CGameGirl::GetInstance()->Is3DView();
}


void COpenGLES3::Render()
{
    m_GameW = WINDOW_W;
    m_GameH = WINDOW_H;

    Sint32 gamew = m_GameW,gameh = m_GameH;
    Sint32 winw  = m_GameW,winh  = m_GameH;
    
    glDisable ( GL_SCISSOR_TEST );
    m_bScissorEnable = gxFalse;
    
    CGameGirl::GetInstance()->GetGameResolution(&gamew, &gameh);
    CGameGirl::GetInstance()->GetWindowsResolution(&winw, &winh);

    // if(m_FrameBufferIndex == -1)
    {
        glGetIntegerv(GL_FRAMEBUFFER_BINDING, &m_FrameBufferIndex);
    }

	//背景色をクリア

	StViewPort viewport;

	m_ShadingWindowW = m_GameW;
	m_ShadingWindowH = m_GameH;

	Uint32 _argb = gxRender::GetInstance()->GetBgColor();
	Float32 rgba[] = {
		((_argb >> 16) & 0xff) / 255.0f,
		((_argb >> 8) & 0xff)  / 255.0f,
		((_argb >> 0) & 0xff)  / 255.0f,
		((_argb >> 24) & 0xff) / 255.0f,
	};

	//-----------------------------------
	//game
	//-----------------------------------

	//レンダーターゲットをゲーム画面用テクスチャにする
	glBindFramebuffer( GL_FRAMEBUFFER, m_OffScreenTexture[ enGamePostProcess0 + m_CurrentPage ].frameBufferObject );

	if( rgba[0] + rgba[1] + rgba[2] )
	{
		glClearColor( rgba[0], rgba[1], rgba[2], rgba[3] );
		glClear(GL_COLOR_BUFFER_BIT);
	}

	Float32 fLR = 3.0f;

	viewport.TopLeftX = 0.0f;
	viewport.TopLeftY = 0.0f;
	viewport.Width    = m_ShadingWindowW;
	viewport.Height   = m_ShadingWindowH;


	if( m_bUpConvert )
	{
		viewport.Width    = 2048;
		viewport.Height   = 2048;
	}


	glViewport( 0, 0, viewport.Width , viewport.Height );
	glDisable ( GL_SCISSOR_TEST );
	m_bScissorEnable = gxFalse;

	render();

	//-----------------------------------
	//system
	//-----------------------------------

	glDisable ( GL_SCISSOR_TEST );
	m_bScissorEnable = gxFalse;

	viewport.TopLeftX = 0.0f;
	viewport.TopLeftY = 0.0f;


#ifdef USE_FLIP
	m_ShadingWindowW = FINAL_BUFFER_WIDTH;
	m_ShadingWindowH = FINAL_BUFFER_WIDTH;
	viewport.Width    = FINAL_BUFFER_WIDTH;
	viewport.Height   = FINAL_BUFFER_WIDTH;
#else
	viewport.Width    = winw;//m_BackBufferSize.Width;
	viewport.Height   = winh;//m_BackBufferSize.Height;
	m_ShadingWindowW = winw;
	m_ShadingWindowH = winh;
#endif

	glViewport( 0, 0, viewport.Width , viewport.Height );
	renderSystem();

	SwapBuffer();


}


void COpenGLES3::render()
{
	if( !m_bInitCompleted ) return;

	gxDebug::GetInstance()->ResetDrawCallCnt();

    m_ShadingWindowW = m_GameW;
	m_ShadingWindowH = m_GameH;

	//GLの最終座標
	float glX1, glY1;
	float glX2, glY2;

	Sint32 gamew = m_GameW,gameh = m_GameH;
	Sint32 winw  = m_GameW,winh  = m_GameH;

	CGameGirl::GetInstance()->GetGameResolution   ( &gamew , &gameh );
	CGameGirl::GetInstance()->GetWindowsResolution( &winw  , &winh   );

	glX1 = 0.0f;
	glY1 = 0.0f;

	glX2 = 2.0f * gamew / winw;
	glY2 = 2.0f * gameh / winh;

	glX1 = -glX2 /2.0f;
	glY1 =  glY2 /2.0f;
	glX2 =  glX2 /2.0f;
	glY2 = -glY2 /2.0f;

	{
		Sint32 sCommandMax      = gxRender::GetInstance()->GetCommandNum();
		StCustomVertex *pVertex = gxRender::GetInstance()->GetVertex(0);

		Sint32 indexUnitSize = 4;

		Uint32 vtx_max = gxRender::GetInstance()->GetVertexNum();
		Uint32 idx_max = gxRender::GetInstance()->GetIndexNum();

		size_t vram_draw_max = enVertexBufferSize / sizeof(VertexPositionColorTexCoord);
		if (vtx_max >= vram_draw_max - 32)
		{
			vtx_max = (Uint32)vram_draw_max - 32;
			gxDebug::GetInstance()->SetError(gxDebug::ErrVertexBufferOver);
		}

		VertexPositionColorTexCoord* pVtxTex = (VertexPositionColorTexCoord*)pVertex;

		//頂点バッファ、インデックスバッファは小分けにしてMap,UnMapしてはいけない
		//やるなら位置をずらすこと

		Float32 w = 1.0f;
		Float32 offsetx = 0.0f;

		if( m_b3DView )
		{
			offsetx = 0.5f;
		}

		const VertexPositionColorTexCoord cubeVertices[] = 
		{
			//背景を描く:0
			{ StXMFLOAT4( -1.f*w,  1.f*w,  0.f,1), StXMFLOAT4( 1.0f, 1.0f, 1.0f , 1.0f ) , StXMFLOAT2( 0.f , 1.0f-0.f ) ,StXMFLOAT2(0.f , 1.0f - 0.f),StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f ,StXMFLOAT2(1.0f,1.0f) , StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f) , StPointLightInfo() , },
			{ StXMFLOAT4(  1.f*w, -1.f*w,  0.f,1), StXMFLOAT4( 1.0f, 1.0f, 1.0f , 1.0f ) , StXMFLOAT2( 1.f , 1.0f-1.f ) ,StXMFLOAT2(1.f , 1.0f - 1.f),StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f ,StXMFLOAT2(1.0f,1.0f) , StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f) , StPointLightInfo() },
			{ StXMFLOAT4( -1.f*w, -1.f*w,  0.f,1), StXMFLOAT4( 1.0f, 1.0f, 1.0f , 1.0f ) , StXMFLOAT2( 0.f , 1.0f-1.f ) ,StXMFLOAT2(0.f , 1.0f - 1.f),StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f ,StXMFLOAT2(1.0f,1.0f) , StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f) , StPointLightInfo() },
			{ StXMFLOAT4(  1.f*w,  1.f*w,  0.f,1), StXMFLOAT4( 1.0f, 1.0f, 1.0f , 1.0f ) , StXMFLOAT2( 1.f , 1.0f-0.f ) ,StXMFLOAT2(1.f , 1.0f - 0.f),StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f ,StXMFLOAT2(1.0f,1.0f) , StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f) , StPointLightInfo() },

			//レンダーテクスチャを描く
			//FLIP1用:6
			{ StXMFLOAT4( glX1 - offsetx,  glY1 ,  0.f,1), StXMFLOAT4(1.0f, 1.0f, 1.0f , 1.0f) , StXMFLOAT2(0.f , 1.0f-0.f), StXMFLOAT2(0.f , 1.0f - 0.f) ,StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f,StXMFLOAT2(1.0f,1.0f) , StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f) , StPointLightInfo() },
			{ StXMFLOAT4( glX2 - offsetx,  glY2 ,  0.f,1), StXMFLOAT4(1.0f, 1.0f, 1.0f , 1.0f) , StXMFLOAT2(1.f , 1.0f-1.f), StXMFLOAT2(1.f , 1.0f - 1.f) ,StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f,StXMFLOAT2(1.0f,1.0f) , StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f) , StPointLightInfo() },
			{ StXMFLOAT4( glX1 - offsetx,  glY2 ,  0.f,1), StXMFLOAT4(1.0f, 1.0f, 1.0f , 1.0f) , StXMFLOAT2(0.f , 1.0f-1.f), StXMFLOAT2(0.f , 1.0f - 1.f) ,StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f,StXMFLOAT2(1.0f,1.0f) , StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f) , StPointLightInfo() },
			{ StXMFLOAT4( glX2 - offsetx,  glY1 ,  0.f,1), StXMFLOAT4(1.0f, 1.0f, 1.0f , 1.0f) , StXMFLOAT2(1.f , 1.0f-0.f), StXMFLOAT2(1.f , 1.0f - 0.f) ,StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f,StXMFLOAT2(1.0f,1.0f) , StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f) , StPointLightInfo() },

			//レンダーテクスチャを描く
			//FLIP2用:12
			{ StXMFLOAT4( glX1 + offsetx,  glY1 ,  0.f,1), StXMFLOAT4(1.0f, 1.0f, 1.0f , 1.0f) , StXMFLOAT2(0.f , 1.0f-0.f), StXMFLOAT2(0.f , 1.0f - 0.f) ,StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f,StXMFLOAT2(1.0f,1.0f) , StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f) , StPointLightInfo() },
			{ StXMFLOAT4( glX2 + offsetx , glY2 ,  0.f,1), StXMFLOAT4(1.0f, 1.0f, 1.0f , 1.0f) , StXMFLOAT2(1.f , 1.0f-1.f), StXMFLOAT2(1.f , 1.0f - 1.f) ,StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f,StXMFLOAT2(1.0f,1.0f) , StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f) , StPointLightInfo() },
			{ StXMFLOAT4( glX1 + offsetx,  glY2 ,  0.f,1), StXMFLOAT4(1.0f, 1.0f, 1.0f , 1.0f) , StXMFLOAT2(0.f , 1.0f-1.f), StXMFLOAT2(0.f , 1.0f - 1.f) ,StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f,StXMFLOAT2(1.0f,1.0f) , StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f) , StPointLightInfo() },
			{ StXMFLOAT4( glX2 + offsetx,  glY1 ,  0.f,1), StXMFLOAT4(1.0f, 1.0f, 1.0f , 1.0f) , StXMFLOAT2(1.f , 1.0f-0.f), StXMFLOAT2(1.f , 1.0f - 0.f) ,StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f,StXMFLOAT2(1.0f,1.0f) , StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f) , StPointLightInfo() },

			//ブラー用 1.0 -> 0.25 :18
			{ StXMFLOAT4( -1.f*w,  1.f*w,  0.f,1), StXMFLOAT4( 1.0f, 1.0f, 1.0f , 1.0f ) , StXMFLOAT2( 0.f , 1.0f-0.f ), StXMFLOAT2(0.f , 1.0f - 0.f) ,StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f ,StXMFLOAT2(1.0f,1.0f) , StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f) , StPointLightInfo() },
			{ StXMFLOAT4( -0.5f*w, 0.5f*w, 0.f,1), StXMFLOAT4( 1.0f, 1.0f, 1.0f , 1.0f ) , StXMFLOAT2( 1.f , 1.0f-1.f ), StXMFLOAT2(1.f , 1.0f - 1.f) ,StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f ,StXMFLOAT2(1.0f,1.0f) , StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f) , StPointLightInfo() },
			{ StXMFLOAT4( -1.f*w,  0.5f*w, 0.f,1), StXMFLOAT4( 1.0f, 1.0f, 1.0f , 1.0f ) , StXMFLOAT2( 0.f , 1.0f-1.f ), StXMFLOAT2(0.f , 1.0f - 1.f) ,StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f ,StXMFLOAT2(1.0f,1.0f) , StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f) , StPointLightInfo() },
			{ StXMFLOAT4( -0.5f*w, 1.f*w,  0.f,1), StXMFLOAT4( 1.0f, 1.0f, 1.0f , 1.0f ) , StXMFLOAT2( 1.f , 1.0f-0.f ), StXMFLOAT2(1.f , 1.0f - 0.f) ,StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f ,StXMFLOAT2(1.0f,1.0f) , StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f) , StPointLightInfo() },

			//ブラー用書き戻し 0.25 -> 1.0 : 24
			{ StXMFLOAT4( -1.f*w,  1.f*w,  0.f,1), StXMFLOAT4( 1.0f, 1.0f, 1.0f , 1.0f ), StXMFLOAT2( 0.f   , 1.0f  )	, StXMFLOAT2( 0.f   , 1.0f  )	,StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f ,StXMFLOAT2(1.0f,1.0f) , StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f) , StPointLightInfo() },
			{ StXMFLOAT4(  1.f*w, -1.f*w,  0.f,1), StXMFLOAT4( 1.0f, 1.0f, 1.0f , 1.0f ), StXMFLOAT2( 0.25f , 0.75f )	, StXMFLOAT2( 0.25f , 0.75f )	,StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f ,StXMFLOAT2(1.0f,1.0f) , StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f) , StPointLightInfo() },
			{ StXMFLOAT4( -1.f*w, -1.f*w,  0.f,1), StXMFLOAT4( 1.0f, 1.0f, 1.0f , 1.0f ), StXMFLOAT2( 0.f   , 0.75f )	, StXMFLOAT2( 0.f   , 0.75f )	,StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f ,StXMFLOAT2(1.0f,1.0f) , StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f) , StPointLightInfo() },
			{ StXMFLOAT4(  1.f*w,  1.f*w,  0.f,1), StXMFLOAT4( 1.0f, 1.0f, 1.0f , 1.0f ), StXMFLOAT2( 0.25f , 1.0f  )	, StXMFLOAT2( 0.25f , 1.0f  )	,StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f ,StXMFLOAT2(1.0f,1.0f) , StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f) , StPointLightInfo() },

			//ブラー中	0.25 -> 0.25 : 30
			{ StXMFLOAT4( -1.f*w ,  1.f*w,  0.f,1), StXMFLOAT4( 1.0f, 1.0f, 1.0f , 1.0f ), StXMFLOAT2( 0.f   , 1.0f)  , StXMFLOAT2( 0.f   , 1.0f)  ,	StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f ,StXMFLOAT2(1.0f,1.0f) , StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f) , StPointLightInfo() },
			{ StXMFLOAT4( -0.5f*w,  0.5f*w, 0.f,1), StXMFLOAT4( 1.0f, 1.0f, 1.0f , 1.0f ), StXMFLOAT2( 0.25f , 0.75f) , StXMFLOAT2( 0.25f , 0.75f) ,	StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f ,StXMFLOAT2(1.0f,1.0f) , StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f) , StPointLightInfo() },
			{ StXMFLOAT4( -1.f*w,   0.5f*w, 0.f,1), StXMFLOAT4( 1.0f, 1.0f, 1.0f , 1.0f ), StXMFLOAT2( 0.f   , 0.75f) , StXMFLOAT2( 0.f   , 0.75f) ,	StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f ,StXMFLOAT2(1.0f,1.0f) , StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f) , StPointLightInfo() },
			{ StXMFLOAT4( -0.5f*w,  1.f*w,  0.f,1), StXMFLOAT4( 1.0f, 1.0f, 1.0f , 1.0f ), StXMFLOAT2( 0.25f , 1.0f)  , StXMFLOAT2( 0.25f , 1.0f)  ,	StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f ,StXMFLOAT2(1.0f,1.0f) , StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f) , StPointLightInfo() },

			//背景を描く:0
			{ StXMFLOAT4(-1.f * w,  1.f * w,  0.f,1), StXMFLOAT4(1.0f, 1.0f, 1.0f , 0.9f), StXMFLOAT2(0.f , 1.0f - 0.f) , StXMFLOAT2(0.f , 1.0f - 0.f),StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f ,StXMFLOAT2(1.0f,1.0f) , StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f) , StPointLightInfo() },
			{ StXMFLOAT4(1.f * w, -1.f * w,  0.f,1) , StXMFLOAT4(1.0f, 1.0f, 1.0f , 0.9f), StXMFLOAT2(1.f , 1.0f - 1.f) , StXMFLOAT2(1.f , 1.0f - 1.f),StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f ,StXMFLOAT2(1.0f,1.0f) , StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f) , StPointLightInfo() },
			{ StXMFLOAT4(-1.f * w, -1.f * w,  0.f,1), StXMFLOAT4(1.0f, 1.0f, 1.0f , 0.9f), StXMFLOAT2(0.f , 1.0f - 1.f) , StXMFLOAT2(0.f , 1.0f - 1.f),StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f ,StXMFLOAT2(1.0f,1.0f) , StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f) , StPointLightInfo() },
			{ StXMFLOAT4(1.f * w,  1.f * w,  0.f,1) , StXMFLOAT4(1.0f, 1.0f, 1.0f , 0.9f), StXMFLOAT2(1.f , 1.0f - 0.f) , StXMFLOAT2(1.f , 1.0f - 0.f),StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f ,StXMFLOAT2(1.0f,1.0f) , StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f) , StPointLightInfo() },

		};

		gxUtil::MemCpy( &pVtxTex[vtx_max] , (void*)cubeVertices , sizeof(cubeVertices) );

		Uint32 cubeIndices[] =
		{
			0+vtx_max, 1+vtx_max, 2+vtx_max,
			0+vtx_max, 3+vtx_max, 1+vtx_max,

			0+vtx_max+4, 1+vtx_max+4, 2+vtx_max+4,
			0+vtx_max+4, 3+vtx_max+4, 1+vtx_max+4,

			0+vtx_max+8, 1+vtx_max+8, 2+vtx_max+8,
			0+vtx_max+8, 3+vtx_max+8, 1+vtx_max+8,

			0+vtx_max+12, 1+vtx_max+12, 2+vtx_max+12,
			0+vtx_max+12, 3+vtx_max+12, 1+vtx_max+12,

			0+vtx_max+16, 1+vtx_max+16, 2+vtx_max+16,
			0+vtx_max+16, 3+vtx_max+16, 1+vtx_max+16,

			0+vtx_max+24, 1+vtx_max+24, 2+vtx_max+24,
			0+vtx_max+24, 3+vtx_max+24, 1+vtx_max+24,

			0 + vtx_max + 30, 1 + vtx_max + 30, 2 + vtx_max + 30,
			0 + vtx_max + 30, 3 + vtx_max + 30, 1 + vtx_max + 30,
		};

		gxUtil::MemCpy( &m_pIndexBuffer[0]        , gxRender::GetInstance()->GetIndexBuffer( 0 ) , idx_max*4 );
		gxUtil::MemCpy( &m_pIndexBuffer[idx_max*4], cubeIndices, sizeof(cubeIndices) );
	}

	//m_CurrentPage = 0;

	if( !m_b3DView )
	{
/*
		if( rgba[0] + rgba[1] + rgba[2] )
		{
			//レンダーターゲットをゲーム画面用テクスチャにする
			glBindFramebuffer( GL_FRAMEBUFFER, m_OffScreenTexture[ enGamePostProcess0 + m_CurrentPage ].frameBufferObject );
			glClearColor( rgba[0], rgba[1], rgba[2], rgba[3] );
			glClear(GL_COLOR_BUFFER_BIT);
		}
*/

		m_ConstBuffer3dView.pos[0] = 0.0f;

	Sint32 start = gxRender::GetInstance()->GetBGOrderMax();
	Sint32 max   = gxRender::GetInstance()->GetGameOrderMax();

	//---------------

	Uint32 vtx_max = gxRender::GetInstance()->GetVertexNum();
	Uint32 idx_max = gxRender::GetInstance()->GetIndexNum();

	size_t vram_draw_max = enVertexBufferSize / sizeof(VertexPositionColorTexCoord);

	if (vtx_max >= vram_draw_max - 32)
	{
		vtx_max = (Uint32)vram_draw_max - 32;
	}

	StCustomVertex* pVertex = gxRender::GetInstance()->GetVertex(0);
	size_t size = (vtx_max+32) * sizeof(VertexPositionColorTexCoord);
	glBindBuffer( GL_ARRAY_BUFFER, m_vertexBufferObject);
	glBufferSubData(GL_ARRAY_BUFFER , 0, size , pVertex );

	//---------------

		renderGameObject( start , max );

		//ToDo:Game1へコピー
		//glBindFramebuffer( GL_FRAMEBUFFER, m_TextureData[ enGameScreen1 ].frameBufferObject);
	}
	else
	{

	}

}

void COpenGLES3::ClearFrameBuffer(Uint32 pageFrmBuf)
{
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);
}

void COpenGLES3::CopyFullScreen(Sint32 pageTexBuf, Sint32 pageFrmBuf)
{
	Uint32 idx_max = gxRender::GetInstance()->GetIndexNum();
	glBindTexture(GL_TEXTURE_2D, m_OffScreenTexture[pageTexBuf].texture2D);
	glBindFramebuffer(GL_FRAMEBUFFER, m_OffScreenTexture[pageFrmBuf].frameBufferObject);

	ClearFrameBuffer(pageFrmBuf);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, &m_pIndexBuffer[(idx_max + 0) * 4]);
	gxDebug::GetInstance()->AddDrawCallCnt();
}

void COpenGLES3::renderGameObject( Sint32 start , Sint32 max )
{
	//テクスチャにゲームオブジェクトを描く

	CCommandList* pCommand = NULL;
	Sint32 cmdMax = gxRender::GetInstance()->GetCommandNum();

	Uint32 idx_max = gxRender::GetInstance()->GetIndexNum();

	cmdMax = max;

	glActiveTexture( GL_TEXTURE0 + 0 );
	glBindTexture  ( GL_TEXTURE_2D, 0 );

	//通常のしぇーだーに戻す
	changeShader( enShaderDefault );

	//ゲーム画面の描画

	glBlendEquation( GL_FUNC_ADD );
	glBlendFunc( GL_SRC_ALPHA , GL_ONE_MINUS_SRC_ALPHA);

	for(Sint32 n=start; n<max; n++)
	{
		pCommand = gxRender::GetInstance()->GetCommandList( n );
		Sint32 v_start = pCommand->arg[0];
		Sint32 v_num   = pCommand->arg[1];
		Sint32 i_start = pCommand->arg[2];
		Sint32 i_num   = pCommand->arg[3];

		switch( pCommand->eCommand ){
		case eCmdBindNoneTexture:
			//テクスチャをはずす
			glActiveTexture( GL_TEXTURE0 + enAlbedoTextureMapSlot );
			glBindTexture( GL_TEXTURE_2D, m_OffScreenTexture[ enBasicBuff ].texture2D );
			break;

		case eCmdBindAlbedoTexture:
			{
				//テクスチャをつける
				glActiveTexture( GL_TEXTURE0 + enAlbedoTextureMapSlot );

				Sint32 page = pCommand->arg[0];

				//if( page == enBackBuffer )
				//{
				//	glBindTexture( GL_TEXTURE_2D, m_OffScreenTexture[ enGameScreen1 ].texture2D );
				//}
				//else if( page == enCapturePage )
				//{
				//	glBindTexture( GL_TEXTURE_2D, m_OffScreenTexture[ enCaptureScreen ].texture2D );
				//}
				if( page <= -100 )
				{
					page = abs(page)-100;
					glBindTexture( GL_TEXTURE_2D, m_OffScreenTexture[ page ].texture2D );
				}
				else
				{
					if( page < 0 )	page = abs(page);
					glBindTexture( GL_TEXTURE_2D, m_TextureData[ page ].texture2D );
				}

			}
			break;

		case eCmdBindNormalTexture:
			{
				//法線マップ用テクスチャをつける
				glActiveTexture( GL_TEXTURE0 + enNormalTextureMapSlot );

				Sint32 page = pCommand->arg[0];

				if( page == enBackBuffer )
				{
					glBindTexture( GL_TEXTURE_2D, m_OffScreenTexture[ enGameScreen1 ].texture2D );
				}
				else if( page == enCapturePage )
				{
					glBindTexture( GL_TEXTURE_2D, m_OffScreenTexture[ enCaptureScreen ].texture2D );
				}
				else
				{
					glBindTexture( GL_TEXTURE_2D, m_TextureData[ page ].texture2D );
				}

			}
			break;

		case eCmdBindPalletTexture:
			{
				//パレットマップ用テクスチャをつける
				glActiveTexture( GL_TEXTURE0 + 2 );

				Sint32 page = pCommand->arg[0];

				if( page == enBackBuffer )
				{
					glBindTexture( GL_TEXTURE_2D, m_OffScreenTexture[ enGameScreen1 ].texture2D );
				}
				else if( page == enCapturePage )
				{
					glBindTexture( GL_TEXTURE_2D, m_OffScreenTexture[ enCaptureScreen ].texture2D );
				}
				else
				{
					glBindTexture( GL_TEXTURE_2D, m_TextureData[ page ].texture2D );
				}
			}
			break;

		case eCmdBindCaptureTexture:
			glActiveTexture(GL_TEXTURE0 + enCaptureTextureMapSlot);
			glBindTexture(GL_TEXTURE_2D, m_OffScreenTexture[enCaptureScreen].texture2D);
			break;

		case eCmdChgAttributeAlphaNml:
			//ブレンディング(標準)
			glBlendEquation( GL_FUNC_ADD );
			glBlendFunc( GL_SRC_ALPHA , GL_ONE_MINUS_SRC_ALPHA);
			break;

		case eCmdChgAttributeAlphaAdd:
			//ブレンディング(加算)
			glBlendEquation( GL_FUNC_ADD );
			glBlendFunc( GL_SRC_ALPHA , GL_ONE);
			break;

		case eCmdChgAttributeAlphaSub:
			//ブレンディング(減算)
//			glBlendFunc( GL_ZERO , GL_ONE_MINUS_SRC_ALPHA);
			glBlendEquation( GL_FUNC_REVERSE_SUBTRACT  );
			glBlendFunc( GL_SRC_ALPHA , GL_ONE);
			break;

		case eCmdChgAttributeAlphaCrs:
			//ブレンディング(乗算)
			glBlendFunc( GL_ZERO , GL_SRC_COLOR);
			break;

		case eCmdChgAttributeAlphaRvs:
			//ブレンディング(反転)
			glBlendFunc( GL_ONE_MINUS_DST_COLOR , GL_ZERO );
			break;

		case eCmdChgAttributeAlphaXor:
			//ブレンディング(XOR)
			glBlendFunc( GL_ONE_MINUS_DST_COLOR , GL_ONE_MINUS_SRC_COLOR );
			break;

		case eCmdChgAttributeAlphaScr:
			//ブレンディング(スクリーン乗算)
			glBlendFunc( GL_ONE_MINUS_DST_COLOR , GL_ONE );
			break;

		case eCmdRenderPoint:
			//点の描画
            glDrawElements( GL_POINTS , i_num , GL_UNSIGNED_INT  , &m_pIndexBuffer[i_start*4] );
			gxDebug::GetInstance()->AddDrawCallCnt();
			break;

		case eCmdRenderLineStrip:
			//連続線の描画
            glDrawElements( GL_LINES , i_num , GL_UNSIGNED_INT  , &m_pIndexBuffer[i_start*4] );
			gxDebug::GetInstance()->AddDrawCallCnt();
			break;

		case eCmdRenderLineNormal:
			//線の描画
            glDrawElements( GL_LINES , i_num , GL_UNSIGNED_INT  , &m_pIndexBuffer[i_start*4] );
			gxDebug::GetInstance()->AddDrawCallCnt();
			break;

		case eCmdRenderTriangle:
		case eCmdRenderSquare:
			//三角形の描画
            glDrawElements( GL_TRIANGLES , i_num , GL_UNSIGNED_INT  , &m_pIndexBuffer[i_start*4] );
			gxDebug::GetInstance()->AddDrawCallCnt();
			break;

		case eCmdRenderFont:
			//フォントの描画
			break;

		case eCmdScissor:
			//シザリング
			if( pCommand->arg[2] == 0 || pCommand->arg[3] == 0 )
			{
				glDisable ( GL_SCISSOR_TEST );
				m_bScissorEnable = gxFalse;
			}
			else
			{
				Sint32 x,y,w,h;
				w = pCommand->arg[2];
				h = pCommand->arg[3];
				x = pCommand->arg[0];
				y = m_GameH - pCommand->arg[1] - h;
				glEnable ( GL_SCISSOR_TEST );
				glScissor( x, y, w ,h );
				m_Scissor.SetWH(x,y,w,h);
				m_bScissorEnable = gxTrue;
			}
			break;
		case eCmdChangeShader:
			{
				switch( pCommand->arg[0] ){
				case gxShaderReset:
				case gxShaderDefault:
					changeShader(enShaderDefault );
					break;
				case gxShaderBloom:
					changeShader(enShaderBloom );
					break;
				case gxShaderBlur:
					changeShader(enShaderBlur );
					break;
				case gxShaderRaster:
					changeShader(enShaderRaster );
					break;
				case gxShaderNormal:
					changeShader(enShaderNormal );
					break;
				case gxShaderPallet:
					changeShader(enShaderNormal );
					break;
                case gxShaderFont:
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
                    changeShader(enShaderFont );

                    break;
                case gxShaderGreyScale:
                    changeShader( enShaderGreyScale );
                    break;
                case gxShaderHighSpeed:
                    changeShader( enShaderHighSpeed );
                    break;
				default:
					//changeShader(enShaderDev );
					changeShader(enShaderDefault );
					break;
				}
			}
			break;


		case eCmdChangeRenderTarget:
			break;

		case eCmdProcessingBloom:
			{
				Sint32 pageFrmBuf = 0;
				Sint32 pageTexBuf = 0;

				//enGameScreen2に現在の画面を退避する
				glActiveTexture( GL_TEXTURE0 + enAlbedoTextureMapSlot );
		changeShader(enShaderDefault );

				glBlendEquation( GL_FUNC_ADD );
				glBlendFunc( GL_SRC_ALPHA , GL_ONE_MINUS_SRC_ALPHA);

				glBindFramebuffer(GL_FRAMEBUFFER, m_OffScreenTexture[ enGameScreen0 ].frameBufferObject );
				glBindTexture(GL_TEXTURE_2D     , m_OffScreenTexture[ enGamePostProcess0+m_CurrentPage ].texture2D );

				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
				glDrawElements( GL_TRIANGLES , 6 , GL_UNSIGNED_INT  , &m_pIndexBuffer[ (idx_max+0)*4 ] );
				gxDebug::GetInstance()->AddDrawCallCnt();

#if 1
				//ブルームシェーダーで輝度の高いものを抽出する
				changeShader( enShaderBloom );

				GLfloat option[enOptionNum] = { pCommand->arg[1]/100.0f , 0.0 };
				Sint32 optionID = glGetUniformLocation(m_ShaderProg[enShaderBlur], "u_option");
				glUniform1fv(optionID, 4, option);

				pageTexBuf = enGamePostProcess0 + m_CurrentPage;
				m_CurrentPage ++;
				m_CurrentPage = m_CurrentPage%3;
				pageFrmBuf = enGamePostProcess0 + m_CurrentPage;

				glBindFramebuffer(GL_FRAMEBUFFER, m_OffScreenTexture[pageFrmBuf].frameBufferObject);
				glBindTexture(GL_TEXTURE_2D     , m_OffScreenTexture[ pageTexBuf ].texture2D );
				ClearFrameBuffer(pageFrmBuf);

				//ブルーム対象の描画を行う
				glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
				glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
				glDrawElements( GL_TRIANGLES , 6 , GL_UNSIGNED_INT  , &m_pIndexBuffer[ (idx_max+0)*4 ] );
				gxDebug::GetInstance()->AddDrawCallCnt();
#endif

				//Bloom画像をぼかす
				Float32 fBlurPower[2] = { pCommand->arg[0]/10000.0f , 1.0f };

				changeShader( enShaderBlur , fBlurPower );

				for (Sint32 aa = 0; aa < 5; aa++ )//pCommand->arg[1]; aa++)
				{
					pageTexBuf = enGamePostProcess0 + m_CurrentPage;
					m_CurrentPage++;
					m_CurrentPage = m_CurrentPage % 3;

					pageFrmBuf = enGamePostProcess0 + m_CurrentPage;

					//まず横
					GLfloat option[enOptionNum] = { 0.0 , 0.0 };
					Sint32 optionID = glGetUniformLocation(m_ShaderProg[enShaderBlur], "u_option");
					glUniform1fv(optionID, enOptionNum, option);

					glBindFramebuffer(GL_FRAMEBUFFER, m_OffScreenTexture[pageFrmBuf].frameBufferObject);
					glBindTexture(GL_TEXTURE_2D, m_OffScreenTexture[ pageTexBuf ].texture2D);
					ClearFrameBuffer(pageFrmBuf);

					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
					glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, &m_pIndexBuffer[(idx_max + 0) * 4]);
					gxDebug::GetInstance()->AddDrawCallCnt();

					//次に縦

					pageTexBuf = enGamePostProcess0 + m_CurrentPage;
					m_CurrentPage++;
					m_CurrentPage = m_CurrentPage % 3;
					pageFrmBuf = enGamePostProcess0 + m_CurrentPage;

					option[0] = 1.0;
					optionID = glGetUniformLocation(m_ShaderProg[enShaderBlur], "u_option");
					glUniform1fv(optionID, enOptionNum, option);

					glBindFramebuffer(GL_FRAMEBUFFER, m_OffScreenTexture[ pageFrmBuf ].frameBufferObject);
					glBindTexture(GL_TEXTURE_2D, m_OffScreenTexture[ pageTexBuf ].texture2D);
					ClearFrameBuffer(pageFrmBuf);

					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
					glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, &m_pIndexBuffer[(idx_max + 0) * 4]);
					gxDebug::GetInstance()->AddDrawCallCnt();
				}

				//---------------

				//通常シェーダーに戻して退避していた画面を元に戻す
			changeShader();
				Sint32 bloomBuf = enGamePostProcess0 + m_CurrentPage;
				pageTexBuf = enGameScreen0;
				m_CurrentPage ++;
				m_CurrentPage = m_CurrentPage%3;
				pageFrmBuf = enGamePostProcess0 + m_CurrentPage;

				glBindFramebuffer(GL_FRAMEBUFFER, m_OffScreenTexture[ pageFrmBuf ].frameBufferObject );
				glBindTexture(GL_TEXTURE_2D     , m_OffScreenTexture[ pageTexBuf ].texture2D );

				glDrawElements( GL_TRIANGLES , 6 , GL_UNSIGNED_INT  , &m_pIndexBuffer[(idx_max+0)*4] );
				gxDebug::GetInstance()->AddDrawCallCnt();

				//加算合成にする
				glBlendEquation(GL_FUNC_ADD);
				glBlendFunc(GL_SRC_ALPHA, GL_ONE);

//				glDrawElements( GL_TRIANGLES , 6 , GL_UNSIGNED_INT  , &m_pIndexBuffer[(idx_max+30)*4] );

				pageTexBuf = bloomBuf;
				glBindTexture(GL_TEXTURE_2D, m_OffScreenTexture[ pageTexBuf ].texture2D);
				glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, &m_pIndexBuffer[(idx_max + 30) * 4]);
				gxDebug::GetInstance()->AddDrawCallCnt();

				//シェーダーを元に戻す
			//changeShader();

				glBlendEquation( GL_FUNC_ADD );
				glBlendFunc( GL_SRC_ALPHA , GL_ONE_MINUS_SRC_ALPHA);
			}
			break;

		case eCmdProcessingBlur:
			{
				//ボケシェーダーに変更する
				glActiveTexture( GL_TEXTURE0 + enAlbedoTextureMapSlot );

				Float32 fBlurPower[2] = { pCommand->arg[0] / 10000.f , 0.0f };
				changeShader( enShaderBlur , fBlurPower );

				//for (Uint32 aa = 0; aa < pCommand->arg[1]; aa++)
				for (Uint32 aa = 0; aa < 1; aa++)
				{
					Sint32 pageTexBuf = enGamePostProcess0 + m_CurrentPage;
					m_CurrentPage++;
					m_CurrentPage = m_CurrentPage % 3;

					Sint32 pageFrmBuf = enGamePostProcess0 + m_CurrentPage;

					//-------------------------------

					GLfloat options[enOptionNum] = { 0.0 , 1.0 };
					Sint32 optionsID = glGetUniformLocation( m_ShaderProg[ enShaderBlur ], "u_option");
					glUniform1fv( optionsID, 4, options );

					glBindFramebuffer(GL_FRAMEBUFFER, m_OffScreenTexture[ pageFrmBuf ].frameBufferObject);
					glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
					glClear(GL_COLOR_BUFFER_BIT);
					glBindTexture(GL_TEXTURE_2D, m_OffScreenTexture[ pageTexBuf ].texture2D);

					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
					glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, &m_pIndexBuffer[(idx_max + 0) * 4]);
					gxDebug::GetInstance()->AddDrawCallCnt();

					//-------------------------------

					pageTexBuf = enGamePostProcess0 + m_CurrentPage;
					m_CurrentPage++;
					m_CurrentPage = m_CurrentPage % 3;
					pageFrmBuf = enGamePostProcess0 + m_CurrentPage;

					options[0] = 1.0f;
					optionsID = glGetUniformLocation( m_ShaderProg[ enShaderBlur ], "u_option");
					glUniform1fv( optionsID, 4, options );

					glBindFramebuffer(GL_FRAMEBUFFER, m_OffScreenTexture[ pageFrmBuf ].frameBufferObject);
					glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
					glClear(GL_COLOR_BUFFER_BIT);
					glBindTexture(GL_TEXTURE_2D, m_OffScreenTexture[ pageTexBuf ].texture2D);

					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
					glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, &m_pIndexBuffer[(idx_max + 0) * 4]);
					gxDebug::GetInstance()->AddDrawCallCnt();

				}
				//シェーダーを元に戻す
			changeShader();
			}
			break;

		case eCmdCaptureScreen:
			{
				glActiveTexture( GL_TEXTURE0 + enAlbedoTextureMapSlot );
				changeShader( enShaderRaster );
				GLfloat option[enOptionNum] = { 10.0 , 0.1f , 0.f };
				option[0] = 10.0f * (pCommand->arg[0]/10000.0f);	//揺れの回数
				option[1] = pCommand->arg[1]/10000.0f;				//揺れの幅
				option[2] = pCommand->arg[2]/10000.0f;				//揺れ速度

				Sint32 optionID = glGetUniformLocation(m_ShaderProg[enShaderRaster], "u_option");
				glUniform1fv(optionID, 4, option );

				glDisable ( GL_SCISSOR_TEST );

				CopyFullScreen(enGamePostProcess0 + m_CurrentPage, enCaptureScreen);

				glBindFramebuffer(GL_FRAMEBUFFER, m_OffScreenTexture[enGamePostProcess0 + m_CurrentPage].frameBufferObject);
				if( m_bScissorEnable )
				{
					glEnable ( GL_SCISSOR_TEST );
					glScissor( m_Scissor.x1, m_Scissor.y1, m_Scissor.x2-m_Scissor.x1 ,m_Scissor.y2 - m_Scissor.y1 );
				}
				changeShader( enShaderDefault );
				CopyFullScreen(enCaptureScreen, enGamePostProcess0 + m_CurrentPage);
		}
		break;

		case eCmdPostProcessGreyScale:
		{
			Sint32 pageTexBuf = enGamePostProcess0 + m_CurrentPage;
			m_CurrentPage++;
			m_CurrentPage = m_CurrentPage % 3;
			Sint32 pageFrmBuf = enGamePostProcess0 + m_CurrentPage;

			GLfloat option[enOptionNum] = { pCommand->arg[0]/1000.0f , 0.1f , 0.f};
			Sint32 optionID = glGetUniformLocation(m_ShaderProg[enShaderGreyScale], "u_option");

			changeShader(enShaderGreyScale);

			glUniform1fv(optionID, 4, option);
			glBindFramebuffer(GL_FRAMEBUFFER, m_OffScreenTexture[enGamePostProcess0 + m_CurrentPage].frameBufferObject);
			CopyFullScreen(pageTexBuf, pageFrmBuf);
		}
		break;

		case eCmdPostProcessRasterScroll:
			{
				glActiveTexture(GL_TEXTURE0 + enAlbedoTextureMapSlot);
				changeShader(enShaderRaster);
				GLfloat option[enOptionNum] = { 10.0 , 0.1f , 0.f };
				option[0] = 10.0f * (pCommand->arg[0] / 10000.0f);	//揺れの回数
				option[1] = pCommand->arg[1] / 10000.0f;				//揺れの幅
				option[2] = pCommand->arg[2] / 10000.0f;				//揺れ速度

				Sint32 optionID = glGetUniformLocation(m_ShaderProg[enShaderRaster], "u_option");
				glUniform1fv(optionID, 4, option);

				glDisable(GL_SCISSOR_TEST);
				Sint32 pageTexBuf = enGamePostProcess0 + m_CurrentPage;
				Sint32 pageFrmBuf = enCaptureScreen;

				glBindFramebuffer(GL_FRAMEBUFFER, m_OffScreenTexture[pageFrmBuf].frameBufferObject);
				glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
				glClear(GL_COLOR_BUFFER_BIT);
				glBindTexture(GL_TEXTURE_2D, m_OffScreenTexture[pageTexBuf].texture2D);

				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

				glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, &m_pIndexBuffer[(idx_max + 0) * 4]);
				gxDebug::GetInstance()->AddDrawCallCnt();

				pageFrmBuf = enGamePostProcess0 + m_CurrentPage;
				glBindFramebuffer(GL_FRAMEBUFFER, m_OffScreenTexture[pageFrmBuf].frameBufferObject);
				if (m_bScissorEnable)
				{
					glEnable(GL_SCISSOR_TEST);
					glScissor(m_Scissor.x1, m_Scissor.y1, m_Scissor.x2 - m_Scissor.x1, m_Scissor.y2 - m_Scissor.y1);
				}
				changeShader(enShaderDefault);
			}
			break;

			case eCmdDevelop:
			{
				glActiveTexture( GL_TEXTURE0 + enAlbedoTextureMapSlot );

				changeShader(enShaderDev);
				glBlendEquation(GL_FUNC_ADD);
				glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

				glActiveTexture(GL_TEXTURE0 + 1 );
				glBindTexture(GL_TEXTURE_2D, m_TextureData[1].texture2D);
				glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, &m_pIndexBuffer[0 * 4]);
				gxDebug::GetInstance()->AddDrawCallCnt();

				glActiveTexture(GL_TEXTURE0);
				changeShader(enShaderDefault);
		}
		break;

		default:
			break;
		}
	}
}


void COpenGLES3::renderSystem()
{
	//----------------------------------------------------------------------
	// 頂点シェーダーをアタッチします。
	//----------------------------------------------------------------------
	Uint32 idx_max = gxRender::GetInstance()->GetIndexNum();

	Float32 rgba[4] = { 0,0,0.5f,1 };	//rgba
#ifdef USE_FLIP
	glBindFramebuffer( GL_FRAMEBUFFER, m_OffScreenTexture[enFinalBuffer].frameBufferObject );	//最終的な描画バッファに変更
#else
	glBindFramebuffer( GL_FRAMEBUFFER, m_FrameBufferIndex );	//最終的な描画バッファに変更
#endif
	// バック バッファーと深度ステンシル ビューをクリアします。

	glClearColor( rgba[0], rgba[1], rgba[2], rgba[3] );
	glClear( GL_COLOR_BUFFER_BIT );

	glActiveTexture( GL_TEXTURE0 + enAlbedoTextureMapSlot );

	changeShader();

	glBlendEquation( GL_FUNC_ADD );
	glBlendFunc( GL_SRC_ALPHA , GL_ONE_MINUS_SRC_ALPHA);

	//--------------------------------------------------
	//プライオリティが０以前の描画は全画面で行う
	//--------------------------------------------------

	Sint32 start = 0;
	Sint32 max   = gxRender::GetInstance()->GetBGOrderMax();
	if( max )
	{
		renderGameObject( start , max );
	}
	else
	{
		//壁紙画像をアタッチ
		glBindTexture( GL_TEXTURE_2D, m_OffScreenTexture[ enWallPaper ].texture2D );
		glDrawElements( GL_TRIANGLES , 6 , GL_UNSIGNED_INT  , &m_pIndexBuffer[idx_max*4] );
	}



	//--------------------------------------------------
	//ゲーム画面をフリップ
	//--------------------------------------------------

	glBlendFunc( GL_SRC_ALPHA , GL_ONE_MINUS_SRC_ALPHA);
	glBlendEquation( GL_FUNC_ADD );
	glBlendFunc( GL_SRC_ALPHA , GL_ONE_MINUS_SRC_ALPHA);

	if( !m_b3DView )
	{
		//ピクセルシェーダーにリソース（テクスチャ）をアタッチする
		glBindTexture( GL_TEXTURE_2D, m_OffScreenTexture[ enGamePostProcess0+m_CurrentPage ].texture2D );

	// テクスチャが使用するサンプラー設定(Bilinear)
	switch(m_SamplingFilter){
	case enSamplingNearest:
		//context->PSSetSamplers( 0, 1, &m_SamplerState[0] );
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		break;
	case enSamplingBiLenear:
		//context->PSSetSamplers( 0, 1, &m_SamplerState[1] );
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		break;
	default:
		break;
	}

		//ゲーム画面を描画
		glDrawElements( GL_TRIANGLES , 6 , GL_UNSIGNED_INT  , &m_pIndexBuffer[(idx_max+6)*4] );
	}
	//--------------------------------------------------
	//プライオリティ以上の描画は全画面で行う
	//--------------------------------------------------

	start = gxRender::GetInstance()->GetGameOrderMax();
	max   = gxRender::GetInstance()->GetCommandNum();
	renderGameObject( start , max );
}


void COpenGLES3::flip()
{
	//	SwapBuffers( CWindows::GetInstance()->m_WinDC );

	glBindFramebuffer( GL_FRAMEBUFFER, m_FrameBufferIndex );	//最終的な描画バッファに変更

	// バック バッファーと深度ステンシル ビューをクリアします。

	glClearColor( 0xff , 0x00, 0xff , 0x00 );
	glClear( GL_COLOR_BUFFER_BIT );

	glBlendFunc( GL_SRC_ALPHA , GL_ONE_MINUS_SRC_ALPHA);
	glBlendEquation( GL_FUNC_ADD );
	glBlendFunc( GL_SRC_ALPHA , GL_ONE_MINUS_SRC_ALPHA);

	// テクスチャが使用するサンプラー設定(Bilinear)
	switch(m_SamplingFilter){
	case enSamplingNearest:
		//context->PSSetSamplers( 0, 1, &m_SamplerState[0] );
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		break;
	case enSamplingBiLenear:
		//context->PSSetSamplers( 0, 1, &m_SamplerState[1] );
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		break;
	default:
		break;
	}

	//Uint32 idx_max = gxRender::GetInstance()->GetIndexNum();
	//glDrawElements( GL_TRIANGLES , 6 , GL_UNSIGNED_INT  , &m_pIndexBuffer[(idx_max+6)*4] );

	Float32 w = 1.0f;

	static const VertexPositionColorTexCoord cubeVertices[] =
	{
		//背景を描く:0
		{
			//0:左上
			StXMFLOAT4(-1.f * w,  1.f * w,  0.f,1),	//pos
			StXMFLOAT4(1.0f, 1.0f, 1.0f , 1.0f) ,	//argb
			StXMFLOAT2(0.f , 1.0f),					//uv
			StXMFLOAT2(0.f , 1.0f - 0.f),			//uvnormal
			StXMFLOAT2(1.0f,1.0f),
			StXMFLOAT2(0.0f,0.0f),
			0.f,
			StXMFLOAT2(1.0f,1.0f),
			StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f),
			StPointLightInfo(), },
		{
			//1:右下
			StXMFLOAT4(1.f * w, -1.f * w,  0.f,1),
			StXMFLOAT4(1.0f, 1.0f, 1.0f , 1.0f),
			StXMFLOAT2(1.f , 0.0f),
			StXMFLOAT2(1.f , 1.0f - 1.f),
			StXMFLOAT2(1.0f,1.0f),
			StXMFLOAT2(0.0f,0.0f),
			0.f,
			StXMFLOAT2(1.0f,1.0f),
			StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f),
			StPointLightInfo() },
		{
			//2:左下
			StXMFLOAT4(-1.f * w, -1.f * w,  0.f,1),
			StXMFLOAT4(1.0f, 1.0f, 1.0f , 1.0f),
			StXMFLOAT2(0.f , 0.0f),
			StXMFLOAT2(0.f , 1.0f - 1.f),
			StXMFLOAT2(1.0f,1.0f),
			StXMFLOAT2(0.0f,0.0f),
			0.f,
			StXMFLOAT2(1.0f,1.0f),
			StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f),
			StPointLightInfo() },
		{
			//3:右上
			StXMFLOAT4(1.f * w,  1.f * w,  0.f,1),
			StXMFLOAT4(1.0f, 1.0f, 1.0f , 1.0f),
			StXMFLOAT2(1.f , 1.0f),
			StXMFLOAT2(1.f , 1.0f - 0.f),
			StXMFLOAT2(1.0f,1.0f),
			StXMFLOAT2(0.0f,0.0f),
			0.f,
			StXMFLOAT2(1.0f,1.0f),
			StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f),
			StPointLightInfo() },
	};

	static Uint32 cubeIndices[] =
	{
		0, 1, 2,
		0, 3, 1,
	};

	StCustomVertex *pVertex = gxRender::GetInstance()->GetVertex(0);

	gxUtil::MemCpy( pVertex , (void*)cubeVertices , sizeof(cubeVertices) );
	gxUtil::MemCpy( &m_pIndexBuffer[0], cubeIndices, sizeof(cubeIndices) );

	glBindBuffer( GL_ARRAY_BUFFER, m_vertexBufferObject);
	glBufferSubData(GL_ARRAY_BUFFER , 0, sizeof(cubeVertices) , pVertex );

	glActiveTexture( GL_TEXTURE0 + enAlbedoTextureMapSlot );
	glBindTexture(GL_TEXTURE_2D, m_OffScreenTexture[enFinalBuffer].texture2D);

    Sint32 winw,winh;
    CGameGirl::GetInstance()->GetWindowsResolution(&winw, &winh);

	StViewPort viewport;
	viewport.Width    = winw;//m_BackBufferSize.Width;
	viewport.Height   = winh;//m_BackBufferSize.Height;
	m_ShadingWindowW = winw;
	m_ShadingWindowH = winh;
	glViewport( 0, 0, viewport.Width , viewport.Height );

	changeShader();

	glDrawElements( GL_TRIANGLES , 6 , GL_UNSIGNED_INT  , &m_pIndexBuffer[0] );

}

void COpenGLES3::Present() 
{
	//画面のフリップ

	if ( !m_bInitCompleted ) return;

#ifdef USE_FLIP
	flip();
#endif

	gxChar* pFileName = CGameGirl::GetInstance()->GetScreenShotFileName();

	if (pFileName)
	{
		GetFrameBufferImage(pFileName);
		CGameGirl::GetInstance()->SetScreenShot(gxFalse);
	}
}

void COpenGLES3::ReadTexture( int texPage  )
{
	//外部からファイルを読み込む

	Uint32 col = 0x0000FF00;
	Uint32 w, h;
	
	w = gxTexManager::enMasterWidth;
	h = gxTexManager::enMasterHeight;

	if ( m_TextureData[texPage].texture2D == 0 )
	{
		//必要なときにだけ作ることにする
		makeTexture( &m_TextureData[texPage], w, h, 32);
	}

	//glActiveTexture(GL_TEXTURE0 );
	glBindTexture(GL_TEXTURE_2D, m_TextureData[texPage].texture2D );
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	//バイリニア
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	//ニアレストネイバー
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	//----------------------------------------------------------------------

	Uint8* pData = gxTexManager::GetInstance()->GetAtlasTexture(texPage)->GetTexelImage();
	size_t sz = 2048 * 2048 * 4;
	Uint8* pData2 = new Uint8[sz];

	Uint32 n = 0;
	for (Sint32 n = 0; n < sz; n += 4)
	{
		pData2[n + 3] = pData[n + 3];	//a
		pData2[n + 0] = pData[n + 2];	//r
		pData2[n + 1] = pData[n + 1];	//g
		pData2[n + 2] = pData[n + 0];	//b
	}

	GLenum err = glGetError();
	glTexImage2D(GL_TEXTURE_2D, 0, m_TextureData[texPage].format, w, h, 0, m_TextureData[texPage].format, GL_UNSIGNED_BYTE, pData2);// gxTexManager::GetInstance()->GetAtlasTexture(texPage)->GetTexelImage() );
	err = glGetError();

	SAFE_DELETES(pData2);
}


gxBool COpenGLES3::makeTexture( TextureData *pTexture , int w , int h , int bitDepth , Uint8 *pData , Uint32 uSize )
{
	//----------------------------------------------------------------
	//書き込み可能なテクスチャの設定
	//----------------------------------------------------------------
	if (pTexture->texture2D != 0)
	{
		glDeleteTextures( 1, &pTexture->texture2D);
	}

	glGenTextures( 1, &pTexture->texture2D );

	glActiveTexture(GL_TEXTURE0 + 0);
	glBindTexture( GL_TEXTURE_2D, pTexture->texture2D );
	glPixelStorei( GL_UNPACK_ALIGNMENT, 1);

	//----------------------------------------------------------------------
	//バイリニアフィルタリング
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	//ニアレストネイバー
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	//----------------------------------------------------------------------

	GLenum err = glGetError();
	Uint32 textureFormat = GL_RGBA;
	if( bitDepth < 32 )
	{
		textureFormat = GL_RGB;
	}

	if( pData )
	{
		glTexImage2D(GL_TEXTURE_2D, 0, textureFormat, w, h, 0, textureFormat, GL_UNSIGNED_BYTE, pData );
		err = glGetError();
	}
	else
	{
		Uint8 *pData2 = new Uint8[w*h*(bitDepth/8)];
		gxUtil::MemSet( pData2 , 0x00 , w*h*(bitDepth/8) );
		
		if( bitDepth == 32 )
		{
			glTexImage2D(GL_TEXTURE_2D, 0, textureFormat, w, h, 0, textureFormat, GL_UNSIGNED_BYTE, pData2 );
		}
		else
		{
			glTexImage2D(GL_TEXTURE_2D, 0, textureFormat, w, h, 0, textureFormat, GL_UNSIGNED_BYTE, pData2 );
		}
		err = glGetError();
		SAFE_DELETES( pData2 );
	}

	pTexture->format = textureFormat;

	return gxTrue;
}


void COpenGLES3::makeWallPaper()
{
	//ノイズテクスチャの作成

	Sint32 wh = 256;
	static Uint32 *p1 = NULL;
	Uint32 cnt = 0;

	if( p1 == NULL )
	{
		p1 = (Uint32*)malloc( wh*wh*4 );
	}

	for( int ii=0;ii<wh; ii++)
	{
		for (int jj = 0; jj < wh; jj++)
		{
            p1[cnt] =0x00;
            //0xFF000000|((gxLib::Rand()%256)<<16)|((gxLib::Rand()%256)<<8)|((gxLib::Rand()%256)<<0);

            cnt ++;
		}
	}

	makeTexture( &m_OffScreenTexture[enWallPaper] , wh , wh , 32 , (Uint8*)p1 , wh*wh*4 );

	//free( p1 );
}


void COpenGLES3::configTextureSampling()
{
	//------------------------------------------------------
	//テクスチャのサンプリング方法の設定
	//------------------------------------------------------


}


void COpenGLES3::configBlendState()
{
	//------------------------------------------------------
	// ブレンドステート（透過テクスチャ用）作成
	//------------------------------------------------------

}


struct ShaderAttributes
{
	gxChar* name;
	Uint32  num;
	Uint32  size;
	GLuint set( GLuint shader , GLuint offset )
	{
		Uint64 _offset = offset;
		GLuint id = glGetAttribLocation( shader, name );
		glEnableVertexAttribArray( id );
		glVertexAttribPointer( id, num, GL_FLOAT, GL_FALSE, sizeof(VertexPositionColorTexCoord), (GLvoid*)(_offset) );
		offset += num * size;
		return offset;
	}
};

ShaderAttributes ShaderDefault[]={
	{ "a_position"	, 4,sizeof(Float32)},
	{ "a_color"		, 4,sizeof(Float32)},
	{ "a_texCoord"	, 2,sizeof(Float32)},
	{ "a_nmlCoord"	, 2,sizeof(Float32)},
	{ "a_scale"		, 2,sizeof(Float32)},
	{ "a_offset"	, 2,sizeof(Float32)},
	{ "a_rotation"	, 1,sizeof(Float32)},
	{ "a_flip"		, 2,sizeof(Float32)},
	{ "a_blend"		, 4,sizeof(Float32)},
	{ "a_pointlight_pos"   , 3,sizeof(Float32)},
	{ "a_pointlight_rgb"   , 3,sizeof(Float32)},
	{ "a_pointlight_length", 1,sizeof(Float32)},
    { "a_options"   , 4 ,sizeof(Float32)},
};

ShaderAttributes ShaderHighSpeed[] = {

	{ "a_position"	, 4,sizeof(Float32)},
	{ "a_color"		, 4,sizeof(Float32)},
	{ "a_texCoord"	, 2,sizeof(Float32)},
	{ "a_nmlCoord"	, 2,sizeof(Float32)},
	{ "a_scale"		, 2,sizeof(Float32)},
	{ "a_offset"	, 2,sizeof(Float32)},
	{ "a_rotation"	, 1,sizeof(Float32)},
	{ "a_flip"		, 2,sizeof(Float32)},
	{ "a_blend"		, 4,sizeof(Float32)},
};

void COpenGLES3::changeShader(ETypeShader sheaderIndex , Float32 *pFloatValue )
{
	//------------------------------------------------------
	// シェーダーを変更する
	//------------------------------------------------------

	glEnableVertexAttribArray( 0 );

	if(sheaderIndex == enShaderDefault )
	{
		//ノーマルシェーダー

		GLuint shader = m_ShaderProg[ enShaderDefault ];
		glUseProgram( shader );

		GLuint offset = 0;
		for( Sint32 ii=0; ii<ARRAY_LENGTH(ShaderDefault); ii++ )
		{
			ShaderAttributes *p = &ShaderDefault[ii];
			offset = p->set( shader , offset );
		}

		GLfloat screenSize[2] = { m_ShadingWindowW*1.0f , m_ShadingWindowH*1.0f};
		Sint32 screenSizeID = glGetUniformLocation(shader, "u_screen");
		glUniform2fv(screenSizeID, 1, screenSize );

		int textureID1;
		textureID1 = glGetUniformLocation(shader, "u_textureID1" );
		glUniform1i( textureID1, 0 );

	}
	else if (sheaderIndex == enShaderGreyScale)
	{
		GLuint shader = m_ShaderProg[enShaderGreyScale];
		glUseProgram(shader);

		GLuint offset = 0;
		for (Sint32 ii = 0; ii < ARRAY_LENGTH(ShaderDefault); ii++)
		{
			ShaderAttributes* p = &ShaderDefault[ii];
			offset = p->set(shader, offset);
		}

		GLfloat screenSize[2] = { m_ShadingWindowW * 1.0f , m_ShadingWindowH * 1.0f };
		Sint32 screenSizeID = glGetUniformLocation(shader, "u_screen");
		glUniform2fv(screenSizeID, 1, screenSize);

		int textureID1;
		textureID1 = glGetUniformLocation(shader, "u_textureID1");
		glUniform1i(textureID1, 0);

	}
	else if (sheaderIndex == enShaderHighSpeed)
	{
		GLuint shader = m_ShaderProg[ enShaderHighSpeed ];
		glUseProgram( shader );

		GLuint offset = 0;
		for( Sint32 ii=0; ii<ARRAY_LENGTH(ShaderHighSpeed); ii++ )
		{
			ShaderAttributes *p = &ShaderHighSpeed[ii];
			offset = p->set( shader , offset );
		}

		GLfloat screenSize[2] = { m_ShadingWindowW*1.0f , m_ShadingWindowH*1.0f};
		Sint32 screenSizeID = glGetUniformLocation(shader, "u_screen");
		glUniform2fv(screenSizeID, 1, screenSize );

		int textureID1;
		textureID1 = glGetUniformLocation(shader, "u_textureID1" );
		glUniform1i( textureID1, 0 );
	}
	else if(sheaderIndex == enShaderBloom )
	{
		//Bloomシェーダー
		GLuint shader = m_ShaderProg[ enShaderBloom ];
		glUseProgram( shader );

		GLuint offset = 0;
		for( Sint32 ii=0; ii<ARRAY_LENGTH(ShaderDefault); ii++ )
		{
			ShaderAttributes *p = &ShaderDefault[ii];
			offset = p->set( shader , offset );
		}

		GLfloat screenSize[2] = { m_ShadingWindowW*1.0f , m_ShadingWindowH*1.0f};
		Sint32 screenSizeID = glGetUniformLocation(shader, "u_screen");
		glUniform2fv(screenSizeID, 1, screenSize );

		GLfloat option[enOptionNum] = { 1.0 , 0.0 };
		Sint32 optionID = glGetUniformLocation(shader, "u_option");
		glUniform1fv(optionID, enOptionNum, option);

		int textureID1;
		textureID1 = glGetUniformLocation(shader, "u_textureID1" );
		glUniform1i(textureID1, 0 );
	}
	else if(sheaderIndex == enShaderBlur )
	{
		//Blurシェーダー

		GLuint shader = m_ShaderProg[ enShaderBlur ];
		glUseProgram( shader );

		GLuint offset = 0;
		for( Sint32 ii=0; ii<ARRAY_LENGTH(ShaderDefault); ii++ )
		{
			ShaderAttributes *p = &ShaderDefault[ii];
			offset = p->set( shader , offset );
		}

		GLfloat screenSize[2] = { m_ShadingWindowW*1.0f , m_ShadingWindowH*1.0f};
		Sint32 screenSizeID = glGetUniformLocation(shader, "u_screen");
		glUniform2fv(screenSizeID, 1, screenSize );

		GLfloat options[enOptionNum] = { 1.0f , 1.0f };
		Sint32 optionsID = glGetUniformLocation(shader, "u_option");
		glUniform1fv( optionsID, enOptionNum, options );


		GLfloat weights[10] = {};

		if( pFloatValue[1] == 0.0f )
		{
			//通常のガウスブラー
			Float32 eRange_value = pFloatValue[0]*100;
			eRange_value = CLAMP(eRange_value,10.0f, 100.0f);

			Float32 t = 0.0f;
			Float32 d = eRange_value * eRange_value / 10.0f;
			for( Sint32 i = 0; i < 10; i++){
			    Float32 r = 1.0 + 2.0 * i;
			    Float32 w = exp(-0.5 * (r * r) / d);
			    weights[i] = w;
			    if(i > 0){w *= 2.0;}
			    t += w;
			}

			for(Sint32 i = 0; i < 10; i++){
			    weights[i] /= t;
			}
		}
		else
		{
			//Bloom用
			static Float32 weight_temp[10]={
				0.111111112f,
				0.0999999940f,
				0.0888888910f,
				0.0777777731f,
				0.0666666701f,
				0.0555555560f,
				0.0444444455f,
				0.0333333351f,
				0.0222222228f,
				0.0111111114f,
			};
			for (Sint32 i = 0; i < 10; i++) {
			//	weights[i] = (weight_temp[i]/9.0f)* pFloatValue[0];
				weights[i] = weight_temp[i];
			}
		}

		Sint32 weightID = glGetUniformLocation(shader, "u_weight");
		glUniform1fv( weightID, 10, weights );

		int textureID1;
		textureID1 = glGetUniformLocation(shader, "u_textureID");
		glUniform1i(textureID1, 0);

	}
	else if(sheaderIndex == enShaderRaster )
	{
		//Rasterシェーダー

		GLuint shader = m_ShaderProg[ enShaderRaster ];
		glUseProgram( shader );

		GLuint offset = 0;
		for( Sint32 ii=0; ii<ARRAY_LENGTH(ShaderDefault); ii++ )
		{
			ShaderAttributes *p = &ShaderDefault[ii];
			offset = p->set( shader , offset );
		}

		GLfloat screenSize[2] = { m_ShadingWindowW*1.0f , m_ShadingWindowH*1.0f};
		Sint32 screenSizeID = glGetUniformLocation(shader, "u_screen");
		glUniform2fv(screenSizeID, 1, screenSize );

		GLfloat option[enOptionNum] = { 1.0 , 0.0 };
		Sint32 optionID = glGetUniformLocation(shader, "u_option");
		glUniform1fv(optionID, enOptionNum, option);

		int textureID1;
		textureID1 = glGetUniformLocation(shader, "u_textureID1" );
		glUniform1i(textureID1, 0 );

	}
	else if (sheaderIndex == enShaderNormal )
	{
		//法線マップシェーダー
		GLuint shader = m_ShaderProg[enShaderNormal];
		glUseProgram(shader);

		GLuint offset = 0;
		for (Sint32 ii = 0; ii < ARRAY_LENGTH(ShaderDefault); ii++)
		{
			ShaderAttributes* p = &ShaderDefault[ii];
			offset = p->set(shader, offset);
		}

		GLfloat screenSize[2] = { m_ShadingWindowW * 1.0f , m_ShadingWindowH * 1.0f };
		Sint32 screenSizeID = glGetUniformLocation(shader, "u_screen");
		glUniform2fv(screenSizeID, 1, screenSize);

		int textureID1;
		textureID1 = glGetUniformLocation(shader, "u_textureID1");
		glUniform1i(textureID1, enAlbedoTextureMapSlot);

		int textureID2;
		textureID2 = glGetUniformLocation(shader, "u_textureID2");
		glUniform1i(textureID2, enNormalTextureMapSlot);

		int textureID3;
		textureID3 = glGetUniformLocation(shader, "u_textureID3");
		glUniform1i(textureID3, enCaptureTextureMapSlot);

		//int textureID2;
		//textureID2 = glGetUniformLocation(shader, "u_texNormal");
		//glUniform1i(textureID2, 1);

		//法線マップテクスチャスロット

		//ライト位置
//		Float32 fRot = (gxLib::GetGameCounter()*2)%360;
//		Float32 mx = 2*gxUtil::Touch()->GetPosition()->x/m_GameW - 1.0f;
//		Float32 my = 2 * gxUtil::Touch()->GetPosition()->y/m_GameH - 1.0f;
//		my *= -1.0f;
//		Float32 pointlight_param[10] = { mx , my , 1.0 , 1.0, 1.0 ,    /*argb*/1.0, 1.0 , 1.0 , 1.0 };
//		int pointLightID1;
//		pointLightID1 = glGetUniformLocation(shader, "u_PLight");
//		glUniform1fv(pointLightID1, 10 , pointlight_param );

	}
    else if(sheaderIndex == enShaderFont )
    {
        //フォントシェーダー
        
        GLuint shader = m_ShaderProg[ enShaderFont ];
        glUseProgram( shader );

        GLuint offset = 0;
        for( Sint32 ii=0; ii<ARRAY_LENGTH(ShaderDefault); ii++ )
        {
            ShaderAttributes *p = &ShaderDefault[ii];
            offset = p->set( shader , offset );
        }
        
        GLfloat screenSize[2] = { m_ShadingWindowW*1.0f , m_ShadingWindowH*1.0f};
        Sint32 screenSizeID = glGetUniformLocation(shader, "u_screen");
        glUniform2fv(screenSizeID, 1, screenSize );
        
        int textureID1;
        textureID1 = glGetUniformLocation(shader, "u_textureID1" );
        glUniform1i( textureID1, 0 );

		//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    }
	else if (sheaderIndex == enShaderDev )
	{
		GLuint shader = m_ShaderProg[enShaderDev];
		glUseProgram(shader);

		GLuint offset = 0;
		for (Sint32 ii = 0; ii < ARRAY_LENGTH(ShaderDefault); ii++)
		{
			ShaderAttributes* p = &ShaderDefault[ii];
			offset = p->set(shader, offset);
		}

		GLfloat screenSize[2] = { m_ShadingWindowW * 1.0f , m_ShadingWindowH * 1.0f };
		Sint32 screenSizeID = glGetUniformLocation(shader, "u_screen");
		glUniform2fv(screenSizeID, 1, screenSize);

		int textureID1;
		textureID1 = glGetUniformLocation(shader, "u_textureID1");
		glUniform1i(textureID1, 0);

		int textureID2;
		textureID2 = glGetUniformLocation(shader, "u_textureID2");
		glUniform1i(textureID2, 1);

		Float32 fRot = (gxLib::GetGameCounter()*2)%360;

		Float32 mx = 2*gxUtil::Touch()->GetPosition().x/m_GameW - 1.0f;
		Float32 my = 2 * gxUtil::Touch()->GetPosition().y/m_GameH - 1.0f;
		my *= -1.0f;
		Float32 pointlight_param[10] = { mx , my , 1.0 , 1.0, 1.0 ,    /*argb*/1.0, 1.0 , 1.0 , 1.0 };
		int pointLightID1;
		pointLightID1 = glGetUniformLocation(shader, "u_PLight");
		glUniform1fv(pointLightID1, 10 , pointlight_param );

	}

}


void COpenGLES3::GetFrameBufferImage(gxChar* pFileName)
{
	// フレームバッファの内容をPBOに転送

	// PBO作成とバインド
	Uint32 length = m_GameW * m_GameH * 3;

}


void COpenGLES3::_CheckError()
{
	GLenum err = glGetError();

	if( err != GL_NO_ERROR )
	{
		GX_DEBUGLOG("err[%d]",err);
	}
}


#endif
