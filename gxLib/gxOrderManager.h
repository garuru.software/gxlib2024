//--------------------------------------------------
//
// gxOrderManager.h
//
//--------------------------------------------------

#ifndef _GGXORDER_MANAGER_H_
#define _GGXORDER_MANAGER_H_

 enum
{
	enOrderMax = MAX_ORDER_NUM,
};

enum
{
	enOrderTypePoint,
	enOrderTypeLine,
	enOrderTypeTriangle,
	enOrderTypWired,
	enOrderTypeNoneTexPolygon,
	enOrderTypeTexturePolygon,
	enOrderTypeTextureTriangle,
	enOrderTypeFont,
	enOrderTypeChangeRenderTarget,
	enOrderTypeChangeScissor,
	enOrderTypeProcessingBlur,
	enOrderTypeProcessingBloom,
	enOrderTypeCaptureScreen,
	enOrderTypeProcessingGreyScale,
	enOrderTypeDevelop,
};


typedef struct StOrder {
	Sint32  sType;			// 0	//オーダータイプ
	Sint32  sAtribute;		// 4	//アトリビュート
	Sint32  x,y,prio;			// 12	//表示位置
	Float32 sx,sy,sz;		// 12	//拡大率
	Float32 rx,ry,rz;		// 12	//回転
	Sint32  u,v,w,h,cx,cy;	// 24	//テクスチャ座標＆中心位置

	Uint32 color[4];		// 16
	Uint32 blend;			// 4	//ブレンド色
	void    *pString;		// 8    //文字列へのポインタ（フォント用）

	Uint32 opt;				//		オプション

	Sint32  pg[4];			// 4	テクスチャページ
	Float32 x1[4],y1[4],z1[4],u1[4],v1[4];	//表示用の４頂点とテクスチャのＵＶｘ４

	//テクスチャマッピング用ＵＶオフセット、WHはalbedo用を使用する
	//0:a;bedo(diffuse) / normal / pallet
	Sint32  map_u[3];
	Sint32  map_v[3];

	//点光源制御用
	Float32 plight_pos[3];
	Float32 plight_rgb[4];
	Float32 plight_intensity;

	StOrder *m_pNextOrder;	//同一プリミティブがある場合の処理
    Sint32  shader = 0;
    Float32 option[4] ={0};

} StOrder;

class gxOrderTable
{

public:
	gxOrderTable();
	~gxOrderTable();

	StOrder* get( Sint32 n )
	{
		return &m_pOrder[n];
	}

	Sint32 set( StOrder* pOrder , gxBool bSubOrderEnable = gxTrue );

	void Reset();

	void ZSort();

	Uint32* getZsortBuf()
	{
		return m_zIndex;
	}

	Sint32 GetOrderNum()
	{
		return m_sCount;
	}

	Sint32 GetSubOrderNum()
	{
		return m_uSubOrderCnt;
	}

	void SetRequestEnable( gxBool bEnable )
	{
		m_bRequestEnable = bEnable;
	}

	gxBool IsRequestEnable()
	{
		return m_bRequestEnable;
	}

private:

	void QSort(Sint32* zsort,Uint32* zIndex, int left, int right);

	StOrder* m_pOrder;
	Sint32   m_sCount;
	Sint32   m_sStringCnt;
	Sint32   m_zSort[MAX_ORDER_NUM] = {0};
	Uint32   m_zIndex[MAX_ORDER_NUM] = {0};

	Uint32 m_uSubOrderCnt;
	StOrder* m_pSubOrder;
	StOrder* m_pLastOrder;

	gxBool m_bRequestEnable;
};


class gxOrderManager
{
public:
	gxOrderManager();
	~gxOrderManager();

	Sint32 set(StOrder* pOrder , gxBool bSubOrderEnable = gxTrue)
	{
		Sint32 uCurrent = m_uCurrent;
		if( uCurrent < 0 ) return 0;
		return m_pOrderTable[uCurrent].set( pOrder ,bSubOrderEnable );
	}

	//------------------------

	StOrder* get( Sint32 n )
	{
		Sint32 uCurrent = m_uDrawLayer;// m_uNewerLayer;
		if( uCurrent < 0 ) return NULL;

		return m_pOrderTable[uCurrent].get( n );
	}

	void ZSort()
	{
		Sint32 uCurrent = m_uDrawLayer;// m_uNewerLayer;
		if( uCurrent < 0 ) return;

		m_pOrderTable[uCurrent].ZSort();
	}

	Sint32 GetOrderNum()
	{
		Sint32 uCurrent = m_uDrawLayer;// m_uNewerLayer;
		if( uCurrent < 0 ) return 0;

		return m_pOrderTable[uCurrent].GetOrderNum();
	}

	void GetUpdateOrderNum( Sint32 *odr , Sint32 *sub )
	{
		Sint32 uCurrent = m_uCurrent;
		if (uCurrent < 0) return;

		*odr = 0;
		*sub = 0;

		*odr = m_pOrderTable[uCurrent].GetOrderNum();
		*sub = m_pOrderTable[uCurrent].GetSubOrderNum();
	}

	Sint32 GetSubOrderNum()
	{
		Sint32 uCurrent = m_uDrawLayer;// m_uNewerLayer;
		if( uCurrent < 0 ) return 0;

        return m_pOrderTable[uCurrent].GetSubOrderNum();

    }

	Uint32* getZsortBuf()
	{
		Sint32 uCurrent = m_uDrawLayer;// m_uNewerLayer;
		if( uCurrent < 0 ) return 0;
		return m_pOrderTable[uCurrent].getZsortBuf();
	}

	//---------------

	void SetRequestEnable( gxBool bEnable )
	{
		Sint32 uCurrent = m_uCurrent;
		if( uCurrent < 0 ) return;

		m_pOrderTable[uCurrent].SetRequestEnable(bEnable);
	}

	gxBool IsRequestEnable()
	{
		Sint32 uCurrent = m_uCurrent;
		if( uCurrent < 0 ) return gxFalse;

		return m_pOrderTable[uCurrent].IsRequestEnable();
	}


	//---------------------

	void RequestInit();
	void ChangeRequestBuffer();
	gxBool IsDrawable();
	gxBool DrawInit();

	void Reset()
	{
		m_pOrderTable[ m_uDrawLayer ].Reset();
	}

	void DrawEnd()
	{

	}

	SINGLETON_DECLARE( gxOrderManager );

private:
	Sint32		 m_uCurrent;
	Sint32		 m_uDrawLayer;
	Sint32		 m_uNewerLayer;
	gxOrderTable m_pOrderTable[3];

};


#endif


