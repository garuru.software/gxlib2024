﻿
#define WIN32_LEAN_AND_MEAN
#include <gxLib.h>
#include <gxLib/Util/CFileTxt.h>
#include "CXML.h"

#define _DEBUG_LOG_DISP

CXML::Node* CXML::adjustRoot( gxChar *pString )
{
	tinyxml2::XMLElement *pElem;
	CXML::Node *ret = nullptr;
	CXML::Node* pRoot = nullptr;

	if( pString )
	{
		pElem = searchRout( pString );
	}
	else
	{
		pElem = m_pXML->FirstChildElement();
	}

	if( pElem )
	{
		const char *pName = pElem->Name();

		SAFE_DELETE(pRoot);
		pRoot = new CXML::Node();
		pRoot->SetElement( pElem );
		ret = pRoot->makeTree( (gxChar*)pName );

#ifdef _DEBUG_LOG_DISP
		GX_DEBUGLOG("ROOT[%s]", pRoot->Name());
#endif
	}

	return ret;
}


tinyxml2::XMLElement* CXML::searchRout( gxChar *pString )
{
	tinyxml2::XMLElement *element;
	element = m_pXML->FirstChildElement( pString );

	if( element == NULL )
	{
		//routを探してそこから検索する
		element = m_pXML->FirstChildElement();

		if( element == NULL )
		{
			//routすらない
			return NULL;
		}
	}

	tinyxml2::XMLElement *child;
	child = searchChild( element , pString );

	if( child )
	{
#ifdef _DEBUG_LOG_DISP
//		GX_DEBUGLOG("-[%s]",element->Name() );
#endif
		return child;
	}

	return element;
}

tinyxml2::XMLElement* CXML::searchChild( tinyxml2::XMLElement *element , gxChar *pString )
{
	if( element )
	{
#ifdef _DEBUG_LOG_DISP
//		GX_DEBUGLOG("[%s]",element->Name() );
#endif
	}

	if( isSame( (gxChar*)element->Name() , pString ) ) return element;

	tinyxml2::XMLElement *child;

	child = element->FirstChildElement();

	if( child )
	{
		child = searchChild( child , pString );
		if( child != NULL ) return child;
	}

	element = element->NextSiblingElement();

	if( element )
	{
		element = CXML::searchChild( element , pString );
		if( element != NULL ) return element;
	}

	return NULL;
}

gxBool CXML::isSame( gxChar *str1 , gxChar *str2 )
{

	if( str1 == NULL || str2 == NULL ) return gxFalse;

	if( strcmp( str1 , str2 ) == 0 ) return gxTrue;

	return gxFalse;
}

CXML::Node* CXML::Node::makeTree( gxChar *pName )
{
	Sint32 broNum = 0;
	tinyxml2::XMLElement *element;

	m_sChildNum   = 0;
	m_sBrotherNum = 0;
	SAFE_DELETES( m_pReturnStaticNode );
	SAFE_DELETES( m_pChildArray );

	element = CXML::searchChild( m_pElement , pName );

	if( element )
	{
		m_pReturnStaticNode = analyseBrother( element , &m_sBrotherNum ,&m_sChildNum );
	}

	return m_pReturnStaticNode;
}


CXML::Node* CXML::Node::analyseBrother( tinyxml2::XMLElement *element , Sint32 *pNum , Sint32 *pChildNum )
{
	Sint32 broNum = 0;

	broNum ++;
	tinyxml2::XMLElement *bro = element;

	while( gxTrue )
	{
		bro = bro->NextSiblingElement();
		if( bro )	broNum ++;
		else break;
	}

	if( broNum == 0 ) return NULL;

	CXML::Node *p;
	p = new CXML::Node[ broNum ];

	tinyxml2::XMLElement *chi;
	Sint32 childNum = 0;

	for( Sint32 ii=0; ii<broNum; ii++ )
	{
		p[ii].SetElement( element );
		//p[ii].SetBrotherNum( broNum );
		p[ii].m_sBrotherNum = broNum;
		chi = element->FirstChildElement();

		if( chi )
		{
			Node *p2;
			p2 = analyseBrother( chi , &childNum );
			p[ii].SetChildren( p2 , childNum );
		}
		else
		{
			p[ii].SetChildren( NULL , 0 );
		}

		element = element->NextSiblingElement();
	}

	*pNum = broNum;
	if( pChildNum ) *pChildNum = childNum;

	return p;
}


CXML::Node* CXML::Node::Get( gxChar* pName )
{
	size_t len = strlen( pName );
	gxChar *p[32];
	gxChar string[256];
	Uint32 splitPos[256];
	Sint32 cnt = 0;

	sprintf( string , "%s" , pName );

	p[0] = &string[0];
	splitPos[cnt] = 0;
	cnt ++;

	for( Sint32 ii=0;ii<len;ii++)
	{
		if( pName[ii] == '/' )
		{
			splitPos[cnt] = ii;
			string[ii] = 0x00;
			p[cnt] = &string[ii+1];
			cnt ++;
		}
	}

	//gxLib::DebugLog( "%s" , Name() );

	CXML::Node *pReturnNode = NULL;

	if (cnt == 1)
	{
		if (CXML::isSame(Name(), p[0]))
		{
			return this;
		}
	}
		
	Sint32 cNum = GetChildNum();

	for (Sint32 kk = 0; kk < cNum; kk++)
	{
		CXML::Node *pChild = GetChild(kk);
		if (pChild)
		{
			if (CXML::isSame(pChild->Name(), p[1]))
			{
				return pChild->Get(&pName[splitPos[1] + 1]);
			}
		}
	}
				
	return NULL;
}

#if 0
tinyxml2::XMLDocument doc;
doc.LoadFile( "test.xml" );
const char * pSource = "<html><head></head><body><p>hello, world!</p></body></html>";
tinyxml2::XMLDocument doc;
doc.Parse(pSource);
doc.Print();

//.xml ファイルの読み込み - tinyxml2::XMLDocument::LoadFile
tinyxml2::XMLDocument doc;
doc.LoadFile( "test.xml" );

//文字列から読み込み - tinyxml2::XMLDocument::Parse
const char * pSource = "<html><head></head><body><p>hello, world!</p></body></html>";
tinyxml2::XMLDocument doc;
doc.Parse(pSource);
doc.Print();

//ルート要素の取得と、要素内容の取得 - tinyxml2::XMLDocument::FirstChildElement
const char * pSource = "<title>The Marshmallow Times</title>";
tinyxml2::XMLDocument doc;
doc.Parse(pSource);

tinyxml2::XMLElement * elem = doc.FirstChildElement("title");
std::cout << elem->GetText() << std::endl; // "The Marshmallow Times"

//要素内の要素を取得、要素名の取得 - tinyxml2::XMLElement::FirstChildElement
const char * pSource = "<book><title>The Marshmallow Times</title></book>";
tinyxml2::XMLDocument doc;
doc.Parse(pSource);

tinyxml2::XMLElement * elem = doc.FirstChildElement("book")->FirstChildElement("title");
std::cout << elem->Name() << std::endl;    // "title"
std::cout << elem->GetText() << std::endl; // "The Marshmallow Times"

//要素の次の要素を取得 - tinyxml2::XMLElement::NextSiblingElement
const char * pSource = "<book><title>The Marshmallow Times</title><author>Lun Lun Yamamoto</author></book>";
tinyxml2::XMLDocument doc;
doc.Parse(pSource);

tinyxml2::XMLElement * elem = doc.FirstChildElement("book")->FirstChildElement();
std::cout << elem->Name() << std::endl;                       // "title"
std::cout << elem->NextSiblingElement()->Name() << std::endl; // "author"


//要素の追加 - tinyxml2::XMLElement::NewElement, tinyxml2::XMLElement::InsertEndChild
const char * pSource = "<book><title>The Marshmallow Times</title></book>";
tinyxml2::XMLDocument doc;
doc.Parse(pSource);

tinyxml2::XMLElement * root = doc.FirstChildElement();
tinyxml2::XMLElement * newElement = doc.NewElement( "author" );
root->InsertEndChild(newElement);
doc.Print();


//要素の属性（Attribute）を取得 - tinyxml2::XMLElement::Attribute
const char * pSource = "<book title=\"The Marshmallow Times\"/>";
tinyxml2::XMLDocument doc;
doc.Parse(pSource);

tinyxml2::XMLElement * bookElem = doc.FirstChildElement("book");
const char * bookTitle = bookElem->Attribute("title");
if ( nullptr != bookTitle )
  std::cout << bookTitle << std::endl; //The Marshmallow Times
#endif



int CXML::getChildCSV(tree &p_node, CCsv& csv, int x, int y)
{
	if (p_node.tag != "" )
	{
		csv.SetCell(x, y, "<%s>", p_node.tag.c_str());
	}

	std::string str = p_node.value;
	if (str != "")
	{
		csv.SetCell(x + 1, y, str.c_str());
	}

	//int attrNum = p_node.attr.size();
	//if (attrNum > 0)
	//{
	//	for (int ii = 0; ii < attrNum; ii++)
	//	{
	//		y++;
	//		csv.SetCell(x + 0, y, "(%s)", p_node.attr[ii].title.c_str());
	//		csv.SetCell(x + 1, y, "%s", p_node.attr[ii].data.c_str());
	//	}
	//}

	size_t num = p_node.array.size();

	if (num > 0)
	{
		for (size_t ii = 0; ii < num; ii++)
		{
			y = getChildCSV(p_node.array[ii], csv, x + 1, y + 1);
		}
	}

	return y;
}

gxBool CXML::SaveCSV( std::string fileName )
{
	CCsv csv;

	getChildCSV(m_gxXML, csv, 0, 0);
	
	csv.Save(fileName.c_str());

	return gxTrue;
}


void CXML::search(Node *pSlot , tree &list )
{
	std::string dat;
	dat = pSlot->Name();
	if (dat != "No Name")
	{
		list.tag = dat;
	}

	dat = pSlot->Data();
	if (dat != "No Data")
	{
		list.value = dat;
	}

	int n = pSlot->GetAttributeNum();
	for (int ii = 0; ii < n; ii++)
	{
		const char* p = pSlot->GetAttributeName(ii);
		if (p)
		{
			CXML::Attr atr;
			atr.first = p;

			dat = pSlot->Attribute((gxChar*)atr.first.c_str());
			if (dat != "No Attribute")
			{
				atr.second = dat;

			}
			list.atr.push_back(atr);
		}
	}

	Sint32 childNum = pSlot->GetChildNum();

	for (Sint32 ii = 0;ii< childNum;ii++)
	{
		tree chld;
		list.array.push_back(chld);
		search(pSlot->GetChild(ii), list.array.back());
	}

}

gxBool CXML::Read(gxChar* pString, size_t uSize)
{
	m_ErrorReason = "No Error";
	SAFE_DELETE(m_pXML);

	m_pXML = new tinyxml2::XMLDocument();
	m_pXML->Parse((const char*)pString, uSize);

	//m_XMLFileSize = strlen((const char*)pString);

	tree temp;
	m_gxXML = temp;

	Node *pSlot;
	pSlot = adjustRoot();
	if (pSlot == nullptr)
	{
		m_ErrorReason = "XML Perse Error";
		gxLib::DebugLog(m_ErrorReason.c_str());
		return gxFalse;
	}
	for (int ii = 0; ii < pSlot->GetBrotherNum(); ii++)
	{
		Node* pSlot2;
		if (pSlot->GetBrotherNum() == 1)
		{
			pSlot2 = pSlot;
		}
		else
		{
			pSlot2 = pSlot->GetBrother(ii);
		}
		search(pSlot2, m_gxXML);
	}

	return gxTrue;
}


gxBool CXML::Load(gxChar* pFileName)
{
	Uint8* pData;
	size_t XMLFileSize = 0;
	pData = gxLib::LoadFile(pFileName, &XMLFileSize);

	if (pData == NULL)
	{
		m_ErrorReason = "File not found";
		return gxFalse;
	}

	gxBool bSuccess = gxTrue;
	bSuccess = Read((gxChar*)pData, XMLFileSize);

	delete[] pData;

	return bSuccess;
}


int CXML::make_element_from_csv(CXML::tree& parent, CCsv& csv, int x, int y)
{
	std::string str;

	CXML::tree* cur_info = nullptr;
	for (int ii = y; ii < csv.GetHeight(); ii++)
	{
		if (x >= 1) {
			csv.GetCell(x - 1, ii, str);
			if (str[0] == '<')
			{
				//find bros.
				return 0;
			}
		}
		csv.GetCell(x, ii, str);
		if (str == "")
		{
		}
		else if (str[0] == '<')
		{
			CXML::tree child;
			//child.title = str;
			size_t sz = str.size();
			child.tag = str.substr(1, sz - 2);

			std::string data_str;
			csv.GetCell(x + 1, ii, data_str);
			if (data_str != "")
			{
				child.value = data_str;
			}

			parent.array.push_back(child);

			size_t n = parent.array.size() - 1;
			cur_info = &parent.array[n];
			make_element_from_csv(*cur_info, csv, x + 1, ii + 1);
		}
		else if (str[0] == '(')
		{
			CXML::Attr _attr;

			if (cur_info)
			{
				std::string attr_str;

				//_attr.title = str;
				size_t sz = str.size();
				_attr.first = str.substr(1, sz - 2);

				csv.GetCell(x + 1, ii, attr_str);
				_attr.second = attr_str;
				cur_info->atr.push_back(_attr);
			}
		}
	}

	return 0;
}


void CXML::make_xml_from_elements(CXML::tree& xml, CTxt& txt, int x, int y)
{
#if 1
	if (xml.tag != "")
	{
		for (int ii = 0; ii < x; ii++)
		{
			txt.printf(" ");
		}

		if (xml.atr.size() > 0)
		{
			//printf("<%s>", xml.title.c_str());
			txt.printf("<%s", xml.tag.c_str());
			for (int ii = 0; ii < xml.atr.size(); ii++)
			{
				txt.printf(" %s=\"%s\"", xml.atr[ii].first.c_str(), xml.atr[ii].second.c_str());
			}
			txt.printf(">");
		}
		else
		{
			txt.printf("<%s>", xml.tag.c_str());
		}
		if (xml.value != "")
		{
			txt.printf("%s", xml.value.c_str());
		}

		if (xml.array.size() > 0)
		{
			txt.printf("\n");
			for (int ii = 0; ii < xml.array.size(); ii++)
			{
				make_xml_from_elements(xml.array[ii], txt, x + 1, y);
			}

			for (int ii = 0; ii < x; ii++)
			{
				txt.printf(" ");
			}
			txt.printf("</%s>\n", xml.tag.c_str());
		}
		else
		{
			txt.printf("</%s>\n", xml.tag.c_str());
		}
	}
	else if (xml.array.size() > 0)
	{
		for (int ii = 0; ii < xml.array.size(); ii++)
		{
			make_xml_from_elements(xml.array[ii], txt, x + 1, y);
		}
	}
#endif
}

std::string CXML::ToString()
{
	CTxt txt;
	txt.SetLF("\n");
	make_xml_from_elements(m_gxXML, txt, 0, 0);

	std::string str = txt.ToString();

	return str;
}

gxBool CXML::Save(std::string fileName)
{
	CTxt txt;

	make_xml_from_elements(m_gxXML, txt, 0, 0);

	//CTxt::POS pos = { 0,0 };
	auto str = txt.ToString();

	return gxLib::SaveFile(fileName.c_str(), (char*)str.c_str(), str.size());
}

//CXML::stElementInfo diffxml;

CXML::tree getDiff(CXML::tree* src, CXML::tree* dst)
{
	//srcとdstを比べてdstにしかないものをリストアップする

	CXML::tree diffxml;

	if (src->tag != dst->tag)
	{
		diffxml.tag = dst->tag;
	}

	//attrチェック
	for (int ii = 0; ii < src->atr.size(); ii++)
	{
		bool sameChildExist = false;
		for (int jj = 0; jj < dst->atr.size(); jj++)
		{
			if (src->atr[ii] == dst->atr[jj])
			{
				//同じ奴がいた
				sameChildExist = true;
				break;
			}
		}

		if (!sameChildExist)
		{
			diffxml.atr.push_back(src->atr[ii]);
		}
	}

	//ここまでは違いがないがchildの内容おいてことなる場合があるか確認

	//for (int ii = 0; ii < src->array.size(); ii++)
	//{
	//	bool sameChildExist = false;
	//	for (int jj = 0; jj < dst->array.size(); jj++)
	//	{
	//		CXML::stElementInfo diff;
	//
	//		//順番が異なるだけで同じやつがいないかチェックする
	//		if (src->array[ii] == dst->array[jj])
	//		{
	//			//同じ奴がいた
	//			sameChildExist = true;
	//			break;
	//		}
	//	}
	//
	//	if (!sameChildExist)
	//	{
	//		//同じ奴を発見できずSRCにしかないものだった
	//		diffxml.array.push_back(src->array[ii]);
	//	}
	//}

	for (int ii = 0; ii < dst->array.size(); ii++)
	{
		bool sameChildExist = false;
		for (int jj = 0; jj < src->array.size(); jj++)
		{
			CXML::tree diff;

			//順番が異なるだけで同じやつがいないかチェックする
			if (dst->array[ii] == src->array[jj])
			{
				//同じ奴がいた
				sameChildExist = true;
				break;
			}
		}

		if (!sameChildExist)
		{
			//同じ奴を発見できずDSTにしかないものだった
			diffxml.array.push_back(dst->array[ii]);
		}
	}

	return diffxml;
}


CXML::tree CXML::GetDiff(tree* src, tree* dst)
{
	// src + dstするための差分を検出する(dstにしかないものを検出する)
	CXML::tree diffdst;

	diffdst = getDiff(src,dst);

	return diffdst;
}


//gxBool CXML::LoadCSV(std::string fileName)
//{
//	CCsv csv;
//	csv.LoadFile(fileName.c_str());
//	int w = csv.GetWidth();
//	int h = csv.GetHeight();
//
//	stElementInfo temp;
//	m_gxXML = temp;
//
//	make_element_from_csv(m_gxXML, csv, 0, 0);
//
//	return gxTrue;
//

gxBool CXML::sort(CXML::tree &xml)
{
	std::sort(xml.array.begin(), xml.array.end(),[](auto &l ,auto &r){	return (l.value < r.value); });

	//std::map<std::string, CXML::tree> map;
	//for (int ii = 0; ii < xml.array.size(); ii++)
	//{
	//	map[xml.array[ii].value] = xml.array[ii];
	//}
	//
	//xml.array.clear();
	//
	////並べなおす
	//
	//for (auto itr=map.begin(); itr!=map.end(); ++itr)
	//{
	//	xml.array.push_back(itr->second);
	//}

	//子供についても並べ治す

	for (int ii = 0; ii < xml.array.size(); ii++)
	{
		sort(xml.array[ii]);
	}

	return true;
}

gxBool CXML::Sort()
{
	sort(m_gxXML);

	return gxTrue;
}


void CXML::EraseBlanks()
{
//	eraseBlanks(m_gxXML);
}
