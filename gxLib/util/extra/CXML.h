﻿#ifndef _XMLLOADER_
#define _XMLLOADER_

//tinyxml2のラッパー、使い方を忘れないようにする備忘録

#include "tinyXML/tinyxml2.h"
#include <gxLib/util/CFileTxt.h>
#include <gxLib/util/CFileCsv.h>

class CCsv;
class CXML
{
public:
	typedef std::pair<std::string, std::string> Attr;

	std::string GetErrorReason()
	{
		return m_ErrorReason;
	}

	struct tree {
		std::string tag;
		std::string value;
		std::vector<Attr> atr;	//key=”yes” dat="no"　みたいに複数存在する
		std::vector<tree> array;

		bool check(tree& src , tree& dst)
		{
			if (src.value != dst.value) return false;
			if (src.tag != dst.tag) return false;
			if (src.atr.size() != dst.atr.size()) return false;
			if (src.array.size() != dst.array.size()) return false;

			for (int ii = 0; ii < src.atr.size(); ii++)
			{
				if (src.atr[ii].first != dst.atr[ii].first) return false;
				if (src.atr[ii].second != dst.atr[ii].second) return false;
			}

			for (int ii = 0; ii < src.array.size(); ii++)
			{
				//同じ数の順番違いを検出したい
				//if (!check(src.array[ii], dst.array[ii])) return false;

				bool sameExist = false;
				for (int jj = 0; jj < src.array.size(); jj++)
				{
					if (check(src.array[ii], dst.array[jj]))
					{
						//同じものがあった
						sameExist = true;
						break;
					}
				}
				if (!sameExist) return false;

			}

			return true;
		}

		bool operator == (tree & dst)
		{
			bool eq = check(*this, dst);

			return eq;
		}

		bool operator != (tree& dst)
		{
			bool eq = check(*this, dst);

			return !eq;
		}

	};

	class Node
	{
	public:

		Node()
		{
			m_pElement = NULL;

			m_sBrotherNum = 0;
			m_pReturnStaticNode = NULL;

			m_sChildNum = 0;
			m_pChildArray = NULL;
		}

		~Node()
		{
			SAFE_DELETES( m_pChildArray );
			SAFE_DELETES( m_pReturnStaticNode );
		}

		Node* makeTree( gxChar *pName = NULL );

		Node* Get( gxChar* pName );

		gxChar *Name()
		{
			gxChar *p;

			if(m_pElement)
			{
				p = (gxChar*)m_pElement->Name();
				if( p ) return p;
			}
			
			return "No Name";
		}

		int GetAttributeNum()
		{
			if (m_pElement)
			{
				return m_pElement->GetAttributeNum();
			}

			return 0;
		}

		const char* GetAttributeName( int n )
		{
			if (m_pElement)
			{
				return m_pElement->GetAttributeName(n);
			}

			return nullptr;
		}

		gxChar *Attribute( gxChar *pName )
		{
			gxChar *p;

			if(m_pElement)
			{
				p = (gxChar*)m_pElement->Attribute(pName);
				if( p ) return p;
			}
			
			return "No Attribute";
		}

		gxChar *Data()
		{
			gxChar *p;

			if(m_pElement)
			{
				p = (gxChar*)m_pElement->GetText();
				if( p ) return p;
			}
			
			return "No Data";
		}

		void SetChildren( Node* pChildren , Sint32 num )
		{
			SAFE_DELETE( m_pChildArray );

			m_pChildArray = pChildren;
			m_sChildNum = num;
		}

		Sint32 GetChildNum()
		{
			return m_sChildNum;
		}

		Node* GetChild( Sint32 n)
		{
			return &m_pChildArray[n];
		}

		Node* GetParent()
		{
			if (m_pElement == NULL) return NULL;
			tinyxml2::XMLNode *pParent = m_pElement->Parent();
			Node* pTemp = NULL;

			if (pParent)
			{
				pTemp = new Node();
				pTemp->SetElement(pParent->ToElement());
			}

			return pTemp;
		}


		Sint32 GetBrotherNum()
		{
			return m_sBrotherNum;
		}

		Node* GetBrother( Sint32 n )
		{
			return &m_pReturnStaticNode[n];
		}

		void SetElement( tinyxml2::XMLElement *pElement )
		{
			if( pElement != NULL )
			{
				m_pElement = pElement;
			}
		}

	private:

		tinyxml2::XMLElement* searchChild( tinyxml2::XMLElement *element , gxChar *pString );

		Node* analyseBrother( tinyxml2::XMLElement *element , Sint32 *pNum , Sint32 *pChiNum = NULL );
		Node* analyseChild( tinyxml2::XMLElement *element );

		tinyxml2::XMLElement *m_pElement;

		Node *m_pReturnStaticNode;
		Node *m_pChildArray;

		Sint32 m_sBrotherNum;
		Sint32 m_sChildNum;
	};

	CXML()
	{
		m_pXML = nullptr;
	}
	
	~CXML()
	{
		SAFE_DELETE(m_pXML);
	}

	gxBool Load(gxChar* pFileName);
	gxBool Read(gxChar* pString, size_t uSize = 0);
	gxBool Save(std::string fileName);
	std::string ToString();

	gxBool SaveCSV(std::string fileName);


	//実験的（特定のフォーマットに従ったCSVのみXML化できる）
	//gxBool LoadCSV(std::string fileName);


	//XML中の特定のタグをROOTとする
	Node* adjustRoot(gxChar* pString = NULL);

	void SetElementList(tree& list)
	{
		m_gxXML = list;
	}

	tree& GetElementList()
	{
		return m_gxXML;
	}

	void search2(std::string &name, tree &elem , std::vector<tree> &result)
	{
		if (elem.tag == name)
		{
			result.push_back(elem);
		}

		for (int ii = 0; ii < elem.array.size(); ii++)
		{
			search2(name, elem.array[ii],result);
		}
	}
		
	std::vector<tree> Find(std::string name)
	{
		std::vector<tree> result;

		search2(name, m_gxXML, result);

		return result;
	}

	static tree GetDiff(tree* src, tree* dst);

	gxBool Sort();
	void EraseBlanks();
private:

	tree m_gxXML;
	std::string m_ErrorReason;

	gxBool sort(CXML::tree& xml);
	int getChildCSV(tree& p_node, CCsv& csv, int x, int y);
	void search(Node* pSlot, tree& list);
	void make_xml_from_elements(CXML::tree& xml, CTxt& txt, int x, int y);
	int make_element_from_csv(CXML::tree& parent, CCsv& csv, int x, int y);

	//XML読み込み用
	tinyxml2::XMLDocument* m_pXML;
	tinyxml2::XMLElement* searchRout(gxChar* pString);
	static gxBool isSame(gxChar* str1, gxChar* str2);
	static tinyxml2::XMLElement* searchChild(tinyxml2::XMLElement* element, gxChar* pString);
};

#endif

