﻿#ifndef CJSON_H_
#define CJSON_H_

#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/prettywriter.h"
#include "rapidjson/pointer.h"
#include "rapidjson/stringbuffer.h"
#include "CXML.h"

class CJson
{
public:

	struct JsonTree
	{
		enum ValueType {
			eTypeNoData,
			eTypeNull,
			eTypeArrayIndex,
			eTypeName,
			eTypeObject,
			eTypeArray,
			eTypeTrue,
			eTypeFalse,
			eTypeInt,
			eTypeUint,
			eTypeInt64,
			eTypeUint64,
			eTypeFloat,
			eTypeDouble,
			eTypeString,
		};

		std::vector<JsonTree> child;
		std::string name;
		ValueType type = eTypeNoData;

		void Clear()
		{
			type = JsonTree::eTypeNoData;
			name = "";
			child.clear();
		}

		JsonTree& addDir(std::string str, JsonTree::ValueType type)
		{
			JsonTree d2;
			d2.type = type;
			d2.name = str;
			child.push_back(d2);

			return child.back();
		}

		void AddStringValue(std::string _name, std::string _value)
		{
			//JsonTree info;
			JsonTree dat;
			//info.type = eTypeName;
			//info.name = name;
			//dat.type = eTypeString;
			//dat.name = "\"" + value + "\"";
			//info.child.push_back(dat);
			//child.push_back(info);

			type = eTypeName;
			name = _name;
			dat.type = eTypeString;
			dat.name = "\"" + _value + "\"";
			child.push_back(dat);
		}

		void AddIntValue(std::string _name, int  _value)
		{
			//JsonTree info;
			//info.type = eTypeName;
			//info.name = name;
			//dat.type = eTypeInt;
			//char buf[128];
			//sprintf(buf, "%d", value);
			//dat.name = buf;
			//info.child.push_back(dat);
			//child.push_back(info);

			JsonTree dat;
			char buf[128];
			type = eTypeName;
			name = _name;
			sprintf(buf, "%d", _value);

			dat.type = eTypeInt;
			dat.name = "\"";
			dat.name += buf;
			dat.name += "\"";

			child.push_back(dat);
		}

		void AddFloatValue(std::string _name, float _value)
		{
			//JsonTree info;
			//JsonTree dat;
			//info.type = eTypeName;
			//info.name = name;
			//dat.type = eTypeFloat;
			//char buf[128];
			//sprintf(buf, "%f", value);
			//dat.name = buf;
			//info.child.push_back(dat);
			//child.push_back(info);
			JsonTree dat;
			char buf[128];
			type = eTypeName;
			name = _name;
			sprintf(buf, "%f", _value);

			dat.type = eTypeFloat;
			dat.name = "\"";
			dat.name += buf;
			dat.name += "\"";

			child.push_back(dat);
		}
	};

	CJson()
	{
	}

	~CJson()
	{
	}

	gxBool Load(std::string fileName);
	gxBool Read(char* pData, size_t uSize);
	void Save(std::string fileName);
	void SaveXML(std::string fileName);

	void SetXML(CXML& xml)
	{
		CXML::tree xmllist = xml.GetElementList();

		m_Dir.Clear();

		readXML(xmllist, m_Dir);
	}

	void GetXML(CXML &xml)
	{
		CTxt txt;
		txt.printf("<xml>\n");

		make_XML_text(m_Dir, txt);

		txt.printf("</xml>");

		std::string str = txt.ToString();
		//printf("%s", str.c_str());
		//gxLib::SaveFile("test.txt", (char*)str.c_str(), str.size());

		//CXML xml;// = new CXML();
		xml.Read((char*)str.c_str(), str.size());

		//return xml;
	}

	JsonTree& GetTree()
	{
		return m_Dir;
	}

	std::string ToString();

	std::string &check_json_format(std::string &str)
	{
		for (int ii = 0; ii < str.size(); ii++)
		{
			if (str[ii] == '\\')
			{
				str[ii] = '/';
			}
		}

		return str;
	}

	void readXML(CXML::tree& xml , JsonTree &json )
	{
		std::string title;
		if (xml.tag.size() == 0)
		{
			title = "";
		}
		else if (xml.tag[0] != '\"')
		{
			title = '\"' + xml.tag + '\"';
		}
		else
		{
			title = xml.tag;
		}

		if (xml.array.size() == 0)
		{
			json.AddStringValue(title,check_json_format(xml.value));
		}
		else
		{
			//JsonTree& parent = json.addDir(xml.title, JsonTree::eTypeArray);
			json.name = title;
			json.type = JsonTree::eTypeObject;
			for (int ii = 0; ii < xml.array.size(); ii++)
			{
				JsonTree data;
				json.child.push_back(data);
				readXML(xml.array[ii], json.child.back() );
			}
		}

	}

	//デバッグ情報出力

	void SaveInfo(std::string fileName);
	void DispDebugInfo();
	void Print();


	//void SetObject( DirectoryInfo info , )
	//{
	//
	//}

	gxBool AddStringValue(JsonTree& info, std::string name , std::string string)
	{
		if (info.type == JsonTree::eTypeObject)
		{
			JsonTree d2,d3;
			d3.type = JsonTree::eTypeString;
			d3.name = "\"";
			d3.name += string;
			d3.name += "\"";

			d2.type = JsonTree::eTypeName;
			d2.name = "\"";
			d2.name += name;
			d2.name += "\"";
			d2.child.push_back(d3);

			info.child.push_back(d2);

			return gxTrue;
		}
		return gxFalse;
	}

	gxBool SetValue(JsonTree &info,std::string string )
	{
		if (info.type == JsonTree::eTypeName)
		{
			JsonTree &dir2 = info.child[0];
			dir2.type = JsonTree::eTypeString;
			dir2.name = "\"";
			dir2.name += string;
			dir2.name += "\"";
			return gxTrue;
		}
		return gxFalse;

	}

	gxBool SetValue(JsonTree &info, Float32 value)
	{
		if (info.type == JsonTree::eTypeName)
		{
			JsonTree &dir2 = info.child[0];
			dir2.type = JsonTree::eTypeFloat;
			char buf[256];
			sprintf(buf, "%.8f", value);
			dir2.name = buf;
			return gxTrue;
		}
		return gxFalse;
	}

	gxBool SetValue(JsonTree &info, Sint32 value)
	{
		if (info.type == JsonTree::eTypeName)
		{
			JsonTree &dir2 = info.child[0];
			dir2.type = JsonTree::eTypeInt;
			char buf[256];
			sprintf(buf, "%d", value);
			dir2.name = buf;
			return gxTrue;
		}
		return gxFalse;
	}

	gxBool SetObjectValue(JsonTree& info, std::string name , Sint32 value)
	{
		std::string srcName = "\"" + name + "\"";

		if (info.type == JsonTree::eTypeObject)
		{
			for (size_t ii = 0; ii < info.child.size(); ii++)
			{
				JsonTree& dir2 = info.child[ii];
				if (dir2.name == srcName)
				{
					SetValue(dir2, value);
					break;
				}
			}

			return gxTrue;
		}
		return gxFalse;
	}

	gxBool GetObjectValue(JsonTree& info, std::string name , std::string &ret )
	{
		std::string srcName = "\"" + name + "\"";

		if (info.type == JsonTree::eTypeObject)
		{
			for (size_t ii = 0; ii < info.child.size(); ii++)
			{
				JsonTree& dir2 = info.child[ii];
				if (dir2.name == srcName)
				{
					if(dir2.type == JsonTree::eTypeString) 
					ret = dir2.child[0].name;
					return gxTrue;
				}
			}

		}
		return gxFalse;
	}

	void search2(std::string& name, JsonTree& elem, std::vector<JsonTree>& result)
	{
		if (elem.name == name)
		{
			result.push_back(elem);
		}

		for (int ii = 0; ii < elem.child.size(); ii++)
		{
			search2(name, elem.child[ii], result);
		}
	}

	std::vector<JsonTree> Find(std::string name)
	{
		if (name[0] != '\"')
		{
			std::string tmp = "\"" + name + "\"";
			name = tmp;
		}

		std::vector<JsonTree> result;

		search2(name, m_Dir, result);

		return result;
	}

private:

	//JsonTree& addDir(JsonTree& d, std::string str, JsonTree::ValueType type)
	//{
	//	JsonTree d2;
	//	d2.type = type;
	//	d2.name = str;
	//	d.child.push_back(d2);
	//
	//	return d.child.back();
	//}

	void listup(JsonTree& d, rapidjson::Value& val);

	//JSON情報出力
	void make_JSON_text(JsonTree& d , CTxt &txt , bool bLast = gxFalse );
	void make_XML_text(JsonTree& d, CTxt& txt);

	std::string getString(rapidjson::Value& s, JsonTree::ValueType* pType = nullptr);
	rapidjson::Value parseObjectName(std::string key);

	//Debug情報出力
	void outputDirInfo(JsonTree& info , CTxt &txt , gxBool NoTab = gxFalse);

	std::vector<std::string> splitstring(std::string string);
	std::string getPaths(std::string path);

	rapidjson::Document m_ParsedDoc;
	JsonTree m_Dir;
	Sint32 m_Indent = 0;

};

#endif
