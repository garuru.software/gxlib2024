//------------------------------------------------------------------
//
//JSONデータをVECTOR配列に詰めなおす
//
//------------------------------------------------------------------
#include <gxLib.h>
#include <gxLib/util/CFileTxt.h>
#include "CJson.h"

static const char* TypeName[] = {
	"NoData",
	"Null",
	"ArrayIndex",
	"Name",
	"Object",
	"Array",
	"True",
	"False",
	"Int",
	"Uint",
	"Int64",
	"Uint64",
	"Float",
	"Double",
	"String",
};

std::string CJson::ToString()
{
	CTxt txt;

	m_Dir.name = "";
	txt.SetLF("\n");
	make_JSON_text(m_Dir, txt,true);

	//自分で生成したJSONをRAPIDJSONに食わせて答え合わせ＆整形
	//alltext.insert(361, "\r\n");
	//txt.Save("test.json");

	std::string alltext = txt.ToString();
	//gxLib::SaveFile("test2.txt", (char*)alltext.c_str(), alltext.size());

	if (!Read((char*)alltext.c_str(), alltext.size()))
	{
//		Load("test.json");

	}

	rapidjson::StringBuffer ws;
#if 0
	rapidjson::Writer<rapidjson::StringBuffer> writer(ws);
#else
	rapidjson::PrettyWriter<rapidjson::StringBuffer> writer(ws);
	writer.SetIndent('\t', 1);
#endif

	m_ParsedDoc.Accept(writer);
	txt = ws.GetString();

	return txt.ToString();
}

void CJson::SaveXML(std::string fileName)
{
	
	CXML xml;
	GetXML(xml);

	xml.Save(fileName);

	//SAFE_DELETE(p);
}

std::string chgXMLElementName(std::string src)
{
	src.erase(std::remove(src.begin(), src.end(), '\"'), src.end());
	src.erase(std::remove(src.begin(), src.end(), '\''), src.end());
	src.erase(std::remove(src.begin(), src.end(), '\''), src.end());
	std::replace(src.begin(), src.end(), ' ','_');
	std::string dst = src;

	switch (src[0]) {
	case '\"':
	case '\'':
	case '&':
	case '#':
	case '@':
	case '0':
	case '1':
	case '2':
	case '3':
	case '4':
	case '5':
	case '6':
	case '7':
	case '8':
	case '9':
		dst = '_';
		dst += src;
		break;
	}

	return dst;
}

std::string& chgXMLDataName(std::string &src)
{
	std::replace(src.begin(), src.end(), '<','_');
	std::replace(src.begin(), src.end(), '>','_');

	return src;
}


void CJson::make_XML_text(JsonTree& d , CTxt &txt)
{

	static char buf[256];

	{
		if (d.child.size() == 0)
		{
			if (d.type == JsonTree::eTypeArray)
			{
				//size０のアレイ対応
				std::string str2 = chgXMLElementName(d.name);
				txt.printf("<%s>\n", str2.c_str());
				txt.printf("</%s>\n", str2.c_str());
			}
			else if (d.name != "<object>")
			{
				//サイズ０のオブジェクト

				std::string str2 = chgXMLElementName(d.name);
				if (d.type == JsonTree::eTypeObject)
				{
					txt.printf("<%s>\n", str2.c_str());
					txt.printf("</%s>\n", str2.c_str());
					return;
				}

				txt.printf("%s", chgXMLDataName(d.name).c_str());
			}
			return;
		}

		if (d.type == JsonTree::eTypeArray)
		{
			std::string str2 = chgXMLElementName(d.name);
			txt.printf("<%s>\n", str2.c_str());

			for (size_t ii = 0; ii < d.child.size(); ii++)
			{
				txt.printf("<array index=\"%d\">", ii);
				make_XML_text(d.child[ii], txt);
				txt.printf("</array>");
			}
			txt.printf("</%s>\n", str2.c_str());
		}
		else if (d.type == JsonTree::eTypeObject)
		{
			std::string str2 = chgXMLElementName(d.name);
			if (str2 == "")
			{
				str2 = "eTypeObject";
			}
			txt.printf("<%s>\n", str2.c_str());

			txt.printf("\n");

			//複数のchildがいるObject

			for (size_t ii = 0; ii < d.child.size(); ii++)
			{
				//if (d.child[ii].name == "\"stack\"")
				//{
				//	static int n = 0;
				//	n++;
				//}
				//std::string str2 = 
				chgXMLElementName(d.name);
				make_XML_text(d.child[ii], txt);
			}
			txt.printf("</%s>\n", str2.c_str());

		}
		else if (d.type == JsonTree::eTypeArrayIndex)
		{
			for (size_t ii = 0; ii < d.child.size(); ii++)
			{
				make_XML_text(d.child[ii], txt);
			}
		}
		else
		{
			if (d.name != "")
			{
				std::string str = chgXMLElementName(d.name);

				txt.printf("<%s>", str.c_str());
				for (size_t ii = 0; ii < d.child.size(); ii++)
				{
					make_XML_text(d.child[ii], txt);
				}
				txt.printf("</%s>\n", str.c_str());
			}
			else
			{
				for (size_t ii = 0; ii < d.child.size(); ii++)
				{
					make_XML_text(d.child[ii], txt);
				}
			}

		}
	}

}

void CJson::listup(JsonTree& d, rapidjson::Value& val)
{
	static char buf[256];

	if (val.IsArray())
	{
		size_t max = val.Size();
		//size_t ii = 0;
		d.type = JsonTree::eTypeArray;

//		char tempbuf[128];

		for (rapidjson::SizeType ii = 0; ii < max; ii++)
		{
//			std::string temp = "";// d.name;
//			temp += "[";
//			temp += itoa(ii, buf, 10);
//			snprintf(tempbuf,sizeof(tempbuf),"%d",ii);
//			temp += tempbuf;
//			temp += "]";
			//JsonTree& d2 = d.addDir( temp, JsonTree::eTypeArrayIndex);
			JsonTree& d2 = d.addDir("", JsonTree::eTypeArrayIndex);
			listup(d2, val[ii]);
			//listup(d, val[ii]);
		}
	}
	else if (val.IsObject())
	{
		size_t num = val.MemberCount();
		Sint32 index = 0;

		//JsonTree& d2 = d;// .addDir("<object>", JsonTree::eTypeObject);
		d.type = JsonTree::eTypeObject;
		for (auto itr = val.MemberBegin(); itr != val.MemberEnd(); ++itr)
		{
			if (val.HasMember(itr->name))
			{
				const char* name = itr->name.GetString();

				std::string temp = "\"";
				temp += name;
				temp += "\"";
				JsonTree& subdir = d.addDir( temp, JsonTree::eTypeName);
				listup(subdir, val[name]);
			}
			index++;
		}
	}
	else
	{
		JsonTree::ValueType type = JsonTree::eTypeNull;
		std::string str = getString(val, &type).c_str();
		if (d.name == "")
		{
			d.type = JsonTree::eTypeString;
			d.name = str;
		}
		else
		{
			d.addDir(str.c_str(), type);
		}
	}
}


std::string CJson::getString(rapidjson::Value& s, JsonTree::ValueType* pType)
{
	char buf[256];

	std::string ret;

	JsonTree::ValueType type = JsonTree::eTypeNull;

	if (s.IsNumber())
	{
		if (s.IsInt())
		{
			type = JsonTree::eTypeInt;
			sprintf(buf, "%d", s.GetInt());
			ret = buf;
		}
		else if (s.IsUint())
		{
			type = JsonTree::eTypeUint;
			sprintf(buf, "%ud", s.GetUint());
			ret = buf;
		}
		else if (s.IsInt64())
		{
			type = JsonTree::eTypeInt64;
			sprintf(buf, "%d", (int)s.GetInt64());
			ret = buf;
		}
		else if (s.IsUint64())
		{
			type = JsonTree::eTypeUint64;
			sprintf(buf, "%ud", (unsigned)s.GetUint64());
			ret = buf;
		}
		else if (s.IsFloat())
		{
			type = JsonTree::eTypeFloat;
			sprintf(buf, "%f", s.GetFloat());
			ret = buf;
		}
		else if (s.IsDouble())
		{
			type = JsonTree::eTypeDouble;
			sprintf(buf, "%lf", s.GetDouble());
			ret = buf;
		}
	}
	else
	{
		if (s.IsString())
		{
			type = JsonTree::eTypeString;
			ret = '\"';
			//ret += s.GetString();
			std::string nnn = s.GetString();
			nnn = CTxt::Replace(nnn, "\r", "\\r");
			nnn = CTxt::Replace(nnn, "\n", "\\n");
			//nnn = CTxt::Replace(nnn, "\r\n", "\\r\\n");
			ret += nnn;
			ret += '\"';
		}
		else if (s.IsTrue())
		{
			type = JsonTree::eTypeTrue;
			ret = "true";
		}
		else if (s.IsFalse())
		{
			type = JsonTree::eTypeFalse;
			ret = "false";
		}
		else if (s.IsNull())
		{
			type = JsonTree::eTypeNull;
			ret = "null";
		}
	}
	if (pType) *pType = type;

	return ret;
}

//--------------------------------------------------------------------
//ディレクトリ構造可視化用
//--------------------------------------------------------------------

void CJson::DispDebugInfo()
{
	CTxt txt;
	txt.Clear();
	m_Indent = 0;
	outputDirInfo(m_Dir,txt);
	txt.OutPutText();
	//m_Txt.Save("test.txt");
}

void CJson::SaveInfo( std::string fileName )
{
	CTxt txt;
	txt.Clear();
	m_Indent = 0;
	outputDirInfo(m_Dir,txt);
	txt.Save(fileName);
}

void CJson::Print()
{
	CTxt txt;
	make_JSON_text(m_Dir, txt);
	txt.OutPutText();
}

void CJson::outputDirInfo(JsonTree &info, CTxt &txt ,gxBool bNoTab)
{
	//デバッグ用（解析情報を表示）
	//printf("[%s:%s]\r\n", TypeName[info.type],info.name.c_str());

	static std::vector<Sint32> arrayIndex;

	if( !bNoTab )
	{
		if( arrayIndex.size() > 0 )
		{
			for (Sint32 ii=0; ii < m_Indent; ii++)
			{
				bool bDraw = false;

				//for (Sint32 jj=0; jj < arrayIndex.size(); jj++)
				//{
				//	if( ii == abs(arrayIndex[jj]) )
				//	{
				//		bDraw = true;
				//		if (info.type == JsonTree::eTypeArrayIndex)
				//		{
				//			if (arrayIndex[jj] < 0)
				//			{
				//				txt.Add("-");
				//				arrayIndex.pop_back();
				//
				//			}
				//			else if (jj+1 == arrayIndex.size())
				//			{
				//				txt.Add("+");
				//			}
				//			else
				//			{
				//				txt.Add("|");
				//			}
				//		}
				//		else
				//		{
				//			txt.Add("|");
				//		}
				//
				//	}
				//}

				//if(!bDraw)
				{
					txt.Add("  ");
				}
			}
		}
		else
		{
			for (Sint32 ii = 0; ii < m_Indent; ii++)
			{
				txt.Add("  ");
			}
		}
	}

	bNoTab = gxFalse;

	if (info.child.size() > 1)
	{
		if (info.type == JsonTree::eTypeArray)
			arrayIndex.push_back( m_Indent );
	}

	//if (info.type == DirectoryInfo::eTypeArrayIndex)
	//{
	//	printf("|");
	//}

	auto* p = info.name.c_str();
	if (p[0] != 0x00 )
	{
		if (info.child.size() == 1)
		{
			//m_Txt.Add2("%s:", p);
			char buf[255];
			sprintf(buf, "%s:", p);
			txt.Add(buf);
			bNoTab = gxTrue;
		}
		else
		{
//			m_Txt.Add2(" %s" LF, p);
			char buf[255];
			sprintf(buf, " %s" LF, p);
			txt.Add(buf);
		}
	}

	m_Indent++;
	for (Sint32 ii = 0; ii < info.child.size(); ii++)
	{
		if (info.child.size() > 1)
		{
			if (info.type == JsonTree::eTypeArray)
			{
				if (info.child.size()-1 == ii )
				{
					arrayIndex.back() = arrayIndex.back()*-1;
				}
			}
		}
		outputDirInfo(info.child[ii], txt, bNoTab);
	}

	m_Indent--;


}

//--------------------------------------------------------------------
//--------------------------------------------------------------------

void CJson::make_JSON_text(JsonTree &d, CTxt& txt, bool bLast )
{
	size_t num = d.child.size();

	switch(d.type){
	case JsonTree::eTypeNoData:
		for (size_t ii = 0; ii < num; ii++)
		{
			make_JSON_text(d.child[ii],txt , (ii==num-1)? true : false );
		}
		break;

	case JsonTree::eTypeObject:
	{
		if (num > 0)
		{
			if (d.name == "")
			{
				txt += "{";
				//txt.AppendLine();
				for (size_t ii = 0; ii < num; ii++)
				{
					make_JSON_text(d.child[ii], txt, (ii == num - 1) ? true : false);
				}
				txt += "}";
				//if (!bLast)
				//	txt += "@";	//@@@
				txt.AppendLine();

			}
			else
			{
				txt.AppendLine();
				txt += d.name;
				txt += " : {";
				txt.AppendLine();
				for (size_t ii = 0; ii < num; ii++)
				{
					make_JSON_text(d.child[ii], txt, (ii == num - 1) ? true : false);
					//if (ii != (num - 1)) txt += "-";
				}
				txt += "}";
				if (!bLast)
					txt += ",";	//@@@
				txt.AppendLine();
			}
		}
		else {
			txt += "{";
			txt += "}";
			txt.AppendLine();
		}
	}
	break;

	case JsonTree::eTypeName:
	{
		txt += d.name;
		txt += ":";
		if (num == 0)
		{
			txt += "{}";
			txt.AppendLine();
		}
		else if (num == 1)
		{
			//txt += "{";
			//txt.AppendLine();
			make_JSON_text(d.child[0], txt);
			//txt += "}";
			//txt.AppendLine();
		}
		else if (num > 1)
		{
			txt += "{";
			txt.AppendLine();

			for (size_t ii = 0; ii < num; ii++)
			{
				make_JSON_text(d.child[ii], txt, (ii == num - 1) ? true : false);
			}

			txt += "}";
			txt.AppendLine();
		}
		if( !bLast )
			txt += ",";	//@@@
	}
	break;

	//case JsonTree::eTypeArrayIndex:
	//{
	//	for (size_t ii = 0; ii < num; ii++)
	//	{
	//		make_JSON_text(d.child[ii], txt);
	//	}
	//}
	//break;

	case JsonTree::eTypeArray:
	{
		if (d.name != "")
		{
			txt += d.name;
			txt += ":[";
		}
		else
		{
			txt += "[";
		}
		for (size_t ii = 0; ii < num; ii++)
		{
			make_JSON_text( d.child[ii], txt,false);
			if (ii != (num - 1)) txt += ",";
		}
		txt += "]";
		if (!bLast)	txt += ",";	//@@@
		txt.AppendLine();
	}
	break;

	case JsonTree::eTypeNull:
	case JsonTree::eTypeTrue:
	case JsonTree::eTypeFalse:
	case JsonTree::eTypeInt:
	case JsonTree::eTypeUint:
	case JsonTree::eTypeInt64:
	case JsonTree::eTypeUint64:
	case JsonTree::eTypeFloat:
	case JsonTree::eTypeDouble:
	case JsonTree::eTypeString:
		txt += d.name;
		for (size_t ii = 0; ii < num; ii++)
		{
			make_JSON_text(d.child[ii], txt);
		}
		break;
	default:
		break;
	}
}

//--------------------------------------------------------------------
//--------------------------------------------------------------------

std::vector<std::string> CJson::splitstring(std::string string)
{
	auto separator = std::string("/");          // 区切り文字
	auto separator_length = separator.length(); // 区切り文字の長さ

	std::vector<std::string> list;

	//if (separator_length == 0)
	//{
	//	list.push_back(string);
	//}
	//else
	{
		auto offset = std::string::size_type(0);
		while (true) {
			auto pos = string.find(separator, offset);
			if (pos == std::string::npos) {
				list.push_back(string.substr(offset));
				break;
			}
			list.push_back(string.substr(offset, pos - offset));
			offset = pos + separator_length;
		}
	}

	return list; // list == {"a", "b", "c"}
}

std::string CJson::getPaths(std::string path)
{
	path = path.erase(0, 1);
	path = path + "/";
	return path;
}

rapidjson::Value CJson::parseObjectName(std::string key)
{
	rapidjson::Value s;


	auto list = splitstring(key);

	s = m_ParsedDoc[list[0].c_str()];
	for (Sint32 ii = 1; ii < list.size(); ii++)
	{
		if (s.IsArray())
		{
			s = s[0];
			s = s[list[ii].c_str()];
		}
		else if (s.IsObject())
		{
			s = s[list[ii].c_str()];
		}

	}

	//auto r = getString(s);

	return s;
}

void CJson::Save(std::string fileName)
{
	CTxt txt;
	m_Dir.name = "";

	txt = ToString();

	txt.Save(fileName);
}

gxBool CJson::Load(std::string fileName)
{
	size_t uSize = 0;
	Uint8* pData = gxLib::LoadFile(fileName.c_str(), &uSize);

	char* pData2 = new char[uSize + 1];
	gxUtil::MemCpy(pData2, pData, uSize);
	pData2[uSize] = 0x00;

	if (!Read(pData2, uSize))
	{
		auto err = m_ParsedDoc.GetParseError();
		if (err == rapidjson::kParseErrorDocumentRootNotSingular)
		{
			char* pData3 = new char[uSize + 1+2];
			pData3[0] = '{';
			gxUtil::MemCpy(&pData3[1], pData, uSize);
			pData3[1+ uSize] = '}';
			pData3[uSize+2] = 0x00;
			Read(pData3, uSize + 2);
			SAFE_DELETES(pData3);
		}

	}

	SAFE_DELETES(pData);
	SAFE_DELETES(pData2);

	return gxTrue;
}

gxBool CJson::Read(char* pData, size_t uSize)
{

	if (((Uint8)pData[0]) == 0xEF && ((Uint8)pData[1] == 0xBB) && ((Uint8)pData[2] == 0xBF))
	{
		//BOMよけ
		pData += 3;
		uSize -= 3;
	}

	std::string src = "";
	src = (char*)pData;
	m_ParsedDoc.Parse(src.c_str());

	if (m_ParsedDoc.HasParseError())
	{
		auto err = m_ParsedDoc.GetParseError();

		size_t line = m_ParsedDoc.GetErrorOffset();

		switch (err) {
		case rapidjson::kParseErrorNone:	                        //!< No error.
			return gxFalse;
		case rapidjson::kParseErrorDocumentEmpty:
			gxLib::DebugLog("CJSon::Error::The document is empty.(%d)", line);
			return gxFalse;
		case rapidjson::kParseErrorDocumentRootNotSingular:
			gxLib::DebugLog("CJSon::Error::The document root must not follow by other values.(%d)", line);
			return gxFalse;

		case rapidjson::kParseErrorValueInvalid:
			gxLib::DebugLog("CJSon::Error::Invalid value.(%d)", line);
			return gxFalse;

		case rapidjson::kParseErrorObjectMissName:
			gxLib::DebugLog("CJSon::Error::Missing a name for object member.(%d)", line);
			return gxFalse;
		case rapidjson::kParseErrorObjectMissColon:
			gxLib::DebugLog("CJSon::Error::Missing a colon after a name of object member.(%d)", line);
			return gxFalse;
		case rapidjson::kParseErrorObjectMissCommaOrCurlyBracket:
			gxLib::DebugLog("CJSon::Error::Missing a comma or '}' after an object member.(%d)", line);
			return gxFalse;
		case rapidjson::kParseErrorArrayMissCommaOrSquareBracket:
			gxLib::DebugLog("CJSon::Error::Missing a comma or ']' after an array element.(%d)", line);
			return gxFalse;

		case rapidjson::kParseErrorStringUnicodeEscapeInvalidHex:
			gxLib::DebugLog("CJSon::Error::Incorrect hex digit after \\u escape in string.(%d)", line);
			return gxFalse;
		case rapidjson::kParseErrorStringUnicodeSurrogateInvalid:
			gxLib::DebugLog("CJSon::Error::The surrogate pair in string is invalid.(%d)", line);
			return gxFalse;
		case rapidjson::kParseErrorStringEscapeInvalid:
			gxLib::DebugLog("CJSon::Error::Invalid escape character in string.(%d)", line);
			return gxFalse;

		case rapidjson::kParseErrorStringMissQuotationMark:
			gxLib::DebugLog("CJSon::Error::Missing a closing quotation mark in string.(%d)", line);
			return gxFalse;
		case rapidjson::kParseErrorStringInvalidEncoding:
			gxLib::DebugLog("CJSon::Error::Invalid encoding in string.(%d)", line);
			return gxFalse;

		case rapidjson::kParseErrorNumberTooBig:
			gxLib::DebugLog("CJSon::Error::Number too big to be stored in double.(%d)", line);
			return gxFalse;

		case rapidjson::kParseErrorNumberMissFraction:
			gxLib::DebugLog("CJSon::Error::Miss fraction part in number.(%d)", line);
			return gxFalse;

		case rapidjson::kParseErrorNumberMissExponent:
			gxLib::DebugLog("CJSon::Error::Miss exponent in number.(%d)", line);
			return gxFalse;

		case rapidjson::kParseErrorTermination:
			gxLib::DebugLog("CJSon::Error::Parsing was terminated.(%d)", line);
			return gxFalse;

		case rapidjson::kParseErrorUnspecificSyntaxError:
			gxLib::DebugLog("CJSon::Error::Unspecific syntax error.(%d)", line);
			return gxFalse;
		default:
			gxLib::DebugLog("CJSon::Error::Unknown Error.(%d)", line);
			return gxFalse;
		}
	}
	else
	{
		m_Dir.Clear();
		rapidjson::Type type = m_ParsedDoc.GetType();

		if (type == rapidjson::kArrayType)
		{
			size_t max = m_ParsedDoc.Size();
			m_Dir.type = JsonTree::eTypeArray;
			for (rapidjson::SizeType i = 0; i < max; i++)
			{
				rapidjson::Value& val = m_ParsedDoc[i];
				JsonTree d;
				m_Dir.child.push_back(d);
				listup(m_Dir.child[i], val);
			}
		}
		else
		{
			rapidjson::Value& val = m_ParsedDoc;
			listup(m_Dir, val);
		}
	}

	return gxTrue;
}

//--------------------------------------------------------------------
//--------------------------------------------------------------------

//void CJson::Test()
//{
//	Load("sample.json");
//
//	DispDebugInfo();
//
//	JsonTree &info = GetTree();
//	AddStringValue(info.child[0], "n_noMember3", "stringData");
//
//	DispDebugInfo();
//
//	Print();
//
//	Save("test.txt");
//}
//