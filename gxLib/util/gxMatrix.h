﻿#ifndef _GX_MATRIX_H_
#define _GX_MATRIX_H_
struct VECTOR3
{
public:
	float	x,y,z;

	VECTOR3() { x=y=z=0.0f; };
	VECTOR3(float x, float y, float z) {
		(this->x) = x;
		(this->y) = y;
		(this->z) = z;
	};

	VECTOR3 operator + (const VECTOR3& dvec) const {
		VECTOR3 v3;

		v3.x = ((this)->x) + dvec.x;
		v3.y = ((this)->y) + dvec.y;
		v3.z = ((this)->z) + dvec.z;
		return v3;
	};

	VECTOR3 operator - (const VECTOR3& dvec) const {
		VECTOR3 v3;

		v3.x = ((this)->x) - dvec.x;
		v3.y = ((this)->y) - dvec.y;
		v3.z = ((this)->z) - dvec.z;
		return v3;
	};

	VECTOR3 operator * (const VECTOR3 &dvec) const {
		VECTOR3 v3;

		v3.x = ((this)->x) * dvec.x;
		v3.y = ((this)->y) * dvec.y;
		v3.z = ((this)->z) * dvec.z;

		return v3;
	};
	VECTOR3 operator * (float fDat) const {
		VECTOR3 v3;

		v3.x = ((this)->x) * fDat;
		v3.y = ((this)->y) * fDat;
		v3.z = ((this)->z) * fDat;

		return v3;
	};

	VECTOR3 operator / (const VECTOR3& dvec) const {
		VECTOR3 v3;

		v3.x = ((this)->x) / dvec.x;
		v3.y = ((this)->y) / dvec.y;
		v3.z = ((this)->z) / dvec.z;

		return v3;
	};

	VECTOR3 operator / (float fDat) const {
		VECTOR3 v3;

		if (fDat == 0.0f) {
			v3 = *this;
			return v3;
		}

		v3.x = ((this)->x) / fDat;
		v3.y = ((this)->y) / fDat;
		v3.z = ((this)->z) / fDat;
		return v3;
	};

	VECTOR3 operator += (const VECTOR3 dvec) {
		((this)->x) += dvec.x;
		((this)->y) += dvec.y;
		((this)->z) += dvec.z;
		return *this;
	};

	VECTOR3 operator += (const Float32 fDat ) {
		((this)->x) += fDat;
		((this)->y) += fDat;
		((this)->z) += fDat;
		return *this;
	};

	VECTOR3 operator -= (const VECTOR3 dvec) {
		((this)->x) -= dvec.x;
		((this)->y) -= dvec.y;
		((this)->z) -= dvec.z;
		return *this;
	};

	VECTOR3 operator *= (float fDat) {
		((this)->x) *= fDat;
		((this)->y) *= fDat;
		((this)->z) *= fDat;
		return *this;
	};

	VECTOR3 operator *= (const VECTOR3& dvec) {
		((this)->x) *= dvec.x;
		((this)->y) *= dvec.y;
		((this)->z) *= dvec.z;
		return *this;
	};

	VECTOR3 operator /= (float fDat) {

		if (fDat == 0.0f) {
			return *this;
		}

		((this)->x) /= fDat;
		((this)->y) /= fDat;
		((this)->z) /= fDat;
		return *this;
	};

	VECTOR3 operator /= (const VECTOR3& dvec) {
		((this)->x) /= dvec.x;
		((this)->y) /= dvec.y;
		((this)->z) /= dvec.z;
		return *this;
	};

	gxBool operator == (VECTOR3 &fDat) {

		if ((this->x) != fDat.x) return gxFalse;;
		if ((this->y) != fDat.y) return gxFalse;;
		if ((this->z) != fDat.z) return gxFalse;;
		return gxTrue;
	};

	Float32 length()
	{
		return sqrt ( (x*x)+ (y*y) + (z*z) );
//		return pow ( (x*x)+ (y*y) + (z*z) , 0.5 );
	}

	VECTOR3 normalize() {
		Float32 len = length();
		if (len == 0.0f )
		{
			x = y = z = 0.0f;
		}
		else
		{
			x /= len;
			y /= len;
			z /= len;
		}

		return *this;
	}

	VECTOR3 crossProduct( VECTOR3 &v )
	{
		//外積
		//外積を行うとベクトルa,bに直行するベクトルが作られます。
		//ベクトルの差が 結果がゼロなら平行である、１なら直行している
		//・２つのベクトルに垂直なベクトルを求める。
		//・ポリゴンの向き(法線ベクトル)を求める。
		//・２つのベクトルが所属する平面において左右の位置関係を知る
		//・平面上の三角形と点の内外判定
		//・ポリゴンの面積を計る
		//・平面上の閉領域の面積を計る
		//・平面上の閉領域の向き(時計回りか、反時計回り)
		VECTOR3 v2;

		v2.x = (y * v.z) - (z * v.y);
		v2.y = (z * v.x) - (x * v.z);
		v2.z = (x * v.y) - (y * v.x);
		return v2;
	
	}

	Float32 dotProduct(VECTOR3 &v)
	{
		//内積から２つのベクトルのなす角が作り出す平行四辺形の面積が分かります
		//・２つのベクトルのなす角度を求める
		//・線上の最近点を求める
		//・点が平面の表、裏どちら側にあるか判定
		//・平面上に点があるか
		return (Float32)( x*v.x + y*v.y + z*v.z);
	}

	static Float32 dotProduct(VECTOR3& v1 , VECTOR3& v2)
	{
		//内積から２つのベクトルのなす角が作り出す平行四辺形の面積が分かります
		//・２つのベクトルのなす角度を求める
		//・線上の最近点を求める
		//・点が平面の表、裏どちら側にあるか判定
		//・平面上に点があるか
		return (Float32)(v1.x * v2.x + v1.y * v2.y + v1.z * v2.z);
	}


};// VECTOR;

// 行列フォーマット
typedef struct MATRIX {
	float	_11,_12,_13,_14;
	float	_21,_22,_23,_24;
	float	_31,_32,_33,_34;
	float	_41,_42,_43,_44;
}MATRIX;

//-------------------------------------------------------
//行列操作
//-------------------------------------------------------
void mtxSetUnit(void);
void mtxSetUnit2(MATRIX *m);
void mtxTrans(VECTOR3 *v);
void mtxRotZ(float r);
void mtxRotX(float r);
void mtxRotY(float r);
void mtxScale(float x, float y, float z);
void mtxAffin2(VECTOR3 *d, MATRIX *m, VECTOR3 *s);
void mtxAffin(VECTOR3 *d, VECTOR3 *s);
void mtxAffinLocal(VECTOR3 *d, VECTOR3 *s);

void vctAdd(VECTOR3 *d, VECTOR3 *v1, VECTOR3 *v2);
void vctSub(VECTOR3 *d, VECTOR3 *v1, VECTOR3 *v2);
void vctMul(VECTOR3 *d, VECTOR3 *v1, VECTOR3 *v2);
void vctDiv(VECTOR3 *d, VECTOR3 *v1, VECTOR3 *v2);
void mtxMul(MATRIX *m2,MATRIX *m1);
void TransPers(VECTOR3 *v);

MATRIX GetCurrentMatrix();
void SetCurrentMatrix(MATRIX *m);

#endif
