﻿#ifndef _CVIRTUALPAD_H_
#define _CVIRTUALPAD_H_

class CVirtualStick
{
public:

	enum {
		enStickRange = 64,
	};

	enum {
		enVSButtonA,
		enVSButtonB,
		enVSButtonX,
		enVSButtonY,
		enVSButtonL1,
		enVSButtonR1,
		enVSButtonSelect,
		enVSButtonStart,
		enVS_ButtonMax,
	};

	CVirtualStick()
	{
		m_bDisp = gxFalse;
	}

	~CVirtualStick()
	{
	}


	void Action();
	void Draw();

	void SetDisp( gxBool bDisp )
	{
		//バーチャルスティックの表示切り替え
		m_bDisp = bDisp;
	}

	gxBool IsDisp()
	{
		return m_bDisp;
	}

	SINGLETON_DECLARE( CVirtualStick );

private:

	struct StButton
	{
		StButton()
		{
			m_bPush = gxFalse;
		}

		gxVector2  m_Pos = {0,0};
		gxRect     m_Rect = { 0,0,0,0 };
		gxBool     m_bPush = gxFalse;
		Sint32     m_TouchID = -1;
		Float32    m_fRingWidth = 0.0f;
	};

	struct StStick
	{
		StStick()
		{
			m_bPush = gxFalse;
			m_sTimer = 0;
			m_FingerID = -1;
		}

		gxVector2  m_Pos;
		gxVector2  m_Old;
		gxRect     m_Rect;
		gxBool     m_bPush;
		Sint32     m_sTimer;

		Sint32 m_FingerID;

		gxVector2 m_Center;
		gxVector2 m_Spot;
		Uint32 m_OldLever = 0;

	};

	void stickControl( StStick *pStick );
	void btnControl();

	void stickDraw( StStick *pStick );
	void btnDraw(Sint32 id);

	gxBool m_bDisp;

	StStick m_StickL;

	StButton m_Button[ enVS_ButtonMax ];

	Sint32 m_DispWait = FRAME_PER_SECOND*2;

};

#endif

