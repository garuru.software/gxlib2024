//-----------------------------------------------------------------------------------
//targaファイル１枚を管理する
//-----------------------------------------------------------------------------------
#ifdef _CFILETARGA_H_
#else
#define _CFILETARGA_H_


#define HUTTER_SIZE (26)

#pragma pack(push , 1)

class CFileTarga
{
public:

	//	typedef struct stTgaInfo {
	//		/* tgaヘッダ情報 */
	//		Uint16			exHdrSz;		/* ヘッダ後のピクセルデータまでのバイト数? */
	//		gxChar			id;				/* ?t?H?[?}?b?g?̎嵭ﾞ. 0,1,2,3,9,10,11 */
	//		Uint16			clutNo;			/* 色パレットの開始番号 */
	//		Uint16			clutNum;		/* 色パレットの個数 */
	//		gxChar			clutBit;		/* ?F?p???b?g?̃r?b?g?? 15,16,24,32 */
	//		short			x0;				/* 始点x */
	//		short			y0;				/* 始点y 画面左下が原点(0,0)なので注意！ */
	//		short			w;				/* 横幅 */
	//		short			h;				/* 縦幅 */
	//		gxChar			bpp;			/* ?s?N?Z???̃r?b?g??  8,16,24,32 */
	//		gxChar			mode;			/* bit 3..0(4bits) : ????. 0:???Ȃ?. 1:???闍`?̂??ߖ????\. 2:???闍`?????ۑ????ׂ? */
	//										/* 3:通常のα 4:予め乗算されたα. 8:詳細不明. */
	//										/*  (bpp=32のとき:0x08, 24のとき0x00, 16のとき0x01 8のとき0x08,だった) */
	//										/* bit 5,4         : ピクセルの格納順は  0:左下から 1:左上 2:右下  3:右上 */
	//	} stTgaInfo;


	typedef struct stTgaInfo
	{
		/* tgaヘッダ情報 ver 2.0 */
		gxChar	FieldLength;
		gxChar	ColorMapType;			//パレット　なし(0) / あり(1)
		gxChar	ImageType;				//0:なし　1:8bit: 2:24bit 3:Gray 9:index(RLE) 10:24bit(RLE) 11:gray(RLE)
		short	ColorMapIndex;			//// type of image 0=none,1=indexed,2=rgb,3=grey,+8=rle packed;//Low | High	(00)
		short	clutNum;				//Low | High	(0100)
		gxChar	ColorMapSize;			//0x18

		short			x0;				/* 始点x (00)*/
		short			y0;				/* 始点y (00) 画面左下が原点(0,0)なので注意！ */
		short			w;				/* 横幅 */
		short			h;				/* 縦幅 */

		gxChar			bpp;
		gxChar			mode;

	} stTgaInfo;

	typedef struct stTgaPixel
	{
		Uint8	blue;
		Uint8	green;
		Uint8	red;
		Uint8	alpha;

	} stTgaPixel;

	CFileTarga(void);
	~CFileTarga(void);

	//新規に画像を作成する

	gxBool Create(int width,int height,int colbit,Uint8 *pal=NULL);

	//画像のファイルデータそのものを取得する
	Uint8* GetFileImage(){	return m_pFileImage;	}

	//画像のファイルサイズを取得する
	size_t GetFileSize()
	{
		return m_FileSize;
	}

	//ファイルに吐き出す
	gxBool SaveFile( gxChar *filename);

	//ファイルを読み込む
	gxBool LoadFile( gxChar *filename,Uint32 col=0x00000000 );

	//Targaファイルを読み込む
	gxBool LoadTarga( gxChar *filename,Uint32 col=0x00000000);

	//メモリからTGAファイルを読み込む
	gxBool ReadFile( Uint8 *pMem,size_t sz,Uint32 col=0x00000000);
	gxBool ApplyTgaImage( Uint8 *pFileImage , size_t filesz);

	//RGBの輝度から自動的にアルファチャンネルを作成する
	void MakeAutoAlphaChannel(Sint32 mode = 0);

	//背景色を設定
	void SetBgColor(Uint32 bgcolor)
	{
		m_BgColor = bgcolor;
	}

	gxBool Line(int x1,int y1,int x2,int y2,int atr,int col=0xffffffff);
	gxBool Point(int x1,int y1,int atr,int col=0xffffffff);
	gxBool Box(int x1,int y1,int x2,int y2,int atr,int col=0xffffffff);
	gxBool BoxFill(int x1,int y1,int x2,int y2,int atr,int col=0xffffffff);
	gxBool Copy(int x1,int y1,int x2,int y2,int x3,int y3,int atr=0,int col=0xffffffff);
	gxBool Copy(CFileTarga *pTga,int x1,int y1,int x2,int y2,int x3,int y3,int atr=0,int col=0xffffffff);

	Uint32 GetPallet(int n)	{
		Sint32 palSize = m_pTgaInfo->ColorMapSize / 8;
		Uint8*p =  (Uint8*) &m_Pallet[0];

		Uint32 ii = palSize* n;
	
		Uint32 argb = 0x00000000;
		
		if (palSize == 3)
		{
			argb = (p[ii] << 16) | (p[ii + 1] << 8) | (p[ii + 2] << 0);
		}
		if (palSize == 4)
		{
			argb = (p[ii] << 24) | (p[ii+1] << 16) | (p[ii + 2] << 8) | (p[ii + 3] << 0);
		}

		return argb;	
	}
	void SetPallet(int n , Uint32 argb) {
		if (m_pTgaInfo->clutNum > 0)
		{
			Sint32 palSize = m_pTgaInfo->ColorMapSize / 8;
			Uint32 ii = palSize * n;
			Uint8* p = (Uint8*)&m_Pallet[0];
			if (palSize == 3)
			{
				p[ii+0] = (argb >> 16) & 0xff;
				p[ii+1] = (argb >> 8) & 0xff;
				p[ii+2] = (argb >> 0) & 0xff;
			}

			if (palSize == 4)
			{
				p[ii + 0] = (argb >> 24) & 0xff;
				p[ii + 1] = (argb >> 16) & 0xff;
				p[ii + 2] = (argb >> 8) & 0xff;
				p[ii + 3] = (argb >> 0) & 0xff;
			}
		}
	}
	Uint32 GetWidth()		{	return m_pTgaInfo->w;	}
	Uint32 GetHeight()		{	return m_pTgaInfo->h;	}
	Sint32 GetBPP()			{	return m_pTgaInfo->bpp;	}

	Uint32 SetRGB(int x,int y,Uint32 col);
	Uint32 SetARGB(int x,int y,Uint32 col);
	void   SetARGB32(int x,int y,Uint32 col);
	Uint32 SetARGBadd(int x,int y,Uint32 col);
	Uint32 SetARGBsub(int x,int y,Uint32 col);
	Uint32 SetARGBmul(int x,int y,Uint32 col);
	Uint32 GetARGB(int x,int y);
	Uint32 GetARGB32(int x,int y);

	Sint32 GetPalletIndex(int x,int y);
	void SetPalletIndex(int x, int y, Uint8 index);

	void GetARGB( Uint32 uCol , stTgaPixel* pCol )
	{
		pCol->blue  = (uCol&0x000000ff)>>0;
		pCol->red   = (uCol&0x00ff0000)>>16;
		pCol->green = (uCol&0x0000ff00)>>8;
		pCol->alpha = (uCol&0xff000000)>>24;
	}

	void Paint( Uint32 uCol , Sint32 x , Sint32 y );

	void ClearImage();

	Uint8* GetTexelImage()
	{
		return m_TexelImage;
	}

	void Convert32( Uint32 colorKey );

	void setTexelImage(Uint8* pBuf);

private:

	void convToPlatformPixels();

	Uint8  setID(Sint8 palletnum);

	gxBool loadBitmap(gxChar *filename,Uint32 colorKey=0xff00ff00 );
	gxBool loadPNG( gxChar *filename , Uint32 colorKey );

	gxBool convertBMPtoTGA(Uint8 *p, size_t sz , Uint32 colorKey = 0xff00ff00 );
	gxBool convertPNGtoTGA(Uint8 *p, size_t sz , Uint32 colorKey = 0xff00ff00 );

	void paint( Uint32 uPaintColor , Sint32 x , Sint32 y );
	void scanLine( int lx, int rx, int y, Uint32 col );

	Uint8* RLEunpack( Uint8* pBuf , Uint32 uSize );

	Uint8   m_Filename[FILENAMEBUF_LENGTH];
	Uint8*  m_pFileImage;	//ファイルイメージ
	Uint32* m_Pallet;		//パレットデータへのポインタ
	Uint8*  m_TexelImage;	//テクスチャデータの開始位置
	size_t  m_FileSize;		//ファイルサイズ
	Uint32  m_BgColor;		//クリアする背景色

	stTgaInfo *m_pTgaInfo;	//ヘッダー情報へのポインタ

	INLINE Uint8* getPos(int x,int y);

	void releaseImage()
	{
		if(m_pFileImage)
		{
			SAFE_DELETES(m_pFileImage);
		}
		m_pFileImage = NULL;
	};
	//----------------------------------
	//BMP関連
	//----------------------------------
};
#pragma pack (pop)

#endif	//_CFILETARGA_H_
