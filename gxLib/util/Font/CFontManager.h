//--------------------------------------------------------------------------
//
// CFontManager.h
// written bt tomi.
// 2013.01.21　更新
// 
//--------------------------------------------------------------------------
#ifndef _CFONTMANAGER_H_
#define _CFONTMANAGER_H_

#define DEFAULT_FONT_SIZE (1.0f)

class CFontManager
{
public:
	struct SInfo {
		wchar_t  id;
		Uint16  pg;
		Uint16  u;
		Uint16  v;
		Uint16  w;
		Uint16  h;
		Uint16  ox;
		Uint16  oy;
		Uint16  xa;
		Uint16  ch;
		Uint16  texPage;
	};

	struct StSpriteEX
	{
		int lineIndex = 0;
		gxLib::StSprite spr;
	};

	CFontManager();

	~CFontManager()
	{
	}

	void Init();
	void Init(CFontManager* p);	//他のFontManagerの設定をコピーして使用する
	void Load(Sint32 page, gxChar* pFileName , Sint32 fontSize );
	void Read( Sint32 page , uint8_t *pData ,size_t uSize , Sint32 fontSize );

	Sint32 GetFontSize()
	{
		return m_FontSize;
	}

	void SetScale(Float32 fScale)
	{
		m_fScale = fScale;
	}

	void SetSpacing( Float32 w , Float32 h )
	{
		m_SpacingW = w;
		m_SpacingH = h;
	}

	void SetOffset( Sint32 x , Sint32 y )
	{
		m_Offset.x = x;
		m_Offset.y = y;
	}

	Float32 GetScale()
	{
		return m_fScale;
	}

	Sint32 Printf(Sint32 x, Sint32 y, Sint32 prio, Uint32 atr, Uint32 argb, gxChar const* str, ...)
	{
		va_list app;
		va_start(app, str);
		vsnprintf(m_Temp, enTempLength, str, app);
		va_end(app);

		return Print(x, y, prio, atr, argb, m_Temp);

	}

	Sint32 Print(Sint32 x, Sint32 y, Sint32 prio, Uint32 atr, Uint32 argb, gxChar* str )
	{
		size_t sz = 0;

		std::vector<Uint32> p32 = gxUtil::UTF8toUTF32(str);
		//gxChar* p32 = gxUtil::UTF8toUTF32(str , &sz);
		sz = p32.size() * 4;
		calc((gxChar*) & p32[0], sz);

		if( atr&ATR_STR_NODISP) return m_Width;

		x += m_Offset.x;
		y += m_Offset.y;

		draw(x, y, prio, atr, argb, 0.0f, 1.0f);

		//SAFE_DELETES(p32);

		return m_Width;
	}

	gxRect PrintUTF32(Sint32 x, Sint32 y, Sint32 prio, Uint32 atr, Uint32 argb, gxChar* str )
	{
		size_t sz = 0;
		//gxChar* p32 = CDeviceManager::UTF8toUTF32(str , &sz);

		Uint32 *p32 = (Uint32*)str;
		for(Sint32 ii=0; ii<1024; ii++ )
		{
			if( p32[ii] == 0 )
			{
				sz = ii*4;
				break;
			}
		}

		calc(str,sz);

		x += m_Offset.x;
		y += m_Offset.y;

		draw(x, y, prio, atr, argb, 0.0f, 1.0f);

		//SAFE_DELETES(p32);

		gxRect rect;
		rect.SetWH(x, y, m_Width, m_Height);

		return rect;
	}


    void draw( Sint32 ax , Sint32 ay, Sint32 prio , Uint32 atr , Uint32 argb , Float32 fRot , Float fScale );

	void Test()
	{
		//テストルーチン

		//wchar_t* pStr = L"-ABCDE0123456789\nＡＢＣＤＥ０１２３４５６７８９\n改行後の制御\n(C)　ガルルソフトウェア研究所\n最終行のテストてす";

	//	calc(pStr);

		//描画

		Sint32 ax, ay;
		ax = WINDOW_W / 2;
		ay = WINDOW_H / 2;

		//m_Attribute |= ATR_STR_CENTER;
		//m_Attribute |= ATR_STR_RIGHT;
		//m_fRotation = gxLib::GetGameCounter() % 360;
		//m_Attribute |= ATR_STR_MID;
		//m_Attribute |= ATR_STR_BOTTOM;

		draw(ax, ay, 100,m_Attribute, m_ARGB, m_fCardRotationY, m_fScale);
	}

	void MakeFontData();

	void SetWidth( Float32 width )
	{
		//自動改行する幅の設定
		m_NewLineWidth = width;
	}

	gxRect GetLastDrawnRect()
	{
		return m_DrawnRect;
	}

	void SetHalCharaMonospaced( gxBool bMonoSpaced = gxTrue )
	{
		//半角キャラは等幅とする（デフォルトON）
		m_HalfSizeSameWidth = bMonoSpaced;
	}

	SINGLETON_DECLARE( CFontManager );

private:
	std::map<Uint32, Uint32> m_GyomatsyKinsoku;
	Sint32 m_TexturePage = 0;
	std::map<wchar_t, SInfo> m_List;
	Sint32 m_FontSize = 48;

	unsigned int PushColor(unsigned int argb)
	{
		m_Color.push_back(argb);

		return m_Color.back();
	}

	unsigned int PopColor()
	{
		if (m_Color.size() > 1)
		{
			m_Color.pop_back();
		}

		return m_Color.back();
	}

	float PushScale(float fScale)
	{
		m_Scale.push_back(fScale);

		return m_Scale.back();
	}

	float PopScale()
	{
		if (m_Scale.size() > 1)
		{
			m_Scale.pop_back();
		}

		return m_Scale.back();
	}

	void PushSprite(float fScale)
	{
		m_Scale.push_back(fScale);

		return;
	}

	void PopSprite()
	{
		/*
		if (m_Scale.size() > 1)
		{
			m_Scale.pop_back();
		}

		return m_Scale.back();
		*/
	}

	gxBool makeCmd(std::vector<std::vector<unsigned char>> list)
	{
		unsigned int argb = 0x00000000;
		float scale = 1.0f;
		if (strcmp((char*)&list[0][0], "COLOR") == 0)
		{
			argb = strtoul((char*)&list[1][0], NULL, 16);
			PushColor(argb);
			return gxFalse;
		}
		else if (strcmp((char*)&list[0][0], "/COLOR") == 0)
		{
			PopColor();
			return gxFalse;
		}
		else if (strcmp((char*)&list[0][0], "SCALE") == 0)
		{
			scale = strtof((char*)&list[1][0], NULL);
			PushScale(scale);
			return gxFalse;
		}
		else if (strcmp((char*)&list[0][0], "/SCALE") == 0)
		{
			PopScale();
			return gxFalse;
		}
		else if (strcmp((char*)&list[0][0], "SPRITE") == 0)
		{
			m_ChangeSpr.page = strtof((char*)&list[1][0], NULL);
			m_ChangeSpr.u    = strtof((char*)&list[2][0], NULL);
			m_ChangeSpr.v    = strtof((char*)&list[3][0], NULL);
			m_ChangeSpr.w    = strtof((char*)&list[4][0], NULL);
			m_ChangeSpr.h    = strtof((char*)&list[5][0], NULL);
			m_ChangeSpr.cx   = strtof((char*)&list[6][0], NULL);
			m_ChangeSpr.cy   = strtof((char*)&list[7][0], NULL);
			//PushSprite(scale);
			return gxFalse;
		}
		else if (strcmp((char*)&list[0][0], "/SPRITE") == 0)
		{
			//PopSprite();
			return gxFalse;
		}

		return gxFalse;
	}


	std::vector<std::vector<unsigned char>> splitString(char32_t *buf, size_t sz)
	{

		std::vector<Uint32> str;
		for( Sint32 ii=0; ii<sz/4; ii++ )
		{
			str.push_back( buf[ii] );
		}
		str = gxUtil::StrUpper32( str );

		std::vector<std::vector<unsigned char>> list;
		std::vector<unsigned char> line;

		for (int ii = 0; ii < sz / 4; ii++)
		{
			unsigned char chr = (unsigned char)str[ii];
			switch (chr) {
			case 0x00:
			case '=':
			case ',':
				line.push_back(0x00);
				list.push_back(line);
				line.clear();
				continue;
			case ' ':
			case '#':
				continue;
			default:
				break;
			}

			line.push_back(chr);
		}
		if (line.size() > 0)
		{
			line.push_back(0x00);
			list.push_back(line);
			line.clear();
		}

		return list;
	}

	std::vector<unsigned int> m_Color = { 0x00000000 };
	std::vector<float> m_Scale = { 1.0f };


	void calc(gxChar* pStr, size_t sz);
	void drawFont(StSpriteEX spr , Sint32 ax, Sint32 ay, Sint32 prio, Uint32 atr, Uint32 argb, Float32 fRot, Float fScale);


	std::vector<StSpriteEX> m_Sprites;
	std::vector<int> m_LineWidth;

	gxBool m_HalfSizeSameWidth = gxTrue;
	Sint32 m_Width = 0;
	Sint32 m_Height = 0;

	struct Info
	{
		Float32 m_fScale = 1.0f;
		Float32 m_fCardRotationY = 0.0f;
		Uint32  m_Attribute = 0;
		Uint32  m_ARGB = 0xffffffff;

		Float32 m_SpacingW = 2.0f;
		Float32 m_SpacingH = 2.0f;
		gxVector2 m_Offset = { 0.0f , 0.0f };
	};

	std::vector<Info> m_InfoList;
public:

	void push()
	{
		Info info;

		info.m_fScale			 = m_fScale;
		info.m_fCardRotationY	 = m_fCardRotationY;
		info.m_Attribute			 = m_Attribute;
		info.m_ARGB				 = m_ARGB;
		info.m_SpacingW			 = m_SpacingW;
		info.m_SpacingH			 = m_SpacingH;
		info.m_Offset			 = m_Offset;
		m_InfoList.push_back(info);
	}

	void pop()
	{
		if (m_InfoList.size() == 0 ) return;
		Info info;
		info = m_InfoList.back();
		m_InfoList.pop_back();

		m_fScale			= info.m_fScale;
		m_fCardRotationY	= info.m_fCardRotationY;
		m_Attribute			= info.m_Attribute;
		m_ARGB				= info.m_ARGB;
		m_SpacingW			= info.m_SpacingW;
		m_SpacingH			= info.m_SpacingH;
		m_Offset			 = info.m_Offset;
	}
private:


	Float32 m_fScale = 1.0f;
	Float32 m_fCardRotationY = 0.0f;
	Uint32  m_Attribute = 0;
	Uint32  m_ARGB = 0xffffffff;

	Float32 m_SpacingW = 2.0f;
	Float32 m_SpacingH = 2.0f;
	gxVector2 m_Offset = { 0.0f , 0.0f };

	gxSprite m_ChangeSpr;

	enum {
		enTempLength = 512,
	};

	gxChar m_Temp[enTempLength] = {0};
	gxBool m_bTouhaba = gxFalse;// True;
	Float32 m_NewLineWidth = 0;

	void cmdTest();

	gxRect m_DrawnRect;

};


#if 0
void calc2(wchar_t* pStr)
{
	//表示するのに必要な情報を集める

	size_t len = wcslen(pStr);

	m_Sprites.clear();
	m_LineWidth.clear();

	m_Width = 0;
	m_Height = 0;

	Sint32 x = 0, y = 0, z = 0, w = 0, h = 0;
	Sint32 lineHeight = 0;
	Sint32 ln = 0;

	for (size_t ii = 0; ii < len; ii++)
	{
		wchar_t id = pStr[ii];

		auto itr = m_List.find(id);

		if (itr == m_List.end())
		{
			if (id == '\n')
			{
				m_LineWidth.push_back(x);
				ln++;

				x = 0;
				y += lineHeight;
				m_Height += lineHeight;
				continue;
			}
			else
			{
				continue;
			}
		}

		if (id == '\t' || id == ' ' || id == 12288 /*　*/)
		{
			id = ' ';
			itr = m_List.find(id);
			x += 16;
			continue;
		}

		SInfo info = itr->second;

		StSpriteEX sprex;
		sprex.lineIndex = ln;
		sprex.spr.pos = { x,y,0 };
		sprex.spr.albedo = {
			//				(info.pg+m_TexturePage) * 64,
							(m_TexturePage),// * 64,
							info.u,
							info.v,
							info.w,
							info.h,
							-info.ox,
							-info.oy,
		};

		w = info.w + info.ox;
		h = info.h + info.oy;

		if (m_bTouhaba)
		{
			x += 18;
		}
		else
		{
			x += w;
		}

		if (h > lineHeight) lineHeight = h;
		if (x > m_Width)    m_Width = x;

		sprex.spr.shader = gxShaderFont;
		sprex.spr.option[0] = info.pg;
		m_Sprites.push_back(sprex);
	}

	m_LineWidth.push_back(x);
	m_Height += lineHeight;
	ln++;

	//gxLib::DrawPoint(m_Width , m_Height , 100, ATR_DFLT, 0xC080F080 , 8.0f);
}
#endif

#endif

