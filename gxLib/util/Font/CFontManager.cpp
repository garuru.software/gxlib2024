//--------------------------------------------------------------------------
//
// CFontManager.cpp
// written bt tomi.
// 2019.04.14　更新
// 
//--------------------------------------------------------------------------

#include <gxLib.h>
#include <gxLib/Util/CFileTarga.h>
#include <gxLib/Util/CFileCsv.h>
#include <gxLib/gx.h>
#include "CFontManager.h"
#include "../CFileZip.h"

#include "fontTbl.cxx"

SINGLETON_DECLARE_INSTANCE( CFontManager );

static Uint32 kinsoku[]={
	//行頭禁則処理
	L'っ',L'、',L'。',L'，',L'．',L'・',L'？',L'！',L'゛',L'゜',L'ヽ',L'ヾ',L'ゝ',L'ゞ',L'々',L'ー',L'）',L'］',L'｝',
	L'」',L'』',L'!',L')',L',',L'.',L':',L';',L'?',L']',L'}',L'｡',L'｣',L'､',L'･',L'ｰ',L'ﾞ',L'ﾟ'
};


CFontManager::CFontManager()
{

}

void CFontManager::Init()
{
	//m_fScale = 1.0f;
	//gxLib::SaveFile("utf8.zip" , ZipData, sizeof(ZipData) );

	//return;
	//MakeFontData();
	//return;

	m_FontSize =32;

	Float32 fColokStart = gxLib::GetTime();

	m_TexturePage = (MAX_MASTERTEX_NUM-1)*64;

	size_t uSize;
	Uint8 *pData;
	CFileZip zip;

	//zip.Load("fontU8.zip");
	zip.Read(ZipData, sizeof(ZipData));
	pData = zip.Decode("utf8.csv",&uSize);

	//gxLib::SaveFile("test.txt" , pData, uSize);

	CCsv csv;
	csv.Read(pData,uSize);

	Sint32 max = csv.GetHeight();
	SInfo info;

	for (Sint32 ii = 0; ii < max; ii++)
	{
		Sint32  id;
		Sint32  pg;
		Sint32  u;
		Sint32  v;
		Sint32  w;
		Sint32  h;
		Sint32  ox;
		Sint32  oy;
		Sint32  xa;
		Sint32  ch;

		csv.GetCell(2, ii, id);
		csv.GetCell(4, ii, u);
		csv.GetCell(6, ii, v);
		csv.GetCell(8, ii, w);
		csv.GetCell(10, ii, h);
		csv.GetCell(12, ii, ox);
		csv.GetCell(14, ii, oy);
		csv.GetCell(16, ii, xa);
		csv.GetCell(18, ii, pg);
		csv.GetCell(20, ii, ch);

		info.id = id;
		info.pg = pg;;
		info.u  = u;
		info.v  = v;
		info.w  = w;
		info.h  = h;
		info.ox = ox;
		info.oy = oy;
		info.xa = xa;
		info.ch = ch;
		m_List[id] = info;

	}

	SAFE_DELETES(pData);

	pData = zip.Decode("utf8.tga",&uSize);
	gxLib::ReadTexture( (m_TexturePage+0)/* * 64*/, pData, uSize , 0x00000000);
	SAFE_DELETES(pData);

	gxLib::UploadTexture();

	Float32 fColokNow = gxLib::GetTime();
	Float32 benchTime = fColokNow - fColokStart;
	CGameGirl::GetInstance()->SetBenchmarkTime( benchTime );

	Sint32 num = ARRAY_LENGTH(kinsoku);
	for( Sint32 ii=0; ii<num; ii++ )
	{
		m_GyomatsyKinsoku[kinsoku[ii]] = 1;
	}
}

void CFontManager::Init(CFontManager* p)
{
	//他のFontManagerの設定をコピーして使用する
	m_GyomatsyKinsoku = p->m_GyomatsyKinsoku;
	m_FontSize        = p->m_FontSize;
	m_TexturePage     = p->m_TexturePage;
	m_List            = p->m_List;

}

	
void CFontManager::Load( Sint32 page , gxChar *pFileName , Sint32 fontSize )
{

	size_t uSize = 0;

	uint8_t *pData = gxLib::LoadFile( pFileName , &uSize);
	
	if(pData)
	{
		Read( page , pData ,uSize , fontSize );
		SAFE_DELETES(pData);
	}
}

void CFontManager::Read( Sint32 page , uint8_t *pData ,size_t uSize , Sint32 fontSize )
{
	m_FontSize = fontSize;
	m_TexturePage = page;
	CFileZip zip;

	if (!zip.Read(pData , uSize))
	{
		//Loadに失敗した
		ASSERT(0);
	}
	//zip.Read(ZipData, sizeof(ZipData));
	pData = zip.Decode("font.csv", &uSize);

	CCsv csv;
	csv.Read(pData, uSize);

	Sint32 max = csv.GetHeight();
	SInfo info;

	for (Sint32 ii = 0; ii < max; ii++)
	{
		Sint32  id;
		Sint32  pg;
		Sint32  u;
		Sint32  v;
		Sint32  w;
		Sint32  h;
		Sint32  ox;
		Sint32  oy;
		Sint32  xa;
		Sint32  ch;

		csv.GetCell(2, ii, id);
		csv.GetCell(4, ii, u);
		csv.GetCell(6, ii, v);
		csv.GetCell(8, ii, w);
		csv.GetCell(10, ii, h);
		csv.GetCell(12, ii, ox);
		csv.GetCell(14, ii, oy);
		csv.GetCell(16, ii, xa);
		csv.GetCell(18, ii, pg);
		csv.GetCell(20, ii, ch);

		info.id = id;
		info.pg = pg;;
		info.u = u;
		info.v = v;
		info.w = w;
		info.h = h;
		info.ox = ox;
		info.oy = oy;
		info.xa = xa;
		info.ch = ch;
		m_List[id] = info;

	}

	SAFE_DELETES(pData);

	pData = zip.Decode("FontRGBA.tga", &uSize);
	if (!pData)
	{
		GX_DEBUGLOG("Font Texture Not Found!");
		ASSERT(pData);
		return;
	}
	gxLib::ReadTexture((m_TexturePage + 0), pData, uSize, 0xff00ff00);
	SAFE_DELETES(pData);

	gxLib::UploadTexture();

	Sint32 num = ARRAY_LENGTH(kinsoku);
	for (Sint32 ii = 0; ii < num; ii++)
	{
		m_GyomatsyKinsoku[kinsoku[ii]] = 1;
	}
}


void CFontManager::MakeFontData()
{
	size_t uSize = 0;
	Uint8* pData;

	pData = gxLib::LoadFile("fontU8.zip",&uSize);

	gxChar* pText = new gxChar[uSize*8];
	Uint32 uLength = 0;

	uLength += sprintf( &pText[uLength], "Uint8 ZipData[]={" LF);
	for (Uint32 ii = 0; ii < uSize; ii++)
	{
		if (ii % 16 == 0)
		{
			uLength += sprintf( &pText[uLength], LF );
		}

		uLength += sprintf(&pText[uLength], "0x%02x,", pData[ii]);
	}

	uLength += sprintf(&pText[uLength], "};" LF);

	gxLib::SaveFile("gxLib/util/font/fontTbl.cxx", (Uint8*)pText, uLength);

	//CFileText txt;
	//txt.AddLine("0x%x,0x%x,0x%x,0x%x,0x%x,0x%x,0x%x,0x%x,0x%x,0x%x,0x%x,0x%x,0x%x,0x%x,0x%x,0x%x,");
	//txt.SaveFile("gxlib/util/font/fontTbl.cxx");
	SAFE_DELETES(pText);
	SAFE_DELETES(pData);

}


void CFontManager::calc(gxChar* pStr, size_t sz)
{
	//utf32で受け取った文字列から表示するのに必要な情報を集める
	Sint32 space = m_FontSize/10;

	size_t len = sz / 4;

	m_Sprites.clear();
	m_LineWidth.clear();
	m_Color.clear();
	m_Color.push_back(0x00);

	m_Width = 0;
	m_Height = 0;

	Sint32 x = 0, y = 0, w = 0, h = 0;	//, z = 0
	Sint32 lineHeight = 0;
	Sint32 ln = 0;

	Sint32 cnt = 0;
	Sint32 start = -1;
	Sint32 num   = -1;
	//Uint32 argb = 0xffffffff;
	//Float32 fScale = m_Scale.back();

	for (Sint32 ii = 0; ii<len; ii++)
	{
		Uint32* q2 = (Uint32*)&pStr[ii * 4];
		Uint32 id = *q2;

		auto itr = m_List.find(id);

		if (itr == m_List.end())
		{
			if (id == '\n')
			{
				m_LineWidth.push_back(x);
				ln++;

				x = 0;
				y += lineHeight;
				m_Height += lineHeight;
				continue;
			}
			else
			{
				continue;
			}
		}

		if (id == '\t' || id == ' ' || id == 12288 /*　*/)
		{
			id = ' ';
			itr = m_List.find(id);
			x += 16;
			continue;
		}

		if (id == '\n')
		{
			m_LineWidth.push_back(x);
			ln++;

			x = 0;
			y += lineHeight;
			m_Height += lineHeight;
			continue;
		}

		if (id == '<')
		{
			//閉じタグを探してなかったらタグとして扱わない
			Sint32 tagCnt = 1;
//			gxBool bCloseTagFound = gxFalse;
			gxBool bCloseTagFound = gxTrue;	//かるただけの特別処理
			for (Sint32 jj = ii + 1; jj < len; jj++)
			{
				q2 = (Uint32*)&pStr[jj * 4];
				Uint32 _id = *q2;
				auto _itr = m_List.find(_id);
				if (_itr == m_List.end())
				{
				}
				if (_id == '<') tagCnt++;
				if (_id == '>')
				{
					tagCnt--;
					if (tagCnt == 0)
					{
						bCloseTagFound = gxTrue;
						break;
					}
				}
			}

			if (bCloseTagFound)
			{
				//閉じカッコを発見したのでタグの開始として認める
				start = ii + 1;
				cnt++;
			}
		}

		SInfo info = itr->second;
		info.texPage = m_TexturePage;

		if (id == '>')
		{
			if (start >= 0)
			{
				//ただのカーソルとか
				std::vector<std::vector<Uint8>> list;
				num = ii - start;
				char32_t buf[256];
				memcpy(buf, (char*)&pStr[start * 4], 4 * num);
				buf[num] = 0x0000;
				list = splitString(buf, 4 * num);
				if( makeCmd(list) )
				{
					//spriteのときは続ける
					cnt--;
					start = -1;
				}
				else
				{
					cnt--;
					start = -1;
					continue;
				}
			}
		}

		if (cnt > 0)
		{
			//TAGのなかなので表示用の処理は行わない
			continue;
		}

		StSpriteEX sprex;
		sprex.lineIndex = ln;
		sprex.spr.trans.pos = { float(x),float(y),0 };
		sprex.spr.albedo = {
							info.texPage,
							info.u,
							info.v,
							info.w,
							info.h,
							0,
							0,
		};
		sprex.spr.shader = gxShaderFont;


		if (m_ChangeSpr.page != -1)
		{
//			m_ChangeSpr = { 0,0,0,160,160 ,0,80};

			sprex.spr.shader = gxShaderDefault;
			sprex.spr.albedo = m_ChangeSpr;
			info.w = sprex .spr.albedo.w;
			//info.h = sprex.spr.albedo.h;
			m_ChangeSpr.page = -1;
			id = 999;
		}

		Float32 fScale = m_Scale.back();

		w = info.w + m_SpacingW;
		h = info.h;

		if (id < 256 && m_HalfSizeSameWidth )
		{
			//半角か等幅指定の時は大きさを決め打ちにする
			w = (m_FontSize / 2 );// + space;
		}
		else if (m_bTouhaba)
		{
			w = m_FontSize + space;
		}
		else
		{
			w = w+space;
		}

		w *= fScale;

		if( (x+w) > m_NewLineWidth && m_NewLineWidth > 0.0f )
		{
			//自動改行処理が必要な時
			if (m_GyomatsyKinsoku.find(id) == m_GyomatsyKinsoku.end())
			{
				//行末禁則処理に該当する文字がなければ改行する
				m_LineWidth.push_back(x);
				ln++;
				x = 0;
				y += lineHeight;
				m_Height += lineHeight;
				ii--;
				continue;
			}


		}

		x += w;

		if (h+m_SpacingH > lineHeight) lineHeight = h+m_SpacingH;
		if (x > m_Width)    m_Width = x;

		sprex.spr.option[0] = info.pg;
		sprex.spr.argb = m_Color.back();
		sprex.spr.trans.scl.x = fScale;
		sprex.spr.trans.scl.y = fScale;
//		sprex.spr.trans.pos.y += -(sprex.spr.albedo.h * fScale) / 2;


		m_Sprites.push_back(sprex);
	}

	m_LineWidth.push_back(x);
	m_Height += lineHeight;
	ln++;

}

void CFontManager::draw( Sint32 ax , Sint32 ay, Sint32 prio , Uint32 atr , Uint32 argb , Float32 fRot , Float fScale )
{
    //実際の描画リクエストの発行
	m_DrawnRect.x1 = 99999;
	m_DrawnRect.y1 = 99999;
	m_DrawnRect.x2 = 0;
	m_DrawnRect.y2 = 0;

    if (atr & ATR_STR_RIGHT)
    {
        for (int ii = 0; ii < m_Sprites.size(); ii++)
        {
            auto p = &m_Sprites[ii];
            Sint32 w = m_LineWidth[p->lineIndex];
            p->spr.trans.pos.x = m_Width - w + p->spr.trans.pos.x - m_Width;

        }
    }
    else if (atr & ATR_STR_CENTER)
    {
        for (int ii = 0; ii < m_Sprites.size(); ii++)
        {
            auto p = &m_Sprites[ii];
            Sint32 w = m_LineWidth[p->lineIndex];
            p->spr.trans.pos.x = m_Width/2 - w/2 + p->spr.trans.pos.x- m_Width/2;
        }
    }

    if (atr & ATR_STR_MID)
    {
        for (int ii = 0; ii < m_Sprites.size(); ii++)
        {
            auto p = &m_Sprites[ii];
            Sint32 h = m_Height / 2;
            //p->spr.trans.pos.y -= h;
			p->spr.trans.pos.y -= h * p->spr.trans.scl.y;

        }
    }
    if (atr & ATR_STR_BOTTOM)
    {
        for (int ii = 0; ii < m_Sprites.size(); ii++)
        {
            auto p = &m_Sprites[ii];
            Sint32 h = m_Height;
            p->spr.trans.pos.y -= h* p->spr.trans.scl.y;

        }
    }

	//
	if(atr&ATR_STR_OUTLINE)
	{
		Sint32 ox = 0;
		Sint32 oy = 0;
		Uint32 argb2 = 0x80010101;

		int argb1_alp = (argb & 0xFF000000)>>24;
		int argb2_alp = (argb2 & 0xFF000000) >> 24;
		int alpha = argb2_alp * argb1_alp / 0xff;
		Float32 fAlpha = alpha / 255.0f;
		argb2 = SET_ALPHA(fAlpha, argb2);
		//argb2 = 0xFFFF0000;

		for (int ii = 0; ii < m_Sprites.size(); ii++)
	    {
	        for( Float32 fr=0.0f ; fr<360.0f; fr+=90.0f)
	        {
				ox = gxUtil::Cos(fr)*2.0f;
				oy = gxUtil::Sin(fr)*2.0f;
				drawFont( m_Sprites[ii] , ax+ox, ay+oy, prio, atr, argb2, fRot, m_fScale );
	        }
	    }
	}


	if(atr&ATR_STR_SHADOW)
	{
	    for (int ii = 0; ii < m_Sprites.size(); ii++)
	    {
			drawFont( m_Sprites[ii] , ax+2, ay+2, prio, atr, 0xff010101, fRot, m_fScale );
	    }
	}

    for (int ii = 0; ii < m_Sprites.size(); ii++)
    {
		drawFont( m_Sprites[ii] , ax, ay, prio, atr, argb, fRot, m_fScale );
    }


/*
        //outline
        gxLib::StSprite spr = m_Sprites[ii].spr;
        //spr.argb = SET_ALPHA(0.25f , spr.argb);//0x80808080;
        spr.argb = 0x80202020;

        mx = m_Sprites[ii].spr.trans.pos.x;
        my = m_Sprites[ii].spr.trans.pos.y;
        
        for( Float32 fr=0.0f ; fr<360.0f; fr+=45.0f)
        {
            spr.trans.pos.x = mx + gxUtil::Cos(fr)*4.0f;
            spr.trans.pos.y = my + gxUtil::Sin(fr)*4.0f;
            gxLib::PutSpriteEx(&spr);
        }
*/


    //gxLib::DrawBox(ax, ay, ax + m_Width, ay + m_Height, 100, gxFalse,ATR_DFLT, 0xff00ff00, 3.0f);

}


void CFontManager::drawFont(StSpriteEX sprite , Sint32 ax, Sint32 ay, Sint32 prio, Uint32 atr, Uint32 argb, Float32 fRot, Float fScale)
{
	Float32 fRot2 = fRot;
	if( atr&ATR_STR_ADJUSTROT ) fRot2 = 0;


	if (fRot != 0.0f)
	{
		//Sint32 mx = 0, my = 0;
		gxVector2 pnt;

		pnt.x = sprite.spr.trans.pos.x;
		pnt.y = sprite.spr.trans.pos.y;
		gxUtil::RotationPoint(&pnt, fRot);
		sprite.spr.trans.pos.x = pnt.x;
		sprite.spr.trans.pos.y = pnt.y;
		sprite.spr.trans.pos.z = prio;
		sprite.spr.trans.rot.z = fRot2;
	}

	if (fScale != 1.0f)
	{
		sprite.spr.trans.pos.x *= fScale;
		sprite.spr.trans.pos.y *= fScale;
		sprite.spr.trans.scl.x = fScale;
		sprite.spr.trans.scl.y = fScale;
	}

	sprite.spr.trans.pos.x += ax;
	sprite.spr.trans.pos.y += ay;
	sprite.spr.trans.pos.z = prio;

	if (sprite.spr.argb == 0x00000000)
	{
		sprite.spr.argb = argb;
	}
	else
	{
		//せめてアルファだけは反映させる
		Float32 alp1 = GET_ALPHA(sprite.spr.argb);
		Float32 alp2 = GET_ALPHA(argb);
		alp1 = alp1*alp2;
		sprite.spr.argb = SET_ALPHA(alp1,sprite.spr.argb);

	}

	gxBool bDraw = gxTrue;
	//if ((sprite.spr.trans.pos.x + sprite.spr.albedo.w * fScale) > WINDOW_W) bDraw = gxFalse;
	//if ((sprite.spr.trans.pos.y + sprite.spr.albedo.h * fScale) > WINDOW_H) bDraw = gxFalse;
	//if ((sprite.spr.trans.pos.x ) < -(sprite.spr.albedo.w * fScale)) bDraw = gxFalse;
	//if ((sprite.spr.trans.pos.y ) < -(sprite.spr.albedo.h * fScale)) bDraw = gxFalse;

	if(bDraw)	gxLib::PutSprite(&sprite.spr);


	if( sprite.spr.trans.pos.x < m_DrawnRect.x1 ) m_DrawnRect.x1 = sprite.spr.trans.pos.x;
	if( m_DrawnRect.x2 < sprite.spr.trans.pos.x+ sprite.spr.albedo.w* fScale) m_DrawnRect.x2 = sprite.spr.trans.pos.x + sprite.spr.albedo.w* fScale;

	if( sprite.spr.trans.pos.y < m_DrawnRect.y1 ) m_DrawnRect.y1 = sprite.spr.trans.pos.y;
	if( m_DrawnRect.y2 < sprite.spr.trans.pos.y + sprite.spr.albedo.h* fScale) m_DrawnRect.y2 = sprite.spr.trans.pos.y+ sprite.spr.albedo.h* fScale;

	/*
	gxLib::PutSprite(
		&sprite.spr.albedo,
		sprite.spr.trans.pos.x,
		sprite.spr.trans.pos.y,
		sprite.spr.trans.pos.z,
		0,
		sprite.spr.argb,
		sprite.spr.rot.fz,
		sprite.spr.scl.fx,
		sprite.spr.scl.fy
	);
	*/

}



void CFontManager::cmdTest()
{
/*
    wchar_t* p = L"dialog<color,ffffff>test</color>head";

	size_t sz = wcslen(p);
	int cnt = 0;
	int start = -1;
	int num   = -1;
	for (int ii = 0; ii < sz; ii++)
	{
		wchar_t w = p[ii];
		if (w == '<')
		{
			start = ii+1;
			cnt++;
		}
		if (w == '>')
		{
			std::vector<std::wstring> list;
			num = ii - start;
			wchar_t buf[256];
			memcpy(buf, (char*)&p[start], sizeof(wchar_t)*num);
			buf[num] = 0x0000;
			list = splitString(buf);
			makeCmd(list);
			cnt--;
		}
	}
*/
}
