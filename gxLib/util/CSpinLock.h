/**
 *	@file SpinLock.h
 *  @brief	spin lock
 *  @note
        https://cpprefjp.github.io/reference/atomic/atomic_flag.html
 */
#ifndef CSPINLOCK_H
#define CSPINLOCK_H

// スピンロックの実装.
#include <atomic>
//#include <mutex>

class CSpinLock
{
    std::atomic_flag m_state;

public:
    CSpinLock() : m_state() {}

    void lock()
    {
        // 現在の状態をロック状態にする
        while (m_state.test_and_set(std::memory_order_acquire))
        {
            // busy-wait...アンロックされるまで待機
         #ifndef NDEBUG
            static unsigned s_u;
            ++s_u;
         #endif
        }
    }

    void unlock()
    {
        // 値をアンロック状態にする
        m_state.clear(std::memory_order_release);
    }
};

#endif
