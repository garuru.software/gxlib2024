﻿#include <gxLib.h>
#include <gxLib/gxPadManager.h>
#include "CVirtualPad.h"

SINGLETON_DECLARE_INSTANCE( CVirtualStick );

static gxVector2 tbl[] = {
	{ 0.75f + 0.1f * 0, 0.9f},		//A
	{ 0.75f + 0.1f * 1, 0.9f-0.05f},//B

	{ 0.7f + 0.1f * 0, 0.8f},		//X
	{ 0.7f + 0.1f * 1, 0.8f-0.05f},	//Y

	{ 0.05f       , 0.5f},		//L1
	{ 1.0f - 0.05f, 0.5f},		//R1
	{ 0.5f - 0.1f * 1, 0.1f},	//SL
	{ 0.5f + 0.1f * 1, 0.1f},	//ST
};

void CVirtualStick::Action()
{
	if( !m_bDisp ) return;

	stickControl( &m_StickL );
	btnControl();

	if( m_DispWait > 0 )
	{
		m_DispWait --;
	}

	gxBool bPush = gxFalse;

	for( Sint32 ii=0; ii<3; ii++ )
	{
		if( gxUtil::Touch(ii)->IsPush() )
		{
			bPush = gxTrue;
		}
	}
	
	if(bPush)
	{
		m_DispWait = FRAME_PER_SECOND*2;
	}

}

void CVirtualStick::Draw()
{
	if (!m_bDisp) return;
	if( m_DispWait == 0 ) return;

	stickDraw(&m_StickL);

	for (Sint32 ii = 0; ii < enVS_ButtonMax; ii++)
	{
		btnDraw(ii);
	}
}

void CVirtualStick::btnControl()
{
	Sint32 gw,gh,sw,sh;
	gxLib::GetDeviceResolution( &gw , &gh , &sw , &sh );

	Float32 fLengthW = 0.12f * sw;
	Float32 fLengthH = 0.12f * sh;
	Float32 fLength = (fLengthW < fLengthH) ? fLengthW : fLengthH;


	//mx = gxLib::Joy(0)->mx;
	//my = gxLib::Joy(0)->my;


	for (Sint32 ii = 0; ii < enVS_ButtonMax; ii++)
	{
		m_Button[ii].m_Pos.x = sw * tbl[ii].x;
		m_Button[ii].m_Pos.y = sh * tbl[ii].y;

		//m_Button[ii].m_bPush = gxFalse;

		for (Sint32 jj = 0; jj < 3; jj++)
		{
			Sint32 mx, my;
			mx = gxUtil::Touch(jj)->GetPosition().x;
			my = gxUtil::Touch(jj)->GetPosition().y;
			gxLib::GetNativePosition(&mx, &my);

			Sint32 x, y;
			x = m_Button[ii].m_Pos.x;
			y = m_Button[ii].m_Pos.y;
			m_Button[ii].m_Rect.SetWH(x - fLength / 2, y - fLength / 2, fLength, fLength);

			if (!gxUtil::Touch(jj)->IsPush())
			{
				continue;
			}

			if (m_Button[ii].m_Rect.IsHit(mx, my))
			{
				if (gxUtil::Touch(jj)->IsTrigger())
				{
					m_Button[ii].m_bPush = gxTrue;
					m_Button[ii].m_TouchID = jj;
					break;
				}
			}
		}
	}

	for (Sint32 ii = 0; ii < enVS_ButtonMax; ii++)
	{
		Sint32 id = m_Button[ii].m_TouchID;
		if ( id != -1 && (!gxUtil::Touch(id)->IsPush()) )
		{
			m_Button[ii].m_bPush = gxFalse;
			m_Button[ii].m_TouchID = -1;
		}

		if(m_Button[ii].m_bPush)
		{
			m_Button[ii].m_fRingWidth += (1.0f - m_Button[ii].m_fRingWidth) / 3.0f;
		}
		else
		{
			m_Button[ii].m_fRingWidth += (0.0f - m_Button[ii].m_fRingWidth) / 10.0f;
		}
	}

	Uint32 bPush = 0x00;

	if (m_Button[enVSButtonA].m_bPush)   bPush |= BTN_A;
	if (m_Button[enVSButtonB].m_bPush)   bPush |= BTN_B;
	if (m_Button[enVSButtonX].m_bPush)   bPush |= BTN_X;
	if (m_Button[enVSButtonY].m_bPush)   bPush |= BTN_Y;
	if (m_Button[enVSButtonL1].m_bPush)  bPush |= BTN_L1;
	if (m_Button[enVSButtonR1].m_bPush)  bPush |= BTN_R1;
	if (m_Button[enVSButtonSelect].m_bPush)  bPush |= BTN_SELECT;
	if (m_Button[enVSButtonStart].m_bPush)  bPush |= BTN_START;
	
	gxPadManager::GetInstance()->SetVPadInfo(bPush);
	//gxLib::DrawBox(0, 0, fLength, fLength, 1000, gxTrue, ATR_DFLT, 0xff00ff00);
	//gxLib::DrawBox(sw - fLength, sh - fLength, sw, sh, 1000, gxTrue, ATR_DFLT, 0xff00ff00);
}


void CVirtualStick::stickControl( StStick *pStick )
{
	Sint32 gw,gh,sw,sh;
	Sint32 mx , my;
	gxBool bPush = gxFalse;
	gxBool bTrigger = gxFalse;
	gxLib::GetDeviceResolution( &gw , &gh , &sw , &sh );

//	mx = gxLib::Joy(0)->mx;
//	my = gxLib::Joy(0)->my;

	if( pStick->m_FingerID == -1 )
	{
		//今まで押していなかった場合
		for( Sint32 ii=0; ii<3; ii++ )
		{
			if( gxUtil::Touch(ii)->IsPush() )
			{
				mx = gxUtil::Touch(ii)->GetPosition().x;
				my = gxUtil::Touch(ii)->GetPosition().y;
				gxLib::GetNativePosition( &mx , &my );

				if( gxUtil::Touch(ii)->IsTrigger() )
				{
					if( mx < sw/2.0f )
					{
						bPush = gxTrue;
						pStick->m_FingerID = ii;
						pStick->m_Old.x = mx;
						pStick->m_Old.y = my;
						bTrigger = gxTrue;
					}
				}
			}
		}
	}
	else
	{
		//さっきまで押してた場合

		if( gxUtil::Touch( pStick->m_FingerID )->IsPush() )
		{
			mx = gxUtil::Touch(pStick->m_FingerID)->GetPosition().x;
			my = gxUtil::Touch(pStick->m_FingerID)->GetPosition().y;
			gxLib::GetNativePosition( &mx , &my );
			bPush = gxTrue;
		}
		else
		{
			//離した
			pStick->m_FingerID = -1;
		}
	}

	if( bPush )
	{
		//指が確定した
        mx = gxUtil::Touch(pStick->m_FingerID)->GetPosition().x;
        my = gxUtil::Touch(pStick->m_FingerID)->GetPosition().y;
        gxLib::GetNativePosition( &mx , &my );

		if( bTrigger )
		{
			if (pStick->m_sTimer == 0)
			{
				//初回タッチ

				//中心位置を設定する
				pStick->m_Pos.x = mx;
				pStick->m_Pos.y = my;
				pStick->m_Old.x = mx;
				pStick->m_Old.y = my;
				pStick->m_Spot.x = mx;
				pStick->m_Spot.y = my;
			}
		}

		//さっきの中心位置を利用する
		//現在のスティック位置を検出

		//移動の差分
		Float32 sx,sy;
		sx = mx - pStick->m_Old.x;
		sy = my - pStick->m_Old.y;

		//------------------------------------

		Float32 cx, cy, ax, ay;
		cx = pStick->m_Pos.x;
		cy = pStick->m_Pos.y;
		ax = pStick->m_Spot.x  + sx;
		ay = pStick->m_Spot.y  + sy;

		//角度を設定する
		Float32 fRot, fDist;
		fRot  = gxUtil::Angle(ax - cx, ay - cy);
		fDist = gxUtil::Distance(cx, cy, ax, ay);
		if (fDist >= Float32(enStickRange)) fDist = Float32(enStickRange);

		pStick->m_Center.x = cx;
		pStick->m_Center.y = cy;
		pStick->m_Spot.x   = cx + gxUtil::Cos(fRot)*fDist;
		pStick->m_Spot.y   = cy + gxUtil::Sin(fRot)*fDist;

		pStick->m_sTimer = FRAME_PER_SECOND / 8;	//しばらく表示されている

		while (fRot < 0) fRot += 360.0f;
		while (fRot > 360) fRot -= 360.0f;

		Uint32 pressButtonUD = 0x00000000;
		Uint32 pressButtonLR = 0x00000000;
		Uint32 pressButton   = 0x00000000;

		if (fDist > 2)
		{
			//遊び部分を超えていれば入力したことにする

			if (fRot >= 180 + 45 - 20 && fRot < 270 + 45 + 20 )
			{
				pressButtonUD |= JOY_U;
			}
			else if (fRot >= 0 + 45 - 20 && fRot <  90 + 45 + 20   )
			{
				pressButtonUD |= JOY_D;
			}
			if (fRot >= 270 + 45 - 20 || fRot <   0 + 45 + 20 )
			{
				pressButtonLR |= JOY_R;
			}
			else if (fRot >= 90 + 45 - 20 && fRot < 180 + 45 + 20  )
			{
				pressButtonLR |= JOY_L;
			}

			if (fDist < enStickRange/2 )
			{
				//中心付近の時は4方向レバーとする
				if( pressButtonUD )
				{
					pressButton = pressButtonUD;
				}
				else
				{
					pressButton = pressButtonLR;
				}
			}
			else
			{
				//外側の時
				pressButton = pressButtonUD|pressButtonLR;
			}

			gxPadManager::GetInstance()->SetVPadInfo(pressButton);
			gxPadManager::GetInstance()->SetVPadAnalog( (pStick->m_Spot.x - pStick->m_Center.x)/ int(enStickRange), (pStick->m_Spot.y - pStick->m_Center.y)/ int(enStickRange));
		}

		pStick->m_Old.x = mx;
		pStick->m_Old.y = my;

		/*
		if( pressButton != pStick->m_OldLever )
		{
			gxLib::SetRumble(0,2);
		}
		*/
		pStick->m_OldLever = pressButton;
	}
	else
	{
		//スティックをセンターに戻す
		pStick->m_Spot.x = pStick->m_Center.x;
		pStick->m_Spot.y = pStick->m_Center.y;

		if( pStick->m_sTimer > 0 ) pStick->m_sTimer --;

		pStick->m_OldLever = 0;
	}

}

void CVirtualStick::stickDraw( StStick *pStick )
{
	//スティックの表示

	if( pStick->m_sTimer == 0 ) return;

	Sint32 ax1,ay1,az;
	Sint32 ax2,ay2;
	Sint32 w = enStickRange;
	az = 1000;

	ax1 = pStick->m_Center.x;
	ay1 = pStick->m_Center.y;

	ax2 = pStick->m_Spot.x;
	ay2 = pStick->m_Spot.y;

	gxLib::DrawCircle(ax1, ay1-160, az, ATR_DFLT, 0xf080ffff, 128.0f, 6.0f);
	gxLib::DrawPoint (ax2, ay2-160, az, ATR_DFLT, 0xff00ff80, 64.0f);

	//gxLib::DrawBox( ax1 - w , ay1 -w , ax1 + w , ay1 + w , az , gxFalse ,ATR_DFLT , ARGB_DFLT );
	//gxLib::DrawBox( ax2 - 32 , ay2 -32 , ax2 + 32 , ay2 + 32 , az , gxTrue ,ATR_DFLT , ARGB_DFLT );
}

void CVirtualStick::btnDraw( Sint32 id )
{
	gxBool bFill = gxFalse;
	Sint32 x1, y1, x2, y2, az;
	az = 1000;

	x1 = m_Button[id].m_Rect.x1;
	y1 = m_Button[id].m_Rect.y1;
	x2 = m_Button[id].m_Rect.x2;
	y2 = m_Button[id].m_Rect.y2;

	Sint32 x, y;
	x = (x1 + x2 ) / 2;
	y = (y1 + y2) / 2;

	Uint32 argb = 0xf080ffff;

	if (m_Button[id].m_bPush)
	{
		gxLib::DrawCircle(x, y, az, ATR_DFLT, 0xf0ff8000, 320.0f*m_Button[id].m_fRingWidth, 3.0f);
		argb = 0xf0ff8000;
	}

	gxLib::DrawPoint(x, y, az, ATR_DFLT, argb, 90);
}

