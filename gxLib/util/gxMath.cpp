﻿#include <gxLib.h>
#include "../gx.h"
//#include "gxMath.h"

//-----------------------------------------------------
//円と円の当たり判定
//-----------------------------------------------------
CircleCircle::CircleCircle()
{

	m_Circle[0].x = 0;
	m_Circle[0].y = 0;
	m_Circle[0].r = 1;

	m_Circle[1].x = 0;
	m_Circle[1].y = 0;
	m_Circle[1].r = 1;

	m_Near.x = 0;
	m_Near.y = 0;

	m_Kouten[0].x = 0;
	m_Kouten[0].y = 0;

	m_Kouten[1].x = 0;
	m_Kouten[1].y = 0;

	m_sNearest = -1;
}


CircleCircle::~CircleCircle()
{


}


Sint32 CircleCircle::Calc()
{
	//-------------------------------------
	//交点を計算
	//-------------------------------------

	Float32	rksq, rlsq, xlk, ylk, distsq, delrsq, sumrsq, root, dstinv,
			scl, xfac, yfac, x, y, x1, y1, x2, y2, sqdst1, sqdst2;
	Float32 accy = 1.0f;//E-15;

    rksq = pow(m_Circle[0].r, 2.0f);
    rlsq = pow(m_Circle[1].r, 2.0f);

    xlk = m_Circle[1].x - m_Circle[0].x;
    ylk = m_Circle[1].y - m_Circle[0].y;

    distsq = pow(xlk, 2.0f) + pow(ylk, 2.0f);
    
    if(distsq < accy)
	{
        return (0);	//同じだった？
    }
	else
	{
        delrsq = rlsq - rksq;
        sumrsq = rksq + rlsq;
        root = 2.0f*sumrsq*distsq - pow(distsq, 2.f) - pow(delrsq, 2.f);
        
        if (root < -accy)
		{
	        return (0);	//同じだった？
        }
		else
		{
            dstinv = 0.5f / distsq;
            scl = 0.5f - delrsq*dstinv;
            x = xlk*scl + m_Circle[0].x;
            y = ylk*scl + m_Circle[0].y;
            
            if (root < accy)
			{
			   /* 2つの円は接する。*/
				m_Kouten[0].x = x;
				m_Kouten[0].y = y;
				m_sNearest = 0;
                return (1);
            }
			else
			{
                root = dstinv * sqrt(root);
                xfac = xlk * root;
                yfac = ylk * root;
                x1 = x - yfac;
                y1 = y + xfac;
                x2 = x + yfac;
                y2 = y - xfac;
            }
         }
    }

	//交点が２つある場合

    sqdst1 = pow((m_Near.x-x1), 2.0f) + pow((m_Near.y-y1), 2.0f);	//交点との距離１
    sqdst2 = pow((m_Near.x-x2), 2.0f) + pow((m_Near.y-y2), 2.0f);	//交点との距離２

    if (sqdst1 <= sqdst2)
	{
		m_Kouten[0].x = x1;
		m_Kouten[0].y = y1;

		m_Kouten[1].x = x2;
		m_Kouten[1].y = y2;

		m_sNearest = 0;
    }
	else
	{
		m_Kouten[0].x = x2;
		m_Kouten[0].y = y2;

		m_Kouten[1].x = x1;
		m_Kouten[1].y = y1;

		m_sNearest = 1;
	}

    return (2);
}

void gxUtil::RotationPoint( gxVector2 *pPoint, Float32 fRot )
{
	Float32 fSin = gxUtil::Sin(fRot);
	Float32 fCos = gxUtil::Cos(fRot);

	Float32 fPx = (fCos * pPoint->x) - (fSin * pPoint->y);
	Float32 fPy = (fSin * pPoint->x) + (fCos * pPoint->y);

	pPoint->x = fPx;
	pPoint->y = fPy;
}

Float32 gxUtil::Cos( Float32 deg )
{
	Float32 l;

	l = cos( DEG2RAD(deg) );

	return l;
}


Float32 gxUtil::Sin( Float32 deg )
{
	Float32 l;

	l = sin( DEG2RAD(deg));

	return l;
}


Float32 gxUtil::Angle( Float32 x ,Float32 y )
{
	Float32 r = atan2( y , x );

	return RAD2DEG(r);

}


Float32 gxUtil::Distance( Float32 x ,Float32 y )
{
	x = x/100.f;
	y = y/100.f;

	Float32 dist = (Float32)sqrt(x*x+y*y);
	return dist*100.f;

	return (Float32)sqrt(x*x+y*y);
}

Float32 gxUtil::Distance( Float32 x1 ,Float32 y1 , Float32 x2 ,Float32 y2 )
{
	x1 = (x1-x2)/100.f;
	y1 = (y1-y2)/100.f;

	Float32 dist = (Float32)sqrt(x1*x1+y1*y1);

	return dist*100.f;
}


Float32 gxUtil::Sqrt( Float32 n )
{
	return (Float32)sqrt(n);
}


void gxUtil::BezierCurve( Float32 fromX, Float32 fromY, Float32 cpX, Float32 cpY, Float32 cpX2, Float32 cpY2, Float32 toX, Float32 toY, Float32 div ,std::vector<gxVector2> &path )
{
	//ベジェ曲線を描く
	path.clear();
    Float32 dt = 0;
    Float32 dt2 = 0;
    Float32 dt3 = 0;
    Float32 t2 = 0;
    Float32 t3 = 0;

	gxVector2 pts;
	pts.Set(fromX, fromY);

    path.push_back(pts);

    for ( Float32 i = 1, j = 0; i <= div; ++i )
    {
        j = i / div;

        dt = (1 - j);
        dt2 = dt * dt;
        dt3 = dt2 * dt;

        t2 = j * j;
        t3 = t2 * j;

		pts.Set(
            (dt3 * fromX) + (3 * dt2 * j * cpX) + (3 * dt * t2 * cpX2) + (t3 * toX),
            (dt3 * fromY) + (3 * dt2 * j * cpY) + (3 * dt * t2 * cpY2) + (t3 * toY)
		);
	    path.push_back(pts);
    }
}


gxBool gxUtil::IsInsidePolygon( gxVector2 &p, gxVector2* poly , Uint32 length )
{
	//点と多角形の内外判定

	gxVector2 p1, p2;
	gxBool bInside = gxFalse;

	gxVector2 oldPoint = poly[ length-1 ];

	for ( Uint32 ii=0; ii<length; ii++)
	{
		gxVector2 newPoint = poly[ii];
		if (newPoint.x > oldPoint.x)
		{
			p1 = oldPoint;
			p2 = newPoint;
		}
		else
		{
			p1 = newPoint;
			p2 = oldPoint;
		}


		if ((p1.x < p.x)  == (p.x <= p2.x) && (p.y-p1.y)*(p2.x-p1.x) < (p2.y-p1.y)*(p.x-p1.x) )
		{
			bInside = !bInside;
		}

		oldPoint = newPoint;
	}

	return bInside;
}


gxBool gxUtil::IsCross( gxVector2 &r1, gxVector2 &r2, gxVector2 &p1, gxVector2 &p2 )
{
	//線と線の衝突判定

	Float32 t1, t2;

	//衝突判定計算
	t1 = (r1.x-r2.x)*(p1.y-r1.y)+(r1.y-r2.y)*(r1.x-p1.x);
	t2 = (r1.x-r2.x)*(p2.y-r1.y)+(r1.y-r2.y)*(r1.x-p2.x);

	//それぞれの正負が異なる（積が負になる）か、0（点が直線上にある）
	//ならクロスしている

	if( t1*t2 < 0 || t1 == 0 || t2 == 0 )
	{
		t1 = (p1.x-p2.x)*(r1.y-p1.y)+(p1.y-p2.y)*(p1.x-r1.x);
		t2 = (p1.x-p2.x)*(r2.y-p1.y)+(p1.y-p2.y)*(p1.x-r2.x);
		if( t1*t2 < 0 || t1 == 0 || t2 == 0 )
		{
			return( gxTrue ); //クロスしている
		}
	}
	else
	{
		return( gxFalse );	  //クロスしない
	}

	return( gxFalse );	  //クロスしない

}


gxBool gxUtil::PolyvsPoly( gxVector2 *p1 , Uint32 length1 , gxVector2* p2 , Uint32 length2 )
{
	//多角形同士の衝突判定
	//p1・・・・・多角形１の頂点データ
	//length1・・ 多角形の頂点数
	//p2・・・・・多角形１の頂点データ
	//length2・・ 多角形の頂点数

	gxVector2 *q1, *q2;
	gxVector2 *r1, *r2;

	gxBool bHit = gxFalse;

	//線と線で当たり判定を得る

	for (Uint32 ii=0; ii<length1; ii++)
	{
		q1 = &p1[ii];
		q2 = &p1[(ii + 1) % length1];

		for (Uint32 jj = 0; jj < length2; jj++)
		{
			r1 = &p2[jj];
			r2 = &p2[(jj + 1) % length2];

			if( IsCross( *q1, *q2, *r1, *r2) )
			{
				return gxTrue;
			}
		}
	}

	//重なりは検出できなかったが内包されている可能性を調査する

	gxBool bInside = gxTrue;

	for (Uint32 ii=0; ii<length1; ii++)
	{
		if( !IsInsidePolygon( p1[ii], p2 , length2 ) )
		{
			bInside = gxFalse;
			break;
		}
	}

	if( !bInside )
	{
		bInside = gxTrue;

		for ( Uint32 ii=0; ii<length2; ii++)
		{
			if( !IsInsidePolygon( p2[ii], p1 , length1 ) )
			{
				//ポリゴンの外にあることがわかったので中断
				bInside = gxFalse;
				break;
			}
		}

		if( bInside )
		{
			return gxTrue;
		}
	}
	else
	{
		return gxTrue;
	}

	return gxFalse;

}


gxBool gxUtil::CirclevsCircle( gxVector2 *p1 , Float32 fRadius1 , gxVector2 *p2 , Float32 fRadius2 )
{
	//円と円の当たり判定

	Float32 fHankei = (fRadius1 + fRadius2) * (fRadius1 + fRadius2);
	Float32 fDist   = ( p1->x - p2->x )*( p1->x - p2->x ) + ( p1->y - p2->y )*( p1->y - p2->y );

	if( fDist > fHankei ) return gxFalse;

	return gxTrue;
}



void mathTest()
{
	gxVector2 rect1[]={
		{0,0},
		{100,10},
		{200,100},
		{10,100}
	};

	gxVector2 rect2[]={
		{300+0  ,100+0},
		{300+300,100+310},
		{300+200,100+500},
		{300+10 ,100+500}
	};

	static gxVector2 pos={0,0};

	if( gxLib::Joy(0)->psh&JOY_L) pos.x -= 8;
	if( gxLib::Joy(0)->psh&JOY_R) pos.x += 8;
	if( gxLib::Joy(0)->psh&JOY_U) pos.y -= 8;
	if( gxLib::Joy(0)->psh&JOY_D) pos.y += 8;

	gxVector2 px1[4],px2[4];

	for( Sint32 ii=0; ii<4; ii++ )
	{
		px1[ii] = rect1[ii];
		px1[ii].x += pos.x;
		px1[ii].y += pos.y;
	}

	for (Sint32 ii = 0; ii < 4; ii++)
	{
		px2[ii] = rect2[ii];
		//px2[ii].x += pos.x;
		//px2[ii].y += pos.y;
	}

	for (Sint32 ii = 0; ii < 4; ii++)
	{
		gxVector2 *p1,*p2;
		p1 = &px1[ii];
		p2 = &px1[(ii + 1) % 4];

		gxLib::DrawLine(p1->x, p1->y, p2->x, p2->y, 100, ATR_DFLT, 0xff0000ff, 3);
	}

	for (Sint32 ii = 0; ii < 4; ii++)
	{
		gxVector2* p1, * p2;
		p1 = &px2[ii];
		p2 = &px2[(ii + 1) % 4];

		gxLib::DrawLine(p1->x, p1->y, p2->x, p2->y, 100, ATR_DFLT, 0xffff0000, 3);
	}

	if( gxUtil::PolyvsPoly( px1 , 4 , px2 , 4 ) )
	{
		gxLib::Printf(WINDOW_W/2 , WINDOW_H/2 , MAX_PRIORITY_NUM , ATR_STR_CENTER , ARGB_DFLT , "HIT!" );
	}

	//void GeoWars();
	//GeoWars();

	if (gxLib::KeyBoard(gxKey::P) & enStatTrig)
	{
		gxLib::Pause(gxTrue);
	}

}


const Float32 gxVector2::Angle()
{
	return gxUtil::Angle(x, y);
}

const Float32 gxVector2::Distance()
{
	return gxUtil::Distance(x, y);
}

void gxVector2::SetPosFromRot( Float32 fRot , Float32 fDist )
{
	x = gxUtil::Cos(fRot)*fDist;
	y = gxUtil::Sin(fRot)*fDist;
}

//--------------------------------------------------
//--------------------------------------------------


#if 0 
#include <iostream>
#include <cmath>

// 外積を計算する関数
Float32 crossProduct(const gxVector2 & a, const gxVector2& b)
{
    return a.x * b.y - a.y * b.x;
}

// 与えられた座標が四角形の内部にあるかどうかを判定
bool gxUtil::IsInsideRectangle( const gxVector2& a , const gxVector2& b , const gxVector2& c , const gxVector2& d ,const gxVector2& p)
{
    Float32 AB = crossProduct({B.x - A.x, B.y - A.y}, {p.x - A.x, p.y - A.y});
    Float32 BC = crossProduct({C.x - B.x, C.y - B.y}, {p.x - B.x, p.y - B.y});
    Float32 CD = crossProduct({D.x - C.x, D.y - C.y}, {p.x - C.x, p.y - C.y});
    Float32 DA = crossProduct({A.x - D.x, A.y - D.y}, {p.x - D.x, p.y - D.y});

    return (AB >= 0 && BC >= 0 && CD >= 0 && DA >= 0) || (AB <= 0 && BC <= 0 && CD <= 0 && DA <= 0);
}

/*
int main() {
	// 四角形の頂点を設定
	Point A = {2, 2};
	Point B = {3, 1};
	Point C = {3, 3};
	Point D = {4, 4}; // 例として四角形の4つ目の頂点を追加

    Point testPoint = {2.5, 2.5}; // テスト用の座標 (例: 四角形の内部)
    if (isInsideRectangle(testPoint)) {
        std::cout << "指定した座標は四角形の内部にあります。\n";
    } else {
        std::cout << "指定した座標は四角形の外部にあります。\n";
    }

    return 0;
}
*/
#endif
