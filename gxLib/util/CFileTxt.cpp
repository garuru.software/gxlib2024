
#include <gxLib.h>
#include <gxLib/util/CFileCsv.h>
#include "gxLib/util/CFileZip.h"
#include "CFileTxt.h"

static char _tempbuf_[CTxt::eTempBufMax];

void CTxt::InsertLine( Sint32 line , std::string text)
{
	gxBool bLFExist = gxFalse;
	size_t start = 0;
	size_t end = 0;

	if (text.size() == 0)
	{
		//空文字を渡された場合は行の追加のみ
		addLine(nullptr,0,line);
		return;
	}

	for (size_t ii = 0; ii < text.size(); ii++)
	{
		//改行がある場合は複数の行に分ける
		switch (text[ii]) {
		case '\n':
			bLFExist = gxTrue;
			break;
		case '\r':
			break;
		default:
			end++;
			break;
		}

		gxBool bStrEnd = (ii == (text.size() - 1)) ? true : false;
		if (bLFExist || bStrEnd)
		{
			line = (Sint32)addLine( (Uint8*)(&text[start]), end - start, line );
			bLFExist = gxFalse;
			start = ii + 1;
			end = start;
		}
	}
}


void CTxt::RemoveLine(size_t line)
{
	if (m_List.size() == 0) return;
	if (line < 0) return;
	if (line > m_List.size()) return;

	auto itr = std::next(m_List.begin(), line);
	if (itr != m_List.end())
	{
		m_List.erase(itr);
	}
}

void CTxt::ReplaceLine(size_t line, std::string str)
{
	RemoveLine(line);
	InsertLine((Sint32)line, str);
}

gxBool CTxt::Load(std::string fileName)
{
	Clear();

	size_t uSize = 0;
	Uint8* pData = gxLib::LoadFile((const char*)fileName.c_str(), &uSize);

	if (Read(pData, uSize))
	{
		m_FileName = fileName;
	}
	else
	{
		SAFE_DELETES(pData);
		return gxFalse;
	}

	SAFE_DELETES(pData);

	return gxTrue;
}

gxBool CTxt::LoadSJIS(std::string fileName)
{
	Clear();

	size_t uSize = 0;
	Uint8* pData = gxLib::LoadFile((const char*)fileName.c_str(), &uSize);
	std::string sjis = (char*)pData;

	std::string u8 = gxUtil::SJIStoUTF8(sjis);

	return Read((Uint8*)u8.c_str(), u8.size());

	//std::vector<Uint32> dat;
	//Uint32 d;
	//
	//for (size_t ii = 0; ii < uSize; ii++)
	//{
	//	char c = pData[ii];
	//
	//	if (((c >= 0x81) && (c <= 0x9f)) || ((c >= 0xe0) && (c <= 0xfc)))
	//	{
	//		if (ii < uSize - 1)
	//		{
	//			d = (pData[ii] << 8) | pData[ii + 1];
	//		}
	//		dat.push_back(d);
	//		ii++;
	//	}
	//	else
	//	{
	//		d = pData[ii];
	//		dat.push_back(d);
	//	}
	//}




}

gxBool CTxt::Read( Uint8 *pData , size_t uSize)
{
	if (pData == nullptr) return gxFalse;

	printf("Now Loading...\r\n");

	if (!analysis(pData, uSize))
	{
		return gxFalse;
	}

	return gxTrue;
}

gxBool CTxt::Save(std::string fileName)
{
	//size_t uSize = 0;
	std::string output;

	std::vector<Uint32> u32s;

	char buf[32] = {0};
//	Sint32 len = sprintf(buf, "%s", LF);
	Sint32 len = sprintf(buf, "%s", m_LF.c_str());

	std::vector<Uint32> lf;
	for (Sint32 ii = 0; ii < len; ii++)
	{
		lf.push_back(buf[ii]);
	}

	size_t lineIndex = 0;
	for (auto itr = m_List.begin(); itr != m_List.end(); ++itr)
	{
		auto &ref = (*itr);
		u32s.insert(u32s.end(), ref.begin(), ref.end());

		//改行を挿入

		if (lineIndex == m_List.size() - 1)
		{
			//最後の行だった
			if (ref.size() == 0)
			{
				//１文字もなければここで終わり（改行させない）
				u32s.push_back(0x00000000);
				break;
			}

		}
		u32s.insert(u32s.end(), lf.begin(), lf.end());
		lineIndex++;
	}

	if (u32s.size() > 0)
	{
		output += gxUtil::UTF32toUTF8(u32s);
	}

	gxLib::SaveFile(fileName.c_str(), (void*)output.c_str(), output.size());

	return gxTrue;
}

gxBool CTxt::SaveSJIS(std::string fileName)
{
//	size_t uSize = 0;
	std::string output;

	std::vector<Uint32> u32s;

	char buf[32] = { 0 };
	Sint32 len = sprintf(buf, "%s", m_LF.c_str());

	std::vector<Uint32> lf;
	for (Sint32 ii = 0; ii < len; ii++)
	{
		lf.push_back(buf[ii]);
	}

	size_t lineIndex = 0;
	for (auto itr = m_List.begin(); itr != m_List.end(); ++itr)
	{
		auto& ref = (*itr);
		u32s.insert(u32s.end(), ref.begin(), ref.end());

		//改行を挿入

		if (lineIndex == m_List.size() - 1)
		{
			//最後の行だった
			if (ref.size() == 0)
			{
				//１文字もなければここで終わり（改行させない）
				u32s.push_back(0x00000000);
				break;
			}

		}
		u32s.insert(u32s.end(), lf.begin(), lf.end());
		lineIndex++;
	}

	if (u32s.size() > 0)
	{
		std::string line = gxUtil::UTF32toUTF8(u32s);
		std::string sjis = CDeviceManager::UTF8toSJIS((char*)line.c_str());

		output += sjis;
	}

	gxLib::SaveFile(fileName.c_str(), (void*)output.c_str(), output.size());

	return gxTrue;
}

void CTxt::EraseBlanks()
{
	//余計な空白を削除する

//	int line = 0;
	bool dq = false;

	dqList = m_List;
	int spaceNum = 0;

	//文字列の領域を検出してダブルコーテーション内は空白を削除しないようにする

	for (int yy=0;yy< m_List.size(); yy++)
	{
		for (int xx=0; xx< m_List[yy].size(); xx++)
		{
			dqList[yy][xx] = 0x00;

			if (m_List[yy][xx] == '\"')
			{
				spaceNum = 0;
				dq = !dq;
				continue;
			}

			if (dq)
			{
				dqList[yy][xx] = 0x01;
				continue;
			}

			if (m_List[yy][xx] == ' ' || m_List[yy][xx] == '\t')
			{
				if (spaceNum == 0)
				{
					dqList[yy][xx] = 0x02;
				}
				spaceNum++;
			}
			else
			{
				spaceNum = 0;
			}
		}
	}

	bool temp = m_bSearchInsideDoubleQuatation;
	m_bSearchInsideDoubleQuatation = false;
	GrepReplace(" ", "");
	m_bSearchInsideDoubleQuatation = temp;
}

void CTxt::GrepReplace(std::string src, std::string dst, size_t x, size_t y)
{

	for( size_t ii=y; ii< m_List.size(); ii++)
	{
		EditGrepReplace(0, ii, src , dst );
//		printf("GrepReplace:%zd/%zd           \r", ii, m_List.size());
	}
	printf("\r\n");
}

void CTxt::EditGrepReplace(size_t x, size_t y, std::string src, std::string dst)
{
	CTxt::POS pos;
	pos.x = (Sint32)x;
	pos.y = (Sint32)y;
	while (true)
	{
		if (Search( src, &pos, pos.x, pos.y))
		{
			if (!m_bSearchInsideDoubleQuatation)
			{
				//ダブルコーテーション内は置換対象にしない処理
				if (dqList[pos.y][pos.x] > 0)
				{
					pos.x++;
					continue;
				}
			}

			if (y != pos.y) break;
			EditReplace(pos.x, pos.y, src, dst);
			pos.x += (Sint32)(dst.size()+0);
		}
		else
		{
			break;
		}
	}
}

void CTxt::EditReplace(size_t x, size_t y, std::string words, std::string str)
{
	auto itr = std::next(m_List.begin(), y);

//	size_t uSize = 0;
	std::vector<Uint32> words32 = gxUtil::UTF8toUTF32(words);

	if (itr != m_List.end())
	{
		std::vector<Uint32> u32line = (*itr);

		std::vector<Uint32> tmp;
		for (Sint32 ii = 0; ii < x; ii++)
		{
			tmp.push_back(u32line[ii]);
		}

		for (size_t ii = 0; ii < words32.size(); ii++)
		{
			if (u32line[x+ii] != words32[ii])
			{
				return;
			}
		}

		for (size_t ii=0; ii<str.size(); ii++)
		{
			tmp.push_back(str[ii]);
		}

		for (size_t ii = x+words32.size(); ii < u32line.size(); ii++)
		{
			tmp.push_back(u32line[ii]);
		}
		*itr = tmp;
	}
}

std::vector<Uint32> CTxt::getLine32(size_t y)
{
	auto itr = std::next(m_List.begin(), y);
	if (itr != m_List.end())
	{
		return *itr;
	}
	std::vector<Uint32> ret;

	return ret;
}

void CTxt::setLine32(size_t y , std::vector<Uint32> line)
{
	auto itr = std::next(m_List.begin(), y);

	if (itr != m_List.end())
	{
		*itr = line;
		return;
	}

	AppendLine();
	itr = std::next(m_List.begin(), y);
	if (itr != m_List.end())
	{
		*itr = line;
	}
}

void CTxt::EditDelete(size_t x, size_t y, size_t num)
{
	auto linestr = getLine32(y);

	std::vector<Uint32> dst;

	for (Sint32 ii = 0; ii < linestr.size(); ii++)
	{
		if (ii >= x && num == 0) break;
		if (ii >= x && ii < x + num) continue;
		dst.push_back(linestr[ii]);
	}

	setLine32(y, dst);
}


void CTxt::printf(char* buf, ...)
{
	//gxChar temp[eTempBufMax];

	va_list app;
	va_start(app, buf);
	vsprintf(_tempbuf_, buf, app);
	va_end(app);

	std::string tmpstr = _tempbuf_;

	Add(tmpstr);

	//printf("%s",tmpstr)
}

void CTxt::Add(std::string str)
{
	size_t y = GetLineNum();

	if (y > 0) y--;
	auto linestr = getLine32(y);

	std::vector<Uint32> str32 = gxUtil::UTF8toUTF32(str);

	for (Sint32 ii = 0; ii < str32.size(); ii++)
	{
		if (str32[ii] == '\r') continue;
		if (str32[ii] == '\n')
		{
			setLine32(y, linestr);
			linestr.clear();
			y++;
			continue;
		}
		linestr.push_back(str32[ii]);
	}

	setLine32(y, linestr);
}

void CTxt::EditInsert(size_t x, size_t y, std::string str)
{
	auto itr = std::next(m_List.begin(), y);

//	size_t uSize = 0;
	std::vector<Uint32> dst = gxUtil::UTF8toUTF32(str);

	if (itr != m_List.end())
	{
		std::vector<Uint32> u32 = (*itr);

		std::vector<Uint32> tmp;
		for (Sint32 ii = 0; ii < x; ii++)
		{
			tmp.push_back(u32[ii]);
		}

		for (size_t ii = 0; ii < dst.size(); ii++)
		{
			tmp.push_back(dst[ii]);
		}

		for (size_t ii = x; ii < u32.size(); ii++)
		{
			tmp.push_back(u32[ii]);
		}
		*itr = tmp;
	}
}

std::string CTxt::GetLine(size_t line)
{
	auto itr = std::next(m_List.begin(), line);

	if (itr != m_List.end())
	{
		std::vector<Uint32> u32 = (*itr);

		if (u32.size() == 0) return "";

//テスト		u32.push_back(0);

		return gxUtil::UTF32toUTF8(u32);
	}

	return "";
}

gxBool CTxt::Search(std::string text, POS *pPos , size_t x, size_t y)
{
	POS pos;
	pos.x = (Sint32)x;
	pos.y = (Sint32)y;

	auto u32findStr = gxUtil::UTF8toUTF32(text);

	std::vector<Uint32> src32;
	Sint32 findIndex = -1;
	for (auto itr = std::next(m_List.begin(), pos.y); itr != m_List.end(); ++itr)
	{
		size_t sz = (*itr).size();
		src32.clear();

		for (Sint32 ii=pos.x; ii<sz ; ii++)
		{
			src32.push_back((*itr)[ii]);
		}

		findIndex = CTxt::findString32(src32, u32findStr);
		if (findIndex >= 0)
		{
			pos.x = findIndex+pos.x;
			if (pPos) *pPos = pos;
			return gxTrue;
		}
		pos.x = 0;
		pos.y++;
	}
	pos.x = -1;
	pos.y = -1;
	if (pPos) *pPos = pos;

	return gxFalse;
}


std::vector<CTxt::POS> CTxt::GetFindList(std::string text, std::function<void(POS pos)>func)
{
	std::vector<POS> grepList;

	POS pos;

	while(gxTrue)
	{
		if (!Search(text, &pos, 0, pos.y))
		{
			break;
		}

		if (func)
		{
			func( pos );
		}

		grepList.push_back(pos);
		pos.y++;
	}

	return grepList;
}

gxBool CTxt::analysis(Uint8* pData, size_t size)
{
	if (size == 0)
	{
		return gxTrue;
	}

	char* p = new char[size + 1];

	if (p == nullptr)
	{
		return gxFalse;
	}

	gxUtil::MemCpy(p, pData, size);
	p[size] = 0;

	m_List.clear();

	InsertLine(0, p );

	SAFE_DELETES(p);

	return gxTrue;
}

size_t CTxt::addLine(Uint8* pData, size_t size, Sint32 line)
{
	if (line < 0)
	{
		//末尾に追加する
		if (m_List.size() == 0)
		{
			//１行もなければ最初に追加
			line = 0;
		}
		else
		{
			//行が存在すれば最終行
			line = (Sint32)m_List.size();
		}
	}
	else
	{
		//指定行
		if (line > m_List.size())
		{
			line = (Sint32)m_List.size();
		}
	}

	if (size == 0 || pData == nullptr)
	{
		//空文字を受け渡された場合
		std::vector<Uint32> str;
		//str.emplace_back(0);
		//m_List.emplace_back(str);
		//return line+1;
		m_List.push_back(str);
		return m_List.size();
	}


	size_t firstLine = line;

	char* p = new char[size+1];
	memcpy(p, pData, size);
	p[size] = 0;

	std::string temp = p;

	SAFE_DELETES(p);

	if (temp.size() >= size)
	{
		temp.push_back('\0');
	}

	std::vector<Uint32> dst  = gxUtil::UTF8toUTF32(temp);

	auto itr = std::next(m_List.begin(), firstLine);
	m_List.insert(itr, dst);

//	std::vector<Uint32> append;
//
//	if (m_List.size() > firstLine)
//	{
//		for (size_t ii = 0; ii < m_List[firstLine].size(); ii++)
//		{
//			append.emplace_back(m_List[firstLine][ii]);
//		}
//	}
//		
//	for (size_t ii = 0; ii < dst.size(); ii++)
//	{
//		switch (dst[ii]) {
//		case '\r':
//		case '\n':
//			continue;
//		}
//		append.emplace_back(dst[ii]);
//	}
//
//	auto itr = std::next(m_List.begin(), firstLine);
//	m_List.insert(itr,append);
//
	
//#if 0
//	m_List.push_back(append);
//#else
//	if (firstLine > m_List.size()) return gxFalse;
//
//	RemoveLine(firstLine);
//	auto itr = std::next(m_List.begin(), firstLine);
//	m_List.insert(itr,append);
//
//#endif

	return m_List.size();
}


gxBool CTxt::LoadBin(std::string fileName)
{
	Clear();

	std::string text, comment;
	size_t uSize = 0;
	Uint8* pData = gxLib::LoadFile(fileName.c_str(), &uSize);

	char buf[1024] = { 0x00 };
	size_t addr = 0x00;
	if (pData)
	{
		Uint8 data = 0;
		char moji = 0x00;
		char mojibuf[3] = { 0x00 };
//		char* pMoji = nullptr;
		for (size_t ii = 0; ii < uSize; ii += 16)
		{
			text = "";
			comment = "";
			text += "	";

			sprintf(buf, "	//%08zX", ii);
			comment += buf;

			for (Sint32 jj = 0; jj < 16; jj++)
			{
				sprintf(buf, "0x%02x,", pData[ii + jj]);
				text += buf;

				mojibuf[0] = '.';
				mojibuf[1] = 0x00;

				comment += mojibuf;
			}

			sprintf(buf, "%s%s", text.c_str(), comment.c_str());
			std::string binstr = buf;
			addLine((Uint8*)binstr.c_str(), binstr.size());

			addr += 16;

			if (addr + 16 >= uSize) break;
		}

		//端数を処理する

		text = "";
		comment = "";
		text += "	";

		sprintf(buf, "	//%08zX", addr);
		comment += buf;

		for (size_t jj = 0; jj < 16; jj++)
		{
			if (jj >= (uSize % 16))
			{
				sprintf(buf, "     ");
			}
			else
			{
				sprintf(buf, "0x%02x,", pData[addr + jj]);
			}

			text += buf;

			mojibuf[0] = moji;
			comment += mojibuf;
		}
		sprintf(buf, "%s%s", text.c_str(), comment.c_str());
		std::string binstr = buf;
		addLine((Uint8*)binstr.c_str(), binstr.size());
	}

	return gxTrue;
}

void CTxt::OutPutText()
{
	size_t uSize = 0;

	SetLF("\n");

	printf("test 3\n");

	for (auto itr = m_List.begin(); itr != m_List.end(); ++itr)
	{
		std::vector<Uint32> u32s = *itr;
		std::string output;

		u32s.push_back(0);

		//if (u32s.size() > 0)
		{
			output += gxUtil::UTF32toUTF8(u32s);
		}

		printf("test 4\n");

		output += m_LF;
		printf( "%s",output.c_str() );
	}
	printf("test 5\n");

}

std::string CTxt::ToString()
{
	std::string alltext;

	for (size_t ii = 0; ii < GetLineNum(); ii++)
	{
		alltext += GetLine(ii);
		alltext += m_LF;
	}

	return alltext;
}

void  CTxt::EditDeleteBefore(Sint32 x, Sint32 y)
{
	std::string line = GetLine(y);
	line = CTxt::DeleteBefore(line, x);
	ReplaceLine(y, line);
}





void CTxt::AppendLine2(char* buf, ...)
{
//	gxChar temp[eTempBufMax];

	va_list app;
	va_start(app, buf);
	vsprintf(_tempbuf_, buf, app);
	va_end(app);

	std::string tmpstr = _tempbuf_;
	AppendLine(tmpstr);
}

void CTxt::AppendLine(std::string text)
{
	InsertLine(-1, text);
}

void CTxt::EditDeleteAfter(Sint32 x, Sint32 y)
{
	std::string line = GetLine(y);
	line = CTxt::DeleteAfter(line, x);
	ReplaceLine(y, line);
}

gxBool CTxt::GetPartnerBracket(Sint32 x, Sint32 y, CTxt::POS *pos , Sint32 dir )
{
	//対応するカッコを見つける

	auto itr = std::next(m_List.begin(), (size_t)y);

	if (itr == m_List.end())
	{
		return gxFalse;
	}

	Uint32 s32=0,e32=0;

	{
		switch (itr[0][x]) {
		case '[':	s32 = '['; e32 = ']';	dir = 1;	break;
		case '{':	s32 = '{'; e32 = '}';	dir = 1;	break;
		case '(':	s32 = '('; e32 = ')';	dir = 1;	break;
		case '<':	s32 = '<'; e32 = '>';	dir = 1;	break;
		case '\"':	s32 = 1;   e32 = '\"';	/*dir = 1;*/	break;
		case '\'':	s32 = 1;   e32 = '\'';	/*dir = 1;*/	break;

		case ']':	s32 = ']'; e32 = '[';	dir = -1;	break;
		case '}':	s32 = '}'; e32 = '{';	dir = -1;	break;
		case ')':	s32 = ')'; e32 = '(';	dir = -1;	break;
		case '>':	s32 = '>'; e32 = '<';	dir = -1;	break;
			break;
		}
	}

	if (s32 == 0) return gxFalse;

	if(dir == 0 ) dir = 1;

	Sint32 cnt = 1;

	if (dir == 1)
	{
		//右方向
		x += 1;
		for (Sint32 yy = y; yy < m_List.size(); yy++)
		{
			auto itr2 = std::next(m_List.begin(), (size_t)yy);

			for (Sint32 xx = x; xx < itr2[0].size(); xx++)
			{
				if (itr2[0][xx] == s32)
				{
					cnt++;
				}
				else if (itr2[0][xx] == e32)
				{
					cnt--;
				}
				if (cnt == 0)
				{
					pos->x = xx;
					pos->y = yy;
					return gxTrue;
				}
			}

			x = 0;
		}
	}
	else if (dir == -1)
	{
		//左方向
		x -= 1;
		gxBool bFirst = gxTrue;
		for (Sint32 yy = y; yy >= 0; yy--)
		{
			auto itr2 = std::next(m_List.begin(), (size_t)yy);

			if(!bFirst) x = (Sint32)(itr2[0].size())-1;

			bFirst = gxFalse;

			for (Sint32 xx = x; xx >= 0; xx--)
			{
				if (itr2[0][xx] == s32)
				{
					cnt++;
				}
				else if (itr2[0][xx] == e32)
				{
					cnt--;
				}
				if (cnt == 0)
				{
					pos->x = xx;
					pos->y = yy;
					return gxTrue;
				}
			}
		}
	}

	return gxFalse;
}

std::string CTxt::GetString(POS n)
{
	auto itr = std::next(m_List.begin(), (size_t)n.y);
	POS m;
	m.x = (Sint32)itr[0].size();
	m.y = n.y;

	return GetStringInRange(n, m);
}

std::string CTxt::GetStringInRange(POS n, POS m)
{

	Sint32 x1 = 0,x2=0;
	std::vector<Uint32> str;

	if (n.y > m.y)
	{
		POS t = n;
		n = m;
		m = t;
	}
	else
	{
		if (n.y == m.y)
		{
			if (n.x > m.x)
			{
				POS t = n;
				n = m;
				m = t;
			}
		}
	}

	std::vector<Uint32> LF32 = gxUtil::UTF8toUTF32(m_LF);

	for (Sint32 yy = n.y; yy <= m.y; yy++)
	{
		auto itr = std::next(m_List.begin(), (size_t)yy);

		if (itr == m_List.end())
		{
			return "";
		}
		x2 = (Sint32)itr[0].size();

		if (yy == n.y)
		{
			x1 = n.x;
			if (n.y == m.y) x2 = m.x;
		}
		else if (yy == m.y)
		{
			x2 = m.x;
		}
		else
		{
			x1 = 0;
		}

		for (Sint32 xx=x1; xx<x2; xx++)
		{
			if (xx >= itr[0].size()) break;	//行の大きさよりも大きい値を指示した場合はそこまで
			str.push_back(itr[0][xx]);
		}

		if (yy < m.y)
		{
			for (Sint32 ii = 0; ii < LF32.size(); ii++)
			{
				str.push_back(LF32[ii]);
			}
		}

	}

	std::string ret = gxUtil::UTF32toUTF8(str);

	return ret;
}

//----------------------------------------------------------------------
//static
//----------------------------------------------------------------------

std::string CTxt::DecodeURL(std::string url)
{
	size_t uSize = 0;
	std::vector<Uint8> buf8;
	std::vector<Uint32> words32 = gxUtil::UTF8toUTF32(url);

	for (Sint32 ii = 0; ii < words32.size(); ii++)
	{
		if (words32[ii] == '%')
		{
			char num[5];
			num[0] = (char)words32[ii + 1];
			num[1] = (char)words32[ii + 2];
			num[2] = 0x00;
			Uint32 u32 = gxUtil::atox(num);
			buf8.push_back(u32&0xff);
			ii += 2;
			continue;
		}
		buf8.push_back(words32[ii] & 0xff);
	}

	buf8.push_back(0);
	std::string ret = (char*)(&buf8[0]);

	return ret;
}


Sint32 CTxt::FindFront(std::string src, std::string find, Sint32 x)
{
	std::vector<Uint32> src32  = gxUtil::UTF8toUTF32(src);
	std::vector<Uint32> find32 = gxUtil::UTF8toUTF32(find);

	return CTxt::findString32(src32, find32, 1 , x );
}


Sint32 CTxt::FindBack(std::string src, std::string find, Sint32 x)
{
	std::vector<Uint32> src32 = gxUtil::UTF8toUTF32(src);
	std::vector<Uint32> find32 = gxUtil::UTF8toUTF32(find);

	return CTxt::findString32(src32, find32, -1 , x);
}

std::string CTxt::DeleteAfter(std::string src, Sint32 x)
{
	std::vector<Uint32> src32 = gxUtil::UTF8toUTF32(src);
	std::vector<Uint32> dst32;

	gxBool bModeDelete = gxTrue;
	for (size_t ii = 0; ii <src32.size(); ii++)
	{
		if (ii >= x)
		{
			if (src[ii] == '\r' || src[ii] == '\n') bModeDelete = gxFalse;
			if( bModeDelete) continue;
		}

		dst32.push_back(src32[ii]);
	}

	//ここでターミネータを入れておかないとUTF32toUTF8で終了を判定できない
//テスト	dst32.push_back(0x00);

	//printf("DeleteAfter\r\n");
	std::string ret8 = gxUtil::UTF32toUTF8(dst32);

	//printf("DeleteAfter2=%s\r\n",ret8.c_str());
	return ret8;
}

std::string CTxt::DeleteBefore(std::string src, Sint32 x)
{
	std::vector<Uint32> src32 = gxUtil::UTF8toUTF32(src);
	std::vector<Uint32> dst32;

	size_t start = 0;
	for (size_t ii = x; ii > 0; ii--)
	{
		if (ii > 0)
		{
			if (src[ii-1] == '\r' || src[ii-1] == '\n') start = ii;
		}
	}

	for (size_t ii=0; ii<start; ii++)
	{
		dst32.push_back(src32[ii]);
	}

	gxBool bModeDelete = gxTrue;
	for (size_t ii=x; ii<src32.size(); ii++)
	{
		dst32.push_back(src32[ii]);
	}

	std::string ret32 = gxUtil::UTF32toUTF8(dst32);

	return ret32;
}


std::string CTxt::InsertText(std::string src, Sint32 x, std::string addText)
{
	//挿入する

	std::vector<Uint32> src32 = gxUtil::UTF8toUTF32(src);
	std::vector<Uint32> add32 = gxUtil::UTF8toUTF32(addText);
	std::vector<Uint32> dst32;

	for (size_t ii = 0; ii < x; ii++)
	{
		dst32.push_back(src32[ii]);
	}

	for (size_t ii = 0; ii < add32.size(); ii++)
	{
		dst32.push_back(add32[ii]);
	}

	for (size_t ii = x; ii < src32.size(); ii++)
	{
		dst32.push_back(src32[ii]);
	}

	dst32.push_back(0);
	std::string ret8 = gxUtil::UTF32toUTF8(dst32);

	return ret8;
}


std::string CTxt::DeleteText(std::string src, Sint32 x, Sint32 num)
{
	std::vector<Uint32> src32 = gxUtil::UTF8toUTF32(src);
	std::vector<Uint32> dst32;

	for (size_t ii = 0; ii < src32.size(); ii++)
	{
		if (ii >= x && ii < x+num )
		{
			continue;
		}

		dst32.push_back(src32[ii]);
	}

	std::string ret32;

	if (dst32.size() == 0)
	{
		return ret32;
	}
//テスト	dst32.push_back(0);
	ret32 = gxUtil::UTF32toUTF8(dst32);

	return ret32;
}

std::string CTxt::BackSpace(std::string src, Sint32 x, Sint32 num)
{
	std::vector<Uint32> src32 = gxUtil::UTF8toUTF32(src);
	std::vector<Uint32> dst32;

	for (size_t ii = 0; ii < src32.size(); ii++)
	{
		if (ii >= (x-num) && ii<x)
		{
			continue;
		}
		dst32.push_back(src32[ii]);
	}

	std::string ret32 = gxUtil::UTF32toUTF8(dst32);

	return ret32;
}


std::vector<std::string> CTxt::Separate(std::string src, std::string svalue , gxBool bSearchInsideDoubleQuatation)
{

	std::vector<std::string> ret;

	std::vector<Uint32> src32 = gxUtil::UTF8toUTF32(src);
	std::vector<Uint32> find32 = gxUtil::UTF8toUTF32(svalue);

	size_t x = 0;;
	Sint32 s1=0, s2=0;
	Sint32 pos;

	auto dqArea = src;
	int dq = 0;
	if( !bSearchInsideDoubleQuatation)
	{
		//DQ内は検索対象としない場合
		for (int ii = 0; ii < src.size(); ii++)
		{
			dqArea[ii] = dq;
			if (src[ii] == '\"')
			{
				dq = 1-dq;
			}
		}
	}

	do {
		//先頭から文字列を捜す
		pos = CTxt::findString32(src32, find32, 1, x);

		if (!bSearchInsideDoubleQuatation)
		{
			if (dqArea[pos] == 1) pos = -1;
		}

		if (pos != -1 )
		{
			//文字が見つかった
			s2 = pos;
			if (s1 == s2)
			{
				//空データの場合
				ret.push_back("");
				s1 += (Sint32)svalue.size();
				x = s1;
				continue;
			}
			else
			{
				//文字列が見つかった
				auto str = getPartText32toUTF8(src32, s1, s2);
				//str += '\0';
				ret.push_back(str);
				s1 = pos + (Sint32)svalue.size();
				x = s1;
				continue;
			}

		}
		else
		{
			//みつからなかったので最終データか確認
			if (s1 < src32.size())
			{
				//最後尾未満なら何か文字があったと判断する
				s2 = (Sint32)src32.size();
				auto str = getPartText32toUTF8(src32, s1, s2);
				if (str != "")
				{
					//何か入っていた
					//str += '\0';
					ret.push_back(str);
				}
			}
			break;
		}
		x = pos + 1;

	} while ( pos >= 0);

	return ret;
}

std::string CTxt::GetStringinText(std::string text, size_t x1, size_t x2)
{
	auto u32Str = gxUtil::UTF8toUTF32(text);
	std::vector<Uint32> str32;

	for (size_t ii = x1; ii < x2; ii++)
	{
		str32.push_back(u32Str[ii]);
	}

//テスト	str32.push_back(0);
	auto u8Str = gxUtil::UTF32toUTF8(str32);

	return u8Str;
}


std::string CTxt::Sprintf( char* pBuf, ... )
{
//	gxChar buf[eTempBufMax];

	va_list app;
	va_start(app, pBuf);
	vsprintf(_tempbuf_, pBuf, app);
	va_end(app);

	std::string tmpstr = _tempbuf_;

	return tmpstr;
}


Sint32 CTxt::findString32(std::vector<Uint32> src32, std::vector<Uint32> findStr32, Sint32 dir, size_t x )
{
	Sint32 max = (Sint32)src32.size();
	Sint32 num = (Sint32)findStr32.size();
	gxBool bFound = false;
	Sint32 loopnum = 0;
	Sint32 loopcnt = 0;

	max -= (Sint32)x;

	if (num > max) return -1;

	max -= num;

	if (max < 0) return -1;

	loopnum = max;

	if (loopnum == 0)
	{
		loopnum = 1;
	}

	Sint32 ii = 0;

	if (dir > 0)
	{
		ii = (Sint32)x;
	}
	else
	{
		ii = (Sint32)src32.size() - 1;
		if (x > num)
		{
			ii -= (Sint32)x;
		}
		else
		{
			ii -= num;
		}
	}

	do {
		bFound = true;
		for (Sint32 jj = 0; jj < num; jj++)
		{
			if ((ii + jj) >= src32.size())
			{
				bFound = false;
				break;
			}

			if (src32[ii + jj] != findStr32[jj])
			{
				bFound = false;
				break;
			}
		}
		if (bFound)
		{
			return ii;
		}
		ii += dir;
		loopcnt++;

	} while (loopcnt <= loopnum);

	return -1;
}


std::string CTxt::getPartText32toUTF8(std::vector<Uint32> src32, size_t s1, size_t s2)
{
	std::vector<Uint32> dst32;

	if (s1 >= src32.size()) return "";
	//if ((s2+1) >= src32.size()) return "";
	if (s1 == s2)
	{
		dst32.push_back(src32[s1]);
	}
	else
	{
		for (size_t x = s1; x < s2; x++)
		{
			if (x >= src32.size()) return "";
			dst32.push_back(src32[x]);
		}
	}
	//テスト	dst32.push_back(0);
	std::string ret = gxUtil::UTF32toUTF8(dst32);

	return ret;
}

bool CTxt::StrMatch(const char* str, const char* ptn)
{
	switch (*ptn)
	{
	case '\0':
		return ('\0' == *str)? true : false;
	case '*':
		return (StrMatch(ptn + 1, str) || (('\0' != *str) && StrMatch(ptn, str + 1))) ? true : false;
	case '?':
		return ('\0' != *str) && StrMatch(ptn + 1, str + 1)? true : false;
	default:
		return ((unsigned char)*ptn == (unsigned char)*str) && StrMatch(ptn + 1, str + 1)? true : false;
	}
}//strmatch


uint64_t CTxt::Atox(std::string txt)
{
	std::vector<Uint32> str32 = gxUtil::UTF8toUTF32(txt);

	Uint32* p = &str32[0];

	if (str32[0] == L'0' && str32[1] == L'x')
	{
		str32.erase(str32.begin());
		str32.erase(str32.begin());
	}

	uint64_t n = 0,ret = 0;
	uint64_t keta = 0;
	for (int ii= (int)str32.size()-1; ii > 0; ii--)
	{
		switch (str32[ii]) {
		case L'0':	case L'０':	n = 0; break;
		case L'1':	case L'１':	n = 1; break;
		case L'2':	case L'２':	n = 2; break;
		case L'3':	case L'３':	n = 3; break;
		case L'4':	case L'４':	n = 4; break;
		case L'5':	case L'５':	n = 5; break;
		case L'6':	case L'６':	n = 6; break;
		case L'7':	case L'７':	n = 7; break;
		case L'8':	case L'８':	n = 8; break;
		case L'9':	case L'９':	n = 9; break;
		case L'A':	case L'Ａ':	case L'a':	case L'ａ':	n = 10; break;
		case L'B':	case L'Ｂ':	case L'b':	case L'ｂ':	n = 11; break;
		case L'C':	case L'Ｃ':	case L'c':	case L'ｃ':	n = 12; break;
		case L'D':	case L'Ｄ':	case L'd':	case L'ｄ':	n = 13; break;
		case L'E':	case L'Ｅ':	case L'e':	case L'ｅ':	n = 14; break;
		case L'F':	case L'Ｆ':	case L'f':	case L'ｆ':	n = 15; break;
		default:
			continue;
		}
		ret |= (n << (4 * keta));
		keta++;
	}

	return ret;
}

int64_t CTxt::Atoi(std::string txt)
{
	std::vector<Uint32> str32 = gxUtil::UTF8toUTF32(txt);

	Uint32* p = &str32[0];

	uint64_t n = 0, ret = 0;

	uint64_t keta = 1;
	for (int ii = (int)str32.size() - 1; ii > 0; ii--)
	{
		switch (str32[ii]) {
		case L'0':	case L'０':	n = 0; break;
		case L'1':	case L'１':	n = 1; break;
		case L'2':	case L'２':	n = 2; break;
		case L'3':	case L'３':	n = 3; break;
		case L'4':	case L'４':	n = 4; break;
		case L'5':	case L'５':	n = 5; break;
		case L'6':	case L'６':	n = 6; break;
		case L'7':	case L'７':	n = 7; break;
		case L'8':	case L'８':	n = 8; break;
		case L'9':	case L'９':	n = 9; break;
		case L'.':	case L'．':
			continue;
		default:
			continue;
		}
		ret += (n* keta);
		keta *= 10;
	}

	return ret;
}

float CTxt::Atof(std::string txt)
{
	std::vector<Uint32> str32 = gxUtil::UTF8toUTF32(txt);

	Uint32* p = &str32[0];

	std::string str;
	for (int ii = 0; ii < str32.size(); ii++)
	{
		switch (str32[ii]) {
		case L'0':	case L'０':	str += '0'; break;
		case L'1':	case L'１':	str += '1'; break;
		case L'2':	case L'２':	str += '2'; break;
		case L'3':	case L'３':	str += '3'; break;
		case L'4':	case L'４':	str += '4'; break;
		case L'5':	case L'５':	str += '5'; break;
		case L'6':	case L'６':	str += '6'; break;
		case L'7':	case L'７':	str += '7'; break;
		case L'8':	case L'８':	str += '8'; break;
		case L'9':	case L'９':	str += '9'; break;
		case L'.':	case L'．':	str += '.'; break;
		default:
			continue;
		}
	}

	float ret = atof(str.c_str());

	return ret;

}

bool CTxt::IsNumber(std::string txt)
{
	bool hex = false;
	std::vector<Uint32> str32 = gxUtil::UTF8toUTF32(txt);
	if (str32[0] == L'0' && str32[1] == L'x')
	{
		str32.erase(str32.begin());
		str32.erase(str32.begin());
		hex = true;
	}
	if (str32[str32.size() - 1] == L'f' || str32[str32.size() - 1] == L'F')
	{
		str32.erase(str32.end()-1);
	}

	for (int ii = (int)str32.size() - 1; ii > 0; ii--)
	{
		switch (str32[ii]) {
		case L'0':	case L'０': break;
		case L'1':	case L'１': break;
		case L'2':	case L'２': break;
		case L'3':	case L'３': break;
		case L'4':	case L'４': break;
		case L'5':	case L'５': break;
		case L'6':	case L'６': break;
		case L'7':	case L'７': break;
		case L'8':	case L'８': break;
		case L'9':	case L'９': break;
		case L'.':	case L'．': break;
		default:
			if (hex)
			{
				switch (str32[ii]) {
				case L'A':	case L'Ａ':	case L'a':	case L'ａ':	break;
				case L'B':	case L'Ｂ':	case L'b':	case L'ｂ':	break;
				case L'C':	case L'Ｃ':	case L'c':	case L'ｃ':	break;
				case L'D':	case L'Ｄ':	case L'd':	case L'ｄ':	break;
				case L'E':	case L'Ｅ':	case L'e':	case L'ｅ':	break;
				case L'F':	case L'Ｆ':	case L'f':	case L'ｆ':	break;
				default:
					return false;
				}
			}
			else
			{
				return false;
			}
		}
	}
	return true;
}


std::string CTxt::Replace( std::string s, std::string src, std::string dst )
{
	size_t pos;
	while ((pos = s.find(src)) != std::string::npos) {
		
		s.replace(pos, src.size(), dst);
	}

	return s;
}



#include <regex>
#include <string>
#include <iostream>

std::string escape_regex(const std::string& str)
{
	std::string result;
	for (char c : str) {
		if (std::strchr(".^$|()[]{}*+?\\", c)) {
			result += '\\';
		}
		result += c;
	}
	return result;
}


bool CTxt::IsMatch(const std::string& str, const std::string& pattern)
{
	// ワイルドカードを正規表現に変換
	std::string regex_pattern = "^";
	for (char c : pattern) {
		switch (c) {
		case '*': regex_pattern += ".*"; break;
		case '?': regex_pattern += "."; break;
		default: regex_pattern += escape_regex(std::string(1, c));
		}
	}
	regex_pattern += "$";

	// 正規表現のマッチング
	std::regex r(regex_pattern);
	return std::regex_match(str, r);

}
