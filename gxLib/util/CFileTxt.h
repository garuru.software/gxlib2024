//TXT変換
#ifndef _CTxt_H_
#define _CTxt_H_

#include <list>

#define TEXT_LENGTH_MAX (1024*1024)

class CTxt
{
public:
	enum {
		eTempBufMax = TEXT_LENGTH_MAX,
	};

	enum {
		DIR_AUTO = 0,
		DIR_RIGHT = 1,
		DIR_LEFT = -1
	};

	CTxt( std::string str = "" )
	{
		init(str);
	}

	~CTxt()
	{
	}

	struct POS
	{
		Sint32 x=0, y=0;
	};

	void operator = (std::string text)
	{
		init(text);
	}

	void operator += ( std::string text )
	{
		size_t last = m_List.size();
		if (last > 0) last--;
		std::string str = GetLine(last);
		str += text;
		ReplaceLine(last, str);
	}

	void operator += (CTxt &txt)
	{
		for (size_t y = 0; y < txt.GetLineNum(); y++)
		{
			auto line = txt.GetLine(y);
			AppendLine(line);
		}
	}

	static uint64_t Atox(std::string txt);
	static int64_t Atoi(std::string txt);
	static float Atof(std::string txt);
	static bool IsNumber(std::string txt);
	static std::string DecodeURL(std::string url);
	static Sint32 FindFront(std::string src, std::string find, Sint32 x = 0);
	static Sint32 FindBack(std::string src, std::string find, Sint32 x = 0);
	static std::string DeleteAfter(std::string src, Sint32 x = 0);
	static std::string DeleteBefore(std::string src, Sint32 x = 0);
	static std::string InsertText(std::string src, Sint32 x, std::string addtext);
	static std::string DeleteText(std::string src, Sint32 x = 0, Sint32 num = 1);
	static std::string BackSpace(std::string src, Sint32 x = 0, Sint32 cnt = 1);
	static std::vector<std::string> Separate(std::string line, std::string svalue , gxBool bSearchInsideDoubleQuatation = gxTrue );
	static std::string GetStringinText(std::string txt, size_t x, size_t y);
	static std::string Sprintf(char* pBuf, ...);
	static Sint32 findString32(std::vector<Uint32> itr, std::vector<Uint32> u32findStr, Sint32 dir = 1, size_t x = 0);
	static std::string getPartText32toUTF8(std::vector<Uint32> src32, size_t s1, size_t s2);
	static bool StrMatch(const char* str, const char* ptn);
	static std::string Replace( std::string s, std::string src, std::string dst );
	static bool IsMatch(const std::string& str, const std::string& pattern);

	gxBool Load(std::string fileName);
	gxBool LoadSJIS(std::string fileName);
	gxBool LoadBin(std::string fileName);
	gxBool Read(Uint8* pdata, size_t uSize);
	gxBool Save(std::string fileName);
	gxBool SaveSJIS(std::string fileName);


	void EditDeleteAfter(Sint32 x, Sint32 y);
	void EditDeleteBefore(Sint32 x, Sint32 y);

	gxBool GetPartnerBracket(Sint32 x, Sint32 y, CTxt::POS* pos ,Sint32 dir = 0);
	//-----------------------------------

	std::string GetLine(size_t line);

	size_t GetLineNum()
	{
		return m_List.size();
	}

	void Clear()
	{
		m_List.clear();
	}

	void SetLF(std::string lf )
	{
		m_LF = lf;
	}

	void OutPutText();

	void SetSearchInsideDoubleQuatation( bool bSearchInside = true )
	{
		m_bSearchInsideDoubleQuatation = bSearchInside;
	}

	std::string ToString();
	gxBool Search(std::string searchWord, POS *pPos= nullptr , size_t sx=0 , size_t sy = 0);

	std::vector<POS> GetFindList(std::string searchWord, std::function<void(POS)>func = nullptr );
	void GrepReplace(std::string src, std::string dst, size_t x=0, size_t y=0);
	void EraseBlanks();

	void AppendLine2(char* buf, ...);
	void AppendLine(std::string text = "");
	void InsertLine(Sint32 lineNumber, std::string text);
	void RemoveLine(size_t lineNumber);
	void ReplaceLine(size_t lineNumber, std::string str);

	//---------------------------------

	void printf(char *buf, ...);
	void Add(std::string str);
	void EditInsert(size_t x, size_t y, std::string str);
	void EditDelete(size_t x, size_t y, size_t num=1);
	void EditReplace(size_t x, size_t y, std::string src, std::string dst);
	void EditGrepReplace(size_t x, size_t y, std::string src, std::string dst);

	std::string GetString(POS n);
	std::string GetStringInRange(POS n, POS m);

	std::string GetFileName(std::string path = "")
	{
		if (path == "")
		{
			return m_FileName.FileName;
		}

		return m_FileName.GetRelativeFileName(path);
	}

private:

	void init(std::string str)
	{
		analysis((Uint8*)str.c_str(), str.size());
	}

//	std::list<std::vector<Uint32>> m_List;
	std::vector<std::vector<Uint32>> m_List;
	std::vector<std::vector<Uint32>> dqList;

	gxBool analysis(Uint8* pData, size_t size);

	void setLine32(size_t y, std::vector<Uint32> line);
	std::vector<Uint32> getLine32(size_t y);
	size_t addLine(Uint8* pData, size_t size, Sint32 line=-1);

	gxUtil::FileNames m_FileName;

	std::string m_LF = LF;

	bool m_bSearchInsideDoubleQuatation = true;
};
#endif
