﻿//-----------------------------------------------------
//
// CFileWave.cpp
//
//-----------------------------------------------------

#include <gxLib.h>
#include "CFileWave.h"

#define WAVE_FORMAT_EXTENSIBLE	0xFFFE
#define WAVE_FORMAT_PCM     1

CFileWave::CFileWave()
{
	m_pWaveImage = NULL;	//WAVEデータイメージ
	m_uFormat    = 0;		//フォーマット( -1:未知のWAVE / 0:ＰＣＭ / 1:マルチチャンネル ウェーブ フォーマット / 2:圧縮WAVE )
	m_DataSize   = 0;		//WAVEデータイメージのサイズ
	m_pPCMImage	 = nullptr;	//PCMデータへのポインタ
}


CFileWave::~CFileWave()
{
	SAFE_DELETES( m_pWaveImage );
}

Uint8* Ogg2Wav(Uint8* pOggFile, size_t uOggSize, Uint32* uWavSize);

gxBool CFileWave::Read( Uint8* pMemory , size_t uSize )
{
	//----------------------------------------------------
	//とりあえずWAVにして読み込める形にして読む
	//----------------------------------------------------

//	Uint8 *pFileImage = NULL;

	SAFE_DELETE( m_pWaveImage );

	if( pMemory[0] == 'O' && pMemory[1] == 'g' && pMemory[2] == 'g' )
	{
		//oggやんけ！

		//変換することにする

		Uint32 uWavSize = 0;
		Uint8 *pWavFile;

		pWavFile = Ogg2Wav( pMemory , uSize, &uWavSize);

	m_pWaveImage = new Uint8[uWavSize];
	gxUtil::MemCpy( m_pWaveImage , pWavFile , uWavSize );
	m_DataSize = Uint32(uSize);

		getWaveData( m_pWaveImage , &m_pPCMImage , &m_stWaveInfo );

		SAFE_DELETES( pWavFile );

	}
	else if( pMemory[0] == 'R' && pMemory[1] == 'I' && pMemory[2] == 'F' && pMemory[3] == 'F' )
	{
		//WAVなら展開してPCMデータを取り出しますよ

	m_pWaveImage = new Uint8[uSize];
	gxUtil::MemCpy( m_pWaveImage , pMemory , uSize );
	m_DataSize = Uint32(uSize);

		getWaveData( m_pWaveImage , &m_pPCMImage , &m_stWaveInfo );
	}
	else
	{
		//もうしらないっ
		return NULL;
	}


	return gxTrue;

}


gxBool CFileWave::Load(gxChar* pFileName)
{

	Uint8 *pFileImage = NULL;
	size_t uLength;

	pFileImage = gxLib::LoadFile(pFileName, &uLength);

	if (pFileImage == NULL) return gxFalse;

	return Read(pFileImage , uLength );
}

Uint8* CFileWave::GetPCMImage()
{
	return &m_pPCMImage[0];
}


Uint32 CFileWave::getWaveData( Uint8 *pFileImage, Uint8 **ppPCMImage, StWaveInfo *pWaveInfo )
{
	//WAVEデータの取得

	Uint8 *pHeader	= NULL;
	Uint32 WaveType	= 0;
	Uint32 bResult	= gxTrue;
	gxChar Chunk[5]	= "";

	//--------------------------------------------------------
	// "RIFF"の文字列
	//--------------------------------------------------------
	gxUtil::MemCpy( Chunk, pFileImage, 4 );
	pFileImage += 4;
	if ( strcmp( "RIFF", Chunk ) )
	{
		//"RIFF"じゃない
		bResult = gxFalse;
		goto EXIT_DATALOAD;
	}

	//--------------------------------------------------------
	// RIFFデータサイズ
	//--------------------------------------------------------
	pFileImage += 4;

	//--------------------------------------------------------
	// "WAVE"の文字列
	//--------------------------------------------------------
	gxUtil::MemCpy( Chunk, pFileImage, 4 );
	pFileImage += 4;
	if ( strcmp( "WAVE", Chunk ) )
	{
		//"WAVE"じゃない
		bResult = gxFalse;
		goto EXIT_DATALOAD;
	}

	//========================================================
	// チャンク情報取得
	//========================================================
	while ( gxTrue )
	{
		Uint32 size = 0;

		gxUtil::MemCpy( Chunk, pFileImage, 4 );
		pFileImage += 4;

		// フォーマットのチャンク

		if ( strcmp( "fmt ", Chunk ) == 0 )
		{
			size += getChunk( pFileImage, &pHeader, NULL );
			m_uFormat = (Uint16)(*pHeader);
			pFileImage += size;
		}
		else if ( strcmp( "data", Chunk ) == 0 )
		{
			// データチャンク

			size += getChunk( pFileImage, NULL, &m_DataSize  );
			break;
		}
		else
		{
			// ？？？
			size = *( (Uint32 *)pFileImage );
			pFileImage += 4 + size;
		}
	}

	// WAVEデータの中身確認

	WaveType = makeWaveInfo( pHeader, pWaveInfo );

	if ( WaveType < 0 )
	{
		//知らない形式だった
		bResult = gxFalse;
		goto EXIT_DATALOAD;
	}

	//WAVEデータのみを取得する

//	SAFE_DELETES( *ppPCMImage );
//	(*ppPCMImage) = new Uint8 [ m_DataSize ];
//	gxUtil::MemCpy( (*ppPCMImage), pFileImage, m_DataSize );

	*ppPCMImage = pFileImage;


EXIT_DATALOAD:

	SAFE_DELETES( pHeader );

	if( m_DataSize == 0 )
	{
		bResult = gxFalse;
	}

	return bResult;
}


Uint32 CFileWave::getChunk( Uint8 *pSrcData, Uint8 **ppDstData, Uint32 *pSize )
{
	//チャンクデータ取得

	Uint32 DataSize = 0;

	// チャンクサイズ取得
	DataSize = *( (Uint32 *)pSrcData );

	pSrcData += 4;

	if( ppDstData )
	{
		// データ受信用バッファ生成
		SAFE_DELETES( *ppDstData );

		(*ppDstData) = new Uint8 [ DataSize ];

		// チャンクデータ取得
		gxUtil::MemCpy( (*ppDstData), pSrcData, DataSize );
	}

	if ( pSize != NULL) *pSize = DataSize;

	return DataSize + 4;
}

Uint32 CFileWave::makeWaveInfo( Uint8 *pHeader, StWaveInfo *pWaveInfo )
{
	//Waveデータのヘッダーを解析します

	Uint32 result = -1;

	memset( pWaveInfo, 0x00 ,sizeof(StWaveInfo) );

	//=================================================
	// フォーマットごとに処理わけ
	//=================================================

	switch ( m_uFormat )
	{
	//-------------------------------------------------
	// PCM
	//-------------------------------------------------
	case WAVE_FORMAT_PCM:
		pWaveInfo->Format.wFormatTag		= *((Uint16 *)pHeader);	pHeader += 2;
		pWaveInfo->Format.nChannels			= *((Uint16 *)pHeader);	pHeader += 2;
		pWaveInfo->Format.nSamplesPerSec	= *((Uint32 *)pHeader);	pHeader += 4;
		pWaveInfo->Format.nAvgBytesPerSec	= *((Uint32 *)pHeader);	pHeader += 4;
		pWaveInfo->Format.nBlockAlign		= *((Uint16 *)pHeader);	pHeader += 2;
		pWaveInfo->Format.wBitsPerSample	= *((Uint16 *)pHeader);
		pWaveInfo->Format.cbSize			= 0;
		result = 0;
		break;

	//-------------------------------------------------
	// マルチチャンネル ウェーブ フォーマット
	//-------------------------------------------------
	case WAVE_FORMAT_EXTENSIBLE:
		*pWaveInfo = *((StWaveInfo *)pHeader);
		result = 1;
		break;

	// 圧縮WAVE
	default:
		pWaveInfo->Format.wFormatTag		= *((Uint16 *)pHeader);	pHeader += 2;
		pWaveInfo->Format.nChannels			= *((Uint16 *)pHeader);	pHeader += 2;
		pWaveInfo->Format.nSamplesPerSec	= *((Uint32 *)pHeader);	pHeader += 4;
		pWaveInfo->Format.nAvgBytesPerSec	= *((Uint32 *)pHeader);	pHeader += 4;
		pWaveInfo->Format.nBlockAlign		= *((Uint16 *)pHeader);	pHeader += 2;
		pWaveInfo->Format.wBitsPerSample	= *((Uint16 *)pHeader);	pHeader += 2;
		pWaveInfo->Format.cbSize			= *((Uint16 *)pHeader);
		result = 2;
	}

	return result;
}


Uint32 CFileWave::makeWaveHeader( Uint8 *pHeader, StWaveInfo *pWaveInfo )
{
	//Info情報からWaveデータのヘッダーを作成します

	//
	makeWaveInfo( pHeader , &m_stWaveInfo );

	return 0;
}

