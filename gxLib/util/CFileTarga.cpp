﻿//-----------------------------------------------------------------------------------
//targaファイル１枚を管理する
//-----------------------------------------------------------------------------------
//---------------------------------------------------------------------------
//型宣言
//---------------------------------------------------------------------------

#include <gxLib.h>
#include "CFileTarga.h"
#include "CFilePng.h"

#pragma pack(push,1)
static const gxChar tgaHutter[] = {
//	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x54,0x52,0x55,0x45,0x56,0x49,0x53,0x49,0x4f,0x4e,0x2d,0x58,0x46,0x49,0x4c,0x45,0x2e,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,'C' ,'r' ,'e' ,'a' ,'t' ,'e' ,'d' ,'B' ,'y' ,'R' ,'a' ,'g' ,'i' ,'.' ,'T' ,'G' ,'A' ,' ' ,
};

CFileTarga::CFileTarga()
{
	m_Filename[0] = 0x00;
	m_pFileImage = NULL;		//ファイルイメージ
	m_TexelImage = NULL;		//テクスチャデータの開始位置
	m_FileSize 	= 0;			//ファイルサイズ
	m_BgColor 	= 0x00000000;	//クリアする背景色
	m_Pallet 	= NULL;			//パレットデータへのポインタ
	m_pTgaInfo 	= NULL;			//ヘッダー情報へのポインタ

}


CFileTarga::~CFileTarga()
{
	releaseImage();
}


gxBool CFileTarga::Create(int width,int height,int colbit,Uint8 *pal)
{
	//int palsize = 0;
	int palletNum=0;
	int colorDepth = 32;
	releaseImage();

	if(colbit == 8 )
	{
		palletNum = 256;
//		palsize = palletNum*4;
		m_FileSize = width*height*(colbit>>3) + 256*4;
		colorDepth = 32;// 24;
	}
	else
	{
		m_FileSize = width*height*(colbit/8);
	}

	if( m_pFileImage ) SAFE_DELETES(m_pFileImage);

	m_FileSize += sizeof(stTgaInfo) + sizeof(tgaHutter);

	m_pFileImage = new Uint8[m_FileSize];
	memset((void*)m_pFileImage,m_BgColor,m_FileSize);	//背景色でクリア
	m_pTgaInfo   = (stTgaInfo*)m_pFileImage;

	//-----------------------------------------------------------
	//ヘッダ情報の構築
	//-----------------------------------------------------------
	m_pTgaInfo->FieldLength   = 0;					/* palletNum*4 ヘッダ後のピクセルデータまでのバイト数? */
	m_pTgaInfo->ColorMapType  = palletNum? 1 : 0;	/* フォーマットの種類. 0,1,2,3,9,10,11 */
	m_pTgaInfo->ImageType     = palletNum? 1 : 2;	/* フォーマットの種類. 0,1,2,3,9,10,11 */

	m_pTgaInfo->ColorMapIndex = 0;				/* 色パレットの開始番号 */
	m_pTgaInfo->clutNum       = palletNum;		/* 色パレットの個数 */
	m_pTgaInfo->ColorMapSize  = colorDepth;		/* 色パレットのビット数 15,16,24,32 */

	m_pTgaInfo->x0 = 0;					/* 始点x */
	m_pTgaInfo->y0 = 0;					/* 始点y 画面左下が原点(0,0)なので注意！ */
	m_pTgaInfo->w = width;				/* 横幅 */
	m_pTgaInfo->h = height;				/* 縦幅 */
	m_pTgaInfo->bpp = colbit;			/* ピクセルのビット数  8,16,24,32 */
	m_pTgaInfo->mode = 8;				//不明/* bit 3..0(4bits) : 属性. 0:αなし. 1:未定義のため無視可能. 2:未定義だが保存すべき */

	if( m_pTgaInfo->ColorMapType )
	{
		//パレット付の場合
		m_Pallet     = (Uint32*)&m_pFileImage[sizeof(stTgaInfo)];
		m_TexelImage = (Uint8*)&m_pFileImage[ sizeof(stTgaInfo)+m_pTgaInfo->ColorMapSize/8 * m_pTgaInfo->clutNum ];//m_pTgaInfo->ColorMapType];

		if (pal == NULL)
		{
			//ダミーのパレットを作成する
			Uint32* pPallet = (Uint32*)&m_Pallet[0];
			for (Sint32 ii = 0; ii < 256; ii++)
			{
				pPallet[ii] = 0xff000000;
			}
		}
		else
		{
			gxUtil::MemCpy((Uint8*)m_Pallet, pal, m_pTgaInfo->ColorMapSize / 8 * m_pTgaInfo->clutNum);
		}
	}
	else
	{
		//パレット無の場合
		m_TexelImage = &m_pFileImage[sizeof(stTgaInfo)];
		m_Pallet 	= NULL;
	}

	//-----------------------------------------------------------
	//フッタ情報の追加
	//-----------------------------------------------------------
	gxUtil::MemCpy(&m_pFileImage[m_FileSize-sizeof(tgaHutter)/*HUTTER_SIZE*/],(void*)tgaHutter,sizeof(tgaHutter));

	return true;
}

gxBool CFileTarga::SaveFile( gxChar *filename)
{
	//---------------------------------------
	//ファイルを保存する
	//---------------------------------------

	if( !gxLib::SaveFile( filename , m_pFileImage , m_FileSize ) )
	{
		return gxFalse;
	}


	return true;
}

gxBool CFileTarga::LoadFile( gxChar *filename , Uint32 col )
{
	gxChar Buf[FILENAMEBUF_LENGTH];
	sprintf(Buf,"%s",filename);
	size_t len = strlen(Buf);

	gxUtil::StrUpper( Buf );

	for(size_t ii=len;ii>0;ii--)
	{
		if( filename[ii] == '.' )
		{
			if( strcmp( (gxChar*)&Buf[ii],".BMP" ) == 0 )
			{
				return loadBitmap( (gxChar*)filename , col );
			}
			else if( strcmp( (gxChar*)&Buf[ii],".PNG" ) == 0 )
			{
				return loadPNG( (gxChar*)filename , col );
			}
			else
			{
				return LoadTarga( filename , col );
			}
		}
	}

	return gxFalse;	
}


gxBool CFileTarga::LoadTarga( gxChar *filename , Uint32 col)
{
	//---------------------------------------
	//ファイルを読み込む
	//---------------------------------------

	Uint8* pBuffer=NULL;

	pBuffer = gxLib::LoadFile( filename ,  &m_FileSize );

	if( pBuffer == NULL  )
	{
		return gxFalse;
	}

	ReadFile( pBuffer , m_FileSize , col );

	SAFE_DELETES( pBuffer );

	return true;

}


gxBool CFileTarga::ReadFile( Uint8 *pMem,size_t filesz,Uint32 col)
{
	//---------------------------------------------
	//メモリからテクスチャデータを構築する
	//---------------------------------------------

	releaseImage();

	if (filesz == 0 || pMem == NULL) return gxFalse;

	if(pMem[0] == 'B' && pMem[1] == 'M' )//&& pMem[2] == '6')
	{
		return convertBMPtoTGA((Uint8*)pMem , filesz , col);
	}
	if(pMem[1] == 'P' && pMem[2] == 'N' && pMem[3] == 'G')
	{
		return convertPNGtoTGA((Uint8*)pMem , filesz , col);
	}

	m_FileSize = filesz;
	m_pFileImage = new Uint8[filesz];
	gxUtil::MemCpy(m_pFileImage,pMem,filesz);

	return ApplyTgaImage( m_pFileImage , m_FileSize );
}


gxBool CFileTarga::ApplyTgaImage( Uint8 *pFileImage , size_t filesz)
{
	//確保済みのメモリからTGAイメージを確立する

	m_pFileImage = pFileImage;
	m_FileSize = filesz;

	m_pTgaInfo 	= (stTgaInfo*)m_pFileImage;	//ヘッダー情報へのポインタ

	if( m_pTgaInfo->ImageType >= 0x08 )
	{
		//RLE圧縮タイプ
		Sint32 sRLEunPack = 0;	//RLE圧縮展開ルーチンが必要
		RLEunpack( NULL/*m_TexelImage*/ , 0 );
	}

	if( m_pTgaInfo->w <= 0 || m_pTgaInfo->h <= 0 || m_pTgaInfo->bpp <= 0 ) return gxFalse;

	if(m_pTgaInfo->ColorMapType)
	{
		//パレット付の場合
		m_Pallet     = (Uint32*)&m_pFileImage[sizeof(stTgaInfo)];
		m_TexelImage = (Uint8*)&m_pFileImage[sizeof(stTgaInfo)+m_pTgaInfo->clutNum*(m_pTgaInfo->ColorMapSize/8)];//ColorMapType];
	}
	else
	{
		//パレット無の場合
		m_TexelImage = &m_pFileImage[sizeof(stTgaInfo)];
		m_Pallet 	= NULL;
	}

	convToPlatformPixels();

	return true;
}

struct _RGB {
	Uint8 r;
	Uint8 g;
	Uint8 b;
};

struct _ABGR {
	Uint8 a;
	Uint8 b;
	Uint8 g;
	Uint8 r;
};


void CFileTarga::convToPlatformPixels()
{
#if 0   //defined BIG_ENDIAN
	//assert(m_pTgaInfo->bpp == 32 || m_pTgaInfo->bpp == 24);

	unsigned size = m_pTgaInfo->w * m_pTgaInfo->h;

	switch (m_pTgaInfo->bpp) {
	case 32:
		for (unsigned i = 0; i < size; ++i) {
			unsigned c = ((unsigned*)m_TexelImage)[i];
			unsigned a = TARGA_GET_A(c);
			unsigned r = TARGA_GET_R(c);
			unsigned g = TARGA_GET_G(c);
			unsigned b = TARGA_GET_B(c);
			c		   = TEX_ABGR(a, r, g, b);
			((unsigned*)m_TexelImage)[i] = c;
		}
		break;

	case 24:
		for (unsigned i = 0; i < size; ++i) {
			_RGB c = ((_RGB*)m_TexelImage)[i];
			//unsigned a = 0xff;
			unsigned r = c.r;
			unsigned g = c.g;
			unsigned b = c.b;
			c.r = r;
			c.g = g;
			c.b = b;
			((RGB*)m_TexelImage)[i] = c;
		}
		break;
	case 8:
		{
	        if( false ) {
	            if( m_pTgaInfo->ColorMapSize == 32 ) {
	                for (unsigned i = 0; i < size; ++i) {
	                    int index = ((Uint8*)m_TexelImage)[i];
	                    unsigned c = 
						_ABGR c = (_ABGR*)((unsigned*)m_Pallet)[index];
	                    unsigned a = c.a;
	                    unsigned r = c.r;
	                    unsigned g = c.g;
	                    unsigned b = c.b;
	                    ((unsigned*)m_TexelImage)[i] = ARGB(a,b,g,r);
	                }
	            } else {
	                for (unsigned i = 0; i < size; ++i) {
	                    int index = ((Uint8*)m_TexelImage)[i];
	                    RGB c = ((RGB*)m_Pallet)[index];
	                    unsigned a = 0xff;
	                    unsigned r = c.b;
	                    unsigned g = c.g;
	                    unsigned b = c.r;
	                    ((unsigned*)m_TexelImage)[i] = ARGB(a,b,g,r);
	                }
	            }
	        } else {
	            unsigned clutNum = m_pTgaInfo->clutNum[0] | (m_pTgaInfo->clutNum[1] << 8);
	            Uint8* clut = (Uint8*)m_Pallet;
	            if (m_pTgaInfo->ColorMapSize == 32) {
	                for (int i = 0; i < clutNum; i++) {
	                    m_Pallet[i] = ARGB(*(clut + 3), *(clut + 0), *(clut + 1), *(clut + 2));
	                    clut += 4;
	                }
	            }
	        }
		}
	    break;

	default:
		break;
	}
#endif
}


Uint32 CFileTarga::SetRGB(int x,int y,Uint32 col)
{
	//------------------------------------------------------
	//ピクセルの合成(新設RGBのみアルファチャンネルを残す版)
	//------------------------------------------------------

	if( m_pTgaInfo && m_pTgaInfo->ColorMapType )
	{
		return 0x00;
	}

	Uint32 *pos2 = (Uint32*)getPos(x,y);

	*pos2  = (*pos2)&0xff000000;
	*pos2 |= (col&0x00ffffff);

	return *pos2;	
}


Uint32 CFileTarga::SetARGB(int x,int y,Uint32 col)
{
	//--------------------------------------------
	//ピクセルの合成
	//--------------------------------------------

///*毒*/	Uint8 *pos = getPos(x,y);

	if(m_pTgaInfo && m_pTgaInfo->ColorMapType)
	{
		return 0x00;
	}

	
	*(Uint32*)getPos(x,y) = col;

	//Uint32 *pos2 = 
	//*pos2 = col;

	return col;//*pos2;	
}


void CFileTarga::SetARGB32(int x,int y,Uint32 col)
{
	//--------------------------------------------
	//ピクセルの合成
	//--------------------------------------------


	*(Uint32*)getPos(x,y) = col;
}


Uint32 CFileTarga::SetARGBadd(int x,int y,Uint32 col)
{
	//--------------------------------------------
	//ピクセルの加算合成
	//--------------------------------------------
	Uint32 *pos = (Uint32*)getPos(x,y);

	//stTgaPixel dstcolor;
	stTgaPixel *srccolor = (stTgaPixel*)pos;

	int a,r,g,b;
	a = (col>>24)&0x000000ff;
	r = (col>>16)&0x000000ff;
	g = (col>>8)&0x000000ff;
	b = (col>>0)&0x000000ff;

	a += srccolor->alpha;
	r += srccolor->red;
	g += srccolor->green;
	b += srccolor->blue;

	if(a>= 0xff) a = 0xff;
	if(r>= 0xff) r = 0xff;
	if(g>= 0xff) g = 0xff;
	if(b>= 0xff) b = 0xff;

	*pos = ((a<<24)|(r<<16)|(g<<8)|(b<<0));

	return *pos;

}

Uint32 CFileTarga::SetARGBsub(int x,int y,Uint32 col)
{
	//--------------------------------------------
	//ピクセルの減算合成
	//--------------------------------------------
	Uint32 *pos = (Uint32*)getPos(x,y);

	//stTgaPixel dstcolor;
	stTgaPixel *srccolor = (stTgaPixel*)pos;

	int a,r,g,b;
	a = (col>>24)&0x000000ff;
	r = (col>>16)&0x000000ff;
	g = (col>>8)&0x000000ff;
	b = (col>>0)&0x000000ff;

	a -= srccolor->alpha;
	r -= srccolor->red;
	g -= srccolor->green;
	b -= srccolor->blue;

	if(a<= 0x00) a = 0xff;
	if(r<= 0x00) r = 0xff;
	if(g<= 0x00) g = 0xff;
	if(b<= 0x00) b = 0xff;

	*pos = ((a<<24)|(r<<16)|(g<<8)|(b<<0));

	return *pos;

}


Uint32 CFileTarga::SetARGBmul(int x,int y,Uint32 col)
{
	//--------------------------------------------
	//EsENEZEaCAa?eZcae¨
	//--------------------------------------------
	Uint32 *pos = (Uint32*)getPos(x,y);
    
	stTgaPixel dstcolor;
	stTgaPixel *srccolor = (stTgaPixel*)pos;
    
	Uint8 a,r,g,b;
	dstcolor.alpha = a = (col>>24)&0x000000ff;
	dstcolor.red   = r = (col>>16)&0x000000ff;
	dstcolor.green = g = (col>>8)&0x000000ff;
	dstcolor.blue  = b = (col>>0)&0x000000ff;
    
	a = srccolor->alpha * dstcolor.alpha / 255.0f;
	r = srccolor->red   * dstcolor.red   / 255.0f;
	g = srccolor->green * dstcolor.green / 255.0f;
	b = srccolor->blue  * dstcolor.blue  / 255.0f;
    
	*pos = ((a<<24)|(r<<16)|(g<<8)|(b<<0));
    
	return *pos;
    
}


Sint32 CFileTarga::GetPalletIndex(int x,int y)
{
	Uint8 *pos = getPos(x,y);

	if(m_pTgaInfo->ColorMapType)
	{
		return *pos;
	}

	return 0x00;
}

void CFileTarga::SetPalletIndex(int x, int y, Uint8 index)
{
	Uint8* pos = getPos(x, y);

	if (m_pTgaInfo->ColorMapType)
	{
		*pos = index;
	}
}

Uint8* __pos;
Uint32 __1_argb;
Uint8 *__pPal;

Uint32 CFileTarga::GetARGB(int x,int y)
{
	__pos = getPos(x,y);

	if(m_pTgaInfo->ColorMapType)
	{
		if(m_pTgaInfo->ColorMapSize == 24 )
		{
			__pPal = (Uint8*)&m_Pallet[0];//(*pos)*3];
			__pPal += (*__pos)*3;
			int a,r,g,b;
			a = 0xff;
			b = __pPal[0];
			g = __pPal[1];
			r = __pPal[2];

			return (a<<24) | (r<<16) | (g<<8) | (b<<0);
		}

		if(m_pTgaInfo->ColorMapSize == 32 )
		{
			return m_Pallet[*__pos];
		}
	}

	 __1_argb = *(Uint32*)__pos;

	if(m_pTgaInfo->bpp == 24 )
	{
		__1_argb = 0xff000000;
		__1_argb = (__pos[2]<<16) | (__pos[1]<<8 ) | (__pos[0]<<0 ) ;
		__1_argb |= 0xff000000;
	}

	return __1_argb;
}


Uint32 CFileTarga::GetARGB32(int x,int y)
{
	//とりあえず最高速度のGETARGB

	return *(Uint32*)getPos(x,y);
}


Uint8 CFileTarga::setID(Sint8 _palletNum)
{
	//--------------------------------------------
	//ヘッダー情報のIDを生成する
	//--------------------------------------------

	if(_palletNum) return 1;	//INDEX
	return 0;					//RGB

/*
	case 1:	//INDEX
	case 2:	//RGB
		break;
	case 0:	//データなし
	case 3:	//白黒
	case 9:	//INDEX:RLE圧縮
	case 10://RGB  :RLE圧縮
	case 11://白黒 :RLE圧縮
	default:
		break;
	}
*/
}


 Uint8* CFileTarga::getPos(int x,int y)
{
	//-----------------------------------------------
	//イメージ上の座標位置アドレスを得る
	//-----------------------------------------------

	static int s_mode;

	s_mode = m_pTgaInfo->mode&0x30;

	if((s_mode&0x10) != 0 ) x = (m_pTgaInfo->w-1)-x;	//右から
	if((s_mode&0x20) == 0 ) y = (m_pTgaInfo->h-1)-y;	//下から

	Sint32 n = 0;
	
	n = ((x) + (y*m_pTgaInfo->w)) *(m_pTgaInfo->bpp>>3);
	if( n >= (m_pTgaInfo->w * m_pTgaInfo->h)*(m_pTgaInfo->bpp>>3) || n < 0 )
	{
		ASSERT( n == 0 );
		n = 0;
	}
	return &m_TexelImage[ n ];

}

gxBool CFileTarga::Line(int x0,int y0,int x1,int y1,int atr,int col)
{
	int E,x,y;
	int dx,dy,sx,sy,i;

	sx = ( x1 > x0 ) ? 1 : -1;
	dx = ( x1 > x0 ) ? x1 - x0 : x0 - x1;
	sy = ( y1 > y0 ) ? 1 : -1;
	dy = ( y1 > y0 ) ? y1 - y0 : y0 - y1;

	x = x0;
	y = y0;

	if( dx >= dy )
	{
		// 傾きが1以下の場合

		E = -dx;

		for( i = 0; i <= dx; i++ )
		{
			Point( x, y, atr,col );

			x += sx;
			E += 2 * dy;

			if( E >= 0 )
			{
				y += sy;
				E -= 2 * dx;
			}
		}
	}
	else
	{
		// 傾きが1より大きい場合
		E = -dy;
		for( i = 0; i <= dy; i++ )
		{
			Point( x, y, atr,col );
			y += sy;
			E += 2 * dx;
			if( E >= 0 )
			{
				x += sx;
				E -= 2 * dy;
			}
		}
	}

	return true;
}

gxBool CFileTarga::Point(int x1,int y1,int atr,int col)
{
	//点を打つ

	if(atr&0x04){
		SetARGBadd(x1,y1,col);
	}else if(atr&0x08){
		SetARGBsub(x1,y1,col);
	}else{
		SetARGB(x1,y1,col);
	}
	return gxTrue;
}

gxBool CFileTarga::Box(int x1,int y1,int x2,int y2,int atr,int col)
{
	//箱を書く

	int tmp;

	if(x2<x1){
		tmp = x1;
		x1 = x2;
		x1 = tmp;
	}

	if(y2<y1){
		tmp = y1;
		y1 = y2;
		y1 = tmp;
	}

	for(int y=y1;y<=y2;y++){
		for(int x=x1;x<=x2;x++){
			if(y==y1 || x==x1 || y==y2 || x==x2) {
				Point(x,y,atr,col);
				continue;
			}
		}
	}

	return true;
}

gxBool CFileTarga::BoxFill(int x1,int y1,int x2,int y2,int atr,int col)
{
	int tmp;

	if(x2<x1)
	{
		tmp = x1;
		x1 = x2;
		x1 = tmp;
	}

	if(y2<y1)
	{
		tmp = y1;
		y1 = y2;
		y1 = tmp;
	}

	for(int y=y1;y<y2;y++)
	{
		for(int x=x1;x<x2;x++)
		{
			Point(x,y,atr,col);
		}
	}

	return true;
}

gxBool CFileTarga::Copy(int x1,int y1,int x2,int y2,int x3,int y3,int atr,int col)
{
	//-----------------------------------------
	//自分のテクスチャの中でコピーを行う
	//x1,y1,x2,y2::コピー元の矩形
	//x3,y3::コピー先の座標
	//-----------------------------------------

	//x1,y1=u1,v1 x2,y2 = u2,v2, x3,y3=x,y;
	CFileTarga *rect = new CFileTarga();
	int xx,yy,x,y;//,flip=atr&(0x01|0x02);

	rect->Create(x2-x1,y2-y1,GetBPP(),(Uint8*)m_Pallet);

	yy = 0;
	for(y=y1;y<y2;y++)
	{
		xx = 0;
		for(x=x1;x<x2;x++)
		{
			rect->SetARGB(xx,yy,GetARGB(x,y));
			xx++;
		}
		yy++;
	}

	Copy(rect,0,0,x2-x1,y2-y1,x3,y3);

	delete rect;

	return true;

}

gxBool CFileTarga::Copy(CFileTarga *pTga,int x1,int y1,int x2,int y2,int x3,int y3,int atr,int col)
{
	//-----------------------------------------
	//他のテクスチャとコピーを行う
	//x1,y1,x2,y2::コピー元の矩形
	//x3,y3::コピー先の座標
	//-----------------------------------------

	//x1,y1=u1,v1 x2,y2 = u2,v2, x3,y3=x,y;
	int xx,yy,x,y,flip=atr&(0x01|0x02);
	int w=0,h=0,wx=-1,hy=-1,argb;
	int xmax = GetWidth();
	int ymax = GetHeight();

	int mode = atr&(0x04|0x08);

	yy = 0;
	if(flip&0x01)
	{
		w = (x2-x1)-1;
		wx = 1;
	}

	if(flip&0x02)
	{
		h = (y2-y1)-1;
		hy = 1;
	}

	int px,py;

	for(y=y1;y<y2;y++)
	{
		xx = 0;
		py = y3+(h-yy)*hy;
		if(py > ymax) continue;

		for(x=x1;x<x2;x++)
		{
			px = x3+(w-xx)*wx;
			if(px >= xmax || px < 0)
			{
				xx ++;
				continue;
			}

			argb = pTga->GetARGB(x1+xx,y1+yy);
			if(0)
				{//(m_BgColor&0x00ffffff) == (unsigned int)(argb&0x00ffffff)){
				//抜き色なしにしてしまいます
			}
			else
			{
				if(mode&0x04)
				{
					SetARGBadd(px,py,argb);
				}
				else if(mode&0x08)
				{
					SetARGBsub(px,py,argb);
				}
				else
				{
					SetARGB(px,py,argb);
				}
			}
			xx ++;
		}
		yy ++;
	}

	return true;
}

gxBool CFileTarga::loadBitmap(gxChar *filename,Uint32 colorKey )
{
	//--------------------------------------------------
	//Bmpファイルを読み込んでtgaファイルとして保持する
	//ただし、何が何でも32bitTGAとします。
	//--------------------------------------------------

	size_t sLength;
	Uint8 * pBuffer=NULL;

	pBuffer = gxLib::LoadFile( filename , &sLength );

	if( pBuffer == NULL )
	{
		return gxFalse;
	}

	convertBMPtoTGA((Uint8*)pBuffer , sLength , colorKey);

	delete[] pBuffer;

	return true;
}

gxBool CFileTarga::loadPNG( gxChar *filename , Uint32 colorKey )
{
	//--------------------------------------------------
	//Bmpファイルを読み込んでtgaファイルとして保持する
	//ただし、何が何でも32bitTGAとします。
	//--------------------------------------------------

	size_t sLength;
	Uint8 * pData = NULL;

	pData = gxLib::LoadFile( filename , &sLength );

	if( pData == NULL )
	{
		return gxFalse;
	}

	convertPNGtoTGA(pData, sLength , colorKey);

	delete[] pData;

	return gxTrue;
}


//---------------------------------------------------------
//BMP関連
//---------------------------------------------------------

typedef struct stBmpHead {
	Uint16/*short*/	bfType;				// BM 0x4d42
	Uint32/*long*/	bfSize;				// Filesize
	Uint16/*short*/	bfReserved1;		// 0
	Uint16/*short*/	bfReserved2;		// 0
	Uint32/*long*/	bfOffBits;			// ファイルの先頭からデータ部までのオフセット
} stBmpHead;

typedef struct stBmpInfo {
	Uint32/*long*/	biSize;				// BMPINFOのサイズ：40Byte
	Uint32/*long*/	biWidth;			// 画像の横幅
	Uint32/*long*/	biHeight;			// 画像の高さ
	Uint16/*short*/	biPlanes;			// プレーン数(必1)
	Uint16/*short*/	biBitCount;			// PixelあたりのBit数
	Uint32/*long*/	biCompression;		// 圧縮(必：無し：0)
	Uint32/*long*/	biSizeImage;		// ?? 0
	Uint32/*long*/	biXPelsPerMeter;	// 横解像度(7198)
	Uint32/*long*/	niYPelsPerMeter;	// 縦解像度(7198)
	Uint32/*long*/	biClrUsed;			// 使用色数(0)
	Uint32/*long*/	biClrImportant;		// 重要な色数(0)
} stBmpInfo;

#define GETOFFSET_BMP(x,y,bmpx,bmpy)		(( ( (bmpy-1)-(y) ) *(bmpx) )  +(x))

gxBool CFileTarga::convertBMPtoTGA(Uint8 *p, size_t sz ,Uint32 colorKey )
{
	//-------------------------------------------------
	//BMPからTGAに変換します
	//-------------------------------------------------
	stBmpHead* pBmpHead;
	stBmpInfo* pBmpInfo;
	int w,h;
	Uint8 *pImage;
	Uint8 *pPallet;

	pBmpHead = (stBmpHead*)p;
	pBmpInfo = (stBmpInfo*)&p[sizeof(stBmpHead)];

	w = pBmpInfo->biWidth;
	h = pBmpInfo->biHeight;

	if(pBmpInfo->biBitCount==8){
		//パレットの場合
		pPallet = &p[sizeof(stBmpHead)+sizeof(stBmpInfo)];
		pImage  = &p[pBmpHead->bfOffBits];
	}else{
		//RGBの場合
		pPallet = NULL;
		pImage  = &p[sizeof(stBmpHead)+sizeof(stBmpInfo)];
	}

	//BMPサイズの空のtgaファイルを生成
	Create(pBmpInfo->biWidth,pBmpInfo->biHeight,32/*pBmpInfo->biBitCount*/,NULL);

	//ピクセル単位でデータをコピーします
	Uint32 a=0x00,r=0,g=0,b=0;

	for(int y=0;y<h;y++) {
		for(int x=0;x<w;x++) {
			int offset = GETOFFSET_BMP(x,y,w,h)*(pBmpInfo->biBitCount>>3);
			Uint8 index = pImage[offset];
			int col=0;
			switch(pBmpInfo->biBitCount){
			case 4:
				//めんどいのでサポートしない
				break;
			case 8:
				b = pPallet[index*4+0]&0x000000ff;
				g = pPallet[index*4+1]&0x000000ff;
				r = pPallet[index*4+2]&0x000000ff;
				a = ( (colorKey&0x00ffffff) == ((r<<16)|(g<<8)|(b<<0)) )? 0x00 : 0xff;
				break;
			case 24:
				b = pImage[offset+0]&0x000000ff;
				g = pImage[offset+1]&0x000000ff;
				r = pImage[offset+2]&0x000000ff;
				//colorKey = 0xffff00ff;
				a = ( (colorKey&0x00ffffff) == ((r<<16)|(g<<8)|(b<<0)) )? 0x00 : 0xff;
				break;
			case 32:
				b = pImage[offset+0]&0x000000ff;
				g = pImage[offset+1]&0x000000ff;
				r = pImage[offset+2]&0x000000ff;
				a = ( (colorKey&0x00ffffff) == ((r<<16)|(g<<8)|(b<<0)) )? 0x00 : 0xff;
				break;
			}

			col = ((a<<24)|(r<<16)|(g<<8)|(b<<0));

			SetARGB(x,y,col);
		}
	}

	return gxTrue;
}


void CFileTarga::setTexelImage( Uint8* pBuf )
{
	if (pBuf == nullptr)
	{
		//特殊処理(applyTextureを行ったあとにメモリを開放させたくない)
		m_pFileImage = nullptr;
		return;
	}
	gxUtil::MemCpy( &m_pFileImage[sizeof(stTgaInfo)] , pBuf , m_pTgaInfo->w*m_pTgaInfo->h*(m_pTgaInfo->bpp>>3) );
	m_pTgaInfo->mode |= 0x20;
}


void CFileTarga::ClearImage()
{
	gxUtil::MemSet(m_TexelImage,0x00,m_pTgaInfo->w*m_pTgaInfo->h*(m_pTgaInfo->bpp/8));
}


Uint8* CFileTarga::RLEunpack( Uint8* pBuf , Uint32 uSize )
{
	//-------------------------------------------
	// RLE圧縮されたTGAを展開します
	//-------------------------------------------

	Uint32 w = m_pTgaInfo->w;
	Uint32 h = m_pTgaInfo->h;
	Uint32 pixelSize = m_pTgaInfo->bpp/8;
	Uint32 uBufSize = w*h*pixelSize;
	Uint32 uBufPos1 = 0;	//圧縮データのポインタ
	Uint32 uBufPos2 = 0;	//展開後データのポインタ

	Sint32 bpp = m_pTgaInfo->bpp;
	Uint8* pPalletBuffer = NULL;
	Uint32 uPalletBufSize = m_pTgaInfo->clutNum*(m_pTgaInfo->ColorMapSize/8);

	//画像フォーマットに応じたバッファサイズを確保
	Uint8* pFileImage = new Uint8[ uBufSize ];

	pBuf = &m_pFileImage[ sizeof(stTgaInfo) + uPalletBufSize ];

	if( uPalletBufSize > 0 )
	{
		m_Pallet = (Uint32*)&m_pFileImage[ sizeof(stTgaInfo) ];
	}


	//イメージタイプを非圧縮タイプに変更
//	if( m_pTgaInfo->ImageType >= 0x08 ) m_pTgaInfo->ImageType -= 0x08;


	for(Uint32 yy=0; yy<h; yy++)
	{
		//１ライン毎の対応
		for(Uint32 xx=0; xx<w; xx++)
		{
			Uint32 sCnt = 0;
			gxBool bSingle = gxFalse;	//リテラルなグループか？

			if( (pBuf[uBufPos1+0]&0x80) )
			{
				//連続したデータ領域がある(反復)
				sCnt = pBuf[ uBufPos1+0 ]&0x7f;
			}
			else
			{
				//連続してないピクセルがsCnt+1続く
				sCnt = pBuf[ uBufPos1+0 ]&0x7f;
				bSingle = gxTrue;
			}

			sCnt ++;

			//制御データ分を１つすすめる
			uBufPos1 ++;

			for(Uint32 ii=0;ii<sCnt;ii++)
			{
				if( uBufPos2 > uBufSize )
				{
					//展開後のバッファサイズを超えてしまった
					GX_DEBUGLOG("ERROR::展開後のバッファサイズを超えてしまった(%d / %d )",uBufPos2 , uBufSize);
					break;
				}

				if( uBufPos1 >= uSize )
				{
					//圧縮データの範囲を超えてデコードしにいってしまった
					//GX_DEBUGLOG("ERROR::展開後のバッファサイズを超えてしまった(%d / %d )",uBufPos1 , uSize );
					//break;
				}

				switch(pixelSize){
				case 1:	//8bit
					pFileImage[ uBufPos2+0 ] = pBuf[uBufPos1+0];
					break;

				case 3:	//24bit
					pFileImage[ uBufPos2+0 ] = pBuf[uBufPos1+0];
					pFileImage[ uBufPos2+1 ] = pBuf[uBufPos1+1];
					pFileImage[ uBufPos2+2 ] = pBuf[uBufPos1+2];
					break;

				case 4:	//32bit
					pFileImage[ uBufPos2+0 ] = pBuf[uBufPos1+0];
					pFileImage[ uBufPos2+1 ] = pBuf[uBufPos1+1];
					pFileImage[ uBufPos2+2 ] = pBuf[uBufPos1+2];
					pFileImage[ uBufPos2+3 ] = pBuf[uBufPos1+3];
					break;
				default:
					GX_DEBUGLOG("ERROR::よくわからない謎のTGA画像(%dbit)",pixelSize*8);
					break;
				}

				uBufPos2 += pixelSize;

				if( bSingle ) uBufPos1 += pixelSize;
			}

			xx += sCnt;
			xx --;
			if( !bSingle ) uBufPos1 += pixelSize;
		}
	}

	//--------------------------------------------------
	//パレット関連の制御
	//--------------------------------------------------

	if( m_Pallet )
	{
		//パレットがある場合はパレットデータをコピーしておく

		pPalletBuffer = new Uint8[ uPalletBufSize ];
		gxUtil::MemCpy( pPalletBuffer , m_Pallet , uPalletBufSize );
	}

	//新規にテクスチャを生成
	Create( w , h , bpp , (Uint8*)pPalletBuffer );

	//テクスチャイメージをコピー
	gxUtil::MemCpy( m_TexelImage , pFileImage , uBufSize );

	//パレットバッファを解放
	if( pPalletBuffer )	SAFE_DELETES( pPalletBuffer );

	SAFE_DELETES( pFileImage );

	return NULL;
}


gxBool CFileTarga::convertPNGtoTGA( Uint8 *p, size_t sz ,Uint32 colorKey )
{
	Sint32 w,h;
	Sint32 comp;
	Uint8 *pData2;
	pData2 = stbi_load_from_memory( (Uint8*)p, (int)sz, &w , &h, &comp, 0 );

	CFileTarga tga;
	tga.Create(w, h, comp * 8);
	gxUtil::MemCpy(tga.GetTexelImage(), pData2, w*h*comp);

	Create(w, h, comp * 8);

	Uint32 argb;
	Uint32 abgr;
	for (Sint32 y = 0; y < h; y++)
	{
		for (Sint32 x = 0; x < w; x++)
		{
			abgr = tga.GetARGB(x, y);
			argb = 0x0;
			argb |= (((abgr >> 24)&0xff)   << 24);
			argb |= (((abgr >> 0) & 0xff)  << 16);
			argb |= (((abgr >> 8) & 0xff)  << 8);
			argb |= (((abgr >> 16) & 0xff) << 0);
			SetARGB(x, h-y-1 ,argb );
		}
	}
	//SaveFile("test2.tga");
	//gxLib::UploadTexture();
	return gxTrue;
}

Sint32 __mx;
Sint32 __my;
Uint32 __n;
Uint32 __argb;
Uint32 __Counter;

void CFileTarga::Paint(Uint32 uCol, Sint32 x, Sint32 y)
{
	Sint32 mx = x;
	Sint32 my = y;

	if ((m_pTgaInfo->mode & 0x10) != 0) mx = (m_pTgaInfo->w - 1) - mx;	//右から
	if ((m_pTgaInfo->mode & 0x20) == 0) my = (m_pTgaInfo->h - 1) - my;	//下から

	Sint32 n = ((mx)+(my * m_pTgaInfo->w)) * (m_pTgaInfo->bpp >> 3);

	if (n >= (m_pTgaInfo->w * m_pTgaInfo->h) * (m_pTgaInfo->bpp >> 3) || n < 0)
	{
		//範囲外なら帰る
		return;
	}

	Uint32 baseColor = GetARGB(x, y);

	__Counter = 0;

	paint( uCol , x , y );

}

/*
void CFileTarga::paint( Uint32 uBaseColor , Uint32 uPaintColor , Sint32 x , Sint32 y )
{
	__Counter ++;
	if( __Counter >= 1024*1024*10 ) return;


	if ((m_pTgaInfo->mode & 0x10) != 0) __mx = (m_pTgaInfo->w - 1) - __mx;	//右から
	if ((m_pTgaInfo->mode & 0x20) == 0) __my = (m_pTgaInfo->h - 1) - __my;	//下から

	__n = ((__mx)+(__my * m_pTgaInfo->w)) * (m_pTgaInfo->bpp >> 3);

	if (__n >= (m_pTgaInfo->w * m_pTgaInfo->h) * (m_pTgaInfo->bpp >> 3 ) )
	{
		//範囲外なら帰る
		return;
	}


	__argb = GetARGB( x , y )&0x00ffffff;

	if( __argb != (uBaseColor&0x00ffffff) )
	{
		//同じ色でなければ帰る
		return;
	}

	if( __argb == (uPaintColor&0x00ffffff) )
	{
		//すでに塗っていれば帰る
		return;
	}

	SetARGB( x , y , 0xFF000000|uPaintColor );

	paint( uBaseColor, uPaintColor , x-1 , y );
	paint( uBaseColor, uPaintColor , x+1 , y );
	paint( uBaseColor, uPaintColor , x   , y-1 );
	paint( uBaseColor, uPaintColor , x   , y+1 );
}
*/



//#define MAXSIZE 1024*1024 /* バッファサイズ */

/* 画面サイズは 1024 X 1024 とする */
//#define MINX 0
//#define MINY 0
//#define MAXX (2048-1)
//#define MAXY (2048-1)

Uint32 m_uMaxBuffSize = 0;

//struct BufStr {
//  int sx; /* 領域右端のX座標 */
//  int sy; /* 領域のY座標 */
//};
gxPos *m_pBuff;		//buff[MAXSIZE]; /* シード登録用バッファ
gxPos *sIdx, *eIdx;  // buffの先頭・末尾ポインタ


/*
  scanLine : 線分からシードを探索してバッファに登録する

  int lx, rx : 線分のX座標の範囲
  int y : 線分のY座標
  unsigned int col : 領域色
*/
void CFileTarga::scanLine( int lx, int rx, int y, Uint32 col )
{
  while ( lx <= rx ) {

    // 非領域色を飛ばす
	for ( ; lx <= rx ; lx++ )
	{
		if ( GetARGB( lx, y ) == col ) break;
	}

    if ( GetARGB( lx, y ) != col ) break;

	// 領域色を飛ばす
	for ( ; lx <= rx ; lx++ )
	{
		if ( GetARGB( lx, y ) != col ) break;
	}

    eIdx->x = lx - 1;
    eIdx->y = y;

    if ( ++eIdx == &m_pBuff[ m_uMaxBuffSize ] ) eIdx = m_pBuff;
  }
}


void CFileTarga::paint( Uint32 paintCol , Sint32 x , Sint32 y )
{
	//塗りつぶし処理
	//http://fussy.web.fc2.com/algo/algo3-1.htm

	Sint32 lx, rx;
	Sint32 ly;
	Sint32 i;

	Uint32 uBaseColor = GetARGB( x, y );	// 塗りつぶし開始の色

	if ( uBaseColor == paintCol ) return;	// 塗りつぶそうとしている色と下地が同じであれば処理しない

	m_uMaxBuffSize = GetWidth() * GetHeight();

	SAFE_DELETES( m_pBuff );

	m_pBuff = new gxPos[m_uMaxBuffSize];

	sIdx = m_pBuff;
	eIdx = m_pBuff + 1;

	sIdx->x = x;
	sIdx->y = y;

	do {

		lx = rx = sIdx->x;
		ly = sIdx->y;

		if ( ++sIdx == &m_pBuff[ m_uMaxBuffSize ] )
		{
			sIdx = m_pBuff;
		}

		// 処理済のシードなら無視

		if ( GetARGB( lx, ly ) != uBaseColor )
		{
			continue;
		}

		// 右方向の境界を探す
		while ( rx < Sint32(GetWidth()-1) )
		{
			if ( GetARGB( rx + 1, ly ) != uBaseColor ) break;
			rx++;
		}

		// 左方向の境界を探す
		while ( lx > 0 )
		{
			if ( GetARGB( lx - 1, ly ) != uBaseColor ) break;
			lx--;
		}

		// lx-rxの線分を描画
		for ( i = lx ; i <= rx ; i++ )
		{
			SetARGB( i, ly, paintCol );
		}

		// 真上のスキャンラインを走査する
		if ( ly - 1 >= 0 )
		{
			scanLine( lx, rx, ly - 1, uBaseColor );
		}

		// 真下のスキャンラインを走査する
		if ( ly + 1 <= Sint32(GetHeight()-1) )
		{
			scanLine( lx, rx, ly + 1, uBaseColor );
		}

	} while ( sIdx != eIdx );
}

void CFileTarga::Convert32( Uint32 colorKey )
{
	//32bitカラーに変換する

	Uint32 xmax = GetWidth();
	Uint32 ymax = GetHeight();

	Uint32 *pARGB32Buf = new Uint32[ymax*xmax];

	Uint32 n = 0;
	Uint32 argb;
	for( Uint32 y=0; y<ymax; y++)
	{
		for( Uint32 x=0; x<xmax; x++)
		{
			argb = GetARGB(x,y);
			pARGB32Buf[n] = argb;
			n ++;
		}
	}

	Create( xmax , ymax,32 );

	n = 0;

	for( Uint32 y=0; y<ymax; y++)
	{
		for( Uint32 x=0; x<xmax; x++)
		{
			SetARGB(x,y , pARGB32Buf[n] );
			n ++;
		}
	}

	SAFE_DELETES(pARGB32Buf);
}


void ConvertUE4Normal2GXNormal( std::string fileName )
{
	size_t uSize = 0;
	Uint8* pData = gxLib::LoadFile(fileName.c_str(), &uSize);

	CFileTarga tga;
	CFileTarga tga2;
	tga.ReadFile(pData, uSize);

	tga2.Create(tga.GetWidth(), tga.GetHeight(), 32);

	Uint32 argb;
	Sint32 a=0, r, g, b;
	Float32 fx, fy, fz;

	for (Uint32 y = 0; y < tga.GetHeight(); y++)
	{
		for (Uint32 x = 0; x < tga.GetWidth(); x++)
		{
			argb = tga.GetARGB(x, y);

			//argb = tga.GetARGB(334, 116);
			r = (argb >> 16) & 0xff;
			g = (argb >> 8) & 0xff;
			b = (argb >> 0) & 0xff;

			r = r -= 127;
			g = g -= 127;
			b = b -= 127;

			fx = g / 127.0f;
			fy = b / 127.0f;
			fz = -r / 127.0f;

			r = 128 + fx * 127;
			g = 128 + fy * 127;
			b = 128 + fz * 127;

			r = CLAMP(r, 0, 255);
			g = CLAMP(g, 0, 255);
			b = CLAMP(b, 0, 255);

			argb = ARGB(0xff, (Uint8)(r), (Uint8)(g), (Uint8)(b));
			/*
			argb = (0xff << 24);
			argb |= (r<<16);
			argb |= (g << 8);
			argb |= (b << 0);
			*/
			tga2.SetARGB(x, y, argb);
		}
	}

	tga2.SaveFile("test.tga");
}

void CFileTarga::MakeAutoAlphaChannel(Sint32 mode )
{
	Uint32 argb;
	Sint32 a = 0, r, g, b;
	Uint32 max = 0;
	for (Uint32 y = 0; y < GetHeight(); y++)
	{
		for (Uint32 x = 0; x < GetWidth(); x++)
		{
			argb = GetARGB(x, y);
			r = (argb >> 16) & 0xff;
			g = (argb >> 8)  & 0xff;
			b = (argb >> 0)  & 0xff;
			if (mode == 0)
			{
				//最も輝度が高いものでアルファを作る
				a = LARGER(r, g);
				a = LARGER(a, b);
				//a = (a > 200) ? 255 : 0;
			}
			else if (mode == 1)
			{
				//色がついていればアルファを作成する
				a = (r + g + b)? 0xff : 0x00;
			}
			else if (mode == 2)
			{
				//RGBの平均でグレースケールを作成する
				a = (r + g + b) / 3;
			}
			SetARGB(x, y,ARGB(a,r,g,b));
		}
	}



}

#pragma pack(pop)


