﻿//UI関連の作業場所
#ifndef GX_UIMANAGER_H_
#define GX_UIMANAGER_H_
class gxUIManager
{
public:
	enum FadeType{
		eFadeTypeAuto,
		eFadeTypeNormal,
		eFadeTypeWipe,
		eFadeTypeLine,
		eFadeTypeCircle,
		eFadeTypeCrossFade,
	};

	gxUIManager();

	~gxUIManager()
	{

	}

	void Start()
	{
		m_bStart = gxTrue;
	}

	void NowLoading( gxBool bNowLoadingOn = gxTrue )
	{
		//m_bNowLoadingOn = bNowLoadingOn;
		m_fNowLoading = 2.0f;
	}

	void NowSaving( gxBool bNowLoadingOn = gxTrue )
	{
		//m_bNowLoadingOn = bNowLoadingOn;
		m_fNowSaving = 2.0f;
	}

	void NowNetworking( gxBool bNowNetworking = gxTrue )
	{
		//m_bNowNetworking = bNowNetworking;
		m_fNowHttpConnecting = 2.0f;

	}

	void NowBlueToothConnect( gxBool bNowBluetoothing )
	{
		//m_bNowBluetoothing = bNowBluetoothing;
		m_fNowBlueToothEnable = 2.0f;
	};

	void   SetFade( gxBool bFadeOut , FadeType fadeType = eFadeTypeAuto)
	{
		if (bFadeOut)
		{
			if (m_fFadeNow < 1.0f) m_FadeDir = 1;
		}
		else
		{
			if (m_fFadeNow > 0.0f) m_FadeDir = -1;
		}

		if (m_FadeDir == 0)
		{
			return;
		}

		SetFadeSpeed(15.0f);

		if (fadeType != eFadeTypeAuto)
		{
			m_FadeType = fadeType;
		}
	};

	void   SetFadeSpeed( Float32 fSec )
	{
		if (fSec <= 0.0f)
		{
			//瞬時にフェードさせる
			if (m_FadeDir > 0)
			{
				m_fFadeNow = 1.0f;
				m_fFadeSpeed = 0.0f;
			}
			else
			{
				m_fFadeNow = 0.0f;
				m_fFadeSpeed = 0.0f;
			}
			return;
		}

		m_fFadeSpeed = m_FadeDir / fSec ;
	};

	void SetFadeColor( Uint32 rgb )
	{
		m_FadeARGB = SET_ALPHA(1.0f, rgb);
	}

	gxBool IsFade()
	{
		if (m_FadeDir == 0) return gxFalse;

		return gxTrue;
	}

	void SetDialog( gxBool bEndless , gxChar *pCaption , gxChar *pString , ... );

	void DispVolumeMeter()
	{
		m_sVolumeMeterDispCnt = 2*FRAME_PER_SECOND;
	}

	void Action();
	void Draw();

	virtual void SeqMain(){};
	virtual void SeqDraw(){};

	SINGLETON_DECLARE( gxUIManager );

private:

	void drawVolumeMeter();

	//gxBool m_bNowLoadingOn    = gxFalse;
	//gxBool m_bNowNetworking   = gxFalse;
	//gxBool m_bNowBluetoothing = gxFalse;

	//Fade
	Sint32   m_FadeDir    = -1;
	Float32  m_fFadeSpeed = -0.01f;
	Float32  m_fFadeNow   = 1.0f;
	Uint32   m_FadeARGB   = 0xFF010101;
	FadeType m_FadeType   = eFadeTypeNormal;

	Float32 m_fNowLoading         = 0.0f;
	Float32 m_fNowSaving          = 0.0f;
	Float32 m_fNowHttpConnecting  = 0.0f;
	Float32 m_fNowBlueToothEnable = 0.0f;

	Float32 m_fDialogTime   = 0.0f;
	Float32 m_fDialogAddTime= 0.0f;
	gxChar  m_CaptionString[256]={0};
	gxChar  m_MessageString[256]={0};


	Sint32 m_LoadDispCnt = 0;
	Sint32 m_SaveDispCnt = 0;
	Sint32 m_HttpDispCnt = 0;
	Sint32 m_BToothDispCnt = 0;

	Sint32 m_sVolumeMeterDispCnt = 0;
	Float32 m_fDispVolumeRatio = 0.0f;
	gxBool m_bStart = gxFalse;

	Sint32 m_sInitTimer = 120;

};

#endif
