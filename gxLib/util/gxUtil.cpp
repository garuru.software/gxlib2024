//--------------------------------------------------
//
// gxTexdManager.h
// CRC値や、円と円、線と線の当たり判定など
// よく使うコードをまとめています
//
//--------------------------------------------------

#include <gxLib.h>
#include "../gx.h"
#include "../gxNetworkManager.h"
#include "../gxFileManager.h"
#include "../gxPadManager.h"
#include "../gxTexManager.h"
#include "../gxDebug.h"
#include "CFileCsv.h"
#include "CFileTarga.h"
#include "CFileTxt.h"


gxUtil::CKeyBoard gxUtil::m_KeyBoard;
gxUtil::CGamePad   gxUtil::m_GamePad[PLAYER_MAX];
gxUtil::CTouch    gxUtil::m_Touch[GX_TOUCH_MAX];

extern const gxChar *JoyStringTbl[];
extern const gxChar *KeyStringTbl[];
gxBool gxUtil::s_LoadTask = gxFalse;

void gxUtil::Update()
{
	gxUtil::Touch()->Update();

}

Uint64 gxUtil::atox( gxChar *p )
{
	int max=0;
	Uint64 n=0,ret = 0;

	if (p[0] == '0' && (p[1] == 'x' || p[1] == 'X')) max = 2;

	for(int i= gxUtil::StrLen(p) - 1,j=0 ;i>= max;i--,j++)
	{	
		switch(p[i]){
		 case '0':  n=0;	break;
		 case '1':  n=1;	break;
		 case '2':  n=2;	break;
		 case '3':  n=3;	break;
		 case '4':  n=4;	break;
		 case '5':  n=5;	break;
		 case '6':  n=6;	break;
		 case '7':  n=7;	break;
		 case '8':  n=8;	break;
		 case '9':  n=9;	break;

		 case 'A':  case 'a':   n=10;   break;
		 case 'B':  case 'b':   n=11;   break;
		 case 'C':  case 'c':   n=12;   break;
		 case 'D':  case 'd':   n=13;   break;
		 case 'E':  case 'e':   n=14;   break;
		 case 'F':  case 'f':   n=15;   break;
		 default:
			return 0;
		}

		ret |= (n<<(4*j));
	}

	return ret;
}

gxBool gxUtil::GetFileNameWithoutPath( gxChar * in , gxChar *out)
{
	//ファイル名だけを取り出す

	for(int i=gxUtil::StrLen(in);i>0;i--)
	{
		if( in[i]=='\\' || in[i]=='/')
		{
			sprintf( out,"%s",&in[i+1]);
			return gxTrue;
		}
	}

	sprintf(out, "%s", in);
	return gxFalse;
}


gxBool gxUtil::GetPath( gxChar *in , gxChar *out )
{
	//ファイルのパスだけを取り出す

	for(int i=gxUtil::StrLen(in);i>0;i--)
	{
		if( in[i]=='\\' || in[i]=='/')
		{
			sprintf(out,"%s",&in[0]);
			out[i] = 0x00;
			return gxTrue;
		}
	}
	return gxFalse;
}

gxBool gxUtil::GetExt( gxChar *in , gxChar* out )
{
	//拡張子を得る

	for(int i=gxUtil::StrLen(in);i>=0;i--)
	{
		if(in[i]=='.')
		{
			sprintf(out,"%s",&in[i]);
			//out[i] = 0x00;
			return gxTrue;
		}
	}

	return gxFalse;

}

gxBool gxUtil::GetFileNameWithoutExt( gxChar *in , gxChar *out)
{
	//拡張子をカットする

	for(int i=gxUtil::StrLen(in);i>0;i--)
	{
		if(in[i]=='.')
		{
			sprintf(out,"%s",in);
			out[i] = 0x00;
			return gxTrue;
		}
	}

	out[0] = 0x00;
	return gxFalse;
}




Float32 gxUtil::GetTargetRotation( Float32 target_angle , Float32 my_angle )
{
	//自分の向いている方向に対して
	//ターゲット角度を－１８０～０～１８０の角度で返す
	Float32 sabun;

	while(my_angle>360){
		my_angle-=360;
	}
	while(my_angle<0){
		my_angle+=360;
	}
	while(target_angle>360){
		target_angle-=360;
	}
	while(target_angle<0){
		target_angle+=360;
	}

	sabun = target_angle-my_angle;

	if(sabun<-180){
		sabun = 360+sabun;
	}else if(sabun>180){
		sabun = (180-(sabun-180))*-1;
	}

	return sabun;

}


void gxUtil::StrUpper( gxChar* pStr )
{
	size_t len = gxUtil::StrLen(pStr);

	for(size_t ii=0; ii<len; ii++ )
	{
		if (pStr[ii] < 0x80)
		{
			pStr[ii] = toupper(pStr[ii]);
		}
	}
}


void gxUtil::StrUpper(std::string &str)
{
	std::vector<Uint32> str32 = gxUtil::UTF8toUTF32(str);

	gxUtil::StrUpper32(str32);

	str = gxUtil::UTF32toUTF8(str32);
}

std::vector<Uint32> gxUtil::StrUpper32( std::vector<Uint32> &str )
{
	std::vector<Uint32> ret;

	size_t len = str.size();

	for(size_t ii=0; ii<len; ii++ )
	{
		switch( str[ii] ){
		case L'a':	ret.push_back( L'A');	break;
		case L'b':	ret.push_back( L'B');	break;
		case L'c':	ret.push_back( L'C');	break;
		case L'd':	ret.push_back( L'D');	break;
		case L'e':	ret.push_back( L'E');	break;
		case L'f':	ret.push_back( L'F');	break;
		case L'g':	ret.push_back( L'G');	break;
		case L'h':	ret.push_back( L'H');	break;
		case L'i':	ret.push_back( L'I');	break;
		case L'j':	ret.push_back( L'J');	break;
		case L'k':	ret.push_back( L'K');	break;
		case L'l':	ret.push_back( L'L');	break;
		case L'm':	ret.push_back( L'M');	break;
		case L'n':	ret.push_back( L'N');	break;
		case L'o':	ret.push_back( L'O');	break;
		case L'p':	ret.push_back( L'P');	break;
		case L'q':	ret.push_back( L'Q');	break;
		case L'r':	ret.push_back( L'R');	break;
		case L's':	ret.push_back( L'S');	break;
		case L't':	ret.push_back( L'T');	break;
		case L'u':	ret.push_back( L'U');	break;
		case L'v':	ret.push_back( L'V');	break;
		case L'w':	ret.push_back( L'W');	break;
		case L'x':	ret.push_back( L'X');	break;
		case L'y':	ret.push_back( L'Y');	break;
		case L'z':	ret.push_back( L'Z');	break;
		case L'ａ':	ret.push_back( L'Ａ');	break;
		case L'ｂ':	ret.push_back( L'Ｂ');	break;
		case L'ｃ':	ret.push_back( L'Ｃ');	break;
		case L'ｄ':	ret.push_back( L'Ｄ');	break;
		case L'ｅ':	ret.push_back( L'Ｅ');	break;
		case L'ｆ':	ret.push_back( L'Ｆ');	break;
		case L'ｇ':	ret.push_back( L'Ｇ');	break;
		case L'ｈ':	ret.push_back( L'Ｈ');	break;
		case L'ｉ':	ret.push_back( L'Ｉ');	break;
		case L'ｊ':	ret.push_back( L'Ｊ');	break;
		case L'ｋ':	ret.push_back( L'Ｋ');	break;
		case L'ｌ':	ret.push_back( L'Ｌ');	break;
		case L'ｍ':	ret.push_back( L'Ｍ');	break;
		case L'ｎ':	ret.push_back( L'Ｎ');	break;
		case L'ｏ':	ret.push_back( L'Ｏ');	break;
		case L'ｐ':	ret.push_back( L'Ｐ');	break;
		case L'ｑ':	ret.push_back( L'Ｑ');	break;
		case L'ｒ':	ret.push_back( L'Ｒ');	break;
		case L'ｓ':	ret.push_back( L'Ｓ');	break;
		case L'ｔ':	ret.push_back( L'Ｔ');	break;
		case L'ｕ':	ret.push_back( L'Ｕ');	break;
		case L'ｖ':	ret.push_back( L'Ｖ');	break;
		case L'ｗ':	ret.push_back( L'Ｗ');	break;
		case L'ｘ':	ret.push_back( L'Ｘ');	break;
		case L'ｙ':	ret.push_back( L'Ｙ');	break;
		case L'ｚ':	ret.push_back( L'Ｚ');	break;
		default:
			ret.push_back(str[ii]);
			break;
		}
	}

	str = ret;
	return ret;
}

void gxUtil::StrLower(std::string& str)
{
	std::vector<Uint32> str32 = gxUtil::UTF8toUTF32(str);

	gxUtil::StrLower32(str32);

	str = gxUtil::UTF32toUTF8(str32);
}

std::vector<Uint32> gxUtil::StrLower32(std::vector<Uint32>& str)
{
	std::vector<Uint32> ret;

	size_t len = str.size();

	for (size_t ii = 0; ii < len; ii++)
	{
		switch (str[ii]) {
		case L'A':	ret.push_back(L'a');	break;
		case L'B':	ret.push_back(L'b');	break;
		case L'C':	ret.push_back(L'c');	break;
		case L'D':	ret.push_back(L'd');	break;
		case L'E':	ret.push_back(L'e');	break;
		case L'F':	ret.push_back(L'f');	break;
		case L'G':	ret.push_back(L'g');	break;
		case L'H':	ret.push_back(L'h');	break;
		case L'I':	ret.push_back(L'i');	break;
		case L'J':	ret.push_back(L'j');	break;
		case L'K':	ret.push_back(L'k');	break;
		case L'L':	ret.push_back(L'l');	break;
		case L'M':	ret.push_back(L'm');	break;
		case L'N':	ret.push_back(L'n');	break;
		case L'O':	ret.push_back(L'o');	break;
		case L'P':	ret.push_back(L'p');	break;
		case L'Q':	ret.push_back(L'q');	break;
		case L'R':	ret.push_back(L'r');	break;
		case L'S':	ret.push_back(L's');	break;
		case L'T':	ret.push_back(L't');	break;
		case L'U':	ret.push_back(L'u');	break;
		case L'V':	ret.push_back(L'v');	break;
		case L'W':	ret.push_back(L'w');	break;
		case L'X':	ret.push_back(L'x');	break;
		case L'Y':	ret.push_back(L'y');	break;
		case L'Z':	ret.push_back(L'z');	break;
		case L'Ａ':	ret.push_back(L'ａ');	break;
		case L'Ｂ':	ret.push_back(L'ｂ');	break;
		case L'Ｃ':	ret.push_back(L'ｃ');	break;
		case L'Ｄ':	ret.push_back(L'ｄ');	break;
		case L'Ｅ':	ret.push_back(L'ｅ');	break;
		case L'Ｆ':	ret.push_back(L'ｆ');	break;
		case L'Ｇ':	ret.push_back(L'ｇ');	break;
		case L'Ｈ':	ret.push_back(L'ｈ');	break;
		case L'Ｉ':	ret.push_back(L'ｉ');	break;
		case L'Ｊ':	ret.push_back(L'ｊ');	break;
		case L'Ｋ':	ret.push_back(L'ｋ');	break;
		case L'Ｌ':	ret.push_back(L'ｌ');	break;
		case L'Ｍ':	ret.push_back(L'ｍ');	break;
		case L'Ｎ':	ret.push_back(L'ｎ');	break;
		case L'Ｏ':	ret.push_back(L'ｏ');	break;
		case L'Ｐ':	ret.push_back(L'ｐ');	break;
		case L'Ｑ':	ret.push_back(L'ｑ');	break;
		case L'Ｒ':	ret.push_back(L'ｒ');	break;
		case L'Ｓ':	ret.push_back(L'ｓ');	break;
		case L'Ｔ':	ret.push_back(L'ｔ');	break;
		case L'Ｕ':	ret.push_back(L'ｕ');	break;
		case L'Ｖ':	ret.push_back(L'ｖ');	break;
		case L'Ｗ':	ret.push_back(L'ｗ');	break;
		case L'Ｘ':	ret.push_back(L'ｘ');	break;
		case L'Ｙ':	ret.push_back(L'ｙ');	break;
		case L'Ｚ':	ret.push_back(L'ｚ');	break;
		default:
			ret.push_back(str[ii]);
			break;
		}
	}
	str = ret;
	return ret;
}

#if 1
Uint32 gxUtil::StrLen( const gxChar* pStr )
{
	size_t len = strlen(pStr);

	return Uint32(len);
}


void gxUtil::MemSet( void* pMem ,Uint8 val , size_t sz )
{
#if 1
	std::memset(pMem, val, sz);
#else
	Uint8 *pMem8 = (Uint8*)pMem;

	for(size_t ii=0; ii<sz; ii++ )
	{
		pMem8[ii] = val;
	}
#endif
}


void gxUtil::MemCpy(void* pDst , void* pSrc , size_t sz )
{
#if 1
	std::memcpy(pDst, pSrc, sz);
#else
	Uint32 *pMemSrc = (Uint32*)pSrc;
	Uint32 *pMemDst = (Uint32*)pDst;

	for(size_t ii=0; ii<sz/4; ii++ )
	{
		pMemDst[ii] = pMemSrc[ii];
	}

	Uint8 *pMemSrc8 = (Uint8*)pSrc;
	Uint8 *pMemDst8 = (Uint8*)pDst;

	size_t sz2 = (sz/4)*4;
	for(size_t ii=sz2; ii<sz; ii++ )
	{
		pMemDst8[ii] = pMemSrc8[ii];
	}
#endif
}
#endif

void gxUtil::DrawBox( gxRect *pRect , Sint32 prio , gxBool bFill , Uint32 atr  , Uint32 argb  , Float32 fFat )
{
	gxLib::DrawBox( pRect->x1 , pRect->y1 , pRect->x2 , pRect->y2 , prio , bFill , atr , argb , fFat );

}

void gxUtil::DrawLine( gxVector2 p1 , gxVector2 p2 , Sint32  prio , Uint32 atr  , Uint32 argb  , Float32 fFat )
{
	gxLib::DrawLine( p1.x,p1.y,p2.x,p2.y,prio , atr,argb , fFat);
}


//多角形を描画します
Uint32 gxUtil::DrawPolygon(
		Sint32 num,
		gxVector2 *pXY,
		gxVector2 *pUV,
		Uint32 *argb,
		Sint32 prio,
		Uint32 atr ,
		Sint32 page,
		gxBool bFill )
{

	//三角形分割

	//

	return 0;
}



Uint32 gxUtil::PutFreeSprite(
	Sint32 x,Sint32 y,Sint32 prio,
	gxVector2 *pXY,
	Sint32 tpg,
	Sint32 u,Sint32 v , Sint32 w , Sint32 h,
	Sint32 divNum,
	Uint32 atr,
	Uint32  argb,
	Float32 fRot,
	Float32 fx,
	Float32 fy
	)
{
	gxVector2 pos[4];

	Sint32 div = divNum+1;

	if( div < 2  ) div = 2;
	if( div > 16 ) div = 16;

	gxBool bDebug = gxFalse;
	Uint32 debARGB = 0xff00ff00;

	for( Sint32 ii=0; ii<4; ii++ )
	{
		pos[ii].x = pXY[ii].x;
		pos[ii].y = pXY[ii].y;

		pos[ii].x *= fx;
		pos[ii].y *= fy;

		gxUtil::RotationPoint( &pos[ii] , fRot );

		if( bDebug )
		{
			gxLib::DrawBox( pos[ii].x -3 +x, pos[ii].y -3+y , pos[ii].x +3 +x, pos[ii].y +3 +y, MAX_PRIORITY_NUM,gxTrue , ATR_DFLT , 0xffffffff );
		}
	}

	Sint32 max = div*div;

	gxVector2* pMat = new gxVector2[max];
	gxVector2* pTx  = new gxVector2[max];

	//四辺の分割座標を確定

	for( Sint32 ii=0;ii<div;ii++ )
	{
		pMat[ ii ].x = pos[0].x + 1.0f * ii * ( pos[1].x - pos[0].x ) /( div-1 );
		pMat[ ii ].y = pos[0].y + 1.0f * ii * ( pos[1].y - pos[0].y ) /( div-1 );

		pMat[ (div-1)*div+ii ].x = pos[3].x + 1.0f * ii * ( pos[2].x - pos[3].x ) /( div-1 );
		pMat[ (div-1)*div+ii ].y = pos[3].y + 1.0f * ii * ( pos[2].y - pos[3].y ) /( div-1 );

		pMat[ ii*div ].x = pos[0].x + 1.0f * ii * ( pos[3].x - pos[0].x ) /( div -1 );
		pMat[ ii*div ].y = pos[0].y + 1.0f * ii * ( pos[3].y - pos[0].y ) /( div -1 );

		pMat[ ii*div+(div-1) ].x = pos[1].x + 1.0f * ii * ( pos[2].x - pos[1].x ) /( div -1 );
		pMat[ ii*div+(div-1) ].y = pos[1].y + 1.0f * ii * ( pos[2].y - pos[1].y ) /( div -1 );

		if( bDebug )
		{
			gxLib::DrawPoint( pMat[ ii             ].x+x , pMat[ ii                ].y+y , prio+1 ,ATR_DFLT , debARGB );
			gxLib::DrawPoint( pMat[ (div-1)*div+ii ].x+x , pMat[(div - 1)*div + ii ].y+y , prio+1 ,ATR_DFLT , debARGB );
			gxLib::DrawPoint( pMat[ ii*div         ].x+x , pMat[ ii*div ].y+y            , prio+1 ,ATR_DFLT , debARGB );
			gxLib::DrawPoint( pMat[ ii*div+(div-1) ].x+x , pMat[ ii*div+(div-1) ].y+y    , prio+1 ,ATR_DFLT , debARGB );
		}
	}

	//中の補完座標を策定

	for( Sint32 ii=1;ii<div-1;ii++ )
	{
		for( Sint32 xx=1;xx<div-1;xx++ )
		{
			Float32 x1 = pMat[ ii*div+0 ].x;
			Float32 y1 = pMat[ ii*div+0 ].y;
			Float32 x2 = pMat[ ii*div+(div-1) ].x;
			Float32 y2 = pMat[ ii*div+(div-1) ].y;

			pMat[ ii*div+xx ].x = x1 + xx * (x2-x1)/(div-1);
			pMat[ ii*div+xx ].y = y1 + xx * (y2-y1)/(div-1);

			if( bDebug )
			{
				gxLib::DrawPoint( pMat[ ii*div+xx ].x+x , pMat[ ii*div+xx ].y+y , prio+1 ,ATR_DFLT , debARGB );
			}
		}
	}

	for( Sint32 yy=0;yy<div;yy++)
	{
		for( Sint32 xx=0;xx<div;xx++)
		{
			pTx[ yy*div + xx ].x = u + 1.0f * xx *  w  / (div-1);
			pTx[ yy*div + xx ].y = v + 1.0f * yy *  h  / (div-1);
		}
	}


	for( Sint32 yy=0;yy<div-1;yy++)
	{
		for( Sint32 xx=0;xx<div-1;xx++)
		{
			Sint32 p1,p2,p3,p4;
			p1 = yy*div + xx;
			p2 = yy*div + xx+1;
			p3 = yy*div + xx+1+div;
			p4 = (yy+1)*div + xx;


			if( bDebug )
			{
				gxLib::DrawTriangle(
						pMat[ p1 ].x+x , pMat[ p1 ].y+y ,
						pMat[ p2 ].x+x , pMat[ p2 ].y+y ,
						pMat[ p4 ].x+x , pMat[ p4 ].y+y ,
						prio+1 , gxFalse,ATR_DFLT , debARGB );

				gxLib::DrawTriangle(
						pMat[ p2 ].x+x , pMat[ p2 ].y+y ,
						pMat[ p4 ].x+x , pMat[ p4 ].y+y ,
						pMat[ p3 ].x+x , pMat[ p3 ].y+y ,
						prio+1 ,gxFalse,ATR_DFLT , debARGB );
			}

			gxLib::PutTriangle(
					pMat[ p1 ].x+x , pMat[ p1 ].y+y ,	pTx[p1].x , pTx[p1].y,
					pMat[ p2 ].x+x , pMat[ p2 ].y+y ,	pTx[p2].x , pTx[p2].y,
					pMat[ p4 ].x+x , pMat[ p4 ].y+y ,	pTx[p4].x , pTx[p4].y,
					prio , tpg,
					atr , argb );

			gxLib::PutTriangle(
					pMat[ p2 ].x+x , pMat[ p2 ].y+y ,	pTx[p2].x , pTx[p2].y,
					pMat[ p4 ].x+x , pMat[ p4 ].y+y ,	pTx[p4].x , pTx[p4].y,
					pMat[ p3 ].x+x , pMat[ p3 ].y+y ,	pTx[p3].x , pTx[p3].y,
					prio , tpg,
					atr , argb );

		}
	}


	SAFE_DELETES( pMat );
	SAFE_DELETES( pTx );

	return 0;
}


void gxUtil::DrawSonicBoom( Sint32 x , Sint32 y , Sint32 prio , Float32 fRadius  , Float32 fWidth , Float32 fDistPow, Uint32 tpg , Uint32 atr,Uint32 argb )
{
	//Float32 fRadius・・・・円の半径
	//Float32 fWidth ・・・・縁の大きさ（ゼロの場合は中心からFill）
	//Float32 fDistortion・・歪みの強度（0～1.0f）
	//Sint32 x , Sint32 y , Sint32 prio	・・・中心位置
	//Uint32 tpg ・・・使用するテクスチャページ（通常はTEXPAGE_CAPTURE）, Uint32 atr , Uint32 argb , Float32 fRadius , Float32 fWidth , Float32 fDistortion )

	fDistPow = CLAMP(fDistPow, 0.0f, 1.0f);
	fWidth   = CLAMP(fWidth, 0.0f, fRadius*2);

	Float32 fRot = 0.0f;
	Sint32 div = 32;
	Float32 fDistortion = 45 * fDistPow;

	gxRect  *pRectV = new gxRect[div];;
	gxRect  *pRectUV = new gxRect[div];;

	Float32 fx1,fy1;
	Float32 fx2,fy2;

	for( Sint32 ii=0; ii<div;ii++ )
	{
		fRot = (360.0f / div)*ii;
		fx1 = gxUtil::Cos( fRot );
		fy1 = gxUtil::Sin( fRot );
		fx2 = gxUtil::Cos( fRot+fDistortion );
		fy2 = gxUtil::Sin( fRot+fDistortion );

		if( fWidth <= 0.0f )
		{
			pRectV[ii].x1 = 0.0f;
			pRectV[ii].y1 = 0.0f;
			pRectV[ii].x2 = fx1*fRadius;
			pRectV[ii].y2 = fy1*fRadius;

			pRectUV[ii].x1 = fx1 * fRadius*0.1f;
			pRectUV[ii].y1 = fx1 * fRadius*0.1f;
			pRectUV[ii].x2 = fx1 * fRadius*1.1f;
			pRectUV[ii].y2 = fy1 * fRadius * 1.1f;
		}
		else
		{
			pRectV[ii].x1 = fx1 * (fRadius - fWidth / 2);
			pRectV[ii].y1 = fy1 * (fRadius - fWidth / 2);
			pRectV[ii].x2 = fx1 * (fRadius + fWidth / 2);
			pRectV[ii].y2 = fy1 * (fRadius + fWidth / 2);

			pRectUV[ii].x1 = fx2 * (fRadius - fWidth / 2);
			pRectUV[ii].y1 = fy2 * (fRadius - fWidth / 2);
			pRectUV[ii].x2 = fx2 * (fRadius + fWidth / 2);
			pRectUV[ii].y2 = fy2 * (fRadius + fWidth / 2);
		}
	}


	Sint32 x1,y1,x2,y2;
	Sint32 x3,y3,x4,y4;

	Sint32 u1,v1,u2,v2;
	Sint32 u3,v3,u4,v4;
	for( Sint32 ii=0; ii<div;ii++ )
	{
		Sint32 n1 = ii;
		Sint32 n2 = (ii+1)%div;
		Sint32 n3 = (ii+2)%div;

		//Sint32 m1 = ii;
		//Sint32 m2 = (ii+1)%div;
		//Sint32 m3 = (ii+2)%div;

		if (fWidth <= 0.0f)
		{
			x1 = pRectV[n1].x1 + x;
			y1 = pRectV[n1].y1 + y;
			x2 = pRectV[n1].x2 + x;
			y2 = pRectV[n1].y2 + y;
			x3 = pRectV[n2].x1 + x;
			y3 = pRectV[n2].y1 + y;
			x4 = pRectV[n2].x2 + x;
			y4 = pRectV[n2].y2 + y;


			u1 = pRectUV[n1].x1 + x;
			v1 = pRectUV[n1].y1 + y;
			u2 = pRectUV[n1].x2 + x;
			v2 = pRectUV[n1].y2 + y;
			u3 = pRectUV[n2].x1 + x;
			v3 = pRectUV[n2].y1 + y;
			u4 = pRectUV[n2].x2 + x;
			v4 = pRectUV[n2].y2 + y;
		}
		else
		{
			x1 = pRectV[n1].x1 + x;
			y1 = pRectV[n1].y1 + y;
			x2 = pRectV[n1].x2 + x;
			y2 = pRectV[n1].y2 + y;
			x3 = pRectV[n2].x1 + x;
			y3 = pRectV[n2].y1 + y;
			x4 = pRectV[n2].x2 + x;
			y4 = pRectV[n2].y2 + y;


			u1 = pRectUV[n1].x1 + x;
			v1 = pRectUV[n1].y1 + y;
			u2 = pRectV[n1].x2 + x;
			v2 = pRectV[n1].y2 + y;
			u3 = pRectUV[n2].x1 + x;
			v3 = pRectUV[n2].y1 + y;
			u4 = pRectV[n2].x2 + x;
			v4 = pRectV[n2].y2 + y;

		}


		gxLib::PutTriangle(
			x1,y1,u1,v1,
			x2,y2,u2,v2,
			x3,y3,u3,v3,
			prio,
			tpg,
			atr , argb );

		gxLib::PutTriangle(
			x2,y2,u2,v2,
			x4,y4,u4,v4,
			x3,y3,u3,v3,
			prio,
			tpg,
			atr , argb );
	}
	SAFE_DELETES(pRectV);
	SAFE_DELETES(pRectUV);
}


Sint32 gxUtil::OpenWebFile( std::string URL , gxChar *pPostData, std::function<gxBool(gxBool bSuccess, uint8_t* p, size_t size)> func)
{
	Sint32 index = 0;
	size_t len = 0;
	if (pPostData)
	{
		len = strlen(pPostData);
	}

	index = gxNetworkManager::GetInstance()->OpenURL( URL.c_str() , pPostData, len , func);

	return index;
}

gxBool gxUtil::CloseWebFile()
{
	auto list = gxNetworkManager::GetInstance()->GetHTTPInfoIDList();

	if (list.size() == 0) return gxFalse;

	for (Sint32 ii = 0; ii < list.size(); ii++)
	{
		gxNetworkManager::GetInstance()->CloseURL(list[ii]);
	}

	return gxTrue;
}

gxBool gxUtil::CloseWebFile(Uint32 uIndex)
{
	gxNetworkManager::GetInstance()->CloseURL(uIndex);

	return gxTrue;
}

gxBool gxUtil::IsDownloadWebFile(Float32* fRatio )
{
	auto list = gxNetworkManager::GetInstance()->GetHTTPInfoIDList();

	if (list.size() == 0) return gxFalse;

	Float32 ftotal = 0.0f;
	Float32 ratio  = 0.0f;

	gxBool bDownoading = gxFalse;

	for (Sint32 ii = 0; ii < list.size(); ii++)
	{
		if (gxUtil::IsDownloadWebFile(list[ii], &ratio))
		{
			bDownoading = gxTrue;
		}
		ftotal += ratio;
	}

	if (fRatio)
	{
		*fRatio = ftotal / list.size();
	}

	return bDownoading;
}

gxBool gxUtil::IsDownloadWebFile( Uint32 index , Float32 *pRatio )
{
	gxNetworkManager::StHTTP *http;

	http = gxNetworkManager::GetInstance()->GetHTTPInfo(index );

	if (http == nullptr) return gxFalse;

	if( pRatio )
	{
		size_t max = http->uFileSize;
		size_t now = http->uReadFileSize;

		if (now == 0 || max == 0)
		{
			*pRatio = 0.0f;
		}
		else if (now == max)
		{
			*pRatio = 100.0f;
		}
		else
		{
			size_t ratio = 10000 * now / max;

			Float32 fRatio = ratio / 100.0f;

			*pRatio = fRatio;
		}
	}

	if ( http->IsNowLoading() ) 
	{
        return gxTrue;
	}

	return gxFalse;
}


gxBool gxUtil::IsDownloadSucceeded(Uint32 index)
{
	gxNetworkManager::StHTTP *http;

	http = gxNetworkManager::GetInstance()->GetHTTPInfo(index);
	if (!http) return gxFalse;

	size_t max = http->uFileSize;
	size_t now = http->uReadFileSize;

	if (!http->IsNowLoading())
	{
		if (now >= max && max > 0 )
		{
			return gxTrue;
		}
	}

	return gxFalse;
}


Uint8* gxUtil::GetDownloadWebFile( Uint32 uIndex , size_t* uSize )
{
	gxNetworkManager::StHTTP *http;
	http = gxNetworkManager::GetInstance()->GetHTTPInfo( uIndex  );

	if (http == nullptr) return nullptr;
	*uSize = http->uFileSize;

	return http->pData;
}

gxBool gxUtil::IsSameEXT( gxChar *in1 , gxChar* in2 )
{
	//同じ拡張子か？
	gxChar ext1[256] = {0};
	gxChar ext2[256] = {0};

	GetExt( in1 ,ext1 );
	GetExt( in2 ,ext2 );

	StrUpper( ext1 );
	StrUpper( ext2 );

	if( strcmp( ext1 , ext2 ) == 0 )
	{
		return gxTrue;
	}

	return gxFalse;
}


Sint32 gxUtil::LoadFile( std::function<void( Sint32 id , Uint8 *pData , size_t uSize )>func , gxChar* pFileName , ... )
{
    gxChar temp[1024];

    va_list app;
    va_start(app, pFileName);
    vsprintf(temp, pFileName, app);
    va_end(app);

	std::string tmpstr = temp;

    Sint32 id = gxFileManager::GetInstance()->LoadReq(tmpstr.c_str(), ESTORAGE_LOCATION::AUTO, [tmpstr,pFileName, func](Sint32 id)
	{
		Uint8* pData = gxFileManager::GetInstance()->GetFileAddr( id );
		size_t uSize = gxFileManager::GetInstance()->GetFileSize( id );

		if( func )
		{
			func(id,pData,uSize);
		}

		if( pData == nullptr || uSize == 0 )
		{
			GX_DEBUGLOG("File Not Found! .. gxUtil::LoadFileASync(\"%s\");", tmpstr.c_str());
			return;
		}
	});

	//Sint32 index = gxFileManager::GetInstance()->LoadReq( pFileName , location );	//card&rom&dvd捜索
	//GX_DEBUGLOG("gxUtil::LoadFile(\"%s\");",pFileName );

	return id;
}

gxBool gxUtil::IsLoadFile( Sint32 id , Uint8 **pData , size_t* pLength )
{
	if( !gxFileManager::GetInstance()->IsLoadEnd( id ) )
	{
		return gxFalse;
	}

	*pLength = gxFileManager::GetInstance()->GetFileSize(id);
	*pData   = gxFileManager::GetInstance()->GetFileAddr(id);

	gxFileManager::GetInstance()->Clear( id );

	if( *pData == NULL )
	{
		GX_DEBUGLOG("[gxLib::Error]gxUtil::LoadFile　読み込み失敗(\"%d\");" , id );
		return gxFalse;
	}

	return gxTrue;
}

gxBool gxUtil::IsNowLoading()
{
	if (gxUtil::s_LoadTask) return gxTrue;

	return gxFileManager::GetInstance()->IsLoadTaskExist();
}

//テクスチャをファイルからマスターテクスチャへ読み込みます
gxBool gxUtil::LoadTexture ( Uint32 texPage , Uint32 ox , Uint32 oy , gxChar* pFileName, ... )	//Uint32 colorKey ,Uint32 ox , Uint32 oy )
{
	gxChar temp[1024];

	va_list app;
	va_start(app, pFileName);
	vsprintf(temp, pFileName, app);
	va_end(app);

	std::string tmpstr = temp;

		gxUtil::LoadFile( [tmpstr, texPage, ox, oy](Sint32 id, Uint8* pData, size_t uSize)
		{
			gxUtil::s_LoadTask = gxTrue;
			if (pData == nullptr || uSize == 0)
			{
				GX_DEBUGLOG("Failed! .. gxUtil::LoadTextureASync(\"%s\");", tmpstr.c_str());
				gxUtil::s_LoadTask = gxFalse;
				return;
			}

			gxLib::ReadTexture(texPage, pData, uSize, 0x00000000, ox, oy);
			GX_DEBUGLOG("gxUtil::LoadTextureASync(\"%s\");", tmpstr.c_str());
			SAFE_DELETES(pData);
			gxUtil::s_LoadTask = gxFalse;

		},temp);

	return gxTrue;
}

struct FileReq {
	Uint32 uIndex = 0;
	Uint8 *pData;
	Uint32 uSize;
};

void* SoundUpload( void *p )
{
	FileReq *q = (FileReq*)p;

	gxLib::ReadAudio(q->uIndex, q->pData, q->uSize);

	return nullptr;
}

//サウンドファイルをファイルから指定バンクに読み込みます
Sint32 gxUtil::LoadAudio( Uint32 uIndex , gxChar* pFileName , ... )
{
	gxChar temp[1024];

	va_list app;
	va_start(app, pFileName);
	vsprintf(temp, pFileName, app);
	va_end(app);

	std::string tmpstr = temp;

	int id = gxUtil::LoadFile( [tmpstr, uIndex](Sint32 id, Uint8* pData, size_t uSize)
		{
			gxUtil::s_LoadTask = gxTrue;
			if (pData == nullptr || uSize == 0)
			{
				GX_DEBUGLOG("Failed! .. gxUtil::LoadAudioASync(\"%s\");", tmpstr.c_str());
				gxUtil::s_LoadTask = gxFalse;
				return;
			}

/*
static FileReq req;
			req.pData = pData;
			req.uIndex = uIndex;
			req.uSize = uSize;
			gxLib::CreateThread( SoundUpload , (void*)&req );
*/
			gxLib::ReadAudio(uIndex, pData, uSize);
			GX_DEBUGLOG("gxUtil::LoadAudioASync(\"%s\");", tmpstr.c_str());
			SAFE_DELETES(pData);
			gxUtil::s_LoadTask = gxFalse;

		},temp);


	return id;
}

gxBool gxUtil::IsLoadComplete(int id)
{
	if (id == -1)
	{
		if (gxFileManager::GetInstance()->IsLoadTaskExist()) return gxFalse;
		if (gxUtil::s_LoadTask) return  gxFalse;
	}
	else
	{
		if (!gxFileManager::GetInstance()->IsLoadEnd(id))
		{
			return gxFalse;
		}
	}

	return gxTrue;
}


/*
std::string gxUtil::GetUpperPath(std::string path)
{
	size_t sz = path.size();

	std::string path2 = path;
	char buf[512];

	sprintf(buf, "%s", path.c_str());

	for (size_t ii = sz-1-1; ii > 0; ii--)
	{
		if (buf[ii] == '\\' || buf[ii] == '/')
		{
			buf[ii+1] = 0x00;
			break;
		}
	}

	path2 = buf;

	return path2;

}
std::string gxUtil::GetRelativePath(gxChar* pCurrentPath, gxChar* path)
{
	std::string path1 = pCurrentPath;
	std::string path2 = path;

	size_t num = path2.find( path1 );

	if (num == std::string::npos)
	{
		return path;
	}

	char buf[512];
	sprintf(buf, "%s", path);

	size_t sz = strlen(pCurrentPath);
	if (buf[num + sz] == '\\' || buf[num + sz] == '/')
	{
		sz++;
	}
	return &buf[num + sz];
}
*/

std::string gxUtil::GetMD5(Uint8 *pData, size_t sz)
{
	if (sz == 0x00000000)
	{
		return "00000000000000000000000000000000";
	}

	MD5 md5;
	md5.update( (uint8_t*)pData, (uint8_t*)((uint8_t*)pData + (sz)));

	std::string str;
	md5.hex_digest(str);
	return str;
	//return md5.GetHash();
}


gxUtil::CGamePad::CGamePad()
{
	for( Sint32 ii=0; ii<PLAYER_MAX; ii++ )
	{
		m_GamePad[ii].id = ii;
	}
}


Float32 gxUtil::CGamePad::GetRotationAngle(Float32 *pRot )
{
    Float32 fDist = gxUtil::Distance( gxLib::Joy(0)->lx , gxLib::Joy(0)->ly );

    gxBool bPush = gxFalse;
    
    Float32 fRot = 0.0f;

    if( fDist > 0.2f )
    {
        bPush = gxTrue;
        fRot = gxUtil::Angle( gxLib::Joy(0)->lx , gxLib::Joy(0)->ly );
        fRot -= 90.0f;
        NORMALIZE(fRot);
    }
    else
    {
        bPush = gxTrue;
        switch(gxLib::Joy(0)->psh&(JOY_U|JOY_D|JOY_R|JOY_L)){
        case JOY_U:
            fRot = 0.0f;
            break;
        case JOY_U|JOY_R:
            fRot = 45.0f;
            break;
        case JOY_R:
            fRot = 90.0f;
            break;
        case JOY_R|JOY_D:
            fRot = 135.0f;
            break;
        case JOY_D:
            fRot = 180.0f;
            break;
        case JOY_D|JOY_L:
            fRot = 225.0f;
            break;
        case JOY_L:
            fRot = 270.0f;
            break;
        case JOY_U|JOY_L:
            fRot = 315.0f;
            break;
        default:
            bPush = gxFalse;
            break;
        }
    }
    
    *pRot = fRot;

    if( !bPush ) return gxFalse;

    return gxTrue;
}

gxBool gxUtil::CGamePad::IsTrigger( Uint32 key )
{
	if( gxLib::Joy(id)->trg&key ) return gxTrue;
	return gxFalse;
}

gxBool gxUtil::CGamePad::IsDoubleClick(Uint32 key)
{
	if (gxLib::Joy(id)->dcl & key) return gxTrue;
	return gxFalse;
}

gxBool gxUtil::CGamePad::IsLongTap(Uint32 key)
{
	if (gxLib::Joy(id)->tap & key) return gxTrue;
	return gxFalse;
}

gxBool gxUtil::CGamePad::IsPush( Uint32 key )
{
	if( gxLib::Joy(id)->psh&key ) return gxTrue;
	return gxFalse;
}

gxBool gxUtil::CGamePad::IsRepeat( Uint32 key )
{
	if( gxLib::Joy(id)->rep&key ) return gxTrue;
	return gxFalse;
}

gxBool gxUtil::CGamePad::IsRelease( Uint32 key )
{
	if( gxLib::Joy(id)->rls&key ) return gxTrue;
	return gxFalse;
}


gxVector2* gxUtil::CGamePad::GetLeftStick()
{
	m_LeftStick.x = gxLib::Joy(id)->lx;
	m_LeftStick.y = gxLib::Joy(id)->ly;
	return &m_LeftStick;
}

gxVector2* gxUtil::CGamePad::GetRightStick()
{
	m_LeftStick.x = gxLib::Joy(id)->rx;
	m_LeftStick.y = gxLib::Joy(id)->ry;

	return &m_LeftStick;
}


const gxChar* gxUtil::CGamePad::GetString( EJoyBit btn )
{
	//ボタン名を返す

	Uint8 index = gxPadManager::GetInstance()->CnvJoytoIndex( btn );

	return JoyStringTbl[index];
}

gxUtil::CKeyBoard* gxUtil::KeyBoard()
{
	//キーボード構造体を返す

	return &gxUtil::m_KeyBoard;
}


gxUtil::CGamePad* gxUtil::GamePad( Uint32 player )
{
	//プレイヤーを指定したジョイパッド構造体を返す

	return &m_GamePad[player];
}

gxUtil::CTouch* gxUtil::Touch(Uint32 id)
{
	m_Touch[id].id = id;

	return &m_Touch[id];
}

gxBool gxUtil::CKeyBoard::IsTrigger( Uint32 key )
{
	//コントローラーのボタンが押された瞬間か？

	if( gxLib::KeyBoard(key)&enStatTrig ) return gxTrue;
	return gxFalse;
}

gxBool gxUtil::CKeyBoard::IsPush( Uint32 key )
{
	//コントローラーのボタンは押しっぱなしか？

	if( gxLib::KeyBoard(key)&enStatPush ) return gxTrue;
	return gxFalse;
}

gxBool gxUtil::CKeyBoard::IsRepeat( Uint32 key )
{
	//コントローラーのボタンがリピートしているか？

	if( gxLib::KeyBoard(key)&enStatRepeat ) return gxTrue;
	return gxFalse;
}

gxBool gxUtil::CKeyBoard::IsRelease( Uint32 key )
{
	//コントローラーのボタンが離されたか？

	if( gxLib::KeyBoard(key)&enStatRelease ) return gxTrue;
	return gxFalse;
}

std::vector<gxKey::KeyType> gxUtil::CKeyBoard::GetInputKey(Uint32 atr )
{
	//キーボードが押されたことを検出する
	std::vector<gxKey::KeyType> ret;

	for(Uint32 ii=0; ii<gxKey::KEYMAX; ii++ )
	{
		if( gxLib::KeyBoard( (gxKey::KeyType)ii )&atr )
		{
			ret.push_back( (gxKey::KeyType)ii );
		}
	}
	return ret;
}


const gxChar* gxUtil::CKeyBoard::GetKeyString( gxKey::KeyType key )
{
	//gxKeyに対応したキーボードの文字を返す

	if( key >= gxKey::KEYMAX ) return "-----";

	return KeyStringTbl[key];
};


gxBool gxUtil::CTouch::IsTrigger()
{

	Uint32 tbl[]={	MOUSE_L,	MOUSE_R,	MOUSE_M };

	return (gxLib::Joy(0)->trg&tbl[id])? gxTrue : gxFalse;
}

gxBool gxUtil::CTouch::IsPush()
{
	Uint32 tbl[]={	MOUSE_L,	MOUSE_R,	MOUSE_M };
	Uint32 psh = gxLib::Joy(0)->psh;

	return (psh&tbl[id])? gxTrue : gxFalse;
}

gxBool gxUtil::CTouch::IsRepeat()
{
	Uint32 tbl[]={	MOUSE_L,	MOUSE_R,	MOUSE_M };

	return (gxLib::Joy(0)->rep&tbl[id]) != 0;
}

gxBool gxUtil::CTouch::IsRelease()
{
	Uint32 tbl[]={	MOUSE_L,	MOUSE_R,	MOUSE_M };

	//if ((gxLib::Joy(0)->rls & tbl[id]))
	//{
	//	int nnn = 0;
	//	nnn++;
	//}
	return (gxLib::Joy(0)->rls&tbl[id]) != 0;
}

gxBool gxUtil::CTouch::IsDoubleTap()
{
	return (gxLib::Joy(0)->dcl & MOUSE_L) != 0;
}

gxBool gxUtil::CTouch::IsLongTap()
{
	return ((gxLib::Joy(0)->tap & MOUSE_L)!=0);
}

gxBool gxUtil::CTouch::IsFlick(Uint32 key)
{
	if( IsRelease() )
	{
		Sint32 x1 = gxLib::Touch(id)->sx;
		Sint32 y1 = gxLib::Touch(id)->sy;
		Sint32 x2 = gxLib::Touch(id)->ex;
		Sint32 y2 = gxLib::Touch(id)->ey;

		Float32 fRot  = gxUtil::Angle( x2-x1 , y2-y1 );
		Float32 fDist = gxUtil::Distance( x2-x1 , y2-y1 );

		if( fDist >= 32.0f )
		{
			Uint32 joy = 0x00;
			NORMALIZE(fRot);

			if( fRot > (360.0f-45) || fRot < ( 45.0f) )      joy |= JOY_R;
			if( fRot > ( 90.0f-45) && fRot < ( 90.0f+45.0) ) joy |= JOY_D;
			if( fRot > (180.0f-45) && fRot < (180.0f+45.0) ) joy |= JOY_L;
			if( fRot > (270.0f-45) && fRot < (270.0f+45.0) ) joy |= JOY_U;

			return (joy&key)? gxTrue : gxFalse;
		}
	}

	return gxFalse;
}

Float32 gxUtil::CTouch::GetFlickRotation()
{
	if( IsRelease() )
	{
		Sint32 x1 = gxLib::Touch(id)->sx;
		Sint32 y1 = gxLib::Touch(id)->sy;
		Sint32 x2 = gxLib::Touch(id)->ex;
		Sint32 y2 = gxLib::Touch(id)->ey;

		Float32 fRot  = gxUtil::Angle( x2-x1 , y2-y1 );
		Float32 fDist = gxUtil::Distance( x2-x1 , y2-y1 );

		if( fDist >= 100.0f )
		{
			NORMALIZE(fRot);
			return fRot;
		}
	}

	return 0.0f;
}


gxVector2 gxUtil::CTouch::GetPosition()
{
	m_Touch[id].m_Position.x = gxLib::Touch(id)->x;
	m_Touch[id].m_Position.y = gxLib::Touch(id)->y;

	return m_Position;
}

gxBool gxUtil::CTouch::IsPinchIn()
{
	if (m_fPinchScale < 0)
	{
		return gxTrue;

	}
	return gxFalse;
}

gxBool gxUtil::CTouch::IsPinchOut()
{
	if (m_fPinchScale > 0)
	{
		return gxTrue;
	}
	return gxFalse;
}


void gxUtil::CTouch::Update()
{
	Float32 whl = gxLib::Joy(0)->whl;

	m_fPinchScale = 0.0f;

	if (whl != 0)
	{
		//ホイール操作があった場合はこっちを優先
		if (whl < 0)
		{
			m_fPinchScale = -1.0f;
		}
		else
		{
			m_fPinchScale = 1.0f;
		}
	}
	else
	{
		Float32 f = GetZoomRatio();
		if (f < 1.0f)
		{
			m_fPinchScale = -1.0f;
		}
		else if (f > 1.0f)
		{
			m_fPinchScale = 1.0f;
		}
	}
}

Float32 gxUtil::CTouch::GetZoomRatio()
{
	//ズーム操作（ピンチイン、ピンチアウト）したか？
	//pRatioはズーム率

	//ツーフィンガー操作があった場合はピンチイン、アウトを検証する

	Sint32 x1 = gxLib::Touch(0)->sx;
	Sint32 y1 = gxLib::Touch(0)->sy;
	Sint32 x3 = gxLib::Touch(0)->ex;
	Sint32 y3 = gxLib::Touch(0)->ey;

	Sint32 x2 = gxLib::Touch(1)->sx;
	Sint32 y2 = gxLib::Touch(1)->sy;
	Sint32 x4 = gxLib::Touch(1)->ex;
	Sint32 y4 = gxLib::Touch(1)->ey;

	if( (gxLib::Touch(1)->stat&enStatPush) == 0x00)
	{
		return 1.0f;
	}

	Float32 fDist1 = gxUtil::Distance( x1 ,y1 , x2 , y2 );
	Float32 fDist2 = gxUtil::Distance( x3 ,y3 , x4 , y4 );

/*
    gxLib::Printf( 32, 32+32*0,100,ATR_STR_LEFT , 0xffffffff , "%d,%d" , x1,y1 );
	gxLib::Printf( 32, 32+32*1,100,ATR_STR_LEFT , 0xffffffff , "%d,%d" , x3,y3 );
	gxLib::Printf( 32, 32+32*2,100,ATR_STR_LEFT , 0xffffffff , "%d,%d" , x2,y2 );
	gxLib::Printf( 32, 32+32*3,100,ATR_STR_LEFT , 0xffffffff , "%d,%d" , x4,y4 );
*/
	if (fDist1 <= 0.0f) return 0.0f;

	m_fPinchScale = (fDist2 / fDist1);

	return m_fPinchScale;
}


gxBool gxUtil::CTouch::IsTap( Sint32 x , Sint32 y , Sint32 w , Sint32 h)
{
	if( !IsPush() ) return gxFalse;

	gxVector2 p = GetPosition();

	if( p.x < x || p.x >= (x+w)) return gxFalse;
	if( p.y < y || p.y >= (y+h)) return gxFalse;

	return gxTrue;
}


gxBool gxUtil::CTouch::IsTap( Sint32 x , Sint32 y , Float length )
{
	if (!IsPush()) return gxFalse;

	gxVector2 p = GetPosition();

	if( Distance( p.x -x , p.y-y ) >= length ) return gxFalse;

	return gxTrue;
}

void gxUtil::RoundButton::Update()
{
	gxBool bPush    = gxFalse;
	gxBool bRelease = gxFalse;
	gxBool bDecide  = gxFalse;
	gxBool bInRange = gxFalse;

/*
	Sint32 gw,gh,sw,sh;
	gxLib::GetDeviceResolution( &gw , &gh , &sw , &sh );

	gxPos pos;
	pos.x = m_Pos.x / WINDOW_W;
	pos.y = m_Pos.y / WINDOW_H;

	Sint32 mx,my;
*/

	for( Sint32 ii=0; ii<3; ii++ )
	{
		gxBool bInRound = gxFalse;

		gxPos pos1,pos2;

		pos1.x = gxUtil::Touch(ii)->GetPosition().x;
		pos1.y = gxUtil::Touch(ii)->GetPosition().y;

		pos2.x = m_Pos.x;
		pos2.y = m_Pos.y;

		if( m_Pos.z >= MAX_PRIORITY_NUM )
		{
			gxLib::GetNativePosition( &pos1.x , &pos1.y );
			//gxLib::GetNativePosition( &pos2.x , &pos2.y );
		}

		if (Distance(pos1.x - pos2.x ,  pos1.y - pos2.y) <= m_fRadius)
		{
			bInRound = gxTrue;
		}

		if (m_PushCnt == 0)
		{
			if (bInRound && gxUtil::Touch(ii)->IsTrigger() )
			{
				//初めて押して範囲内ならトリガー成功
				bPush = gxTrue;
				break;
			}
		}
		else
		{
			if (bInRound && gxUtil::Touch(ii)->IsPush())
			{
				//ボタンが押されている間は押しっぱなしは継続
				bPush = gxTrue;

				bInRange = gxTrue;

			}
			else if (gxUtil::Touch(ii)->IsRelease())
			{
				//離したことを検出
				//離したときに範囲内にいるか？
				if (bInRound)
				{
					bDecide = gxTrue;
				}
			}
		}

	}
	if( bPush )
	{
		m_PushCnt ++;
	}
	else
	{
		if (bDecide)
		{
			m_PushCnt = -1;
		}
		else
		{
			m_PushCnt = 0;
		}
	}

}

void gxUtil::RoundButton::Draw()
{
	gxVector2 offset;

	if (m_PushCnt > 0)
	{
		Float32 fRadius = m_fRadius*1.0f;
		offset.x = 8;
		offset.y = 8;
		//gxLib::DrawCircle(m_Pos.x, m_Pos.y, m_Pos.z, ATR_DFLT, m_ARGB, fRadius , 3.0f );
		gxLib::DrawPoint(m_Pos.x+offset.x, m_Pos.y + offset.y , m_Pos.z, ATR_DFLT, m_ARGB, fRadius);
	}
	else
	{
		gxLib::DrawPoint(m_Pos.x, m_Pos.y, m_Pos.z, ATR_DFLT, m_ARGB, m_fRadius);
	}
	
	if( m_Name != "" )
	{
       if( m_Centering == 1 )
       {
           gxLib::Printf( m_Pos.x+m_fRadius + offset.x, m_Pos.y + offset.y, m_Pos.z, ATR_STR_LEFT| ATR_STR_MID, 0xffffffff, "%s" , m_Name.c_str() );
       }
       else if( m_Centering == -1 )
        {
            gxLib::Printf( m_Pos.x-m_fRadius + offset.x, m_Pos.y + offset.y, m_Pos.z, ATR_STR_RIGHT| ATR_STR_MID, 0xffffffff, "%s" , m_Name.c_str() );
        }
        else
        {
            gxLib::Printf( m_Pos.x + offset.x, m_Pos.y + offset.y, m_Pos.z, ATR_STR_CENTER| ATR_STR_MID, 0xffffffff, "%s" , m_Name.c_str() );
        }

	}
}


void gxUtil::FileNames::set( gxChar *pStr , ... )
{
	gxChar *pTemp = new gxChar[1024];
	
	va_list app;
	va_start(app, pStr);
	vsprintf(pTemp, pStr, app);
	va_end(app);

	init(pTemp);

	SAFE_DELETES(pTemp);
}

void gxUtil::FileNames::init(const std::string& str )
{
	//strのファイル名を受け取り各パス情報を初期化する

	clear();

	size_t sz32 = 0;
	
	std::vector<Uint32> buf32 = CDeviceManager::UTF8toUTF32( (gxChar*)str.c_str(), &sz32);

	//「￥￥」表記を「/」に置き換える

	for (Sint32 ii = 0; ii < buf32.size(); ii++)
	{
		if (buf32[ii] == '\\' )
		{
			buf32[ii] = '/';
		}
	}

	// ２重のファイル区切り「//」を検出して「/」にただす


	std::vector<Uint32> temp;
	//for (Sint32 ii = 0; ii <buf32.size(); ii++)
	//{
	//	temp.push_back(buf32[ii]);
	//
	//	if (ii < buf32.size()-2 && buf32[ii] == '/' && buf32[ii+1] == '/')
	//	{
	//		ii++;
	//		continue;
	//	}
	//}
	for (Sint32 ii = 0; ii < buf32.size()-1; ii++)
	{
		if ( buf32[ii] == '/' && buf32[ii + 1] == '/')
		{
			buf32[ii] = 0xFF;
		}
	}
	for (Sint32 ii = 0; ii < buf32.size(); ii++)
	{
		if (buf32[ii] == 0xFF)
		{
			continue;
		}
		temp.push_back(buf32[ii]);
	}

	buf32 = temp;

//	size_t sz = 0;
	FullName = gxUtil::UTF32toUTF8( buf32 );

	gxBool bFoundEXT = gxFalse;
	Sint32 extIndex = -1;

	for (Sint32 ii = Sint32(buf32.size()-1); ii >= 0; ii-=1 )
	{
		Uint32 word = buf32[ii];
		if (word == '.' && !bFoundEXT)
		{
			//拡張子を発見した ".ext"の形式で返す

			bFoundEXT = gxTrue;

			extIndex = ii;
//			size_t sz1 = 0;
			std::vector<Uint32> tmp;
			std::copy(buf32.begin() + extIndex, buf32.begin() + buf32.size(), std::back_inserter(tmp));

			Ext = gxUtil::UTF32toUTF8(tmp);
			StrUpper((gxChar*)Ext.c_str());

		}
		else if (word == '/' || ii == 0 )
		{
			//区切りを発見したのでファイル名を記録する
			Sint32 offset = 1;
			if (ii == 0) offset = 0;
//			size_t sz1 = 0;

			std::vector<Uint32> tmp;
			std::copy(buf32.begin() + ii+offset, buf32.begin() + buf32.size(), std::back_inserter(tmp));
			FileName = gxUtil::UTF32toUTF8(tmp);

			//拡張子なしのファイル名も記録しておく
			if (bFoundEXT)
			{
				std::vector<Uint32> tmp;

				int extsize = int(buf32.size() - extIndex);
				std::copy(buf32.begin(), buf32.begin() + buf32.size() - extsize, std::back_inserter(tmp));

				FileNameWithoutExt = gxUtil::UTF32toUTF8(tmp);
			}

			if (ii != 0)
			{
				//先頭でなければファイルパスも記録する(パスは「/」で終わるように返す)
				auto tmp = buf32;
				size_t sz2 = 0;
				tmp.resize(ii+1);
				FilePath = CDeviceManager::UTF32toUTF8(tmp, &sz2);
			}
			break;
		}
	}

	if (FileName.empty() )
	{
		//ファイル名が入っていない場合はフルネームを記録しておく
		FileName.resize(1);
		FileName[0] = 0x00;

		//当然拡張子もなし
		Ext.resize(1);
		Ext[0] = 0x00;

		//拡張子抜きの本体名もなし
		FileNameWithoutExt.resize(1);
		FileNameWithoutExt[0] = 0x00;
	}

	if (FilePath.empty())
	{
		//ファイルパスが存在しないときは空文字をセットしておく
		FilePath.resize(1);
		FilePath[0] = 0x00;
	}

	if (Ext.empty())
	{
		//拡張子のみの場合はあり得る
		Ext.resize(1);
		Ext[0] = 0x00;
	}

	if (FileNameWithoutExt.empty() && (!FileName.empty()) )
	{
		//拡張子なしが設定されていない場合は、ここで設定する(拡張子ごと？)
		//size_t strLen = FileName.size();
		FileNameWithoutExt = FileName;
	}
}

void gxUtil::FileNames::clear()
{
	//登録情報をいったんクリアする

	FullName.clear();
	FileName.clear();
	FilePath.clear();

	Ext.clear();

	FileNameWithoutExt.clear();
}

void gxUtil::FileNames::ChangeExt(const std::string& ext)
{
	//拡張子を変更する

	std::string path = FilePath;
	path += '/';
	path += FileNameWithoutExt;
	path += ext;

	init(path);
}

void gxUtil::FileNames::ConvPathtoBackSlash()
{

	auto name = CTxt::Replace(FullName, "\\", "/");
	init(name);
}

void gxUtil::FileNames::ChangePath(const std::string& path)
{
	//パスを変更する

	std::string newpath = path;
	newpath += FileName;

	init( newpath.c_str());

}
void gxUtil::FileNames::ChangeDirUp()
{
	//上位のディレクトリにパスを変更する

	std::string path = FilePath;

	size_t sz32;
	//Uint32* pFileName32 = (Uint32*)CDeviceManager::UTF8toUTF32((gxChar*)path.c_str(), &sz32);	//SAFE
	//sz32 = sz32 / 4;
	std::vector<Uint32> fname32 = CDeviceManager::UTF8toUTF32((gxChar*)path.c_str(), &sz32);	//SAFE
	sz32 = fname32.size();


	if (fname32[sz32 - 1] == '/')
	{
		for (size_t ii = sz32 - 2; ii > 0; ii--)
		{
			if (fname32[ii] == '/')
			{
				size_t sz32_2;
				fname32[ii] = 0x00;
				std::string moji = CDeviceManager::UTF32toUTF8(fname32, &sz32_2);
				gxChar* pRet = &moji[0];
				std::string fileName = pRet;
				fileName += "/";

				FilePath = fileName;
				break;
			}
		}
	}
}

void gxUtil::FileNames::ChangeDirDown(const std::string& dir)
{
	//下層のディレクトリにパスを変更する

	std::string path = FilePath;

	size_t sz32;
	std::vector<Uint32> pFileName32 = CDeviceManager::UTF8toUTF32((gxChar*)path.c_str(), &sz32);	//SAFE
	sz32 = sz32 / 4;

	if (pFileName32[sz32 - 1] == '/')
	{
		for (size_t ii = sz32 - 2; ii > 0; ii--)
		{
			if (pFileName32[ii] == '/')
			{
				size_t sz32_2;
				pFileName32[ii] = 0x00;
				std::string moji = CDeviceManager::UTF32toUTF8(pFileName32, &sz32_2);
				gxChar* pRet = &moji[0];
				std::string fileName = pRet;
				fileName += "/";
				fileName += dir;
				fileName += "/";

				FilePath = fileName;
				break;
			}
		}
	}

}

gxBool gxUtil::FileNames::IsIncludePath(const std::string& path)
{
	//指定されたパスが含まれているか確認

	std::string path1 = path;
	std::string path2 = path.substr(path.length() - 1);

	if (path2 != "\\" && path2 != "/")
	{
		path1 += "/";
	}

	if (FullName.find(path1) != std::string::npos)
	{
		//末尾に/つきのパスが見つかった
		return gxTrue;
	}

	//if (FullName.find(path) == std::string::npos)
	//{
	//	return gxFalse;
	//}

	if (FullName == path)
	{
		//
		return gxTrue;
	}

	if (path1 == "\\" || path2 == "/")
	{
		//末尾に/がついていた場合消して比較してみる
		path1.pop_back();
		if (FullName == path1)
		{
			return gxTrue;
		}

	}

	return gxFalse;
}


Uint32 gxUtil::FileNames::GetSubDirectoryDepth(const std::string& basePath)
{
	std::string path1 = GetRelativeFileName(basePath);
	//std::string path2 = GetRelativeFilePath(basePath);

	//FileNames fName = path1;
	//path1 = fName.GetFilePathWithOutSlash();
	int count = std::count(path1.begin(), path1.end(), '/');

	if( CTxt::FindFront(path1,":/") >= 0 )
	{
		count--;
	}

	return count;
}


std::string gxUtil::FileNames::GetFilePathWithOutSlash()
{
	std::string ret = FilePath;

	size_t sz = ret.size();

	if (ret.back() == '\\')
	{
		ret.back() = 0;
		ret.resize(sz - 1);
	}
	else if (ret.back() == '/')
	{
		ret.back() = 0;
		ret.resize(sz - 1);
	}

	return ret;
}

std::string gxUtil::FileNames::GetRelativeFilePath(const std::string& _basepath)
{
	//相対パスを求める

	std::string basePath = _basepath;

	basePath = pathNormalize(basePath);

	auto fullNameArray = CTxt::Separate(FilePath, "/");
	auto basePathArray = CTxt::Separate(basePath, "/");

	int length = (int)basePathArray.size();
	if (fullNameArray.size() < basePathArray.size()) length = (int)basePathArray.size();

	int sameDepth = 0;

	for (int ii = 0; ii < length; ii++)
	{
		if (fullNameArray[ii] == basePathArray[ii])
		{
			sameDepth++;
		}
		else
		{
			return FilePath;
		}
	}

	std::string ret;

	for (int ii = sameDepth; ii < fullNameArray.size(); ii++)
	{
		ret += fullNameArray[ii];
		//if (ii < fullNameArray.size())
		{
			ret += "/";
		}
	}

	//size_t sz = 0;
	//std::vector<Uint32> p1 = CDeviceManager::UTF8toUTF32((gxChar*)basePath.c_str(), &sz);
	//std::vector<Uint32> p2 = CDeviceManager::UTF8toUTF32((gxChar*)FullName.c_str(), &sz);
	//
	//sz = p2.size();
	//if (p1.size() < p2.size()) sz = p1.size();
	//
	//Sint32 FoundIndex = -1;
	//std::vector<Uint32> tmp;
	//for (Sint32 ii = 0; ii < sz; ii++)
	//{
	//
	//	if (p1[ii] == p2[ii])
	//	{
	//		tmp.push_back(p1[ii]);
	//		continue;
	//	}
	//
	//	//異なる場所を発見
	//	FoundIndex = ii;
	//	break;
	//}
	//
	//if (FoundIndex == 0)
	//{
	//	return FullName;
	//}
	//
	//if (tmp[tmp.size() - 1] != '/')
	//{
	//	return FullName;
	//}
	//
	//std::string ret = gxUtil::UTF32toUTF8(tmp);

	return ret;
}

std::string gxUtil::FileNames::pathNormalize(const std::string& _basepath)
{
	//ベースパスが「/」で終わっていないときに付け足すことにする

	std::string Path = CTxt::Replace(_basepath, "\\", "/");

	if (Path.size() > 0)
	{
		switch (Path[_basepath.size() - 1]) {
		case '\\':
			Path[_basepath.size() - 1] = '/';
			break;
		case '/':
			break;
		default:
			Path += '/';
			break;
		}
	}

	return Path;
}

std::string gxUtil::FileNames::GetRelativeFileName(const std::string& _basepath )
{
	//相対パスのファイル名を取得する

	//相対パスを求める

	std::string basePath = _basepath;

	basePath = pathNormalize(basePath);

	size_t length = 0;

	auto fullNameArray = CTxt::Separate(FullName, "/");
	auto basePathArray = CTxt::Separate(basePath, "/");

	length = basePathArray.size();
	if (fullNameArray.size() < basePathArray.size()) length = basePathArray.size();

	int sameDepth = 0;

	for (size_t ii = 0; ii < length; ii++)
	{
		if (fullNameArray[ii] == basePathArray[ii])
		{
			sameDepth++;
		}
		else
		{
			//ベースパスが存在しない場合はフルパスで返す
			return FullName;
		}
	}

	std::string ret;

	for (int ii = sameDepth; ii < fullNameArray.size(); ii++)
	{
		ret += fullNameArray[ii];
		if (ii < fullNameArray.size()-1)
		{
			ret += "/";
		}
	}
	if (IsDirectory())
	{
		ret += "/";
	}
	//std::string RelName = GetRelativeFilePath(_basePath);
	//std::string name = "";
	//
	//if (RelName.empty())
	//{
	//	//全く合致していなかった
	//	return FileName;
	//}
	//
	//size_t sz = 0;
	//std::vector<Uint32> p1 = CDeviceManager::UTF8toUTF32( (gxChar*)RelName.c_str() , &sz );
	//std::vector<Uint32> p2 = CDeviceManager::UTF8toUTF32((gxChar*)FullName.c_str(), &sz);
	//
	//sz = p1.size();
	//if (p1.size() < p2.size()) sz = p2.size();
	//
	//Sint32 FoundIndex = -1;
	//std::vector<Uint32> tmp;
	//for (Sint32 ii = 0; ii < sz; ii++)
	//{
	//	if (ii >= p1.size())
	//	{
	//		tmp.push_back(p2[ii]);
	//	}
	//	else if (p1[ii] == p2[ii])
	//	{
	//		continue;
	//	}
	//
	//	//異なる場所を発見
	//	if (FoundIndex == -1) FoundIndex = ii;
	//
	//}
	//
	//if (FoundIndex == -1)
	//{
	//	return FullName;
	//}
	//
	//std::string ret = gxUtil::UTF32toUTF8(tmp);
	//
	return ret;
}


std::vector<std::string> gxUtil::GetCSVArray( std::string csvValue )
{
	//１行のCSVデータを配列に分解する

	std::vector<std::string> ret;

	CCsv csv;

	csv.Read( (uint8_t*)csvValue.c_str(), csvValue.size());

	Sint32 w = csv.GetWidth();

	std::string str;
	for (Sint32 ii = 0; ii < w; ii++)
	{
		csv.GetCell( ii, 0, str);
		if (str[0]) ret.push_back(str);
	}

	return ret;
}



std::string gxUtil::SJIStoUTF8( std::string str )
{
	//SJIS->UTF-8へ変換（Windowsのみ）

	size_t size;

	std::string textUTF8 = CDeviceManager::SJIStoUTF8 ((char*)str.c_str() , &size );

	return textUTF8;
}

std::string gxUtil::UTF8toSJIS(std::string str)
{
	size_t size;
	std::string  sjis = CDeviceManager::UTF8toSJIS((char*)str.c_str(), &size);

	return sjis;
}


std::string gxUtil::UTF32toUTF8(std::vector<Uint32> str32)
{
	size_t uSize = 0;

	str32.push_back(0x00);	//ターミネータを挿入しておく

	std::string ret;
	ret = CDeviceManager::UTF32toUTF8(str32, &uSize);

	return ret;
}


std::vector<Uint32> gxUtil::UTF8toUTF32( std::string str )
{
	//UTF-8->UTF32 へ変換
	size_t size = 0;
	std::vector<Uint32> data = CDeviceManager::UTF8toUTF32((char*)str.c_str(), &size);

	return data;
}

std::vector<std::string> gxUtil::GetDirectoryList( std::string rootDir, std::string filter )
{
	auto list = CDeviceManager::GetDirList(rootDir);

	if (filter != "")
	{
		list = FindFileFromUTF8FileList(list, filter);
	}
		
	return list;
}

std::vector<std::string> gxUtil::GetFileList(std::string rootDir, std::string filter)
{
	//ディレクトリからファイルのみ（ディレクトリを除いたもの）のリストを作成する

	auto list0 = CDeviceManager::GetDirList(rootDir);

	std::vector<std::string> list;

	for (int ii = 0; ii < list0.size(); ii++)
	{
		gxUtil::FileNames fileNames = list0[ii];
		if (fileNames.IsDirectory()) continue;

		list.push_back(list0[ii]);
	}

	if (filter != "")
	{
		list = FindFileFromUTF8FileList(list, filter);
	}

	return list;
}

std::vector<std::string> gxUtil::MakeRelativeDirectoryList(std::vector<std::string> list ,std::string rootDir, std::string filter)
{
	//auto list = CDeviceManager::GetDirList(rootDir);
	std::vector<std::string> retlist;
	for (size_t ii = 0; ii < list.size(); ii++)
	{
		FileNames fName = list[ii];
		retlist.push_back(fName.GetRelativeFileName(rootDir));
	}

	if (filter != "")
	{
		retlist = FindFileFromUTF8FileList(retlist, filter);
	}

	return retlist;
}

gxBool gxUtil::ReadTexture2(Uint32 texPage, Uint8* pBuffer, size_t Size, Uint32 colorKey, Uint32 ox, Uint32 oy, Sint32* w, Sint32* h)
{
	//実際のメモリ転送を行わずにポインタの付替えだけでTGAを構築する
	//pBufferの中身もこの関数を抜けた時点で消滅するので消滅させないように繻子栄

	CFileTarga tga;
	//	tga.ReadFile( pBuffer , Size , colorKey );	//一時的なメモリ転送が多すぎるので。。
	tga.ApplyTgaImage(pBuffer, Size);

	if (w) *w = tga.GetWidth();
	if (h) *h = tga.GetHeight();

	gxTexManager::GetInstance()->addTexture(texPage, &tga, colorKey, ox, oy);

	tga.setTexelImage(nullptr);

		//ポインタの付け替えを行ったためこのあとpBufferのメモリは消滅していたのをやめた

	return gxTrue;
}


void gxUtil::DrawFPS( Sint32 x,Sint32 y , Sint32 prio )
{
	gxLib::Printf( x,y,    MAX_PRIORITY_NUM, ATR_DFLT, 0xffffffff, "Tick   %d / %d", gxDebug::GetInstance()->GetFPS() , FRAME_PER_SECOND);
	gxLib::Printf( x,y+32, MAX_PRIORITY_NUM, ATR_DFLT, 0xffffffff, "Render %d / %d", gxDebug::GetInstance()->GetRenderFPS(),FRAME_PER_SECOND);
}


/*
bool StrMatch32(Uint32* ptn, Uint32* str)
{
	switch (*ptn)
	{
	case 0x00000000:
		return (0x00000000 == *str) ? true : false;
	case '*':
		return (StrMatch32(ptn + 1, str) || ((0x00000000 != *str) && StrMatch32(ptn, str + 1))) ? true : false;
	case '?':
		return (0x00000000 != *str) && StrMatch32(ptn + 1, str + 1) ? true : false;
	default:
		return (*ptn == *str) && StrMatch32(ptn + 1, str + 1) ? true : false;
	}
}//strmatch
*/


std::vector<std::string> gxUtil::FindFileFromUTF8FileList(std::vector<std::string>& utf8list, std::string filter)
{
	std::vector<std::string> retlist;

	for (int ii = 0; ii < utf8list.size(); ii++)
	{
		if( CTxt::IsMatch(utf8list[ii],filter) )
		{
			retlist.push_back(utf8list[ii]);
		}
	}

/*
	gxUtil::StrUpper(searchwords);


	if (searchwords == "") return utf8list;
	//std::string findwords = "*" + words + "*";	//あいまいに
	std::string findwords = searchwords;

	std::vector<Uint32> utf32words = gxUtil::UTF8toUTF32(findwords);
	utf32words.push_back(0);

	std::string fileName;

	for (int ii = 0; ii < utf8list.size(); ii++)
	{
		fileName = utf8list[ii];
		gxUtil::StrUpper(fileName);	//大文字小文字を区別しない

		std::vector<Uint32> utf32strs = gxUtil::UTF8toUTF32(fileName);
		utf32strs.push_back(0);

		if (StrMatch32(&utf32words[0], &utf32strs[0]))
		{
			retlist.push_back(utf8list[ii]);
		}
	}

*/
	return retlist;
}

Sint64 gxUtil::AdjustEaseInOut64(Sint64 now_gold, Sint64 tgt_gold, Sint64 ratio)
{
	Sint64 oldGold = now_gold;

	Sint64 add = (tgt_gold - now_gold) / ratio;

	if (add == 0)
	{
		//増加率がなかった場合
		now_gold += (tgt_gold - now_gold) / 2;

		if (now_gold < tgt_gold)
		{
			now_gold++;
		}
		else if (now_gold > tgt_gold)
		{
			now_gold--;

		}
	}
	else
	{
		now_gold += (tgt_gold - now_gold) / ratio;
	}

	if (oldGold == now_gold)
	{
		//計算前のGOLDと変わらないくらい微小な差分の場合はもう合わせる
		now_gold = tgt_gold;
	}

	return now_gold;

}

Float64 gxUtil::AdjustEaseInOut(Float64 now_gold, Float64 tgt_gold , Float64 ratio)
{
	Float64 oldGold = now_gold;
	if (now_gold < tgt_gold)
	{
		now_gold += (tgt_gold - now_gold)/ratio;
		now_gold += 0.05f;
		if (now_gold >= tgt_gold)
		{
			now_gold = tgt_gold;
		}
	}
	else if (now_gold > tgt_gold)
	{
		now_gold += (tgt_gold - now_gold) / ratio;;
		now_gold -= 0.05f;
		if (now_gold <= tgt_gold)
		{
			now_gold = tgt_gold;
		}
	}

	if (oldGold == now_gold)
	{
		//計算前のGOLDと変わらないくらい微小な差分の場合はもう合わせる
		now_gold = tgt_gold;
	}

	return now_gold;
}

void SampleHTTP()
{
	Uint8* pData;
	size_t uSize;

	static Sint32 seq = 0;
	static Sint32 index;

	if (seq == 0)
	{
		index = gxUtil::OpenWebFile("http://garuru.co.jp/api/basic.php?ope=GetIP");
		seq++;
	}

	if (gxUtil::IsDownloadWebFile(index))
	{
		pData = gxUtil::GetDownloadWebFile(index, &uSize);
	}
}

