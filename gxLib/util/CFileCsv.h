﻿//------------------------------------------
//
// CCSV ver.2.0
//
//------------------------------------------

#ifndef _CCSV_H_
#define _CCSV_H_
#include <string>
#include <map>
#include <vector>
//#include "extra/CXML.h"
//#include "extra/CJSON.h"

#define KAIGYO_CODE LF	//"\r\n"
class CXML;
class CJson;

class CCsv {

private:

	enum{
		CSV_DATA_NONE = 0x00,				//データが無いことを示す
		CSV_CELL_BUF_MAX = 1024,			//１セルあたり持てるデータの上限値(bytes)
		CSV_LINE_BUF_MAX = CSV_CELL_BUF_MAX*1024,			//１ラインあたり持てるデータの上限
	};

public:

	bool operator == ( CCsv &csv )
	{
		return Compare( csv );
	}

	bool operator != ( CCsv &csv )
	{
		return !Compare( csv );
	}

	bool operator >= ( CCsv &csv )
	{
		size_t sz1 = GetHeight()*GetWidth();
		size_t sz2 = csv.GetHeight()*csv.GetWidth();

		return (sz1 >= sz2)? true : false;
	}

	bool operator > ( CCsv &csv )
	{
		size_t sz1 = GetHeight()*GetWidth();
		size_t sz2 = csv.GetHeight()*csv.GetWidth();

		return (sz1 > sz2)? true : false;
	}

	bool operator <= ( CCsv &csv )
	{
		size_t sz1 = GetHeight()*GetWidth();
		size_t sz2 = csv.GetHeight()*csv.GetWidth();

		return (sz1 <= sz2)? true : false;
	}

	bool operator < ( CCsv &csv )
	{
		size_t sz1 = GetHeight()*GetWidth();
		size_t sz2 = csv.GetHeight()*csv.GetWidth();

		return (sz1 < sz2)? true : false;
	}

	CCsv& operator |= ( CCsv &csv )
	{
		size_t szx = (GetWidth()  > csv.GetWidth() )? GetWidth()  : csv.GetWidth();
		size_t szy = (GetHeight() > csv.GetHeight())? GetHeight() : csv.GetHeight();

		char *p1,*p2;

		for( int y=0; y<szy; y++ )
		{
			for( int x=0; x<szx; x++ )
			{
				p1 = this-> GetCell( x , y );
				p2 = csv.GetCell( x , y );
				
				if( p1[0] == CSV_DATA_NONE && p2[0] != CSV_DATA_NONE )
				{
					SetCell( x , y , p2 );
				}
			}
		}

		return *this;
	}


	CCsv();
	~CCsv();

	void Clear()
	{
		//現在のドキュメントを白紙にする

		//CSVの最大列
		m_RangeW = 1;

		//CSVの最大行
		m_RangeH = 1;

		m_pCellMap.clear();
		m_pTempBuf.clear();

		//セル取得時にコメントを削除するフラグ
		//m_bIgnoreComment = gxFalse;
		//m_bCleanDquote   = gxFalse;

		SAFE_DELETES(m_pCsvArray);
		SAFE_DELETES(m_pCsvReadOnlyBuffer);
	}

	bool Load( char const* filename, bool bCommentOut = true);
	bool Read( uint8_t *pData , size_t sz , bool bCommentOut = true );
	bool Save( const std::string filename="");
	bool SaveTSVFile(char const* filename);
	bool SaveXMLFile(char const* filename , gxBool bUseRawName = gxFalse );
	bool SaveJSONFile(char const* pFileName, gxBool bUseRawName = gxFalse);
	CXML ConvertXML(gxBool bUseRawName = gxFalse);
	//CJson& ConvertJSon(gxBool bUseRawName = gxFalse);

	int GetWidth()
	{
		return m_RangeW;
	}

	int GetHeight()
	{
		return m_RangeH;
	}

	int GetColumnIndex(std::string name, int raw = 0)
	{
		std::string cellName = "";
		for (int ii = 0; ii < GetWidth(); ii++)
		{
			GetCell(ii, raw, cellName);
			if (cellName == name) return ii;
		}
		return -1;
	}

	char* GetCell(int x,int y);
	bool  SetCell(int x,int y,char const *msg,...);
	bool  SetCellFix(int x, int y, char const* msg);
	bool  DelCell(int x,int y);

	void GetCell(int x, int y, size_t& param);
	void GetCell(int x,int y , int& param );
	void GetCell(int x,int y , float& param);
	void GetCell(int x, int y, bool& param);
	void GetCell(int x, int y, std::string& param);

	//特定の文字列を含む行列を返す
	bool  SearchWord(char *p,int &x,int &y);

	//指定エリアにあるデータの数を数える
	int   COUNTA( int x1 , int y1 , int x2 , int y2 );

	//X1列目から指定された名前のセルを探して、X2列目のデータを取る。
	char* VLOOKUP( char *name , int x1 , int x2 );

	bool Compare( CCsv &csv )
	{
		if( csv.GetWidth()  != this->GetWidth()  ) return false;
		if( csv.GetHeight() != this->GetHeight() ) return false;

		char *p1,*p2;

		for( int y=0; y<GetHeight(); y++ )
		{
			for( int x=0;x<GetWidth(); x++ )
			{
				p1 = this-> GetCell( x , y );
				p2 = csv.GetCell( x , y );

				if (strcmp(p1, p2))
				{
					return false;
				}
			}
		}

		return true;
	}

	struct POS
	{
		size_t x,y;
	};

	//wordの含まれる行をすべて取得する
	std::vector<size_t> Grep(std::string word , gxBool bUpper = gxFalse);

	//wordの含まれるセルをすべて取得する
	std::vector<POS>    GrepPos(std::string word, gxBool bUpper = gxFalse);

	//特定の行を１行取得する
	std::string GetLine(size_t y);

	//セルの最大範囲を最適化する
	void UpdateCellRange();

	//SJISドキュメントをUTF8化する
	void ChangeSJIStoUTF8();

	void SetCleanDoubleQuoteFlag(gxBool bClean)
	{
		m_bCleanDquote = bClean;
	}

	void Print();
	void PrintSJIS();

private:

	//文字列からダブルコーテーションを削除する
	bool cleanDQuote(char* pString);

	bool saveFile(int mode , char const* filename);

	gxBool initWritableSheet();

	//CSVの最大列
	int m_RangeW = 1;

	//CSVの最大行
	int m_RangeH = 1;

	std::map< std::string,std::string> m_pCellMap;
	std::vector<char> m_pTempBuf;

	//セル取得時にコメントを削除するフラグ
	bool m_bIgnoreComment;
	bool m_bCleanDquote;

	char* getCellBuf(int x, int y);

	//CSVデータを解析して配列に詰める
	bool analysingCsv( char *p,size_t sz);

	//コメント以降のデータを削除する
	bool clearComment( char *p );

	//Tab,Spaceを削除した値を返す
	bool cleanTab( char* pString );

	//ラインに含まれる最後のセル列を検出
	Sint32 getLineWidth(Sint32 y);

	gxChar* m_pCsvReadOnlyBuffer = NULL;

	//各セルごとに参照するm_pCsvReadOnlyBufferのアドレス（個別にメモリ確保はしていない）
	gxChar** m_pCsvArray = NULL;

	std::string m_FileName;
};

#endif
