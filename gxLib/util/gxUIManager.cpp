﻿//-------------------------------------------------
//行列計算
//-------------------------------------------------

#include <gxLib.h>
#include "gxUIManager.h"

SINGLETON_DECLARE_INSTANCE(gxUIManager);

#define TEXPAGE_UI_MANAGER ((MAX_MASTERTEX_NUM-1)*64)

//ベース
static gxSprite sprTbl[]={
	{ TEXPAGE_UI_MANAGER , 256*0 , 256*0 , 256,256,128,128},
	{ TEXPAGE_UI_MANAGER , 256*1 , 256*0 , 256,256,128,128},	//1:SAVE
	{ TEXPAGE_UI_MANAGER , 256*2 , 256*0 , 256,256,128,128},	//2:Load
	{ TEXPAGE_UI_MANAGER , 256*3 , 256*0 , 256,256,128,128},	//3:Sound

	{ TEXPAGE_UI_MANAGER , 256*0 , 256*1 , 256,256,128,128},	//4:Net
	{ TEXPAGE_UI_MANAGER , 256*1 , 256*1 , 256,256,128,128},	//5:BT
	{ TEXPAGE_UI_MANAGER , 256*2 , 256*1 , 256,256,128,128},
	{ TEXPAGE_UI_MANAGER , 256*3 , 256*1 , 256,256,128,128},	//7:Disable

};

gxUIManager::gxUIManager()
{
	m_sInitTimer = 120;
	m_fDispVolumeRatio = gxLib::GetAudioMasterVolume();
}

void gxUIManager::SetDialog( gxBool bEndless , gxChar *pCaption , gxChar *pString , ... )
{
	//Dialogを表示する

	va_list app;
	va_start(app, pString);
	vsprintf(m_MessageString, pString, app );
	va_end(app);

	sprintf( m_CaptionString , "%s" , pCaption );

	m_fDialogTime = 2.0f;
	m_fDialogAddTime = -1.0f / 120.0f;

	SetFade(gxFalse);

	if( bEndless )
	{
		//m_fDialogAddTime = 0.0f;
	}

#ifdef GX_DEBUG
	m_FadeARGB = 0xFF01FF01;
#endif

}


void gxUIManager::Action()
{
	m_fFadeNow += m_fFadeSpeed;

	if (m_fFadeNow <= 0.0f)
	{
		m_fFadeNow = 0.0f;
		m_FadeDir = 0;
		m_fFadeSpeed = 0.0f;
	}
	else if (m_fFadeNow >= 1.0f)
	{
		m_fFadeNow = 1.0f;
		m_FadeDir = 0;
		m_fFadeSpeed = 0.0f;
	}
	else
	{
		//Fade中
		//int ct = 0;
		//ct++;
	}

	if( m_fNowLoading > 0.0f )
	{
		m_fNowLoading -= 1.0f/60.0f;
		m_LoadDispCnt ++;
	} 
	else
	{
		m_LoadDispCnt = 0;
	}

	if( m_fNowSaving > 0.0f )
	{
		m_fNowSaving -= 1.0f/60.0f;
		m_SaveDispCnt ++;
	} 
	else
	{
		m_SaveDispCnt = 0;
	}

	if( m_fNowHttpConnecting > 0.0f )
	{
		m_fNowHttpConnecting -= 1.0f / 60.0f;
		m_HttpDispCnt ++;
	} 
	else
	{
		m_HttpDispCnt = 0;
	}

	if( m_fNowBlueToothEnable > 0.0f )
	{
		m_fNowBlueToothEnable -= 1.0f / 60.0f;
		m_BToothDispCnt ++;
	}
	else
	{
		m_BToothDispCnt = 0;
	}

	if( m_sInitTimer > 0 )
	{
		//最初はボリューム表示なし
		m_sVolumeMeterDispCnt = 0;
		m_sInitTimer --;
	}
	else if( m_sVolumeMeterDispCnt > 0 )
	{
		m_fDispVolumeRatio += (gxLib::GetAudioMasterVolume() - m_fDispVolumeRatio)/3.0f;
		m_sVolumeMeterDispCnt --;
	}

	m_fDialogTime   += m_fDialogAddTime;

	m_fNowLoading         = CLAMP( m_fNowLoading , 0.0f , 120.f );
	m_fNowSaving          = CLAMP( m_fNowSaving , 0.0f , 120.f );
	m_fNowHttpConnecting  = CLAMP( m_fNowHttpConnecting , 0.0f , 120.f );
	m_fNowBlueToothEnable = CLAMP( m_fNowBlueToothEnable, 0.0f , 120.f );
	m_fDialogTime         = CLAMP( m_fDialogTime , 0.0f , 120.0f );


}


void gxUIManager::Draw()
{
	Sint32 hh = 22;
	Sint32 gw,gh,sw,sh;
	gxLib::GetDeviceResolution( &gw , &gh , &sw , &sh );
	Sint32 winw = sw, winh = sh;
	Sint32 offw = 64;
	Sint32 prio = MAX_PRIORITY_NUM;

	switch (m_FadeType) {
	case eFadeTypeNormal:
		if (m_fFadeNow > 0.0f)
		{
			gxLib::DrawBox(-32, -32, sw + 32, sh + 32, MAX_PRIORITY_NUM, gxTrue, ATR_DFLT, SET_ALPHA(m_fFadeNow, m_FadeARGB));
		}
		break;

	case eFadeTypeWipe:
	case eFadeTypeLine:
	case eFadeTypeCircle:
	case eFadeTypeCrossFade:
		break;
	}

	gxChar* prc[]={
		".",
		"..",
		"...",
	};
	//gxLib::DrawBox(0,0,10,10, MAX_PRIORITY_NUM, gxTrue, ATR_DFLT, 0xFFF01010);	//毒

	//gxChar *progress = prc[ (gxLib::GetGameCounter()/8)%3];

	Float32 fRot = (gxLib::GetGameCounter()*2)%360;
	Float32 fScale1 = abs( gxUtil::Cos(fRot) );
	Float32 fScale2 = gxUtil::Cos(fRot);

	if( !m_bStart ) return;

	if( m_fNowLoading > 0.0f )
	{
		//LOAD
		if( m_LoadDispCnt >= FRAME_PER_SECOND )
		{
			Sint32 x = winw - offw / 2 - offw * 1;
			gxLib::PutSprite( &sprTbl[0] , x,32 , prio , ATR_DFLT , ARGB_DFLT ,fRot , 0.25f , 0.25f );
			gxLib::PutSprite( &sprTbl[2] , x,32 , prio , ATR_DFLT , ARGB_DFLT ,fRot , 0.25f , 0.25f );
		}
	}

	if( m_fNowSaving > 0.0f )
	{
		//SAVE
		if( m_SaveDispCnt >= FRAME_PER_SECOND )
		{
			Sint32 x = winw - offw / 2 - offw * 2;
			gxLib::PutSprite( &sprTbl[0] , x,32 , prio , ATR_DFLT , ARGB_DFLT ,0.f , 0.25f , 0.25f );
			gxLib::PutSprite( &sprTbl[1] , x,32 , prio , ATR_DFLT , ARGB_DFLT ,0.f , 0.25f+0.1f*fScale1 , 0.25f+0.1f*fScale1 );
		}
	}

	if( m_fNowHttpConnecting > 0.0f )
	{
		//NET
		if( m_HttpDispCnt >= FRAME_PER_SECOND )
		{
			Sint32 x = winw - offw / 2 - offw * 3;
			gxLib::PutSprite( &sprTbl[0] , x,32 , prio , ATR_DFLT , ARGB_DFLT ,0.f , 0.25f , 0.25f );
			gxLib::PutSprite( &sprTbl[4] , x,32 , prio , ATR_DFLT , ARGB_DFLT ,0.f , 0.25f*fScale2 , 0.25f );
		}
	}

	if( m_fNowBlueToothEnable > 0.0f )
	{
		//BT
		if( m_BToothDispCnt >= FRAME_PER_SECOND )
		{
			Sint32 x = winw - offw / 2 - offw * 4;
			Float32 fScaleBT = +0.05f*fScale2;
			gxLib::PutSprite( &sprTbl[0] , x,32 , prio , ATR_DFLT , ARGB_DFLT ,0.f , 0.25f , 0.25f );
			gxLib::PutSprite( &sprTbl[5] , x,32 , prio , ATR_DFLT , ARGB_DFLT ,0.f , 0.25f+fScaleBT , 0.25f+fScaleBT );
		}

	}

	if( m_fDialogTime > 0.0f )
	{
		Float32 fAlpha = m_fDialogTime;
		fAlpha = CLAMP( fAlpha , 0.0f , 1.0f );
		gxLib::DrawBox( -32 , sh/2-64 , sw+32 , sh/2+32 , prio , gxTrue , ATR_ALPHA_MINUS , SET_ALPHA( fAlpha , 0xA0FFFFFF) );
		gxLib::DrawBox( -32 , sh/2-64 , sw+32 , sh/2+32 , prio , gxFalse , ATR_DFLT , SET_ALPHA( fAlpha , 0xFF808080) , 4.0f );
		gxLib::Printf (sw/2 , sh/2-32 , prio , ATR_STR_CENTER , SET_ALPHA( fAlpha , ARGB_DFLT ), "%s" , m_CaptionString );
		gxLib::Printf (sw/2 , sh/2+0  , prio , ATR_STR_CENTER , SET_ALPHA( fAlpha , ARGB_DFLT ), "%s" , m_MessageString );
	}

	if( m_sVolumeMeterDispCnt > 0 || (gxLib::GetAudioMasterVolume()==0.0f) )
	{
		drawVolumeMeter();
	}
}

void gxUIManager::drawVolumeMeter()
{
	Sint32 offw = 64;
	Sint32 gw,gh,sw,sh;
	Sint32 prio = MAX_PRIORITY_NUM;
	gxLib::GetDeviceResolution( &gw , &gh , &sw , &sh );

	Sint32 winw = sw, winh = sh;

	if( gxLib::GetAudioMasterVolume() == 0.0f )
	{
		//gxLib::Printf(0 , 0+16*0 , prio , ATR_STR_LEFT , ARGB_DFLT , "SOUND MUTE" );

		//SOUND
		gxLib::PutSprite( &sprTbl[0] , winw - offw / 2 - offw * 0,32 , prio , ATR_DFLT , ARGB_DFLT ,0.f , 0.25f , 0.25f );
		gxLib::PutSprite( &sprTbl[3] , winw - offw / 2 - offw * 0,32 , prio , ATR_DFLT , ARGB_DFLT ,0.f , 0.25f , 0.25f );
		gxLib::PutSprite( &sprTbl[7] , winw - offw / 2 - offw * 0,32 , prio , ATR_DFLT , ARGB_DFLT ,0.f , 0.25f , 0.25f );

		return;
	}

	Float32 fRatio = m_fDispVolumeRatio;


	gxRect rect;
	rect.SetWH( 0.f,0.f, sw*fRatio , sh/30.f );
	gxLib::DrawBox( 0,0 , sw , sh/30+8 , prio , gxTrue , ATR_ALPHA_MINUS , 0x80ffffff);
	//gxLib::DrawBox( 0,0 , sw*fRatio , sh/30 , prio , gxTrue , ATR_DFLT , 0xff00ff00);
	gxLib::DrawColorBox(
			0,0,				0xffFFff00,
			sw*fRatio,0, 		0xff00ff00,
			sw*fRatio , sh/30,	0xff808000,
			0, sh/30,			0xff008000,
			prio,ATR_DFLT);


	gxLib::DrawBox( 0,0 , sw , sh/30 , prio , gxFalse , ATR_DFLT , 0xffA0A080 ,3.0f);

}

