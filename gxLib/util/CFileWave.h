﻿#ifndef _CFILEWAVE_H_
#define _CFILEWAVE_H_

typedef struct guid {
    Uint32 Data1;
    Uint16 Data2;
    Uint16 Data3;
    Uint8  Data4[8];
} guid;

typedef struct StWAVEFORMATEX { 
  Uint16 wFormatTag; 
  Uint16 nChannels; 
  Uint32 nSamplesPerSec; 
  Uint32 nAvgBytesPerSec; 
  Uint16 nBlockAlign; 
  Uint16 wBitsPerSample; 
  Uint16 cbSize; 
} StWAVEFORMATEX; 

struct StWaveInfo
{
    StWAVEFORMATEX Format;

    union {
        Uint16 wValidBitsPerSample;
        Uint16 wSamplesPerBlock;
        Uint16 wReserved;
    } Samples;

    Uint32 dwChannelMask = 0;
    guid SubFormat;

};


class CFileWave
{
public:

	CFileWave();
	~CFileWave();

	gxBool Load( gxChar* pFileName );
	gxBool Read( Uint8* pMemory  , size_t uSize );

	Uint32 GetFileSize()
	{
		return m_DataSize;
	}

	Uint8* GetWaveImage()
	{
		return m_pWaveImage;
	}

	Uint8* GetPCMImage();

	StWaveInfo* GetFormat()
	{
		return &m_stWaveInfo;
	}

	void ReleaseImage()
	{
		if( m_pWaveImage )
		{
			SAFE_DELETE(m_pWaveImage);
		}
	}

private:

	Uint32 getWaveData( Uint8 *pFileImage, Uint8 **ppWaveImage, StWaveInfo *pWaveInfo );

	Uint32 getChunk( Uint8 *pSrcData, Uint8 **ppDstData, Uint32 *pSize );

	Uint32 makeWaveInfo( Uint8 *pHeader, StWaveInfo *pWaveInfo );

	Uint32 makeWaveHeader( Uint8 *pHeader, StWaveInfo *pWaveInfo );

	StWaveInfo m_stWaveInfo;	//ヘッダー情報

	Uint8     *m_pWaveImage;	//WAVEデータイメージ
	Uint8     *m_pPCMImage;		//PCMデータへのポインタ
	Uint16	   m_uFormat;		//フォーマット( -1:未知のWAVE / 0:ＰＣＭ / 1:マルチチャンネル ウェーブ フォーマット / 2:圧縮WAVE )
	Uint32	   m_DataSize;		//WAVEデータイメージのサイズ

};

#endif
