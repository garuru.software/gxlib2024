//--------------------------------------------------
//
// gxUtil.cpp
// 便利な関数群
//
//--------------------------------------------------
#ifndef _GXUTIL_H_
#define _GXUTIL_H_
//#include "CFileCSV.h"
//#include "gxMath.h"
#include <cstring>

//#include <gxLib/Util/CCollisionManager.h>
//#include <gxLib/Util/CActManager.h>

class gxUtil
{
public:
	static gxBool s_LoadTask;

	gxUtil()
	{
	}

	~gxUtil()
	{
	}

	static void Update();

	//ファイルパス、名前、拡張子の変換
	static Uint64 atox( gxChar *p );
	static gxBool GetFileNameWithoutPath(gxChar *in,gxChar *out);
	static gxBool GetPath( gxChar *in , gxChar *out );
	static gxBool GetExt ( gxChar *in , gxChar* out );
	static gxBool GetFileNameWithoutExt(gxChar *in , gxChar *out);
	static gxBool IsSameEXT( gxChar *in1 , gxChar* in2 );

	static std::string GetMD5(Uint8 *pData, size_t sz);

	//文字列変換
	static std::string SJIStoUTF8( std::string str );
	static std::string UTF8toSJIS(std::string str);
	static std::string UTF32toUTF8(std::vector<Uint32> str32);

	//static std::vector<Uint16> UTF8toUTF16(std::string str);
	static std::vector<Uint32> UTF8toUTF32(std::string str);


	
	//static std::string GetUpperPath(std::string path);
	//static std::string GetRelativePath( gxChar* pcurrentPath , gxChar* path );

	//文字列を大文字に変換する
	static void StrUpper( gxChar* pStr );
	static void StrUpper(std::string& str);
	static std::vector<Uint32> StrUpper32( std::vector<Uint32> &str );
	static void StrLower(std::string& str);
	static std::vector<Uint32> StrLower32(std::vector<Uint32>& str);

	//文字列の長さを得る
	static Uint32 StrLen( const gxChar* pStr );

	//FPSを表示する
	static void DrawFPS( Sint32 x,Sint32 y , Sint32 prio );

	static void MemSet( void* pMem , Uint8 val, size_t sz );
	static void MemCpy( void* pDst , void *pSrc , size_t sz );

	//非同期のHTTPアクセス

	static Sint32 OpenWebFile  ( std::string URL, gxChar *pPostData = nullptr, std::function<gxBool(gxBool bSuccess, uint8_t* p, size_t size)> func=nullptr);
	static gxBool IsDownloadWebFile( Float32* fRatio = nullptr);
	static gxBool IsDownloadWebFile( Uint32 index , Float32 *fRatio= nullptr);
	static gxBool IsDownloadSucceeded(Uint32 index);
	static Uint8* GetDownloadWebFile(Uint32 uIndex, size_t* uSize);
	static gxBool CloseWebFile();
	static gxBool CloseWebFile(Uint32 uIndex);

	//非同期のファイルアクセス
	static Sint32 LoadFile( std::function<void(Sint32 id , Uint8 *pData , size_t uSize)>func , gxChar* pFileName , ... );
	static gxBool IsLoadFile( Sint32 id , Uint8 **pData , size_t* pLength );
	static gxBool IsNowLoading();

	//テクスチャをファイルからマスターテクスチャへ読み込みます
	static gxBool LoadTexture ( Uint32 texPage , Uint32 ox , Uint32 oy , gxChar* pFileName, ... );

	//サウンドファイルをファイルから指定バンクに読み込みます
	static Sint32 LoadAudio( Uint32 uIndex , gxChar* pFileName , ... );

	//ロードタスクがすべて完了しているか？
	static gxBool IsLoadComplete(int id = -1);

	//１行のCSVデータを配列に分解する
    static std::vector<std::string> GetCSVArray(std::string csvValue);

	//加速度を伴う値のアジャスト
	static Float64 AdjustEaseInOut(Float64 now_gold, Float64 tgt_gold , Float64 ratio=10.0f);
	static Sint64 AdjustEaseInOut64(Sint64 now_gold, Sint64 tgt_gold, Sint64 ratio = 10.0f);

	//数学系

	static Float32 Cos( Float32 deg );
	static Float32 Sin( Float32 deg );
//	static Float32 Atan( Float32 x ,Float32 y );
	static Float32 Angle(Float32 x, Float32 y);
	static Float32 Sqrt( Float32 n );

	//２点間の距離を返します
	static Float32 Distance( Float32 x ,Float32 y );
	static Float32 Distance( Float32 x1 ,Float32 y1 , Float32 x2 ,Float32 y2);

	//座標を回転させます
	static void RotationPoint( gxVector2 *pPt, Float32 fRot );

	//目標角度への回転方向と量を算出する
	static Float32 GetTargetRotation(Float32 target_angle, Float32 my_angle);

	static void BezierCurve( Float32 fromX, Float32 fromY, Float32 cpX, Float32 cpY, Float32 cpX2, Float32 cpY2, Float32 toX, Float32 toY, Float32 div ,std::vector<gxVector2> &path );

	//点と多角形の内外判定
	static gxBool IsInsidePolygon( gxVector2 &p, gxVector2* poly , Uint32 length );

	//bool IsInsideRectangle( const gxVector2& a , const gxVector2& b , const gxVector2& c , const gxVector2& d, const gxVector2& p);

	//線と線の衝突判定
	static gxBool IsCross( gxVector2 &r1, gxVector2 &r2, gxVector2 &p1, gxVector2 &p2 );

	//多角形同士の当たり判定
	static gxBool PolyvsPoly( gxVector2 *p1 , Uint32 length1 , gxVector2* p2 , Uint32 length2 );

	//円と円の当たり判定
	static gxBool CirclevsCircle( gxVector2 *p1 , Float32 fRadius1 , gxVector2 *p2 , Float32 fRadius2 );

	static Float32 GetBlinkAlpha(Float32 spd)
	{
		Float32 r = ((int)(gxLib::GetGameCounter() * spd)) % 360;
		Float32 a = 0.5f + gxUtil::Cos(r) * 0.5f;
		a = CLAMP(a, 0.0, 1.0f);
		return a;
	}

	static gxBool ReadTexture2(Uint32 texPage, Uint8* pBuffer, size_t Size, Uint32 colorKey = 0xff00ff00, Uint32 ox = 0, Uint32 oy = 0, Sint32* w = nullptr, Sint32* h = nullptr);

	static Uint32 PutFreeSprite(
		Sint32 x,Sint32 y,
		Sint32 prio,
		gxVector2 *pUV,
		Sint32 tpg,
		Sint32 u,Sint32 v , Sint32 w , Sint32 h,
		Sint32 divNum = 3,
		Uint32  atr = ATR_DFLT,
		Uint32  argb = ARGB_DFLT,
		Float32 fRot = 0.0f,
		Float32 fx = 1.0f,
		Float32 fy = 1.0f
		);

	//多角形を描画します
	static Uint32 DrawPolygon(
			Sint32 num,
			gxVector2 *pXY,
			gxVector2 *pUV,
			Uint32 *argb,
			Sint32 prio,
			Uint32 atr ,
			Sint32 page,
			gxBool bFill );

	struct CKeyBoard
	{
		gxBool IsTrigger( Uint32 key );
		gxBool IsPush( Uint32 key );
		gxBool IsRepeat( Uint32 key );
		gxBool IsRelease( Uint32 key );
		std::vector<gxKey::KeyType> GetInputKey( Uint32 atr = enStatTrig );
		const gxChar* GetKeyString( gxKey::KeyType key );
	};

	struct CGamePad
	{

	public:
		Sint32 id = 0;

		CGamePad();
		~CGamePad()
		{
		}
        Float32 GetRotationAngle( Float32 *pRot );
		gxBool IsTrigger( Uint32 key );
		gxBool IsPush( Uint32 key );
		gxBool IsRepeat( Uint32 key );
		gxBool IsRelease( Uint32 key );
		gxBool IsDoubleClick(Uint32 key);
		gxBool IsLongTap(Uint32 key);
		gxVector2* GetLeftStick();
		gxVector2* GetRightStick();
		const gxChar*  GetString( EJoyBit btn );

	private:

		gxVector2 m_LeftStick;
		gxVector2 m_RightStick;
		gxVector2 m_Mouse;
	};

	struct CTouch
	{
		//keyはMOUSE_L / MOUSE_M / MOUSE_Rのどれか
		gxBool IsTrigger();
		gxBool IsPush();
		gxBool IsRepeat();
		gxBool IsRelease();
		gxBool IsDoubleTap();
		gxBool IsLongTap();
		gxBool IsFlick(Uint32 key);
		Float32 GetZoomRatio();
		gxBool IsPinchIn();
		gxBool IsPinchOut();

		Float32 GetFlickRotation();

		gxBool IsTap( Sint32 x , Sint32 y , Sint32 w , Sint32 h);
		gxBool IsTap( Sint32 x , Sint32 y , Float length );
		gxVector2 GetPosition();
		void Update();

		Sint32 id=0;

		Float32 m_fPinchScale    = 1.0f;

	private:
		gxVector2 m_Position;

	};

	static CKeyBoard *KeyBoard();
	static CGamePad  *GamePad( Uint32 player );
	static CTouch    *Touch(Uint32 id = 0);

	//衝撃波
	static void DrawSonicBoom( Sint32 x , Sint32 y , Sint32 prio , Float32 fRadius  , Float32 fWidth = 0.0f , Float32 fDistPow = 0.1f , Uint32 tpg=TEXPAGE_CAPTURE , Uint32 atr=ATR_DFLT,Uint32 argb=ARGB_DFLT );

	static void DrawBox( gxRect *pRect , Sint32 prio , gxBool bFill , Uint32 atr=ATR_DFLT  , Uint32 argb=ARGB_DFLT  , Float32 fFat=2.0f );
	static void DrawLine( gxVector2 p1 , gxVector2 p2 , Sint32  prio , Uint32 atr=ATR_DFLT  , Uint32 argb=ARGB_DFLT  , Float32 fFat=2.0f );


	static std::vector<std::string> GetDirectoryList( std::string rootDir , std::string filter = "");
	static std::vector<std::string> GetFileList(std::string rootDir, std::string filter = "");
	std::vector<std::string> MakeRelativeDirectoryList(std::vector<std::string> list, std::string rootDir, std::string filter="");
	static std::vector<std::string> FindFileFromUTF8FileList(std::vector<std::string>& utf8list, std::string searchwords);

	class RoundButton
	{
	public:
		RoundButton()
		{
			m_Pos.x = WINDOW_W / 2;
			m_Pos.y = WINDOW_H / 2;
			m_Pos.z = 0;
			m_ARGB = ARGB_DFLT;
			m_fRadius = 128;
			m_bOn = gxTrue;
			m_PushCnt = 0;
		}

		RoundButton( Sint32 x , Sint32 y , Sint32 prio , Uint32 argb , Float32 radius)
		{
			m_Pos.x = x;
			m_Pos.y = y;
			m_Pos.z = prio;
			m_ARGB = argb;
			m_fRadius = radius;
			m_bOn = gxTrue;
			m_PushCnt = 0;
		}

		~RoundButton(){};

		void SetPos(Sint32 x, Sint32 y, Sint32 prio )
		{
			m_Pos.x = x;
			m_Pos.y = y;
			m_Pos.z = prio;
		}

		gxPos& GetPos()
		{
			return m_Pos;
		}

		void Update();
		void Draw();
		void SetOn( gxBool bOn )
		{
			m_bOn = bOn;
		}

		gxBool IsTrigger()
		{
			if( m_PushCnt == 1 ) return gxTrue;
			return gxFalse;
		}

		gxBool IsPush()
		{
			if( m_PushCnt > 0 ) return gxTrue;

			return gxFalse;
		}

		gxBool IsRelease()
		{
			if( m_PushCnt == -1 ) return gxTrue;

			return gxFalse;
		}

		void SetText( std::string str )
		{
            m_Name = str;
		}

		void SetRadius(Float32 radius)
		{
			m_fRadius = radius;
		}

		void SetARGB(Uint32 argb )
		{
			m_ARGB = argb;
		}
        void SetCenter(Uint32 center )
        {
            m_Centering = center;
        }

	private:
		gxPos   m_Pos={0,0,0};
		Float32 m_fRadius = 32.0f;
		Uint32  m_ARGB = 0xffffffff;
		gxBool  m_bOn = gxTrue;
		Sint32  m_PushCnt = 0;
        std::string m_Name="BTN";
        Sint32 m_Centering = 0;
	};


	class FileNames
	{
	public:

		FileNames()
		{
		}

		~FileNames()
		{
			clear();
		}

		FileNames(const char* str)
		{
			set((gxChar*)str);
		}

		FileNames(const std::string &str)
		{
			set((gxChar*)str.c_str());
		}

		FileNames(gxChar* str)
		{
			set((gxChar*)str);
		}

		void operator = (const char* str)
		{
			set((gxChar*)str);
		}

		void operator = (std::string& str)
		{
			set((gxChar*)str.c_str());
		}

		void operator = (gxChar* str)
		{
			set((gxChar*)str);
		}

		bool operator == (char* fileName)
		{
			std::string _name = fileName;
			return (FullName == _name) ? true : false;
		}
		bool operator != (char* fileName)
		{
			std::string _name = fileName;
			return (FullName != _name) ? true : false;
		}

		bool operator == (std::string &fileName)
		{
			return (FullName == fileName) ? true : false;
		}

		bool operator != (std::string& fileName)
		{
			return (FullName != fileName) ? true : false;
		}

		void SetBackSlash()
		{
			if (FullName.back() == '/')
			{
				return;
			}

			std::string name = FullName;
			name += "/";
			set((gxChar*)name.c_str());
		}
		gxBool IsSameEXT(const std::string& ext )
		{
			std::string str1,str2;

			char buf[FILENAMEBUF_LENGTH];
			sprintf( buf , "%s",ext.c_str() );
			StrUpper(buf);
			str1 = buf;

			sprintf( buf , "%s",Ext.c_str() );
			StrUpper(buf);
			str2 = buf;

			if (str1 == str2) return gxTrue;

			return gxFalse;
		}

		gxBool IsExistEXT()
		{

			if (Ext.size() <= 0) return gxFalse;

			return gxTrue;
		}

		gxBool IsDirectory()
		{
			if (Ext.size() <= 0) return gxFalse;
			size_t sz = FullName.size();
			if (sz > 0)
			{
				if (FullName[sz - 1] == '/')  return gxTrue;
				if (FullName[sz - 1] == '\\') return gxTrue;
			}
			return gxFalse;
		}

		void ChangeExt(const std::string& ext);
		void ChangePath(const std::string& path);
		void ConvPathtoBackSlash();

		std::string FullName;
		std::string FileName;
		std::string FilePath;
		std::string Ext;
		std::string FileNameWithoutExt;

		Uint32 GetSubDirectoryDepth(const std::string& basePath);
		std::string GetFilePathWithOutSlash();
		std::string GetRelativeFileName(const std::string& basePath);
		std::string GetRelativeFilePath(const std::string& basePath);
		gxBool IsIncludePath(const std::string& basePath);

		void ChangeDirUp();
		void ChangeDirDown(const std::string& dir );

	private:

		void set(gxChar *pStr, ...);
		void init(const std::string& str);
		void clear();
		//std::vector<gxChar*> m_Temp;
		//gxChar *m_pFileName32 = nullptr;
		//gxChar* m_pFileName32 = nullptr;

		std::string pathNormalize(const std::string& _basepath);
	};

private:

	static CKeyBoard m_KeyBoard;
	static CGamePad  m_GamePad[PLAYER_MAX];
	static CTouch    m_Touch[GX_TOUCH_MAX];

};


class CircleCircle
{
public:
	typedef struct StCircle {
		//円の定義用
		Float32 x,y,r;
		StCircle()
		{
			x = y = 0;
			r = 1;
		}
	} StCircle;

	CircleCircle();
	~CircleCircle();

	void SetCircle( Sint32 id , Float32 x , Float32 y , Float32 r )
	{
		//円を定義する
		m_Circle[id].x = (Float32)x;
		m_Circle[id].y = (Float32)y;
		m_Circle[id].r = (Float32)r;
	}

	void SetNear( Float32 x , Float32 y )
	{
		//近い座標を設定する
		m_Near.x = x;
		m_Near.y = y;
	}

	gxVector2* GetKouten( Sint32 sIndex = 0 )
	{
		//交点座標を返す
		return &m_Kouten[sIndex];
	}

	Sint32 Calc();
	Sint32 GetNearest()
	{
		return m_sNearest;
	}
private:

	gxVector2    m_Near;		//交点が２つある場合に近い点を探す際の参照する座標
	Sint32     m_sNearest;	//交点１、２の最も近い点
	StCircle   m_Circle[2];	//円１、２
	gxVector2    m_Kouten[2];	//交点１，２

};


#include <iterator>
#include <cstdint>

class MD5 {
private:
	std::uint32_t 	a0_;
	std::uint32_t 	b0_;
	std::uint32_t 	c0_;
	std::uint32_t 	d0_;
	std::uint32_t*	m_array_first_=nullptr;
	std::uint32_t	m_array_[16] = {0};

private:
	static std::uint32_t left_rotate(std::uint32_t x, std::uint32_t c) {
		return (x << c) | (x >> (32 - c));
	}

	template <class OutputIterator>
	static void uint32_to_byte(std::uint32_t n, OutputIterator & first) {
		*first++ = n & 0xff;
		*first++ = (n >> 8) & 0xff;
		*first++ = (n >> 16) & 0xff;
		*first++ = (n >> 24) & 0xff;
	}

	template <class OutputIterator>
	static void uint32_to_hex(std::uint32_t n, OutputIterator & first) {
		const char * hex_chars = "0123456789abcdef";

		std::uint32_t b;

		b = n & 0xff;
		*first++ = hex_chars[b >> 4];
		*first++ = hex_chars[b & 0xf];

		b = (n >> 8) & 0xff;
		*first++ = hex_chars[b >> 4];
		*first++ = hex_chars[b & 0xf];

		b = (n >> 16) & 0xff;
		*first++ = hex_chars[b >> 4];
		*first++ = hex_chars[b & 0xf];

		b = (n >> 24) & 0xff;
		*first++ = hex_chars[b >> 4];
		*first++ = hex_chars[b & 0xf];
	}

private:
	void reset_m_array() {
		m_array_first_ = &m_array_[0];
	}

	template<class InputIterator>
	static std::uint8_t input_u8(const InputIterator& it)
	{
		return *it;
	}

	template <class InputIterator>
	void bytes_to_m_array(InputIterator & first, std::uint32_t* m_array_last)
	{
		/*
					uint8_t * p = (uint8_t*)(&(*first));
					*m_array_first_ = (*p);

					first++;
					p = (uint8_t*)(&(*first));
					*m_array_first_ |= (*p)<<8;
		*/
		for (; m_array_first_ != m_array_last; ++m_array_first_)
		{
			*m_array_first_ = input_u8(first++);
			*m_array_first_ |= input_u8(first++) << 8;
			*m_array_first_ |= input_u8(first++) << 16;
			*m_array_first_ |= input_u8(first++) << 24;
		}
	}

	template<class InputIterator>
	void true_bit_to_m_array(InputIterator& first, std::ptrdiff_t chunk_length)
	{
		switch (chunk_length % 4) {
		case 0:
			*m_array_first_ = 0x00000080;
			break;
		case 1:
			*m_array_first_ = input_u8(first++);
			*m_array_first_ |= 0x00008000;
			break;
		case 2:
			*m_array_first_ = input_u8(first++);
			*m_array_first_ |= input_u8(first++) << 8;
			*m_array_first_ |= 0x00800000;
			break;
		case 3:
			*m_array_first_ = input_u8(first++);
			*m_array_first_ |= input_u8(first++) << 8;
			*m_array_first_ |= input_u8(first++) << 16;
			*m_array_first_ |= 0x80000000;
			break;
		}
		++m_array_first_;
	}
	void zeros_to_m_array(std::uint32_t* m_array_last) {
		for (; m_array_first_ != m_array_last; ++m_array_first_) {
			*m_array_first_ = 0;
		}
	}

	void original_length_bits_to_m_array(std::uint64_t original_length_bits) {
#if 1
		* m_array_first_++ = uint32_t(original_length_bits);
		*m_array_first_++ = uint32_t(original_length_bits >> 32);
#else
		original_length_bits &= 0xffffffffffffffff;
		*m_array_first_++ = (original_length_bits) & 0x00000000ffffffff;
		*m_array_first_++ = (original_length_bits & 0xffffffff00000000) >> 32;
#endif
	}

	void hash_chunk() {
		static std::uint32_t const k_array[64] = {
			0xd76aa478, 0xe8c7b756, 0x242070db, 0xc1bdceee,
			0xf57c0faf, 0x4787c62a, 0xa8304613, 0xfd469501,
			0x698098d8, 0x8b44f7af, 0xffff5bb1, 0x895cd7be,
			0x6b901122, 0xfd987193, 0xa679438e, 0x49b40821,
			0xf61e2562, 0xc040b340, 0x265e5a51, 0xe9b6c7aa,
			0xd62f105d, 0x02441453, 0xd8a1e681, 0xe7d3fbc8,
			0x21e1cde6, 0xc33707d6, 0xf4d50d87, 0x455a14ed,
			0xa9e3e905, 0xfcefa3f8, 0x676f02d9, 0x8d2a4c8a,
			0xfffa3942, 0x8771f681, 0x6d9d6122, 0xfde5380c,
			0xa4beea44, 0x4bdecfa9, 0xf6bb4b60, 0xbebfbc70,
			0x289b7ec6, 0xeaa127fa, 0xd4ef3085, 0x04881d05,
			0xd9d4d039, 0xe6db99e5, 0x1fa27cf8, 0xc4ac5665,
			0xf4292244, 0x432aff97, 0xab9423a7, 0xfc93a039,
			0x655b59c3, 0x8f0ccc92, 0xffeff47d, 0x85845dd1,
			0x6fa87e4f, 0xfe2ce6e0, 0xa3014314, 0x4e0811a1,
			0xf7537e82, 0xbd3af235, 0x2ad7d2bb, 0xeb86d391
		};
		static std::uint32_t const s_array[64] = {
			7, 12, 17, 22,  7, 12, 17, 22,  7, 12, 17, 22,  7, 12, 17, 22,
			5,  9, 14, 20,  5,  9, 14, 20,  5,  9, 14, 20,  5,  9, 14, 20,
			4, 11, 16, 23,  4, 11, 16, 23,  4, 11, 16, 23,  4, 11, 16, 23,
			6, 10, 15, 21,  6, 10, 15, 21,  6, 10, 15, 21,  6, 10, 15, 21
		};

		std::uint32_t A = a0_;
		std::uint32_t B = b0_;
		std::uint32_t C = c0_;
		std::uint32_t D = d0_;

		std::uint32_t F;
		unsigned int g;

		for (unsigned int i = 0; i < 64; ++i) {
			if (i < 16) {
				F = (B & C) | ((~B) & D);
				g = i;
			}
			else if (i < 32) {
				F = (D & B) | ((~D) & C);
				g = (5 * i + 1) & 0xf;
			}
			else if (i < 48) {
				F = B ^ C ^ D;
				g = (3 * i + 5) & 0xf;
			}
			else {
				F = C ^ (B | (~D));
				g = (7 * i) & 0xf;
			}

			std::uint32_t D_temp = D;
			D = C;
			C = B;
			B += left_rotate(A + F + k_array[i] + m_array_[g], s_array[i]);
			A = D_temp;
		}

		a0_ += A;
		b0_ += B;
		c0_ += C;
		d0_ += D;
	}

public:
	template <class InputIterator>
	void update(InputIterator first, InputIterator last) {

		std::uint64_t original_length_bits = std::distance(first, last) * 8;

		std::ptrdiff_t chunk_length;
		while ((chunk_length = std::distance(first, last)) >= 64) {
			reset_m_array();
			bytes_to_m_array(first, &m_array_[16]);
			hash_chunk();
		}

		reset_m_array();
		bytes_to_m_array(first, m_array_ + chunk_length / 4);
		true_bit_to_m_array(first, chunk_length);

		if (chunk_length >= 56) {
			zeros_to_m_array(&m_array_[16]);
			hash_chunk();

			reset_m_array();
			zeros_to_m_array(&m_array_[16] - 2);
			original_length_bits_to_m_array(original_length_bits);
			hash_chunk();
		}
		else {
			zeros_to_m_array(&m_array_[16] - 2);
			original_length_bits_to_m_array(original_length_bits);
			hash_chunk();
		}
	}

public:

	MD5()
		: a0_(0x67452301),
		b0_(0xefcdab89),
		c0_(0x98badcfe),
		d0_(0x10325476)
	{}

	template <class Container>
	void digest(Container & container) {
		container.resize(16);
		auto it = container.begin();

		uint32_to_byte(a0_, it);
		uint32_to_byte(b0_, it);
		uint32_to_byte(c0_, it);
		uint32_to_byte(d0_, it);
	}

	template <class Container>
	void hex_digest(Container & container) {
		container.resize(32);
		auto it = container.begin();

		uint32_to_hex(a0_, it);
		uint32_to_hex(b0_, it);
		uint32_to_hex(c0_, it);
		uint32_to_hex(d0_, it);
	}

	uint8_t *GetHash()
	{
		static uint32_t hash[5] = { a0_,b0_,c0_,d0_ ,0x00000000};

		hash[0] = a0_;
		hash[1] = b0_;
		hash[2] = c0_;
		hash[3] = d0_;

		return (uint8_t*)&hash[0];



		static uint8_t ret[65];

		uint8_t *p = (uint8_t*)&hash[0];

		for (int ii = 0; ii < 4; ii++)
		{
			ret[ii * 4 + 0] = p[ii * 4 + 0];
			ret[ii * 4 + 1] = p[ii * 4 + 1];
			ret[ii * 4 + 2] = p[ii * 4 + 2];
			ret[ii * 4 + 3] = p[ii * 4 + 3];
		}
		ret[64] = 0x00;
		return &ret[0];
	}

};


#endif
