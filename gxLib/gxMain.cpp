//--------------------------------------------------
//
// gxMain.h
// メインループ制御
//
//--------------------------------------------------

#include <gxLib.h>
#include "gx.h"
#include "gxOrderManager.h"
#include "gxTexManager.h"
#include "gxRender.h"
#include "gxPadManager.h"
#include "gxSoundManager.h"
#include "gxFileManager.h"
#include "gxNetworkManager.h"
#include "gxDebug.h"
#include "util/gxUIManager.h"
#include "util/Font/CFontManager.h"
#include "util/CFileCsv.h"
#include "util/CVirtualPad.h"
#include "util/gxResource.cxx"
#include <atomic>
#include <thread>

//#define BGV_ON
#ifdef GX_MASTER
//	#define DRAW_LOGO
#endif

enum {
	enOrderRequestSeqInit,
	enOrderRequestSeqAccept,
	enOrderRequestSeqEnd,
};

void* GameMainThread(void *pArgs);
void* GameNetWorkThread(void*);
void* GameFileThread(void*);

void  GameMainVsyncWakeup();

extern int g_bgw, g_bgh;

SINGLETON_DECLARE_INSTANCE( CGameGirl );

struct gxclock_t {
	time_t time;
	uint64_t usec;
};

int FRAME_PER_SECOND = DEFAULT_FRAME_PER_SECOND;

//std::mutex s_BarrierFileAccess;

CGameGirl::CGameGirl()
{
	m_bMainLoop  = gxTrue;
	m_bAppFinish = gxFalse;
	m_bResume    = gxFalse;

	m_bHardPause     = gxFalse;
	m_bSoftPause     = gxFalse;
	m_sStepFrm = 0;

	m_sTimer = 0;
	m_uGameCounter = 0;
	m_sFrameSkip = 0;
	m_fVsyncSec = 0.0f;

	m_uMemoryTotal   = 1;
	m_uMemoryMaximum = 1;
	m_uMemoryUse     = 1;

	m_bResetButton   = gxFalse;
	m_bPadDeviceConfigMode = gxFalse;

	m_bInitializeCompleted = gxFalse;

	m_bWaitVSync = gxTrue;

	m_uGlobalUID = 0x00000000;
	m_uGlobalIP  = 0x00000000;
	m_uLocalIP   = 0x00000000;
	m_bOnLine    = gxFalse;

	m_bSubThread = gxFalse;
	m_b3DView    = gxFalse;

	//m_sRequestAccept = enOrderRequestSeqInit;

	gxOrderManager::GetInstance()->RequestInit();

	m_bToggleDeviceMode = gxFalse;

	m_bThroughGameMain  = gxFalse;
	m_bDrawSw			= gxTrue;

	m_ScreenMode = enScreenModeAspect;

	m_GameScreenWidth  = WINDOW_W;
	m_GameScreenHeight = WINDOW_H;

	m_WindowScreenWidth  = WINDOW_W;
	m_WindowScreenHeight = WINDOW_H;

	gxFileManager::CreateInstance();

	m_pFontManager = new CFontManager();
}


CGameGirl::~CGameGirl()
{
	while (gxTrue)
	{
		gxFileManager::GetInstance()->Action();

		if (gxFileManager::GetInstance()->IsLoadTaskExist())
		{
			VSync();
			continue;
		}
		if (!m_GameMainThreadExist)
		{
			break;
		}
	}

	drawEnd();
	soundEnd();
	inputEnd();

	gxDebug::DeleteInstance();
	gxUIManager::DeleteInstance();
	gxFileManager::DeleteInstance();
	gxNetworkManager::DeleteInstance();
	CVirtualStick::DeleteInstance();

	//gxUIManager::DeleteInstance();
	//gxFileManager::DeleteInstance();
	//gxNetworkManager::DeleteInstance();

	SAFE_DELETE( m_pFontManager );
}

void CGameGirl::Init()
{
	GetTime(&m_StartTime);

	if( !gxLib::LoadConfig() )
	{
		//ロード失敗したのでデフォルトデータを作る

		gxLib::SaveData.makeDefault();
	}

	{
		GX_DEBUGLOG("[GameGirl] Application Init.");

		drawInit();
		soundInit();
		inputInit();

	}

	m_bInitializeCompleted = gxTrue;

	gxLib::SetAudioMasterVolume(gxLib::SaveData.MasterVolume);

	gxOrderManager::GetInstance()->RequestInit();

}

namespace
{
	void WaitUpToSec(float endTime, int restMilliSec = 0)
	{
		auto curTime	= CGameGirl::GetInstance()->GetTime();
		auto restTime	= endTime - curTime;
		if (restTime > 0)
		{
			int sleep_millisec = int(restTime * 1000.0f);
			if (sleep_millisec > restMilliSec)
			{
			    CDeviceManager::GetInstance()->Sleep(sleep_millisec - restMilliSec);
				curTime		= CGameGirl::GetInstance()->GetTime();
				restTime	= endTime - curTime;
			}

			if (restTime > 0 && restMilliSec)
			{
				do {
					curTime = CGameGirl::GetInstance()->GetTime();
					//CDeviceManager::GetInstance()->Sleep(0);
				} while (curTime < endTime);
			}
		}
	}
}

void CGameGirl::Action()
{
	m_bStartGameGirl = gxTrue;

	//MULTIスレッド時は別スレッドでゲームが回っている
	static gxBool m_bFirst = gxTrue;

	if (m_bFirst)
	{
		CDeviceManager::GetInstance()->MakeThread(GameMainThread, NULL);
        CDeviceManager::GetInstance()->MakeThread(GameFileThread, NULL);
        CDeviceManager::GetInstance()->MakeThread(GameNetWorkThread, NULL);
		GX_DEBUGLOG("[GameGirl] Activate MultiThread.");

		gxLib::LoadTexture(0, "bg.tga", 0xff00ff00, 0, 0, &g_bgw, &g_bgh);
		gxLib::UploadTexture();

		m_bFirst = gxFalse;
	}

 #if RENDER_FPS != FRAME_PER_SECOND
	if (gxRender::GetInstance()->GetRenderFPS() >= FRAME_PER_SECOND)
	{
		m_bDrawSw = gxTrue;
	}
 #else
	m_bDrawSw = gxTrue;
 #endif

	if (!IsExist())
	{
		GX_DEBUGLOG("[GameGirl] Accept Exit Signal.");
		if (CDeviceManager::GetInstance()->m_ThreadNum.load() > 0)
		{
			CDeviceManager::GetInstance()->Sleep(10);
		}
		else
		{
			m_bAppFinish = gxTrue;
		}
		return;
	}

	gxNetworkManager::GetInstance()->Action();

	if(gxTexManager::GetInstance()->IsUploadTextureExist())
	{
		gxTexManager::GetInstance()->UploadTexture();	//スレッド使うとどうもバッティングする？
	}

	soundMain();

 #if RENDER_FPS != FRAME_PER_SECOND // render_fps = 30or60 前提.
	if (gxRender::GetInstance()->GetRenderFPS() < FRAME_PER_SECOND)
	{
		m_bDrawSw = !m_bDrawSw;
	}
 #endif

	//リクエストの受付を締め切っていれば描画開始
	if( m_bDrawSw && gxOrderManager::GetInstance()->DrawInit() )
	{
		gxDebug::GetInstance()->Action();

		drawMain();

		gxOrderManager::GetInstance()->Reset();

		gxDebug::GetInstance()->AddDrawCount();

		gxOrderManager::GetInstance()->DrawEnd();
	}
	else
	{
		m_bDrawSw = gxFalse;
		//if (IsPause()) break;
	}

	VSync();		// vsync待ち風時間待ち.

	if (m_bDrawSw)	// 描画したときのみ flip;
	{
		flip();		//FLIPは描画タイミングで毎回行う
	}

	m_fVsyncSec = CGameGirl::GetInstance()->GetTime();
	GameMainVsyncWakeup();	// GameMainThread 側の VsyncWait での待ち終了合図.

	if( m_bResume )
	{
		resume();
		m_bResume = gxFalse;
	}
}


void CGameGirl::GameMain()
{
	//ゲーム用のサブスレッドの中身
	{
		static Sint32 OldWinW = 0, OldWinH = 0;
		static int drawCnt = 0;
		Sint32 WinW = 0, WinH = 0;
		Sint32 GameW = 0, GameH = 0;
		CGameGirl::GetInstance()->GetWindowsResolution(&WinW, &WinH);
		CGameGirl::GetInstance()->GetGameResolution(&GameW, &GameH);

		Float32 fw = 1.0f * WinW / g_bgw;
		Float32 fh = 1.0f * WinH / g_bgh;
		Float32 fScale = (fw < fh) ? fw : fh;

		if ((OldWinW != WinW) || (OldWinH != WinH))
		{
			drawCnt = 10;
		}
		else if (CGameGirl::GetInstance()->IsNeedReDraw())
		{
			drawCnt = 10;
		}

		if ( drawCnt > 0 )
		{
			CGameGirl::GetInstance()->SetNeedReDraw(0);
			drawCnt--;
			Uint32 argb = ARGB_DFLT;
			
			//argb = ARGB(0xff, gxLib::Rand()%256,gxLib::Rand()%256,gxLib::Rand()%256);
			
			if (WinW < WinH)	//((1.0f*WinW / WinH) > (1.0f * GameW / GameH) )//(fw > fh)
			{
				//int w = WinW - GameW;
				//w = w / 2;
				//fScale = fh;
				//if (fScale < 1.0) fScale = 1.0f;
				//gxLib::PutSprite(0, 0, -255,
				//	0 * 64, 0, 0, w, g_bgh, 0, 0,
				//	ATR_DFLT, argb, 0, fScale, fScale);
				//
				//int u = (g_bgw - w);
				//if (u < 0) u = 0;
				//if (w >= g_bgw) w = g_bgw;
				//gxLib::PutSprite(WinW, 0, -255,
				//	0 * 64, u, 0, w, g_bgh, w, 0,
				//	ATR_DFLT, argb, 0, fScale, fScale);

				fScale = fh;
				gxLib::PutSprite(WinW / 2, 0, -255,
					0 * 64, 0, 0, g_bgw, g_bgh / 2, g_bgw / 2, 0,
					ATR_DFLT, argb, 0, fScale, fScale);

				gxLib::PutSprite(WinW / 2, WinH, -255,
					0 * 64, 0, g_bgh / 2, g_bgw, g_bgh / 2, g_bgw / 2, g_bgh / 2,
					ATR_DFLT, argb, 0, fScale, fScale);

			}
			else
			{
				fScale = fw;
				gxLib::PutSprite(WinW/2, 0, -255,
					0 * 64, 0, 0, g_bgw, g_bgh/2, g_bgw/2, 0,
					ATR_DFLT, argb, 0, fScale, fScale);

				gxLib::PutSprite(WinW / 2, WinH, -255,
					0 * 64, 0, g_bgh/2, g_bgw, g_bgh / 2, g_bgw / 2, g_bgh / 2,
					ATR_DFLT, argb, 0, fScale, fScale);
			}
		}
		OldWinW = WinW;
		OldWinH = WinH;
	}

	if( m_bResetButton )
	{
		//リセットが押された
		GX_DEBUGLOG("[GameGirl] Accept Reset Signal.");
		m_bResetButton = gxFalse;
		::GameReset();
	}

	if( IsPause() )
	{
		//ポーズ中
		if( m_sStepFrm == 0 )
		{

		}
		else
		{
			m_sStepFrm ++;
			goto exit;
		}
	}

	m_sStepFrm ++;

	{
		gxOrderManager::GetInstance()->SetRequestEnable( gxTrue );
	}

	//システム更新

	CDeviceManager::GetInstance()->GameInit();

	//入力更新

	inputMain();

	//ゲームメイン

	if (!gameMain())
	{
		m_bMainLoop = gxFalse;
	}


	//Drag&Drop処理

	if( gxFileManager::GetInstance()->IsDragEnable() )
	{
		//マルチスレッド時にコンフリクトするのでブロックする

		for( Sint32 ii=0; ii<gxFileManager::GetInstance()->GetDropFileNum(); ii++ )
		{
			gxBool DragAndDrop(char* szFileName);
			gxChar* pFileName = gxFileManager::GetInstance()->GetDropFileName( ii );

			DragAndDrop( pFileName );
		}

		gxFileManager::GetInstance()->ClearDropFiles();
	}


	gxUIManager::GetInstance()->Action();
	gxUIManager::GetInstance()->Draw();

	if( m_bExit )
	{
		m_bMainLoop = gxFalse;
	}

	gxDebug::GetInstance()->AddAction();

	Sint32 odrNum, subNum;
	gxOrderManager::GetInstance()->GetUpdateOrderNum(&odrNum, &subNum);
	gxDebug::GetInstance()->UpdateRenderInfo( odrNum, subNum );

	gxOrderManager::GetInstance()->ChangeRequestBuffer();

exit:

	if( m_DelayPauseFrm > 0 )
	{
		m_DelayPauseFrm --;
		if( m_DelayPauseFrm == 0 )
		{
			m_bHardPause = m_DelayHardPause;
		}
	}

}



gxBool CGameGirl::network()
{
	//ネットワーク処理

	static Sint32 seq = 0;
	Float32 fRatio = 0.0f;

	//ネットワーク接続要求
	static Sint32 index=0;

#if 1
	//IPアドレス取得
	switch( seq ){
	case 0:
		m_bOnLine = gxFalse;
		CGameGirl::GetInstance()->SetIPAddressV4(0, 0);
		seq = 100;
		break;

	case 100:
		index = gxUtil::OpenWebFile( GLOBAL_IP_ADDRESS_CHECK_URL );
		seq = 200;
		break;

	case 200:
		if (!gxUtil::IsDownloadWebFile(index, &fRatio))
		{
			if (gxUtil::IsDownloadSucceeded(index))
			{
				Uint8* pData;
				size_t uSize;

				pData = gxUtil::GetDownloadWebFile(index, &uSize);

				if (pData)
				{
					m_bOnLine = gxTrue;
					CCsv* pCsv = new CCsv();
					pCsv->Read(pData, uSize);
					gxChar* pString = pCsv->GetCell(5, 0);
					gxChar buf[FILENAMEBUF_LENGTH];
					sprintf(buf, "0x%s", pString);
					Uint32 ip = strtoul(buf, NULL, 16);
					CGameGirl::GetInstance()->SetIPAddressV4(ip, 0);
					SAFE_DELETE( pCsv );
					seq = 999;

					SAFE_DELETES(pData);
				}
				gxUtil::CloseWebFile(index);
				seq = 999;
			}
			else
			{
				m_bOnLine = gxFalse;
				seq = 999;
			}
		}
		break;

	case 999:
	default:
		break;
	}
#endif
	CDeviceManager::GetInstance()->NetWork();

	return gxFalse;

}

void CGameGirl::End()
{
	if (m_bEndComplete) return;

	gxLib::SaveConfig();

	{
		GX_DEBUGLOG("[GameGirl] Application Finish.");

		gameEnd();
	}

	m_bEndComplete = gxTrue;
}


void CGameGirl::resume()
{
	GX_DEBUGLOG("[GameGirl] Accept Resume Signal.");

	CDeviceManager::GetInstance()->Resume();

}


gxBool CGameGirl::drawInit()
{
	GX_DEBUGLOG("[GameGirl] drawInit.");

	gxTexManager::GetInstance();
	gxOrderManager::GetInstance();
	gxRender::GetInstance();

	return gxTrue;
}

gxBool CGameGirl::drawMain()
{
	Float32 fTime = GetTime();

	gxRender::GetInstance()->Action();

	CDeviceManager::GetInstance()->Render();

	fTime = GetTime() - fTime;

	gxDebug::GetInstance()->UpdateRenderTime( fTime );

	return gxTrue;
}

gxBool CGameGirl::drawEnd()
{
	GX_DEBUGLOG("[GameGirl] drawEnd.");

	gxTexManager::DeleteInstance();
	gxOrderManager::DeleteInstance();
	gxRender::DeleteInstance();

	return gxTrue;
}


gxBool CGameGirl::soundInit()
{
	GX_DEBUGLOG("[GameGirl] soundInit.");

	gxSoundManager::GetInstance();
	return gxTrue;
}


gxBool CGameGirl::soundMain()
{
	gxSoundManager::GetInstance()->Action();
	gxSoundManager::GetInstance()->Play();

	return gxTrue;
}

gxBool CGameGirl::soundEnd()
{
	GX_DEBUGLOG("[GameGirl] soundEnd.");

	gxSoundManager::DeleteInstance();

	return gxTrue;
}


gxBool CGameGirl::inputInit()
{
	GX_DEBUGLOG("[GameGirl] inputInit.");
	gxPadManager::GetInstance();

	return gxTrue;
}

gxBool CGameGirl::inputMain()
{
	gxPadManager::GetInstance()->Action();

	if( gxLib::Joy(0)->psh&BTN_SELECT && gxLib::Joy(0)->trg&BTN_START )
	{
		CGameGirl::GetInstance()->SetReset();
	}
	gxUtil::Update();

	return gxTrue;
}

gxBool CGameGirl::inputEnd()
{
	GX_DEBUGLOG("[GameGirl] inputEnd.");
	gxPadManager::DeleteInstance();

	return gxTrue;
}

int WINDOW_W = SCREEN_WIDTH;
int WINDOW_H = SCREEN_HEIGHT;
int GameScreenMode = 0;

enum ScreenRotation {
	ROTATION_PORTRAIT,
	ROTATION_LANDSCAPE
};

void AdjustMultiResolution()
{
    Sint32 gamew = WINDOW_W, gameh = WINDOW_H;
    Sint32 winw = WINDOW_W, winh = WINDOW_H;

	CGameGirl::GetInstance()->GetGameResolution(&gamew, &gameh);
	CGameGirl::GetInstance()->GetWindowsResolution(&winw, &winh);
	
	if(winw > winh)
    {
		//横向き時
        GameScreenMode = ROTATION_LANDSCAPE;
    }
    else
    {
		//縦向き時
        GameScreenMode = ROTATION_PORTRAIT;
	}

	//SCREEN_WIDTH,SCREEN_HEIGHTの縦横比によって縦横のデフォルトを決定する
	ScreenRotation rotationDefault = ROTATION_LANDSCAPE;
	if( SCREEN_WIDTH < SCREEN_HEIGHT )
	{
		rotationDefault = ROTATION_PORTRAIT;
	}

#ifdef ENABLE_MULTI_RESOLUTION
	//縦持ち、横持ちで解像度が異なる場合
	int w1 = SCREEN_WIDTH;		//100
	int h1 = SCREEN_HEIGHT;		//200

	int w2 = SCREEN_WIDTH2;		//150
	int h2 = SCREEN_HEIGHT2;	//250

//	if(w1 < h1 ) SWAP(w1,h1);
//	if(w2 < h2 ) SWAP(w2,h2);

	int w=0,h=0;

	if( GameScreenMode == ROTATION_LANDSCAPE )
	{
		if(rotationDefault == ROTATION_LANDSCAPE)
		{
			//確認済み
			w = w1;
			h = h1;
		}
		else
		{
			w = w2;
			h = h2;
		}
	}
	else if( GameScreenMode == ROTATION_PORTRAIT )
	{
		if( rotationDefault == ROTATION_PORTRAIT )
		{
			w = w1;
			h = h1;
		}
		else
		{
			//確認済み
			w = w2;
			h = h2;
		}
	}
	WINDOW_W = w;
	WINDOW_H = h;
	CGameGirl::GetInstance()->AdjustScreenResolution();
#else

	//縦横ともに同じ解像度の時
    CGameGirl::GetInstance()->AdjustScreenResolution();

#endif

}

gxBool CGameGirl::gameMain()
{
	//--------------------------------------------
	//ゲームのアップデート処理
	//--------------------------------------------

    AdjustMultiResolution();
    
    gxBool bExist = gxTrue;

	if( m_bThroughGameMain )
	{
		//一時的に処理を通したくないときを制御する
		m_bThroughGameMain = gxFalse;
	}
	else
	{
		//BG描画
#ifdef BGV_ON

		Sint32 winw,winh;
		CGameGirl::GetInstance()->GetWindowsResolution(&winw, &winh);

		for (Sint32 yy = -200; yy < winh; yy += 100)
		{
			for (Sint32 xx = -200; xx < winw; xx += 200)
			{
				Sint32 ax = ((yy / 100) % 2 == 0) ? 100 + xx : xx;

				ax += gxLib::GetGameCounter() % 100;
				Sint32 ay = gxLib::GetGameCounter() % 100;

				gxLib::DrawBox(ax, yy + ay, ax + 100, yy + 100+ay, -100, gxTrue, ATR_DFLT, 0xff808080);
			}
		}
#endif

		if( m_bSoftPause )
		{
			bExist = CDeviceManager::GetInstance()->GamePause();

			::GamePause();

		}
		else
		{
			{
				static Sint32 InitSeq = 0;
				Sint32 gamew = WINDOW_W, gameh = WINDOW_H;
				Sint32 winw = WINDOW_W, winh = WINDOW_H;

				CGameGirl::GetInstance()->GetGameResolution(&gamew, &gameh);
				CGameGirl::GetInstance()->GetWindowsResolution(&winw, &winh);
				static Float32 LogoAlpha = 1.0f;
				static Float32 LogoScale = 1.0f;
				Float32 fScale = winw / 1024.0f * 0.75f;
#ifdef DRAW_LOGO
				gxLib::DrawBox(0, 0, winw, winh, MAX_PRIORITY_NUM, gxTrue, ATR_DFLT, SET_ALPHA(LogoAlpha, 0xff010101));
				gxLib::PutSprite(winw / 2, winh / 2, MAX_PRIORITY_NUM, (MAX_MASTERTEX_NUM - 1) * 64, 0, 512, 1024, 512, 512, 256, ATR_DFLT, SET_ALPHA(LogoAlpha,ARGB_DFLT), 0.0f,  fScale* LogoScale, fScale* LogoScale);
#endif
				switch (InitSeq){
				case 0:
					//システムデータを読み込む
					if( CDeviceManager::GetInstance()->IsBatchMode() )
					{
						InitSeq = 100;
					}
					else
					{
						gxLib::ReadAudio(MAX_SOUND_NUM - 1, &bootTbl[0], GetbootTblSize());
						gxLib::ReadTexture((MAX_MASTERTEX_NUM - 1) * 64, &iconsTbl[0], GeticonsTblSize());
						gxLib::UploadTexture();
						InitSeq = 10;
					}
					break;
				case 10:
#ifdef DRAW_LOGO
					gxLib::PlayAudio(MAX_SOUND_NUM - 1);
#endif
					InitSeq = 50;
					break;

				case 50:
					LogoScale += 0.001f;
#ifdef DRAW_LOGO
					if (!gxUIManager::GetInstance()->IsFade())
#endif
					{
						m_pFontManager->Init();
						InitSeq = 60;
					}
					break;

				case 60:
					InitSeq = 100;
				break;

				case 100:

					if (::GameInit(DragAndDropArgs))
					{
						gxUIManager::GetInstance()->Start();
						DragAndDropArgs.clear();
						InitSeq = 999;
					}
					break;

				case 999:
					LogoAlpha -= 0.01f;
					LogoAlpha = CLAMP(LogoAlpha, 0.0f, 1.0f);
					bExist = ::GameMain();
					CDeviceManager::GetInstance()->GameUpdate();
					break;
				}

			}
		}

		m_uGameCounter ++;
	}

	gxDebug::GetInstance()->Draw();

	return bExist;
}


gxBool CGameGirl::gameEnd()
{
	GX_DEBUGLOG("[GameGirl] gameEnd.");
	::GameEnd();

	return gxTrue;
}


/// Vsync 待ち風 時間待ち.
/// ※ 全てのハードで 実Vsync 待ちでなく 時間待ちだったので、
///    ゲーム側の Wait 処理に統一.
///  
gxBool CGameGirl::VSync()
{
	if( !CGameGirl::GetInstance()->IsWaitVSync() ) return gxFalse;

#if defined(PLATFORM_IOS) || defined(PLATFORM_ANDROID)
	// Mobileは 60FPS 毎にUpdateされる関数で呼ばれる前提に実1FPSより十分短く待つ(sleepする)
	WaitUpToSec(m_fVsyncSec + 0.75f/FRAME_PER_SECOND, 1);
#elif 1	// Windows では ほぼ正確に 1FPS の待ちを行う.
	WaitUpToSec(m_fVsyncSec + 1.0f/FRAME_PER_SECOND, 1);
#else
	CDeviceManager::GetInstance()->vSync();
#endif

	return gxTrue;
}


gxBool CGameGirl::flip()
{
	CDeviceManager::GetInstance()->Flip();
	return gxTrue;
}


void CGameGirl::AdjustScreenResolution()
{
	//画面サイズに応じたゲーム解像度を設定する
	Float32 sw, sh;

	//Windowの解像度

	sw = m_WindowScreenWidth;
	sh = m_WindowScreenHeight;

	if (m_ScreenMode == enScreenModeAspect)
	{
		//アスペクト比を維持した変形

		//まずは横に合わせる
		float fRatio = sw / WINDOW_W;

		m_GameScreenWidth = WINDOW_W*fRatio;
		m_GameScreenHeight = WINDOW_H*fRatio;

		if (m_GameScreenHeight > sh)
		{
			//縦幅が画面からはみ出した場合
			fRatio = sh / WINDOW_H;

			//縦に合わせる
			m_GameScreenWidth = WINDOW_W * fRatio;
			m_GameScreenHeight = WINDOW_H * fRatio;
		}
	}
	else if (m_ScreenMode == enScreenModeFull)
	{
		//フル画面

		m_GameScreenWidth = sw;
		m_GameScreenHeight = sh;
	}
	else
	{
		//オリジナルサイズ

		m_GameScreenWidth = WINDOW_W;
		m_GameScreenHeight = WINDOW_H;
	}

}

gxChar* CGameGirl::GetScreenShotFileName()
{
	if (m_ScreenShotFileName[0])
	{
		return m_ScreenShotFileName;
	}

	return NULL;
}


void    CGameGirl::SetScreenShot( gxBool bShot )
{
	static Sint32 _snapNum = 0;

	if (bShot)
	{
		Sint32 id = _snapNum;
		sprintf(m_ScreenShotFileName, "%03d_%s.tga", id , APPLICATION_NAME );
		_snapNum++;
	}
	else
	{
		m_ScreenShotFileName[0] = 0x00;
	}

}

void CGameGirl::SetDebugModePage(Sint32 id)
{
	m_bToggleDeviceMode = gxTrue;
	gxDebug::GetInstance()->SetDebugPage(id);
}


namespace detail
{
	static const auto s_appStartTime = std::chrono::system_clock::now();
}


Float32 CGameGirl::GetTime( gxClock *pClock )
{
    auto   now          = std::chrono::system_clock::now();
    auto   elapsed      = now - detail::s_appStartTime;
    Uint64 microseconds = std::chrono::duration_cast<std::chrono::microseconds>(elapsed).count();

    if (pClock != nullptr)
    {
        auto duration_since_epoch = now.time_since_epoch();
        auto seconds 			= std::chrono::duration_cast<std::chrono::seconds>(duration_since_epoch);
        auto milliseconds 		= std::chrono::duration_cast<std::chrono::milliseconds>(duration_since_epoch - seconds);
        auto microseconds_part 	= std::chrono::duration_cast<std::chrono::microseconds>(duration_since_epoch - seconds - milliseconds);

        std::time_t 	now_c   = std::chrono::system_clock::to_time_t(now);
        std::tm 		local_tm;
        #if defined(_MSC_VER)
            localtime_s(&local_tm, &now_c);
        #else	// POSIX
            localtime_r(&now_c, &local_tm);
        #endif

        pClock->Year  = Uint32(local_tm.tm_year + 1900);
        pClock->Month = Uint32(local_tm.tm_mon + 1);
        pClock->Day   = Uint32(local_tm.tm_mday);
        pClock->DOW   = Uint32(local_tm.tm_wday);      // 0-6 (日曜日が 0)
        pClock->Hour  = Uint32(local_tm.tm_hour);
        pClock->Min   = Uint32(local_tm.tm_min);
        pClock->Sec   = Uint32(local_tm.tm_sec);
        pClock->MSec  = Uint32(milliseconds.count());
        pClock->USec  = Uint32(microseconds_part.count());
    }

    return Float32(microseconds / 1000000.0f);
}



void* GameNetWorkThread(void*)
{
	++CDeviceManager::GetInstance()->m_ThreadNum;
	while (CGameGirl::GetInstance()->IsExist())
	{
		CGameGirl::GetInstance()->network();
		CDeviceManager::GetInstance()->Sleep(4);
	}

	--CDeviceManager::GetInstance()->m_ThreadNum;

	return NULL;
}


void* GameFileThread(void*)
{
	//ファイルスレッド

	++CDeviceManager::GetInstance()->m_ThreadNum;
	while (CGameGirl::GetInstance()->IsExist())
	{
		gxFileManager::GetInstance()->Action();

		CDeviceManager::GetInstance()->Sleep(1000/30);
	}

	--CDeviceManager::GetInstance()->m_ThreadNum;

	return NULL;
}


static void GameMainVsyncWait();

namespace
{
    std::atomic_flag		s_GameMainVsync_wakeup;
	Float32					s_GameMainVsync_time;
}

void* GameMainThread( void *pArgs )
{
	//このサブスレッドでゲームがずっと回る
	++CDeviceManager::GetInstance()->m_ThreadNum;
	CGameGirl::GetInstance()->SubThreadExist( gxTrue );

	s_GameMainVsync_time = CGameGirl::GetInstance()->GetTime();

	while( CGameGirl::GetInstance()->IsExist() )
	{
		CGameGirl::GetInstance()->m_GameMainThreadExist = gxTrue;

		//CGameGirl::GetInstance()->SetDeltaTime( diffTime );	//実質未使用だったのでコメントアウト.
		CGameGirl::GetInstance()->GameMain();

		GameMainVsyncWait();
	}

	CGameGirl::GetInstance()->SubThreadExist( gxFalse );

	GX_DEBUGLOG("[GameGirl] Exit Multi Thread");

	CGameGirl::GetInstance()->m_GameMainThreadExist = gxFalse;

	--CDeviceManager::GetInstance()->m_ThreadNum;

	return NULL;
}

void GameMainVsyncWakeup()
{
    if (!std::atomic_flag_test_and_set(&s_GameMainVsync_wakeup))
	{
	   ;
	}
}

static void GameMainVsyncWait()
{
	if (!s_GameMainVsync_wakeup.test())
	{
	 #if 0	// メインスレッドとは非同期.
		auto endTime	= s_GameMainVsync_time + 1.0f / FRAME_PER_SECOND;
		WaitUpToSec(endTime, 1);
	 #else // メインスレッドとの同期を行う.
		auto endTime	= s_GameMainVsync_time + 0.8f / FRAME_PER_SECOND;
		WaitUpToSec(endTime);
		auto curTime	= CGameGirl::GetInstance()->GetTime();
		endTime			= s_GameMainVsync_time + 1.2f / FRAME_PER_SECOND;
		auto restTime	= endTime - curTime;
		if (restTime > 0)
		{
			do {
				curTime = CGameGirl::GetInstance()->GetTime();
			} while (curTime < endTime && !s_GameMainVsync_wakeup.test() );
		}
	 #endif
	}
    std::atomic_flag_clear(&s_GameMainVsync_wakeup);
	s_GameMainVsync_time = CGameGirl::GetInstance()->GetTime();
}


extern int GameScreenMode;
gxBool gxLib::IsPortrait()
{
	if( GameScreenMode == 0 )
	{
		return gxTrue;
	}

	return gxFalse;
}

gxBool gxLib::IsLandscape()
{
	if( gxLib::IsPortrait() )
	{
		return gxFalse;
	}

	return gxTrue;
}
