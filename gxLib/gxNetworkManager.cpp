//ネットワーク管理
#include <gxLib.h>
#include "gxNetworkManager.h"
#include "util/gxUIManager.h"

SINGLETON_DECLARE_INSTANCE( gxNetworkManager );

Sint32 gxNetworkManager::OpenURL( const gxChar *pOpenURL , const gxChar *pPostData, size_t PostSize , std::function<gxBool(gxBool bSuccess , uint8_t *p , size_t size)> func )
{
	Sint32 index = m_sHttpRequestMax;

	sprintf( m_pHttp[ index ].m_URL ,"%s" , pOpenURL );

	m_pHttp[ index ].uFileSize    = 0;
	m_pHttp[ index ].uReadFileSize = 0;
	m_pHttp[ index ].bClose = gxFalse;
	m_pHttp[ index ].pData    = NULL;
	m_pHttp[ index ].bRequest = gxTrue;
	m_pHttp[ index ].SetSeq(ESeq::eHTTPSeqInit);
	m_pHttp[ index ].index    = index;
	m_pHttp[ index ].func     = func;

	if( PostSize > 0 )
	{
		SAFE_DELETES(m_pHttp[index].PostData);
		m_pHttp[ index ].PostData = new gxChar[ PostSize ];
		gxUtil::MemCpy( m_pHttp[index].PostData , (void*)pPostData , PostSize );
	}

	m_sHttpRequestMax ++;

	return index;
}

gxBool gxNetworkManager::CloseURL(Uint32 uIndex)
{
	m_pHttp[uIndex].bClose = gxTrue;
	m_pHttp[uIndex].pData  = nullptr;	//Closeまでしてくれているということは外部で処理されたはず

	return gxTrue;
}


void gxNetworkManager::Action()
{
	std::lock_guard<std::mutex> lock(m_UpdateLock);

	if( m_pHttp.size() > 0 )
	{
		gxUIManager::GetInstance()->NowNetworking();

		for (auto itr = m_pHttp.begin(); itr != m_pHttp.end(); )
		{
			if (itr == m_pHttp.end()) break;

			if (itr->second.GetSeq() == ESeq::eHTTPSeqLoadEnd )
			{
				if( itr->second.func )
				{
					gxBool bDeleteFile = gxFalse;
					bDeleteFile = itr->second.func(
						(itr->second.Error == EError::Success)? gxTrue : gxFalse,
						itr->second.pData ,
						itr->second.uFileSize );

					if (bDeleteFile)
					{
						if (itr->second.pData)
						{
							SAFE_DELETES(itr->second.pData);
							SAFE_DELETES(itr->second.PostData);
						}
					}
				}

				itr->second.SetSeq(ESeq::eHTTPSeqClose);
			}

			if (itr->second.bClose)
			{
				SAFE_DELETES(itr->second.PostData);
				SAFE_DELETES(itr->second.pData);

				itr = m_pHttp.erase(itr);
                continue;
			}
			++itr;
		}
	}
}


gxNetworkManager::StHTTP * gxNetworkManager::GetNextReq()
{
	std::lock_guard<std::mutex> lock(m_UpdateLock);

	if (m_pHttp.size() == 0) return NULL;


	for (auto itr = m_pHttp.begin(); itr != m_pHttp.end(); itr++ )
	{
		if(itr->second.GetSeq()== ESeq::eHTTPSeqClose) continue;

        if(itr->second.bRequest )
		{
            itr->second.bRequest = false;
			return &itr->second;
		}
	}


	return NULL;
}

