﻿//--------------------------------------------------
//
// gx.h
// 
//
//--------------------------------------------------
#ifndef _GX_H_
#define _GX_H_

#define STEP_REPEAT_FRAME (5)

class CFontManager;
class CGameGirl
{
public:

	enum eScreenMode {
		enScreenModeAspect,
		enScreenModeFull,
		enScreenModeOriginal,
	};

	CGameGirl();
	~CGameGirl();

	void makeWindow();

	void resume();

	void Exit()
	{
		m_bMainLoop = false;
	}

	void Init();
	void Action();
	void GameMain();
	void End();

	void SetResume()
	{
		m_bResume = gxTrue;
	}
	gxBool IsResume()
	{
		return m_bResume;
	}

	gxBool IsPause()
	{
		return m_bHardPause;
	}
	gxBool IsNeedReDraw()
	{
		return (m_NeedRedrawWallPaper>0)? gxTrue : gxFalse;
	}
	void SetNeedReDraw( Sint32 cnt = 5)
	{
		m_NeedRedrawWallPaper = 5;
	}
	void SetPause( gxBool bPause , Sint32 delayFrm = 0 )
	{
		if( delayFrm == 0 )
		{
			m_bHardPause = bPause;
		}
		else
		{
			m_DelayPauseFrm = delayFrm;
			m_DelayHardPause = bPause;
		}
	}

	void SetSoftPause( gxBool bPause )
	{
		m_bSoftPause = bPause;
	}
	gxBool IsSoftPause()
	{
		return m_bSoftPause;
	}

	void PauseStep()
	{
		if( m_sStepFrm >= STEP_REPEAT_FRAME ) m_sStepFrm = 0;
	}

	void StopGameMain()
	{
		m_bThroughGameMain = gxTrue;
	}

	Uint32 GetCounter()
	{
		return m_uGameCounter;
	}

	//void GetMemoryRemain(size_t* uNow , size_t* uTotal , size_t* uMax ){};

	gxBool IsExist()
	{
		return m_bMainLoop;
	}

	void SetBenchmarkTime( Float32 fTime  )
	{
		m_fBenchmarkTime = fTime;
	}

	Float32 GetBenchmarkScore()
	{
		Float32 fScore = 100.0f / (m_fBenchmarkTime / 1.19099998f);

		return fScore;
	}

	Float32 GetTime( gxClock *pClock = nullptr );

	void SetReset()
	{
		m_bResetButton = gxTrue;
	}

	void SetPadConfig( gxBool bModeOn = gxTrue )
	{
		m_bPadDeviceConfigMode = bModeOn;
	}

	gxBool IsPadConfigMode()
	{
		return m_bPadDeviceConfigMode;
	}

	gxBool IsInitCompleted()
	{
		return  m_bInitializeCompleted;
	}

	void WaitVSync( gxBool bWaitOn = gxTrue )
	{
		m_bWaitVSync = bWaitOn;
	}

	gxBool IsWaitVSync()
	{
		return m_bWaitVSync;
	}

	Uint32 GetIPAddressV4( gxBool bLocal = gxTrue )
	{
		if( bLocal )
		{
			return m_uLocalIP;
		}
		else
		{
			return m_uGlobalIP;
		}

		return 0x00000000;
	}

	void SetIPAddressV4( Uint32 globalIP , Uint32 localIP )
	{
		m_uGlobalIP = globalIP;
		m_uLocalIP  = localIP;
	}

	void SetUID( Uint32 uid )
	{
		m_uGlobalUID = uid;
	}

	Uint32 GetUID()
	{
		return m_uGlobalUID;
	}

	gxBool IsOnline()
	{
		return m_bOnLine;
	}

	void SubThreadExist( gxBool bExist )
	{
		m_bSubThread = bExist;
	}

	gxBool IsSubThreadExist()
	{
		return m_bSubThread;
	}

	gxBool ToggleDeviceMode()
	{
		m_bToggleDeviceMode = !m_bToggleDeviceMode;
		return m_bToggleDeviceMode;
	}

	void SetDebugModePage(Sint32 id);

	gxBool IsDeviceMode()
	{
		return m_bToggleDeviceMode;
	}

	gxBool IsAppFinish()
	{
		return m_bAppFinish;
	}

	CFontManager* GetFontManager()
	{
		return m_pFontManager;
	}

	gxBool VSync();

	static gxBool IsEndApp()
	{
		if (s_pInstance == NULL) return gxTrue;
		return gxFalse;
	}

	//-------------------------------------------------------
	//画面サイズ関連
	//-------------------------------------------------------

	void AdjustScreenResolution();
	void GetGameResolution( Sint32 *w , Sint32 *h )
	{
		*w = m_GameScreenWidth;
		*h = m_GameScreenHeight;
	}

	void GetWindowsResolution( Sint32 *w , Sint32 *h )
	{
		*w = m_WindowScreenWidth;
		*h = m_WindowScreenHeight;
	}

	void SetScreenMode( eScreenMode mode )
	{
		m_ScreenMode = mode;
	}

	void SetWindowSize( Sint32 w , Sint32 h )
	{
		m_WindowScreenWidth  = w;
		m_WindowScreenHeight = h;
	}

	void Set3DView( gxBool b3DOn )
	{
		m_b3DView = b3DOn;
	}

	gxBool Is3DView()
	{
		return m_b3DView;
	}

	void SetDeltaTime( Float32 delta )
	{
		m_fDeltaTime = delta / (1.0f / FRAME_PER_SECOND );
	}

	Float32 GetDeltaTime()
	{
		return m_fDeltaTime;
	}

	gxBool IsStartGameGirl()
	{
		return m_bStartGameGirl;
	}

	gxBool IsGameMainThreadExist()
	{
		return m_GameMainThreadExist;
	}

	gxBool network();

	gxChar* GetScreenShotFileName();
	void    SetScreenShot(gxBool bShot);

	std::vector<std::string> DragAndDropArgs;

	gxBool m_GameMainThreadExist = gxFalse;
	int g_bgw=0, g_bgh=0;


	SINGLETON_DECLARE( CGameGirl );


private:


	void init();
	void main();
	void end();

	//Uint32  m_uTime[8];
	//Float32 m_fWorks[8];

	gxBool drawInit();
	gxBool drawMain();
	gxBool drawEnd();

	gxBool soundInit();
	gxBool soundMain();
	gxBool soundEnd();

	gxBool inputInit();
	gxBool inputMain();
	gxBool inputEnd();

	gxBool gameMain();
	gxBool gameEnd();

	gxBool flip();

//	gxBool network();

	Sint32 m_sStepFrm;

	gxBool m_bStartGameGirl = gxFalse;
	gxBool m_bMainLoop;
	gxBool m_bAppFinish;

	gxBool m_bResume;

	gxBool m_bHardPause = gxFalse;
	gxBool m_bSoftPause = gxFalse;
	gxBool m_bThroughGameMain;
	gxBool m_bDrawSw;
	Sint32 m_DelayPauseFrm = 0;
	gxBool m_DelayHardPause = gxFalse;

	Sint32 m_sTimer;
	Uint32 m_uGameCounter;
	Sint32 m_sFrameSkip;
	Float32 m_fVsyncSec;

	CFontManager *m_pFontManager;

	//メモリ管理
	Uint32 m_uMemoryTotal;
	Uint32 m_uMemoryMaximum;
	Uint32 m_uMemoryUse;

	gxClock m_StartTime;

	gxBool m_bResetButton;
	gxBool m_bPadDeviceConfigMode;

	gxBool m_bInitializeCompleted;

	gxBool m_bSubThread;
	gxBool m_bWaitVSync;
	Sint32 m_sRequestAccept;

	//ネットワーク用
	Uint32 m_uGlobalUID;
	Uint32 m_uGlobalIP;
	Uint32 m_uLocalIP;
	gxBool m_bOnLine;

	gxBool m_bToggleDeviceMode;

	Float32 m_fBenchmarkTime = 0.0f;

	//-------------------------------
	//ゲーム画面解像度
	//-------------------------------

	eScreenMode m_ScreenMode;

	Sint32 m_GameScreenWidth;
	Sint32 m_GameScreenHeight;

	//ウインドウ解像度

	Sint32 m_WindowScreenWidth;
	Sint32 m_WindowScreenHeight;

	//立体視
	gxBool m_b3DView;

	gxBool m_bExit = gxFalse;

	gxChar m_ScreenShotFileName[FILENAMEBUF_LENGTH] = {0};


	Float32 m_fDeltaTime = 0.0f;


	gxBool m_bEndComplete = gxFalse;
	Sint32 m_NeedRedrawWallPaper = gxFalse;
};


//extern CGameGirl gga;
//#include "gxDebug.h"

#endif
