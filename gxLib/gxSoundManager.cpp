//--------------------------------------------------
//
// gxSoundManager.cpp
// サウンドの管理を行います、フェード処理などもこちらで
// 管理するため、プラットフォーム側で持つフェード処理などを
// 独自に使うことを推奨しません
//
//--------------------------------------------------

#include <gxLib.h>
#include "gx.h"
#include "gxSoundManager.h"

#ifdef GX_DEBUG
//#define _NO_SOUND_
#endif

#define DEFAULT_MASTER_VOLUME (0.75f)
#define DEFAULT_ORIGINAL_VOLUME (0.75f)

SINGLETON_DECLARE_INSTANCE( gxSoundManager )

gxSoundManager::gxSoundManager()
{
	m_fMasterVolume   = DEFAULT_MASTER_VOLUME;
	m_fOldMasterVolume = m_fMasterVolume;

	for(Sint32 ii=0; ii<MAX_SOUND_NUM; ii++)
	{
		m_Info[ ii ].bOverWrap  = gxFalse;
		m_Info[ ii ].bReq       = gxFalse;
		m_Info[ ii ].bLoop      = gxFalse;
		m_Info[ ii ].bUse       = gxFalse;
		m_Info[ ii ].uStatus    = 0;
		m_Info[ ii ].fVolume    = DEFAULT_ORIGINAL_VOLUME;
		m_Info[ ii ].fVolumeAdd = 0.f;
		m_Info[ ii ].bPlayNow   = gxFalse;
		m_Info[ ii ].fFreqRatio = 1.0f;
		m_Info[ ii ].fPanning   = 0.0f;
		m_Info[ ii ].bReverb    = gxFalse;


		m_Info[ ii ].fileName[0] = 0x00;
	}

}


gxSoundManager::~gxSoundManager()
{
	ReleaseAllWaves();

	for(Sint32 ii=0; ii<MAX_SOUND_NUM; ii++)
	{

	}

}

void gxSoundManager::ReleaseAllWaves()
{
	//メモリ解放の順番的に先に開放しておく

	for(Sint32 ii=0; ii<MAX_SOUND_NUM; ii++)
	{
		m_Info[ ii ].m_WaveData.ReleaseImage();
	}

}

void gxSoundManager::Action()
{
	for(Sint32 ii=0; ii<MAX_SOUND_NUM; ii++)
	{
		StPlayInfo *p;

		p = &m_Info[ ii ];
		if( !p->bUse ) continue;

		p->fVolume += p->fVolumeAdd;

		if( p->bPlayNow && p->fVolume <= 0.f )
		{
			p->fVolume    = 0.f;
			p->fVolumeAdd = 0.f;

			//Stopのリクエスト
			p->bReq = gxTrue;
			p->uStatus |= enSoundReqStop;
			p->bPlayNow = gxFalse;
			p->bLoop = gxFalse;
		}
		else if( p->fVolume > 1.f )
		{
			p->fVolume    = 1.f;
			p->fVolumeAdd = 0.f;
			p->uStatus |= enSoundReqVolume;
		}
		else
		{
			if( p->fVolumeAdd || m_fOldMasterVolume != m_fMasterVolume )
			{
				p->bReq = gxTrue;
				p->uStatus |= enSoundReqVolume;
			}
		}

	}

	m_fOldMasterVolume = m_fMasterVolume;
}


gxBool gxSoundManager::LoadAudioFile( Uint32 uIndex , const gxChar* pFileName )
{
	//Audioファイルのロード

	if( uIndex < 0 || uIndex >= MAX_SOUND_NUM ) return gxFalse;

	size_t uSize = 0;

	Uint8* pMemory;

	pMemory = gxLib::LoadStorageFile( (gxChar*)pFileName, &uSize);

	if( pMemory == NULL )
	{
		//ファイルがなかった
		GX_DEBUGLOG("[%d:%s]の音楽ファイルが存在しません", uIndex , pFileName );
		m_Info[ uIndex ].bReq    = gxFalse;
		m_Info[ uIndex ].bUse    = gxFalse;
		m_Info[ uIndex ].uStatus = enSoundReqNone;
		m_Info[ uIndex ].fileName[0] = 0x00;
		return gxFalse;
	}

	GX_DEBUGLOG("[LoadAudioFile] %d , \"%s\"", uIndex , pFileName );

	sprintf( m_Info[ uIndex ].fileName , "%s" , pFileName );

	gxBool ret = ReadAudioFile( uIndex , pMemory, uSize);

	SAFE_DELETES( pMemory );

	return ret;

}


gxBool gxSoundManager::ReadAudioFile( Uint32 uIndex , Uint8* pMemory , size_t uSize )
{
	//Audioファイルをメモリから読み込む

	if( uIndex < 0 || uIndex >= MAX_SOUND_NUM ) return gxFalse;

	if( pMemory == NULL )
	{
		GX_DEBUGLOG("【gxLib::Error】サウンドデータバッファがNULLですよ");
		return gxFalse;
	}
	m_Info[ uIndex ].bUse = gxTrue;
	m_Info[ uIndex ].fVolume    = 0.5f* m_fMasterVolume;
	m_Info[ uIndex ].fVolumeAdd = 0.0f;
	m_Info[ uIndex ].fFreqRatio = 1.0f;
	m_Info[ uIndex ].bPlayNow   = gxFalse;
	m_Info[ uIndex ].bLoop      = gxFalse;

	m_Info[uIndex].m_WaveData.Read( (Uint8*)pMemory , uSize );

	m_Info[uIndex].uStatus |= enSoundReqLoad;	//LoadでOK

	//マルチスレッドで動いているときはステータスの変更を最後にしないとコピー中にサウンドのロードが走る

	m_Info[uIndex].bReq = gxTrue;

	return gxTrue;

}



gxBool gxSoundManager::PlayAudio( Uint32 uIndex , Float32 fVolume , gxBool bOverWrap , gxBool bLoop )
{
	if( uIndex < 0 || uIndex >= MAX_SOUND_NUM ) return gxFalse;

	m_Info[ uIndex ].bOverWrap = bOverWrap;
	m_Info[ uIndex ].bLoop     = bLoop;
	m_Info[ uIndex ].bPlayNow  = gxTrue;
	m_Info[ uIndex ].fVolume   = fVolume* m_fMasterVolume;
	m_Info[ uIndex ].fVolumeAdd  =0.0f;
	m_Info[ uIndex ].fFreqRatio = 1.0f;
	m_Info[ uIndex ].fPanning   = 0.0f;
	m_Info[ uIndex ].bReverb    = gxFalse;

	m_Info[ uIndex ].bReq      = gxTrue;
	m_Info[ uIndex ].uStatus |= enSoundReqPlay;
	m_Info[ uIndex ].uStatus |= enSoundReqVolume;

	return gxTrue;

}


gxBool gxSoundManager::StopAudio( Uint32 uIndex )
{
	if( uIndex < 0 || uIndex >= MAX_SOUND_NUM ) return gxFalse;

	m_Info[ uIndex ].bPlayNow  = gxFalse;
	m_Info[ uIndex ].bLoop     = gxFalse;

	m_Info[ uIndex ].bReq      = gxTrue;
	m_Info[uIndex].uStatus |= enSoundReqStop;

	return gxTrue;

}


gxBool gxSoundManager::SetVolume( Uint32 uIndex , Float32 fVol )
{
	if( uIndex < 0 || uIndex >= MAX_SOUND_NUM ) return gxFalse;

	m_Info[ uIndex ].bReq = gxTrue;
	if (m_Info[uIndex].fVolume == (fVol* m_fMasterVolume)) return gxTrue;
	m_Info[ uIndex ].fVolume = fVol* m_fMasterVolume;
	m_Info[ uIndex ].uStatus |= enSoundReqVolume;

	return gxTrue;
}

Float32 gxSoundManager::GetVolume(Uint32 index )
{
	if(index < 0 || index >= MAX_SOUND_NUM ) return 0.0f;

	return m_Info[index].fVolume;
}


gxBool gxSoundManager::SetFreq(Uint32 uIndex, Float32 fRatio)
{
	if (uIndex < 0 || uIndex >= MAX_SOUND_NUM) return gxFalse;

	m_Info[uIndex].bReq = gxTrue;
	m_Info[uIndex].fFreqRatio = fRatio;
	m_Info[uIndex].uStatus |= enSoundReqChangeFreq;

	return gxTrue;
}

gxBool gxSoundManager::SetFade( Sint32 uIndex , Float32 fVolume , Uint32 frm )
{
	if( uIndex < 0 || uIndex >= MAX_SOUND_NUM ) return gxFalse;

	if( frm <= 0 )
	{
		frm = 1;
		m_Info[ uIndex ].bReq       = gxTrue;
		m_Info[ uIndex ].fVolumeAdd = 1.0f;
		m_Info[ uIndex ].uStatus |= enSoundReqVolume;
		return gxTrue;
	}
	else
	{
		m_Info[ uIndex ].bReq = gxTrue;
		m_Info[ uIndex ].fVolumeAdd = (fVolume-m_Info[ uIndex ].fVolume)/frm;
		m_Info[ uIndex ].uStatus |= enSoundReqVolume;
	}

	return gxTrue;
}

gxBool gxSoundManager::IsPlay( Uint32 uIndex )
{
	if( uIndex < 0 || uIndex >= MAX_SOUND_NUM ) return gxFalse;

	if( !m_Info[ uIndex ].bUse ) m_Info[ uIndex ].bPlayNow = gxFalse;

	return m_Info[ uIndex ].bPlayNow;

}


gxBool gxSoundManager::SetMasterVolumeLevel( Float32 fLevel )
{
	fLevel = CLAMP(fLevel, 0.0f, 1.0f);

	m_fMasterVolume = fLevel;

	return gxTrue;
}


void gxSoundManager::Play()
{
#ifdef _NO_SOUND_
	return;
#endif
	CDeviceManager::GetInstance()->Play();
}


gxBool gxSoundManager::SetPanning( Uint32 uIndex, Float32 pan )
{
	if( uIndex < 0 || uIndex >= MAX_SOUND_NUM ) return gxFalse;
	if( pan < -1.0f ) pan = -1.0f;
	if( pan >  1.0f ) pan =  1.0f;

	m_Info[ uIndex ].bReq      = gxTrue;
	m_Info[ uIndex ].fPanning  = pan;
	m_Info[uIndex].uStatus |= enSoundReqChangePan;

	return gxTrue;
}


gxBool gxSoundManager::SetReverb( Uint32 uIndex, gxBool bEnable )
{
	if( uIndex < 0 || uIndex >= MAX_SOUND_NUM ) return gxFalse;

	m_Info[ uIndex ].bReq    = gxTrue;
	m_Info[ uIndex ].bReverb = bEnable;
	m_Info[uIndex].uStatus |= enSoundReqSetReverb;

	return gxTrue;
}


void gxSoundManager::ResumeAllSounds()
{
/*
 	for(Sint32 ii=0; ii<MAX_SOUND_NUM; ii++)
	{
		if( m_Info[ ii ].fileName[0] )
		{
			LoadAudioFile( ii , &m_Info[ ii ].fileName[0] );
		}
	}
*/
}

