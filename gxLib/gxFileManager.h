#ifndef _gxFileManager_H_
#define _gxFileManager_H_

/*
■ Android
　LoadFile
　ROM

　SaveFile
　EXTERNAL
　sd:files/Save.txt

　SaveStorage / LoadStorage
　INTERNAL
　data/data/03_disk/SaveStorage


■ Win
　LoadFile
　ROM

　SaveFile
　EXTERNAL
　./Save.txt
*/


#include <iostream>
#include <map>
#include <mutex>

class gxFileManager {

	typedef struct StFileInfo {
		Sint32 id = 0;
		gxChar  m_pNameBuf[FILENAMEBUF_LENGTH] = {0};
        gxBool  m_bLoadDone = gxTrue;
		Sint32  m_SaveSuccess = 0;
		size_t  m_uSizeBuf   = 0;
		Uint8  *m_pDataBuf   = nullptr;
		ESTORAGE_LOCATION  m_Location   = ESTORAGE_LOCATION::AUTO;
		Sint32  m_AccessMode = 0;
		std::function<void(Sint32 id)> m_CallBack = nullptr;
	}StFileInfo;

public:
	//enum {
	//	enRequestMax = 1024,
	//};

	enum {
		ACCESSMODE_READ,
		ACCESSMODE_WRITE,
	};

	gxFileManager();
	~gxFileManager();

	Sint32 SaveReq(const gxChar *pFileName, void* pData, size_t uSize, ESTORAGE_LOCATION _Location);
	Sint32 LoadReq(const gxChar *pFileName, ESTORAGE_LOCATION _Location , std::function<void(Sint32 id)>func=nullptr );
	gxBool IsLoadEnd(Sint32 id);
	gxBool IsSaveEnd( Sint32 id );

	Uint8* GetFileAddr(Sint32 id );
	size_t GetFileSize(Sint32 id);
	void Clear( Sint32 id );

	void Action();	//必ずメインスレッドから呼び出されること！！！

	gxBool IsLoadTaskExist()
	{
		//まだファイルアクセス中か？
		//if( m_FileInfo.size() == 0 ) return gxFalse;
        
        std::lock_guard<std::mutex> lock(m_BarrierFileAccess);  //←ファイルアクセス中にファイル管理構造体メンバにアクセスすると競合してしまう
        
		for (auto itr = m_FileInfo.begin(); itr != m_FileInfo.end(); itr++)
		{
			if (itr->second.m_pNameBuf[0])
            //if (itr->second.m_bLoadDone)
			{
				return gxTrue;
			}
		}

		return gxFalse;

		//for (Sint32 ii = 0; ii <enRequestMax; ii++)
		//{
		//	if (m_FileInfo[ii].m_pNameBuf[0]) return gxTrue;
		//}
		//return gxFalse;
	}

	gxBool IsSaveSuccess(Sint32 id)
	{
		if (m_FileInfo[id].m_SaveSuccess == 1) return gxTrue;

		return gxFalse;
	}

	void  SetDropFileNum( Sint32 num  );
	void  AddDropFile( gxChar* pFileName );
	Sint32  GetDropFileNum();
	gxChar* GetDropFileName( Sint32 num );
	void ClearDropFiles();

	gxBool IsDragEnable()
	{
		return m_bDragEnable;
	}

	void SetDragEnable( gxBool bEnable )
	{
		m_bDragEnable = bEnable;
	}

	SINGLETON_DECLARE( gxFileManager );

private:

	Uint8* loadFile( const gxChar* pFileName , size_t* pLength , ESTORAGE_LOCATION _location );
	gxBool saveFile( const gxChar* pFileName  , Uint8 *pData , size_t uSize , ESTORAGE_LOCATION _location );

	std::map<int , StFileInfo> m_FileInfo;
	Sint32 m_sReqCnt;

	gxChar **m_pDropFiles;
	Sint32 m_DropFilesNum;
	Sint32 m_DropFilesCnt;

	gxBool m_bDragEnable;

    std::mutex m_BarrierFileAccess;

};


#endif

