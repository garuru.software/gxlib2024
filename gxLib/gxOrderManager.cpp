//--------------------------------------------------
//
// gxOrderTable.h
// リクエストされた描画をオーダリングテーブルに格納します
//
//--------------------------------------------------

#include <gxLib.h>
#include "gx.h"
#include "gxOrderManager.h"
#include <mutex>
#include "util/CSpinLock.h"

SINGLETON_DECLARE_INSTANCE( gxOrderManager )

gxOrderManager::gxOrderManager()
{
	s_pInstance = this;

	RequestInit();
}


gxOrderManager::~gxOrderManager()
{
	s_pInstance = NULL;
}











gxOrderTable::gxOrderTable()
{
	m_pOrder = new StOrder[ enOrderMax ];
	m_sCount = 0;

	m_uSubOrderCnt = 0;

	m_sStringCnt = 0;

	m_pSubOrder = new StOrder[ 300000 ];//enOrderMax ];
	m_pLastOrder = NULL;

	m_bRequestEnable = gxTrue;
}


gxOrderTable::~gxOrderTable()
{
	SAFE_DELETES(m_pOrder);
	SAFE_DELETES(m_pSubOrder);
}


Sint32 gxOrderTable::set(StOrder* pOrder , gxBool bSubOrderEnable )
{
	if( !m_bRequestEnable ) return 0;

	if( m_sCount >= MAX_ORDER_NUM )
	{
		return -1;
	}

	pOrder->m_pNextOrder = NULL;

/*
    if( pOrder->sType == enOrderTypeFont && m_sStringCnt < MAX_TEXT_NUM )
	{
		gxChar *p = (gxChar*)pOrder->pString;
		Uint32 len;

		len = strlen(p)+1;

		m_pStringBuffer[ m_sStringCnt ] = new gxChar[len];
		gxUtil::MemCpy( m_pStringBuffer[ m_sStringCnt ], pOrder->pString , len-1 );
		m_pStringBuffer[ m_sStringCnt ][len-1] = 0x00; 

		pOrder->pString = (void*)m_pStringBuffer[ m_sStringCnt ];
		m_sStringCnt ++;
	}
	else
*/

    if( m_pLastOrder != NULL  && bSubOrderEnable )
	{
		//

		if( m_pLastOrder->sType == pOrder->sType && m_pLastOrder->shader == pOrder->shader )
		{
			if( m_pLastOrder->prio == pOrder->prio)
			{
				if( m_pLastOrder->sAtribute == pOrder->sAtribute )
				{
					if( m_pLastOrder->pg[0] == pOrder->pg[0] )
					{
						//前と同じタイプのオーダーだった
						//反転を意識させるか？

						m_pSubOrder[ m_uSubOrderCnt ] = *pOrder;
						m_pLastOrder->m_pNextOrder = &m_pSubOrder[ m_uSubOrderCnt ];

						m_pLastOrder = &m_pSubOrder[ m_uSubOrderCnt ];

						m_uSubOrderCnt ++;
						return m_sCount;
					}
				}
			}
		}

	}

	//------------------------

	m_pOrder[m_sCount] = *pOrder;

	m_pLastOrder = &m_pOrder[m_sCount];

	m_sCount ++;

	return m_sCount-1;
}


void gxOrderTable::Reset()
{
	//オーダリングテーブルの中身をリセットする

	m_sStringCnt = 0;

	m_sCount = 0;

	m_uSubOrderCnt = 0;

	m_pLastOrder = NULL;

}


void gxOrderTable::ZSort()
{
	//Zソートを行う
#if 1
	if (m_sCount == 0)
		return;
	for(Uint32 ii=0; ii<(Uint32)m_sCount; ii++)
	{
		m_zSort[ii] = (m_pOrder[ii].prio <<16) + ii;
	}
	std::sort(&m_zSort[0], &m_zSort[m_sCount]);
	for(Uint32 ii=0; ii<(Uint32)m_sCount; ii++)
	{
		m_zIndex[ii] = m_zSort[ii] & 0xffff;
	}
#else
	for(Uint32 ii=0; ii<(Uint32)m_sCount; ii++)
	{
		m_zSort[ii] = (m_pOrder[ii].prio <<16) + ii;
		m_zIndex[ii] = ii;
	}
	if(m_sCount>0)QSort( m_zSort , m_zIndex , 0 , m_sCount -1 );
#endif
}


void gxOrderTable::QSort(Sint32* zsort,Uint32* zIndex ,int left, int right)
{
	//クイックソート

	int i, j;
	Sint32 pivot;

	i = left;								/* ソートする配列の一番小さい要素の添字 */
	j = right;								/* ソートする配列の一番大きい要素の添字 */

	pivot = zsort[(left + right) / 2];		/* 基準値を配列の中央付近にとる */

	while (1) {								/* 無限ループ */

		while (zsort[i] < pivot)			/* pivot より大きい値が */
			i++;							/* 出るまで i を増加させる */

		while (pivot < zsort[j])			/* pivot より小さい値が */
			j--;							/*  出るまで j を減少させる */

		if (i >= j)							/* i >= j なら */
			break;							/* 無限ループから抜ける */
		SWAP(zsort[i], zsort[j]);			/* x[i] と x[j]を交換 */
		SWAP(zIndex[i], zIndex[j]);			/* x[i] と x[j]を交換 */
		i++;								/* 次のデータ */
		j--;
	}

	if (left < i - 1)						/* 基準値の左に 2 以上要素があれば */
		QSort(zsort, zIndex,left, i - 1);	/* 左の配列を Q ソートする */

	if (j + 1 <  right)						/* 基準値の右に 2 以上要素があれば */
		QSort(zsort, zIndex,j + 1, right);	/* 右の配列を Q ソートする */

}



//---------------------

namespace
{
	CSpinLock	 s_gxOrderManager_DrawLock;
}

void gxOrderManager::RequestInit()
{
	std::lock_guard<CSpinLock>	lock(s_gxOrderManager_DrawLock);
	m_uCurrent    = 0; //-1;
	m_uDrawLayer  = 2; //-2;
	m_uNewerLayer = 2; //-3;
}


/// リクエストバッファーを変更する
///
void gxOrderManager::ChangeRequestBuffer()
{
	//リクエストバッファーを変更する
	{
		std::lock_guard<CSpinLock>	lock(s_gxOrderManager_DrawLock);
		m_uNewerLayer = m_uCurrent;

		m_uCurrent ++;
		m_uCurrent = m_uCurrent%3;

		if( m_uCurrent == m_uDrawLayer )
		{
			//新しいバッファが現在使用中ならその次のバッファにする
			m_uCurrent ++;
			m_uCurrent = m_uCurrent%3;
		}
	}
	m_pOrderTable[ m_uCurrent ].Reset();
}

gxBool gxOrderManager::IsDrawable()
{
	std::lock_guard<CSpinLock>	lock(s_gxOrderManager_DrawLock);
 #if 0
	if( m_uNewerLayer < 0 )
	{
		return gxFalse;
	}
 #endif

	if( m_uDrawLayer == m_uNewerLayer )
	{
		return gxFalse;
	}

	return gxTrue;
}

/// 描画開始の初期化.
///  ※ DlawLayer と NewerLayer の比較と設定は同時に行ったほうが矛盾しないので、
///     IsDrawable() をやめて DrawInit の返り値で結果を返す.
///
gxBool gxOrderManager::DrawInit()
{
	std::lock_guard<CSpinLock>	lock(s_gxOrderManager_DrawLock);
 #if 0
	if( m_uNewerLayer < 0 )
	{
		return gxFalse;
	}
 #endif

	if( m_uDrawLayer == m_uNewerLayer )
	{
		return gxFalse;
	}

	m_uDrawLayer = m_uNewerLayer;
	return gxTrue;
}
