#ifndef _GXNETORKMANAGER_H_
#define _GXNETORKMANAGER_H_

//ネットワーク管理
#include <iostream>
#include <map>
#include <mutex>

class gxNetworkManager
{
public:
	enum class ESeq {
		eHTTPSeqNone,		//0
		eHTTPSeqInit,		//1
		eHTTPSeqStarted,	//1
		eHTTPSeqLoadEnd,	//100
		eHTTPSeqClose,		//999
	};

	enum class EError{
		eNone,
		Success,
		Failed,
	};

	struct StHTTP
	{
		StHTTP()
		{
			m_URL[0] = 0x00;
			PostData = nullptr;
			m_PostSize = 0;
			pData = NULL;
			bRequest = gxFalse;
			m_Seq = ESeq::eHTTPSeqNone;
			uFileSize = 0;
			uReadFileSize = 0;
			index = 0;
			bDone  = gxFalse;
			bClose = gxTrue;
		}

		~StHTTP()
		{
			if( PostData )
			{
				SAFE_DELETES(PostData);
			}
		}

		void SetError( EError err )
		{
			Error = err;
		}

		void SetSeq( ESeq seq )
		{
			m_Seq = seq;
		}
		
		ESeq GetSeq()
		{
			return m_Seq;
		}

		gxBool IsNowLoading()
		{
			switch (m_Seq) {
			case ESeq::eHTTPSeqNone:
			case ESeq::eHTTPSeqLoadEnd:
			case ESeq::eHTTPSeqClose:
				return gxFalse;
			default:
				break;
			}

			return gxTrue;
		}

		void MoveData( Uint8 *p , size_t size )
		{
            SAFE_DELETES(pData);
			pData = new Uint8[size];
            gxUtil::MemCpy( pData , p , size );
			uFileSize = size;
		}

		void ClearData()
		{
			SAFE_DELETES(pData);
			uFileSize = 0;
		}

		gxChar m_URL[1024];
		gxChar* PostData = nullptr;
		size_t  m_PostSize = 0;
		Uint8 *pData;
		gxBool bRequest;
		size_t uFileSize;
		size_t uReadFileSize;
		Sint32 index;
		gxBool bClose = gxFalse;
		std::function<gxBool(gxBool bSuccess, uint8_t* p, size_t size)> func = nullptr;
		EError Error = EError::eNone;

	private:

		ESeq m_Seq;
		gxBool bDone = gxFalse;


	};

	gxNetworkManager()
	{
		m_sHttpRequestMax = 0;
	}

	~gxNetworkManager()
	{

	}

	void Action();

	Sint32 OpenURL(
		gxChar const *pOpenURL ,
		gxChar const *pPostData = nullptr ,
		size_t PostSize = 0 ,
		std::function<gxBool(gxBool bSuccess , uint8_t *p , size_t size)> func=nullptr );

	gxBool CloseURL(Uint32 uIndex);

	StHTTP* GetHTTPInfo( Uint32 uIndex )
	{
		if (m_pHttp.find(uIndex) == m_pHttp.end())
		{
			return nullptr;
		}

		return &m_pHttp[ uIndex ];
	}

	std::vector<Sint32> GetHTTPInfoIDList()
	{
		std::vector<Sint32> list;
		for (auto itr = m_pHttp.begin(); itr != m_pHttp.end(); )
		{
			if (itr == m_pHttp.end()) break;
			list.push_back(itr->second.index);

			itr++;
		}

		return list;
	}

	StHTTP* GetNextReq();

	SINGLETON_DECLARE( gxNetworkManager );

private:

	std::map<int , StHTTP> m_pHttp;
	Sint32 m_sHttpRequestMax;
	std::mutex m_UpdateLock; 
};

#endif
