set iconFG=icon.png
set iconBG=iconBG.png
set banner1=bannerH.png
set banner2=bannerV.png

::Android
::magick %iconFG% -resize 512x512! resource\Android\market\storeIcon512x512.png
magick %banner2% -resize 720x1280! market\store_Shot720x1280.png
magick %banner1% -resize 1024x500! market\store_Feat1024x500.png

::magick %iconFG% -resize 72x72!   resource\Android\mipmap-hdpi\ic_launcher.png
::magick %iconFG% -resize 48x48!   resource\Android\mipmap-mdpi\ic_launcher.png
::magick %iconFG% -resize 96x96!   resource\Android\mipmap-xhdpi\ic_launcher.png
::magick %iconFG% -resize 144x144! resource\Android\mipmap-xxhdpi\ic_launcher.png

magick %iconFG% -resize 72x72!   resource\Android\mipmap-hdpi\ic_launcher.png
magick %iconBG% -resize 162x162! resource\Android\mipmap-hdpi\ic_launcher_background.png
magick %iconFG% -resize 162x162! resource\Android\mipmap-hdpi\ic_launcher_foreground.png
magick %iconFG% -resize 72x72!   resource\Android\mipmap-hdpi\ic_launcher_round.png


magick %iconFG% -resize 48x48!   resource\Android\mipmap-mdpi\ic_launcher.png
magick %iconBG% -resize 108x108! resource\Android\mipmap-mdpi\ic_launcher_background.png
magick %iconFG% -resize 108x108! resource\Android\mipmap-mdpi\ic_launcher_foreground.png
magick %iconFG% -resize 48x48!   resource\Android\mipmap-mdpi\ic_launcher_round.png


magick %iconFG% -resize 96x96!   resource\Android\mipmap-xhdpi\ic_launcher.png
magick %iconBG% -resize 216x216! resource\Android\mipmap-xhdpi\ic_launcher_background.png
magick %iconFG% -resize 216x216! resource\Android\mipmap-xhdpi\ic_launcher_foreground.png
magick %iconFG% -resize 96x96!   resource\Android\mipmap-xhdpi\ic_launcher_round.png


magick %iconFG% -resize 144x144!   resource\Android\mipmap-xxhdpi\ic_launcher.png
magick %iconbG% -resize 324x324!   resource\Android\mipmap-xxhdpi\ic_launcher_background.png
magick %iconFG% -resize 324x324!   resource\Android\mipmap-xxhdpi\ic_launcher_foreground.png
magick %iconFG% -resize 144x144!   resource\Android\mipmap-xxhdpi\ic_launcher_round.png


magick %iconFG% -resize 192x192!   resource\Android\mipmap-xxxhdpi\ic_launcher.png
magick %iconBG% -resize 432x432!   resource\Android\mipmap-xxxhdpi\ic_launcher_background.png
magick %iconFG% -resize 432x432!   resource\Android\mipmap-xxxhdpi\ic_launcher_foreground.png
magick %iconFG% -resize 192x192!   resource\Android\mipmap-xxxhdpi\ic_launcher_round.png


magick %iconFG% -resize 512x512!   resource\googleMarket\storeIcon512x512.png

::Windows desktop
magick %iconFG% -resize 32x32!   resource\win\gxLib.ico
magick %iconFG% -resize 128x128! resource\win\gxLib256.ico
magick %iconFG% -resize 256x256! resource\win\gxLib256.ico

::winStore
magick %iconFG% -resize 48x48!    resource\uwp\LockScreenLogo.scale-200.png
magick %banner1% -resize 1240x600! resource\uwp\SplashScreen.scale-200.png
magick %iconFG% -resize 88x88!    resource\uwp\Square44x44Logo.scale-200.png
magick %iconFG% -resize 24x24!    resource\uwp\Square44x44Logo.targetsize-24_altform-unplated.png
magick %iconFG% -resize 300x300!  resource\uwp\Square150x150Logo.scale-200.png
magick %iconFG% -resize 50x50!    resource\uwp\StoreLogo.png
magick %banner1% -resize 620x300!  resource\uwp\Wide310x150Logo.scale-200.png

magick %banner1% -resize 1024x500!    resource\uwp\promo\banner1024x500.png

::iOS
magick %iconFG% -resize 40x40!  resource\iOS\Icons\icon-40x40.png
magick %iconFG% -resize 60x60!  resource\iOS\Icons\icon-60x60.png
magick %iconFG% -resize 58x58!  resource\iOS\Icons\icon-58x58.png
magick %iconFG% -resize 87x87!  resource\iOS\Icons\icon-87x87.png
magick %iconFG% -resize 80x80!  resource\iOS\Icons\icon-80x80.png
magick %iconFG% -resize 120x120!  resource\iOS\Icons\icon-120x120.png
magick %iconFG% -resize 180x180!  resource\iOS\Icons\icon-180x180.png
magick %iconFG% -resize 20x20!  resource\iOS\Icons\icon-20x20.png
	::40x40
magick %iconFG% -resize 29x29!  resource\iOS\Icons\icon-29x29.png
	::58x58
	::40x40
	::80x80
magick %iconFG% -resize 76x76!  resource\iOS\Icons\icon-76x76.png
magick %iconFG% -resize 152x152!  resource\iOS\Icons\icon-152x152.png
magick %iconFG% -resize 167x167!  resource\iOS\Icons\icon-167x167.png
magick %iconFG% -resize 1024x1024!  resource\iOS\Icons\icon-1024x1024.png



::magick %iconFG% -resize 29x29!   resource\iOS\Icons\icon-small.png
::magick %iconFG% -resize 58x58!   resource\iOS\Icons\icon-small@2x.png
::magick %iconFG% -resize 87x87!   resource\iOS\Icons\icon-small@3x.png
::magick %iconFG% -resize 80x80!   resource\iOS\Icons\icon-40@2x.png
::magick %iconFG% -resize 120x120! resource\iOS\Icons\icon-40@3x.png
::magick %iconFG% -resize 57x57!	 resource\iOS\Icons\icon.png
::magick %iconFG% -resize 114x114! resource\iOS\Icons\icon@2x.png
::magick %iconFG% -resize 120x120! resource\iOS\Icons\icon-60@2x.png
::magick %iconFG% -resize 180x180! resource\iOS\Icons\icon-60@3x.png
::magick %iconFG% -resize 29x29!	 resource\iOS\Icons\icon-small.png
::magick %iconFG% -resize 58x58!	 resource\iOS\Icons\icon-small@2x.png
::magick %iconFG% -resize 40x40!	 resource\iOS\Icons\icon-40.png
::magick %iconFG% -resize 80x80!	 resource\iOS\Icons\icon-40@2x.png
::magick %iconFG% -resize 50x50!	 resource\iOS\Icons\icon-50.png
::magick %iconFG% -resize 100x100! resource\iOS\Icons\icon-50@2x.png
::magick %iconFG% -resize 72x72!	 resource\iOS\Icons\icon-72.png
::magick %iconFG% -resize 144x144! resource\iOS\Icons\icon-72@2x.png
::magick %iconFG% -resize 76x76!	 resource\iOS\Icons\icon-76.png
::magick %iconFG% -resize 152x152! resource\iOS\Icons\icon-76@2x.png
::magick %iconFG% -resize 167x167! resource\iOS\Icons\icon-83.5@2x.png
::magick %iconFG% -resize 60x60!	 resource\iOS\Icons\icon_60x60.png
::magick %iconFG% -resize 1024x1024! resource\iOS\Icons\icon_1024x1024.png

