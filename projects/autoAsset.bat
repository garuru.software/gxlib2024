::--------------------------------------------------
::Android
::--------------------------------------------------
::mkdir -p ./Android/Pack01_Install/src/main/assets
::ln -s ../Storage/01_rom ./Android/Pack01_Install/src/main/assets/01_rom



rem ZIPアーカイブシステムを使うとき
rem xcopy /Y Storage\assets.zip projects\Android\app\src\main\assets\
rem xcopy /Y Storage\disc.zip   projects\Android\app\src\main\assets\

::ROMをassetsに
rd /S /q .\projects\Android\Pack01_Install\src\main\assets\01_rom\
xcopy /E /Y ..\Storage\01_rom\*.* ..\projects\Android\Pack01_Install\src\main\assets\01_rom\

::iconをコピーする
::xcopy /Y/S/E ..\Storage\09_etc\resource\Android projects\Android\app\src\main\res



::--------------------------------------------------
::Windows / Desktop
::--------------------------------------------------
xcopy /Y Storage\09_etc\resource\win\gxLib.ico gxLib\hard\Windows\DirectX11


::--------------------------------------------------
::iOS
::--------------------------------------------------

::iconをコピーする
xcopy /Y Storage\09_etc\resource\iOS\Icons projects\iOS\gxLib2020\Assets.xcassets\AppIcon.appiconset

::プロジェクト設定をコピーする
::xcopy /Y Storage\09_etc1\resource\iOS\Info.plist projects\iOS\gxLib2020
::xcopy /Y Storage\09_etc1\resource\iOS\project.pbxproj projects\iOS\gxLib2020.xcodeproj

::--------------------------------------------------
::Windows10 / UWP
::--------------------------------------------------
::Icon類をAssetsフォルダにコピーする
rem xcopy /Y Storage\09_etc\resource\uwp\*.png projects\Windows10UWP\Assets\
rem asset リソースをZipアセットとしてコピーする
rem xcopy /Y Storage\assets.zip projects\Windows10UWP\Assets\
rem xcopy /Y Storage\disc.zip   projects\Windows10UWP\Assets\


::--------------------------------------------------
::AndroidのCMakeFileに追加するcppリストを作成する
::--------------------------------------------------

set INPUT_FILE=temp.txt
set OUTPUT_FILE=projects\Android\app\src\main\cpp\temp.txt

del %INPUT_FILE%
del %OUTPUT_FILE%

dir game\*.cpp /s /b >%INPUT_FILE%

set BEFORE_STRING=%CD%
set AFTER_STRING=		../../../../../..

setlocal enabledelayedexpansion
for /f "delims=" %%a in (%INPUT_FILE%) do (
set line=%%a
echo !line:%BEFORE_STRING%=%AFTER_STRING%!>>%OUTPUT_FILE%
)

del %INPUT_FILE%

