package jp.co.garuru;
import android.app.Activity;
import android.view.Gravity;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import jp.co.garuru.GameGirlActivity;

public class httpTest {

    Activity activity;

    public httpTest(Activity activity){
        this.activity = activity;
    }

    //サスペンド等の終了時の制御
    public void close(){

    }

    private WebView webView = null;
    FrameLayout webViewLayout = null;

    public void start(){
		String url = "https://www.fine-net.com/";//https://seven01.fine-net.com/20200728/wv/info/";

        //Log.e("url ============== "+url);
        final String tmpUrl = url;
        activity.runOnUiThread(
			new Runnable()
			{
                @Override
                public void run() {
                    if(webView == null) {
                        webView = new WebView(activity);
                        //webviewJsInterface = new JavaScriptInterface(nativeEventManager);

                        WebSettings webSettings = webView.getSettings();
                        webSettings.setJavaScriptEnabled(true);
                        //webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);

                        //webView.addJavascriptInterface(webviewJsInterface, JavaScriptInterface.NAME);
                        webView.loadUrl(tmpUrl);
                        FrameLayout.LayoutParams webViewLayoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
                        webViewLayoutParams.width = 1070;//＊デフォルト値
                        webViewLayoutParams.height = 660;//＊デフォルト値
                        webView.setLayoutParams(webViewLayoutParams);

                        webViewLayout = new FrameLayout(activity);
                        webViewLayout.addView(webView);
                        FrameLayout.LayoutParams flp = new FrameLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                        flp.gravity = Gravity.CENTER;
                        activity.addContentView(webViewLayout, flp);
                    }else{
                        webView.loadUrl(tmpUrl);
                    }
                }
            }
		);
    }

}
