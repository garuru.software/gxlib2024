package jp.co.garuru;

import android.content.Context;
import android.graphics.PixelFormat;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;

import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.opengles.GL10;


class gxView extends GLSurfaceView {
	private static final String TAG = "GLES3JNI";
	private static final boolean DEBUG = true;

	public gxView(Context context) {
		super(context);
		// Pick an EGLConfig with RGB8 color, 16-bit depth, no stencil,
		// supporting OpenGL ES 2.0 or later backwards-compatible versions.
		//setEGLConfigChooser(8, 8, 8, 8, 16, 0);
		setEGLConfigChooser(5, 6, 5, 0, 0, 0);
		setEGLContextClientVersion(3);
		setRenderer(new Renderer());
	}

	private static class Renderer implements GLSurfaceView.Renderer {
		public void onDrawFrame(GL10 gl) {
			//javaの処理を更新
			GameGirlActivity.GetInstance().update();

			//Java側のrequestをまとめてC++へ送る
			//byte[] data = GameGirlActivity.GetInstance().GetJavaState();
			//GameGirlActivity.SetJava2Cpp(data,data.length);

			GameGirlJNI.appUpdate();
		}

		public void onSurfaceChanged(GL10 gl, int width, int height) {
			GameGirlJNI.appResume(width, height);
			GameGirlActivity.GetInstance().SetScreenSize(width, height);
		}

		public void onSurfaceCreated(GL10 gl, EGLConfig config) {
			GameGirlJNI.appInit();
		}
	}
}

