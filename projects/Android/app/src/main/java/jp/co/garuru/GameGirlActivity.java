//-------------------------------------------------------------------------------
//
// メインスレッドをつかさどるJavaメイン
//
//-------------------------------------------------------------------------------

package jp.co.garuru;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.ClipboardManager;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.WindowManager;

import java.io.File;
//
import android.os.AsyncTask;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.net.MalformedURLException;
import java.io.IOException;
import java.io.InputStream;

//cursor
import android.view.MotionEvent;
import android.widget.LinearLayout;
import android.widget.Toast;
import android.view.KeyEvent;

//config
import android.content.res.Configuration;
import	android.media.AudioManager;

//seia
import java.io.Serializable;

//fullscreen
import android.view.View;

//sd path
import java.util.ArrayList;
import java.util.List;
//import java.util.Iterator;

//version.txt
import java.io.File;
import java.io.OutputStream;

//画面の回転を抑制する
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;

//ビルドバージョン
import android.os.Build;
import android.util.Log;

//BT
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.BroadcastReceiver;

//timerTask
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import android.os.Handler;

//wifi
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

import static android.content.ContentValues.TAG;
import android.os.StatFs;

//webView
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.view.Gravity;
import android.util.DisplayMetrics;

public class GameGirlActivity extends Activity {

	static public final boolean GGA_FULLSCREEN = true;
	static public final boolean GGA_NO_SLEEP = true;
	static public final boolean GGA_FIX_LANDSCAPE = false;
	static public final boolean GGA_FIX_PORTRAIT = false;
	//	static final boolean GGA_BLUETOOTH_ENABLE = false;
	static public boolean GGA_ENABLE_BLUETOOTH = false;
	static public boolean GGA_ENABLE_CENSOR = true;

	gxView mView;
	gxHttpClient  pHttpClinent;
	gxInput       pMouse;
	gxCensor	  pCensor;

	AudioManager m_AudioManager;

	static GameGirlActivity s_pInstance;


	Timer m_Timer ;
	public String m_ToastString = "";
	public String m_WebViewURLString = "";
	public boolean m_bOpenWebBrowser = false;

	static public byte[] m_JavaState = new byte[32];

	Context m_Context;

	boolean m_bOpenWebView = false;
	View m_ViewWebView = null;
	WebView m_WebViewClient = null;
	int m_Width = 320;
	int m_Height = 240;

	@Override protected void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		mView = new gxView(getApplication());
		setContentView(mView);

		s_pInstance = this;

		//m_Context = getApplication();
		m_Context = getApplicationContext();

		GameGirlJNI.SetAndroidContext(m_Context);

		// スリープさせないようにする
		if( GGA_NO_SLEEP == true )
		{
			getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		}

		//フルスクリーン表示にする
		if( GGA_FULLSCREEN == true )
		{
			if (Build.VERSION.SDK_INT >= 19)
			{
				View decor = this.getWindow().getDecorView();
				decor.setSystemUiVisibility(
					View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
					| View.SYSTEM_UI_FLAG_LAYOUT_STABLE
					| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
					| View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
					| View.SYSTEM_UI_FLAG_FULLSCREEN
					| View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
					);
			}
			else
			{
				getWindow().getDecorView().setSystemUiVisibility( View.SYSTEM_UI_FLAG_LOW_PROFILE );
			}
		}

		if( GGA_FIX_LANDSCAPE == true )
		{
			setRequestedOrientation( ActivityInfo.SCREEN_ORIENTATION_USER_LANDSCAPE);
		}
		if( GGA_FIX_PORTRAIT == true )
		{
			setRequestedOrientation( ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);	//portrait固定
		}

		m_AudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

		//マルチタッチ

		pMouse = new gxInput();
		pMouse.Init(this);

		//ブルートゥース管理

		//if ( GGA_BLUETOOTH_ENABLE )
		//{
		//	BlueToothInit();
		//}

		//各種センサー類
		if( GGA_ENABLE_CENSOR == true )
		{
			pCensor = new gxCensor();
			pCensor.Init(this);
		}

		mView.setOnTouchListener( pMouse );

		//gxOBBFiles gxObb = new gxOBBFiles(m_Context);
		//gxOBBFiles.setObbMount( 1 );

		//timer

		m_Timer = new Timer();
		TimerTask m_TimerTask = new gxTaskManager(this);
		m_Timer.scheduleAtFixedRate(m_TimerTask, 0, 32);

		GameGirlJNI.SetSDPath( getSDCardPath() );

        //InputVoice();

		//WebView
		if( m_ViewWebView == null )
		{
/*
			LayoutInflater a = this.getLayoutInflater();
			View v = a.inflate(R.layout.activity_main, null);
			m_ViewWebView = v;
			m_ViewWebView.setVisibility(View.GONE);
			addContentView(m_ViewWebView, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,ViewGroup.LayoutParams.FILL_PARENT));
			//m_WebViewClient = findViewById(R.id.mainView);
*/
			m_bOpenWebView = false;
		}

		String str = getWifiIPAddress(this);
	}


	private String getSDCardPath()
	{
		String SDString="";

		File exdir = Environment.getExternalStorageDirectory();
		SDString = exdir.getAbsolutePath();

		List<String> sdCardFilesDirPathList = new ArrayList<>();

		File[] dirArr = m_Context.getExternalFilesDirs(null);

		for (File dir : dirArr) {
			if (dir != null) {
				String path = dir.getAbsolutePath();

				SDString = path;
				// isExternalStorageRemovableはAndroid5.0から利用できるAPI。
				// 取り外し可能かどうか（SDカードかどうか）を判定している。
				if (Environment.isExternalStorageRemovable(dir)) {

					// 取り外し可能であればSDカード。
					if (!sdCardFilesDirPathList.contains(path)) {
						sdCardFilesDirPathList.add(path);
						SDString = path;
					}

				} else {
					// 取り外し不可能であれば内部ストレージ。
				}
			}
		}
		return SDString;
	}

	public static GameGirlActivity GetInstance()
	{
		return s_pInstance;
	}

	@Override protected void onPause() {
		super.onPause();
		mView.onPause();
		GameGirlJNI.appPause();
		Log.d("gxJava","-------------------onPause-------------------");
	}

	@Override protected void onResume() {
		super.onResume();
		mView.onResume();

		View decor = this.getWindow().getDecorView();
		decor.setSystemUiVisibility(
				View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
						| View.SYSTEM_UI_FLAG_LAYOUT_STABLE
						| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
						| View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
						| View.SYSTEM_UI_FLAG_FULLSCREEN
						| View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
		);

		Log.d("gxJava","-------------------onResume-------------------");
		GameGirlJNI.appResume(m_Width,m_Height);
	}

	@Override
	protected void onDestroy()
	{
		//落ちたときに来た
		super.onDestroy();
		GameGirlJNI.appEnd();
		Log.d("gxJava","-------------------onDestroy-------------------");
	}

	@Override
	protected void onStop()
	{
		super.onStop();
		Log.d("gxJava","-------------------onStop-------------------");
		GameGirlJNI.appStop();
	}

	@Override
	protected void onRestart()
	{
		super.onRestart();
		Log.d("gxJava","-------------------onRestart-------------------");
		GameGirlJNI.appRestart();
	}

	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		//int id = event.getDeviceId();
		//https://wlog.flatlib.jp/archive/1/2011-08/category/20

		if( keyCode == KeyEvent.KEYCODE_VOLUME_DOWN ) {
			AudioManager am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

			int ringMaxVolume = am.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
			int ringVolume = am.getStreamVolume(AudioManager.STREAM_MUSIC);
			int flags = AudioManager.FLAG_SHOW_UI;// | AudioManager.FLAG_PLAY_SOUND;

			ringVolume -= ringMaxVolume/10;
			if( ringVolume <= 0 ) ringVolume = 0;
			am.setStreamVolume(AudioManager.STREAM_MUSIC, ringVolume, flags);
			byte vol = (byte)(1+(100.0f*ringVolume/ringMaxVolume));
			GameGirlActivity.GetInstance().m_JavaState[17] = vol;
		}
		else if( keyCode == KeyEvent.KEYCODE_VOLUME_UP )
		{
			AudioManager am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
			// 現在の音量を取得する
			int ringMaxVolume = am.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
			int ringVolume = am.getStreamVolume(AudioManager.STREAM_MUSIC);
			int flags = AudioManager.FLAG_SHOW_UI;// | AudioManager.FLAG_PLAY_SOUND;

			ringVolume += ringMaxVolume/10;
			if( ringVolume >= ringMaxVolume ) ringVolume = ringMaxVolume;
			am.setStreamVolume(AudioManager.STREAM_MUSIC, ringVolume, flags);
			byte vol = (byte)(1+(100.0f*ringVolume/ringMaxVolume));
			GameGirlActivity.GetInstance().m_JavaState[17] = vol;
		}
		else if( keyCode == KeyEvent.KEYCODE_BACK )
		{
			//Toast.makeText( s_pInstance, "zipパス「garurulabs.」", Toast.LENGTH_LONG).show();
			//GameGirlActivity.GetInstance().InputVoice();
			//m_WebViewURLString = "http://www.yahoo.co.jp";
			GameGirlActivity.GetInstance().m_JavaState[16] = 0x01;
		}

		pMouse.onKeyDown( keyCode , event );

		return false;
	}

	private boolean isBButtonPressed = false;
	private boolean isXButtonPressed = false;
	private boolean isAButtonPressed = false;
	private boolean isYButtonPressed = false;
	private boolean isSelButtonPressed = false;
	private boolean isSTARTButtonPressed = false;

	@Override
	public boolean dispatchKeyEvent(KeyEvent event) {

		if (event.getKeyCode() == KeyEvent.KEYCODE_BUTTON_SELECT) {
			if (event.getAction() == KeyEvent.ACTION_DOWN) {
				// Bボタンが押されたときの処理を書く
				isSelButtonPressed = true;
			} else if (event.getAction() == KeyEvent.ACTION_UP) {
				isSelButtonPressed = false;
			}
		}
		else if (event.getKeyCode() == KeyEvent.KEYCODE_MENU && isSelButtonPressed) {
			// Bボタンが押されている間はBACKボタンのイベントを無視する
			return true;  // イベントを消費したことを示す
		}

		//B&BACK
		if (event.getKeyCode() == KeyEvent.KEYCODE_BUTTON_B) {
			if (event.getAction() == KeyEvent.ACTION_DOWN) {
				// Bボタンが押されたときの処理を書く
				isBButtonPressed = true;
			} else if (event.getAction() == KeyEvent.ACTION_UP) {
				isBButtonPressed = false;
			}
		}
		else if (event.getKeyCode() == KeyEvent.KEYCODE_BACK && isBButtonPressed) {
			// Bボタンが押されている間はBACKボタンのイベントを無視する
			return true;  // イベントを消費したことを示す
		}

		if (event.getKeyCode() == KeyEvent.KEYCODE_BUTTON_Y) {
			if (event.getAction() == KeyEvent.ACTION_DOWN) {
				// Bボタンが押されたときの処理を書く
				isYButtonPressed = true;
			} else if (event.getAction() == KeyEvent.ACTION_UP) {
				isYButtonPressed = false;
			}
		}
		else if (event.getKeyCode() == KeyEvent.KEYCODE_SPACE && isYButtonPressed) {
			// Bボタンが押されている間はBACKボタンのイベントを無視する
			return true;  // イベントを消費したことを示す
		}

		//X&DEL
		if (event.getKeyCode() == KeyEvent.KEYCODE_BUTTON_X) {
			if (event.getAction() == KeyEvent.ACTION_DOWN) {
				isXButtonPressed = true;
			} else if (event.getAction() == KeyEvent.ACTION_UP) {
				isXButtonPressed = false;
			}
		}
		else if (event.getKeyCode() == KeyEvent.KEYCODE_DEL && isXButtonPressed) {
			return true;  // イベントを消費したことを示す
		}

		//A&DPAD_CENTER
		if (event.getKeyCode() == KeyEvent.KEYCODE_BUTTON_A) {
			if (event.getAction() == KeyEvent.ACTION_DOWN) {
				isAButtonPressed = true;
			} else if (event.getAction() == KeyEvent.ACTION_UP) {
				isAButtonPressed = false;
			}
		}
		else if (event.getKeyCode() == KeyEvent.KEYCODE_DPAD_CENTER && isAButtonPressed) {
			return true;  // イベントを消費したことを示す
		}

		//START&DPAD_CENTER
		if (event.getKeyCode() == KeyEvent.KEYCODE_BUTTON_START) {
			if (event.getAction() == KeyEvent.ACTION_DOWN) {
				isSTARTButtonPressed = true;
			} else if (event.getAction() == KeyEvent.ACTION_UP) {
				isSTARTButtonPressed = false;
			}
		}
		else if (event.getKeyCode() == KeyEvent.KEYCODE_DPAD_CENTER && isSTARTButtonPressed) {
			return true;  // イベントを消費したことを示す
		}

		return super.dispatchKeyEvent(event);
	}



	public boolean onKeyUp(int keyCode, KeyEvent event)
	{

		pMouse.onKeyUp( keyCode , event );

		return true;
	}
	void SetScreenSize( int w , int h )
	{
		m_Width = w;
		m_Height = h;
	}


	@Override
	public boolean onGenericMotionEvent( MotionEvent event )
	{
		float hx= event.getAxisValue( MotionEvent.AXIS_HAT_X );
		float hy= event.getAxisValue( MotionEvent.AXIS_HAT_Y );
		float ax= event.getAxisValue( MotionEvent.AXIS_X );
		float ay= event.getAxisValue( MotionEvent.AXIS_Y );
		float az= event.getAxisValue( MotionEvent.AXIS_Z );
		float rx= event.getAxisValue( MotionEvent.AXIS_RX );
		float ry= event.getAxisValue( MotionEvent.AXIS_RY );
		float rz= event.getAxisValue( MotionEvent.AXIS_RZ );
		float tr3= event.getAxisValue( MotionEvent.AXIS_LTRIGGER );
		float tr4= event.getAxisValue( MotionEvent.AXIS_RTRIGGER );

		float tr1 = event.getAxisValue( MotionEvent.AXIS_THROTTLE );
		float tr2 = event.getAxisValue( MotionEvent.AXIS_RUDDER );
		float tr = event.getAxisValue( MotionEvent.AXIS_GAS );
		float tl = event.getAxisValue( MotionEvent.AXIS_BRAKE );
		float tr5 = event.getAxisValue( MotionEvent.AXIS_DISTANCE );
		float tr6 = event.getAxisValue( MotionEvent.AXIS_WHEEL );
		float tr7 = event.getAxisValue( MotionEvent.AXIS_TILT );

		float[] pos = new float[3];
		float[] rot = new float[3];
		float[] hat = new float[3];
		float[] trg = new float[2];

		pos[0] = ax;
		pos[1] = ay;
		pos[2] = az;

		rot[0] = rx;
		rot[1] = ry;
		rot[2] = rz;

		hat[0] = hx;
		hat[1] = hy;

		trg[0] = tl;
		trg[1] = tr;

		int id = event.getDeviceId();
		pMouse.onAnalogInfo( id , pos ,rot , trg , hat );

		return false;
	}

	@Override
	protected void onActivityResult(int requestCode, int ResultCode, Intent date)
	{
		//BluetoothがONにされた場合の処理

		if(requestCode == REQUEST_ENABLE_BLUETOOTH)
		{
			if( ResultCode == Activity.RESULT_OK )
			{
				m_pBlueTooth.SetEnable();

				if( !m_pBlueTooth.IsExistPairedMachine())
				{
					m_pBlueTooth.SearchBluetoothDevice();
				}

			}else{
				//finish();
			}
		}

		if(requestCode == REQUEST_CODE && ResultCode == RESULT_OK)
		{
			// 認識結果を ArrayList で取得
			ArrayList<String> candidates =
					date.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);

			if(candidates.size() > 0) {
				// 認識結果候補で一番有力なものを表示
				GameGirlActivity.GetInstance().SetToastText( candidates.get(0) );
			}
		}

	}


	void update()
	{
		Configuration config = getResources().getConfiguration();

		//各種コンフィグ

		if (config.orientation == Configuration.ORIENTATION_LANDSCAPE)
		{
		}
		else if (config.orientation == Configuration.ORIENTATION_PORTRAIT)
		{
		}

		//マルチタッチ

		int touchCnt = 0;

		for( int ii=0; ii<pMouse.GetMax(); ii++ )
		{
            if( pMouse.m_Touch[ii].id < 0 ) continue;
			touchCnt ++;
            GameGirlJNI.cursorUpdate( ii , (int)pMouse.m_Touch[ii].pos_x , (int)pMouse.m_Touch[ii].pos_y , pMouse.m_Touch[ii].status );

			if( pMouse.m_Touch[ii].status == 2 )
			{
				pMouse.m_Touch[ii].Reset();
			}
		}

        if( touchCnt == 0 )
        {
            GameGirlJNI.cursorUpdate( 999 , 0,0 , 0  );
        }

		if( GGA_ENABLE_CENSOR == true )
		{
			float[] CensorData = pCensor.GetFloatArray();
			GameGirlJNI.UpdateCensors(CensorData);
		}

		//WebView

		GameGirlJNI.SetJava2Cpp(m_JavaState , 32 );

		for( int ii=0; ii<32; ii++ )
		{
			m_JavaState[ii] = 0x00;
		}

	}

	//class StGameGirlActivityState implements Serializable
	//{
	//	int[] Mouse = new int[5*3];		//マウスカーソル
	//
	//	}



	//-------------------------------------------------------------------------------
	//BlueTooth関連
	//-------------------------------------------------------------------------------

	private gxBlueTooth m_pBlueTooth;
	BroadcastReceiver mReceiver;
	final int REQUEST_ENABLE_BLUETOOTH = 0;

	public void BlueToothInit()
	{
		m_pBlueTooth = new gxBlueTooth();

		m_pBlueTooth.SetHandle(s_pInstance);

		switch( m_pBlueTooth.Init()  ) {
			case 0:
				//OFFだった場合、ONにすることを促すダイアログを表示する画面に遷移
				Intent btOn = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
				startActivityForResult(btOn, REQUEST_ENABLE_BLUETOOTH);
				return;
			case 1:
				//BlueToothが有効だった
				m_pBlueTooth.SetEnable();
				break;
			default:
				return;
		}

		if( !m_pBlueTooth.IsExistPairedMachine())
		{
			//ペアリングされた機器がなければ探しに行く
            m_pBlueTooth.SearchBluetoothDevice();
		}

		m_pBlueTooth.Start();
	}



//	private void searchBluetoothDevice()
//	 */
//	{
//		//検出されたデバイスからのブロードキャストを受ける
//
//		mReceiver = new BroadcastReceiver() {
//
//			@Override
//			public void onReceive(Context context, Intent intent) {
//				String action = intent.getAction();
//				String dName = null;
//				BluetoothDevice foundDevice;
//				if (BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action))
//				{
//
//				}
//
//				if (BluetoothDevice.ACTION_FOUND.equals(action))
//				{
//					//デバイスが検出された
//					foundDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
//
//					if ((dName = foundDevice.getName()) != null)
//					{
//						if (foundDevice.getBondState() != BluetoothDevice.BOND_BONDED)
//						{
//							//接続したことのないデバイスのみアダプタに詰める
//							//nonPairedDeviceAdapter.add(dName + "\n" + foundDevice.getAddress());
//							//Log.d("ACTION_FOUND", dName);
//
//							m_pBlueTooth.AddDeviceList( foundDevice );//, foundDevice.getName() );
//							//m_BTDeviceNameArray[mDeviceCnt] = foundDevice.getName();
//							//m_BTDeviceArray[mDeviceCnt] = foundDevice;
//							//mDeviceCnt++;
//
//						}
//					}
//					//nonpairedList.setAdapter(nonPairedDeviceAdapter);
//				}
//				if (BluetoothDevice.ACTION_NAME_CHANGED.equals(action)) {
//					//名前が検出された
//					//Log.d("ACTION_NAME_CHANGED", dName);
//					foundDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
//					if (foundDevice.getBondState() != BluetoothDevice.BOND_BONDED) {
//						//接続したことのないデバイスのみアダプタに詰める
//						//nonPairedDeviceAdapter.add(dName + "\n" + foundDevice.getAddress());
////					m_BTDeviceNameArray[mDeviceCnt] = foundDevice.getName();
////					m_BTDeviceArray[mDeviceCnt]	  = foundDevice;
//						m_pBlueTooth.AddDeviceList( foundDevice );//, foundDevice.getName() );
//					}
//					//nonpairedList.setAdapter(nonPairedDeviceAdapter);
//				}
//
//				if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action))
//				{
//					//スキャン終了
//					m_pBlueTooth.DiscoveryCancel();
//
//					if( mReceiver != null )
//					{
//						unregisterReceiver(mReceiver);
//					}
//
//					m_pBlueTooth.MakeDeviceList();
//
//					m_pBlueTooth.SelectPairingMachine("ペアリングする機器を選択してください");
//				}
//			}
//		};
//
//		// インテントフィルタの作成
//		//IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
//		IntentFilter filter = new IntentFilter();
//		filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
//		filter.addAction(BluetoothDevice.ACTION_FOUND);
//		filter.addAction(BluetoothDevice.ACTION_NAME_CHANGED);
//		filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
//		//			registerReceiver(searchBluetoothDevice, filter);
//		// ブロードキャストレシーバの登録
//		registerReceiver(mReceiver , filter);
//	};


	//-------------------------------------------------------------------------------
	//-------------------------------------------------------------------------------
	final int REQUEST_CODE = 0x010101;
	private SpeechRecognizer speech;
	public void InputVoice()
	{
		// 音声認識の　Intent インスタンス
		int lang = 0;

		Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);

		if(lang == 0){
			// 日本語
			//intent.putExtra(RecognizerIntent.EXTRA_PROMPT,"サンプル音声入力");
			//intent.putExtra(RecognizerIntent.EXTRA_PREFER_OFFLINE, true);
			intent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE,getPackageName());
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.JAPAN.toString() );
			intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,	RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
			intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 1);
			intent.putExtra(RecognizerIntent.EXTRA_PROMPT,"voice.recognition.test");
			intent.putExtra(RecognizerIntent.EXTRA_PARTIAL_RESULTS, true);
		}
		else if(lang == 1){
			// 英語
			intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.ENGLISH.toString() );
		}
		else if(lang == 2){
			// Off line mode
			intent.putExtra(RecognizerIntent.EXTRA_PREFER_OFFLINE, true);
		}
		else{
			intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
			RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
		}

		//intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "音声を入力");

		try {
			// インテント発行
			//startActivityForResult(intent, REQUEST_CODE);
			speech = SpeechRecognizer.createSpeechRecognizer(s_pInstance);
			speech.setRecognitionListener(new CSpeechListener());
			speech.startListening(intent);
		}
		catch (ActivityNotFoundException e) {
			e.printStackTrace();
			//textView.setText(R.string.error);
			GameGirlActivity.GetInstance().SetToastText("error?");
		}
	}

	// 結果を受け取るために onActivityResult を設置

	class CSpeechListener implements RecognitionListener
	{
		public void onReadyForSpeech(Bundle params)
		{
			Log.d(TAG, "onReadyForSpeech");
		}
		public void onBeginningOfSpeech()
		{
			Log.d(TAG, "onBeginningOfSpeech");
		}
		public void onRmsChanged(float rmsdB)
		{
			Log.d(TAG, "onRmsChanged");
		}
		public void onBufferReceived(byte[] buffer)
		{
			Log.d(TAG, "onBufferReceived");
		}
		public void onEndOfSpeech()
		{
			Log.d(TAG, "onEndofSpeech");
		}
		public void onError(int error)
		{
			Log.d(TAG,  "error " +  error);
			GameGirlActivity.GetInstance().SetToastText( "error--" );
		}
		public void onResults(Bundle results)
		{
			String str = new String();
			Log.d(TAG, "onResults " + results);
			ArrayList data = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
			for (int i = 0; i < data.size(); i++)
			{
				Log.d(TAG, "result " + data.get(i));
				str += data.get(i);
			}
			//mText.setText("results: "+String.valueOf(data.size()));
			GameGirlActivity.GetInstance().SetToastText( String.valueOf(data.size()) );
		}
		public void onPartialResults(Bundle partialResults)
		{
			Log.d(TAG, "onPartialResults");
		}
		public void onEvent(int eventType, Bundle params)
		{
			Log.d(TAG, "onEvent " + eventType);
		}
	}

	//-------------------------------------------------
	//cppから直接呼ばれる
    //-------------------------------------------------

	public static Object GetAssetManager()
	{
		Object obj = s_pInstance.getAssets();
		return obj;
	}

	public static void StartVibration( int msec )
	{
		//振動を制御する
		s_pInstance.pMouse.Rumble( msec );
	}

	public static String GetPackageName()
	{
		//パッケージ名を取得する
		return s_pInstance.getPackageName();
	}

	public static int HTTPAccess( int id , String url )
	{
		//httpテスト

		try {
			s_pInstance.pHttpClinent = new gxHttpClient();
			s_pInstance.pHttpClinent.SetID( id );
			s_pInstance.pHttpClinent.execute( new URL( url ) );
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}

		return 0;
	}

	public static void SetToastText( String text )
	{
		s_pInstance.m_ToastString = text;
	}

	public static void SetWebViewURL( String text )
	{
		s_pInstance.m_WebViewURLString = text;
		s_pInstance.m_bOpenWebBrowser = false;
	}
	public static void SetWebBrowserURL( String text )
	{
		s_pInstance.m_WebViewURLString = text;
		s_pInstance.m_bOpenWebBrowser = true;
	}

	public static String GetClipBoardText()
	{
		// クリップボードからClipDataを取得
		// システムのクリップボードを取得
		ClipboardManager clipManager = (ClipboardManager) s_pInstance.getSystemService(CLIPBOARD_SERVICE);

		ClipData clipData  = clipManager.getPrimaryClip();
		ClipData.Item item = clipData.getItemAt(0);

		String text = item.getText().toString();

		return text;
	}

	public static void SetClipBoardText( String text )
	{
		ClipData.Item clipDataItem = new ClipData.Item(text);

		//MIMETYPEの作成
		String[] mimeType = new String[1];
		mimeType[0] = ClipDescription.MIMETYPE_TEXT_URILIST;

		//クリップボードに格納するClipDataオブジェクトの作成
		ClipData cd = new ClipData(new ClipDescription("text_data", mimeType), clipDataItem);

		//クリップボードにデータを格納
		ClipboardManager clipManager = (ClipboardManager) s_pInstance.getSystemService(CLIPBOARD_SERVICE);
		clipManager.setPrimaryClip(cd);
	}

	public static String getWifiIPAddress(Context context) {
		WifiManager manager = (WifiManager)context.getSystemService(WIFI_SERVICE);
		WifiInfo info = manager.getConnectionInfo();
		int ipAddr = info.getIpAddress();
		String ipString = String.format("%02d.%02d.%02d.%02d",
				(ipAddr>>0)&0xff, (ipAddr>>8)&0xff, (ipAddr>>16)&0xff, (ipAddr>>24)&0xff);
		return ipString;
	}

	FrameLayout webViewLayout = null;

	public  void WebViewProc()
	{
		//WebViewを起動する


		if( m_WebViewURLString != "" )
		{
			if( m_bOpenWebBrowser ) {

				Uri uri = Uri.parse(m_WebViewURLString);
				m_WebViewURLString = "";
				Intent i = new Intent(Intent.ACTION_VIEW,uri);
				startActivity(i);
			}
			else
			{
				if( m_bOpenWebView == true || m_WebViewURLString.equals("CloseWebView") )
				{
					m_bOpenWebView = false;
					if(webViewLayout != null)
					{
						((ViewGroup) webViewLayout.getParent()).removeView(webViewLayout);
						//m_ViewWebView.setVisibility(View.GONE);
					}
					webViewLayout = null;


					m_ViewWebView = null;
				}
				else
				{
					m_WebViewClient = new WebView(GameGirlActivity.GetInstance() );
					m_WebViewClient.setWebViewClient(new WebViewClient());
					m_WebViewClient.getSettings().setJavaScriptEnabled(true);
					m_WebViewClient.getSettings().setUserAgentString("Desktop");

					m_WebViewClient.loadUrl(m_WebViewURLString );
					m_WebViewClient.setVisibility(View.VISIBLE);


					DisplayMetrics dispMetrics = new DisplayMetrics();
					GameGirlActivity.GetInstance().getWindowManager().getDefaultDisplay().getMetrics(dispMetrics);

					FrameLayout.LayoutParams webViewLayoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);

					webViewLayoutParams.width  = 9*dispMetrics.widthPixels/10;
					webViewLayoutParams.height = 9*dispMetrics.heightPixels/10;
					m_WebViewClient.setLayoutParams(webViewLayoutParams);

					webViewLayout = new FrameLayout(this);
					webViewLayout.addView(m_WebViewClient);

					FrameLayout.LayoutParams flp = new FrameLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
					flp.gravity = Gravity.CENTER;
					addContentView(webViewLayout, flp);

					m_bOpenWebView = true;
				}

			}
			m_WebViewURLString = "";
		}
	}

    public static int callGetStorageAvailableSize(){

        //String path = Environment.getExternalStorageDirectory().getAbsolutePath();		// external path.
        String path = Environment.getDataDirectory().getPath();							// internal path.

        StatFs fs = new StatFs(path);
        long blockSize = fs.getBlockSizeLong();
        long availableBlockSize = fs.getAvailableBlocksLong();
        return (int)((blockSize * availableBlockSize) / (1024 * 1024));
    }

	public static void callCloseApp() {
		s_pInstance.finish();
	}

}
