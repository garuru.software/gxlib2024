del app\build\outputs\bundle\debug\app-debug.aab
del pad\aab_test.apks

call gradlew bundle

java -jar bundletool-all-1.6.1.jar build-apks --bundle=app\build\outputs\bundle\debug\app-debug.aab --output=pad\aab_test.apks --local-testing
java -jar bundletool-all-1.6.1.jar install-apks --apks=pad\aab_test.apks
