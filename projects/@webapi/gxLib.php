<?php
	//------------------------------------
	// garuru web api
	// 2017.05.30
	//------------------------------------

	$globalIP = $_SERVER["REMOTE_ADDR"];
	$operation = $_GET['ope'];

	switch( $operation ){
	case "GetIP":
		//新規ユーザー登録
		SetHeader();
		getIPAddress();
		SetFooter();
		break;

	case "Rand":
		//ランダム値を得る
		SetHeader();
		getRand();
		SetFooter();
		break;

	case "CheckPort7777":
		//ランダム値を得る
		SetHeader();
		CheckPort7777();
		SetFooter();
		break;

	case "phpinfo":
		phpinfo();
		break;

	default:
		SetHeader(0);
		printf(",NoFunction\n" );
		SetFooter();
		break;
	}



function SetHeader( $num=0 )
{
	printf("GARURU");
}

function SetFooter()
{
	printf(",EOF,\n");
}

function getIPAddress()
{
	global $globalIP;

	$ip = explode("." , $globalIP );
	$iphex = sprintf( "%02X%02X%02X%02X"   ,$ip[0],$ip[1],$ip[2],$ip[3] );
	$ipdec = sprintf( "%02d,%02d,%02d,%02d",$ip[0],$ip[1],$ip[2],$ip[3] );

//	printf("[GET_IP],". "[DEC]," . $ipdec . "," );
//	printf("[GET_IP],". "[HEX]," . $iphex . "," );

	printf( "," . $ipdec . "," . $iphex );
}


function getRand()
{
	$rand = rand()%255;

	printf(",%d",$rand );

}

function CheckPort7777()
{
	global $globalIP;

	printf(",[CheckPort]1");

	$sock = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP);

	printf(",[CheckPort]2");

    $msg = "Port7777Check";

    $len = strlen($msg);

    socket_sendto($sock, $msg, $len, 0, globalIP, 7777 );

	printf(",[CheckPort]3");
    socket_close($sock);

	printf(",[CheckPort]4");

}

?>

