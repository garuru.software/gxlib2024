//
//  ViewController.h
//  gxLib2020
//
//  Created by garuru on 2019/11/02.
//  Copyright © 2019 garuru.co.jp. All rights reserved.
//

//#import <UIKit/UIKit.h>
//@interface ViewController : UIViewController

#import <UIKit/UIKit.h>
#import <GLKit/GLKit.h>
#import <CoreMotion/CoreMotion.h>

@interface ViewController : GLKViewController{
    CADisplayLink* displaylink;
}

- (void) displayUpdate:(CADisplayLink *)displayLink;

@property (nonatomic, assign) BOOL shouldHideStatusBar;

@end

