//
//  ViewController.m
//  gxLib2020
//
//  Created by garuru on 2019/11/02.
//  Copyright 息 2019 garuru.co.jp. All rights reserved.
//

#import "ViewController.h"
#import <GameController/GameController.h>
//#import "gxWebView.h"

#include <gxLib.h>
#include <gxLib/gx.h>
#include <gxLib/gxPadManager.h>
#include <gxLib/gxOrderManager.h>
#include <gxLib/gxRender.h>
#include <gxLib/hard/iOS/CGamePad.h>
#import <AudioToolbox/AudioToolbox.h>
#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>
//@interface AppDelegate : UIResponder <UIApplicationDelegate>

void AppInit();
void AppUpdate();
void AppFlip();
void AppFinish();

//@interface GCController : NSObject
//+ (NSArray *)controllers

int s_bStatusBarDisp = 0;

@interface ViewController ()
{
    CMMotionManager *_cmm;
}

@property (strong, nonatomic) EAGLContext *context;

@property(retain , nonatomic) GCGamepad * gamepad;
@property(retain , nonatomic) GCExtendedGamepad * gamepadex;

@end


@implementation ViewController

struct multiTouch
{
    unsigned int id = 0;
    int finger = -1;
    int mx = 0;
    int my = 0;
    int ox = 0;
    int oy = 0;
    int press = 0;
    int wait = 0;
};

std::map< unsigned int , multiTouch > m_Touch;
//gxPadManager::StSensor Info;


WKWebView *wkWebView = nullptr;


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES3];
    GLKView *view = (GLKView *)self.view;
    view.context = self.context;
    
    //アプリのSleepを抑制する
    [UIApplication sharedApplication].idleTimerDisabled = YES;
    
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didRotate:)
                                                 name:UIDeviceOrientationDidChangeNotification
                                               object:nil];
    
    [EAGLContext setCurrentContext:self.context];
    self.view.multipleTouchEnabled = YES;
    self.preferredFramesPerSecond = 30;
    
    AppInit();
    
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    
    [center addObserver:self
               selector:@selector(controllerDidConnect)
                   name:GCControllerDidConnectNotification
                 object:nil];
    
    [center addObserver:self
               selector:@selector(controllerDidDisconnect)
                   name:GCControllerDidDisconnectNotification
                 object:nil];
    
    [GCController startWirelessControllerDiscoveryWithCompletionHandler:^{
        [self completionWirelessControllerDiscovery];
    }];
    
    NSArray *controllers = [GCController controllers];
    for (GCController *controller in controllers) {
        [self setEventController:controller];
        
    }
    
    //Sencor
    _cmm = [CMMotionManager new];
    
    
#if 0
    _cmm.accelerometerUpdateInterval = 0.05;
    _cmm.gyroUpdateInterval = 0.05;
    _cmm.magnetometerUpdateInterval = 0.05;
    
    NSOperationQueue *que = [NSOperationQueue currentQueue];
    //accel
    {
        CMAccelerometerHandler handler =
        ^(CMAccelerometerData *data, NSError *error) {
            
            //CMAcceleration ca = accelerometerData.acceleration;
            //[self showAccelerometerValue:ca];
            //[self moveTarget:ca];
            double timestamp = data.timestamp;
            double x = data.acceleration.x;
            double y = data.acceleration.y;
            double z = data.acceleration.z;
            
        };
        [_cmm startAccelerometerUpdatesToQueue:que
                                   withHandler:handler];
    }
    
    //gyro
    {
        CMGyroHandler handler = ^(CMGyroData *data, NSError *error) {
            double timestamp = data.timestamp;
            double x = data.rotationRate.x;
            double y = data.rotationRate.y;
            double z = data.rotationRate.z;
        };
        
        [_cmm startGyroUpdatesToQueue:que
                          withHandler:handler];
    }
    
    //magne
    {
        CMMagnetometerHandler handler = ^(CMMagnetometerData *data, NSError *error) {
            double timestamp = data.timestamp;
            double x = data.magneticField.x;
            double y = data.magneticField.y;
            double z = data.magneticField.z;
            double roll  = data.attitude.roll;
            double pitch = data.attitude.pitch;
            double yaw   = data.attitude.yaw;
        };
        
        [_cmm startMagnetometerUpdatesToQueue:que
                                  withHandler:handler];
    }
#endif
    _cmm.deviceMotionUpdateInterval = 0.01;  // 100Hz
    CMDeviceMotionHandler handler = ^(CMDeviceMotion *motion, NSError *error) {
        double timestamp = motion.timestamp;
        
        double gravityX = motion.gravity.x;
        double gravityY = motion.gravity.y;
        double gravityZ = motion.gravity.z;
        
        double x = motion.userAcceleration.x;
        double y = motion.userAcceleration.y;
        double z = motion.userAcceleration.z;
        
        double rx = motion.rotationRate.x;
        double ry = motion.rotationRate.y;
        double rz = motion.rotationRate.z;
        
        double mx = motion.magneticField.field.x;
        double my = motion.magneticField.field.y;
        double mz = motion.magneticField.field.z;
        CMMagneticFieldCalibrationAccuracy accuracy = motion.magneticField.accuracy;
        
        double roll = motion.attitude.roll;
        double pitch = motion.attitude.pitch;
        double yaw = motion.attitude.yaw;
        
        /*
         Info.accel.x = x;
         Info.accel.y = y;
         Info.accel.z = z;
         Info.gyro.x = rx;
         Info.gyro.y = ry;
         Info.gyro.z = rz;
         Info.magne.x = mx;
         Info.magne.y = my;
         Info.magne.z = mz;
         
         Info.accel *= 1000.0f;
         Info.gyro *= 1000.0f;
         Info.magne *= 1000.0f;
         */
        Float32 ff[12]={0};
        ff[0] = x;
        ff[1] = y;
        ff[2] = z;
        ff[3] = rx;
        ff[4] = ry;
        ff[5] = rz;
        ff[6] = roll;//mx;
        ff[7] = pitch;//my;
        ff[8] = yaw;//mz;
        
        CGamePad::GetInstance()->SetCensorData(ff);
    };
    
    [_cmm startDeviceMotionUpdatesToQueue:[NSOperationQueue currentQueue] withHandler:handler];
    
    //[manager startDeviceMotionUpdatesUsingReferenceFrame:CMAttitudeReferenceFrameXTrueNorthZVertical toQueue:[NSOperationQueue currentQueue] withHandler:handler];
    
    //if (manager.deviceMotionActive) {
    //    [manager stopMagnetometerUpdates];
    //}
    
    
    displaylink = [CADisplayLink displayLinkWithTarget:self selector:@selector(displayUpdate:)];
    if (@available(iOS 15.0, *)) {
        CAFrameRateRange range;
        range.minimum = 10;
        range.maximum = 60;
        range.preferred = 60;
        displaylink.preferredFrameRateRange = range;
    } else {
        displaylink.preferredFramesPerSecond = 60;
    }
    [displaylink addToRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
    
}

-(BOOL)prefersStatusBarHidden
{
    return self.shouldHideStatusBar;
}

- (void) displayUpdate:(CADisplayLink *)displayLink {
    UIApplicationState state = [[UIApplication sharedApplication] applicationState];
    if (state == UIApplicationStateActive)
    {//GPU function is must foreground only
        AppUpdate();
    }

}

- (void)showAccelerometerValue:(CMAcceleration)accelerometerData {
    
    //Accel
    printf( ".2f/%.2f /%.2f",
           accelerometerData.x,accelerometerData.y,accelerometerData.z);
}




#pragma mark - NSNotificationCenter
- (void)controllerDidConnect
{
    NSArray *controllers = [GCController controllers];
    for (GCController *controller in controllers) {
        [self setEventController:controller];
    }
}

- (void)controllerDidDisconnect
{
    self.gamepad = nil;
}

- (void)completionWirelessControllerDiscovery
{
    NSArray *controllers = [GCController controllers];
    for (GCController *controller in controllers) {
        [self setEventController:controller];
    }
}


- (void)setEventController:(GCController *)controller
{
    self.gamepad = controller.gamepad;
    self.gamepadex = controller.extendedGamepad;
    
    //AppDelegate *delegate =  [UIApplication sharedApplication].delegate;
    //GCGamepad *pad = delegate.controller.gamepad;
    
    if (!self.gamepad && controller.gamepad)
    {
        
        self.gamepad = controller.gamepad;
        
    }
    if (!self.gamepadex && controller.extendedGamepad)
    {
        self.gamepadex = controller.extendedGamepad;
    }
    
    __weak typeof(self) weakSelf = self;
    
    self.gamepad.buttonA.valueChangedHandler = ^(GCControllerButtonInput *button, float value, BOOL pressed) {
        //if (pressed)
        [weakSelf pressedButtonA:pressed];
    };
    
}

- (void)pressedButtonA:(bool)push
{
    //  self.myLabel.text = @"A";
    //bool buttons = false;
    //buttons = (self.gamepadex.buttonA.pressed)? true : false;
    
    //CGamePad::GetInstance()->InputKeyCheck( push? 0x01 : 0x02 , 96);
    
}
- (void)update
{
    CGamePad::GetInstance()->InputKeyCheck( (self.gamepadex.dpad.up.pressed)? 0x01 : 0x02 , 19);
    CGamePad::GetInstance()->InputKeyCheck( (self.gamepadex.dpad.down.pressed)? 0x01 : 0x02 , 20);
    CGamePad::GetInstance()->InputKeyCheck( (self.gamepadex.dpad.left.pressed)? 0x01 : 0x02 , 21);
    CGamePad::GetInstance()->InputKeyCheck( (self.gamepadex.dpad.right.pressed)? 0x01 : 0x02 , 22);
    CGamePad::GetInstance()->InputKeyCheck( (self.gamepadex.buttonA.pressed)? 0x01 : 0x02 , 96);
    CGamePad::GetInstance()->InputKeyCheck( (self.gamepadex.buttonB.pressed)? 0x01 : 0x02 , 97);
    CGamePad::GetInstance()->InputKeyCheck( (self.gamepadex.buttonX.pressed)? 0x01 : 0x02 , 99);
    CGamePad::GetInstance()->InputKeyCheck( (self.gamepadex.buttonY.pressed)? 0x01 : 0x02 , 100);
    
    
    
    
    CGamePad::GetInstance()->InputKeyCheck( (self.gamepadex.leftThumbstickButton.pressed)? 0x01 : 0x02 , 200);
    CGamePad::GetInstance()->InputKeyCheck( (self.gamepadex.rightThumbstickButton.pressed)? 0x01 : 0x02 , 201);
    
    CGamePad::GetInstance()->InputKeyCheck( (self.gamepadex.buttonMenu.pressed)? 0x01 : 0x02 , 108);    //START
    CGamePad::GetInstance()->InputKeyCheck( (self.gamepadex.buttonOptions.pressed)? 0x01 : 0x02 , 109);//SELECT
    CGamePad::GetInstance()->InputKeyCheck( (self.gamepadex.buttonHome.pressed)? 0x01 : 0x02 , 204);//PS
    
    
    CGamePad::GetInstance()->InputKeyCheck( (self.gamepadex.leftShoulder.pressed)? 0x01 : 0x02 , 102);
    CGamePad::GetInstance()->InputKeyCheck( (self.gamepadex.rightShoulder.pressed)? 0x01 : 0x02 , 103);
    CGamePad::GetInstance()->InputKeyCheck( (self.gamepadex.leftTrigger.pressed)? 0x01 : 0x02 , 104);
    CGamePad::GetInstance()->InputKeyCheck( (self.gamepadex.rightTrigger.pressed)? 0x01 : 0x02 , 105);
    //self.gamepadex.leftThumbstickButton.pressed
    //self.gamepadex.rightThumbstickButton.pressed
    
    float analog[12];
    analog[0] = self.gamepadex.leftThumbstick.xAxis.value;
    analog[1] = self.gamepadex.leftThumbstick.yAxis.value*-1.0f;
    analog[2] = 0.0f;
    
    analog[3] = self.gamepadex.rightThumbstick.xAxis.value;
    analog[4] = self.gamepadex.rightThumbstick.yAxis.value*-1.0f;
    analog[5] = 0.0f;
    
    analog[6] = self.gamepadex.leftTrigger.value;
    analog[7] = 0.0f;
    analog[8] = 0.0f;
    
    analog[9] = self.gamepadex.rightTrigger.value;  //rY
    analog[10] = 0.0f;
    analog[11] = 0.0f;
    CGamePad::GetInstance()->SetAnalog(analog);
    
    if( CiOS::GetInstance()->m_WevViewURL == "CloseWebView" )
    {
        [wkWebView removeFromSuperview];
        wkWebView = nullptr;
        CiOS::GetInstance()->m_WevViewURL = "";
    }
    else if( CiOS::GetInstance()->m_WevViewURL != "" )
    {
        
        if( CiOS::GetInstance()->m_bOpenWebBrowser )
        {
            static std::string url;
            url = CiOS::GetInstance()->m_WevViewURL;
            NSString *str = [NSString stringWithUTF8String:url.c_str()];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str] options:@{} completionHandler:nil];
            CiOS::GetInstance()->m_WevViewURL = "";
            return;
        }
        
        wkWebView = [[WKWebView alloc]init];
        //wkWebView.UIDelegate = self;
        
        CGRect rect = self.view.frame;
        rect.origin.x = rect.size.width/10;
        rect.origin.y = rect.size.height/10;
        rect.size.width *= 0.8f;
        rect.size.height *= 0.8f;
        
        wkWebView.frame = rect;
        
        //gxWebView* pWebView = [[gxWebView alloc]init];
        [self.view addSubview:wkWebView];
        
        const char *pStr = CiOS::GetInstance()->m_WevViewURL.c_str();
        
        NSString *nsstrDst = [NSString stringWithUTF8String:pStr];
        NSURL *url = [NSURL URLWithString:nsstrDst];
        //CiOS::GetInstance()->m_WevViewURL.c_str();
        //url= [NSURL URLWithString:@"test"];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        
        [wkWebView loadRequest:request];
        
        //pWebView->openWebview("http://www.yahoo.co.jp/");
        CiOS::GetInstance()->m_WevViewURL = "";
    }
    
    Float32 fRatio = 0.0f;
    if(gxPadManager::GetInstance()->GetMotorStat( 0 , 0 , &fRatio ))
    {
        //AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate));
        UISelectionFeedbackGenerator *feedback = [[UISelectionFeedbackGenerator alloc] init];
        [feedback selectionChanged];
        [feedback prepare];
    }
    
    static NSInteger status = 0;
    if(s_bStatusBarDisp != status)
    {
        status = s_bStatusBarDisp;
        self.shouldHideStatusBar = (s_bStatusBarDisp == 1);
        [UIView animateWithDuration:0.5 animations:^{
            [self setNeedsStatusBarAppearanceUpdate];
        }];
    }
    
    self.preferredFramesPerSecond = gxRender::GetInstance()->GetRenderFPS();
    
}

- (void)glkView:(GLKView *)view drawInRect:(CGRect)rect
{
    //AppUpdate();
    for( auto itr = m_Touch.begin(); itr != m_Touch.end();)
    {
        
        CGamePad::GetInstance()->InputCursorCheck(
                                                  itr->second.finger,
                                                  itr->second.press,
                                                  itr->second.mx,
                                                  itr->second.my );
        
        if(itr->second.press == 0 )
        {
            //itr =
            itr = m_Touch.erase(itr);
        }
        else
        {
            if( ( itr->second.mx == itr->second.ox) && ( itr->second.my == itr->second.oy) )
            {
                if( itr->second.wait > 0 )
                {
                    itr->second.wait --;
                    if( itr->second.wait == 0 )
                    {
                        //itr->second.press = 0;
                    }
                }
            }
            else
            {
                itr->second.wait = 30;
            }
            itr->second.ox = itr->second.mx;
            itr->second.oy = itr->second.my;
            itr++;
        }
    }
    
    Uint32 buttons = 0x00;
    /*
     buttons  |= (self.gamepadex.dpad.up.pressed)?    JOY_U : 0;
     buttons  |= (self.gamepadex.dpad.right.pressed)? JOY_R : 0;
     buttons  |= (self.gamepadex.dpad.down.pressed)?  JOY_D : 0;
     buttons  |= (self.gamepadex.dpad.left.pressed)?  JOY_L : 0;
     
     buttons  |= (self.gamepadex.buttonA.pressed)? BTN_A : 0;
     buttons  |= (self.gamepadex.buttonB.pressed)? BTN_B : 0;
     buttons  |= (self.gamepadex.buttonX.pressed)? BTN_X : 0;
     buttons  |= (self.gamepadex.buttonY.pressed)? BTN_Y : 0;
     
     buttons  |= (self.gamepadex.leftShoulder.pressed)? BTN_L1 : 0;
     buttons  |= (self.gamepadex.leftShoulder.pressed)? BTN_R1 : 0;
     buttons  |= (self.gamepadex.leftTrigger.pressed)? BTN_L2  : 0;
     buttons  |= (self.gamepadex.rightTrigger.pressed)? BTN_R2 : 0;
     gxLib::DebugLog("right = %d",self.gamepadex.dpad.right.pressed? 1 : 2);
     */
    //    buttons  |= (self.gamepadex.leftThumbstickButton.pressed)? BTN_L2 : 0;
    //    buttons  |= (self.gamepadex.rightThumbstickButton.pressed)? BTN_L2 : 0;
    
    
    //gxPadManager::GetInstance()->SetPadInfo   ( 0 , buttons );
    
    //   AppUpdate();
}

- (void)dealloc
{
    
    if(displaylink!=nil){
        [displaylink invalidate];
    }
    //[EAGLContext setCurrentContext:self.context];
    
    //if ([EAGLContext currentContext] == self.context) {
    //    [EAGLContext setCurrentContext:nil];
    //}
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    
    //[coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context) {
    //    [self logScreenResolution];
    //} completion:nil];
    
    float scale = [UIScreen mainScreen].scale;
    float scrW0  = UIScreen.mainScreen.nativeBounds.size.height;
    float scrH0 = UIScreen.mainScreen.nativeBounds.size.width;
    float scrW1 = self.view.bounds.size.width;
    float scrH1 = self.view.bounds.size.height;
    float scrW2  = [UIScreen mainScreen].bounds.size.width;
    float scrH2 = [UIScreen mainScreen].bounds.size.height;
    
    
    printf("viewWillTransitionToSize\n");
    printf("{%.1f\n",scale );
    printf("{%.1f,%.1f}\n",scrW0,scrH0 );
    printf("{%.1f,%.1f}\n",scrW1,scrH1 );
    printf("{%.1f,%.1f\n",scrW2,scrH2 );
    
    CGameGirl::GetInstance()->SetWindowSize( scrW2*scale , scrH2*scale ); // h * w !???
    CGameGirl::GetInstance()->AdjustScreenResolution();
}

- (void) didRotate:(NSNotification *)notify
{
    
    float scale = [UIScreen mainScreen].scale;
    float scrW0  = UIScreen.mainScreen.nativeBounds.size.height;
    float scrH0 = UIScreen.mainScreen.nativeBounds.size.width;
    float scrW1 = self.view.bounds.size.width;
    float scrH1 = self.view.bounds.size.height;
    float scrW2  = [UIScreen mainScreen].bounds.size.width;
    float scrH2 = [UIScreen mainScreen].bounds.size.height;
    
    printf("didRotate\n");
    printf("{%.1f\n",scale );
    printf("{%.1f,%.1f}\n",scrW0,scrH0 );
    printf("{%.1f,%.1f}\n",scrW1,scrH1 );
    printf("{%.1f,%.1f\n",scrW2,scrH2 );
    CGameGirl::GetInstance()->SetWindowSize( scrW2*scale , scrH2*scale ); // h * w !???
    CGameGirl::GetInstance()->AdjustScreenResolution();
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    if ([self isViewLoaded] && ([[self view] window] == nil)) {
        self.view = nil;
        
        [EAGLContext setCurrentContext:self.context];
        AppFinish();
        
        if ([EAGLContext currentContext] == self.context) {
            [EAGLContext setCurrentContext:nil];
        }
        self.context = nil;
    }
    
}


int makeFingerID()
{
    int tbl[3] = {-1,-1,-1};
    int cnt = 0;
    for( auto itr = m_Touch.begin(); itr!=m_Touch.end(); itr ++)
    {
        int n = itr->second.finger;
        tbl[n] = cnt;
        cnt ++;
        if( cnt >= 3 ) break;
    }
    for(int ii=0; ii<3;ii++)
    {
        if(tbl[ii] == -1 ) return ii;
    }
    
    return -1;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    float scale = [UIScreen mainScreen].scale;
    
    int cnt = 0;
    for (UITouch *touch in touches)
    {
        CGPoint p = [touch locationInView:self.view];
        //[self rotatePoint:&p rotate:orientation];
        
        multiTouch t;
        t.finger = makeFingerID();
        t.mx = p.x*scale;
        t.my = p.y*scale;
        if( t.finger != -1)
        {
            t.id = [touch hash];
            t.press = 1;
            m_Touch[t.id] = t;
            //CGamePad::GetInstance()->InputCursorCheck(t.finger, true, p.x*scale,p.y*scale );
        }
        cnt ++;
    }
    
}




-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    float scale = [UIScreen mainScreen].scale;
    
    for (UITouch *touch in touches)
    {
        CGPoint p = [touch locationInView:self.view];
        
        unsigned int id = [touch hash];//[touch getId];
        
        if(m_Touch.find(id) != m_Touch.end())
        {
            m_Touch[id].press = 1;
            m_Touch[id].mx = p.x*scale;
            m_Touch[id].my = p.y*scale;
        }
        
        //CGamePad::GetInstance()->InputCursorCheck(t.finger, true, p.x*scale,p.y*scale );
        //if( cnt >= GX_TOUCH_MAX ) break;
    }
    
    //    CPadManager::GetInstance()->SetMouseButtonDown( 0 );
    
    
    
    
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    float scale = [UIScreen mainScreen].scale;
    for (UITouch *touch in touches)
    {
        CGPoint p = [touch locationInView:self.view];
        
        unsigned int id = [touch hash];//[touch getId];
        
        if(m_Touch.find(id) != m_Touch.end())
        {
            m_Touch[id].press = 0;
            m_Touch[id].mx = p.x*scale;
            m_Touch[id].my = p.y*scale;
        }
        
        //CGamePad::GetInstance()->InputCursorCheck(cnt, false, p.x*scale,p.y*scale );
        //if( cnt >= GX_TOUCH_MAX ) break;
    }
    
}

void ResetInput()
{
    m_Touch.clear();
    for(int ii=0; ii<3; ii++)
    {
        CGamePad::GetInstance()->InputCursorCheck(ii,false,0,0);
    }
}


@end
