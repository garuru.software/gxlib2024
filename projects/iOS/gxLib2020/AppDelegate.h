//
//  AppDelegate.h
//  gxLib2020
//
//  Created by garuru on 2019/11/02.
//  Copyright © 2019 garuru.co.jp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

