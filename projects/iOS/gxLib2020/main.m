//
//  main.m
//  gxLib2020
//
//  Created by garuru on 2019/11/02.
//  Copyright © 2019 garuru.co.jp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
